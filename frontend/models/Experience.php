<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "experience".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $subject
 * @property string $position
 * @property string $from_date
 * @property string $to_date
 * @property integer $country
 * @property integer $city
 * @property integer $lang
 * @property string $created_date_time
 * @property string $modified_date_time
 *
 * @property User $user
 * @property Country $country0
 * @property Language $lang0
 * @property ExperienceItems[] $experienceItems
 */
class Experience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'subject', 'position', 'from_date', 'to_date', 'country', 'city', 'created_date_time'], 'required'],
            [['user_id', 'country', 'city', 'lang'], 'integer'],
            [['from_date', 'to_date', 'created_date_time', 'modified_date_time'], 'safe'],
            [['subject', 'position'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'subject' => Yii::t('app', 'Subject'),
            'position' => Yii::t('app', 'Position'),
            'from_date' => Yii::t('app', 'From Date'),
            'to_date' => Yii::t('app', 'To Date'),
            'country' => Yii::t('app', 'Country'),
            'city' => Yii::t('app', 'City'),
            'lang' => Yii::t('app', 'Lang'),
            'created_date_time' => Yii::t('app', 'Created Date Time'),
            'modified_date_time' => Yii::t('app', 'Modified Date Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang0()
    {
        return $this->hasOne(Language::className(), ['id' => 'lang']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperienceItems()
    {
        return $this->hasMany(ExperienceItems::className(), ['exp_id' => 'id']);
    }
}
