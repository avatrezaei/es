<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $tittle
 * @property integer $type
 * @property string $jour_name
 * @property string $jour_year
 * @property string $jour_number
 * @property string $jour_pages
 * @property string $Conf_name
 * @property string $Conf_date
 * @property string $Conf_country
 * @property string $Conf_city
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tittle', 'type', 'jour_name', 'jour_year', 'jour_number', 'jour_pages', 'Conf_name', 'Conf_date', 'Conf_country', 'Conf_city'], 'required'],
           // [['type'], 'integer'],
            [['tittle'], 'string', 'max' => 150],
            [['jour_name'], 'string', 'max' => 50],
            [['jour_year', 'Conf_name', 'Conf_date'], 'string', 'max' => 10],
            [['jour_number', 'jour_pages', 'Conf_country', 'Conf_city'], 'string', 'max' => 20]
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'شناسه',
            'tittle' => 'عنوان',
            'type' => 'نوع',
            'jour_name' => 'نام مجله',
            'jour_year' => 'سال نشر',
            'jour_number' => 'شماره',
            'jour_pages' => 'صفحات',
            'Conf_name' => 'نام کنفرانس',
            'Conf_date' => 'تاریخ برگزاری',
            'Conf_country' => ' کشور',
            'Conf_city' => 'شهر',
        ];
    }
}
