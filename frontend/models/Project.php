<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property integer $title
 * @property integer $date
 * @property integer $city
 * @property integer $country
 * @property integer $desc
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['id', 'title', 'date', 'city', 'country', 'desc'], 'safe'],
//            [['id', 'tittle', 'date', 'city', 'country', 'desc'], 'string',
//                'message'=>'iiiiiiiiiiiii'
//                ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'شناسه',
            'title' => 'عنوان',
            'date' => 'Date',
            'city' => 'City',
            'country' => 'Country',
            'desc' => 'Desc',
        ];
    }
}
