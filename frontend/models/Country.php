<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $en_title
 * @property string $fa_title
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['en_title', 'fa_title'], 'required'],
            [['en_title', 'fa_title'], 'email'],
            [['en_title', 'fa_title'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'en_title' => Yii::t('app', 'English Title'),
            'fa_title' => Yii::t('app', 'Persian  Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicBackgrounds()
    {
        return $this->hasMany(AcademicBackground::className(), ['country' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiences()
    {
        return $this->hasMany(Experience::className(), ['country' => 'id']);
    }
}
