<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form about `app\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type'], 'integer'],
            [['tittle', 'jour_name', 'jour_year', 'jour_number', 'jour_pages', 'Conf_name', 'Conf_date', 'Conf_country', 'Conf_city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                 'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'tittle', $this->tittle])
            ->andFilterWhere(['like', 'jour_name', $this->jour_name])
            ->andFilterWhere(['like', 'jour_year', $this->jour_year])
            ->andFilterWhere(['like', 'jour_number', $this->jour_number])
            ->andFilterWhere(['like', 'jour_pages', $this->jour_pages])
            ->andFilterWhere(['like', 'Conf_name', $this->Conf_name])
            ->andFilterWhere(['like', 'Conf_date', $this->Conf_date])
            ->andFilterWhere(['like', 'Conf_country', $this->Conf_country])
            ->andFilterWhere(['like', 'Conf_city', $this->Conf_city]);

        return $dataProvider;
    }
}
