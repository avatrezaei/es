<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\AcademicBackground;

/**
 * AcademicBackgroundSearch represents the model behind the search form about `\frontend\models\AcademicBackground`.
 */
class AcademicBackgroundSearch extends AcademicBackground
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'science_level', 'country', 'lang'], 'integer'],
            [['title', 'from_date', 'to_date', 'theses_title', 'supervisor', 'city', 'created_date_time', 'modified_date_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AcademicBackground::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'science_level' => $this->science_level,
            'from_date' => $this->from_date,
            'to_date' => $this->to_date,
            'country' => $this->country,
            'lang' => $this->lang,
            'created_date_time' => $this->created_date_time,
            'modified_date_time' => $this->modified_date_time,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'theses_title', $this->theses_title])
            ->andFilterWhere(['like', 'supervisor', $this->supervisor])
            ->andFilterWhere(['like', 'city', $this->city]);

        return $dataProvider;
    }
}
