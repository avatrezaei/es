<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "academic_background".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $science_level
 * @property string $title
 * @property string $from_date
 * @property string $to_date
 * @property string $theses_title
 * @property string $supervisor
 * @property integer $country
 * @property string $city
 * @property integer $lang
 * @property string $created_date_time
 * @property string $modified_date_time
 *
 * @property Country $country0
 * @property User $user
 * @property Language $lang0
 * @property SicenceLevel $scienceLevel
 */
class AcademicBackground extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'academic_background';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'science_level', 'title', 'from_date', 'to_date', 'theses_title', 'supervisor', 'country', 'city'], 'safe'],
            [['id', 'user_id', 'science_level', 'country', 'lang'], 'integer'],
            [['from_date', 'to_date', 'created_date_time', 'modified_date_time'], 'safe'],
            [['title', 'theses_title', 'supervisor', 'city'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'science_level' => Yii::t('app', 'Science Level'),
            'title' => Yii::t('app', 'Title'),
            'from_date' => Yii::t('app', 'From Date'),
            'to_date' => Yii::t('app', 'To Date'),
            'theses_title' => Yii::t('app', 'Theses Title'),
            'supervisor' => Yii::t('app', 'Supervisor'),
            'country' => Yii::t('app', 'Country'),
            'city' => Yii::t('app', 'City'),
            'lang' => Yii::t('app', 'Lang'),
            'created_date_time' => Yii::t('app', 'Created Date Time'),
            'modified_date_time' => Yii::t('app', 'Modified Date Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang0()
    {
        return $this->hasOne(Language::className(), ['id' => 'lang']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScienceLevel()
    {
        return $this->hasOne(SicenceLevel::className(), ['id' => 'science_level']);
    }
}
