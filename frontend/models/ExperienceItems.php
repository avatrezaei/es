<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "experience_items".
 *
 * @property integer $id
 * @property integer $exp_id
 * @property string $item
 * @property string $created_date_time
 * @property string $modified_date_time
 *
 * @property Experience $exp
 */
class ExperienceItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'experience_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exp_id', 'item', 'created_date_time', 'modified_date_time'], 'required'],
            [['exp_id'], 'integer'],
            [['created_date_time', 'modified_date_time'], 'safe'],
            [['item'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'exp_id' => Yii::t('app', 'Exp ID'),
            'item' => Yii::t('app', 'Item'),
            'created_date_time' => Yii::t('app', 'Created Date Time'),
            'modified_date_time' => Yii::t('app', 'Modified Date Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExp()
    {
        return $this->hasOne(Experience::className(), ['id' => 'exp_id']);
    }
}
