<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property string $office
 * @property integer $gender
 * @property integer $age
 * @property integer $grade
 * @property string $edu
 * @property string $univ
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $valid
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Project[] $projects
 * @property ResearchIntrest[] $researchIntrests
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'last_name', 'office', 'edu', 'univ', 'homepage', 'auth_key', 'password_hash', 'email', 'tel', 'mobile', 'created_at', 'updated_at'], 'required'],
            [['gender', 'age', 'grade', 'status', 'created_at', 'updated_at', 'login_attemp_count', 'lock', 'valid'], 'integer'],
            [['last_login_time', 'last_login_attemp_time'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['name', 'last_name', 'office', 'univ'], 'string', 'max' => 100],
            [['edu', 'homepage'], 'string', 'max' => 200],
            [['auth_key'], 'string', 'max' => 32],
            [['tel', 'mobile'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'name' => Yii::t('app', 'Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'office' => Yii::t('app', 'Office'),
            'gender' => Yii::t('app', 'Gender'),
            'age' => Yii::t('app', 'Age'),
            'grade' => Yii::t('app', 'Grade'),
            'edu' => Yii::t('app', 'Edu'),
            'univ' => Yii::t('app', 'Univ'),
            'homepage' => Yii::t('app', 'Homepage'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'tel' => Yii::t('app', 'Tel'),
            'mobile' => Yii::t('app', 'Mobile'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'login_attemp_count' => Yii::t('app', 'Login Attemp Count'),
            'last_login_time' => Yii::t('app', 'Last Login Time'),
            'last_login_attemp_time' => Yii::t('app', 'Last Login Attemp Time'),
            'lock' => Yii::t('app', 'Lock'),
            'valid' => Yii::t('app', 'Valid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicBackgrounds()
    {
        return $this->hasMany(AcademicBackground::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiences()
    {
        return $this->hasMany(Experience::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResearchIntrests()
    {
        return $this->hasMany(ResearchIntrest::className(), ['user_id' => 'id']);
    }
    
        /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }
}
