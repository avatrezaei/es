<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    
      'sourceLanguage'=>'fa',
'language'=>'fa-IR',
  
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
        'modules' => [
             'YumUsers' => [
            'class' => 'frontend\modules\YumUsers\YumUsers',
        ],
              'gridview' =>  [
        'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
    ]
        ],
            

    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        
         'i18n' => [
        'translations' => [
            'app*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/messages',
             'sourceLanguage'=>'fa',
                'fileMap' => [
                    'app' => 'app.php',
                    //'app/error' => 'error.php',
                ],
            ],
        ],
    ],
        
        
        'user' => [
            
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
            
 
    'params' => $params,
];
