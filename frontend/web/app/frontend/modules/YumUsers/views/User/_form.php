<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'salt')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'office')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'organization_id')->textInput() ?>

    <?= $form->field($model, 'department_id')->textInput() ?>

    <?= $form->field($model, 'gender')->textInput() ?>

    <?= $form->field($model, 'age')->textInput() ?>

    <?= $form->field($model, 'grade')->textInput() ?>

    <?= $form->field($model, 'edu')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'univ')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'homepage')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'tel')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 256]) ?>

    <?= $form->field($model, 'type_assist')->textInput() ?>

    <?= $form->field($model, 'login_attemp_count')->textInput() ?>

    <?= $form->field($model, 'update_date')->textInput() ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'last_login_time')->textInput() ?>

    <?= $form->field($model, 'last_login_attemp_time')->textInput() ?>

    <?= $form->field($model, 'lastaction')->textInput() ?>

    <?= $form->field($model, 'lock')->textInput() ?>

    <?= $form->field($model, 'active')->textInput() ?>

    <?= $form->field($model, 'valid')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
