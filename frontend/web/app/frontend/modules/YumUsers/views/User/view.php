<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'password',
            'salt',
            'name',
            'last_name',
            'office',
            'organization_id',
            'department_id',
            'gender',
            'age',
            'grade',
            'edu',
            'univ',
            'homepage',
            'tel',
            'mobile',
            'email:email',
            'type_assist',
            'login_attemp_count',
            'update_date',
            'create_date',
            'last_login_time',
            'last_login_attemp_time',
            'lastaction',
            'lock',
            'active',
            'valid',
        ],
    ]) ?>

</div>
