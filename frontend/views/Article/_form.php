<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
        $("#new_country").on("pjax:end", function() {
            $.pjax.reload({container:"#articles"});  //Reload GridView
        });
    });'
);
?>

<div class="article-form">
<?php yii\widgets\Pjax::begin(['id' => 'new_country']) ?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>

    <div class="form-group col-lg-6"> 
        <?= $form->field($model, 'tittle')->textInput(['maxlength' => 150]) ?>
    </div>
    <div class="form-group col-lg-6"> 
        <?= $form->field($model, 'type')->dropDownList(['jour-wrapper' => 'مجله', 'conf-wrapper' => 'کنفرانس'], ['id' => 'paper-type', 'prompt' => '-- انتخاب کنید --']) ?>

    </div>

    <div id="jour-wrapper" class="more-data" style="display: none;">

        <div class="form-group col-lg-6"> 
            <?= $form->field($model, 'jour_name')->textInput(['maxlength' => 50]) ?>
        </div>
        <div class="form-group col-lg-6"> 

            <?= $form->field($model, 'jour_year')->textInput(['maxlength' => 10]) ?>
        </div>
        <div class="form-group col-lg-6"> 

            <?= $form->field($model, 'jour_number')->textInput(['maxlength' => 20]) ?>
        </div>
        <div class="form-group col-lg-6"> 

            <?= $form->field($model, 'jour_pages')->textInput(['maxlength' => 20]) ?>
        </div>
    </div>
    <div id="conf-wrapper" class="more-data" style="display: none;">
        <div class="form-group col-lg-6"> 

            <?= $form->field($model, 'Conf_name')->textInput(['maxlength' => 10]) ?>
        </div>
        <div class="form-group col-lg-6"> 

            <?= $form->field($model, 'Conf_date')->textInput(['maxlength' => 10]) ?>
        </div>
        <div class="form-group col-lg-6"> 

            <?= $form->field($model, 'Conf_country')->textInput(['maxlength' => 20]) ?>
        </div>
        <div class="form-group col-lg-6"> 

            <?= $form->field($model, 'Conf_city')->textInput(['maxlength' => 20]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'ذخیره اطلاعات' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    
    <?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript" src="js/script.js"></script>

<script>
    $(function () {
        $("#paper-type").change(function () {
            $(".more-data").hide();
            $("#" + $(this).val()).fadeIn();

        });


    });
</script>

