<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tittle') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'jour_name') ?>

    <?= $form->field($model, 'jour_year') ?>

    <?php // echo $form->field($model, 'jour_number') ?>

    <?php // echo $form->field($model, 'jour_pages') ?>

    <?php // echo $form->field($model, 'Conf_name') ?>

    <?php // echo $form->field($model, 'Conf_date') ?>

    <?php // echo $form->field($model, 'Conf_country') ?>

    <?php // echo $form->field($model, 'Conf_city') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
