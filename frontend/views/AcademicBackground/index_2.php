
<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
 
$this->title = Yii::t('app', 'Project');
$this->params['breadcrumbs'][] = $this->title;
?>
 
    <h1><?= Html::encode($this->title) ?></h1>
 


 <?php
$gridColumns = [
                ['class' => 'yii\grid\SerialColumn'],
 [
        'format' => 'html',
        'attribute' => 'title',
       // 'headerOptions' => ['width' => '80%',],
    ],
    [
        'format' => 'html',
        'attribute' => 'date',
      //  'headerOptions' => ['width' => '80%',],
    ],
        [
        'format' => 'html',
        'attribute' => 'lang',
       // 'headerOptions' => ['width' => '80%',],
    ],
       
    [ 'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {delete}',
        'headerOptions' => ['width' => '5%', 'class' => 'activity-view-link',],
        'contentOptions' => ['class' => 'padding-left-5px'],
//        'buttons' => [
//            
//                ],
            ],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'expandAllTitle' => 'Expand all',
                'collapseTitle' => 'Collapse all',
                'expandIcon' => '<span class="glyphicon glyphicon-expand"></span>',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => Url::to(['project/detail', 'id' =>7]),
                'detailRowCssClass' => GridView::TYPE_DEFAULT,
                'pageSummary' => false,
            ],
        ];
        ?>

    
    
    

 

 
<!-- Render create form -->    
    <?= $this->render('_form', [
        'model' => $model,
        'massage'=>$massage
    ]) ?>
 <?php
 
 ?>


<div class="countries-index">


        <?php
        Pjax::begin(['id' => 'Projects']);

        echo \kartik\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumns,
            'panel' => [
               // 'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th"></i> Projects</h3>',
                'type' => 'primary',
                //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
                'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
                'footer' => true
            ],
            'toolbar' => [
                [
                    'content' =>
                    Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                        'type' => 'button',
                        // 'title'=>Yii::t('kvgrid', 'Add Book'), 
                        'title' => 'Reset Grid',
                        'class' => 'btn btn-success'
                    ]) . ' ' .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                        'class' => 'btn btn-default',
                        //'title' => Yii::t('kvgrid', 'Reset Grid')
                        'title' => 'Reset Grid'
                    ]),
                    'options' => ['class' => 'btn-group-sm']
                ],
                '{export}',
                '{toggleData}',
            ],
            'toggleDataContainer' => ['class' => 'btn-group-sm'],
            'exportContainer' => ['class' => 'btn-group-sm'],
            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                //  'showPageSummary' => true,
                //  'pageSummaryRowOptions'=>['class' => 'kv-page-summary warning']
//    'floatHeaderOptions'=>['scrollingTop'=>'50'],
//    'hover'=>true
        ]);
        Pjax::end();
        ?>      




</div>
    
    
    