<?php
use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title='View Project'; // $model->name;
$this->params['breadcrumbs'][]=['label'=>Yii::t('app','Country'), 'url'=>['index']];
$this->params['breadcrumbs'][]=$this->title;

// setup your attributes
$attributes=[
    [
        'attribute'=>'fa_title', 
        'value'=>$model->fa_title, 
      'type'=>DetailView::INPUT_TEXT,
    ],
     [
        'attribute'=>'en_title', 
        'value'=>$model->en_title, 
      'type'=>DetailView::INPUT_TEXT,
    ], 
      

];

echo DetailView::widget([
    'model'=>$model,
        'condensed'=>true,
    'hover'=>true,
    //'mode'=>DetailView::MODE_VIEW,
    'panel'=>[
        'heading'=>Yii::t('app','Project') . $model->fa_title,
       'type'=>DetailView::TYPE_INFO,
    ],
    'attributes'=>$attributes,
    'deleteOptions'=>[ // your ajax delete parameters
        'params' => ['id' => $model->id, 'custom_param' => true],
        'url'=>Url::to(['country/delete','id'=>$model->id])
    ],
  
]);
