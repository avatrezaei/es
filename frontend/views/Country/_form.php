<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
 
/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >
    
</div>

<?php
 
$this->registerJs(
   '

$("document").ready(function(){ 
        $("#new_countries").on("pjax:end", function() {
          //    $("#results").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\">t</a></div>");
            //   $("#results").show("slow");

$.pjax.reload({container:"#Countries"});  //Reload GridView)
        });
    });'
);
?>
 
<div class="countries-form">
 
<?php yii\widgets\Pjax::begin(['id' => 'new_countries']) ?>
       <?= $massage ?>
           <?php
$form = ActiveForm::begin(['options' => ['enableClientValidation'=>0,'data-pjax' => true,'id'=>'form3' ]]); ?>
 
  <div class="form-group col-lg-6"> 
    <?= $form->field($model, 'en_title')->textInput(['maxlength' => 50]) ?>
    </div>
    <div class="form-group col-lg-6"> 

    <?= $form->field($model, 'fa_title')->textInput(['maxlength' => 50]) ?>
    </div>
    <hr>
    
    
    
    
 
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'submit_btn']) ?>
    </div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>
</div>

