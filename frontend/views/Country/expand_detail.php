
<?php
use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
echo '<div class="col-sm-4"><img src="'.\Yii::$app->basePath.'\image\3.jpg"  style="background: #d9edf7;
		overflow: hidden;
		left: 0;
		top: 0;
		width: 40%;
		height: 5%;"></div>';
?>
<div class="col-sm-1">
            <div class="kv-button-stack">
            <button data-toggle="tooltip" title="Add to cart" class="btn btn-default btn-lg" type="button"><span class="glyphicon glyphicon-tags"></span></button>
            <button data-toggle="tooltip" title="Call for details" class="btn btn-default btn-lg" type="button"><span class="glyphicon glyphicon-eye-open"></span></button>
            <button data-toggle="tooltip" title="Email for details" class="btn btn-default btn-lg" type="button"><span class="glyphicon glyphicon-envelope"></span></button>
            </div>
        </div>
<?php
// setup your attributes
$attributes=[
    [
        'name'=>'title',
        'attribute'=>'title', 
        //'format'=>'raw', 
        'value'=>Html::encode($model->en_title), 
        'displayOnly'=>true
    ],
    [
        'attribute'=>'lang', 
       // 'format'=>'raw', 
        'value'=>  Html::encode($model->fa_title) ,
        'displayOnly'=>true,
        'inputWidth'=>'40%'
    ],

];
echo '<div class="col-sm-4">';
echo DetailView::widget([
    'model'=>$model,
        'condensed'=>true,
    'hover'=>true,
   'mode'=>DetailView::MODE_VIEW,
    'panel'=>[
        'heading'=>Yii::t('app','Country').' # ' . $model->id,
        'type'=>DetailView::TYPE_INFO,
        'buttons1'=>'{reset}',
        'buttons2'=>'{reset}',
    ],
    'attributes'=>$attributes,
 
]);
echo '</div>';
?>

