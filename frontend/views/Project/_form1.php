<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
 
/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >
    
</div>
<script>
//    $("document").ready(function(){ 
//        $("#new_country").on("pjax:end", function() {
//                $("#results").html("8888888888888");
//
//$.pjax.reload({container:"#articles"});  //Reload GridView)
//        });
//    });
</script>
<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
        $("#new_country").on("pjax:end", function() {
            //   $("#results").html("8888899999999988888888");
             //   $("#results").show("slow");

$.pjax.reload({container:"#Projects"});  //Reload GridView)
        });
    });'
);
?>
 
<div class="countries-form">
   <div class="panel panel-info">
      <div class="panel-heading"><?php echo Yii::t('app', 'Create').' '.Yii::t('app', 'Project')?></div>
      <div class="panel-body">
          <?php yii\widgets\Pjax::begin(['id' => 'new_country']) ?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>
 
  <div class="form-group col-lg-6"> 
            <?= $form->field($model, 'description')->textInput(['maxlength' => 200]) ?>
    </div>
    <div class="form-group col-lg-6"> 

    <?= $form->field($model, 'title')->textInput(['maxlength' => 200]) ?>
    </div>
              <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>
      </div>
    </div>
    
    

    
    
    
    
 

</div>