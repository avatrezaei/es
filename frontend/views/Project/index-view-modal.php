<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
 
$this->title = Yii::t('app', 'Project');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="countries-index">
 
    <h1><?= Html::encode($this->title) ?></h1>
 


 <?php
$gridColumns = [
                ['class' => 'yii\grid\SerialColumn'],
 [
        'format' => 'html',
        'attribute' => 'title',
       // 'headerOptions' => ['width' => '80%',],
    ],
    [
        'format' => 'html',
        'attribute' => 'date',
      //  'headerOptions' => ['width' => '80%',],
    ],
        [
        'format' => 'html',
        'attribute' => 'lang',
       // 'headerOptions' => ['width' => '80%',],
    ],
       
    [ 'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {delete}',
        'headerOptions' => ['width' => '5%', 'class' => 'activity-view-link',],
        'contentOptions' => ['class' => 'padding-left-5px'],
        'buttons' => [
            'view' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                            'id' => 'activity-view-link',
                            'title' => Yii::t('yii', 'View'),
                            'data-toggle' => 'modal',
                            'data-target' => '#activity-modal',
                            'data-id' => $key,
                       //     'data-pjax' => '0',
                ]);
            },
                ],
            ],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'expandAllTitle' => 'Expand all',
                'collapseTitle' => 'Collapse all',
                'expandIcon' => '<span class="glyphicon glyphicon-expand"></span>',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => Url::to(['project/detail', 'id' => 19]),
                'detailRowCssClass' => GridView::TYPE_DEFAULT,
                'pageSummary' => false,
            ],
        ];
        ?>


        <?php
        Pjax::begin();

        echo \kartik\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumns,
            'panel' => [
                'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Projects</h3>',
                'type' => 'primary',
                //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
                'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
                'footer' => true
            ],
            'toolbar' => [
                [
                    'content' =>
                    Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                        'type' => 'button',
                        // 'title'=>Yii::t('kvgrid', 'Add Book'), 
                        'title' => 'Reset Grid',
                        'class' => 'btn btn-success'
                    ]) . ' ' .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                        'class' => 'btn btn-default',
                        //'title' => Yii::t('kvgrid', 'Reset Grid')
                        'title' => 'Reset Grid'
                    ]),
                    'options' => ['class' => 'btn-group-sm']
                ],
                '{export}',
                '{toggleData}',
            ],
            'toggleDataContainer' => ['class' => 'btn-group-sm'],
            'exportContainer' => ['class' => 'btn-group-sm'],
            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                //  'showPageSummary' => true,
                //  'pageSummaryRowOptions'=>['class' => 'kv-page-summary warning']
//    'floatHeaderOptions'=>['scrollingTop'=>'50'],
//    'hover'=>true
        ]);
        Pjax::end();
        ?>      


        <?php
        $this->registerJs(
                "$('.activity-view-link').click(function() {
    $.get(
        'imgview',         
        {
            id: $(this).closest('tr').data('key')
        },
        function (data) {
            $('.modal-body').html(data);
            $('#activity-modal').modal();
        }  
    );
});
    "
        );
        ?>



        <?php
//        Modal::begin([
//            'id' => 'activity-modal',
//            'header' => '<h4 class="modal-title">View Image</h4>',
//            'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',
//        ]);
        
        
        
        Modal::begin([
                        'id' => 'activity-modal',

    'header' => '<h4 class="modal-title">Detail View Demo</h4>',
    //'toggleButton' => ['label' => '<i class="glyphicon glyphicon-th-list"></i> Detail View in Modal', 'class' => 'btn btn-primary'],
  'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',
                ]);
        
        
        
        ?>

<?php
$attributes=[
    [
        'attribute'=>'title', 
        'format'=>'raw', 
        'value'=>'<kbd>'.$model->title.'</kbd>', 
        'displayOnly'=>true
    ],
    'attribute'=>'title',
    [
        'attribute'=>'lang', 
        'format'=>'raw', 
        'value'=>Html::tag('span', ' ', [
            'class'=>'badge',
            'style'=>'background-color' . $model->lang,
        ]) . '<code>' . $model->lang . '</code>',
        'type'=>DetailView::INPUT_COLOR,
        'inputWidth'=>'40%'
    ],
    [
        'attribute'=>'date', 
        'format'=>'date',
        'type'=>DetailView::INPUT_DATE,
        'widgetOptions'=>[
            'pluginOptions'=>['format'=>'yyyy-mm-dd']
        ],
        'inputWidth'=>'40%'
    ],
    [
        'attribute'=>'lang', 
        'label'=>'Available?',
        'format'=>'raw',
        'value'=>$model->lang ,
       // 'type'=>DetailView::INPUT_SWITCH,
        'widgetOptions'=>[
            'pluginOptions'=>[
                'onText'=>'Yes',
                'offText'=>'No',
            ]
        ]
    ],
    [
        'attribute'=>'id',
        'label'=>'Sale Amount ($)',
       // 'format'=>'double',
        'inputWidth'=>'40%'
    ],  
    [
        'attribute'=>'description',
        'format'=>'raw',
        'value'=> $model->description,
      //  'type'=>DetailView::INPUT_SELECT2, 
        'widgetOptions'=>[
            'data'=>ArrayHelper::map(
                    \app\models\Article::find()->orderBy('tittle')->asArray()->all(), 
                'id', 
                'title'
            ),
            'options'=>['placeholder'=>'Select ...'],
            'pluginOptions'=>['allowClear'=>true]
        ],
        'inputWidth'=>'40%'
    ],
    [
        'attribute'=>'description',
        'format'=>'raw',
        'value'=>'<span class="text-justify"><em>' . $model->description . 
                 '</em></span>',
        'type'=>DetailView::INPUT_TEXTAREA, 
        'options'=>['rows'=>4]
    ]
];

echo DetailView::widget([
    'model'=>$model,
        'condensed'=>true,
    'hover'=>true,
    //'mode'=>DetailView::MODE_VIEW,
    'panel'=>[
        'heading'=>'Book # ' . $model->id,
        'type'=>DetailView::TYPE_INFO,
    ],
    'attributes'=>$attributes,
    'deleteOptions'=>[ // your ajax delete parameters
        'params' => ['id' => $model->id, 'custom_param' => true],
        'url'=>Url::to(['project/delete','id'=>$model->id])
    ],
    'updateOptions'=>[
        'url'=>Url::to(['project/Update1','id'=>$model->id])
    ],
        'saveOptions'=>[
        'url'=>Url::to(['project/Update1','id'=>$model->id])
    ],
]);

?>


        <?php Modal::end(); ?>

    
    
    