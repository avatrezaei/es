<?php
use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title='View Project'; // $model->name;
$this->params['breadcrumbs'][]=['label'=>'ProjectS', 'url'=>['index']];
$this->params['breadcrumbs'][]=$this->title;

// setup your attributes
$attributes=[
    [
        'attribute'=>'title', 
        'format'=>'raw', 
        'value'=>'<kbd>'.$model->title.'</kbd>', 
        'displayOnly'=>true
    ],
    'attribute'=>'title',
    [
        'attribute'=>'lang', 
        'format'=>'raw', 
        'value'=>Html::tag('span', ' ', [
            'class'=>'badge',
            'style'=>'background-color' . $model->lang,
        ]) . '<code>' . $model->lang . '</code>',
        'type'=>DetailView::INPUT_COLOR,
        'inputWidth'=>'40%'
    ],
    [
        'attribute'=>'date', 
        'format'=>'date',
        'type'=>DetailView::INPUT_DATE,
        'widgetOptions'=>[
            'pluginOptions'=>['format'=>'yyyy-mm-dd']
        ],
        'inputWidth'=>'40%'
    ],
    [
        'attribute'=>'lang', 
        'label'=>'Available?',
        'format'=>'raw',
        'value'=>$model->lang ,
       // 'type'=>DetailView::INPUT_SWITCH,
        'widgetOptions'=>[
            'pluginOptions'=>[
                'onText'=>'Yes',
                'offText'=>'No',
            ]
        ]
    ],
    [
        'attribute'=>'id',
        'label'=>'Sale Amount ($)',
       // 'format'=>'double',
        'inputWidth'=>'40%'
    ],  
    [
        'attribute'=>'description',
        'format'=>'raw',
        'value'=> $model->description,
      //  'type'=>DetailView::INPUT_SELECT2, 
        'widgetOptions'=>[
            'data'=>ArrayHelper::map(
                    \app\models\Article::find()->orderBy('tittle')->asArray()->all(), 
                'id', 
                'title'
            ),
            'options'=>['placeholder'=>'Select ...'],
            'pluginOptions'=>['allowClear'=>true]
        ],
        'inputWidth'=>'40%'
    ],
    [
        'attribute'=>'description',
        'format'=>'raw',
        'value'=>'<span class="text-justify"><em>' . $model->description . 
                 '</em></span>',
        'type'=>DetailView::INPUT_TEXTAREA, 
        'options'=>['rows'=>4]
    ]
];

echo DetailView::widget([
    'model'=>$model,
        'condensed'=>true,
    'hover'=>true,
    //'mode'=>DetailView::MODE_VIEW,
    'panel'=>[
        'heading'=>'Book # ' . $model->id,
        'type'=>DetailView::TYPE_INFO,
    ],
    'attributes'=>$attributes,
    'deleteOptions'=>[ // your ajax delete parameters
        'params' => ['id' => $model->id, 'custom_param' => true],
        'url'=>Url::to(['project/delete','id'=>$model->id])
    ],
    'updateOptions'=>[
        'url'=>Url::to(['project/Update1','id'=>$model->id])
    ],
        'saveOptions'=>[
        'url'=>Url::to(['project/Update1','id'=>$model->id])
    ],
]);
