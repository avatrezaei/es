<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
 
/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >
    
</div>
<script>
//$(function() {
//    //$('a').pjax({ container: "#update_panel", timeout: 2000 }).live('click', function() {});
//    
//           $("#new_projects").on("pjax:end", function() {
//          //    $("#results").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\">t</a></div>");
//            //   $("#results").show("slow");
//
//$.pjax.reload({container:"#Projects"});  //Reload GridView)
//        });
//  
//    
//    
//    $("#submit_btn").click(function() {
//        var form = $('#form3');
//        $.pjax({
//            container: "#update_panel", 
//            timeout: 2000,
//            url: "@Url.Action("UpdatePart","Part")",
//            data: form.serialize()
//        });
//    });
//});
</script>
<?php
 
$this->registerJs(
   '

$("document").ready(function(){ 
        $("#new_projects").on("pjax:end", function() {
          //    $("#results").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\">t</a></div>");
            //   $("#results").show("slow");

$.pjax.reload({container:"#Projects"});  //Reload GridView)
        });
    });'
);
?>
 
<div class="countries-form">
 
<?php yii\widgets\Pjax::begin(['id' => 'new_projects']) ?>
       <?= $massage ?>
           <?php
$form = ActiveForm::begin(['options' => ['data-pjax' => true,'id'=>'form3' ]]); ?>
 
  <div class="form-group col-lg-6"> 
            <?= $form->field($model, 'description')->textInput(['maxlength' => 200]) ?>
    </div>
    <div class="form-group col-lg-6"> 

    <?= $form->field($model, 'title')->textInput(['maxlength' => 200]) ?>
    </div>
    <hr>
    
    
    
    
 
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'submit_btn']) ?>
    </div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>
</div>