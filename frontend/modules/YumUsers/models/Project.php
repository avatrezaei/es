<?php

namespace frontend\modules\YumUsers\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property string $date
 * @property integer $lang
 * @property string $created_date_time
 * @property string $modified_date_time
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
         //   [[ 'title', 'description'], 'required'],
            [['user_id', 'lang'], 'integer'],
            [['date', 'created_date_time', 'modified_date_time'], 'safe'],
            [['title', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'date' => Yii::t('app', 'Date'),
            'lang' => Yii::t('app', 'Lang'),
            'created_date_time' => Yii::t('app', 'Created Date Time'),
            'modified_date_time' => Yii::t('app', 'Modified Date Time'),
        ];
    }
}
