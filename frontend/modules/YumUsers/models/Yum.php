<?php
namespace frontend\modules\YumUsers\models;
use Yii;

/**
 * Helper class
 * @author tomasz.suchanek@gmail.com
 * @since 0.6
 * @package Yum.core
 *
 */
class Yum {

    /** Register an asset file of Yum */
    public static function register($file) {
        $url = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('application.modules.user.assets'));

        $path = $url . '/' . $file;
        if (strpos($file, 'js') !== false)
            return Yii::app()->clientScript->registerScriptFile($path);
        else if (strpos($file, 'css') !== false)
            return Yii::app()->clientScript->registerCssFile($path);

        return $path;
    }

    public static function hint($message) {
        return '<div class="hint">' . Yum::t($message) . '</div>';
    }

    public static function getAvailableLanguages() {
        $cache_id = 'yum_available_languages';

        $languages = false;
        if (Yii::app()->cache)
            $languages = Yii::app()->cache->get($cache_id);

        if ($languages === false) {
            $translationTable = Yum::module()->translationTable;
            $sql = "select language from {$translationTable} group by language";

            $command = Yii::app()->db->createCommand($sql);

            $languages = array();
            foreach ($command->queryAll() as $row)
                $languages[$row['language']] = $row['language'];

            if (Yii::app()->cache)
                Yii::app()->cache->set($cache_id, $languages);
        }

        return $languages;
    }

    /* set a flash message to display after the request is done */

    public static function setFlash($type, $message, $delay = 0) {

        Yii::app()->user->setFlash($type, $message);
    }

    /* A wrapper for the Yii::log function. If no category is given, we
     * use the YumController as a fallback value.
     * In addition to that, the message is being translated by Yum::t() */

    public static function log($message, $level = 'info', $category = 'application.modules.user.controllers.YumController') {
        if (Yum::module()->enableLogging)
            return Yii::log(Yum::t($message), $level, $category);
    }

    public static function renderFlash() {
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="' . $key . '">' . $message . "</div><br>";

        }
    }

    public static function p($string, $params = array()) {
        return '<p>' . Yum::t($string, $params) . '</p>';
    }

    /** Fetch the translation string from db and cache when necessary */
    /*
     * in tabe ro neveshtm ke age khastim badan az translate ba db estefadeh konim 
     * faghat code inja ro taghir bedm na jahaye dge
     */
    public static function t($string, $params = array(), $category = 'yii') {
        $message = Yii::t($category, $string, $params);
        return $message;
    }

    // returns the Yii User Management module. Frequently used for accessing 
    // options by calling Yum::module()->option
    public static function module($module = 'YumUsers') {
        return Yii::$app->getModule($module);
    }

    public static function hasModule($module) {
        return array_key_exists($module, Yii::app()->modules);
    }

    /**
     * Produces note: "Field with * are required"
     * @since 0.6
     * @return string 
     */
    public static function requiredFieldNote() {
        return CHtml::tag('p', array('class' => 'note'), Yum::t(
                                'Fields with <span class="required">*</span> are required.'
                        ,array(),'yum'));
    }

}

?>
