<?php

namespace frontend\modules\YumUsers\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\captcha\Captcha;

/* * *************
 * Yii validator
 */
use yii\validators\Validator;
use yii\validators\StringValidator;
use yii\validators\NumberValidator;
use frontend\modules\YumUsers\models\YumPasswordValidator;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property string $office
 * @property integer $gender
 * @property integer $age
 * @property integer $grade
 * @property string $edu
 * @property string $univ
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $valid
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Membership[] $memberships
 */
class User extends ActiveRecord implements IdentityInterface {

    public $passwordInput;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const Active = 1;
    const InActive = 0;
    const MALE = 1;
    const FEMALE = 0;
    const GRADE_PHD = 1;
    const GRADE_PHD_STUDENT = 2;
    const GRADE_MASTER = 3;
    const GRADE_MASTER_STUDENT = 4;
    const GRADE_BACHELOR = 5;
    const GRADE_BACHELOR_STUDENT = 6;
    const GRADE_kARDANI = 7;
    const GRADE_kARDANI_STUDENT = 8;
    const GRADE_DIPLOMA = 9;

    public $active;
    public $organization_id;
    public $role;
    public $verifyCode;
    public $generatedPass;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {

//get setting of module
        $usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
        $passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
        $nameRequirements = Yum::module('YumUsers')->nameRequirements;

        /*
         * password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator
         * 
         */
        $passwordrule = array_merge(array('passwordInput', 'frontend\modules\YumUsers\models\YumPasswordValidator', 'except' => 'create,forgetpass,register')
                , $passwordRequirements
        );

        $rules[] = $passwordrule;


        /*
         * username validation from setting
         */

        if ($usernameRequirements) {
            $rules[] = array('username', 'string',
                'max' => $usernameRequirements['maxLen'],
                'min' => $usernameRequirements['minLen'],
            );
            $rules[] = array(
                'username',
                'match',
                'pattern' => $usernameRequirements['match'],
                'message' => Yum::t($usernameRequirements['dontMatchMessage']));
        }

        $rules[] = array('username', 'unique',);

        /*
         * required field in register scenario
         */
        $requiredArray = (Yum::module('YumUsers')->requiredFieldsRegister);
        foreach ($requiredArray as $required) {
            $rules[] = array($required['required'], 'required',
                'on' => $required['on'],
            );
        }

        /*
         * name validation
         */
        if ($nameRequirements) {
            $rules[] = array(['name', 'last_name'], 'string',
                'max' => $nameRequirements['maxLen'],
                'min' => $nameRequirements['minLen'],
            );
            $rules[] = array(
                ['name', 'last_name'],
                'match',
                'pattern' => $nameRequirements['match'],
                'message' => Yum::t($nameRequirements['dontMatchMessage']));
        }
        /*
         * Emali validation
         */

        $rules[] = array('email', 'email');
        $rules[] = array('email', 'unique', 'on' => 'create,update,register');
        $rules[] = array('email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .');


        /*
         * other vlidations
         */
        //  $rules[] = array('role', 'RolesTypeValidation',);
        $rules[] = array(['office', 'univ', 'homepage', 'tel', 'mobile', 'edu'], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

        $rules[] = array(['gender', 'age', 'grade', 'login_attemp_count', 'lock', 'valid'], 'yii\validators\NumberValidator', 'integerOnly' => true,);
        $rules[] = array('grade', 'in', 'range' => array(1, 2, 3, 4, 5, 6, 7, 8, 9));
        $rules[] = array(['gender', 'lock', 'valid'], 'in', 'range' => array(0, 1));

        // The following rule is used by search().
        // Please remove those attributes that should not be searched.
        $rules[] = array(['verifyCode', 'id', 'username', 'passwordInput', 'name', 'last_name', 'office', 'age', 'grade', 'edu', 'univ', 'homepage', 'mobile'], 'safe',);
        //safe for save in db
        $rules[] = array(['auth_key', 'password_hash', 'password_reset_token'], 'safe',);
        
        $rules[] = array('verifyCode', 'captcha',
            'isEmpty' => !Captcha::checkRequirements(),
            'on' => 'register,captch,forgetpass',
            'message' => ' تصویر امنیتی  صحیح را وارد کنید.'
        );

        // die(var_dump($rules));
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yum::t('ID', array(), 'attribute'),
            //'username' => Yii::t('default', 'Username',Yum::module('users')->basePath.'\messages\fa'),
            'username' => Yum::t('Username', array(), 'attribute'),
            'password' => Yum::t('Password', array(), 'attribute'),
            'salt' => Yum::t('Salt', array(), 'attribute'),
            'name' => Yum::t('Name', array(), 'attribute'),
            'last_name' => Yum::t('Last Name', array(), 'attribute'),
            'office' => Yum::t('Office', array(), 'attribute'),
            'organization_id' => Yum::t('Organization', array(), 'attribute'),
            'gender' => Yum::t('Gender', array(), 'attribute'),
            'age' => Yum::t('Age', array(), 'attribute'),
            'grade' => Yum::t('Grade', array(), 'attribute'),
            'edu' => Yum::t('Edu', array(), 'attribute'),
            'univ' => Yum::t('Univercity', array(), 'attribute'),
            'homepage' => Yum::t('Homepage', array(), 'attribute'),
            'tel' => Yum::t('Tel', array(), 'attribute'),
            'mobile' => Yum::t('Mobile', array(), 'attribute'),
            'email' => Yum::t('Email', array(), 'attribute'),
            'type_assist' => Yum::t('Type Assist', array(), 'attribute'),
            'login_attemp_count' => Yum::t('Login Attemp Count', array(), 'attribute'),
            'update_date' => Yum::t('Update Date', array(), 'attribute'),
            'create_date' => Yum::t('Create Date', array(), 'attribute'),
            'last_login_time' => Yum::t('Last Login Time', array(), 'attribute'),
            'last_login_attemp_time' => Yum::t('Last Login Attemp Time', array(), 'attribute'),
            'lastaction' => Yum::t('Last Action', array(), 'attribute'),
            'lock' => Yum::t('Lock', array(), 'attribute'),
            'valid' => Yum::t('Valid', array(), 'attribute'),
            'active' => Yum::t('Active', array(), 'attribute'),
            'role' => Yum::t('Role', array(), 'attribute'),
            'passwordInput' => Yum::t('passwordInput', array(), 'attribute'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicBackgrounds() {
        return $this->hasMany(AcademicBackground::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiences() {
        return $this->hasMany(Experience::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberships() {
        return $this->hasMany(Membership::className(), ['user_id' => 'id']);
    }

    /* Autentication user model */

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**/

    public function createUser() {
        if ($this->validate()) {
            $this->setPassword($this->passwordInput);
            $this->generateAuthKey();
            if ($this->save()) {
                return TRUE;
            }
        }

        return null;
    }

    /************* ***Add new attribute */

    public function attributes() {
        return array_merge(
                parent::attributes(), ['passwordInput']
        );
    }

    public function setpasswordInput($value) {
        $this->passwordInput = $value;
    }

    public function getpasswordInput() {
        return $this->passwordInput;
    }

    
    
    /********************8Feed DropDowns field    **/
    
        public function getGenderOptions() {
        return array(
            self::FEMALE => 'زن',
            self::MALE => 'مرد',
        );
    }
        public function getGenderText() {
        $gradeOption = $this->getGenderOptions();
        return ((!empty($gradeOption[$this->gender])) ? $gradeOption[$this->gender] : '-----');
    }
    /*------------------------------------------*/
    
    
     public function getGradeOptions() {
        return array(
            self::GRADE_PHD => 'دکتری',
            self::GRADE_PHD_STUDENT => 'دانشجوی دکتری',
            self::GRADE_MASTER => 'کارشناسی ارشد',
            self::GRADE_MASTER_STUDENT => 'دانشجوی کارشناسی ارشد',
            self::GRADE_BACHELOR => 'کارشناسی',
            self::GRADE_BACHELOR_STUDENT => 'دانشجوی کارشناسی',
            self::GRADE_kARDANI => 'کاردانی',
            self::GRADE_kARDANI_STUDENT => 'دانشجوی کاردانی',
            self::GRADE_DIPLOMA => 'دیپلم',
        );
    }

    public function getGradeText() {
        $gradeOption = $this->getGradeOptions();
        return ((isset($gradeOption[$this->grade])) ? $gradeOption[$this->grade] : '-----');
    }


    
     public function getActiveOption() {
        return array(
            self::Active => Yum::t('Yes', array(), 'button'),
            self::InActive => Yum::t('No', array(), 'button'),
        );
        
    }
     public function getActiveText($attribute) {
        $activeOption = $this->getActiveOption();
        return ((isset($activeOption[$this->$attribute])) ? $activeOption[$this->$attribute] : 'Unknown Value');
    }




}
