<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>




    <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'passwordInput')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'office')->textInput(['maxlength' => 100]) ?>

    <?=   $form->field($model, 'gender')->dropDownList( $model->getGenderOptions(), ['prompt'=>'']    );     ?>

    <?= $form->field($model, 'age')->textInput() ?>

    <?=   $form->field($model, 'grade')->dropDownList( $model->getGradeOptions(), ['prompt'=>' ']    );     ?>


    <?= $form->field($model, 'edu')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'univ')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'homepage')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'tel')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => 40]) ?>









    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
