<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\YumUsers\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'name',
            'last_name',
            'office',
            // 'gender',
            // '	age',
            // 'grade',
            // 'edu',
            // 'univ',
            // 'homepage',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            // 'email:email',
            // 'tel',
            // 'mobile',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'login_attemp_count',
            // 'last_login_time',
            // 'last_login_attemp_time',
            // 'lock',
            // 'valid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
