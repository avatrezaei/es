<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'name',
            'last_name',
            'office',
            'gender',
            'age',
            'grade',
            'edu',
            'univ',
            'homepage',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
            'tel',
            'mobile',
            'status',
            'created_at',
            'updated_at',
            'login_attemp_count',
            'last_login_time',
            'last_login_attemp_time',
            'lock',
            'valid',
        ],
    ]) ?>

</div>
