<?php
return array(
'Users' => 'کاربران',
'Create' => 'ایجاد',
    'extra_info'=>'افزودن اطلاعات بیشتر',
    'Home Page'=> 'صفحه اصلی',
    'Manage'=> 'مدیریت',
    'Update'=> 'ویرایش',
    'View'=> 'مشاهده',
    'Register'=> 'ثبت نام',
        'Manage Users'=>'مدیریت کاربران',
        'Create User'=>'ایجاد کاربر جدید ',
        'Update User'=>' ویرایش کاربر ',
        'View User'=>' مشاهده کاربر ',
        'Reset Password'=>'بازنشانی کلمه عبور',
        'Change Password'=>'تغییر کلمه عبور',
        'Login'=>'ورود به سامانه',

);
