<?php

namespace frontend\controllers;

use Yii;
use frontend\models\AcademicBackground;
use frontend\models\AcademicBackgroundSearch;
use yii\web\Controller;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;

/**
 * AcademicBackgroundController implements the CRUD actions for AcademicBackground model.
 */
class AcademicBackgroundController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AcademicBackground models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new AcademicBackgroundSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    
    
     
    public function actionIndex() {
        $model = new AcademicBackground();
        $massage = '';
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            if ($model->save()) {
                $massage = '<div class="alert alert-success" role="alert">
                    <a href="#" class="alert-link">'.Yii::t('app','{item} Successfully Added.', array('item' => 'پروژه')).'</a>
                    </div>';       
              
            } else {
                $massage = '<div class="alert alert-danger" role="alert">
                    <a href="#" class="alert-link">'.Yii::t('app','Failed To Add Items To {item}.', array('item' => 'پروژه')).'</a>
                    </div>';            }
        $model = new AcademicBackground();

        }

        $searchModel = new AcademicBackgroundSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'massage' => $massage,
        ]);
    }

    
    
    
    
    
    /**
     * Displays a single AcademicBackground model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AcademicBackground model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AcademicBackground();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AcademicBackground model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AcademicBackground model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AcademicBackground model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AcademicBackground the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AcademicBackground::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
        public function beforeSave($insert ) {
        if (parent::beforeSave($insert)) {
            $userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
            if ($this->isNewRecord) {
                $this->created_date_time = date('Y-m-d H:i:s');
                $this->user_id = $userId;
            } else {
                $this->modified_date_time = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }
}
