<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Country;
use frontend\models\CountrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends Controller
{
 

       public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['index', 'view','delete','Detail'],
//                'rules' => [
//                   
//                    [
//                        'actions' => ['index', 'view','delete','Detail'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Country models.
     * @return mixed
     */

    public function actionIndex() {
        $model = new Country();
        $massage = '';
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            if ($model->save()) {
                $massage = '<div class="alert alert-success" role="alert">
                    <a href="#" class="alert-link">'.Yii::t('app','{item} Successfully Added.', array('item' => Yii::t('app','Country'))).'</a>
                    </div>';       
              
            } else {
                $massage = '<div class="alert alert-danger" role="alert">
                    <a href="#" class="alert-link">'.Yii::t('app','Failed To Add Items To {item}.', array('item' => 'پروژه')).'</a>
                    </div>';            }
        $model = new Country();

        }

        $searchModel = new CountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'massage' => $massage,
        ]);
    }


    /**
     * Displays a single Country model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
die(var_dump($model->errors));
            if ($model->save()) {
                $url=' <a href="' .Url::to(['/country/Index']) . '" class="btn btn-sm btn-info">' .
                        '<i class="glyphicon glyphicon-hand-right"></i>  Click here</a>';
                echo Json::encode([
                    'success' => true,
                    'messages' => [
                        'kv-detail-info' => Yii::t('app','The {item} # {id} wass successfully deleted.{url} to proceed.',
                                                    ['id'=>(int)$id,'item'=>Yii::t('app','Country'),'url'=>$url])]]);
            } else {
                  echo Json::encode([
                    'success' => false,
                    'messages' => [
                        'kv-detail-error' => Yii::t('app','Cannot Edit the {item} # {id}.',
                                                    ['id'=>(int)$id,'item'=>Yii::t('app','Country')])
                    ]
                ]);
            }
        }


        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }
    
     public function actionDetail() {
        if (isset($_POST['expandRowKey']))
            $id = Yii::$app->request->post('expandRowKey');
        else
            die(('Not Found'));


        $model = $this->findModel($id);

        return $this->renderPartial('expand_detail', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Country model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Country();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Country model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Country model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }
public function actionDelete($id=NULL) {
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['custom_param'])) {
            $id = (int) $post['id'];
            if ($this->findModel($id)->delete() ) {
                     $url=' <a href="' .Url::to(['/country/Index']) . '" class="btn btn-sm btn-info">' .
                        '<i class="glyphicon glyphicon-hand-right"></i>  Click here</a>';
                echo Json::encode([
                    'success' => true,
                    'messages' => [
                        'kv-detail-info' => Yii::t('app','The {item} # {id} was successfully deleted.{url} to proceed.',
                                                    ['id'=>(int)$id,'item'=>Yii::t('app','Country'),'url'=>$url])]]);
         
            } else {
                echo Json::encode([
                    'success' => false,
                    'messages' => [
                        'kv-detail-error' => 'Cannot delete the country # ' . $id . '.'
                    ]
                ]);
            } 
            return;
        } else if(isset($_GET['id'])){
                $id=(int)$_GET['id'];
              $this->findModel($id)->delete();  
              return $this->redirect(['index']);

            }
      //  throw new InvalidCallException("You are not allowed to do this operation. Contact the administrator.");
    }
    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Country the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Country::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
