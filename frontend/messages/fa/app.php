<?php

return array(
'ID' => 'شناسه',
'Title'=>'عنوان',    
'Date'=>'تاریخ',    
'Description'=>'توضیحات',    
'Lang'=>'زبان',   
    /****Models***********/
'Project' => 'پروژه ',
    'Country'=>'کشور',
    
    
    
    /*********************/
    
 /*********Country attibutes***************/
'English Title'=>'عنوان انگلیسی',    
    'Persian  Title'=>'عنوان فارسی',
 /************************/
    
'Username' => 'نام کاربری',
    /************Buttoms***************/
'Create' => 'ایجاد',
    'Reset Grid'=>'به روز رسانی ',
    
    /*******************/
    'Created Date Time'=>'تاریخ ایجاد',
    'Modified Date Time'=>'تاریخ ویرایش',
'User ID'=> ' کاربر',
'Username' => 'نام کاربری',
 'Password' => 'کلمه عبور',
 'Salt' => 'Salt',
 'Name' => 'نام',
 'Last Name' => 'نام خانوادگی',
 'Office' => 'سمت',
 'Gender' => 'جنسیت',
 'Age' => 'سن',
 'Grade' => 'مدرک تحصیلی',
 'Edu' => 'رشته تحصیلی',
 'Univercity' => 'دانشگاه',
 'Homepage' => 'صفحه خانگی',
 'Tel' => 'تلفن',
 'Mobile' => 'موبایل',
 'Email' => 'پست الکترونیکی',
 'Type Assist' => 'نوع همکاری',
 'Organization' => 'سازمان',
 'Login Attemp Count' => '',
 'Update Date' => 'تاریخ ویرایش',
 'Create Date' => 'تاریخ ایجاد ',
 'Last Login Time' => 'زمان آخرین ورود',
 'Last Login Attemp Time' => 'زمان آخرین تلاش ورود',
 'Last Action' => 'آخرین فعالیت',
 'Lock' => 'مسدود',
 'Active' => 'فعال',
 'Valid' => 'معتبر',
 'Role'=>'سطح دسترسی',
 'Retype your new password'=>'تکرار کلمه عبور ',
 'Your current password'=>'  کلمه عبور فعلی',
    /*
     * messages
     */
 '{item} Successfully Added.'=>'{item} با موفقیت اضافه شد',
   'Failed To Add Items To {item}.'=>'عملیات اضافه کردن {item} با مشکل مواجه شده است.',
    'The {item} # {id} was successfully deleted.{url} to proceed.'=>'  با موفقیت حذف شد.{item}'

);
