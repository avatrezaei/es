<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
		/**
		 * Global Mandatory Styles 
		 */
		'fum/global/plugins/font-awesome/css/font-awesome.min.css',
		'fum/global/plugins/simple-line-icons/simple-line-icons.min.css',
		'fum/global/plugins/bootstrap/css/bootstrap-rtl.min.css',
		'fum/global/plugins/uniform/css/uniform.default.css',
		'fum/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css',

		/**
		 * Theme Global Styles
		 */
		'fum/global/css/components-rounded-rtl.min.css',
		'fum/global/css/plugins-rtl.min.css',
		'fum/pages/css/login-rtl.min.css',

		/**
		 * Theme Layout Styles
		 */
		'fum/apps/css/default-rtl.css',
		'fum/apps/css/font.css',

	];

	public $js = [
		'fum/global/plugins/bootstrap/js/bootstrap.min.js',
		'fum/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		'fum/global/plugins/uniform/jquery.uniform.min.js',
		'fum/global/scripts/app.min.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}





