<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Faravaghi <info@dkr.co.ir>
 * @since 2.0
 */
class IntrojsAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [ 
		'fum/global/plugins/introjs/introjs.min.css',
	];

	public $js = [
		'fum/global/plugins/introjs/intro.min.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}