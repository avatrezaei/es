<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Faravaghi <info@dkr.co.ir>
 * @since 2.0
 */
class DrawAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [ 
		'fum/apps/css/main.css',
		'fum/global/plugins/avr-barcket/avr-bracket.css',
		'fum/global/css/animate.css',
		'fum/apps/css/bracket.css',
	];

	public $js = [
		'fum/apps/scripts/ajax-modal-popup.js',
		'fum/global/plugins/avr-steps/avr-steps.js',
		'fum/apps/scripts/main.js',
		'fum/global/plugins/avr-barcket/avr-barcket.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}