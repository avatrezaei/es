<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Portfolio extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	
	public $css = [
		'fum/pages/css/portfolio-rtl.min.css',
		'fum/global/plugins/cubeportfolio/css/cubeportfolio.css',
	];
	
	public $js = [
			'fum/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js',
			'fum/pages/scripts/portfolio-1.min.js',
	];
	
	public $depends = [
		'yii\web\YiiAsset',
	];
}
