<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
		'fum/global/plugins/font-awesome/css/font-awesome.min.css',
		'fum/global/plugins/simple-line-icons/simple-line-icons.min.css',
		'fum/global/plugins/hint/hint.min.css',
	];

	public $js = [
		'fum/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		'fum/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
		// 'fum/global/plugins/uniform/jquery.uniform.min.js',
		'fum/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',

		'fum/global/scripts/app.min.js',
		'fum/layouts/layout/scripts/layout.min.js',
		'fum/layouts/layout/scripts/demo.min.js',
		'fum/global/scripts/hd.js',
		'fum/global/scripts/yii_overrides.js',
		
	];

	public $depends = [
		'yii\web\YiiAsset',
		// 'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',

		//additional import of third party alert project
		'backend\assets\SweetAlertAsset',
	];

	public function init()
	{
		parent::init();
		// resetting BootstrapAsset to not load own css files
		\Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
			'css' => []
		];

		/**
		 * Remove Bootstrap.css from depends
		 */
		// foreach(\Yii::$app->assetManager->bundles as $asset){
		// 	if(isset($asset->depends))
		// 	{
		// 		$key = array_search('yii\\bootstrap\\BootstrapAsset', $asset->depends);
		// 		if ($key !== false) {
		// 			unset($asset->depends[$key]);
		// 		}
		// 	}
		// }
	}
}