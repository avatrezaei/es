<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Faravaghi <info@dkr.co.ir>
 * @since 2.0
 */
class LaddaAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [ 
		'fum/global/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
	];

	public $js = [
		'fum/global/plugins/ladda-bootstrap/dist/spin.min.js',
		'fum/global/plugins/ladda-bootstrap/dist/ladda.min.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}