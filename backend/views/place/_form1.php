<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use ibrarturi\latlngfinder\LatLngFinder;
/* @var $this yii\web\View */
/* @var $model app\models\Place */
/* @var $form yii\widgets\ActiveForm */
$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];

?>

<div class="place-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
    <div class="form-body">

    <?php
			echo $form->field(
				$model,
				'name',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

<div class="form-group">
			<label class="col-md-3 col-sm-3 control-label">&nbsp;</label>
			<div class="col-md-9 col-sm-9">
				<?php
					echo LatLngFinder::widget([
						'model' => $model,
						'id' => 'LatLngFinder',
						'latAttribute' => 'gio_width', // Latitude attribute
						'lngAttribute' => 'gio_lenght', // Longitude attribute
						'mapWidth' => '100%', // Map Canvas width
						'mapHeight' => '300px', // Map Canvas mapHeight
						'defaultLat' => ($model->isNewRecord || $model->gio_width == NULL) ? 36.308624992358055 : $model->gio_width, // Default latitude for the map
						'defaultLng' => ($model->isNewRecord || $model->gio_lenght == NULL) ? 59.528931139502674 : $model->gio_lenght, // Default Longitude for the map
						'defaultZoom' => ($model->isNewRecord ? 15 : 17), // Default zoom for the map
						'enableZoomField' => false,
						'setMarker' => !$model->isNewRecord
					]);
				?>
				<div class="help-block"><?= Yii::t('app', 'Click to select the desired point on the map and to change the marker position drag and drop marker on the map.') ?></div>
			</div>
		</div>
		<?php
			echo $form->field(
				$model,
				'gio_width',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
		<?php
			echo $form->field(
				$model,
				'gio_lenght',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
		<?php
			echo $form->field(
				$model,
				'capacity',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
		<?php
			echo $form->field(
				$model,
				'occupy',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
		<?php
			echo $form->field(
				$model,
				'free',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
		<?php
				echo $form->field(
					$model,
					'num_of_floor',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
		<?php
			echo $form->field(
				$model,
				'num_of_suite',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
		
				<?php
			echo $form->field(
				$model,
				'num_of_room',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>


   <div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
</div>

    <?php ActiveForm::end(); ?>

</div>
