<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Suite;
/* @var $this yii\web\View */
/* @var $model backend\models\Eskan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'اتاق'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$detalView = '<div class="row static-info"><div class="col-md-3 name"> {label}: </div><div class="col-md-5 value"> {value} </div></div>';
?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
    <button aria-hidden="true" data-dismiss="modal" class="close"
        type="button">&times;</button>
</div>
<?php endif; ?>
<div class="caravan-view">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div>
                       
<p>
        <?= Html::a('بروزرسانی', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('حذف', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'آیا از حذف محل اسکان مورد نظر مطمئنید؟',
                'method' => 'post',
            ],
        ]) ?>
    </p>
                        <?php
                        echo DetailView::widget ( [ 
                                'model' => $model,
                                'template' => $detalView,
                                'options' => [ 
                                        'tag' => 'div' 
                                ],
                                'attributes' => [
                                
                                [ 
                                     'attribute' => 'id',
                                     'label' => $model->getAttributeLabel ( 'id' ),
                                ],
                                [ 
                                     'attribute' => 'name',
                                     'label' => $model->getAttributeLabel ( 'name' ),
                                ],
                                [ 
                                     'attribute' => 'capacity',
                                    'label' => $model->getAttributeLabel ( 'capacity' ),
                                ],
								[ 
                                     'attribute' => 'occupy',
                                    'label' => $model->getAttributeLabel ( 'occupy' ),
                                ],
								[ 
                                     'attribute' => 'free',
                                    'label' => $model->getAttributeLabel ( 'free' ),
                                ],
								[ 
                                     'attribute' => 'num_of_suite',
                                     'label' => $model->getAttributeLabel ( 'num_of_suite' ),
                                ],
								[ 
                                     'attribute' => 'num_of_room',
                                     'label' => $model->getAttributeLabel ( 'num_of_room' ),
                                ],
								[ 
                                     'attribute' => 'gio_lenght',
                                     'label' => $model->getAttributeLabel ( 'gio_lenght' ),
                                ],
                                [ 
                                     'attribute' => 'gio_width',
                                     'label' => $model->getAttributeLabel ( 'gio_width' ),
                                ],
                                
								
                                
                            ],

                        ] );
                        
                        ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
