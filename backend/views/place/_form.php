<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Place */
/* @var $form yii\widgets\ActiveForm */
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
?>

<div class="place-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
				echo $form->field(
					$model,
					'id',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

   <?php
				echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?php
				echo $form->field(
					$model,
					'capacity',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?php
				echo $form->field(
					$model,
					'occupy',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?php
				echo $form->field(
					$model,
					'free',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
				<?php
				echo $form->field(
					$model,
					'num_of_floor',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?php
				echo $form->field(
					$model,
					'num_of_suite',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?php
				echo $form->field(
					$model,
					'num_of_room',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'ایجاد' : 'بروزرسانی', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
