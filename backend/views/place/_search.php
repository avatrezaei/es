<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlaceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="place-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'capacity') ?>

    <?= $form->field($model, 'occupy') ?>

    <?= $form->field($model, 'free') ?>

    <?php // echo $form->field($model, 'num_of_suite') ?>

    <?php // echo $form->field($model, 'num_of_room') ?>

    <?php // echo $form->field($model, 'gio_lenght') ?>

    <?php // echo $form->field($model, 'gio_width') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
