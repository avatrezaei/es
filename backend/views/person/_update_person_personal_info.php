<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\City;
use app\models\Language;
use backend\models\University;
use  yii\web\View;
use backend\models\User;
use backend\models\Field;
use backend\models\Trend;
use kartik\switchinput\SwitchInput;
use kartik\file\FileInput;
use yii\helpers\Url;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;



$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3';
$errorClass = Yii::$app->params ['errorClass'];



?>



<?php
 $unis = ArrayHelper::map(University::find()->all(), 'id', 'name'); 
 $fields = ArrayHelper::map(Field::find()->all(), 'id', 'name'); 
 $majors = ArrayHelper::map(Trend::find()->all(), 'id', 'name'); 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#update_user").on("pjax:end", function() {
		
//$("#user_quick_info").load(document.URL +  " #date-format");
	  
  //Reload GridView)
		});
	});'
);
?>

<div class="tab-pane active" id="tab_1_1">
	<?= $message ?>
 	<div class="row">
		
		<div class="col-md-6">
	
			<?=$form->field(
						$model,
						'name',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 52, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>	
		</div>

		<div class="col-md-6">	
			<?=$form->field(
					$model,
					'last_name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 52, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
			?>
		</div>

	</div>	
	
	
	
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'nationalId',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		</div>	
	
		<div class="col-md-6 col-sm-6 col-xs-12">

			<?=$form->field(
						$model,
						'idNo',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 10, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		</div>
	</div>
		 	
		
		
	<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?=$form->field(
							$model,
							'fatherName',
							[
								'options' => ['class' => $formGroupClass],
								'template' => $template
							]
						)
						->textInput(
							['maxlength' => 255, 'class' => $inputClass]
						)
						->label(NULL, ['class'=>$labelClass]);
				?>	
		</div>	
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
									$model,
									'universityId',
									[
										'options' => ['class' => $formGroupClass],
										'template' => $template
									]
								)
								->widget(Select2::classname(), [
									'data' => $unis,
									'theme' => Select2::THEME_BOOTSTRAP,
									'options' => [
										'placeholder' => Yii::t('app', 'Please Select a University'),
										'dir' => 'rtl',
									],
								])
								->label(NULL, ['class'=>$labelClass]);


			?>		
		</div>
	</div>
		
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'student_number',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		</div>


		<div class="col-md-6 col-sm-6 col-xs-12">
			<div id="date-format">
					<?php
						$model->birthDate =$model->dateDisplayFormat();
					?>
					<?=$form->field ( $model, 'birthDate',
						[ 'options' => [ 'class' => $formGroupClass,
						//'value' => $model->start,
						],
					   'template' => $template
					   ] 
					   )->widget ( jalaliDatePicker::className (), [ 'options' => ['format' => 'yyyy/mm/dd','viewformat' => 'yyyy/mm/dd','placement' => 'right','todayBtn' => 'linked','id' => 'event-birthDate','class' => 'form-control'  ] ] )->label ( NULL, [ 'class' => $labelClass ] )

				   ?>
	
			</div>
		</div>
	</div>
		
		
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
				$model,
				'email',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => 255, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
			?>  
		</div>	
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'tel',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>  
		
		</div>
	</div>


	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">		
			<?=$form->field(
						$model,
						'mobile',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?> 
		</div>	
		<div class="col-md-6 col-sm-6 col-xs-12">
			  <?=$form->field(
					$model,
					'username',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		
		</div>
	</div>	
		
	<div class="row">

		<div class="col-md-6">

			<?=$form->field(
									$model,
									'field',
									[
										'options' => ['class' => $formGroupClass],
										'template' => $template
									]
								)
								->widget(Select2::classname(), [
									'data' => $fields,
									'theme' => Select2::THEME_BOOTSTRAP,
									'options' => [
										'placeholder' => Yii::t('app', 'Please Select a Field'),
										'dir' => 'rtl',
									],
								])
								->label(NULL, ['class'=>$labelClass]);


			?>


			  
		</div>

		<div class="col-md-6">
			  

			<?=$form->field(
									$model,
									'major',
									[
										'options' => ['class' => $formGroupClass],
										'template' => $template
									]
								)
								->widget(Select2::classname(), [
									'data' => $majors,
									'theme' => Select2::THEME_BOOTSTRAP,
									'options' => [
										'placeholder' => Yii::t('app', 'Please Select a Major'),
										'dir' => 'rtl',
									],
								])
								->label(NULL, ['class'=>$labelClass]);


			?>
		</div>
</div>

		<div class="row"> 	
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'shift',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'startingyear',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		</div>
	</div>
		
	<div class="row">
			
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'startingsemester',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'units',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		</div>
	</div>	
		
	<div class="row">
			
		


		<div class="col-md-6 col-sm-6 col-xs-12">	
			<?php
						   $dataSelectCity = ArrayHelper::map(City::find()->all(), 'id', 'name');   
				echo $form->field(
						$model, 
						'birthCityId',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->widget(Select2::classname(), [
						'data' => $dataSelectCity,
						'theme' => Select2::THEME_BOOTSTRAP,
						'options' => [
							'placeholder' => Yii::t('app', 'Select ...'),
							'dir' => 'rtl',
						],
					])
					->label(NULL, ['class'=>$labelClass]); 
			?>
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">		
			<?php
				$dataSelectCity = ArrayHelper::map(City::find()->all(), 'id', 'name');   
				echo $form->field(
					$model, 
					'city_location_id',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectCity,
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Select ...'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
			?>
		</div>


		



	</div>
			
			
			
	<div class="row">
			
		
	</div>

	<div class="row">

		<div class="col-md-6">
			 

			<?=$form->field(
					$model,
					'address',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textarea(['rows' => 6, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
			?>


			
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?=$form->field(
						$model,
						'jobincome',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
			?>
		</div>
	</div>
		<div class="row">
			<div class="col-md-6">


				<?php
					echo $form->field(
					$model,
					'gender',
					[
						'options' => ['class' => $formGroupClass],
						'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="nutritionmeal-mealtype">{input} </div> {hint} {error}</div>'
					]
				)
				->radioList(
					User::itemAlias('Gender'),
					[
						'tag' => false,
						'unselect' => NULL,
						'item' => function($index, $label, $name, $checked, $value) {
							return '<label class="btn btn-warning '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value, 'autocomplete'=>'off']) . $label . '</label>';
						}
					]
				)
				->label(NULL, ['class'=>$labelClass]);
				?>
			</div>	
			<div class="col-md-6">



				<?php
						echo $form->field(
						$model,
						'marital',
						[
							'options' => ['class' => $formGroupClass],
							'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="nutritionmeal-mealtype">{input} </div> {hint} {error}</div>'
						]
					)
					->radioList(
						User::itemAlias('Marital'),
						[
							'tag' => false,
							'unselect' => NULL,
							'item' => function($index, $label, $name, $checked, $value) {
								return '<label class="btn btn-warning '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value, 'autocomplete'=>'off']) . $label . '</label>';
							}
						]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?=$form->field($model, 'user_picture')->widget(FileInput::classname(), [
				    'options'=>['accept'=>'image/*'],
				    'pluginOptions'=>[
				    	'allowedFileExtensions'=>['jpg','gif','png'],
				    	'showPreview' => false,
					    'showCaption' => true,
					    'showRemove' => true,
					    'showUpload' => false,
				]])->label(NULL, ['class'=>$labelClass]);; 
				?>
			</div>
			<div class="col-md-6">
				<?=$form->field($model, 'birth_certificate_picture_face')->widget(FileInput::classname(), [
				    'options'=>['accept'=>'image/*'],
				    'pluginOptions'=>[
				    	'allowedFileExtensions'=>['jpg','gif','png'],
				    	'showPreview' => false,
					    'showCaption' => true,
					    'showRemove' => true,
					    'showUpload' => false,
				]])->label(NULL, ['class'=>$labelClass]);; 
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?=$form->field($model, 'birth_certificate_picture_back')->widget(FileInput::classname(), [
				    'options'=>['accept'=>'image/*'],
				    'pluginOptions'=>[
				    	'allowedFileExtensions'=>['jpg','gif','png'],
				    	'showPreview' => false,
					    'showCaption' => true,
					    'showRemove' => true,
					    'showUpload' => false,
				]])->label(NULL, ['class'=>$labelClass]);; 
				?>
			</div>
			<div class="col-md-6">
				<?=$form->field($model, 'national_card_pciture')->widget(FileInput::classname(), [
				    'options'=>['accept'=>'image/*'],
				    'pluginOptions'=>[
				    	'allowedFileExtensions'=>['jpg','gif','png'],
				    	'showPreview' => false,
					    'showCaption' => true,
					    'showRemove' => true,
					    'showUpload' => false,
				]])->label(NULL, ['class'=>$labelClass]);; 
				?>
			</div>
		</div>

		<div class="row">
            <div class="margiv-top-10">
                <?= Html::submitButton(Yii::t('app', 'ارسال'), ['name' => 'update_pic_btn', 'class' => 'btn green']) ?>
                <?= Html::submitButton(Yii::t('app', 'Cancel'), ['class' => 'btn default']) ?>
            </div>
        </div>

	</div>
		 

   
 
</div>