<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = Yii::t('app', 'Update')." ". $model->name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Students'), 'url' => ['students']];
$this->params['breadcrumbs'][] = ['label' => htmlspecialchars($model->username), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('zii', 'Update');
?>
<div class="person-update">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
					<?= $this->render('_form', [
						'model' => $model,
						'modeld' => $modeld,
					]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
