<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Studentss'), 'url' => ['person/students']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Update'),
		'url' => ['members/profileview', 'id' => $model->id]
	],

];

?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="person-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-5">
				<div class="portlet light ">
					<div>
						<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
										 $user=$model->username;
										 $path=Yii::$app->request->BaseUrl.'/../uploads/'. $user.'_image.jpg';
										echo Html::img($path);
									 }
									 ?>			 
									</div>
						<h4 class="profile-desc-title center"> <?= Html::encode($this->title) ?> </h4>
						 

						 
							
					 
						
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="portlet light ">
					<div>
						 
						 
							<?= DetailView::widget([
							'model' => $model,
							'template' => Yii::$app->params['detalView'],
							'attributes' => [
								'name',
								'last_name',
								'student_number',
								'universityName',
								//'startingyear',
								//'startingsemester',
								
								'nationalId',
								//'password',
								'idNo',
							],
						]) ?>
						 

						 
							 
						 
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>