<?php

use yii\helpers\Html;
use backend\assets\UserProfile;
use yii\widgets\ActiveForm;
UserProfile::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = Yii::t('app', 'Create Person');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="theme-panel hidden-xs hidden-sm"></div>
 
					 
	<div class="row">
		<div class="col-md-12">
			


			 
							 
			<div class="profile-content">
				<div class="row">
					<div class="col-md-12">
										
						<div id="results_expand_msg" style="display: none " ></div>										
										
										
							<div class="portlet light ">
								
								<div class="portlet-title tabbable-line">
									
									<div class="caption caption-md">
										<i class="icon-globe theme-font hide"></i>										
										<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'Profile Account')?></span>
									</div>

									 
								</div>

								<div class="portlet-body">
									 
									 
											
											<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>
												<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>
												<?=$this->render('_update_person_personal_info', [
													'model' 	=> $model,
													'message' 	=> $message,
													'form'		=> $form
													])
												?>													   
											 
												 
											<?php ActiveForm::end(); ?>	  
																							
									  
										 
									 
								</div>
							</div>
					</div>
				</div>
			</div>
							 
		</div>
	</div>
