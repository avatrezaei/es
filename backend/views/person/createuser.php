<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\University;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];


$unis = ArrayHelper::map(University::find()->all(), 'id', 'name'); 



$this->title = Yii::t('app', 'Create User');
$this->params['page_title'] = false;
$this->params['breadcrumbs'][] = $this->title;
 
?>

<div class="theme-panel hidden-xs hidden-sm"></div>
 
					 
	<div class="row">
		<div class="col-md-12">
			<?= $message ?>


			 
							 
			<div class="profile-content">
				<div class="row">
					<div class="col-md-12">
										
						<div id="results_expand_msg" style="display: none " ></div>										
										
										
							<div class="portlet light ">
								
								<div class="portlet-title tabbable-line">
									
									<div class="caption caption-md">
										<i class="icon-globe theme-font hide"></i>										
										<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'Profile Account')?></span>
									</div>

									 
								</div>

								<div class="portlet-body">
									 
									 
											
											<div class="person-form">

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">
		<div class="row">
			<div class="col-md-6">
				<?php
					echo $form->field(
						$model,
						'name',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => true, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>

			<div class="col-md-6">
						<?php
							echo $form->field(
								$model,
								'last_name',
								[
									'options' => ['class' => $formGroupClass],
									'template' => $template
								]
							)
							->textInput(
								['maxlength' => true, 'class' => $inputClass]
							)
							->label(NULL, ['class'=>$labelClass]);
						?>
			</div>

		</div>

 


		<div class="row">
			<div class="col-md-6">
						<?php
							echo $form->field(
								$model,
								'nationalId',
								[
									'options' => ['class' => $formGroupClass],
									'template' => $template
								]
							)
							->textInput(
								['maxlength' => true, 'class' => $inputClass]
							)
							->label(NULL, ['class'=>$labelClass]);
						?>

			</div>
			<div class="col-md-6">
				<?php
					echo $form->field(
						$model,
						'email',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textInput(
						['maxlength' => true, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<?php
						echo $form->field(
							$model,
							'username',
							[
								'options' => ['class' => $formGroupClass],
								'template' => $template
							]
						)
						->textInput(
							['class' => $inputClass]
						)
						->label(NULL, ['class'=>$labelClass]);
					?>
			</div>
			<div class="col-md-6">
                <?php
                $roles = [
                    "admin"		=>	Yii::t('app',"admin"),
                    "supervisor"=>	Yii::t('app',"supervisor"),
                ];
                echo $form->field(
                    $model,
                    'roles',
                    [
                        'options' => ['class' => $formGroupClass],
                        'template' => $template
                    ]
                )
                    ->widget(Select2::classname(), [
                        'data' => $roles,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Please Select a role'),
                            'dir' => 'rtl',
                        ],
                    ])
                    ->label(NULL, ['class'=>$labelClass]);


                ?>
            </div>
		</div>
		<div class="row">
			<div class="col-md-6">
                <?php
                echo $form->field(
                    $model,
                    'password',
                    [
                        'options' => ['class' => $formGroupClass],
                        'template' => $template
                    ]
                )
                    ->passwordInput(
                        ['class' => $inputClass]
                    )
                    ->label(NULL, ['class'=>$labelClass]);
                ?>

			</div>
            <div class="col-md-6">
                <?php
                echo $form->field(
                    $model,
                    'repeatpassword',
                    [
                        'options' => ['class' => $formGroupClass],
                        'template' => $template
                    ]
                )
                    ->passwordInput(
                        ['class' => $inputClass]
                    )
                    ->label(NULL, ['class'=>$labelClass]);
                ?>
            </div>
		</div>




		 

		 

 

	</div>

	<div class="form-actions">
		<div class="row">
			<div class=" col-md-9">
				<?= Html::submitButton( Yii::t('app', 'Create') , ['class' =>  'btn blue']) ?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>
 
																							
									  
										 
									 
								</div>
							</div>
					</div>
				</div>
			</div>
							 
		</div>
	</div>
<!--  -->

