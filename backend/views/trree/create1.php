<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Trree */
$ss=$model->room_id;
$id=$_GET['id'];
$this->title = 'تخصیص اتاق '.$model->getAddress($id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roomsa'), 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
?>




<div class="familyform-create">
    <div class="row">
        <div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
                    </div>
                </div>
                <div class="portlet-body form">
                <?= $this->render('_form3', [
                    'model' => $model,
                    //'placeList'=>$placeList,
                ]) ?>

            </div>
<br><br><br>
                <h4>ساکنین اتاق</h4>
            <?php

            $query = \app\models\Eskan::find();

            // add conditions that should always apply here

            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $query,
            ]);

            // grid filtering conditions
            $query->andFilterWhere([
                'room_id' => $model->room_id,
                //'exit_date'=>'0000-00-00',
            ]);
            $searchModel = new \app\models\EskanSearch();


            ?>
            <?= \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [

                        'header' => 'دانشجو',
                        'enableSorting' => false,
                        'value' => function ($model) {
                            return $model->user->name.' '.$model->user->last_name;
                        },
                    ],
                    [

                        'header' => 'شماره دانشجو',
                        'enableSorting' => false,
                        'value' => function ($model) {
                            return $model->user->student_number;
                        },
                    ],
                    [

                        'header' => 'تاریخ ورود',
                        'enableSorting' => false,
                        'value' => function ($model) {
                            return $model->entry_date;
                        },
                    ],
                    [

                        'header' => 'تاریخ خروج',
                        'enableSorting' => false,
                        'value' => function ($model) {
                            return $model->exit_date;
                        },
                    ],
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{deleteeskan}',
                        'buttons' => [

                            'deleteeskan' => function ($url, $model){

                                $_send = Html::a(
                                    '<i class="fa fa-trash font-blue"></i>',
                                    \yii\helpers\Url::to(['/eskan/delete', 'id' => $model->id]),
                                    [
                                        'class' => 'hint--top hint--rounded hint--info',
                                        'data-hint' => Yii::t('app', 'حذف اسکان تخصیص یافته به دانشجو'),
                                    ]
                                );

                                return $_send;
                            },


                        ],
                    ],


                ],
            ]); ?>
            </div>
        </div>
    </div>
</div>
