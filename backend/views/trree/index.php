<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Trree;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Trree */
/* @var $form yii\widgets\ActiveForm */


$this->title = 'tree structure';
$this->params['breadcrumbs'][] = $this->title;
use kartik\popover\PopoverX;
backend\assets\AppAsset::register($this);
















//////////////////////////////
$array=Trree::find()->all();
//die(var_dump($array));
$map = array();
/*foreach ($array as $node) {
    // init self
    if (!array_key_exists($node['id'], $map)) {
        $map[$node['id']] = array('d' => $node['name']);
    }
    else {
        $map[$node['id']]['self'] = $node['name'];
    }

    // init parent
    if (!array_key_exists($node['parent_id'], $map)) {
        $map[$node['parent_id']] = array();
    }

    // add to parent
    $map[$node['parent_id']][$node['id']] = & $map[$node['id']];
}

//print_r($map[0]);
*/
//////////////////////
$arrayCategories = array();

foreach ($array as $row){
    $arrayCategories[$row['id']] = array("parent_id" => $row['parent_id'],"id" => $row['id'], "name" =>
        $row['name'], "type"=>$row['type'], "section"=>$row['section'], "gender"=>$row['gender']);
}
?>
<div id="content" class="general-style1">

    <?php

    createTreeView($arrayCategories, 0); ?>
</div>
<?php


function createTreeView($array, $currentParent, $currLevel = 0, $prevLevel = -1) {

    foreach ($array as $categoryId => $category) {

        if ($currentParent == $category['parent_id']) {
            if ($currLevel > $prevLevel) echo " <ol class='tree'> ";

            if ($currLevel == $prevLevel) echo " </li> ";


            ///////////namayeshe name makan
            if ($category['type']==0) {
                echo '<li> <label for="subfolder2">'.$category['name'].'</label> <input type="checkbox" name="subfolder2"/>';
            }
            else{
                $content= $category['name'];
                $form = ActiveForm::begin();
                $model= new common\models\User();
                $Place = \common\models\User::find()->all();
                $listData=ArrayHelper::map($Place,'id','name');
                //echo $form->errorSummary($model, ['class' => 'alert alert-danger']);
//die(var_dump($model->namejj));
//$model->password = null;
                PopoverX::begin([
                    'placement' => PopoverX::ALIGN_RIGHT,
                    'toggleButton' => ['label'=>$content, 'class'=>'btn btn-default'],
                    'header' => '<i class="glyphicon glyphicon-lock"></i> تخصیص اتاق به دانشجو',
                    'footer'=> Html::submitButton($model->isNewRecord ? 'ایجاد' : 'بروزرسانی', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value'=>'Create', 'name'=>'submit']),
                ]);
                // echo $form->field($model, 'name')->textInput(['placeholder'=>'Enter node name']);
                echo $form->field($model, 'name')->dropDownList($listData,['prompt'=>'نوع گره مورد نظر را انتخاب نمایید', 'id'=>'place_id']);
                PopoverX::end();
                ActiveForm::end();

            }
           if ($category['type']==0) {
echo Html::a(1, ['trree/create','parent_id'=>$category['id']]);
//                echo $this->render('_form1', [
//                    'model' => $model,
//                    'id'=>$category['id'],
//                ]);
           }
            if ($category['type']==0) {
                $content=' E';
                $form = ActiveForm::begin();
                $model= new Trree;
                //echo $form->errorSummary($model, ['class' => 'alert alert-danger']);
//die(var_dump($model->namejj));
//$model->password = null;
                PopoverX::begin([
                    'placement' => PopoverX::ALIGN_RIGHT,
                    'toggleButton' => ['label'=>$content, 'class'=>'btn btn-default'],
                    'header' => '<i class="glyphicon glyphicon-lock"></i> اصلاح گره مورد نظر',
                    'footer'=>Html::submitButton('اصلاح', ['class'=>'btn btn-sm btn-primary']),
                ]);
                echo $form->field($model, 'name')->textInput(['placeholder'=>'Enter node name']);
                echo $form->field($model, 'section')->dropDownList([1=>'کارشناسی',2=>'ارشد', 3=>'دکترا'],['prompt'=>'مقطع تحصیلی مورد نظر را انتخاب نمایید']);
                echo $form->field($model, 'gender')->dropDownList([1=>'مرد',2=>'زن'],['prompt'=>'جنسیت مکان مورد نظر را انتخاب نمایید']);
                echo $form->field($model, 'type')->dropDownList([1=>'خوابگاه',2=>'اتاق'],['prompt'=>'نوع گره مورد نظر را انتخاب نمایید']);
                PopoverX::end();
                ActiveForm::end();
            }
            if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }

            $currLevel++;

            createTreeView ($array, $categoryId, $currLevel, $prevLevel);

            $currLevel--;
        }

    }

    if ($currLevel == $prevLevel) echo " </li>  </ol> ";


}

?>


