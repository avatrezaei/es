<?php

use yii\helpers\Html;
use app\models\Trree;


/* @var $this yii\web\View */
/* @var $model app\models\Trree */

$this->title = Yii::t('app', 'view State');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roomsa'), 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
?>




<div class=""fields-create>
    <div class=" col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green">
                    <span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php
                //die(var_dump($room));
                $result = array();
                if($room)
                {
                    if($room=='x')
                        echo 'اتاق تخصیص یافته به شما هنوز تحویل داده نشده است!';
                    else{
                $mm=Trree::find()->andWhere(['id'=>$room])->one();
                $i=1;
                while ($mm->parent_id!=0)
                {
                $result[$i]= $mm->name;
                //echo $result[$i];
                $mm=Trree::find()->andWhere(['id'=>$mm->parent_id])->one();
                $i=$i+1;
                }
                //die(var_dump($i));
                for($j=$i-1;$j>=1;$j--)
                {
                    echo $result[$j];
                    if($j!=1)
                        echo '-';
                }
                    }
                }
                else
                   echo 'اتاقی به شما تخصیص داده نشده است';

                 ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>
