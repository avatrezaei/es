<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
/* @var $this yii\web\View */
/* @var $model app\models\Trree */
/* @var $form yii\widgets\ActiveForm */
$floor = User::find()->andWhere(['type'=>0])->all();
$listData=ArrayHelper::map($floor,'id',function($model, $defaultValue) {
    //$caravan=Place::find()->andWhere(['id'=>$model['parent_id']])->one();
    return $model['name'].' '.$model['last_name'].'-'.$model['student_number'];
});
$form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]);
//z echo $form->field($model,'name')->textInput(['placeholder'=>'Enter node name']);
//$model->parent_id=$_GET['parent_id'];
//echo $form->field($model, 'parent_id')->hiddenInput();
$model->room_id=$_GET['id'];
 //$form->field($model, 'room_id')->hiddenInput();
$model->room_id=$_GET['id'];
 $form->field(
    $model,
    'room_id',
    [
        'options' => ['class' => $formGroupClass,'disable'=>true],
        'template' => $template
    ]
)
    ->textInput(
        ['maxlength' => 255, 'class' => $inputClass]
    )
    ->label(NULL, ['class'=>$labelClass]);
echo $form->field(
    $model,
    'year',
    [
        'options' => ['class' => $formGroupClass],
        'template' => $template
    ]
)
    ->textInput(
        ['maxlength' => 255, 'class' => $inputClass]
    )
    ->label(NULL, ['class'=>$labelClass]);
echo $form->field(
    $model,
    'term',
    [
        'options' => ['class' => $formGroupClass],
        'template' => $template
    ]
)
    ->textInput(
        ['maxlength' => 255, 'class' => $inputClass]
    )
    ->label(NULL, ['class'=>$labelClass]);


//
//$model->entry_date =$model->dateDisplayFormat();
//
//echo $form->field ( $model, 'entry_date',
//    [ 'options' => [ 'class' => $formGroupClass,
//        //'value' => $model->start,
//    ],
//        'template' => $template
//    ]
//)->widget ( jalaliDatePicker::className (), [ 'options' => ['format' => 'yyyy/mm/dd','viewformat' => 'yyyy/mm/dd','placement' => 'right','todayBtn' => 'linked','id' => 'event-entryDate','class' => 'form-control'  ] ] )->label ( NULL, [ 'class' => $labelClass ] );





echo $form->field($model, 'PersonID',[
    'options' => ['class' => $formGroupClass],
    'template' => $template
])
->widget(Select2::classname(), [
    'data' => $listData,
    'language' => 'fa-IR',
    'options' => ['placeholder' => 'دانشجویان مورد نظر را انتخاب نمایید','multiple' => true],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label(NULL, ['class'=>$labelClass]);




//echo $form->field($model, 'PersonID',[
//    'options' => ['class' => $formGroupClass],
//    'template' => $template
//])->dropDownList($listData,['prompt'=>'نام دانشجوی مورد نظر را انتخاب نمایید', 'id'=>'parent_id','multiple' => true])->label(NULL, ['class'=>$labelClass]);
//echo $form->field($model, 'PersonID')->dropDownList($listData,['prompt'=>'نام دانشجوی مورد نظر را انتخاب نمایید', 'id'=>'parent_id']);
//echo $form->field($model, 'section')->dropDownList([1=>'کارشناسی',2=>'ارشد', 3=>'دکترا'],['prompt'=>'مقطع تحصیلی مورد نظر را انتخاب نمایید']);
//echo $form->field($model, 'gender')->dropDownList([1=>'مرد',2=>'زن'],['prompt'=>'جنسیت مکان مورد نظر را انتخاب نمایید']);
//echo $form->field($model, 'type')->dropDownList([1=>'خوابگاه',2=>'اتاق'],['prompt'=>'نوع گره مورد نظر را انتخاب نمایید']);
//echo $form->field($model, 'enable')->dropDownList(['غیرفعال','فعال'],['prompt'=>'فعال بودن یا نبودن گره را تعیین نمایید']);
//echo '<div>';
echo Html::submitButton($model->isNewRecord ? 'تخصیص' : 'بروزرسانی', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
ActiveForm::end();






