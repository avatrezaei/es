<html>
<body style="text-align: right ">
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Trree;
use yii\helpers\ArrayHelper;
use backend\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Trree */
/* @var $form yii\widgets\ActiveForm */
//require_once('pdo_db.php');
//$database= new core_db;
//$hasil=$database->query('SELECT id as menu_item_id, parent_id as menu_parent_id, title as menu_item_name,concat("/foldername",url)as url,menu_order,icon FROM menu ORDER BY menu_order');
$hasil=Trree::find()->all();
$refs = array();
$list = array();

foreach($hasil as $data)
{
    $thisref = &$refs[ $data['id'] ];
    $thisref['parent_id'] = $data['parent_id'];
    $thisref['name'] = $data['name'];
    $thisref['url'] = $data['url'];
    $thisref['icon'] = $data['icon'];
    $thisref['type'] = $data['type'];
    if ($data['parent_id'] == 0)
    {
        $list[ $data['id'] ] = &$thisref;
    }
    else
    {
        $refs[ $data['parent_id'] ]['children'][ $data['id'] ] = &$thisref;
    }
}
function create_list( $arr ,$urutan)
{
    if($urutan==0){
        $html = "\n<ul class='sidebar-menu'>\n";
    }else
    {
        $html = "\n<ul class='treeview-menu'>\n";
    }
    foreach ($arr as $key=>$v)
    {
        if (array_key_exists('children', $v))
        {
            $result=Trree::find()->andWhere(['parent_id'=>$key])->one();
            //foreach
           // echo"<li>";
            $html .="<li>". $v['name']."<x id='$key' style='display: block'><a onclick=\"toggleContent('$key')\">t</a>";
            if($v['type']==1){
                $html .= '
                                
                                <a href="index.php?r=trree%2Fcreate&parent_id='.$key.'"> +'.'</a>
                                <a href="index.php?r=trree%2Fupdate&id='.$key.'"> *';
            }
            else{
                $html .= '
                                <a href="index.php?r=trree%2Fupdate&id='.$key.'"> *'.'</a>
                            ';
            }


            $html .= create_list($v['children'],1);
            $html .= "</x></li>";
            //echo"";
        }
        //http://localhost/advanced2/backend/web/trree/create
        else{
           // $html .= '<li><a href="'.'index.php?r=trree%2Fcreate&parent_id='.$key.'">';
            $html .= "<li id='$key' style='display: block'>";
            if($urutan==0)
            {
                $html .=    '<i class="'.$v['icon'].'"></i>';
            }
            if($urutan==1)
            {
                $html .=    '<i class="fa fa-angle-double-right"></i>';
            }
            if($v['type']==1){

                $html .= $v['name'];
                $html .= '<a href="'.'index.php?r=trree%2Fcreate&parent_id='.$key.'">';
                $html .= '+'."</a>".'<a href="'.'index.php?r=trree%2Fupdate&id='.$key.'">'.'*';
                $html .="</a></li>";

            }
            else{
                $html .= $v['name'];
                $html .= '<a href="'.'index.php?r=trree%2Fupdate&id='.$key.'">'.'*';
                $html .="</a></li>";

            }
            }
    }
    $html .= "</ul>\n";
    return $html;
}
echo create_list( $list,0 );
?>
</div>
</body>
</html>
<?php
echo "

<script type=\"text/javascript\">
function toggleContent(Item) {
  // Get the DOM reference
  var e = document.getElementById(Item)
        if(e.style.display=='block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
//e.style.display = 'block'; 
}
</script>";
?>


