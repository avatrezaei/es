<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Trree;
use yii\helpers\ArrayHelper;
use backend\assets\TreeAsset;

TreeAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Trree */
/* @var $form yii\widgets\ActiveForm */
//require_once('pdo_db.php');
//$database= new core_db;
//$hasil=$database->query('SELECT id as menu_item_id, parent_id as menu_parent_id, title as menu_item_name,concat("/foldername",url)as url,menu_order,icon FROM menu ORDER BY menu_order');
//$hasil=Trree::find()->all();
//$refs = array();
//$list = array();
//"plugins":["contextmenu"],

$url = Url::to(['/trree/getchild']);
$this->registerJs(
<<<SCRIPT
$('.showtree').jstree({

  "core" : {
    "animation" : 0,
    "check_callback" : false,
    "themes" : { "stripes" : true },
    'data' : {
      'url' : function (node) {
        return '$url';
      },
      'data' : function (node) {
        return { 'parent' : node.id };
      }
    }
  },
});    	
SCRIPT
    , \yii\web\View::POS_END);

?>

<div class="row" style="right">
    <div class="col-md-12" style="right">
        <div class="showtree" style="right"></div>
    </div>
</div>
