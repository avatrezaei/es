<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Floor;
use backend\models\Place;
/* @var $this yii\web\View */
/* @var $model backend\models\Suite */
/* @var $form yii\widgets\ActiveForm */
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];


$floor = Floor::find()->all();
$listData=ArrayHelper::map($floor,'id',function($model, $defaultValue) {
	$caravan=Place::find()->andWhere(['id'=>$model['parent_id']])->one();
        return $model['name'].'-'.$caravan['name'];
    });
?>

<div class="suite-form">

    <?php $form = ActiveForm::begin(); ?>

   <?php
				echo $form->field(
					$model,
					'id',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?= $form->field($model, 'parent_id',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList($listData,['prompt'=>'نام طبقه مورد نظر را انتخاب نمائید', 'id'=>'parent_id'])->label(NULL, ['class'=>$labelClass]); ?>

    <?php
				echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?php
				echo $form->field(
					$model,
					'capacity',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'ایجاد' : 'بروزرسانی', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
