<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoomsaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Roomsas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="roomsa-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Roomsa'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'RoomID',
            'capacity',
            'area',
            'address',
            'sex',
            // 'place',
            // 'subset',
            // 'floor',
            // 'suite',
            // 'room',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
