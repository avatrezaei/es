<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use kartik\select2\Select2;
//use kartik\widgets\Select2;

use kartik\select2\Select2Asset;
use backend\models\Roomsa;
use backend\models\Rom;
use backend\models\Caravan;
use backend\models\Place;
use backend\modules\YumUsers\models\UsersType;
use yii\helpers\Url;
use ibrarturi\latlngfinder\LatLngFinder;
use backend\models\FieldsType;
use yii\web\JsExpression;
use yii\web\View;
use kartik\file\FileInput;
use backend\models\SportLocation;
use yii\base\Widget;
use backend\models\Fields;
use backend\models\SportLocationField;
 
 
 
 
/* @var $this yii\web\View */
/* @var $model backend\models\Event */
/* @var $form yii\widgets\ActiveForm */


/**
 * *select2 uses***
 */

use backend\models\University;
use backend\models\Event;
use backend\models\Eventtype;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];







Select2Asset::register($this);
$caravan = Caravan::find()->all();
$listData=ArrayHelper::map($caravan,'id','name');
$place = Place::find()->all();
$listData2=ArrayHelper::map($place,'id','name');
$role = UsersType::find()->all();
$listData1=ArrayHelper::map($role,'id','title');
         

?>

 



    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true, 'class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>
	<div class="form-body">
	   <?= $form->field($model, 'caravan_id',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList($listData,['prompt'=>'کاروان مورد نظر را انتخاب نمایید', 'id'=>'caravan_id'])->label(NULL, ['class'=>$labelClass]); ?>


<?= $form->field($model, 'role',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList($listData1,['prompt'=>'سمت مورد نظر را انتخاب نمایید', 'id'=>'role_id'])->label(NULL, ['class'=>$labelClass]); ?>


  <?= $form->field($model, 'place_id',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList($listData2,['prompt'=>'مکان مورد نظر را انتخاب نمایید', 'id'=>'place_id'])->label(NULL, ['class'=>$labelClass]); ?>

 



	  <!--  <?= $form->field($model, 'place_id',
		   [
			   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->widget(DepDrop::classname(), [
		   'options'=>['id'=>'place_id'],
		   'pluginOptions'=>[
			   'depends'=>['caravan_id'],
			   'placeholder'=>'مکان مورد نظر را انتخاب نمایید',
			   'url'=>Url::to(['/roomsa/place'])
		   ]
	   ])->label(NULL, ['class'=>$labelClass]);?> -->


	<?= $form->field($model, 'subset_id',[
		   'options' => ['class' => $formGroupClass],
		   'template' => $template
	   ])->widget(DepDrop::classname(), [
    'options'=>['id'=>'subset_id'],
    'pluginOptions'=>[
        'depends'=>['place_id'],
        'placeholder'=>'زیرمجموعه موردنظر را انتخاب نمایید',
        'url'=>Url::to(['/roomsa/subset'])
    ]
	])->label(NULL, ['class'=>$labelClass]);?>


	<?= $form->field($model, 'floor_id',[
		   'options' => ['class' => $formGroupClass],
		   'template' => $template
	   ])->widget(DepDrop::classname(), [
    'options'=>['id'=>'floor_id'],
    'pluginOptions'=>[
        'depends'=>['place_id','subset_id'],
        'placeholder'=>'طبقه مورد نظر را انتخاب نمایید',
        'url'=>Url::to(['/roomsa/floor'])
    ]
	])->label(NULL, ['class'=>$labelClass]);?>
	<?= $form->field($model, 'suite_id',[
		   'options' => ['class' => $formGroupClass],
		   'template' => $template
	   ])->widget(DepDrop::classname(), [
    'options'=>['id'=>'suite_id', 'multiple' => true],
    'pluginOptions'=>[
        'depends'=>['floor_id'],
        'placeholder'=>'سوئیت مورد نظر را انتخاب نمایید',
        'url'=>Url::to(['/roomsa/suite'])
    ]
	])->label(NULL, ['class'=>$labelClass]);?>

	
	 	<?= $form->field($model, 'room_id',[
		   'options' => ['class' => $formGroupClass],
		   'template' => $template
	   ])->widget(DepDrop::classname(), [
    'options'=>['id'=>'room_id', 'multiple' => true],
    'pluginOptions'=>[
        'depends'=>['floor_id','suite_id'],
        'placeholder'=>'اتاق مورد نظر را انتخاب نمایید',
        'url'=>Url::to(['/roomsa/room'])
    ]
	])->label(NULL, ['class'=>$labelClass]);?>









	</div>
	
	
	
	
	
    
	
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>



