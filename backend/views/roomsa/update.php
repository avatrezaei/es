<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Roomsa */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Roomsa',
]) . ' ' . $model->RoomID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roomsas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->RoomID, 'url' => ['view', 'id' => $model->RoomID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="roomsa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'placeList'=>$placeList,
    ]) ?>

</div>
