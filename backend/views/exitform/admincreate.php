<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Exitform */

$this->title = Yii::t('app', 'Exit form management');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exitforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
    [
        'label' => '<i class="ace-icon fa bigger-120 "></i> ' . Yii::t('app', 'Exit Form Lists'),
        'url' => 'index'
    ]
];
?>
<div class="exitform-create">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
                    <?php Pjax::begin(); ?>


                    <?= $this->render('_admincreateform', [
                        'model' => $model,
                    ]) ?>
				</div>
                <?php
                if(isset($_GET['user_id'])) {
                    echo '<script>document.getElementById("users_id").value=' . (int)$_GET['user_id'] . ';</script>';
                    $model = new \app\models\Exitform();
                    $newrow = Yii::$app->request->post('Exitform');
                    // die(var_dump($newrow));
                    $query = \app\models\Exitform::find();
                    // add conditions that should always apply here

                    $dataProvider = new \yii\data\ActiveDataProvider([
                        'query' => $query,
                    ]);
                    // grid filtering conditions
                    $query->andFilterWhere([
                        'status' => 0,'user_id' => $_GET['user_id'],

                        //'exit_date'=>'0000-00-00',
                    ]);
                    $searchModel = new \app\models\ExitformSearch();


                    echo \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [

                                'header' => Yii::t('app', 'Students'),
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->users->name . ' ' . $model->users->last_name . '-' . $model->users->student_number;
                                },
                            ],
                            [

                                'header' => 'نسبت',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->user->relation;
                                },
                            ],
                            [

                                'header' => Yii::t('app', 'Userrel ID'),
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->user->FName . ' ' . $model->user->LName . '-' . $model->user->address;
                                },
                            ],
                            [

                                'header' => 'نسبت',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->user->relation;
                                },
                            ],
                            [

                                'header' => 'تاریخ خروج',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->from_date;
                                },
                            ],
                            [

                                'header' => 'تاریخ ورود',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->to_date;
                                },
                            ],

                            [

                                'header' => 'توضیحات',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->description;
                                },
                            ],//
                            [
                                'header' => 'عملیات',
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view}{updateeskan}',
                                'buttons' => [
                                    'updateeskan' => function ($url, $model) {
                                        $_send1 = Html::a(
                                            '<i class="fa fa-pencil font-blue"></i>',
                                            \yii\helpers\Url::to(['/exitform/adminupdate', 'id' => $model->id]),
                                            [
                                                'class' => 'hint--top hint--rounded hint--info',
                                                'data-hint' => Yii::t('app', 'بروزرسانی رکورد'),
                                            ]
                                        );

                                        return $_send1;
                                    },


                                ],
                            ],


                        ],
                    ]);
                }?>
                <?php Pjax::end(); ?>

            </div>
		</div>
	</div>
</div>
