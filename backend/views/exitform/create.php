<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Exitform */

$this->title = Yii::t('app', 'Create Exitform');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exitforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exitform-create">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
					<?= $this->render('_form', [
						'model' => $model,
					]) ?>
				</div>
                <?php

                $query = \app\models\Exitform::find();
                // add conditions that should always apply here

                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $query,
                ]);
                // grid filtering conditions
                $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
                $query->andFilterWhere([
                    'user_id' => $curentUserId,
                    //'exit_date'=>'0000-00-00',
                ]);
                $searchModel = new \app\models\ExitformSearch();


                ?>
                <h4 style="color:#0000C0 "></h4>
                <?= \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [

                            'header' => Yii::t('app', 'Userrel ID'),
                            'enableSorting' => false,
                            'value'=> function ($model) {
                    //die(var_dump($model));
                                return $model->user->FName.' '.$model->user->LName.'-'.$model->user->address;
                                return '1';
                            },
                        ],
                        [

                            'header' => 'نسبت',
                            'enableSorting' => false,
                            'value'=> function ($model) {
                               return $model->user->relation;
                                //return '1';
                            },
                        ],
                        [

                            'header' => 'تاریخ خروج',
                            'enableSorting' => false,
                           'value'=> function ($model) {
                                return $model->from_date;
                            },
                        ],
                        [

                            'header' => 'تاریخ ورود',
                            'enableSorting' => false,
                            'value'=> function ($model) {
                                return $model->to_date;
                            },
                        ],

                        [

                            'header' => 'توضیحات',
                            'enableSorting' => false,
                            'value'=> function ($model) {
                                return $model->description;
                            },
                        ],//
                        [
                            'header' => 'عملیات',
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{deleteeskan}{updateeskan}',
                            'buttons' => [

                                'deleteeskan' => function ($url, $model){

                                    $_send = Html::a(
                                        '<i class="fa fa-trash font-blue"></i>',
                                        \yii\helpers\Url::to(['/exitform/delete', 'id' => $model->id]),
                                        [
                                            'class' => 'hint--top hint--rounded hint--info',
                                            'data-hint' => Yii::t('app', 'حذف رکورد'),
                                        ]
                                    );

                                    return $_send;
                                },

                                'updateeskan' => function ($url, $model){
                                    $_send1 = Html::a(
                                        '<i class="fa fa-pencil font-blue"></i>',
                                        \yii\helpers\Url::to(['/exitform/update', 'id' => $model->id]),
                                        [
                                            'class' => 'hint--top hint--rounded hint--info',
                                            'data-hint' => Yii::t('app', 'بروزرسانی رکورد'),
                                        ]
                                    );

                                    return  $_send1;
                                },


                            ],
                        ],


                    ],
                ]); ?>

            </div>
		</div>
	</div>
</div>
