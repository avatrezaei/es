<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Exitform */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params['formGroupClass'];
//$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$errorClass = Yii::$app->params['errorClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$labelClass = 'col-md-3';
$dateTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-calendar"></i>{input}</div> {hint} {error}</div>';

?>

<div class="exitform-form">

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">
        <?php
        $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

        $floor = \app\models\Familyform::find()->andWhere(['user_id'=>$curentUserId])->all();
        $listData=ArrayHelper::map($floor,'id',function($model, $defaultValue) {
            //$caravan=Place::find()->andWhere(['id'=>$model['parent_id']])->one();
            return $model['FName'].' '.$model['LName'].'-'.$model['address'];
        });
        ?>
        <input id="exitform-user_id" class="form-control input-sm" name="Exitform[user_id]" maxlength="20" type="hidden" value="<?=$curentUserId;?>">
        <div class="row">
            <div class="col-md-6 col-sm-1 col-xs-5">
                <?php
                echo $form->field($model, 'userrel_id',[
                    'options' => ['class' => $formGroupClass],
                    'template' => $template
                ])
                    ->widget(Select2::classname(), [
                        'data' => $listData,
                        'language' => 'fa-IR',
                        'options' => ['placeholder' => 'یکی از وابستگان را انتخاب نمائید'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(NULL, ['class'=>$labelClass]);
                ?>
            </div></div>
        <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    $model->from_date =$model->dateDisplayFormat();

                    echo $form->field(
                        $model,
                        'from_date',
                        [
                            'options' => ['class' => $formGroupClass],
                            'template' => $dateTemplate
                        ]
                    )->widget ( jalaliDatePicker::className (), [ 'options' =>
                        [
                            'format' => 'yyyy/mm/dd',
                            'viewformat' => 'yyyy/mm/dd',
                            'placement' => 'right',
                            'todayBtn' => 'linked',
                            'id' => 'event-fromdate',
                            'class' => 'form-control',
                            'autoclose'=>true,
                        ] ] )
                        ->label ( NULL, [ 'class' => $labelClass ] );
                    ?>
                </div>
            <div class="col-md-6 col-sm-6 col-xs-12">

                <?php
                $model->to_date =$model->dateDisplayFormat();

                echo $form->field(
                    $model,
                    'to_date',
                    [
                        'options' => ['class' => $formGroupClass],
                        'template' => $dateTemplate
                    ]
                )->widget ( jalaliDatePicker::className (), [ 'options' => ['format' => 'yyyy/mm/dd','viewformat' => 'yyyy/mm/dd','placement' => 'right','todayBtn' => 'linked','id' => 'event-todate','class' => 'form-control','autoclose'=>true,] ] )->label ( NULL, [ 'class' => $labelClass ] );
		?>
            </div></div>
        <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">

		<?php
			echo $form->field(
				$model,
				'description',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textarea(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
                </div></div>

	</div>

            <div class="margiv-top-10">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
			</div>
	<?php ActiveForm::end(); ?>

</div>
