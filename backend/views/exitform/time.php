<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Exitform */
/* @var $form yii\widgets\ActiveForm */
use yii\widgets\Pjax;

$formGroupClass = Yii::$app->params['formGroupClass'];
$inputClass = Yii::$app->params['inputClass'];
$errorClass = Yii::$app->params['errorClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$labelClass = 'col-md-3';

?>

<div class="exitform-form">
    <?php Pjax::begin(); ?>

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">

        <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                     $model->from_date =$model->dateDisplayFormat();

//                     echo $form->field ( $model, 'from_date',
//                        [ 'options' => [ 'class' => $formGroupClass,
//                            //'value' => $model->start,
//                        ],
//                            'template' => $template
//                        ]
//                    )->widget ( jalaliDatePicker::className (), [ 'options' => ['format' => 'yyyy/mm/dd','viewformat' => 'yyyy/mm/dd','placement' => 'right','todayBtn' => 'linked','id' => 'event-fromDate','class' => 'form-control'  ] ] )->label ( NULL, [ 'class' => $labelClass ] );
                    ?>
                </div>
            </div>
<?php
$pizza=$model->from_date;
$pieces = explode("-", $pizza);
//echo $pieces[0];
$year=(int)$pieces[0];
$month=(int)$pieces[1];
$day=(int)$pieces[2];
echo $year;
echo '<br>';
echo $month;
echo '<br>';
echo $day;
?>
	</div>
	<?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
