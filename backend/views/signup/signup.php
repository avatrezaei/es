<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use backend\models\University;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Sign Up');
$this->params['breadcrumbs'][] = $this->title;

$uni = University::find()->all();
$listData=ArrayHelper::map($uni,'id','name');
?>

	<h3 class="font-green">
		<?php echo $this->title ?>
	</h3>

        <!--[if IE]>
            <div class="alert font-red bold center">
                <?= Yii::t('app','To use the system easy to use Firefox or Chrome browser.') ?>
            </div>
        <![endif]-->

	<p class="hint"> <?= Yii::t('app', 'Enter the following information carefully'); ?> : </p>

	<?php yii\widgets\Pjax::begin(['id' => 'signup_pjax']) ?>

		<?php $form = ActiveForm::begin(['id' => 'signup-form', 'enableClientValidation' => false]); ?>

		
					
			<?=
				$form
				->field($model, 'name')
				->textInput(['placeholder' => Yii::t('app', 'name'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'last_name')
				->textInput(['placeholder' =>  Yii::t('app', 'Last Name'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'username')
				->textInput([
					'placeholder' => Yii::t('app', 'National Id'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
            <?php
					   $dataSelectCity = ArrayHelper::map(University::find()->all(), 'id', 'name');   
			echo $form->field(
					$model, 
					'universityId'
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectCity,
					'options' => [
						'placeholder' => Yii::t('app', 'دانشگاه مورد نظر را انتخاب نمائید'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9']); 
		?>
			<?=
				$form
				->field($model, 'student_number')
				->textInput(['placeholder' => Yii::t('app', 'Student Number'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'email')
				->textInput([
					'placeholder' => Yii::t('app', 'Email'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'password')
				->passwordInput(['placeholder' => Yii::t('app', 'password'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9']); 
			?>
            <?=
            $form
                ->field($model, 'repeatpassword')
                ->passwordInput(['placeholder' => Yii::t('app', 'Repeat Password'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
                ->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9']);
            ?>

			<div class="form-actions pull-right">
                    <?= Html::submitButton(Yii::t('app', 'Sign Up'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
				<?= Html::a(Yii::t('zii', 'Back'), ['/login'], ['class' => 'btn btn-default pull-right', 'name' => 'cancle-button']) ?>
			</div>

		

	<?php ActiveForm::end(); ?>
	<?php yii\widgets\Pjax::end() ?>