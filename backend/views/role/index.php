<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $dataProvider array
 * @var $filterModel  dektrium\rbac\models\Search
 * @var $this         yii\web\View
 */


use kartik\select2\Select2;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
 
$this->title = Yii::t('app', 'Roles');
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'][] = [
    'label' => Yii::t('app', 'Create Role'),
    'url' => ['create']
];
?>

 
<div class="send-messsage-index">
    <div class="col-lg-12 col-md-12  ">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green">
                    <span class="caption-subject bold">
                        <i class="icon-envelope-open"></i>
                        <?= Html::encode($this->title) ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <?php Pjax::begin() ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $filterModel,
    'layout'       => "{items}\n{pager}",
    'columns'      => [
        [
            'attribute' => 'name',
            'header'    => Yii::t('app', 'Name'),
            'options'   => [
                'style' => 'width: 20%'
            ],
            'filter' => Select2::widget([
                'model'     => $filterModel,
                'attribute' => 'name',
                'data'      => $filterModel->getNameList(),
                'options'   => [
                    'placeholder' => Yii::t('app', 'Select role'),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]),
        ],
        [
            'attribute' => 'description',
            'header'    => Yii::t('app', 'Description'),
            'options'   => [
                'style' => 'width: 55%',
            ],
            'filterInputOptions' => [
                'class'       => 'form-control',
                'id'          => null,
                'placeholder' => Yii::t('app', 'Enter the description')
            ],
        ],
        [
            'attribute' => 'rule_name',
            'header'    => Yii::t('app', 'Rule name'),
            'options'   => [
                'style' => 'width: 20%'
            ],
            'filter' => Select2::widget([
                'model'     => $filterModel,
                'attribute' => 'rule_name',
                'data'      => $filterModel->getRuleList(),
                'options'   => [
                    'placeholder' => Yii::t('app', 'Select rule'),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]),
        ],
        [
            'class'      => ActionColumn::className(),
            'template'   => '{update} {delete}',
            'urlCreator' => function ($action, $model) {
                return Url::to(['/role/' . $action, 'name' => $model['name']]);
            },
            'options' => [
                'style' => 'width: 5%'
            ],
        ]
    ],
]) ?>

<?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div> 


 