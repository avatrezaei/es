<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userd-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'startingyear') ?>

    <?= $form->field($model, 'startingsemester') ?>

    <?= $form->field($model, 'major') ?>

    <?php // echo $form->field($model, 'fatherName') ?>

    <?php // echo $form->field($model, 'units') ?>

    <?php // echo $form->field($model, 'city_location_id') ?>

    <?php // echo $form->field($model, 'birthCityid') ?>

    <?php // echo $form->field($model, 'shift') ?>

    <?php // echo $form->field($model, 'jobincome') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
