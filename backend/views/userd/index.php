<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Userds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userd-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Userd', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'startingyear',
            'startingsemester',
            'major',
            // 'fatherName',
            // 'units',
            // 'city_location_id',
            // 'birthCityid',
            // 'shift',
            // 'jobincome',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
