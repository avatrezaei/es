<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Userd */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Userds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userd-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'startingyear',
            'startingsemester',
            'major',
            'fatherName',
            'units',
            'city_location_id',
            'birthCityid',
            'shift',
            'jobincome',
        ],
    ]) ?>

</div>
