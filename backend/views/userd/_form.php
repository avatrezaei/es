<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Userd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userd-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'startingyear')->textInput() ?>

    <?= $form->field($model, 'startingsemester')->textInput() ?>

    <?= $form->field($model, 'major')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fatherName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'units')->textInput() ?>

    <?= $form->field($model, 'city_location_id')->textInput() ?>

    <?= $form->field($model, 'birthCityid')->textInput() ?>

    <?= $form->field($model, 'shift')->textInput() ?>

    <?= $form->field($model, 'jobincome')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
