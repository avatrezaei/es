<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Userd */

$this->title = 'Update Userd: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Userds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
