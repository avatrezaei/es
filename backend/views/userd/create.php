<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Userd */

$this->title = 'Create Userd';
$this->params['breadcrumbs'][] = ['label' => 'Userds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
