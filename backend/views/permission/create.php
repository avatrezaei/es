<?php
use yii\helpers\Html;
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $model dektrium\rbac\models\Role
 * @var $this  yii\web\View
 */

$this->title = Yii::t('app', 'Create new permission');
$this->params['breadcrumbs'][] = $this->title;

?>

 <div class="send-messsage-index">
	<div class="col-lg-12 col-md-12  ">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-green">
					<span class="caption-subject bold">
						<i class="icon-key"></i>
						<?= Html::encode($this->title) ?>
					</span>
				</div>
			</div>
			<div class="portlet-body form">
				<?= $this->render('_form', [
				    'model' => $model,
				]) ?>
			</div>
		</div>
	</div>
</div>


 
