<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Place;

/* @var $this yii\web\View */
/* @var $model app\models\Floor */
/* @var $form yii\widgets\ActiveForm */


$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];

$Place = Place::find()->all();
$listData=ArrayHelper::map($Place,'id','name');
?>

<div class="floor-form">

    <?php $form = ActiveForm::begin(); ?>
<?php
				echo $form->field(
					$model,
					'id',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?= $form->field($model, 'parent_id',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList($listData,['prompt'=>'نام محل اسکان را مشخص نمائید', 'id'=>'parent_id'])->label(NULL, ['class'=>$labelClass]); ?>

    <?php
				echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'ایجاد' : 'بروزرسانی', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
