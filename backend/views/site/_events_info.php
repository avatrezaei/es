<?php

use yii\helpers\Html;

$this->title = Yii::t('app','Dashboard');

if(!Yii::$app->user->identity->isAdmin)
{
	$userId = Yii::$app->user->identity->id;

	$userDetail = UsersDetails::find()->select('id')->user($userId)->all();

	$_userDetailIds = NULL;
	foreach ($userDetail as $data) {
		$_userDetailIds[] = $data->id;
	}

	$eventsInfo = Event::find()->joinWith(['eventmember'])->where(['usersDetailsId' => $_userDetailIds])->sortByDate()->all();
}
else
{
	$eventsInfo = Event::find()->sortByDate()->all();
}

$currentEvents = NULL;//Event::find()->current()->all();
$previousEents = NULL;//Event::find()->previous()->all();

?>


<div class="row">

	<?php if(!empty($currentEvents) || !empty($previousEents)): ?>
	<div class="col-md-9 col-sm-12 col-xs-12">
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-directions font-blue-steel"></i>
					<span class="caption-subject font-blue-steel bold uppercase"><?= Yii::t('app', 'Events') ?></span>
				</div>
			</div>
			<div class="portlet-body">

				<?php if(!empty($currentEvents)): ?>
				<span class="font-blue font-lg bold uppercase"><?php echo Yii::t('app', 'Current Event'); ?></span>
				<div class="table-scrollable table-scrollable-borderless">
					<table class="table table-hover table-light">
						<thead>
							<tr class="uppercase">
								<th> <?= Yii::t('app', 'Event Title') ?> </th>
								<th> <?= Yii::t('zii', 'Start') ?> </th>
								<th> <?= Yii::t('zii', 'End') ?> </th>
								<th> <?= Yii::t('app', 'Status') ?> </th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($currentEvents as $event): ?>
							<tr>
								<td> <span class="primary-link"><?= Html::encode($event->name) ?></span> </td>
								<td> <span class="bold theme-font"><?= $event->Start ?></span> </td>
								<td> <span class="bold theme-font"><?= $event->End ?></span> </td>
								<td> <?= $event->EventStatus ?> </td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php endif; ?>

				<?php if(!empty($previousEents)): ?>
				<span class="font-red font-lg bold uppercase"><?php echo Yii::t('app', 'Previous Event'); ?></span>
				<div class="table-scrollable table-scrollable-borderless">
					<table class="table table-hover table-light">
						<thead>
							<tr class="uppercase">
								<th> <?= Yii::t('app', 'Event Title') ?> </th>
								<th> <?= Yii::t('zii', 'Start') ?> </th>
								<th> <?= Yii::t('zii', 'End') ?> </th>
								<th> <?= Yii::t('app', 'Status') ?> </th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($previousEents as $event): ?>
							<tr>
								<td> <span class="primary-link"><?= Html::encode($event->name) ?></span> </td>
								<td> <span class="bold theme-font"><?= $event->Start ?></span> </td>
								<td> <span class="bold theme-font"><?= $event->End ?></span> </td>
								<td> <?= $event->EventStatus ?> </td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>

<?php
/*$auth = Yii::$app->authManager;
$rules = $auth->getAssignmentsWithEvent($userId, $_event->id);

	foreach($rules as $myRole):
		$role = $auth->getRole($myRole->roleName);
		echo Html::encode($role->description)
*/
?>