<?php

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;

use backend\assets\UserProfile;

use backend\models\Event;
use backend\modules\YumUsers\models\UsersDetails;
use backend\modules\YumUsers\models\User;

UserProfile::register($this);

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Dashboard');

$seniorUserEvent = Yii::$app->user->can('SeniorUserEvent');

/**
 * Send Active Code via SMS
 */
// $result = Yii::$app->sms->SendSms2(['09215203627'], 'your access cdoe is: ');
// echo "<pre>";print_r($result);echo "</pre>";die();

/**
 * Food Reserve
 */
// $result = Yii::$app->food->DefinePersons(['09215203627']);
// $result = Yii::$app->food->Reserve(['09215203627'],'2016-06-15', '2016-06-16', 'DINNER', '12');
// $result = Yii::$app->food->Cancel(['09215203627'],'2016-06-15', '2016-06-16', 'DINNER');
// $result = Yii::$app->food->Report(['09215203627'],'2016-06-15', '2016-06-16');
// echo "<pre>";print_r($result);echo "</pre>";die();

/**
 * Google Notification
 */
// $result = Yii::$app->notif->post('send', []);

                        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                            //echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
                        }

$startIntroGuide = NULL;

/**
 * Check if user is newbie and fisrt login show guide
 */
if(Yii::$app->session->has('newbie')){
	$startIntroGuide = 'introguide.start()';
}

$this->registerJs(
<<<SCRIPT
	$startIntroGuide
SCRIPT
, View::POS_END);

/**
 * Help Message
 */
$selectEvent = Yii::t('app', 'HINT_SELECT_EVENT');

?>

<div class="site-index">
	<div class="row">
		 
		 
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-cup font-green-sharp"></i>
						<span class="caption-subject font-green-sharp bold uppercase"><?= Yii::t('app', 'Links Management') ?></span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<ul class="user-dashborad-menu">
								<li class="dashboard-link bg-blue-ebonyclay bg-font-blue-ebonyclay">
									<?php
										echo Html::a(
											'<i class="icon-diamond"></i>' . Yii::t('app', 'Financial Management'),
											['/financial/']
										);
									?>
								</li>

								<li class="dashboard-link bg-blue-madison bg-font-blue-madison">
									<?php
										echo Html::a(
											'<i class="icon-envelope"></i> ' . Yii::t('app', 'Requests'),
											['/request']
										);
									?>
								</li>

								<li class="dashboard-link bg-blue-steel bg-font-blue-steel">
									<?php
										echo Html::a(
											'<i class="icon-user"></i> ' . Yii::t('app', 'Users'),
											['/person']
										);
									?>
								</li>

								<li class="dashboard-link bg-blue-dark bg-font-blue-dark">
									<?php
										echo Html::a(
											'<i class="icon-home"></i> ' . Yii::t('app', 'Eskan Managment'),
											['/eskan']
										);
									?>
								</li>

								 

							 

							 
   

							 
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		 
	</div>
</div>