<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

// $this->title = $name;
$this->title = Html::encode($message);

?>
				<?php if (Yii::$app->user->isGuest): ?>
				<div class="number font-red"> <?= Yii::$app->errorHandler->exception->statusCode ?> </div>
				<div class="details">
					<h3><?= Html::encode($this->title) ?></h3>
					<p> 
						<?= Yii::t('zii', 'The above error occurred when the Web server was processing your request.') ?>
					</p>
				</div>
				<?php else: ?>
				<div class="col-md-12 page-404">
					<div class="number font-green"> <?= Yii::$app->errorHandler->exception->statusCode ?> </div>
					<div class="details">
						<h3><?= Html::encode($this->title) ?></h3>
						<p> 
							<?= Yii::t('zii', 'The above error occurred when the Web server was processing your request.') ?>
						</p>
						<p>
							<?= Html::a( Yii::t('app', 'Dashboard'), ['/'], ['class' => 'btn btn green']) ?>
						</p>
					</div>
				</div>
				<?php endif; ?>