<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2; 

$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;
use backend\assets\InputMaskAsset;
InputMaskAsset::register($this);


$dateTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-calendar"></i>{input}</div> {hint} {error}</div>';
?>

<div class="mdate-form">

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">
		<div class="row">
			 

			<div class="col-md-6">
				<?php

							echo $form->field(
								$model,
								'DateType',
								[
									'options' => ['class' => $formGroupClass],
									'template' => $template
								]
							)
							->widget(Select2::classname(), [
								'data' => [ 'REQUEST' => 'درخواست عضویت', 'FAMILY_FORM' => 'ثبت اعضای خانواده', 'ESKAN' => 'درخواست خوابگاه', ],
								'theme' => Select2::THEME_BOOTSTRAP,
								'options' => [
									'placeholder' => Yii::t('app', 'Please Select a Type'),
									'dir' => 'rtl',
								],
							])
							->label(NULL, ['class'=>$labelClass]);


						?>
			</div>		

		</div>


		<div class="row">
			<div class="col-md-6">
								<?php
									echo $form->field(
										$model,
										'FromDate',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $dateTemplate
										]
									)
									->widget(jalaliDatePicker::className(), [
										'options' => [
											'format' => 'yyyy/mm/dd',
											'viewformat' => 'yyyy/mm/dd',
											'placement' => 'right',
											'todayBtn' => 'linked' ,
											'id' => 'mdate-fromdate',
											'class' => 'form-control' ,
											'autoclose'=>true,
										],
									])
									->label(NULL, ['class' => $labelClass]);
								?>
			</div>

			<div class="col-md-6">
								<?php
									echo $form->field(
										$model,
										'ToDate',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $dateTemplate
										]
									)
									->widget(jalaliDatePicker::className(), [
										'options' => [
											'format' => 'yyyy/mm/dd',
											'viewformat' => 'yyyy/mm/dd',
											'placement' => 'right',
											'todayBtn' => 'linked' ,
											'id' => 'mdate-todate',
											'class' => 'form-control' ,
											'autoclose'=>true,
										],
									])
									->label(NULL, ['class' => $labelClass]);
								?>
			</div>
		</div>

		<div class="row">
		<div class="col-md-6">
            <?php

            echo $form->field(
                $model,
                'EduYear',
                [
                    'options' => ['class' => $formGroupClass],
                    'template' => $template
                ]
            )
                ->widget(Select2::classname(), [
                    'data' => [ '1' => '1395-1396', '2' => '1396-1397','3' => '1397-1398','4' => '1398-1399','5' => '1399-1400',],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Please Select a EduYear'),
                        'dir' => 'rtl',
                    ],
                ])
                ->label(NULL, ['class'=>$labelClass]);


            ?>
			</div>

			<div class="col-md-6">
                <?php

                echo $form->field(
                    $model,
                    'semester',
                    [
                        'options' => ['class' => $formGroupClass],
                        'template' => $template
                    ]
                )
                    ->widget(Select2::classname(), [
                        'data' => [ '1' => 'نیمسال اول', '2' => 'نیمسال دوم','3' => 'ترم تابستان'],
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Please Select a semester'),
                            'dir' => 'rtl',
                        ],
                    ])
                    ->label(NULL, ['class'=>$labelClass]);


                ?>
			</div>
		</div>

	</div>

	<div class="form-actions">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
