<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mdate */

$this->title = Yii::t('app', 'Create Mdate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mdates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mdate-create">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
                    <?php
                    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                        //echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
                    }
                    ?>
					<?= $this->render('_form', [
						'model' => $model,
					]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
