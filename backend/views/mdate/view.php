<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mdate */

$this->title = $model->RecID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mdates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Update'),
		'url' => ['update', 'id' => $model->RecID]
	],
	[
		'label' => Yii::t('app', 'Delete'),
		'url' => ['delete', 'id' => $model->RecID],
		'options' => [
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
			'class' => 'btn btn-danger'
		]
	],
];

?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="mdate-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div>
						<h4 class="profile-desc-title"> <?= Html::encode($this->title) ?> </h4>

						<?= DetailView::widget([
							'model' => $model,
							'template' => Yii::$app->params['detalView'],
							'attributes' => [
								'DateType',
								'FromDate',
								'ToDate',
								'EduYear',
								'semester',
							],
						]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>