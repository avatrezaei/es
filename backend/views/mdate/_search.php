<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MdatesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mdate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'RecID') ?>

    <?= $form->field($model, 'DateType') ?>

    <?= $form->field($model, 'FromDate') ?>

    <?= $form->field($model, 'ToDate') ?>

    <?= $form->field($model, 'EduYear') ?>

    <?php // echo $form->field($model, 'semester') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
