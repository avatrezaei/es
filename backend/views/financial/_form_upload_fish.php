<?php
 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;
use yii\web\View;
use yii\web\JsExpression;

use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

use backend\models\University;
use backend\models\Province;
use backend\models\memberPayment;
use backend\models\User;
use app\models\Language;
use kartik\file\FileInput;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;
use backend\assets\InputMaskAsset;
InputMaskAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];

$this->title = Yii::t('app', 'Register Bill');
$this->params['page_title'] = false;
$timeTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-clock-o"></i>{input}</div> {hint} {error}</div>';
$dateTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-calendar"></i>{input}</div> {hint} {error}</div>';
$moneyTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-money"></i>{input}</div> {hint} {error}</div>';
$persons = ArrayHelper::map(User::find()->students()->all(), 'id', 'info'); 

$this->registerJs(
<<<SCRIPT
 
	$(".time-format").inputmask("hh:mm", {
		autoUnmask: true,
		"placeholder": "_"
	});

	 

SCRIPT
, View::POS_END);


 

?>
<div class="draft-create">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
					<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
					<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

					<div class="form-body">
						<div class="row">
							<div class="col-md-6 col-sm-6 xol-xs-12">
								<?php
									echo $form->field(
										$model,
										'paymentNo',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $template
										]
									)
									->textInput(
										['maxlength' => 255, 'class' => $inputClass]
									)
									->label(NULL, ['class'=>$labelClass]);
								?>
							</div>

							<!-- person name -->
							<div class="col-md-6 col-sm-6 xol-xs-12">
								<?php

									echo $form->field(
										$model,
										'personId',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $template
										]
									)
									->widget(Select2::classname(), [
										'data' => $persons,
										'theme' => Select2::THEME_BOOTSTRAP,
										'options' => [
											'placeholder' => Yii::t('app', 'Please Select a Person'),
											'dir' => 'rtl',
										],
									])
									->label(NULL, ['class'=>$labelClass]);


								 ?>

							</div>

							 



						</div>

						<div class="row">
			<!-- year -->
			<div class="col-md-6 col-sm-6 xol-xs-12">
			 


				<?= $form->field($model, 'eduYear',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList(['1394','1395','1396','1397','1398'],['prompt'=>'سال تحصیلی را وارد کنید.', 'id'=>'eduYear'])->label(NULL, ['class'=>$labelClass]);
		    ?>
			</div>
			<!-- semester -->
			<div class="col-md-6 col-sm-6 xol-xs-12">
					
					<?= $form->field($model, 'semester',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList(['1','2','3'],['prompt'=>'نیم سال تحصیلی را وارد کنید.', 'id'=>'semester'])->label(NULL, ['class'=>$labelClass]);
		    ?>

			</div>
		</div>

						<div class="row">
							<div class="col-md-6 col-sm-6 xol-xs-12">
								<?php
									echo $form->field(
										$model,
										'amount',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $moneyTemplate
										]
									)
									->textInput(
										['maxlength' => 255, 'class' => $inputClass]
									)
									->label(NULL, ['class'=>$labelClass]);
								?>
							</div>

							<div class="col-md-6 col-sm-6 xol-xs-12">
								<?php
									echo $form->field(
										$model,
										'paymentDate',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $dateTemplate
										]
									)
									->widget(jalaliDatePicker::className(), [
										'options' => [
											'format' => 'yyyy/mm/dd',
											'viewformat' => 'yyyy/mm/dd',
											'placement' => 'right',
											'todayBtn' => 'linked' ,
											'id' => 'caravanpayment-paidDate',
											'class' => 'form-control' ,
											'autoclose'=>true,
										],
									])
									->label(NULL, ['class' => $labelClass]);
								?>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 xol-xs-12">
								 

								<?php
									echo $form->field(
										$model,
										'paymentTime',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $timeTemplate
										]
									)
									->textInput(
										['class' => $inputClass . ' time-format']
									)
									->label(NULL, ['class'=>$labelClass]);
								?>
							</div>

							<div class="col-md-6 col-sm-6 xol-xs-12">
								 

								<?php
									echo $form->field(
										$model,
										'accountNo',
										[
											'options' => ['class' => $formGroupClass],
											'template' => $moneyTemplate
										]
									)
									->textInput(
										['class' => $inputClass . ' money-format']
									)
									->label(NULL, ['class'=>$labelClass]);
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 xol-xs-12">
								<?php
									echo $form->field(
									$model,
									'paymentType',
									[
										'options' => ['class' => $formGroupClass],
										'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="nutritionmeal-mealtype">{input} </div> {hint} {error}</div>'
									]
								)
								->radioList(
									memberPayment::itemAlias('Type'),
									[
										'tag' => false,
										'unselect' => NULL,
										'item' => function($index, $label, $name, $checked, $value) {
											return '<label class="btn btn-warning '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value, 'autocomplete'=>'off']) . $label . '</label>';
										}
									]
								)
								->label(NULL, ['class'=>$labelClass]);
							?>
							</div>
							
						</div>

						<div class="row">
							<div class="col-md-6 col-sm-6 xol-xs-12">
								<?php
									echo $form->field(
									$model,
									'payFor',
									[
										'options' => ['class' => $formGroupClass],
										'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="nutritionmeal-mealtype">{input} </div> {hint} {error}</div>'
									]
								)
								->radioList(
									memberPayment::itemAlias('For'),
									[
										'tag' => false,
										'unselect' => NULL,
										'item' => function($index, $label, $name, $checked, $value) {
											return '<label class="btn btn-warning '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value, 'autocomplete'=>'off']) . $label . '</label>';
										}
									]
								)
								->label(NULL, ['class'=>$labelClass]);
							?>
							</div>
							
						</div>

						
					</div>

					<div class="form-actions">
						<div class="row">
							<div class="col-md-6 col-sm-6 xol-xs-12">
								<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Register Bill') : Yii::t('app', 'Update'), ['class' => 'btn green']) ?>
							</div>
						</div>
					</div>

					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
