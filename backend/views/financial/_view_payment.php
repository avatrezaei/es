<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use backend\models\CaravanPayment;

/* @var $this yii\web\View */
/* @var $model backend\models\CaravanPayment */

$this->title = $model->CaravanName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Caravan Payments'), 'url' => ['payments', 'id' => $model->caravanId]];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Update'),
		'url' => ['update', 'id' => $model->id]
	],
];

?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="caravan-payment-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div>
						<h4 class="profile-desc-title"> <?= Html::encode($this->title) ?> </h4>

						<?= DetailView::widget([
							'model' => $model,
							'template' => Yii::$app->params['detalView'],
							'attributes' => [
								'fishNo',
								'fishFileId',
								'payerName',
								'paidDate',
								[
									'label' => $model->getAttributeLabel('paidMony'),
									'value' => $model->Amount,
								],
								[
									'label' => $model->getAttributeLabel('insertDate'),
									'value' => $model->insert,
								],
								[
									'label' => $model->getAttributeLabel('lastUpdateDate'),
									'value' => $model->lastUpdate,
									'visible' => ($model->status > CaravanPayment::STATUS_WAITING)
								],
								[
									'label' => $model->getAttributeLabel('status'),
									'value' => $model->Stat,
									'format' => 'html',
								],
								[
									'label' => $model->getAttributeLabel('statusChangeDate'),
									'value' => $model->statusChange,
									'visible' => ($model->status > CaravanPayment::STATUS_WAITING)
								],
								[
									'label' => $model->getAttributeLabel('statusChangeUserId'),
									'value' => $model->ChangeUserName,
									'visible' => ($model->status > CaravanPayment::STATUS_WAITING)
								],
							],
						]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>