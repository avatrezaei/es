<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manage Payments');
$this->params['page_title'] = false;

$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
<<<SCRIPT
	$('#caravan-university').on('keyup change', function() {
		table.columns(1).search($(this).find(':selected').val()).draw();
	});
SCRIPT
, \yii\web\View::POS_END);

?>

<div class="financial-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-credit-card font-dark"></i> <span
					class="caption-subject bold uppercase"><?= $this->title; ?></span>
			</div>
		</div>

		<div class="portlet-body">
			<?= $widget->run()?>
		</div>
	</div>
</div>