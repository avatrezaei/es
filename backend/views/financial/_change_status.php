<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use backend\assets\iCheckAsset;
use backend\models\Caravan;
use backend\models\CaravanLog;

iCheckAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\CaravanLog */

$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];

$model->changedStatus = $caravan->status;
$reason = ($model->changedStatus != Caravan::STATUS_BACK_TO_REFORMS ? "$('#reason-back').hide();" : "");
$this->registerJs("
$reason
$('body').on('change','#caravanlog-changedstatus', function(){
	var id = $('.reason:checked').val();
	if(id == 4)
		$('#reason-back').show();
	else
		$('#reason-back').hide();
		
	if(id == 4)
		$('#send_sms').show();
	else
		$('#send_sms').hide();
});
");

?>
	<div class="form">
		<div class="caravan-log-form">
			<?php $form = ActiveForm::begin(['action' => ['final', 'id' => $caravan->id], 'options' => ['class' => 'form-horizontal']]); ?>
			<?php echo $form->errorSummary([$model,$caravan], ['class' => 'alert alert-danger']);  ?>

			<div class="form-body">

				<?php
					echo $form->field(
						$model,
						'changedStatus',
						[
							'options' => ['class' => $formGroupClass],
							'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="caravanlog-changedstatus">{input} </div> {hint} {error}</div>'
						]
					)
					->radioList(
						Caravan::itemAlias('Financial'),
						[
							'tag' => false,
							'unselect' => NULL,
							'item' => function($index, $label, $name, $checked, $value) {
								return '<label class="btn btn-info '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value' => $value, 'autocomplete'=>'off', 'class' => 'reason']) . $label . '</label>';
							}
						]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
				<div id="reason-back">
					<?php
					echo $form->field(
						$model,
						'reasonBack',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
						->widget(Select2::classname(), [
							'data' => CaravanLog::itemAlias('FinancialReasonBack'),
							'theme' => Select2::THEME_BOOTSTRAP,
							'options' => [
								'placeholder' => Yii::t('app', 'Disapproval reason and Back to reforms'),
								'dir' => 'rtl',
							],
						])
						->label(NULL, ['class'=>$labelClass]);
					?>
				</div>

				<?php
					echo $form->field(
						$model,
						'description',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->textarea(['rows' => 6, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
				<div id="send_sms">

				<?php
				echo $form->field(
						$model,
						'sendSms',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
					->checkBox([
						'label' => '',
						'uncheck' => null,
						'selected' => true,
						'class' => 'icheck ' . $inputClass,
						'data-checkbox' => 'icheckbox_flat-blue',
					])
					->label(Yii::t('app','Send SMS To University Resp User'), ['class'=>$labelClass]);
				?>
			    </div>
			</div>

			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
					</div>
				</div>
			</div>

			<?php ActiveForm::end(); ?>

		</div> 
	</div>