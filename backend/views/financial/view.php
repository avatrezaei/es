<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Update'),
		'url' => ['update', 'id' => $model->id]
	],
	[
		'label' => Yii::t('app', 'Delete'),
		'url' => ['delete', 'id' => $model->id],
		'options' => [
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
			'class' => 'btn btn-danger'
		]
	],
];

?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="person-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div>
						<?php
									//$img = Url::to(\Yii::$app->request->BaseUrl."/uploads/").$model->member->file;

									//$img = Url::to(\Yii::$app->request->BaseUrl."/uploads/").$model->member->file;
						?>
						<h4 class="profile-desc-title"> 
						<img alt="" class="img-circle" width="100" height="100" src="/esjj/backend/web/fum/layouts/layout/img/avatar.png">
						</h4>
						<div class="row">
							
							<div class="col-md-12">

								<?= DetailView::widget([
									'model' => $model,
									'template' => Yii::$app->params['detalView'],
									'attributes' => [
										'username',
										'paymentNo',
										'paymentType',
										'paymentTime',
										'paymentDate',
										'amount',
										'payFor',
										'accountNo',
										'comment',
										'eduYear',
									],
								]) ?>
							</div>

							 
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>