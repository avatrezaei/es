<?php

use yii\helpers\Html;

use backend\assets\CounterUPAsset;
use backend\models\CaravanPayment;
use backend\models\CaravanLog;

CounterUPAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CaravanPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title =  Yii::t('app', 'Payments') ;
$this->params['page_title'] = false;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

 

$this->registerJs(
<<<SCRIPT
	$('#payment-name').on('keyup change', function() {
		table.columns(1).search($(this).find(':selected').val()).draw();
	});
SCRIPT
, \yii\web\View::POS_END);

?>
<div class="caravan-payment-index">
	 

	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-wallet font-dark"></i>
				<span class="caption-subject bold uppercase"><?= Html::encode($this->title) ?></span>
			</div>
		</div>
		<div class="portlet-body">
			<?=  $widget->run() ?>
		</div>
	</div>

	 
</div>



