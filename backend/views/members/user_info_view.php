<?php

use yii\widgets\DetailView;
use kartik\detail\DetailViewTiny;
use kartik\detail\tiny;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\AppLanguage;

//$this->title = Yii::t('app', 'Profile');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => '#'];
//$this->params['breadcrumbs'][] = $this->title;

//$model->birthDate=$model->getFaDateFromTimeStamp('birthDate');

   $birthDateAttr = [
		'attribute' => 'birthDate',
		'value' => $model->birthDate,
	];
	$createDateAttr = [
		'attribute' => 'created_at',
		'value' => $model->getFaDateFromTimeStamp('created_at'),
		//'format' => 'date',
		'displayOnly' => true,
	];

	$modifiedDateAttr = [
		'attribute' => 'updated_at',
		'value' => $model->getFaDateFromTimeStamp('updated_at'),
		//'format' => 'date',
		'displayOnly' => true,
	];
	
	$lastLoginTimeAttr = [
		'attribute' => 'last_login_time',
		'value' => $model->getFaDateFromTimeStamp('last_login_time'),
		'displayOnly' => true,
	];	
	
$admin 				= Yii::$app->user->can('admin');
$supervisor			= Yii::$app->user->can('supervisor');
$student			= Yii::$app->user->can('student');


$attributes = [];
if($admin || $supervisor){
	$attributes = [	
	[
		'attribute' => 'name',
		'value' => $model->name,
	],
	[
		'attribute' => 'last_name',
		'value' => $model->last_name,
	],
	[
		'attribute' => 'nationalId',
		'value' => $model->nationalId,
	],

	[
		'attribute' => 'username',
		'value' => $model->username,
	],
	 
 
	 
	 
	 
	 
	 
	[
		'attribute' => 'email',
	],
];
}	
else if($student){
	$attributes = [	
	[
		'attribute' => 'name',
		'value' => $model->name,
	],
	[
		'attribute' => 'last_name',
		'value' => $model->last_name,
	],
	[
		'attribute' => 'nationalId',
		'value' => $model->nationalId,
	],
	[
		'attribute' => 'idNo',
		'value' => $model->idNo,
	],
	[
		'attribute' => 'fatherName',
		'value' => $modeld->fatherName,
	],   
	[
		'attribute' => 'uniname',
		'value' => $model->universityName,
		
	],
	[
		'attribute' => 'student_number',
		'value' => $model->student_number,
	],
	[
		'attribute' => 'startingyear',
		'value' => $modeld->startingyear,
	],
	[
		'attribute' => 'startingsemester',
		'value' => $modeld->startingsemester,
	],
	[
		'attribute' => 'major',
		'value' => $modeld->major,
	],
	[
		'attribute' => 'birthDate',
		'value' => $model->birthDate,
	],
	[
		'attribute' => 'tel',
		'value' => $model->tel,
	],
	[
		'attribute' => 'mobile',
		'value' => $model->mobile,
	],
	[
		'attribute' => 'address',
		'value' => $model->address,
	],
	[
		'attribute' => 'gender',
		'format' => 'raw',
		'value' => $model->Sex,
	],
	[
		'attribute' => 'marital',
		'format' => 'raw',
		'value' => $model->Marital,
	],
	[
		'attribute' => 'email',
	],
];
}


?>


<div class="user-info-view">
	<div class="col-md-8">
		<div class="profile-content">
			<div class="portlet light ">
				<div>
					<h4 class="profile-desc-title">  </h4>

						<?php
							echo DetailView::widget([
								'model' => $model,
								'template' => Yii::$app->params['detalView'],
								'options' => [
									'tag' => 'div'
								],
								'id' => 'detail_view_item_' . $model->id,
								'attributes' => $attributes,
							]);
						?>
				</div>
			</div>
		</div>
	</div>
</div>


