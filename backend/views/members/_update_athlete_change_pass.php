<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;



$formGroupClass = "form-group";
$template = '{label}{input} {hint} {error}';
$inputClass = "form-control";
$labelClass ="control-label";
$errorClass = Yii::$app->params['errorClass'];

?>



<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#update_user").on("pjax:end", function() {
		
$("#user_quick_info").load(document.URL +  " #user_quick_info");
	  
  //Reload GridView)
		});
	});'
);
?>
<div class="tab-pane" id="tab_1_3">
	<?php //yii\widgets\Pjax::begin(['id' => 'update_user']) ?>
   
	<?php $form_change_pass= ActiveForm::begin(['options' => ['data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>
	<?php
		
		echo $form_change_pass->errorSummary([$model,$changePasswordModel], ['class' => 'alert alert-danger']);
		
		?>


	<?php
			echo $form_change_pass->field(
					$changePasswordModel,
					'passwordInput',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->passwordInput(
					['maxlength' => 50, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>




	<?php
			echo $form_change_pass->field(
					$changePasswordModel,
					'inputNewPassword',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->passwordInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 




	<?php
			echo $form_change_pass->field(
					$changePasswordModel,
					'inputNewPassword_repeat',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->passwordInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
	?> 

	

	
	
		<div class="margiv-top-10">
		<?= Html::submitButton(Yii::t('app', 'Change Password'), ['class' => 'btn green','name'=>'change_pass_btn']) ?>
		<?= Html::submitButton(Yii::t('app', 'Cancel') , ['class' => 'btn default'])	?>
		</div>

   <?php ActiveForm::end(); ?>
<?php //yii\widgets\Pjax::end() ?>	 
</div>
