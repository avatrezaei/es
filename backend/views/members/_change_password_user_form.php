<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* * *select2 uses*** */
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results_change_pass"  >

</div>
<div>
	<?php yii\widgets\Pjax::begin(['id' => 'change_pass_pjax']) ?>
	<?php echo $message ?>
	<?php
	// var_dump($message);
	/* ------------show flash messages----------------- */
//	$session = Yii::$app->session;
//   // check the availability
//	$result = $session->hasFlash('change_password_success');
//   // get and display the message
//	echo $session->getFlash('change_password_success');
	?>
	<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'id' => 'form_change_pass']]); ?>
	<?php echo $form->errorSummary([$model]); ?>

	<div class="form-group col-xs-6"> 
		<?php
		$model->inputNewPassword = '';
		echo $form->field($model, 'inputNewPassword')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])
		?>
	</div>
	<div class="form-group col-xs-6"> 
		<?php
		$model->inputNewPassword_repeat = '';
		echo $form->field($model, 'inputNewPassword_repeat')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])
		?>
	</div>


	<div class="form-group">
		<input type="submit" name="submitBtn" class = "btn btn-sb" value="<?= Yii::t('app', 'Confirm') ?>"id="submit_btn">
	<?php //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit'])   ?>
	</div>

<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>

</div>
