<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Absence */

$this->title = Yii::t('app', 'Create Absence');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Absences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absence-create">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
					<?= $this->render('_form', [
						'model' => $model,
					]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
