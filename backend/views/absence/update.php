<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Absence */

$this->title = Yii::t('app', 'Update').' غیبت ثبت شده برای '. $model->user->name.' '.$model->user->last_name.'-'.$model->user->student_number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Absences'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('zii', 'Update');
?>
<div class="absence-update">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
					<?= $this->render('_form', [
						'model' => $model,
					]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
