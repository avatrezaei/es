<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Absence */

$this->title ='غیبت ثبت شده برای'.' '. $model->user->name.' '.$model->user->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Absences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Update'),
		'url' => ['update', 'id' => $model->id]
	],
];

?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="absence-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div>
						<h4 class="profile-desc-title"> <?= Html::encode($this->title) ?> </h4>

						<?= DetailView::widget([
							'model' => $model,
							'template' => Yii::$app->params['detalView'],
							'attributes' => [
							        [
                                        'label' => 'شماره دانشجویی',
                                        'value' => $model->user->student_number,
                                    ],
								'date',
								'description',
                                    [
                                        'label' =>Yii::t('app','Absence Reason') ,
                                        'value' => $model->getReason($model->reason_of_absence),
                                    ],
							],
						]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>