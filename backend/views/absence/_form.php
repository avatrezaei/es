<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Absence */
/* @var $form yii\widgets\ActiveForm */


$formGroupClass = Yii::$app->params['formGroupClass'];
//$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$errorClass = Yii::$app->params['errorClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$labelClass = 'col-md-3';
$dateTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-calendar"></i>{input}</div> {hint} {error}</div>';


$floor = \backend\models\User::find()->andWhere(['type'=>0])->all();
$listData=ArrayHelper::map($floor,'id',function($model, $defaultValue) {
    //$caravan=Place::find()->andWhere(['id'=>$model['parent_id']])->one();
    return $model['name'].' '.$model['last_name'].'-'.$model['student_number'];
});
?>

<div class="absence-form">

	<?php $form = ActiveForm::begin(); ?>


	<div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-1 col-xs-5">
        <?php
        echo $form->field($model, 'user_id',[
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ])
            ->widget(Select2::classname(), [
                'data' => $listData,
                'language' => 'fa-IR',
                'options' => ['placeholder' => 'دانشجوی مورد نظر را انتخاب نمایید'],
                'pluginOptions' => [
                    'allowClear' => true
                ],

            ])->label(NULL, ['class'=>$labelClass]);
        ?>
            </div>
                <div class="col-md-6 col-sm-1 col-xs-5">
        <?php
        $model->date =$model->dateDisplayFormat();

        echo $form->field(
            $model,
            'date',
            [
                'options' => ['class' => $formGroupClass],
                'template' => $dateTemplate
            ]
        )->widget ( jalaliDatePicker::className (), [ 'options' =>
            [
                'format' => 'yyyy/mm/dd',
                'viewformat' => 'yyyy/mm/dd',
                'placement' => 'right',
                'todayBtn' => 'linked',
                'id' => 'event-fromdate',
                'class' => 'form-control',
                'autoclose'=>true,
            ] ] )
            ->label ( NULL, [ 'class' => $labelClass ] );
        ?>
                </div></div>
        <div class="row">
            <div class="col-md-6 col-sm-1 col-xs-5">
        <?= $form->field($model, 'reason_of_absence',[
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ])
            ->dropDownList(['1'=>'علت اول', '2'=>'علت دوم','3'=>'علت سوم', '4'=>'علت چهارم','5'=>'علت پنجم'],['prompt'=>'علت غیبت را انتخاب نمائید'])
            ->label(NULL, ['class'=>$labelClass]);
        ?>
            </div>
                <div class="col-md-6 col-sm-1 col-xs-5">
        <?php
        echo $form->field(
            $model,
            'description',
            [
                'options' => ['class' => $formGroupClass],
                'template' => $template
            ]
        )
            ->textarea(
                ['class' => $inputClass]
            )
            ->label(NULL, ['class'=>$labelClass]);
        ?>
                </div></div>
	</div>

    <div class="margiv-top-10">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
