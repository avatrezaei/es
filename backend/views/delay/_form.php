<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\TimePicker;
use kartik\select2\Select2;
use yii\helpers\Url;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Delay */
/* @var $form yii\widgets\ActiveForm */


$formGroupClass = Yii::$app->params['formGroupClass'];
//$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$errorClass = Yii::$app->params['errorClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$labelClass = 'col-md-3';
$dateTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-calendar"></i>{input}</div> {hint} {error}</div>';

$floor = \backend\models\User::find()->andWhere(['type'=>0])->all();
$listData=ArrayHelper::map($floor,'id',function($model, $defaultValue) {
    //$caravan=Place::find()->andWhere(['id'=>$model['parent_id']])->one();
    return $model['name'].' '.$model['last_name'].'-'.$model['student_number'];
});
?>

<div class="delay-form">

	<?php $form = ActiveForm::begin(); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-1 col-xs-5">
        <?php
        echo $form->field($model, 'user_id',[
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ])
            ->widget(Select2::classname(), [
                'data' => $listData,
                'language' => 'fa-IR',
                'options' => ['placeholder' => 'دانشجوی مورد نظر را انتخاب نمایید'],
                'pluginOptions' => [
                    'allowClear' => true
                ],

            ])->label(NULL, ['class'=>$labelClass]);
        ?>
            </div>
                <div class="col-md-6 col-sm-1 col-xs-5">
        <?php
        $model->date =$model->dateDisplayFormat();

        echo $form->field(
            $model,
            'date',
            [
                'options' => ['class' => $formGroupClass],
                'template' => $dateTemplate
            ]
        )->widget ( jalaliDatePicker::className (), [ 'options' =>
            [
                'format' => 'yyyy/mm/dd',
                'viewformat' => 'yyyy/mm/dd',
                'placement' => 'right',
                'todayBtn' => 'linked',
                'id' => 'delayid',
                'class' => 'form-control',
                'autoclose'=>true,
            ] ] )
            ->label ( NULL, [ 'class' => $labelClass ] );
        ?>
                </div></div>
        <div class="row">
            <div class="col-md-6 col-sm-1 col-xs-5">
                <?php
        echo $form->field($model, 'time',[
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ])->widget(TimePicker::classname(),
            [
                'name' => 't1',
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                    ]
            ])
            ->label(NULL, ['class'=>$labelClass]);
        ?>
            </div>
                <div class="col-md-6 col-sm-1 col-xs-5">
        <?php
        echo $form->field(
            $model,
            'description',
            [
                'options' => ['class' => $formGroupClass],
                'template' => $template
            ]
        )
            ->textarea(
                ['class' => $inputClass]
            )
            ->label(NULL, ['class'=>$labelClass]);
        ?>
                </div></div>

	</div>

    <div class="margiv-top-10">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
			</div>
	<?php ActiveForm::end(); ?>

</div>
