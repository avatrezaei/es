<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Exitform */

$this->title = Yii::t('app', 'Delay History');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exitforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exitform-create">
    <div class="row">
        <div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
                    </div>
                </div>
                <?php

                $query = \app\models\Delay::find();
                // add conditions that should always apply here

                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $query,
                ]);
                // grid filtering conditions
                $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
                $query->andFilterWhere([
                    'user_id' => $curentUserId,
                    //'exit_date'=>'0000-00-00',
                ]);
                $searchModel = new \app\models\DelaySearch();


                ?>
                <?= \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                            [

                                'header' => 'تاریخ',
                                'enableSorting' => false,
                                'value'=> function ($model) {
                                    return $model->date;
                                },
                            ],
                        [

                            'header' => 'زمان',
                            'enableSorting' => false,
                            'value'=> function ($model) {
                                return $model->time;
                            },
                        ],
                        [

                            'header' => 'توضیحات',
                            'enableSorting' => false,
                            'value'=> function ($model) {
                                return $model->description;
                            },
                        ],



                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
