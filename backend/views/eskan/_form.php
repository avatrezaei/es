<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Eskan */
/* @var $form yii\widgets\ActiveForm */
use kartik\select2\Select2Asset;
use backend\models\Eskan;
use backend\models\Place;
use backend\models\User;
use backend\models\Floor;
use backend\models\Suite;
use backend\models\Rom;
//use backend\modules\YumUsers\models\UsersType;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use ibrarturi\latlngfinder\LatLngFinder;
//use backend\models\FieldsType;
use yii\web\JsExpression;
use yii\web\View;
use kartik\file\FileInput;
//use backend\models\SportLocation;
use yii\base\Widget;
//use backend\models\Fields;
//use backend\models\SportLocationField;

//use faravaghi\jalaliDatePicker\jalaliDatePicker;
//use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
 
 
 
 $Place = Place::find()->all();
$listData=ArrayHelper::map($Place,'id','name');


 $person = User::find()->students()->all();
//$listData1=ArrayHelper::map($person,'id','firstname');


$listData1=ArrayHelper::map($person,'id',function($model, $defaultValue) {
        return $model['name'].' '.$model['last_name'].' ('.$model['student_number'].')';
    });

?>

<div class="eskan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field(
        $model,
        'room_id',
        [
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ]
    )
        ->textInput(
            [
                'maxlength' => 52,
                'class' => $inputClass,
            ]
        )
        ->label(NULL, ['class'=>$labelClass]); ?>

    <?= $form->field(
        $model,
        'PersonID',
        [
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ]
    )
        ->textInput(
            [
                'maxlength' => 52,
                'class' => $inputClass,
            ]
        )
        ->label(NULL, ['class'=>$labelClass]); ?>
    <?php
    $model->entry_date =$model->dateDisplayFormat();
    ?>
    <?=$form->field ( $model, 'entry_date',
        [ 'options' => [ 'class' => $formGroupClass,
            //'value' => $model->start,
        ],
            'template' => $template
        ]
    )->widget ( jalaliDatePicker::className (), [ 'options' => ['format' => 'yyyy/mm/dd','viewformat' => 'yyyy/mm/dd','placement' => 'right','todayBtn' => 'linked','id' => 'event-birthDate','class' => 'form-control'  ] ] )->label ( NULL, [ 'class' => $labelClass ] )?>
    <?= $form->field(
        $model,
        'year',
        [
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ]
    )
        ->textInput(
            [
                'maxlength' => 52,
                'class' => $inputClass,
            ]
        )
        ->label(NULL, ['class'=>$labelClass]); ?>
    <?= $form->field(
        $model,
        'term',
        [
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ]
    )
        ->textInput(
            [
                'maxlength' => 52,
                'class' => $inputClass,
            ]
        )
        ->label(NULL, ['class'=>$labelClass]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'تحویل اتاق', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
