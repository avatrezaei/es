<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EskanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Eskans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eskan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Eskan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
           // 'room_id',
            [
                'attribute' => 'room_id',
                'label'=>'آدرس اتاق',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'200px',],
                'value'=>function($model){
                  return  $model->getAddress($model->room_id);
                },

            ],
            [
                'attribute' => 'PersonID',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'200px',],
                'value'=>function($model){
                    return  $model->user->name.' '.$model->user->last_name;
                },

            ],
            [
                'label'=>'شماره دانشجویی',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'100px',],
                'value'=>function($model){
                    return  $model->user->student_number;
                },
            ],
            [
                'attribute' => 'term',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'100px',],
            ],
            [
                'attribute' => 'entry_date',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'150px',],
            ],
            [
                'attribute' => 'exit_date',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'150px',],
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                ]


        ],
    ]); ?>
</div>
