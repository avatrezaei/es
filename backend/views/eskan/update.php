<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Eskan */

$this->title = Yii::t('app', 'تحویل اتاق') . ' ' . $model->room_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Eskans'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->room_id, 'url' => ['view', 'id' => $model->room_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="station-update">
    <div class="row">
        <div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
