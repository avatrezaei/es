<?php

use backend\assets\ToastrAsset;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//ToastrAsset::register($this);

$this->title = Yii::t('app', 'Eskans');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roomsa'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
    [
        'label' => '<i class="ace-icon fa bigger-120 "></i> ' . Yii::t('app', 'Form Download'),
        'url' => \yii\helpers\Url::home(true).'eskan_file/eskans.pdf'
    ]
];


$this->registerJs(
    <<<SCRIPT
    	$('#caravanid').on('keyup change', function() {
		table.columns(6).search($(this).find(':selected').val()).draw();
	});
	$('#placeid').on('keyup change', function() {
		table.columns(1).search($(this).find(':selected').val()).draw();
	});
	$('#roleid').on('keyup change', function() {
		table.columns(7).search($(this).find(':selected').val()).draw();
	});
	$('body').on('click','.status',	function(){
		var a = $(this);
		var response = null;

		jQuery.ajax({
			type:'GET',
			url:a.attr('href'),
			cache:false,
			'success':function(data){
				var obj = jQuery.parseJSON(data);
				response = obj;
				
				toastr.options = {
					'closeButton': true,
					'positionClass': "toast-top-right",
				}
				toastr[response.message.search("!") != -1 ? 'error' : 'success'](response.message, 'updateText')
			},
			'complete': function(){
				if(response != null)
				{
					a.attr('data-hint', response.hint)
					a.html(response.content);

					if(response.enable)
					{
						a.removeClass('hint--error');
						a.addClass('hint--success');
					}
					else
					{
						a.removeClass('hint--success');
						a.addClass('hint--error');
					}
				}
			}
		});
		return false;
	});
SCRIPT
    , yii\web\View::POS_END);

?>
<div class="user-index">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-dark hidden-xs">
                <i class="icon-user font-dark"></i>
                <span class="caption-subject bold uppercase"><?= $this->title ?></span>
            </div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body">
            <?= $widget->run() ?>
        </div>
    </div>
</div>