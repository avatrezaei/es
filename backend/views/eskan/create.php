<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Eskan */

$this->title = 'Create Eskan';
$this->params['breadcrumbs'][] = ['label' => 'Eskans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eskan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
