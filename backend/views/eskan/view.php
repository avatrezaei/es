<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\Eskan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Eskans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$detalView = '<div class="row static-info"><div class="col-md-3 name"> {label}: </div><div class="col-md-5 value"> {value} </div></div>';
?>

<?php if (Yii::$app->request->isAjax): ?>
    <div class="modal-header" id="modalHeader">
        <button aria-hidden="true" data-dismiss="modal" class="close"
                type="button">&times;</button>
    </div>
<?php endif; ?>
<div class="request-view">
    <div class="profile-content">

        <div class="row">

            <div class="col-md-5">

                <div class="portlet light ">
                    <?php
                    $detalView  = '<div class="row static-info"><div class="col-md-4 name"> {label}: </div><div class="col-md-8 col-sm-8 col-xs-8 value"> {value} </div></div>';
                    ?>


                        <?= DetailView::widget ( [
                            'model' => $model,
                            'template' => $detalView,
                            'options' => [
                                'tag' => 'div'
                            ],
                                'attributes' => [
                            //'id',
                            //'room_id',
//            [
//                'attribute' => 'room_id',
//                'value' => $model->getAddress($model->room_id),
//                'value' => 'iiiiiiiiiiiiiii',
//            ],
                            [
                                'label' => 'آدرس اتاق',
                                'value' => $model->getAddress($model->room_id),
                                // 'value' => 'iiiiiiiiiiiiiii',
                            ],
                            [
                                'attribute' => 'PersonID',
                                'value' => $model->user->name.' '.$model->user->last_name,
                            ],
                            [
                                'label' => 'شماره دانشجویی',
                                'value' => $model->user->student_number,
                            ],
                            'year',
                            'term',
                            'entry_date',
                            'exit_date',
                             [
                                 'attribute' => 'delivery',
                                 'value'=> $model->getState(),

                             ],
                           // 'delivery',
                        ],

                        ]);


                        ?>
                        <p>
                            <?= Html::a('تحویل اتاق', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        </p>

                    </div></div>
            <div class="col-md-5">
                <div class="portlet light">
                    <?php
                    $user=$model->user->username;
                    $path=Yii::$app->request->BaseUrl.'/../uploads/'. $user.'_image.jpg';
                    //if(file_exists($path)) {
                    //echo $path;
                    echo Html::img($path, [
                        "height" => "200px",
                        "width" => "200px",
                        "class" => 'profile-userpic profile-oldpic',
                    ]);
                    ?>
                </div>
                    </div>
                </div>

            </div>
        </div>

