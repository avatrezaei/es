<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Familyform */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params['formGroupClass'];
$inputClass = Yii::$app->params['inputClass'];
$errorClass = Yii::$app->params['errorClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$labelClass = 'col-md-3';


?>

<div class="familyform-form">

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">
        <?php
        ?>
		<?php
       $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
     $model->user_id=$curentUserId;
//        $nn = $_POST[$curentUserId];
       // $post = $request->post($curentUserId);


        $form->field(
				$model,
				'user_id',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->hiddenInput();
		?>
        <input id="familyform-user_id" class="form-control input-sm" name="Familyform[user_id]" maxlength="20" type="hidden" value="<?=$curentUserId;?>">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'FName',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'LName',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div></div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'relation',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    echo $form->field(
				$model,
				'RelationType',
                        [
                            'options' => ['class' => $formGroupClass],
                            'template' => $template
                        ])
                        ->dropDownList([ '1' => 'بستگانی که دانشجو میتواند به خانه آنها برود', '2' => 'بستگانی که میتوانند به ملاقات دانشجو بیایند'], ['prompt' => ''])->label(NULL, ['class'=>$labelClass]);
                    ?>
                </div></div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'job',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
                        </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'phone',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
                                </div></div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">

		<?php
			echo $form->field(
				$model,
				'address',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
        <?php
			echo $form->field(
				$model,
				'mobile',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
                </div></div>
<!--		--><?php
//			echo $form->field(
//				$model,
//				'AttachStatus')->dropDownList([ 'FINAL' => 'FINAL', 'VALID' => 'VALID', 'INVALID' => 'INVALID', ], ['prompt' => '']);
//		?>

	</div>

    <div class="margiv-top-10">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>

	</div>

	<?php ActiveForm::end(); ?>

</div>
