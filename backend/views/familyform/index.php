<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FamilyformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Familyforms');
$this->params['page_title'] = false;
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'][] = [
	'label' => Yii::t('app', 'Create Familyform'),
	'url' => ['create']
];
?>
<div class="familyform-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-user font-dark"></i>
				<span class="caption-subject bold uppercase"><?= Html::encode($this->title) ?></span>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">

            <?=  $widget->run() ?>
		</div>
	</div>
</div>
