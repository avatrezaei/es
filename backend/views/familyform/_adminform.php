<?php

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Familyform */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params['formGroupClass'];
//$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
//$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];


//$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3';
//$errorClass = Yii::$app->params ['errorClass'];
$floor = \backend\models\User::find()->andWhere(['type'=>0])->all();
$listData=ArrayHelper::map($floor,'id',function($model, $defaultValue) {
    //$caravan=Place::find()->andWhere(['id'=>$model['parent_id']])->one();
    return $model['name'].' '.$model['last_name'].'-'.$model['student_number'];
});

?>

<div class="familyform-form">

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">
        <?php
        ?>
		<?php
       $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
     $model->user_id=$curentUserId;
		?>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
        <?php
        echo $form->field($model, 'user_id',[
            'options' => ['class' => $formGroupClass],
            'template' => $template
        ])
            ->widget(Select2::classname(), [
                'data' => $listData,
                'language' => 'fa-IR',
                'options' => ['placeholder' => 'دانشجوی مورد نظر را انتخاب نمایید'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents'=> [
                    "change" => "function() { var url = '".Url::toRoute("familyform/admincreate?user_id=")."'; ".
                                            " window.location.href = url+document.getElementById('familyform-user_id').value; }",
                ],
                //window.location.href = \"".Url::toRoute('default/over-write?id=document.getElementById(\'familyform-user_id\').value;')."\
            ])->label(NULL, ['class'=>$labelClass]);
        ?>
            </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
        <?php
        echo $form->field(
            $model,
            'AttachStatus',
            [
                'options' => ['class' => $formGroupClass],
                'template' => $template
            ])
            ->dropDownList([ 'NULL' => 'ذخیره اولیه توسط دانشجو','FINAL'=>'نهایی شده توسط دانشجو','VALID'=>'تائید مدیریت'], ['prompt' => ''])->label(NULL, ['class'=>$labelClass]);
        ?>
                </div></div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'FName',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'LName',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div></div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'relation',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    echo $form->field(
				$model,
				'RelationType',
                        [
                            'options' => ['class' => $formGroupClass],
                            'template' => $template
                        ])
                        ->dropDownList([ '1' => 'بستگانی که دانشجو میتواند به خانه آنها برود', '2' => 'بستگانی که میتوانند به ملاقات دانشجو بیایند'], ['prompt' => ''])->label(NULL, ['class'=>$labelClass]);
                    ?>
                </div></div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'job',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
                        </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
				$model,
				'phone',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
                                </div></div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">

		<?php
			echo $form->field(
				$model,
				'address',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
            </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
        <?php
			echo $form->field(
				$model,
				'mobile',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
                </div></div>


	</div>

    <div class="margiv-top-10">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>

	</div>

	<?php ActiveForm::end(); ?>

</div>
