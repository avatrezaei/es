<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Familyform */
$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
$this->title = Yii::t('app', 'Family form management');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Familyforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="familyform-create">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
					<?= $this->render('_adminform', [
						'model' => $model,
					]) ?>
				</div>
                <?php
                if(isset($_GET['user_id'])) {
                    echo '<script>document.getElementById("familyform-user_id").value='.$_GET['user_id'].';</script>';

                    $query = \app\models\Familyform::find();
                    $query1 = \app\models\Familyform::find();

                    // add conditions that should always apply here

                    $dataProvider = new \yii\data\ActiveDataProvider([
                        'query' => $query,
                    ]);
                    $dataProvider1 = new \yii\data\ActiveDataProvider([
                        'query' => $query1,
                    ]);

                    // grid filtering conditions

                    $query->andFilterWhere([
                        'RelationType' => 1,
                        'user_id' => $_GET['user_id'],
                        //'exit_date'=>'0000-00-00',
                    ]);
                    $query1->andFilterWhere([
                        'RelationType' => 2,
                        'user_id' => $_GET['user_id'],
                        //'exit_date'=>'0000-00-00',
                    ]);
                    $searchModel = new \app\models\FamilyformSearch();


                    ?>
                    <h4 style="color:#0000C0 ">بستگانی که دانشجویان میتوانند به خانه آنها بروند</h4>

                    <?= \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'header' => Yii::t('app', 'دانشجو'),
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->user->name . ' ' . $model->user->last_name;
                                },
                            ],
                            [

                                'header' => 'نام',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->FName . ' ' . $model->LName;
                                },
                            ],
                            [

                                'header' => 'نسبت',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->relation;
                                },
                            ],
//                            [
//
//                                'header' => 'نوع نسبت',
//                                'enableSorting' => false,
//                                'value' => function ($model) {
//                                    return $model->getrel($model->RelationType);
//                                },
//                            ],
//
                            [

                                'header' => 'شغل',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->job;
                                },
                            ],
                            [

                                'header' => 'آدرس',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->address . '-' . $model->phone;
                                },
                            ],
                            [

                                'header' => 'موبایل',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->mobile;
                                },
                            ],
                            [

                                'header' => 'وضعیت فرم',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->getState($model->AttachStatus);
                                },
                            ],
//
                            [
                                'header' => 'عملیات',
                                'headerOptions' => ['width' => '70px'],
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{updateeskan}{confirm}{reject}',
                                'buttons' => [

                                    'updateeskan' => function ($url, $model) {
                                        $_send = Html::a(
                                            '<i class="fa fa-pencil font-blue"></i>',
                                            \yii\helpers\Url::to(['/familyform/adminupdate', 'id' => $model->id]),
                                            [
                                                'class' => 'hint--top hint--rounded hint--info',
                                                'data-hint' => Yii::t('app', 'بروزرسانی رکورد'),
                                            ]
                                        );

                                        return $_send;
                                    },
                                    'confirm' => function ($url, $model) {
                                        $_send1 = Html::a(
                                            '<i class="fa fa-check-square-o font-green"></i>',
                                            \yii\helpers\Url::to(['/familyform/confirm', 'id' => $model->id]),
                                            [
                                                'class' => 'hint--top hint--rounded hint--info',
                                                'data-hint' => Yii::t('app', 'تائید'),
                                            ]
                                        );

                                        return $_send1;
                                    },
                                    'reject' => function ($url, $model) {
                                        $_send = Html::a(
                                            '<i class="fa fa-close font-red"></i>',
                                            \yii\helpers\Url::to(['/familyform/reject', 'id' => $model->id]),
                                            [
                                                'class' => 'hint--top hint--rounded hint--info',
                                                'data-hint' => Yii::t('app', 'رد'),
                                            ]
                                        );

                                        return $_send;
                                    },
                                ]
                            ],


                        ],
                    ]);
                ?>
                <br>
            <h4 style="color:#0000C0 ">بستگانی که میتوانند به ملاقات دانشجویان بیایند</h4>
                    <?= \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider1,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'header' => Yii::t('app', 'دانشجو'),
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->user->name . ' ' . $model->user->last_name;
                                },
                            ],
                            [

                                'header' => 'نام',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->FName . ' ' . $model->LName;
                                },
                            ],
                            [

                                'header' => 'نسبت',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->relation;
                                },
                            ],
//                            [
//
//                                'header' => 'نوع نسبت',
//                                'enableSorting' => false,
//                                'value' => function ($model) {
//                                    return $model->getrel($model->RelationType);
//                                },
//                            ],
//
                            [

                                'header' => 'شغل',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->job;
                                },
                            ],
                            [

                                'header' => 'آدرس',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->address . '-' . $model->phone;
                                },
                            ],
                            [

                                'header' => 'موبایل',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->mobile;
                                },
                            ],
                            [

                                'header' => 'وضعیت فرم',
                                'enableSorting' => false,
                                'value' => function ($model) {
                                    return $model->getState($model->AttachStatus);
                                },
                            ],
//
                            [
                                'header' => 'عملیات',
                                'headerOptions' => ['width' => '70px'],
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{updateeskan}{confirm}{reject}',
                                'buttons' => [

                                    'updateeskan' => function ($url, $model) {
                                        $_send = Html::a(
                                            '<i class="fa fa-pencil font-blue"></i>',
                                            \yii\helpers\Url::to(['/familyform/adminupdate', 'id' => $model->id]),
                                            [
                                                'class' => 'hint--top hint--rounded hint--info',
                                                'data-hint' => Yii::t('app', 'بروزرسانی رکورد'),
                                            ]
                                        );

                                        return $_send;
                                    },
                                    'confirm' => function ($url, $model) {
                                        $_send1 = Html::a(
                                            '<i class="fa fa-check-square-o font-green"></i>',
                                            \yii\helpers\Url::to(['/familyform/confirm', 'id' => $model->id]),
                                            [
                                                'class' => 'hint--top hint--rounded hint--info',
                                                'data-hint' => Yii::t('app', 'تائید'),
                                            ]
                                        );

                                        return $_send1;
                                    },
                                    'reject' => function ($url, $model) {
                                        $_send = Html::a(
                                            '<i class="fa fa-close font-red"></i>',
                                            \yii\helpers\Url::to(['/familyform/reject', 'id' => $model->id]),
                                            [
                                                'class' => 'hint--top hint--rounded hint--info',
                                                'data-hint' => Yii::t('app', 'رد'),
                                            ]
                                        );

                                        return $_send;
                                    },
                                ]
                            ],


                        ],
                    ]);
                }?>
			</div>
		</div>
	</div>
</div>
