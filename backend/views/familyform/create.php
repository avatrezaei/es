<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Familyform */
$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
$this->title = Yii::t('app', 'Create Familyform');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Familyforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="familyform-create">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">

                    <div class="col-lg-12">
                        <?php
                        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                           //echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
                        }
                        ?>
                    </div>
					<?= $this->render('_form', [
						'model' => $model,
					]) ?>
				</div>
                <?php

                $query = \app\models\Familyform::find();
                $query1 = \app\models\Familyform::find();

                // add conditions that should always apply here

                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query' => $query,
                ]);
                $dataProvider1 = new \yii\data\ActiveDataProvider([
                    'query' => $query1,
                ]);

                // grid filtering conditions
                $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
                $query->andFilterWhere([
                    'user_id' => $curentUserId,'RelationType'=>1,
                    //'exit_date'=>'0000-00-00',
                ]);
                $query1->andFilterWhere([
                    'user_id' => $curentUserId,'RelationType'=>2,
                    //'exit_date'=>'0000-00-00',
                ]);
                $searchModel = new \app\models\EskanSearch();


                ?>
                <h4 style="color:#0000C0 ">بستگانی که دانشجو میتواند به خانه آنها برود</h4>
                <?= \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [

                            'header' => 'نام',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->FName;
                            },
                        ],
                        [

                            'header' => 'نام خانوادگی',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->LName;
                            },
                        ],
                        [

                            'header' => 'نسبت',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->relation;
                            },
                        ],
//                        [
//
//                            'header' => 'نوع نسبت',
//                            'enableSorting' => false,
//                            'value' => function ($model) {
//                                return $model->getrel($model->RelationType);
//                            },
//                        ],
                        [

                            'header' => 'شغل',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->job;
                            },
                        ],
                        [

                            'header' => 'تلفن',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->phone;
                            },
                        ],
                        [

                            'header' => 'آدرس',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->address;
                            },
                        ],
                        [

                            'header' => 'موبایل',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->mobile;
                            },
                        ],
                        [

                            'header' => 'وضعیت فرم',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->getState($model->AttachStatus);
                            },
                        ],
//
                        [
                            'header' => 'عملیات',
                                'class' => 'yii\grid\ActionColumn',
                            'template' => '{deleteeskan}{updateeskan}',
                            'buttons' => [

                                'deleteeskan' => function ($url, $model){

                                    $_send = Html::a(
                                        '<i class="fa fa-trash font-blue"></i>',
                                        \yii\helpers\Url::to(['/familyform/delete', 'id' => $model->id]),
                                        [
                                            'class' => 'hint--top hint--rounded hint--info',
                                            'data-hint' => Yii::t('app', 'حذف رکورد'),
                                        ]
                                    );

                                    return $_send;
                                },

                                'updateeskan' => function ($url, $model){
                                    $_send1 = Html::a(
                                        '<i class="fa fa-pencil font-blue"></i>',
                                        \yii\helpers\Url::to(['/familyform/update', 'id' => $model->id]),
                                        [
                                            'class' => 'hint--top hint--rounded hint--info',
                                            'data-hint' => Yii::t('app', 'بروزرسانی رکورد'),
                                        ]
                                    );

                                    return  $_send1;
                                },


                            ],
                        ],


                    ],
                ]); ?>

                <br>
            <h4 style="color:#0000C0 ">بستگانی که میتوانند به ملاقات دانشجو بیایند</h4>
                <?= \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider1,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [

                            'header' => 'نام',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->FName;
                            },
                        ],
                        [

                            'header' => 'نام خانوادگی',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->LName;
                            },
                        ],
                        [

                            'header' => 'نسبت',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->relation;
                            },
                        ],
//                        [
//
//                            'header' => 'نوع نسبت',
//                            'enableSorting' => false,
//                            'value' => function ($model) {
//                                return $model->getrel($model->RelationType);
//                            },
//                        ],
                        [

                            'header' => 'شغل',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->job;
                            },
                        ],
                        [

                            'header' => 'تلفن',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->phone;
                            },
                        ],
                        [

                            'header' => 'آدرس',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->address;
                            },
                        ],
                        [

                            'header' => 'موبایل',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->mobile;
                            },
                        ],
                        [

                            'header' => 'وضعیت فرم',
                            'enableSorting' => false,
                            'value' => function ($model) {
                                return $model->getState($model->AttachStatus);
                            },
                        ],
//
                        [
                            'header' => 'عملیات',
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{deleteeskan}{updateeskan}',
                            'buttons' => [

                                'deleteeskan' => function ($url, $model){
                                    $_send = Html::a(
                                        '<i class="fa fa-trash font-blue"></i>',
                                        \yii\helpers\Url::to(['/familyform/delete', 'id' => $model->id]),
                                        [
                                            'class' => 'hint--top hint--rounded hint--info',
                                            'data-hint' => Yii::t('app', 'حذف رکورد'),
                                        ]
                                    );

                                    return $_send;
                                },
                                'updateeskan' => function ($url, $model){
                                    $_send1 = Html::a(
                                        '<i class="fa fa-pencil font-blue"></i>',
                                        \yii\helpers\Url::to(['/familyform/update', 'id' => $model->id]),
                                        [
                                            'class' => 'hint--top hint--rounded hint--info',
                                            'data-hint' => Yii::t('app', 'بروزرسانی رکورد'),
                                        ]
                                    );

                                    return  $_send1;
                                },


                            ],






                        ],


                    ],
                ]); ?>
                <?=  Html::a('نهایی شدن', ['/familyform/accept'], ['name'=>'image','class' => 'btn btn-primary']);?>
			</div>
		</div>
	</div>
</div>
