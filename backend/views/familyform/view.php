<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Familyform */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Familyforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Update'),
		'url' => ['update1', 'id' => $model->id]
	],
];

?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="familyform-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div>

						<?= DetailView::widget([
							'model' => $model,
							'template' => Yii::$app->params['detalView'],
							'attributes' => [
                                [
                                    'attribute' => 'user_id',
                                    'value' => $model->user->name.' '.$model->user->last_name,
                                ],
								'FName',
								'LName',
								'relation',
								'job',
								'phone',
								'address',
								//'RelationType',
                                [
                                    'attribute' => 'RelationType',
                                    'value' =>   $model->getrel($model->RelationType),
                                ],

                                'mobile',
								//'AttachStatus',
                                [
                                    'attribute' => 'AttachStatus',
                                    'value' => $model->getstate($model->AttachStatus),
                                ],
							],
						]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>