<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Suite;
use backend\models\Floor;
use backend\models\Place;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model backend\models\Suite */
/* @var $form yii\widgets\ActiveForm */
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];


$suite = Suite::find()->all();
$listData=ArrayHelper::map($suite,'id','name');


$suite = Suite::find()->all();
$listData=ArrayHelper::map($suite,'id',function($model, $defaultValue) {
	$caravan=Floor::find()->andWhere(['id'=>$model['parent_id']])->one();
	$caravan1=Place::find()->andWhere(['id'=>$caravan['parent_id']])->one();
        return $model['name'].'-'.$caravan['name'].'-'.$caravan1['name'];
    });
?>

<div class="suite-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

   <?php
				echo $form->field(
					$model,
					'id',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?= $form->field($model, 'parent_id',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList($listData,['prompt'=>'نام سوئیت مورد نظر را انتخاب نمایئید', 'id'=>'parent_id'])->label(NULL, ['class'=>$labelClass]); ?>

    <?php
				echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

    <?php
				echo $form->field(
					$model,
					'capacity',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
		 <?php echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
    ]);?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'ایجاد' : 'بروزرسانی', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
