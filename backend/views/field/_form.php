<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Field */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];

?>

<div class="field-form">

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">

		<?php
			echo $form->field(
				$model,
				'name',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
				$model,
				'ename',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

	</div>

	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>
