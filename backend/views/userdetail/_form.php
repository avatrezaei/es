<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserDetail */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];

?>

<div class="user-detail-form">

	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">

		<?php
			echo $form->field(
				$model,
				'user_id',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
				$model,
				'shift',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
				$model,
				'startingyear',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
				$model,
				'startingsemester',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
				$model,
				'units',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
				$model,
				'field',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
				$model,
				'major',
				[
					'options' => ['class' => $formGroupClass],
					'template' => $template
				]
			)
			->textInput(
				['maxlength' => true, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

	</div>

	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>
