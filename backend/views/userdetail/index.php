<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Details');
$this->params['page_title'] = false;
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'][] = [
	'label' => Yii::t('app', 'Create User Detail'),
	'url' => ['create']
];
?>
<div class="user-detail-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-user font-dark"></i>
				<span class="caption-subject bold uppercase"><?= Html::encode($this->title) ?></span>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'user_id',
			'shift',
			'startingyear',
			'startingsemester',
			// 'units',
			// 'field',
			// 'major',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
		</div>
	</div>
</div>
