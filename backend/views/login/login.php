<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use backend\assets\iCheckAsset;

//iCheckAsset::register($this);

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

//$this->title = Yii::t('app', 'Sign in to start your session');
/* ------------show flash messages----------------- */
//$session = Yii::$app->session;
// check the availability
//$result = $session->hasFlash('change_password_success');
// get and display the message
//echo $session->getFlash('change_password_success');
?>

		<h3 class="form-title font-green"><?php echo Yii::t('app', 'Sign in to start your session') ?></h3>
				
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span> Enter any username and password. </span>
		</div>

		<?php yii\widgets\Pjax::begin(['id' => 'login_pjax']) ?>

			<?php $form = ActiveForm::begin(['class' => 'login-form', 'id' => 'login-form', 'enableClientValidation' => false]); ?>
			<?=
				$form
				->field($model, 'username')
				->textInput([
					'placeholder' => Yii::t('app', 'Username'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'password')
				->passwordInput([
					'placeholder'=>Yii::t('app', 'password'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?php if ($model->scenario == 'withCaptcha' && Captcha::checkRequirements()): ?>
				<?=
					$form
					->field($model, 'verifyCode')
					->textInput([
						'placeholder'=>Yii::t('app', 'Enter Captcha Code'),
						'class'=>'form-control form-control-solid placeholder-no-fix',
						'autocomplete'=>'off',
					])
					->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
				?>
				<?php
					echo Captcha::widget([
						'model' => $model,
						'captchaAction'=> Url::to('/login/captcha'),
						'attribute' => 'verifyCode',
						'template' => '{image}'
					]);
					/*$this->widget('ext.RezvanCaptcha.RezvanCaptcha',array(
						'buttonOptions'=>array(
							'class'=>'width-45 pull-left btn btn-small btn-yellow'
						),
					));*/
				?>
			<?php endif; ?>
			<div class="form-group rem-field">
				<?php
					echo Html::activeCheckbox(
						$model,
						'rememberMe',
						[
							'label'=>Yii::t('app', 'Remember me'),
							'class' => 'icheck',
							'data-checkbox' => 'icheckbox_flat-blue',
							'labelOptions' => [
								'class' => 'rememberme check'
							]
						]
					);
				?>
			</div>
			<div class="form-actions pull-right">
				<?= Html::submitButton(Yii::t('app', 'login'), ['class' => 'btn green uppercase ', 'name' => 'login-button']) ?>
				<?= Html::a(Yii::t('app','Sign up'), [
					'/signup'], 
					['class' => 'btn green uppercase ',
				]) ?>
			</div>

			<div class="row">
				
			</div>

		<?php ActiveForm::end(); ?>
		<?php yii\widgets\Pjax::end() ?>
<p style="color: #9d1611">توجه: دانشجویان در قسمت نام کاربری، کد ملی خود را وارد نمایند.</p>
		<div class="create-account">
			<p>
				<a href="<?= \yii\helpers\Url::to(['/members/forget-password']) ?>"><?php echo Yii::t('app', 'I forgot my password'); ?></a><br>
			</p>
		</div>

		