<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $this         \yii\web\View
 * @var $searchModel  \dektrium\rbac\models\RuleSearch
 * @var $dataProvider \yii\data\ArrayDataProvider
 */

use kartik\select2\Select2;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\helpers\Html;
$this->title = Yii::t('app', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'][] = [
    'label' => Yii::t('app', 'Create Rule'),
    'url' => ['create']
];
?>

<div class="send-messsage-index">
    <div class="col-lg-12 col-md-12  ">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green">
                    <span class="caption-subject bold">
                        <i class="icon-envelope-open"></i>
                        <?= Html::encode($this->title) ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                 
<?php Pjax::begin() ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'layout'       => "{items}\n{pager}",
    'columns'      => [
        [
            'attribute' => 'name',
            'label'     => Yii::t('app', 'Name'),
            'options'   => [
                'style' => 'width: 20%'
            ],
            'filter' => Select2::widget([
                'model'     => $searchModel,
                'attribute' => 'name',
                'options'   => [
                    'placeholder' => Yii::t('app', 'Select rule'),
                ],
                'pluginOptions' => [
                    'ajax' => [
                        'url'      => Url::to(['search']),
                        'dataType' => 'json',
                        'data'     => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'allowClear' => true,
                    
                ],
            ]),
        ],
        [
            'attribute' => 'class',
            'label'     => Yii::t('app', 'Class'),
            'value'     => function ($row) {
                $rule = unserialize($row['data']);

                return get_class($rule);
            },
            'options'   => [
                'style' => 'width: 20%'
            ],
        ],
        [
            'attribute' => 'created_at',
            'label'     => Yii::t('app', 'Created at'),
            'format'    => 'datetime',
            'options'   => [
                'style' => 'width: 20%'
            ],
        ],
        [
            'attribute' => 'updated_at',
            'label'     => Yii::t('app', 'Updated at'),
            'format'    => 'datetime',
            'options'   => [
                'style' => 'width: 20%'
            ],
        ],
        [
            'class'      => ActionColumn::className(),
            'template'   => '{update} {delete}',
            'urlCreator' => function ($action, $model) {
                return Url::to(['/rule/' . $action, 'name' => $model['name']]);
            },
            'options'   => [
                'style' => 'width: 5%'
            ],
        ]
    ],
]) ?>

<?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>


 
