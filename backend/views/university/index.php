<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'University');
$this->params['page_title'] = false;

$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Create University'),
		'url' => ['create'],
		'options' => [
			'class' => 'btn btn-success'
		]
	]
];

?>
<div class="user-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-graduation font-dark"></i>
				<span class="caption-subject bold uppercase"><?= $this->title ?></span>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">
			<?= $widget->run() ?>
		</div>
	</div>
</div>