<?php
 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;
use yii\web\View;
use yii\web\JsExpression;

use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

use backend\models\University;
use backend\models\Province;
use backend\models\Event;
use app\models\Language;

/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];

 
 
$setState = '';
$setCity = '';

if(!$model->isNewRecord){
	$setState = "$('#university-provinceid').trigger('change');";
	$setCity = "$('#university-cityid').val(" . $model->cityId . ").trigger('change');";
}

 
?>
 
	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">

		<?php
			echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
					$model,
					'eName',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>

		<?php 
			echo $form->field(
					$model, 
					'universityType',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => University::itemAlias('Type'),
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Please select University Type'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
		?>

		<?php
		echo $form->field(
			$model,
			'level',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => University::itemAlias('Level'),
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Please select University Level'),
					'dir' => 'rtl',
				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>		
		
		<?php
			echo $form->field(
					$model,
					'preffixName',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass])
				->hint(Yii::t('app', 'English abbreviations such an expression between 3 and 10 characters, example: tu'));
		?>

		 

		 

		<?php
			echo $form->field(
					$model,
					'email',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
					$model,
					'url',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
					$model,
					'phone',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
					$model,
					'fax',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>

		<?php
			echo $form->field(
					$model,
					'address',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
	</div>

	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn green'])	?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>