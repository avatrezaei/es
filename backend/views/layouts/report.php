<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!--basic styles-->
		<link href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/fa-font-awesome<?php echo (YII_DEBUG ? '' : '.min'); ?>.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/css.css" />

		<!--ace styles-->
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/ace<?php echo (YII_DEBUG ? '' : '.min'); ?>.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/ace-responsive<?php echo (YII_DEBUG ? '' : '.min'); ?>.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/ace-skins<?php echo (YII_DEBUG ? '' : '.min'); ?>.css" />

		<!-- main styles -->
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/default.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/font.css" />

			
		<!-- notifications -->
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/lib/sticky/sticky.css" />
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/jquery.gritter.css" />
		<!-- tooltips-->
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/lib/qtip2/jquery.qtip<?php echo (YII_DEBUG ? '' : '.min'); ?>.css" />
		<!-- hint.css -->
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/lib/hint_css/hint<?php echo (YII_DEBUG ? '' : '.min'); ?>.css" />

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/favicon.ico" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css/ace-ie.min.css" />
		<![endif]-->

		<!--inline styles related to this page-->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>

	<body>
		<!-- header -->
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container-fluid">
				<?php echo CHtml::link('<i class="icon-leaf"></i>&nbsp; '.Yii::app()->params['company'], 'http://www.dkr.co.ir', array('class'=>'brand company', 'target'=>'_blank')); ?>
				<!--/.brand-->
				</div><!--/.container-fluid-->
			</div><!--/.navbar-inner-->
		</div>

		<!-- main content -->
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="main-content" style="margin-right:0px;">
				<div class="page-content">
					<!--PAGE CONTENT BEGINS-->
					<?php echo $content; ?>
					<!--PAGE CONTENT ENDS-->
				</div><!--/.page-content-->
			</div><!--/.main-content-->
			
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery-10.min.js"></script>

		<!--<![endif]-->

		<!--[if IE]>
		<script src="<script src='<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery.min.js"></script>
		<![endif]-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery-ui-1.10.3.custom<?php echo (YII_DEBUG ? '' : '.min'); ?>.js"></script>

		<!--ace scripts-->

		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/ace-elements<?php echo (YII_DEBUG ? '' : '.min'); ?>.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/ace<?php echo (YII_DEBUG ? '' : '.min'); ?>.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/bootbox.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery-migrate.js"></script>

		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
		
		<!-- sticky messages -->
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/lib/sticky/sticky.min.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery.gritter.min.js"></script>
		<!-- tooltips -->
		<script src="<?php echo Yii::app()->theme->baseUrl;?>/lib/qtip2/jquery.qtip<?php echo (YII_DEBUG ? '' : '.min'); ?>.js"></script>
		
		<script>
			switch_direction()
		</script>

		<!--inline scripts related to this page-->
	</body>
</html>		