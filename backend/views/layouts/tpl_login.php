<?php
use yii\helpers\Url;
use yii\helpers\Html;
use backend\assets\LoginAsset;
use backend\assets\LoginIEAsset;

/* @var $this \yii\web\View */
/* @var $content string */

/**
 * Custome Asset:
 */
LoginAsset::register($this);
LoginIEAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>" dir="rtl">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<?php echo Html::csrfMetaTags(); ?>
		<title><?php echo Yii::$app->name . ' - ' . Html::encode($this->title) ?></title>
		<?php $this->head(); ?>

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo \Yii::$app->request->BaseUrl;?>/rezvan/apps/img/favicon.ico" />
	</head>
	<body class=" login">
		<?php $this->beginBody(); ?>
		<!-- BEGIN LOGO -->
		
		<!-- END LOGO -->
		<!-- BEGIN LOGIN -->
        <div class="content">
			<?php echo $content; ?>
		</div>
		<div class="copyright">
			2017 &copy; 
			
				<?php echo Yii::t('app','Bonyad');?>
			
		</div>

		<?php $this->endBody(); ?>
	</body>
</html>
<?php $this->endPage(); ?>
