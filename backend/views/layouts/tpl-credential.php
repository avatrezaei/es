<?php
use backend\assets\LoginAsset;
use backend\assets\LoginIEAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

/**
 * Custome Asset:
 */
LoginAsset::register($this);
LoginIEAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>" dir="rtl">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<?php echo Html::csrfMetaTags(); ?>
		<title><?php echo Yii::$app->name . ' - ' . Html::encode($this->title) ?></title>
		<?php $this->head(); ?>

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo \Yii::$app->request->BaseUrl;?>/favicon.ico" />
	</head>
	<body class=" login">
		<?php $this->beginBody(); ?>
		<!-- BEGIN LOGO -->
		<div class="logo">
			<a href="index.html">
				<img src="<?= \Yii::$app->request->BaseUrl ?>/images/logo.png" alt="" />
			</a>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN LOGIN -->
		<div class="content with-loyout">
			<?php echo $content; ?>
		</div>
		<div class="copyright"> 2016 © <?= Yii::$app->params['concessionaire'] ?>. </div>

		<?php $this->endBody(); ?>
	</body>
</html>
<?php $this->endPage(); ?>
