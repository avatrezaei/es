<?php
use backend\assets\PDFAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

PDFAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<title><?php echo Yii::$app->name . ' - ' . Html::encode($this->title) ?></title>
		<?php $this->head(); ?>
		<style type="text/css">
		/**
		 * Print Page
		 */
		.print-page {
			width: 210mm;
			min-height: 297mm;
			/*padding: 5mm;*/
			/*margin: 5mm auto;*/
			background: white;
			color: #000;
		}

		.person-card{
			width: 105mm;
			height: 148mm;
			float: right;
			padding: 5px;
			position: relative;
		}

		.person-card-back{
			width: 105mm;
			height: 148mm;
			float: right;
			padding: 5px;
			position: relative;
		}

		.person-card .background{
			text-align: left;
		}

		.person-title {
			font-family: "BZarBold";
			font-size: 28px;
			height: 70px;
			line-height: 30px;
			position: absolute;
			right: 140px;
			text-align: center;
			top: 215px;
			width: 160px;
		}

		.info{
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
			padding: 5px;
		}

		.info .picture {
			height: 110px;
			position: absolute;
			right: 39px;
			top: 178px;
			width: 92px;
		}

		.info-box {
			font-family: "Yekan";
			font-size: 14px;
			font-weight: bold;
			height: 30px;
			line-height: 30px;
			min-width: 50px;
			position: absolute;
		}

		.user-name {
			right: 137px;
			top: 307px;
		}

		.user-unique {
			font-size: 26px;
			right: 135px;
			top: 461px;
		}

		.user-family {
			right: 105px;
			top: 267px;
		}

		.user-university{
			right: 103px;
			top: 349px;
		}

		.user-field{
			right: 120px;
			top: 389px;
		}

		.user-field2{
			right: 112px;
			top: 350px;
		}

		.user-title{
			right: 80px;
			top: 389px;
		}

		.user-title2 {
			right: 50px;
			text-indent: 26px;
			top: 349px;
			width: 242px;
		}

		.event-time{
			right: 70px;
			top: 400px;
		}

		.event-place{
			right: 70px;
			top: 444px;
		}

		.qr-code {
			position: absolute;
			right: 32px;
			top: 436px;
			width: 75px;
		}

		@page {
			size: A4;
			margin: 5mm;
		}

		@media print {
			html, body {
				width: 210mm;
				height: 297mm;
			}
			.print-page {
				margin: 0;
				border: initial;
				border-radius: initial;
				width: initial;
				min-height: initial;
				box-shadow: initial;
				background: initial;
				page-break-after: always;
			}
		}

		</style>
	</head>
	<!-- END HEAD -->

	<body style="direction: rtl;">
		<!-- BEGIN HEADER -->
		<?php $this->beginBody(); ?>

		<?php echo $content; ?>

		<?php $this->endBody(); ?>
	</body>
</html>
<?php $this->endPage(); ?>
