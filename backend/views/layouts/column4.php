<?php
use backend\assets\PDFAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

PDFAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<title><?php echo Yii::$app->name . ' - ' . Html::encode($this->title) ?></title>
		<?php $this->head(); ?>
	</head>
	<!-- END HEAD -->

	<body style="direction: rtl; color: #000;">
		<!-- BEGIN HEADER -->
		<?php $this->beginBody(); ?>

		<div class="page-container page-content-inner" style="padding-top: 10px;">
			<div class="container-fluid container-lf-space">
				<?php echo $content; ?>
			</div>
		</div>

		<?php $this->endBody(); ?>
	</body>
</html>
<?php $this->endPage(); ?>
