<?php 

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\models\EventMembers;
use backend\models\CaravanPayment;
use backend\models\UserSearchForm;
use backend\models\NutritionUsers;
use backend\modules\YumUsers\models\UsersType;

$request = Yii::$app->request;

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$notRegisteredInNutrition = 0;

/**
 * Check if user is admin
 */
$isAdminUser = Yii::$app->user->identity->username == 'admin';
$userId      = Yii::$app->user->identity->id;
/**
 * Get Access Level of User and store in variable for reduce transaction with database:
 */

$admin 				= Yii::$app->user->can('admin');
$supervisor			= Yii::$app->user->can('supervisor');
$student			= Yii::$app->user->can('student');


 
 
/**
 * Variable for active menu
 */
$addCaravan = Yii::t('app', 'HINT_ADD_CARAVAN');
$currentControllerSystem = ($controller == 'user'  || $controller == 'executive-committee'|| $controller == 'technical-committee' ||$controller == 'referee' || $controller == 'athlete' || $controller == 'coach' || $controller == 'local-driver' || $controller == 'media-staff' || $controller == 'vip-misc-caravan' || $controller == 'university-resp' || $controller == 'event-admin' || $controller == 'search');

$currentControllerDesktop = ($controller == 'authorized-fields' || $controller == 'authorized-university' || $controller == 'messages' || $controller == 'teams' || $controller == 'caravan' || $controller == 'cards');

$person  = ($controller == 'person');
$mdate  = ($controller == 'mdate');
$rbak    = ($controller == 'role' || $controller == 'rule' || $controller == 'permission' ||  $controller == 'route' || $controller == 'assignment' );
$request = ($controller == 'request' || ($controller == 'request' && $action == 'create')  || ($controller == 'request' && $action == 'index') );

 
$financialSystem = ($controller == 'financial');
 
$currentControllerBaseInfo = ($controller == 'eventtype' || $controller == 'university' || $controller == 'fields' || ($controller == 'event' && $isAdminUser && $action == 'index') || ($controller == 'event' && $isAdminUser && $action == 'view') || $controller == 'sport-location');
$accommodation = ($controller == 'place' || $controller == 'eskan' || $controller == 'roomsa');
 
$eskan = ($controller == 'eskan');
$form = ($controller == 'familyform' || $controller == 'exitform' || $controller == 'absence' || $controller == 'delay');


$base = ($controller == 'university' ||$controller == 'field' ||$controller == 'trend' || $controller == 'place'  || $controller == 'floor'|| $controller == 'suite'|| $controller == 'rom');

$config = "";
?>



<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		

		<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
			


			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler"> </div>
			</li>

			 

			<li class="nav-item start <?= ($controller == 'site' && $action == 'index') ? 'active' : '' ; ?>">
				<?php 
					echo Html::a(
						'<i class="fa fa-desktop"></i><span class="title">' . Yii::t('app', 'Dashboard') . '</span><span class="selected"></span>',
						['/site'],
						['class' => 'nav-link nav-toggle']
					); 
				?>
			</li>


			<?php if($admin || $supervisor ) : ?>
			<li class="nav-item <?= ($base) ? 'active open' : '' ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-puzzle"></i>
					<span class="title"><?= Yii::t('app', 'Base Information') ?></span>
					<span class="selected"></span>
					<span class="arrow <?= ($currentControllerBaseInfo) ? 'open' : '' ?>"></span>
				</a>

				<ul class="sub-menu">
					 

					 
					<li class="nav-item <?php echo ($controller == 'university' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<span class="title">' . Yii::t('app', 'Universities') . '</span>',
								['/university'],
								['class' => 'nav-link']
							); 
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'field' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<span class="title">' . Yii::t('app', 'Fields') . '</span>',
								['/field'],
								['class' => 'nav-link']
							); 
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'trend' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<span class="title">' . Yii::t('app', 'Trends') . '</span>',
								['/trend'],
								['class' => 'nav-link']
							); 
						?>
					</li>



					<li class="nav-item <?php echo ($controller == 'place' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php
						echo Html::a(
							'<span class="title">' . Yii::t('app', 'Tree Structure') . '</span>',
							['/trree/view'],
							['class' => 'nav-link']
						);
						?>
					</li>




					 

					 

					 

					 
				</ul>
			</li>
			<?php endif;?> 


			<?php if($admin || $supervisor ) : ?> 
			<li class="nav-item <?= ($person) ? 'active open' : '' ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-user"></i>
					<span class="title"><?= Yii::t('app', 'Persons') ?></span>
					<span class="selected"></span>
					<span class="arrow <?= ($person) ? 'open' : '' ?>"></span>
				</a>

				<ul class="sub-menu">
					
					
 					<li class="nav-item <?php echo ($controller == 'person' && $action == 'createuser') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-plus"></i> <span class="title">'.Yii::t('app', 'Create a new User') . '</span>',
								['/person/createuser'],
								['class' => 'nav-link']
							);
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'person' && $action == 'createstudent') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-plus"></i> <span class="title">' . Yii::t('app', 'Create a new Student') . '</span>',
								['/person/createstudent'],
								['class' => 'nav-link']
							);
						?>
					</li>



					<li class="nav-item <?php echo ($controller == 'person' && $action == 'users') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-user"></i> <span class="title">' . Yii::t('app', 'User Managment') . '</span>',
								['/person/users'],
								['class' => 'nav-link']
							);
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'person' && $action == 'students') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-user"></i> <span class="title">' . Yii::t('app', 'Student Managment') . '</span>',
								['/person/students'],
								['class' => 'nav-link']
							);
						?>
					</li>


					

					 
					 
				</ul>
				
				
			</li>
			<?php endif; ?>


			<?php if($admin || $supervisor ) : ?> 
			<li class="nav-item <?= ($mdate) ? 'active open' : '' ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-clock"></i>
					<span class="title"><?= Yii::t('app', 'Date Managment') ?></span>
					<span class="selected"></span>
					<span class="arrow <?= ($person) ? 'open' : '' ?>"></span>
				</a>

				<ul class="sub-menu">
					
					
 					<li class="nav-item <?php echo ($controller == 'mdate' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-plus"></i> <span class="title">'.Yii::t('app', 'Dates') . '</span>',
								['/mdate/index'],
								['class' => 'nav-link']
							);
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'mdate' && $action == 'create') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-plus"></i> <span class="title">' . Yii::t('app', 'Create a new Date') . '</span>',
								['/mdate/create'],
								['class' => 'nav-link']
							);
						?>
					</li>



				 


					

					 
					 
				</ul>
				
				
			</li>
			<?php endif; ?>


			<?php if($admin || $supervisor ) : ?> 
			<li class="nav-item <?= ($rbak) ? 'active open' : '' ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-user"></i>
					<span class="title"><?= Yii::t('app', 'Access Managment') ?></span>
					<span class="selected"></span>
					<span class="arrow <?= ($rbak) ? 'open' : '' ?>"></span>
				</a>

				<ul class="sub-menu">



					<li class="nav-item <?php echo ($controller == 'index' ) ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-user"></i> <span class="title">' . Yii::t('app', 'Assignment') . '</span>',
								['/admin/assignment'],
								['class' => 'nav-link']
							);
						?>
					</li>

					
					
 					<li class="nav-item <?php echo ($controller == 'role') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-user"></i> <span class="title">'.Yii::t('app', 'Roles') . '</span>',
								['/admin/role'],
								['class' => 'nav-link']
							);
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'rule' ) ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-plus"></i> <span class="title">' . Yii::t('app', 'Rules') . '</span>',
								['/admin/rule'],
								['class' => 'nav-link']
							);
						?>
					</li>



					<li class="nav-item <?php echo ($controller == 'permission' ) ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-key"></i> <span class="title">' . Yii::t('app', 'Permissions') . '</span>',
								['/admin/permission'],
								['class' => 'nav-link']
							);
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'route' ) ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-key"></i> <span class="title">' . Yii::t('app', 'Route') . '</span>',
								['/admin/route'],
								['class' => 'nav-link']
							);
						?>
					</li>

					 


					

					 
					 
				</ul>
				
				
			</li>
			<?php endif; ?>


			<li class="nav-item <?= ($request) ? 'active open' : '' ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-envelope"></i>
					<span class="title"><?php 
					if ($admin || $supervisor) {
						echo Yii::t('app', 'Dormitory request Managment');
					}
					if( $student ){
						echo Yii::t('app', 'Dormitory request');
					} 
					?></span>
					<span class="selected"></span>
					<span class="arrow <?= ($request) ? 'open' : '' ?>"></span>
				</a>

				<ul class="sub-menu">

					<?php if($admin || $supervisor ) : ?> 
					<li class="nav-item <?php echo ($controller == 'request' && $action == 'create') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-plus"></i> <span class="title">' . Yii::t('app', 'Make a new request') . '</span>',
								['/request/create'],
								['class' => 'nav-link']
							);
						?>
					</li>
 
					<li class="nav-item <?php echo ($controller == 'request' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<i class="icon-envelope-open"></i> <span class="title">' . Yii::t('app', 'Requests list') . '</span>',
								['/request'],
								['class' => 'nav-link']
							);
						?>
					</li>
					<?php endif;?>


					<?php if($student) : ?> 
					<li class="nav-item <?php echo ($controller == 'request' && $action == 'userrequests') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<span class="title">' . Yii::t('app', 'Requests List') . '</span>',
								['/request/userrequests/'],
								['class' => 'nav-link']
							);
						?>
					</li>

					<li class="nav-item <?php echo ($controller == 'request' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php 
							echo Html::a(
								'<span class="title">' . Yii::t('app', 'Make a new request') . '</span>',
								['/request/make/'.$userId],
								['class' => 'nav-link']
							);
						?>
					</li>
					<?php endif;?>

					 
					 
				</ul>
			</li>

			 


			 
			<li class="nav-item <?= $financialSystem ? 'active open' : '' ; ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-diamond"></i>
					<span class="title"><?php
					if ($admin || $supervisor) {
						echo Yii::t('app', 'Financial Management');
					}
					if( $student ){
						echo Yii::t('app', 'Financial');
					} 
					?>
					<span class="selected"></span>
					<span class="arrow <?= $financialSystem ? 'open' : '' ?>"></span>
				</a>

				<ul class="sub-menu">

					<?php if($admin || $supervisor) : ?> 
					<li class="nav-item <?php echo ($controller == 'financial' && $action == 'register-fish') ? 'active open' : '' ; ?>">
						<?php
						echo Html::a(
							'<span class="title">' . Yii::t('app', 'Register Bill') . '</span>'  ,
							['/financial/register-fish'],
							['class' => 'nav-link']
						);
						?>
					</li>
					 
					 

					<li class="nav-item <?php echo ($controller == 'financial' && $action == 'payments') ? 'active open' : '' ; ?>">
						<?php
						echo Html::a(
							'<span class="title">' . Yii::t('app', 'All Payments') . '</span>'  ,
							['/financial/payments'],
							['class' => 'nav-link']
						);
						?>
					</li>
					<?php endif;?>

					<?php if($student) : ?> 
					<li class="nav-item <?php echo ($controller == 'financial' && $action == 'bills') ? 'active open' : '' ; ?>">
						<?php
						echo Html::a(
							'<span class="title">' . Yii::t('app', 'Bills List') . '</span>'  ,
							['/financial/biils/'.$userId],
							['class' => 'nav-link']
						);
						?>
					</li>
					<?php endif;?>
					


					 
				</ul>
			</li>
			 
 

	 
			 
			


			


			<li class="nav-item <?= ($eskan) ? 'active open' : '' ?>">
				<a href="javascript:;" class="nav-link nav-toggle">



					<i class="icon-home"></i>
                    <span class="title">
                    <?php if($admin || $supervisor) : ?>
                        <?= Yii::t('app', 'room managment') ?>
                    <?php endif;?>
                    <?php if($student) : ?>
                        <?= Yii::t('app', 'eskan-student') ?>
                    <?php endif;?>
                    </span>
					<span class="selected"></span>
					<span class="arrow <?= ($eskan) ? 'open' : '' ?>"></span>
				</a>
			<ul class="sub-menu">

			<?php if($admin || $supervisor) : ?>


			<li class="nav-item <?php echo ($controller == 'place' && $action == 'index') ? 'active open' : '' ; ?>">
						<?php
						echo Html::a(

							'<span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

							//'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

							['/eskan'],
							['class' => 'nav-link']
						);
						?>
			</li>
			<?php endif;?>
            <?php if($student) : ?>
                <li class="nav-item <?php echo ($controller == 'place' && $action == 'index') ? 'active open' : '' ; ?>">
                    <?php
                    echo Html::a(

                        '<span class="title">' . Yii::t('app', 'View Eskan State') . '</span>',

                        //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Place Allotment') . '</span>',

                        ['/trree/state'],
                        ['class' => 'nav-link']
                    );
                    ?>
                </li>
            <?php endif;?>


            </ul>

			</li>


            <li class="nav-item <?= ($form) ? 'active open' : '' ?>">
                <a href="javascript:;" class="nav-link nav-toggle">



                    <i class="fa fa-wpforms"></i>
                    <span class="title">
                        <?php if($admin || $supervisor) : ?>
                        <?= Yii::t('app', 'form managment') ?>
                        <?php endif;?>
                        <?php if($student) : ?>
                            <?= Yii::t('app', 'forms') ?>
                        <?php endif;?>
                    </span>

                    <span class="selected"></span>
                    <span class="arrow <?= ($form) ? 'open' : '' ?>"></span>
                </a>
                <ul class="sub-menu">

                    <?php if($admin || $supervisor) : ?>
                    <li class="nav-item <?php echo ($controller == 'familyform' && $action == 'admincreate') ? 'active open' : '' ; ?>">
                        <?php
                        echo Html::a(

                            '<i class="fa fa-users"></i><span class="title">' . Yii::t('app', 'Family form management') . '</span>',

                            //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                            ['/familyform/admincreate'],
                            ['class' => 'nav-link']
                        );
                        ?>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'exitform' && $action == 'admincreate') ? 'active open' : '' ; ?>">
                        <?php
                        echo Html::a(

                            '<i class="fa fa-share"></i><span class="title">' . Yii::t('app', 'Exit form management') . '</span>',

                            //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                            ['/exitform/admincreate'],
                            ['class' => 'nav-link']
                        );
                        ?>
                    </li>
                        <li class="nav-item <?php echo ($controller == 'absence' && $action == 'create') ? 'active open' : '' ; ?>">
                            <?php
                            echo Html::a(

                                '<i class="fa fa-exclamation-circle"></i><span class="title">' . Yii::t('app', 'absence form management') . '</span>',

                                //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                                ['/absence/create'],
                                ['class' => 'nav-link']
                            );
                            ?>
                        </li>
                        <li class="nav-item <?php echo ($controller == 'delay' && $action == 'create') ? 'active open' : '' ; ?>">
                            <?php
                            echo Html::a(

                                '<i class="fa fa-clock-o"></i><span class="title">' . Yii::t('app', 'delay form management') . '</span>',

                                //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                                ['/delay/create'],
                                ['class' => 'nav-link']
                            );
                            ?>
                        </li>
                    <?php endif;?>
                    <?php if($student) : ?>
                        <li class="nav-item <?php echo ($controller == 'familyform' && $action == 'create') ? 'active open' : '' ; ?>">
                            <?php
                            echo Html::a(

                                '<i class="fa fa-users"></i><span class="title">' . Yii::t('app', 'Family form register') . '</span>',

                                //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                                ['/familyform/create'],
                                ['class' => 'nav-link']
                            );
                            ?>
                        </li>
                        <li class="nav-item <?php echo ($controller == 'exitform' && $action == 'admincreate') ? 'active open' : '' ; ?>">
                            <?php
                            echo Html::a(

                                '<i class="fa fa-share"></i><span class="title">' . Yii::t('app', 'Exit form register') . '</span>',

                                //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                                ['/exitform/create'],
                                ['class' => 'nav-link']
                            );
                            ?>
                        <li class="nav-item <?php echo ($controller == 'absence' && $action == 'studentindex') ? 'active open' : '' ; ?>">
                            <?php
                            echo Html::a(

                                '<i class="fa fa-exclamation-circle"></i><span class="title">' . Yii::t('app', 'Absence History') . '</span>',

                                //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                                ['/absence/studentindex'],
                                ['class' => 'nav-link']
                            );
                            ?>
                        </li>
                        <li class="nav-item <?php echo ($controller == 'delay' && $action == 'studentindex') ? 'active open' : '' ; ?>">
                            <?php
                            echo Html::a(

                                '<i class="fa fa-clock-o"></i><span class="title">' . Yii::t('app', 'Delay History') . '</span>',

                                //'<i class="icon-home"></i><span class="title">' . Yii::t('app', 'Eskan List') . '</span>',

                                ['/delay/studentindex'],
                                ['class' => 'nav-link']
                            );
                            ?>
                        </li>
                        </li>
                    <?php endif;?>
                </ul>
            </li>
			 

			 

		</ul>
	</div>
</div>