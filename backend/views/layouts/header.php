<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;

use backend\assets\IntrojsAsset;
use backend\models\Event;

IntrojsAsset::register($this);

 
/**
 * Get User Avater
 */
$avatar = false;//Yii::$app->user->identity->Avatar;

/**
 * Check if not set image show defualt image
 */
if(!$avatar)
	$avatar = \Yii::$app->request->BaseUrl . '/fum/layouts/layout/img/avatar.png';

/**
 * Set introjs Label
 */
$next = Yii::t('zii', 'Next');
$prev = Yii::t('zii', 'Previous');
$skip = Yii::t('zii', 'Close');
$end = Yii::t('zii', 'End');

/**
 * Insert introjs
 */
$this->registerJs(
<<<SCRIPT
	var introguide = introJs();
	introguide.setOptions({ 'nextLabel': '$next', 'prevLabel': '$prev', 'skipLabel': '$skip', 'doneLabel': '$end', 'tooltipPosition': 'auto' });
	introguide.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top']);
SCRIPT
, View::POS_END);

/**
 * Two functions added by Daliri.ha to have more control on introJs
 */
$this->registerJs(
<<<SCRIPT
    function clearHelpStep(maxStep) {
        $(function() {
            $("[data-intro]").each(function(){
                if($(this).attr('data-step') > maxStep)
                    $(this).attr('data-step','');
            });
        });
    }
        
    function resetHelpStep(id, step) {
        $(function() {
            $("#"+id).attr('data-step',step);
        });
    }
    
SCRIPT
, View::POS_END);

?>
		<div class="page-header navbar navbar-fixed-top">
			<div class="page-header-inner ">
				<div class="page-logo">
					<a href="<?= Url::home() ?>">
						<img src="<?= \Yii::$app->request->BaseUrl ?>/images/olympiadlogo.png" alt="logo" class="logo-default" />
                                                <?php //echo  Yii::$app->params['systemname'] ?>
					</a>
					<div class="menu-toggler sidebar-toggler"> </div>
				</div>
				
				 

				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>

				 
				<div class="current-event hidden-xs">
					<i class="icon-bulb font-yellow"></i>
					<span class="event-title">
						<?=  "پورتال مدیریت اسکان دانشجویی بنیاد"; ?>
					</span>
				</div>
				 

				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<li class="dropdown dropdown-guide">
							<a href="javascript:;" class="dropdown-toggle" onclick="introguide.start()">
								<i class="icon-question"></i>
							</a>
						</li>
						<li class="dropdown dropdown-user">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<img alt="" class="img-circle" src="<?= $avatar ?>" />
								<span class="username username-hide-on-mobile">
								<?php  
									$name=Yii::$app->user->identity->name;
									$lname=Yii::$app->user->identity->last_name;
									echo $name.' '.$lname;								
								?>
								</span>
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-menu-default">
								<li>
									<?php
										echo Html::a(
											'<i class="icon-user"></i>' . Yii::t('app', 'My Profile'),
											['/members/profileinfo']
										);
									?>
								</li>
								<li>
									<?= Html::a('<i class="icon-key"></i>'.Yii::t('app', 'Sign out'), ['/logout/logout'], ['data-method' => 'post']) ?>
								</li>
							</ul>
						</li>

						<?php if(Yii::$app->session->has('lastUser')): ?>
						<li class="dropdown dropdown-return-user">
							<?php
								echo Html::a(
									'<i class="icon-logout"></i>',
									['/site/return'],
									['class'=>'dropdown-toggle']
								);
							?>
						</li>
						<?php endif; ?>
					</ul>
				</div>

			</div>

		</div>
		<div class="clearfix"> </div>