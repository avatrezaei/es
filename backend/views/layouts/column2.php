<?php 
/* @var $this Controller */ 
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use yii\bootstrap\ButtonDropdown;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<div class="page-bar">
    <?php if (isset($this->params['breadcrumbs'])): ?>
        <?php
        echo Breadcrumbs::widget([
            'options' => [
                'class' => 'page-breadcrumb'
            ],
            'itemTemplate' => '<li>{link}<i class="fa fa-angle-left"></i></li>',
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    <?php endif; ?>
    <?php if (isset($this->params['actions'])): ?>
        <div class="page-toolbar">
            <div class="pull-right">
                <?php
                foreach ($this->params['actions'] as $link) {
//                                                                        
                    $options = isset($link['options']) ? $link['options'] : ['class' => 'btn btn-info'];
                    if ($link['url'])
                        echo Html::a($link['label'], $link['url'], $options);
                    else
                        echo Html::label($link['label'], null, $options);
                    
                }
                ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<h3 class="page-title">
    <?php if (isset($this->params['page_title']) && $this->params['page_title']): ?>
        <?= $this->title ?>
    <?php endif; ?>
</h3>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php echo Alert::widget(); ?>

        <!-- PAGE CONTENT BEGINS -->
        <?php echo $content; ?>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->

<?php /* echo $this->render('//layouts/top-menu.php'); */ ?>

<?php $this->endContent(); ?>

