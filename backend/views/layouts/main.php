<?php
use backend\assets\AppAsset;
use backend\assets\LanguageAsset;
use backend\assets\SweetAlertAsset;
use backend\assets\AdminErrorAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

/**
 * Custome Asset:
 */
if (Yii::$app->controller->action->id == 'error'){
	AdminErrorAsset::register($this);
}

LanguageAsset::register($this);
SweetAlertAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--><html lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<title><?php echo Yii::$app->name . ' - ' . Html::encode($this->title) ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=8">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<?php echo Html::csrfMetaTags(); ?>
		<?php $this->head(); ?>

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo \Yii::$app->request->BaseUrl;?>/favicon.ico" />
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-footer-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white">
		<!-- BEGIN HEADER -->
		<?php $this->beginBody(); ?>
		<!-- header -->
		<?php echo $this->render('//layouts/header.php'); ?>

		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<?php echo $this->render('//layouts/tpl_sidebar.php'); ?>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<?php echo $content; ?>
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		<div class="page-footer">
			<div class="page-footer-inner"> 2016 © <?= Yii::$app->params['concessionaire'] ?> </div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
		<?php $this->endBody(); ?>
		<!-- END FOOTER -->
	</body>
</html>
<?php $this->endPage(); ?>
