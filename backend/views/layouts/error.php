<?php
use backend\assets\ErrorAsset;
use backend\assets\IEAsset;

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

ErrorAsset::register($this);
IEAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--><html lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<title><?php echo Yii::$app->name . ' - ' . Html::encode($this->title) ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<?php echo Html::csrfMetaTags(); ?>
		<?php $this->head(); ?>

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo \Yii::$app->request->BaseUrl;?>/favicon.ico" />
	</head>
	<!-- END HEAD -->

	<body class=" page-404-full-page">
		<!-- BEGIN HEADER -->
		<?php $this->beginBody(); ?>
		<div class="row">
			<div class="col-md-12 page-404">
				<?php echo $content; ?>
			</div>
		</div>

		<?php $this->endBody(); ?>
	</body>
</html>
<?php $this->endPage(); ?>
