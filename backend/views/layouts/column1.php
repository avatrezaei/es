<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

 			<div class="page-content">
				<!--PAGE CONTENT BEGINS-->
				<?php echo $content; ?>
				<!--PAGE CONTENT ENDS-->
			</div><!--/.page-content-->

<?php $this->endContent(); ?>