<?php
 
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use yii\widgets\ActiveForm;
use kartik\select2\Select2;

use backend\assets\LaddaAsset;

use backend\models\Draft;
use backend\models\Teams;
use backend\models\Caravan;
use backend\models\Request;
use backend\models\MessageForm;
use backend\models\AuthorizedFields;
use backend\models\AuthorizedUniversity;
use backend\modules\YumUsers\models\UsersType;
use backend\models\User;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use kartik\file\FileInput;

LaddaAsset::register($this);

$persons = ArrayHelper::map(User::find()->students()->all(), 'id', 'Fullname'); 
$dateTemplate = '{label}<div class="col-md-9 col-sm-9"><div class="input-icon"><i class="fa fa-calendar"></i>{input}</div> {hint} {error}</div>';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];

 
?>
 
	 

<div class="request-form">

	<?php $form = ActiveForm::begin(['options' => ['id'=>'form_change_pic','data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary([$model], ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">
		<div class="row">
			<!-- person name -->
			<div class="col-md-6 col-sm-6 xol-xs-12">
									<?php

										echo $form->field(
											$model,
											'personId',
											[
												'options' => ['class' => $formGroupClass],
												'template' => $template
											]
										)
										->widget(Select2::classname(), [
											'data' => $persons,
											'theme' => Select2::THEME_BOOTSTRAP,
											'options' => [
												'placeholder' => Yii::t('app', 'Please Select a Person'),
												'dir' => 'rtl',
											],
										])
										->label(NULL, ['class'=>$labelClass]);


									 ?>

			</div>

			 
		</div>

		
		<div class="row">
			<!-- year -->
			<div class="col-md-6 col-sm-6 xol-xs-12">



				<?= $form->field($model, 'eduYear',[
	   'options' => ['class' => $formGroupClass],
			   'template' => $template
		   ])->dropDownList(['1394'=>'1394','1395'=>'1395','1396'=>'1396','1397'=>'1397','1398'=>'1398'],['prompt'=>'سال تحصیلی را وارد کنید.', 'id'=>'eduYear'])->label(NULL, ['class'=>$labelClass]);
		    ?>
			</div>
			<!-- semester -->
			<div class="col-md-6 col-sm-6 xol-xs-12">

				<?= $form->field($model, 'semester',[
						'options' => ['class' => $formGroupClass],
						'template' => $template
				])->dropDownList([1=>'1',2=>'2'],['prompt'=>'نیمسال تحصیلی را وارد کنید', 'id'=>'semester'])->label(NULL, ['class'=>$labelClass]);
				?>
								<?php
//									echo $form->field(
//									$model,
//									'semester',
//									[
//										'options' => ['class' => $formGroupClass],
//										'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="nutritionmeal-mealtype">{input} </div> {hint} {error}</div>'
//									]
//								)
//								->radioList(
//									Request::itemAlias('Semester'),
//									[
//										'tag' => false,
//										'unselect' => NULL,
//										'item' => function($index, $label, $name, $checked, $value) {
//											return '<label class="btn btn-warning '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value, 'autocomplete'=>'off']) . $label . '</label>';
//										}
//									]
//								)
//								->label(NULL, ['class'=>$labelClass]);
							?>
							</div>
		</div>


		<div class="row">
			<div class="col-md-6 col-sm-6 xol-xs-12">
				<?php
					echo $form->field(
						$model,
						'dateIn',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $dateTemplate
						]
					)
					->widget(jalaliDatePicker::className(), [
						'options' => [
							'format' => 'yyyy/mm/dd',
							'viewformat' => 'yyyy/mm/dd',
							'placement' => 'right',
							'todayBtn' => 'linked' ,
							'id' => 'caravanpayment-dateIn',
							'class' => 'form-control' ,
							'autoclose'=>true,
						],
					])
					->label(NULL, ['class' => $labelClass]);
				?>
			</div>
			<div class="col-md-6 col-sm-6 xol-xs-12">
				<?php
					echo $form->field(
						$model,
						'dateOut',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $dateTemplate
						]
					)
					->widget(jalaliDatePicker::className(), [
						'options' => [
							'format' => 'yyyy/mm/dd',
							'viewformat' => 'yyyy/mm/dd',
							'placement' => 'right',
							'todayBtn' => 'linked' ,
							'id' => 'caravanpayment-dateOut',
							'class' => 'form-control' ,
							'autoclose'=>true,
						],
					])
					->label(NULL, ['class' => $labelClass]);
				?>
			</div>
		</div>
		
	<!-- ######################################### -->
	<div class="row">
		<div class="col-md-6">
			<?php
				echo $form->field(
					$model,
					'comment',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textarea(['rows' => 6, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
			?>
		</div>
		<div class="col-md-6">
			<?php

			
				  // your fileinput widget for single file upload
				echo $form->field($model, 'image')->widget(FileInput::classname(), [
				    'options'=>['accept'=>'image/*'],
				    'pluginOptions'=>[
				    	'allowedFileExtensions'=>['jpg','gif','png'],
				    	'showPreview' => false,
					    'showCaption' => true,
					    'showRemove' => true,
					    'showUpload' => false,
				]])->label(NULL, ['class'=>$labelClass]);;





			?>
		</div>	
	</div>
	
	
 

	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-1 col-md-9">
				<?= Html::submitButton( Yii::t('app', 'Update'), ['class' =>'btn green' ]) ?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>


<script>
	 function savePicture()
	{

		var _form = $('#form_change_pic');
		var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]) ?>"
		var _failmsg = "<?php echo Yii::t('app', 'Failed To Edit Items To {item}.', ['item' => Yii::t('app', 'File')]) ?>"

		var formData = new FormData($('#form_change_pic')[0]);
		formData.append('file', $('input[type=file]')[0].files[0]);

		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: formData,
			processData: false,
			contentType: false,
			dataType: "JSON",
			success: function (data) {

				/**success save**/
				if ((typeof data.success === true))
				{

		   
					/*provide success Message */
					$("#results_expand_msg" ).html("<div class=\"alert alert-success\" role=\"alert\"> " + _successmsg + " </div>");

					/*
					 *1-fade success message
					 *2-close detailview div slidly
					 *3-reload gridview by pjax 
					 */
					$("#results_expand_msg").fadeIn(3500, function () {
					$("#results_expand_msg").fadeOut( 4000 );
					});

				} else  /***********fail save *****************/
				{
			   
					/*provide success Message */
					$("#results_expand_msg" ).html("<div class=\"alert alert-danger\" role=\"alert\"> " + data.errormessage + " </div>");


					$("#results_expand_msg" ).fadeIn(1300, function () {
//					   // $.pjax.reload({container: "#Projects"});
					});
				}



			},
			error: function (data) {

				alert(_failmsg);

			}
		});


	}
	</script>