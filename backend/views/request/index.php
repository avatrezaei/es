<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\ToastrAsset;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
ToastrAsset::register($this);
$this->title = Yii::t('app','Requests');

$admin 				= Yii::$app->user->can('admin');
$supervisor			= Yii::$app->user->can('supervisor');
$student 			= Yii::$app->user->can('student');


$this->params['breadcrumbs'][] = $this->title;

if($student)
{

	$this->params ['actions'] = [
			[
					'label' => "<i class='icon-envelope-open'></i>". Yii::t ( 'app', 'Make a new request' ),
					'url'=>Url::to ( ['/request/make/','id'=> isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0]),
					'options' => [
							'class' => 'btn btn-info',
					],

			]


	];
}else
{

	$this->params ['actions'] = [
			[
					'label' => "<i class='icon-envelope-open'></i>". Yii::t ( 'app', 'Make a new request' ),
					'url'=>Url::to ( ['/request/create/']),
					'options' => [
							'class' => 'btn btn-info',
					],

			]


	];
}

$updateText = Yii::t('app', 'Change Status');

$this->registerJs(
<<<SCRIPT
	$('#nutritionmeal-type').on('keyup change', function() {
		table.columns(3).search($(this).find(':selected').val()).draw();
	});
	$('#nutritionmeal-status').on('keyup change', function() {
		table.columns(4).search($(this).find(':selected').val()).draw();
	});



 
      $(document).on('click', '.showModalButton', function(){
        
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        } else {
            $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        }
    });


    $('body').on('click','.status', function(){
        var a = $(this);
        var b = $('.status-text');
        var response = null;

        jQuery.ajax({
            type:'GET',
            url:a.attr('href'),
            cache:false,
            'success':function(data){
                var obj = jQuery.parseJSON(data);
                response = obj;
                
                toastr.options = {
                    'closeButton': true,
                    'positionClass': "toast-top-right",
                }
                toastr[response.message.search("!") != -1 ? 'error' : 'success'](response.message, '$updateText')
            },
            'complete': function(){
                if(response != null)
                {
                    a.attr('data-hint', response.hint)
                    a.html(response.content);

                    if(response.enable)
                    {
                        a.removeClass('hint--error');
                        a.addClass('hint--success');
                         
                    }
                    else
                    {
                        a.removeClass('hint--success');
                        a.addClass('hint--error');
                         
                    }
                }
            }
        });
        return false;
    });

	 
SCRIPT
, yii\web\View::POS_END);

?>
<div class="send-messsage-index">
	<div class="col-lg-12 col-md-12  ">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-green">
					<span class="caption-subject bold">
						<i class="icon-envelope-open"></i>
						<?= Html::encode($this->title) ?>
					</span>
				</div>
			</div>
			<div class="portlet-body form">
				<?=  $widget->run() ?>
			</div>
		</div>
	</div>
</div>
<div id="preview"></div>


<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>