<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Request */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Update'),
		'url' => ['updateby-user', 'id' => $model->id]
	],
//	[
//		'label' => Yii::t('app', 'Delete'),
//		'url' => ['delete', 'id' => $model->id],
//		'options' => [
//			'data' => [
//				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
//				'method' => 'post',
//			],
//			'class' => 'btn btn-danger'
//		]
//	],
];

?>

<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="request-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-5">
				<div class="portlet light ">
				 
					 	<?php
					 	$detalView  = '<div class="row static-info"><div class="col-md-4 name"> {label}: </div><div class="col-md-8 col-sm-8 col-xs-8 value"> {value} </div></div>';
					 	?>
						<?= DetailView::widget([
							'model' => $model,
							'template' => $detalView,
							'attributes' => [
								'personname',
								'eduYear',
							 
								[
						             'attribute' => 'semester',
						             'format'=>'raw',
						        ],
								'date',
								'dateIn',
								'dateOut',
								'comment:ntext',
								 [
						             'attribute' => 'statusi',
						             'format'=>'raw',						            
						        ],
							],
						]) ?>
					</div>
			</div>
			<div class="col-md-7">
				<div class="portlet light">
					<?php
						//$path = Yii::$app->basePath."\\uploads\\".$model->file;
						$img = Url::to(\Yii::$app->request->BaseUrl."/uploads/").$model->file;
						$image = '<img src="'.$img.'" width="400" height="200" />';
					?>
					<img src="<?= $img ?>" alt="logo" class="logo-default" width="490" height="290"/>
				</div>
			</div>
				
					 
						

						
					
				</div>
			</div>
		</div>
	</div>
</div>