<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Request */

$this->title = Yii::t('app', 'Create Request').' '.$user->name.' '.$user->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-create">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green">
						<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
					</div>
				</div>
				<div class="portlet-body form">
                    <?php
                    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                    //echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
                    }
                    ?>
					<?= $this->render('make_form', [
						'model' => $model,
						'user'  => $user,
					]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
