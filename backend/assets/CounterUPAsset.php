<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Faravaghi <info@dkr.co.ir>
 * @since 2.0
 */
class CounterUPAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [];

	public $js = [
		'fum/global/plugins/counterup/jquery.waypoints.min.js',
		'fum/global/plugins/counterup/jquery.counterup.min.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}