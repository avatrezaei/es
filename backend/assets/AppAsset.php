<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;
use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	/**
 * Check if user is admin
 */
	public $isAdmin = 1;
	public $css;
	

	 

	public $js = [
		'fum/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		'fum/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
		// 'fum/global/plugins/uniform/jquery.uniform.min.js',
		'fum/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',

		'fum/global/scripts/app.min.js',
		'fum/global/scripts/mycode.js',
		'fum/layouts/layout/scripts/layout.min.js',
		'fum/layouts/layout/scripts/demo.min.js',
		'fum/global/scripts/hd.js',
		'fum/global/scripts/yii_overrides.js',
		
	];

	public $depends = [
		'yii\web\YiiAsset',
		// 'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',

		//additional import of third party alert project
		'backend\assets\SweetAlertAsset',
	];

	public function init()
	{
		parent::init();
		// resetting BootstrapAsset to not load own css files
		\Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
			'css' => []
		];
		$admin 		= Yii::$app->user->can('admin');
		$supervisor = Yii::$app->user->can('supervisor');	
 

		if($admin || $supervisor){
		$this->css = [
			'fum/global/plugins/font-awesome/css/font-awesome.min.css',
			'fum/global/plugins/simple-line-icons/simple-line-icons.min.css',
			'fum/global/plugins/hint/hint.min.css',
			//
			'fum/global/plugins/bootstrap/css/bootstrap-rtl.min.css',
			'fum/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css',
			'fum/global/css/components-rounded-rtl.min.css',
			'fum/global/css/plugins-rtl.min.css',
			'fum/layouts/layout/css/layout-rtl.min.css',
			'fum/layouts/layout/css/themes/darkblue-rtl.min.css',
		];
		}
		else{
			$this->css = [
				'fum/global/plugins/font-awesome/css/font-awesome.min.css',
				'fum/global/plugins/simple-line-icons/simple-line-icons.min.css',
				'fum/global/plugins/hint/hint.min.css',
				//
				'fum/global/plugins/bootstrap/css/bootstrap-rtl.min.css',
				'fum/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css',
				'fum/global/css/components-rounded-rtl.min.css',
				'fum/global/css/plugins-rtl.min.css',
				'fum/global/css/extend.css',
				'fum/layouts/layout/css/layout-rtl.min.css',
				'fum/layouts/layout/css/themes/blue-rtl.min.css',
			];
		}
	}
}