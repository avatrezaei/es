<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Faravaghi <info@dkr.co.ir>
 * @since 2.0
 */
class IEAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
		'rezvan/css/ace-part2.css',
		'rezvan/css/ace-ie.css',
	];

	public $js = [
		'rezvan/js/html5shiv.js',
		'rezvan/js/respond.js',
	];

	public $cssOptions = [
		'condition' => 'lte IE 9',
	];

	public $jsOptions = [
		'condition' => 'lte IE 9',
		'position' => \yii\web\View::POS_END
	];
}