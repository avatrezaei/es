<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PDFAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
		'fum/global/plugins/font-awesome/css/font-awesome.min.css',
		// 'fum/global/plugins/simple-line-icons/simple-line-icons.min.css',
		'fum/global/plugins/bootstrap/css/bootstrap-rtl.min.css',

		'fum/global/css/components-rounded-rtl.min.css',
		// 'fum/global/css/plugins-rtl.min.css',
		
		'fum/layouts/layout7/css/layout-rtl.min.css',
		// 'fum/layouts/layout7/css/custom-rtl.min.css',

		'fum/apps/css/default-rtl.css',
		'fum/apps/css/font.css',
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}