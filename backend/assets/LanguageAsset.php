<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Faravaghi <info@dkr.co.ir>
 * @since 2.0
 */
class LanguageAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	// public $sourcePath = '@app/assets/language';
	public $css = [
		
		//'fum/layouts/layout/css/custom-rtl.min.css',

		'fum/apps/css/default-rtl.css',
		'fum/apps/css/font.css',
	];
}