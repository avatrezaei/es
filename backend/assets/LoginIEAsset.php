<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginIEAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $js = [
		'fum/global/plugins/respond.min.js',
		'fum/global/plugins/excanvas.min.js',
	];

	public $jsOptions = [
		'condition' => 'lte IE 9',
		'position' => \yii\web\View::POS_END
	];

	public $depends = [
		'yii\web\YiiAsset',
	];
}
