<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CaravanTeams */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="caravan-teams-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'caravanId')->textInput() ?>

	<?= $form->field($model, 'teamsId')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
