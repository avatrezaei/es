<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CaravanTeams */

$this->title = Yii::t('app', 'Create Caravan Teams');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Caravan Teams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caravan-teams-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
