<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'parent_id',
			'level',
			'path',
			'title',
			// 'alias',
			// 'type',
			// 'note',
			// 'description:ntext',
			// 'published',
			// 'access',
			// 'params:ntext',
			// 'metadata',
			// 'lang',
			// 'file_id',
			// 'created_user_id',
			// 'created_date_time',
			// 'modified_date_time',
			// 'active',
			// 'valid',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
