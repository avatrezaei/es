<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'parent_id')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'level')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'path')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'type')->textInput() ?>

	<?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'published')->textInput() ?>

	<?= $form->field($model, 'access')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'params')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'metadata')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'lang')->textInput() ?>

	<?= $form->field($model, 'file_id')->textInput() ?>

	<?= $form->field($model, 'created_user_id')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'created_date_time')->textInput() ?>

	<?= $form->field($model, 'modified_date_time')->textInput() ?>

	<?= $form->field($model, 'active')->textInput() ?>

	<?= $form->field($model, 'valid')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
