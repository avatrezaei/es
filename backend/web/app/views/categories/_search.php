<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'parent_id') ?>

	<?= $form->field($model, 'level') ?>

	<?= $form->field($model, 'path') ?>

	<?= $form->field($model, 'title') ?>

	<?php // echo $form->field($model, 'alias') ?>

	<?php // echo $form->field($model, 'type') ?>

	<?php // echo $form->field($model, 'note') ?>

	<?php // echo $form->field($model, 'description') ?>

	<?php // echo $form->field($model, 'published') ?>

	<?php // echo $form->field($model, 'access') ?>

	<?php // echo $form->field($model, 'params') ?>

	<?php // echo $form->field($model, 'metadata') ?>

	<?php // echo $form->field($model, 'lang') ?>

	<?php // echo $form->field($model, 'file_id') ?>

	<?php // echo $form->field($model, 'created_user_id') ?>

	<?php // echo $form->field($model, 'created_date_time') ?>

	<?php // echo $form->field($model, 'modified_date_time') ?>

	<?php // echo $form->field($model, 'active') ?>

	<?php // echo $form->field($model, 'valid') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
