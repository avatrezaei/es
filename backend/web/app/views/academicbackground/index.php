<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AcademicBackgroundSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Academic Backgrounds');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-background-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Yii::t('app', 'Create Academic Background'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'user_id',
			'science_level',
			'title',
			'from_date',
			// 'to_date',
			// 'theses_title',
			// 'supervisor',
			// 'country',
			// 'city',
			// 'lang',
			// 'created_date_time',
			// 'modified_date_time',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
