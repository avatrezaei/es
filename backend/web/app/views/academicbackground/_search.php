<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AcademicBackgroundSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="academic-background-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'user_id') ?>

	<?= $form->field($model, 'science_level') ?>

	<?= $form->field($model, 'title') ?>

	<?= $form->field($model, 'from_date') ?>

	<?php // echo $form->field($model, 'to_date') ?>

	<?php // echo $form->field($model, 'theses_title') ?>

	<?php // echo $form->field($model, 'supervisor') ?>

	<?php // echo $form->field($model, 'country') ?>

	<?php // echo $form->field($model, 'city') ?>

	<?php // echo $form->field($model, 'lang') ?>

	<?php // echo $form->field($model, 'created_date_time') ?>

	<?php // echo $form->field($model, 'modified_date_time') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
