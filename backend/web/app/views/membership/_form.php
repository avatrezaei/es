<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Membership */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="membership-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'user_id')->textInput() ?>

	<?= $form->field($model, 'group_name')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'from_date')->textInput() ?>

	<?= $form->field($model, 'to_date')->textInput() ?>

	<?= $form->field($model, 'country')->textInput() ?>

	<?= $form->field($model, 'city')->textInput(['maxlength' => 100]) ?>

	<?= $form->field($model, 'lang')->textInput() ?>

	<?= $form->field($model, 'created_date_time')->textInput() ?>

	<?= $form->field($model, 'modified_date_time')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
