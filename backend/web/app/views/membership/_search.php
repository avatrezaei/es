<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MembershipSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="membership-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'user_id') ?>

	<?= $form->field($model, 'group_name') ?>

	<?= $form->field($model, 'from_date') ?>

	<?= $form->field($model, 'to_date') ?>

	<?php // echo $form->field($model, 'country') ?>

	<?php // echo $form->field($model, 'city') ?>

	<?php // echo $form->field($model, 'lang') ?>

	<?php // echo $form->field($model, 'created_date_time') ?>

	<?php // echo $form->field($model, 'modified_date_time') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
