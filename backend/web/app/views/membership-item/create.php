<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MembershipItem */

$this->title = Yii::t('app', 'Create Membership Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membership Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membership-item-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
