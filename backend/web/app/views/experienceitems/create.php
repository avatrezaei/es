<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExperienceItems */

$this->title = Yii::t('app', 'Create Experience Items');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Experience Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="experience-items-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
