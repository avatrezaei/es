<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExperienceItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="experience-items-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'exp_id')->textInput() ?>

	<?= $form->field($model, 'item')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'created_date_time')->textInput() ?>

	<?= $form->field($model, 'modified_date_time')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
