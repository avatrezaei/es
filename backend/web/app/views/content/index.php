<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ContentManagement\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Yii::t('app', 'Create Content'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'title',
			'alias',
			'introtext:ntext',
			'fulltext:ntext',
			// 'state',
			// 'lang',
			// 'keywords',
			// 'created_user_id',
			// 'created_by_alias',
			// 'created_date_time',
			// 'modified_date_time',
			// 'modified_user_id',
			// 'checked_out',
			// 'checked_out_time',
			// 'publish_up',
			// 'publish_down',
			// 'images:ntext',
			// 'urls:ntext',
			// 'attribs',
			// 'version',
			// 'ordering',
			// 'metadesc:ntext',
			// 'access',
			// 'hits',
			// 'metadata:ntext',
			// 'xreference',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
