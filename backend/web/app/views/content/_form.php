<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ContentManagement\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'introtext')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'fulltext')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'state')->textInput() ?>

	<?= $form->field($model, 'lang')->textInput() ?>

	<?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'created_user_id')->textInput() ?>

	<?= $form->field($model, 'created_by_alias')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'created_date_time')->textInput() ?>

	<?= $form->field($model, 'modified_date_time')->textInput() ?>

	<?= $form->field($model, 'modified_user_id')->textInput() ?>

	<?= $form->field($model, 'checked_out')->textInput() ?>

	<?= $form->field($model, 'checked_out_time')->textInput() ?>

	<?= $form->field($model, 'publish_up')->textInput() ?>

	<?= $form->field($model, 'publish_down')->textInput() ?>

	<?= $form->field($model, 'images')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'urls')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'attribs')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'version')->textInput() ?>

	<?= $form->field($model, 'ordering')->textInput() ?>

	<?= $form->field($model, 'metadesc')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'access')->textInput() ?>

	<?= $form->field($model, 'hits')->textInput() ?>

	<?= $form->field($model, 'metadata')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'xreference')->textInput(['maxlength' => true]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
