<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ContentManagement\models\Content */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'title',
			'alias',
			'introtext:ntext',
			'fulltext:ntext',
			'state',
			'lang',
			'keywords',
			'created_user_id',
			'created_by_alias',
			'created_date_time',
			'modified_date_time',
			'modified_user_id',
			'checked_out',
			'checked_out_time',
			'publish_up',
			'publish_down',
			'images:ntext',
			'urls:ntext',
			'attribs',
			'version',
			'ordering',
			'metadesc:ntext',
			'access',
			'hits',
			'metadata:ntext',
			'xreference',
		],
	]) ?>

</div>
