<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ContentManagement\models\ContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'title') ?>

	<?= $form->field($model, 'alias') ?>

	<?= $form->field($model, 'introtext') ?>

	<?= $form->field($model, 'fulltext') ?>

	<?php // echo $form->field($model, 'state') ?>

	<?php // echo $form->field($model, 'lang') ?>

	<?php // echo $form->field($model, 'keywords') ?>

	<?php // echo $form->field($model, 'created_user_id') ?>

	<?php // echo $form->field($model, 'created_by_alias') ?>

	<?php // echo $form->field($model, 'created_date_time') ?>

	<?php // echo $form->field($model, 'modified_date_time') ?>

	<?php // echo $form->field($model, 'modified_user_id') ?>

	<?php // echo $form->field($model, 'checked_out') ?>

	<?php // echo $form->field($model, 'checked_out_time') ?>

	<?php // echo $form->field($model, 'publish_up') ?>

	<?php // echo $form->field($model, 'publish_down') ?>

	<?php // echo $form->field($model, 'images') ?>

	<?php // echo $form->field($model, 'urls') ?>

	<?php // echo $form->field($model, 'attribs') ?>

	<?php // echo $form->field($model, 'version') ?>

	<?php // echo $form->field($model, 'ordering') ?>

	<?php // echo $form->field($model, 'metadesc') ?>

	<?php // echo $form->field($model, 'access') ?>

	<?php // echo $form->field($model, 'hits') ?>

	<?php // echo $form->field($model, 'metadata') ?>

	<?php // echo $form->field($model, 'xreference') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
