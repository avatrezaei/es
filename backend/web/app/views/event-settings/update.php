<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\EventSettings */

$this->title = 'Update Event Settings: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Event Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-settings-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
