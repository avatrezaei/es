<div class="scalendar tresult">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#rtab_a" data-toggle="tab">۲۷ <span> تیر</span></a></li>
        <li><a href="#rtab_b" data-toggle="tab">۲۸ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_c" data-toggle="tab">۲۹ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_d" data-toggle="tab">۳۰ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_e" data-toggle="tab">۳۱ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_f" data-toggle="tab">۱ <span class="monthname">مرداد</span></a></li>
        <li><a href="#rtab_g" data-toggle="tab">۲ <span class="monthname">مرداد</span></a></li>
        <li class="deactive"><span class="mn">۳ <span class="monthname">مرداد</span></span></li>
        <li class="deactive"><span class="mn">۴ <span class="monthname">مرداد</span></span></li>
        <li class="deactive"><span class="mn">۵ <span class="monthname">مرداد</span></span></li>
        <li class="totalmedal"><a href="#rtab_t" data-toggle="tab">نتیجه کلی بازی ها</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="rtab_a">
            <p>ورود تیم ها</p>
        </div>

        <div class="tab-pane fade" id="rtab_b">
            <p>افتتاحیه</p>
        </div>

        <div class="tab-pane fade" id="rtab_c">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="align-center">رشته</th>
                            <th>دانشگاه</th>
                            <th>ورزشکار</th>
                            <th class="align-center">مدال </th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-2 align-center">بدمینتون</td>
                            <td class="col-md-3">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                            </td>
                            <td class="col-md-3 ">
                                سعیده موحدزاده 
                            </td>
                            <td class="col-md-1 align-center">
                                <i class="icon-hd-medal font-color-gold font-size-28"></i>
                            </td>
                        </tr>
                         <tr>
                            <td class="col-md-2 align-center">بدمینتون</td>
                            <td class="col-md-3">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                            </td>
                            <td class="col-md-3">
                                سعیده موحدزاده 
                            </td>
                            <td class="col-md-1 align-center">
                                <i class="icon-hd-medal font-color-gold font-size-28"></i>
                            </td>
                        </tr>
                         <tr>
                            <td class="col-md-2 align-center">بسکتبال</td>
                            <td class="col-md-3">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                            </td>
                            <td class="col-md-3 ">
                                سعیده موحدزاده 
                            </td>
                            <td class="col-md-1 align-center">
                                <i class="icon-hd-medal font-color-bronze font-size-28"></i>
                            </td>
                        </tr>
                         <tr>
                            <td class="col-md-2 align-center">بدمینتون</td>
                            <td class="col-md-3">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                            </td>
                            <td class="col-md-3 ">
                                سعیده موحدزاده 
                            </td>
                            <td class="col-md-1 align-center">
                                <i class="icon-hd-medal font-color-silver font-size-28"></i>
                            </td>
                        </tr>
                         <tr>
                            <td class="col-md-2 align-center">بدمینتون</td>
                            <td class="col-md-3">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                            </td>
                            <td class="col-md-3">
                                سعیده موحدزاده 
                            </td>
                            <td class="col-md-1 align-center">
                                <i class="icon-hd-medal font-color-gold font-size-28"></i>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="rtab_d">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="rtab_e">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="rtab_f">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="rtab_g">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        
        </div>
                                
                                <div class="tab-pane fade" id="rtab_t">
           <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="align-center">مقام</th>
                            <th>دانشگاه</th>
                            <th class="align-center">
                                <i class="icon-hd-medal font-color-gold"></i> 
                            </th>
                            <th class="align-center">
                                <i class="icon-hd-medal font-color-silver"></i> 
                             </th>
                            <th class="align-center">
                                <i class="icon-hd-medal font-color-bronze"></i> 
                             </th>
                             <th class="align-center">مجموع امتیاز</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-2 align-center">۱</td>
                            <td class="col-md-5">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۵</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۴</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۸</span>
                            </td>
                            <td class="col-md-2 align-center">
                                <div class="respan">
                                    <span>۴۵</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 align-center">۲</td>
                            <td class="col-md-5">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و فرهنگ
                                </span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۵</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۴</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۸</span>
                            </td>
                            <td class="col-md-2 align-center">
                                <div class="respan">
                                    <span>۴۵</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 align-center">۳</td>
                            <td class="col-md-5">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۵</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۴</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۸</span>
                            </td>
                            <td class="col-md-2 align-center">
                                <div class="respan">
                                    <span>۴۵</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2 align-center">۴</td>
                            <td class="col-md-5">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۵</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۴</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>۸</span>
                            </td>
                            <td class="col-md-2 align-center">
                                <div class="respan">
                                    <span>۴۵</span>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
                                </div>
    </div>
</div>