<?php

define('_GIRLSEVENT', 1);
define('BOYSEVENT', 5);

function DBConnect() {
    $dsn = 'mysql:dbname=sporteventdb;host=localhost;port=3306';
    //$dsn = 'mysql:dbname=sporteventdb;host=172.20.7.76;port=3306';

 
    $username = "reportuser";
    //$username = "daliri";

        
    $password = "GR3G0R1BoyInTehran";
    
    $dbname = "sporteventdb";

try {
//    $db = new PDO($dsn, $username, $password, array (PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $db = new PDO($dsn, $username, $password);

    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch(PDOException $e) {
    die('Could not connect to the database:<br/>' . $e);
}

$db->exec("set names utf8");
return $db;
   
}

/*
 * 
 */
function getFields($eventID, $fieldID) {
    
    $db = DBConnect();
    
    $gender[1] = 'woman';
    $gender[5] = 'man';
        
    $statement = $db->prepare('select t3.name,t3.id,t1.entryDate,t1.exitDate
                                from authorized_fields t1
                                join fields as t2 on t1.fieldsid=t2.id
                                join subfields t3 on t2.id=t3.fieldsid
                                where t1.valid=1 and t2.valid=1 and t3.valid=1 
                                    and t1.eventid=:eventid and t2.id=:fieldid 
                                    and (t3.gender = "both" OR t3.gender = :gender)
                                group by t3.id order by t3.name');

    $statement->bindValue(':eventid', $eventID);
    $statement->bindValue(':fieldid', $fieldID);
    $statement->bindValue(':gender', $gender[$eventID]);

    
    $statement->execute();
    
    $fields = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $fields;
    
}
/*
 * 
 */
function getField($eventID, $fieldID) {
    
    $db = DBConnect();
    
    $statement = $db->prepare('select t2.name,t2.id,t1.entryDate,t1.exitDate
                                from authorized_fields t1
                                join fields as t2 on t1.fieldsid=t2.id
                                where t1.valid=1 and t2.valid=1 
                                    and t1.eventid=:eventid and t2.id=:fieldid group by t2.id');

    $statement->bindValue(':eventid', $eventID);
    $statement->bindValue(':fieldid', $fieldID);
    
    $statement->execute();
    
    $field = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $field;
    
}
/*
 * 
 */
function getUniversities($eventID, $fieldID, $subfieldID = NULL) {
    
    $db = DBConnect();
    
    $statement = $db->prepare('SELECT t2.id,t2.name,t1.id as teamid
                                FROM `teams` t1
                                join university t2 on t1.universityid = t2.id 
                                where t1.valid=1 and t2.valid=1 and t1.eventid=?
                                    and t1.fieldid=?
                                ORDER BY t2.name ASC ');

    $statement->bindValue(1, $eventID);
    $statement->bindValue(2, $fieldID);
    
    $statement->execute();
    
    $univs = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $univs;
}

/*
 * 
 */
function getRanks($eventID, $index = 'score') {
    
    $db = DBConnect();
    
    if($index == 'score') {
        $statement = $db->prepare(' SELECT university.id,name,rank1,rank2,rank3,rank4,rank5,rank6,point,rank 
                                    FROM `university_ranking` 
                                    LEFT JOIN university on(university.id=`university_ranking`.university_id) 
                                    where university_ranking.event_id=?
                                        and university_ranking.valid=1 order by point desc  ');
    
        $statement->bindValue(1, $eventID);
        
    }
    else {
        $statement = $db->prepare('SELECT university.id,name,rank1,rank2,rank3
                                    FROM `university_ranking` 
                                    LEFT JOIN university on(university.id=`university_ranking`.university_id) 
                                    where university_ranking.event_id=? and university_ranking.valid=1 
                                    order by rank1 DESC , rank2 DESC , rank3 DESC ');
    
        $statement->bindValue(1, $eventID);
        
    }
    
    $statement->execute();
    
    $univs = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    //print_r($univs);
    
    return $univs;
    
}

/*
 * 
 */
function getDuration($eventID, $end = NULL, $enm = NULL, $exd = NULL, $exm = NULL) {
    
    $enum = array(0,1,2,3,4,5,6,7,8,9);
    $pnum = array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹');
    
    $end = str_replace($pnum, $enum, $end);
    $exd = str_replace($pnum, $enum, $exd);

    
    if($exd < $end) {
        for($i=$end; $i<=31; $i++) {
            
            $eDuration[] = $i . ' '. $enm; 
        }
        for($i=1; $i<=$exd; $i++) {
            
            $eDuration[] = $i . ' '. $exm;
        }
    }
    else {
        for($i=$end; $i<=$exd; $i++) {
            $eDuration[] = $i . ' '. $enm;
        }
    }
    //print_r($eDuration);
    return $eDuration;
}

/*
 * 
 */
function convertNum($from, $to, $ary) {
   
    $enum = array(0,1,2,3,4,5,6,7,8,9);
    $pnum = array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹');
    
    return str_replace($$from, $$to, $ary);
}

/*
 * 
 */
function getTeamMembers($eventID, $teamID) {
    
    $db = DBConnect();
    
    $statement = $db->prepare('SELECT `user`.id, `user`.name, `user`.last_name, 
                                      `user`.fileId, `event_members`.user_type_id AS `user_type_id` 
                                FROM `user` LEFT JOIN `event_members` ON `user`.`id` = `event_members`.`userId` 
                                WHERE ((`user`.valid=1) AND ((((`event_members`.valid=1) 
                                        AND (`event_members`.eventId=?)) AND (`teamsId`=?))
                                        AND (`user_type_id` IN (16, 17, 18, 19, 2)))) 
                                        ORDER BY `user_type_id` Desc, `user`.last_name');

    $statement->bindValue(1, $eventID);
    $statement->bindValue(2, $teamID);
    
    $statement->execute();
    
    $members = $statement->fetchAll(PDO::FETCH_ASSOC);
    return $members;
    
    
}

/*
 * 
 */
function getImageFile($fileID) {
    $db = DBConnect();
    
    $statement = $db->prepare('SELECT hashed_name FROM file WHERE id = ?');

    $statement->bindValue(1, $fileID);
   //echo 'SELECT hashed_name FROM file WHERE id='.$fileID;
    $statement->execute();
    $hash = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    //print_r($hash);
    return 'http://sems.um.ac.ir/YiiFileManager/file/view-file?name='.$hash[0]['hashed_name'];

}

/*
 * 
 */
function makeResult($eventID, $fieldID) {
    $resultArray = array();
    //if($eventID == 1) {
    $context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
    $url = 'http://api.sems.um.ac.ir/v1/match/games?fieldId='.$fieldID.'&eventId='.$eventID;

$xml = file_get_contents($url, false, $context);
$xml = simplexml_load_string($xml);
//echo $xml->success;
$pmonth = array('04' => 'تیر', '05'=>'مرداد');

$eventStart[1] = array('day' => '28', 'month' => 'تیر');
$eventStart[5] = array('day' => '11', 'month' => 'مرداد');

$eventEnd[1] = array('day' => '5', 'month' => 'مرداد');
$eventEnd[5] = array('day' => '20', 'month' => 'مرداد');
//$eDuration = getDuration($eventID, 27, 'تیر', 5, 'مرداد');

$eDuration = getDuration($eventID, $eventStart[$eventID]['day'], $eventStart[$eventID]['month'], 
                                   $eventEnd[$eventID]['day'], $eventEnd[$eventID]['month']);

//echo $eventID;die;

$subfields = getFields($eventID, $fieldID);
//print_r($subfields);die;
//$todayDuration = getDuration(1,$todayDay, $todayMonth, 28, '04');
$data = $xml->data;
//print_r($data);

$games = $xml->data->Games;
//echo count($games);
if(!count($games))
    return false;

    foreach($games as $game) {
        //echo $game->date;
        $item = array();
        //echo $game->date.'**';
        if(!empty($game->date)) {
         list($y, $m, $d) = explode ('/', $game->date);
        $mn = $pmonth[$m];
        //$td = getDuration(1,$d, $mn, $d, $mn);
        $tIndex = array_search((int)$d.' '.$mn, $eDuration);
       // print_r($td);
        //echo $tIndex;
        $item['group'] = $game->group->name;
        $item['starttime'] = $game->starttime;
        $univ = array();
        foreach($game->participants->item as $par) {
            $univ[] = array( 'name' => $par->participant->name, 
                             'result' => $par->result,
                             'resultstatus' => $par->resultstatus,
                             'rank' => $par->rank);
        }
        $item['univ'] = $univ;
      //  print_r($item);
        if(!empty($game->subfieldname))
            $subIndex = "$game->subfieldname";
        else
            $subIndex = 0;
        //echo $subIndex;
        $resultArray[$tIndex][$subIndex][] = $item;
        //print_r($resultArray);
        //echo '<hr>';

        //echo $m;
       // echo $game->date;
       // echo $game->fieldId;
        
        //echo '<hr>';
        }
    }
  //  }
   // print_r($resultArray);
    return $resultArray;
}
?>