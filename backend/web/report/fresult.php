<div class="scalendar rcalendar">
    
    <ul class="nav nav-tabs">
<?php

list($enday, $enmonth, $enyear) = explode(' ', $entryDate);
list($exday, $exmonth, $exyear) = explode(' ', $exitDate);
list($todayDay, $todayMonth, $todayYear) = explode(' ', $today);

$eventStart[1] = array('day' => '27', 'month' => 'تیر');
$eventStart[5] = array('day' => '11', 'month' => 'مرداد');

$eventEnd[1] = array('day' => '5', 'month' => 'مرداد');
$eventEnd[5] = array('day' => '20', 'month' => 'مرداد');


$eDuration = getDuration($eventID, $eventStart[$eventID]['day'], $eventStart[$eventID]['month'], 
                                   $eventEnd[$eventID]['day'], $eventEnd[$eventID]['month']);


$fDuration = getDuration($eventID, $enday, $enmonth, $exday, $exmonth);
$todayDuration = getDuration($eventID,$todayDay, $todayMonth, $todayDay, $todayMonth);

//print_r($todayDuration);


$enindex = array_search($fDuration[0], $eDuration);
end($fDuration);
$exindex = key($fDuration);
//print_r($fDuration);
$exindex = array_search($fDuration[$exindex], $eDuration);
//$todayIndex = array_search($toda, $haystack)
//echo $exindex;
$eDuration = convertNum('enum', 'pnum', $eDuration);
$todayIndex = array_search($todayDay.' '.$todayMonth, $eDuration);

foreach($eDuration as $key=>$value) {
   
    if($key < $enindex || $key > $exindex)
       echo '<li class="deactive"><span class="mn">'.$value.'</span></li>';
    else {
       echo '<li';
       if($key == $todayIndex)
               echo ' class="active" ';
       echo '><a href="#stab-r-'.($key+1).'" data-toggle="tab">'.$value.'</a></li>'; 
    }
    
    }
 

?>
       <?php if($eventID == 1) { ?> 
        <li>
            <a style="background:#C30;color:#fff;" href="#stab-r-f" data-toggle="tab">نتیجه نهایی</a>
        </li>
       <?php } ?>
    </ul>
</div>
<div class="scalendar rcalendar">
    <div class="tab-content">
    <?php
       // if($fieldID != 178 && $fieldID != 4) {
        $medal[] = 'gold';
        $medal[] = 'silver';
        $medal[] = 'bronze';
        $results = makeResult($eventID, $fieldID);
        
        if($results) {
        //print_r($subfields);
               
        for($i = $enindex; $i<= $exindex; $i++) {
             
            echo '<div class="tab-pane fade in ';
            if($i == $todayIndex)
                echo ' active';
            echo '" id="stab-r-'.($i+1).'">';
            
            if(!empty($results[$i])) {

            $tdResult = $results[$i];
              
            if(empty($subfields))
                $hsubfields = array(0 => array('name' => 0));
            else
                $hsubfields = $subfields;
//            print_r($hsubfields);die;
            foreach($hsubfields as $k => $v) { 
                if(!empty($tdResult[$v['name']])) {
                    $data = $tdResult[$v['name']];
                    //print_r($data);echo '<hr>';
                    echo '<div style="margin-bottom:50px;margin-top:10px;">';
                    if($v['name'])
                        echo '<h4>'.convertNum('enum','pnum',$v['name']).'</h4>';
                    echo '<table class="tbl1 table-striped table-hover">'."\n";
                    
                    echo '<tr>'."\n";
                    
                    if($fieldID != 178 && $fieldID != 4 && $fieldID != 38) {
                        echo '<th>گروه</th>'."\n".'
                            <th colspan="2">مسابقه بین دانشگاه</th>'."\n".'
                            <th class="align-center">ساعت برگزاری</th>'."\n".'
                            <th class="align-center">نتیجه رقابت</th>'."\n";
                    }
                    else {
                        if($fieldID == 38) {
                            echo '<th colspan="3">شرکت کننده</th>'."\n".'
                                  <th class="align-center">ساعت برگزاری</th>'."\n".'
                                  <th class="align-center">نتیجه رقابت</th>'."\n";
                            
                        }
                        else
                            echo '
                                <th colspan="2">شرکت کننده</th>'."\n".'
                                <th class="align-center">نتیجه رقابت</th>'."\n"; 
                    }
                    echo '</tr>'."\n";
                    
                   // foreach ($data as $ki) {
                      //print_r($tdr);
                    $ki = $data;
                      for($w = 0; $w < count($ki); $w++) {
                        if($fieldID != 178 && $fieldID != 4 ) {
                            echo '<tr>';
                            if($fieldID != 38)
                                echo '<td>'.$ki[$w]['group'].'</td>';
                            else {
                                $rank1 = $ki[$w]['univ'][0]['rank'];
                                $rank2 = $ki[$w]['univ'][1]['rank'];

                                echo '<td style="width:40px;">'."\n".'<span class="respan">&nbsp;';
                                
                                if(in_array($rank1, array(1,2,3)))
                                    echo '<i class="icon-hd-medal2 font-color-'.$medal[$rank1-1].' font-size-20"></i>';
                                echo '</span><span class="respan">&nbsp;';
                                if(in_array($rank2, array(1,2,3)))
                                    echo '<i class="icon-hd-medal2 font-color-'.$medal[$rank2-1].' font-size-20"></i>';
                                echo '</td>'."\n";
                            }
                            echo '<td style="width:40px;">'."\n".'<span class="respan">&nbsp;';
                            
                            if($ki[$w]['univ'][0]['resultstatus'] == 2)
                                echo '<i class="icon-hd-hold font-color-green  font-size-20"></i>';
                            
                            echo '</span><span class="respan">&nbsp;';
                            if($ki[$w]['univ'][1]['resultstatus'] == 2)
                                echo '<i class="icon-hd-hold font-color-green  font-size-20"></i>';
                            
                            echo '</span></td>'."\n";
                            echo '<td>'."\n".'<span class="respan">'.$ki[$w]['univ'][0]['name'];
//                            if($ki[$w]['univ'][0]['resultstatus'] == 2)
//                                echo '<i class="icon-hd-hold font-color-green  font-size-20"></i>';
                            echo '</span>'
                                    . ' <span class="respan">'.$ki[$w]['univ'][1]['name'];
//                            echo '<td style="width:40px;">';
//                            if($ki[$w]['univ'][1]['resultstatus'] == 2) 
//                                echo '<i class="icon-hd-hold font-color-green font-size-20"></i>';
                            //echo '</td>'."\n";
                            echo '</span></td>'."\n";
                            echo '<td class="align-center">'.mb_substr($ki[$w]['starttime'],0,  mb_strrpos($ki[$w]['starttime'], ':')).'</td>'."\n";
                            $result1 = str_replace('null', '', $ki[$w]['univ'][0]['result']);
                            $result2 = str_replace('null', '', $ki[$w]['univ'][1]['result']);
                        
                            echo '<td class="align-center"><span class="respan">'.convertNum('enum','pnum' ,$result1).'</span>'
                                    . '<span class="respan">'.convertNum('enum','pnum' ,$result2).'</span></td>'."\n";
                            
                            echo '</tr>'."\n";
                        }
                        else {
                            echo ' ( <i class="glyphicon glyphicon-time"></i> ساعت برگزاری: '
                                 .mb_substr($ki[$w]['starttime'],0,  mb_strrpos($ki[$w]['starttime'], ':')).' )';
                            for($h = 0; $h < count($ki[$w]['univ']); $h++) {
                                //$rank = '';
                                $result1 = str_replace('null', '', $ki[$w]['univ'][$h]['result']);
                                //echo $ki[$w]['univ'][$h]['result'].'--';
                                if($result1) {
                                echo '<tr>'."\n";
                                
                                //print_r($ki[$w]['univ'][$h]); echo '<hr>';
                                $rank = $ki[$w]['univ'][$h]['rank'];
                                echo '<td style="width:40px;">';
                                
                                if($rank <= 3 && $rank!=0)
                                    echo '<i class="icon-hd-medal2 font-color-'.$medal[$rank-1].' font-size-20"></i>';
                                echo '</td>'."\n";
                                echo '<td>'.$ki[$w]['univ'][$h]['name']. '</td>'."\n";
                            
                                echo '<td class="align-center">'.convertNum('enum','pnum' ,$result1).'</td>'."\n";
                            
                                echo '</tr>'."\n"; 
                                }
                            }
                        
                        }
                      }
                  //  }
                    
                    echo '</table></div>'."\n";
                }
            }
            }
            else {
                //die;
                echo '<div style="margin:10px 20px;">';
                if($i == $todayIndex) 
                    echo 'منتظر اعلام نتایج باشید.';
                else
                    echo ' نتیجه ای منتشر نشده است.';
                
                echo '</div>';
            }
            
            
//            $resfilename = 'files/result/'.$fieldID.'/res-'.$eventPrefix[$eventID].'-'.$fieldID.'-'.($i+1).'.html';
//           
//            if(file_exists($resfilename)) {
//                $sfile = fopen($resfilename, 'r');
//                $scontent = fread($sfile, filesize($resfilename));
//                $scontent = str_replace('tbl1', 'mtbl', $scontent);
//                echo convertNum('enum', 'pnum',$scontent);
//                
//            }
//            else {
//                if($i == $todayIndex) 
//                    echo 'منتظر اعلام نتایج باشید.';
//                else
//                    echo 'هنوز رقابتی برگزار نشده است.';
//            }
            echo '</div>';
        }
        }
//        }
//        else {
//            echo '<div style="margin:10px 20px;">';
//            echo 'منتظر اعلام نتایج باشید.'; 
//            echo '</div>';
//        }
    ?>
        
    <div class="tab-pane fade" id="stab-r-f">
       <?php
       $resfilename = 'files/result/'.$fieldID.'/res-'.$eventPrefix[$eventID].'-'.$fieldID.'-f.html';
            if(file_exists($resfilename)) {
                $resfile = fopen($resfilename, 'r');
                $rescontent = fread($resfile, filesize($resfilename));
                $rescontent = str_replace('tbl1', 'mtbl', $rescontent);
                echo convertNum('enum', 'pnum',$rescontent);
                
            }
       
       ?>
    </div>   
        
    </div>
</div>

    <!--
    <ul class="nav nav-tabs">
        <li class="active"><a href="#rtab_a" data-toggle="tab">۲۷ <span> تیر</span></a></li>
        <li><a href="#rtab_b" data-toggle="tab">۲۸ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_c" data-toggle="tab">۲۹ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_d" data-toggle="tab">۳۰ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_e" data-toggle="tab">۳۱ <span class="monthname"> تیر</span></a></li>
        <li><a href="#rtab_f" data-toggle="tab">۱ <span class="monthname">مرداد</span></a></li>
        <li><a href="#rtab_g" data-toggle="tab">۲ <span class="monthname">مرداد</span></a></li>
        <li class="deactive"><span class="mn">۳ <span class="monthname">مرداد</span></span></li>
        <li class="deactive"><span class="mn">۴ <span class="monthname">مرداد</span></span></li>
        <li class="deactive"><span class="mn">۵ <span class="monthname">مرداد</span></span></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="rtab_a">
            <p>ورود تیم ها</p>
        </div>

        <div class="tab-pane fade" id="rtab_b">
            <p>افتتاحیه</p>
        </div>

        <div class="tab-pane fade" id="rtab_c">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>محل برگزاری</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-3">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3"></td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td class="col-md-3"></td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td class="col-md-3"></td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3"></td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="rtab_d">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="rtab_e">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="rtab_f">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="rtab_g">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                            <th>نتیجه</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-3">۱۰:۳۰ صبح</td>
                            <td class="col-md-6">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td class="col-md-3">
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <div class="respan">
                                    <span>۰</span>
                                    <span>۲</span> 
                                </div>
                                <div class="respan">
                                    <span>۱</span>
                                    <span>۳</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>-->
</div>