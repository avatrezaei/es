
<div class="bs-glyphicons bg-color-purple" id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 960px; height: 150px; overflow: hidden; visibility: hidden;">

    <div class="bs-glyphicons-list" data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden;">
        <div class="hitm" style="display: none;">
            <a href="">
                <i class="icon-fum-badminton"></i> 
                <span class="glyphicon-class">بدمینتون</span> 
            </a>
        </div>
        <div class="hitm"  style="display: none;">
            <a href="">
                <i class="icon-fum-basketball"></i> 
                <span class="glyphicon-class">بستکبال</span> 
            </a>
        </div>
        <div class="hitm"  style="display: none;">
            <a href="">
                <i class="icon-fum-ping-pong"></i>
                <span class="glyphicon-class">تنیس روی میز</span> 
            </a>
        </div>
        <div class="hitm"  style="display: none;">
            <a href="">
                <i class="icon-fum-taekwondo"></i> 
                <span class="glyphicon-class">تکواندو</span> 
            </a>
        </div>
        <div class="hitm"  style="display: none;">
            <a href="">
                <i class="icon-fum-track"></i> 
                <span class="glyphicon-class">دوومیدانی</span> 
            </a>
        </div>
        <div  class="hitm" style="display: none;">
            <a href="">
                <i class="icon-fum-swimming"></i> 
                <span class="glyphicon-class">شنا</span> 
            </a>
        </div>
        <div class="hitm"  style="display: none;">
            <a href="">
                <i class="icon-fum-football"></i> 
                <span class="glyphicon-class">فوتسال</span> 
            </a>
        </div>
        <div  class="hitm" style="display: none;">
            <a href="">
                <i class="icon-fum-volleyball"></i> 
                <span class="glyphicon-class">والیبال</span> 
            </a>
        </div>


    </div>

    <!-- Arrow Navigator -->
    <span data-u="arrowleft" class="jssora03l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2"></span>
    <span data-u="arrowright" class="jssora03r" style="top:0px;right:8px;width:55px;height:55px;" data-autocenter="2"></span>
</div>

