<?php
include 'functions.php';

$eventID = (int) @$_GET['e'];
$guniversitiesByScore['girls'] = getRanks($eventID, 'score');
//$buniversitiesByScore['boys'] = getRanks(5, 'score');

//print_r($universitiesByScore['girls']);
$labels['girls'] = 'دختران';
$labels['boys'] = 'پسران';

$eventName[1] = 'دختران';
$eventName[5] =  'پسران';

$rank = array('0','مقام اول','مقام دوم','مقام سوم','مقام چهارم','مقام پنجم','مقام ششم','مقام هفتم');

foreach($guniversitiesByScore as $key=>$value) {
//print_r($value);
    echo '<h2>'.$eventName[$eventID].'</h2>';
    echo '<div class="scalendar tresult">';
    /*
    echo '<div class="update-time">
        <i class="glyphicon glyphicon-refresh font-color-red"></i>
        <span class="font-color-red"> آخرین تاریخ بروزرسانی:</span> یکشنبه ۲۰ تیرماه ۱۳۹۵ - ساعت ۱۷:۰۰
        </i>
    </div>';
    */
    echo '<div class="tab-content1">
           <div class="" id="rtab_t">
             <div>
                <table class="tbl1 table-striped table-hover" id="'.$key.'-sdtble">
                    <thead>
                        <tr>
                            <th class="align-center col-md-1">رتبه بندی</th>
                            <th class="col-md-4">دانشگاه</th>
                            <th class="align-center col-md-1">مقام اول </th>
                            <th class="align-center col-md-1">مقام دوم </th>
                            <th class="align-center col-md-1">مقام سوم</th>
                            <th class="align-center col-md-1">مقام چهارم</th>
                            <th class="align-center col-md-1">مقام پنجم </th>
                            <th class="align-center col-md-1">مقام ششم </th>
                            <th class="align-center col-md-1">مجموع امتیاز</th>
                        </tr>
                    </thead>';
    
    echo '<tbody>';
    foreach($value as $i=>$j) {
    
        $logoFile = 'img/unis-logo/' . $j['id'] . '.jpg';
        if(!file_exists($logoFile))
            $logoFile = 'img/unis-logo/no-logo.jpg';
        
        echo '           <tr>
                            <td class="col-md-1 align-center">'.convertNum('enum', 'pnum',$j['rank']).'</td>
                            <td class="col-md-4">
                                <span class="unispan">
                                    <img class="unicon" src="'.$logoFile.'">'.$j['name'].'
                                </span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.convertNum('enum', 'pnum',$j['rank1']).'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.convertNum('enum', 'pnum',$j['rank2']).'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.convertNum('enum', 'pnum',$j['rank3']).'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.convertNum('enum', 'pnum',$j['rank4']).'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.convertNum('enum', 'pnum',$j['rank5']).'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.convertNum('enum', 'pnum',$j['rank6']).'</span>
                            </td>
                            <td class="col-md-2 align-center">
                                <div class="respan">
                                    <span>'.convertNum('enum', 'pnum',$j['point']).'</span>
                            </td>
                        </tr>';
    }
    echo '</tbody>';
    
    echo '</table>';
    echo '</div></div></div></div>';
}   

?>


    <script>
        $(document).ready(function() {
            $("[id$='-sdtble']").DataTable( {
                "bSort": false,
                "pageLength": 25,
                "language": {
                    "search": "جستجو ",
                    "paginate": {
                        "first":      "اولین",
                        "last":       "آخرین",
                        "next":       "بعدی",
                        "previous": "قبلی"
                    },
                    "emptyTable": "اطلاعاتی موجود نیست",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ مورد",
                    "lengthMenu":     "نمایش _MENU_ مورد",
                }
            });
        });
    
    </script>