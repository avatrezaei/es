<?php
    include 'functions.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>نتایج سیزدهمین المپیاد فرهنگی ورزشی دانشجویان سراسر کشور - دانشگاه فردوسی مشهد</title>
    <link href="bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/semsfont.css" rel="stylesheet">
    <link href="css/hd-fonts.css" rel="stylesheet">
    <link href="css/datatable.min.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12" class="main">
                <div class="row">
                    <div class="md-col-12 header">
                        <div class="logo">
                            <a href="http://olympiad13.um.ac.ir/" target="_blank"><img src="img/olympiadlogo-b.png"></a>
                        </div>
                    </div>
                </div>