<?php

?>
<div class="col-md-3">

        <ul class="nav nav-pills nav-stacked">
            <li class="active">
                <a href="#tab_b" data-toggle="pill">
                    <i class="icon-hd-chart font-size-22"></i>جدول گروه بندی
                </a>
            </li>
            <li>
                <a href="#tab_c" data-toggle="pill">
                    <i class="glyphicon glyphicon-time font-size-22"></i> برنامه زمان بندی
                </a>
            </li>
            <li>
                <a href="#tab_d" data-toggle="pill">
                    <i class="icon-hd-cup font-size-22"></i> نتایج بازی ها
                </a>
            </li>

        </ul>
    </div>
    <div class="tab-content col-md-9">
        <div class="tab-pane active" id="tab_b">
            <h4 class="font-color-black font-bold">جدول گروه بندی</h4>
            <div class="scalendar">
            <?php
                $dfilename = 'files/draw/d-'.$eventPrefix[$eventID].'-'.$fieldID.'.html';
                if(file_exists($dfilename)) {
                    $dfile = fopen($dfilename, 'r');
                    $dcontent = fread($dfile, filesize($dfilename));
                    $dcontent = str_replace('tbl1', 'mtbl', $dcontent);
                    echo convertNum('enum', 'pnum',$dcontent);

                }
            
            ?>
            
        </div>
        
        </div>

        <div class="tab-pane" id="tab_c">
            <h4 class="font-color-black font-bold">برنامه زمانبندی مسابقات </h4>
            <div>
<?php include 'schedule.php'; ?>
            </div>
        </div>

        <div class="tab-pane" id="tab_d">
            <h4 class="font-color-black font-bold">نتایج بازی ها</h4>
            <div>
<?php include 'fresult.php'; ?>
            </div>
        </div>


    </div><!-- tab content -->