
            </div>
        </div>   
        
    </div>
    <div class="footer"><a href="http://olympiad13.um.ac.ir/" target="_blank">
            سیزدهمین المپیاد فرهنگی ورزشی دانشجویان    </a>/ 
        <a href="http://www.um.ac.ir" >دانشگاه فردوسی مشهد</a></div>
 
    <script src="bootstrap/js/bootstrap.min.js"></script>  
    <script src="js/datatable.min.js"></script>
    <script src="js/datatables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $("[id$='-dtble']").DataTable( {
                "bSort": false,
                "pageLength": 25,
                "language": {
                    "search": "جستجو ",
                    "paginate": {
                        "first":      "اولین",
                        "last":       "آخرین",
                        "next":       "بعدی",
                        "previous": "قبلی"
                    },
                    "emptyTable": "اطلاعاتی موجود نیست",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ مورد",
                    "lengthMenu":     "نمایش _MENU_ مورد",
                }
            });
            
//            $("[id$='-dtble']").DataTable( {
//                "bSort": false,
//                "pageLength": 25,
//                "language": {
//                    "search": "جستجو ",
//                    "paginate": {
//                        "first":      "اولین",
//                        "last":       "آخرین",
//                        "next":       "بعدی",
//                        "previous": "قبلی"
//                    },
//                    "emptyTable": "اطلاعاتی موجود نیست",
//                    "info": "نمایش _START_ تا _END_ از _TOTAL_ مورد",
//                    "lengthMenu":     "نمایش _MENU_ مورد",
//                }
//            });
        } );
        
        
    
    </script>
        
    
</body>
</html>