<?php
$universitiesByScore['girls'] = getRanks($eventID, 'score');
//$universitiesByScore['boys'] = getRanks(5, 'score');

//print_r($universitiesByScore['girls']);
$labels['girls'] = 'دختران';
$labels['boys'] = 'پسران';

$rank = array('0','مقام اول','مقام دوم','مقام سوم','مقام چهارم','مقام پنجم','مقام ششم','مقام هفتم');

foreach($universitiesByScore as $key=>$value) {
//print_r($value);
    echo '<h2>'.$labels[$key].'</h2>';
    echo '<div class="scalendar tresult">';
    /*
    echo '<div class="update-time">
        <i class="glyphicon glyphicon-refresh font-color-red"></i>
        <span class="font-color-red"> آخرین تاریخ بروزرسانی:</span> یکشنبه ۲۰ تیرماه ۱۳۹۵ - ساعت ۱۷:۰۰
        </i>
    </div>';
    */
    echo '<div class="tab-content1">
           <div class="" id="rtab_t">
             <div>
                <table class="tbl1 table-striped table-hover" id="'.$key.'-dtble">
                    <thead>
                        <tr>
                            <th class="align-center col-md-1">رتبه بندی</th>
                            <th class="col-md-4">دانشگاه</th>
                            <th class="align-center col-md-1">مقام اول </th>
                            <th class="align-center col-md-1">مقام دوم </th>
                            <th class="align-center col-md-1">مقام سوم</th>
                            <th class="align-center col-md-1">مقام چهارم</th>
                            <th class="align-center col-md-1">مقام پنجم </th>
                            <th class="align-center col-md-1">مقام ششم </th>
                            <th class="align-center col-md-1">مجموع امتیاز</th>
                        </tr>
                    </thead>';
    
    echo '<tbody>';
    foreach($value as $i=>$j) {
    
        echo '           <tr>
                            <td class="col-md-1 align-center">'.$j['rank'].'</td>
                            <td class="col-md-4">
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/'.$j['id'].'.jpg">'.$j['name'].'
                                </span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.$j['rank1'].'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.$j['rank2'].'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.$j['rank3'].'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.$j['rank4'].'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.$j['rank5'].'</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>'.$j['rank6'].'</span>
                            </td>
                            <td class="col-md-2 align-center">
                                <div class="respan">
                                    <span>'.$j['point'].'</span>
                            </td>
                        </tr>';
    }
    echo '</tbody>';
    
    echo '</table>';
    echo '</div></div></div></div>';
}   

?>
