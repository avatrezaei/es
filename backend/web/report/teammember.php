<?php
include 'functions.php';

$eventID = (int) @$_GET['e'];
$teamID = (int) @$_GET['t'];

$members = getTeamMembers($eventID, $teamID);
$role = array('16' => 'مربی', '17' => 'سرمربی', '18' => 'کمک مربی', '19' => 'سرپرست تیم');

foreach($members as $member) {
    if($member['user_type_id'] == 2) {
        $players[] = $member;
    }
    else
        $techs[] = $member;
}

//print_r($players);
echo '<div class="teams temember">';
if(@$techs) {
echo '<h4>کادر فنی</h4><div class="bs-glyphicons bg-color-purple"><ul class="bs-glyphicons-list">';

foreach($techs as $tech) {
    
    echo '<li><img src='.  getImageFile($tech['fileId']).' class="rounded"><span class="mem-name">'.$tech['name'].' '.$tech['last_name']
               .'  <span class="font-color-blue">'
               .$role[$tech['user_type_id']].'</span></span>';
    echo '</li>';
}
echo '</ul></div>';
}

if(@$players) {
echo '<h4>ورزشکاران</h4><div class="bs-glyphicons bg-color-purple"><ul class="bs-glyphicons-list">';
foreach($players as $player) {
    
    echo '<li><img src='.  getImageFile($player['fileId']).'  class="rounded">'
            . '<span class="mem-name">'.$player['name'].' '.$player['last_name']. '</span></li>';
}
echo '</ul></div>';
}
echo '</div>';
?>