<?php
include 'header.php';
?>

<div class="intrpage">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_fld" data-toggle="tab">رشته ها</a></li>
        <li><a href="#tab_rsc_sel" data-toggle="tab">رده بندی بر اساس امتیاز</a></li>
        <!--<li><a href="#tab_rmd" data-toggle="tab">رده بندی بر اساس رنگ مدال</a></li>-->
    </ul>
    <div class="tab-content intrpane">
        <div class="tab-pane fade in active" id="tab_fld">
            <?php
            include 'fields.php';
            ?>
        </div>
        <div class="tab-pane fade" id="tab_rsc_sel">
            <?php
           // include 'tresult.php';
            ?>
        </div>
        <div class="tab-pane fade" id="tab_rmd">
            <?php
            //include 'tmresult.php';
            ?>
        </div>

    </div><!-- tab content -->   


</div>
<script>
    
	

    $(document).ready(function() { 

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
  $(target).html('<div class="ajax-loader"><img src="img/ajax-loader.gif"></div>');
  
  //alert(target);
  fileurl = target.substr(1);
  //alert(fileurl+".php");
//  if ($(target).is(':empty')) {
    $.ajax({
      type: "GET",
      url: fileurl+".php",
      error: function(data){
       // alert("There was a problem");
      },
      success: function(data){
         // alert(data);
        $(target).html(data);
      }
  })
// }
})
    });
    </script>


<?php
include 'footer.php';
?>