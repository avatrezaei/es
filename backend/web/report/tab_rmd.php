<?php
include 'functions.php';
$muniversitiesByScore['girls'] = getRanks($eventID, 'medal');
//$muniversitiesByScore['boys'] = getRanks(5, 'medal');

//print_r($universitiesByScore['girls']);
$labels['girls'] = 'دختران';
$labels['boys'] = 'پسران';

$rank = array('0','مقام اول','مقام دوم','مقام سوم','مقام چهارم','مقام پنجم','مقام ششم','مقام هفتم');

foreach($muniversitiesByScore as $key=>$value) {
//print_r($value);
    echo '<h2>'.$labels[$key].'</h2>';
    echo '<div class="scalendar tresult">';
    /*
    echo '<div class="update-time">
        <i class="glyphicon glyphicon-refresh font-color-red"></i>
        <span class="font-color-red"> آخرین تاریخ بروزرسانی:</span> یکشنبه ۲۰ تیرماه ۱۳۹۵ - ساعت ۱۷:۰۰
        </i>
    </div>';
    */
    
     echo '<div class="tab-content1">
           <div class="" id="rtab_t">
             <div>
                <table class="tbl1 table-striped table-hover" id="'.$key.'-mdtble">
                    <thead>
                        <tr>
                            <th class="align-center">رتبه بندی</th>
                            <th>دانشگاه</th>
                            <th class="align-center">
                                <i class="icon-hd-medal2 font-color-gold"></i> 
                            </th>
                            <th class="align-center">
                                <i class="icon-hd-medal2 font-color-silver"></i> 
                            </th>
                            <th class="align-center">
                                <i class="icon-hd-medal2 font-color-bronze"></i> 
                            </th>
                            <th class="align-center">مجموع مدال</th>
   
                        </tr>
                    </thead>';
    
    echo '<tbody>';
    $rowno = 0;
    foreach($value as $i=>$j) {
        $rowno++;
       // print_r($j);
        $logoFile = 'img/unis-logo/' . $j['id'] . '.jpg';
        if(!file_exists($logoFile))
            $logoFile = 'img/unis-logo/no-logo.jpg';
        echo '           <tr>
                            <td class="col-md-1 align-center">' . convertNum('enum', 'pnum',$rowno) . '</td>
                            <td class="col-md-4">
                                <span class="unispan">
                                    <img class="unicon" src="'.$logoFile.'">' . $j['name'] . '
                                </span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>' . convertNum('enum', 'pnum', $j['rank1']) . '</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>' . convertNum('enum', 'pnum',$j['rank2']) . '</span>
                            </td>
                            <td class="col-md-1 align-center">
                                <div class="respan">
                                    <span>' . convertNum('enum', 'pnum',$j['rank3']) . '</span>
                            </td>
                            
                            <td class="col-md-2 align-center">
                                <div class="respan">
                                    <span>' . convertNum('enum', 'pnum',($j['rank1']+$j['rank2']+$j['rank3'])) . '</span>
                            </td>
                        </tr>';
    }
    echo '</tbody>';
    
    echo '</table>';
    echo '</div></div></div></div>'; 
        
     
    }
      
?>

<script>
        $(document).ready(function() {
           $("[id$='-mdtble']").DataTable( {
                "bSort": false,
                "pageLength": 25,
                "language": {
                    "search": "جستجو ",
                    "paginate": {
                        "first":      "اولین",
                        "last":       "آخرین",
                        "next":       "بعدی",
                        "previous": "قبلی"
                    },
                    "emptyTable": "اطلاعاتی موجود نیست",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ مورد",
                    "lengthMenu":     "نمایش _MENU_ مورد",
                }
            });
        });
    
    </script>

