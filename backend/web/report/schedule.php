
<div class="scalendar">
    <ul class="nav nav-tabs">
<?php

list($enday, $enmonth, $enyear) = explode(' ', $entryDate);
list($exday, $exmonth, $exyear) = explode(' ', $exitDate);
list($todayDay, $todayMonth, $todayYear) = explode(' ', $today);

$eDuration = getDuration($eventID, 27, 'تیر', 5, 'مرداد');
$fDuration = getDuration($eventID, $enday, $enmonth, $exday, $exmonth);
$todayDuration = getDuration($eventID,$todayDay, $todayMonth, $todayDay, $todayMonth);

//echo $todayMonth;
//print_r($todayDuration);die;

//print_r($todayDuration);


$enindex = array_search($fDuration[0], $eDuration);
end($fDuration);
$exindex = key($fDuration);
//print_r($fDuration);
$exindex = array_search($fDuration[$exindex], $eDuration);
//$todayIndex = array_search($toda, $haystack)
//echo $exindex;
$eDuration = convertNum('enum', 'pnum', $eDuration);
$todayIndex = array_search($todayDay.' '.$todayMonth, $eDuration);

foreach($eDuration as $key=>$value) {
   
    if($key < $enindex || $key > $exindex)
       echo '<li class="deactive"><span class="mn">'.$value.'</span></li>';
    else {
       echo '<li';
       if($key == $todayIndex)
               echo ' class="active" ';
       echo '><a href="#stab-s-'.($key+1).'" data-toggle="tab">'.$value.'</a></li>'; 
    }
    
    }
?>

    </ul>
</div>
        
<div class="scalendar">
    <div class="tab-content">
    <?php
        for($i = $enindex; $i<= $exindex; $i++) {
            echo '<div class="tab-pane fade in ';
            if($i == $todayIndex)
                echo ' active';
            echo '" id="stab-s-'.($i+1).'">';

            $sfilename = 'files/schedule/'.$fieldID.'/s-'.$eventPrefix[$eventID].'-'.$fieldID.'-'.($i+1).'.html';
            if(file_exists($sfilename)) {
                $sfile = fopen($sfilename, 'r');
                $scontent = fread($sfile, filesize($sfilename));
                $scontent = str_replace('tbl1', 'mtbl', $scontent);
                echo convertNum('enum', 'pnum',$scontent);
                
            }
            else
                echo 'در این روز بازی برگزار نمی شود.';

            echo '</div>';
        }
    ?>
    </div>
</div>
<!--        <div class="tab-pane fade in active" id="stab_a">
            <p>ورود تیم ها</p>
        </div>

        <div class="tab-pane fade" id="stab_b">
            <p>افتتاحیه</p>
        </div>

        <div class="tab-pane fade" id="stab_c">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>۱۰:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="stab_d">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>۱۰:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="stab_e">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>۱۰:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="stab-s-10">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>۱۰:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="stab_g">
            <div>
                <table class="tbl1 table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ساعت مسابقه</th>
                            <th>تیم ها</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>۱۰:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۰۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/elmofarhang.jpg"> دانشگاه علم و هنر
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/kashan.jpg"> دانشگاه کاشان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۹:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/mazandaran.jpg"> دانشگاه مازندران
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/hormozgan.jpg"> دانشگا هرمزگان
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>۸:۳۰ صبح</td>
                            <td>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/fum.jpg"> دانشگاه فردوسی مشهد
                                </span>
                                <span class="unispan">
                                    <img class="unicon" src="img/unis-logo/shiraz.jpg"> دانشگاه شیراز
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>-->