<div class="tab-content col-md-12">
    <div class="container" id="tab_a">
        <div class="row">
            <!--  In/Out   -->
            <div class="inout cnttxt col-md-6">
                <h4 class="font-color-black font-bold">تاریخ ورود و خروج تیم ها</h4>
                <div>
                    <span>
                        <i class="glyphicon glyphicon-arrow-right font-color-green"></i> <?php echo @$entryDate; ?>
                    </span>
                    <span>
                        <i class="glyphicon glyphicon-arrow-left font-color-red"></i> <?php echo @$exitDate; ?>
                    </span>
                </div>
            </div>

            <!--  Facilities  -->
            <div class="facilities cnttxt col-md-6">
                <h4 class="font-color-black font-bold">محل برگزاری</h4>
                <div>
                    <?php echo $locations[$fieldID];  ?>
                </div>
            </div>
        </div>
        <!--  teams  -->
        <div class="teams cnttxt">
            <h4 class="font-color-black font-bold">تیم های شرکت کننده</h4>

            <div>
                <div class="bs-glyphicons bg-color-purple"> 
                    <ul class="bs-glyphicons-list  teamuniv"> 
                        <?php
                        foreach ($universities as $university) {
                            $logoFile = 'img/unis-logo/' . $university['id'] . '.jpg';
                            if(!file_exists($logoFile))
                                $logoFile = 'img/unis-logo/no-logo.jpg';
                            echo '<li>
                                        <a data-toggle="modal"  data-target="#teamodal"
                                        data-href="teammember.php?e=' . $eventID . '&f=' . $fieldID . '&u=' . $university['id'] . '&t=' . $university['teamid'] . '">
                                            <img src="'.$logoFile.'" class="unicon">
                                            <span>' . $university['name'] . '</span>
                                        </a>
                                  </li>';
                        }
                        ?>

                    </ul>
                </div>
            </div>
        </div>

        <!-- Regulation -->
        <div class="regulations cnttxt">
            <h4 class="font-color-black font-bold">آیین نامه ها</h4>
            <?php
                $filename = 'files/rules/'.$eventPrefix[$eventID].'-'.$fieldID.'.html';
                if(file_exists($filename)) {
                    $file = fopen($filename, 'r');
                    $content = fread($file, filesize($filename));
                    echo $content;
                }
            
            ?>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="teamodal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header noborder">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body noborder"></div>
            <div class="modal-footer noborder">
                <button type="button" class="btn btn-info" data-dismiss="modal">بستن</button>
            </div>
        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $('#teamodal').on('shown.bs.modal', function () {
            $(this).find('.modal-dialog').css({width: '70%',
                height: 'auto'});
        });
        $(".teamuniv").delegate("a", "click", function (e) {
            e.preventDefault();
            fieldname = $(".flds .factive a span").text();
            $("#teamodal .modal-title").html('اعضای تیم '+fieldname+' '+$("span", this).text());
            $("#teamodal .modal-body").html('<div class="ajax-loader"><img src="img/ajax-loader.gif"></div>');
            fldurl = $(this).attr("data-href");
            $.ajax({
                type: 'GET',
                url: fldurl,
                success: function (data) {
                    $("#teamodal .modal-body").html(data);
                }
            });
        });

    });
</script>