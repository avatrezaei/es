
<h2>پسران
    <span class="curn evdt"><i class="glyphicon glyphicon-calendar"></i>
       ۱۱ مرداد - ۲۰ مرداد ۱۳۹۵</span>
</h2>
<div class="bs-glyphicons bg-color-blue"> 
    <ul class="bs-glyphicons-list smlsize-1 flds"> 
        <li>
            <a data-href="fieldinfo.php?e=5&f=155">
                <i class="icon-fum-basketball"></i>
                <span class="glyphicon-class">بسکتبال</span>
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=5&f=117">
                <i class="icon-fum-badminton"></i> 
                <span class="glyphicon-class">بدمینتون</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=5&f=116">
                <i class="icon-fum-ping-pong"></i> 
                <span class="glyphicon-class">تنیس روی میز</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=5&f=38">
                <i class="icon-fum-taekwondo"></i> 
                <span class="glyphicon-class">تکواندو</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=5&f=178">
                <i class="icon-fum-swimming"></i> 
                <span class="glyphicon-class">شنا</span> 
            </a>
        </li> 
        <li>
            <a data-href="fieldinfo.php?e=5&f=4">
                <i class="icon-fum-track"></i>
                <span class="glyphicon-class">دوومیدانی</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=5&f=159">
                <i class="icon-fum-football"></i> 
                <span class="glyphicon-class">فوتسال</span>
            </a>
        </li>
        <li> 
            <a data-href="fieldinfo.php?e=5&f=44">
                <i class="icon-fum-wrestling"></i> 
                <span class="glyphicon-class">کشتی آزاد</span>
            </a>
        </li>
        <li> 
            <a data-href="fieldinfo.php?e=5&f=45">
                <i class="icon-fum-wrestling"></i> 
                <span class="glyphicon-class">کشتی فرنگی</span>
            </a>
        </li>
        <li> 
            <a data-href="fieldinfo.php?e=5&f=156">
                <i class="icon-fum-volleyball"></i>
                <span class="glyphicon-class">والیبال</span>
            </a>
        </li> 

    </ul> 
</div>

<h2>دختران
    <span class="fnsh evdt"><i class="glyphicon glyphicon-calendar"></i>
        ۲۷ تیرماه - ۵ مرداد ۱۳۹۵</span>
</h2>
<div class="bs-glyphicons bg-color-purple"> 
    <ul class="bs-glyphicons-list smlsize-1 flds"> 
        <li>
            <a data-href="fieldinfo.php?e=1&f=155">
                <i class="icon-fum-basketball"></i>
                <span class="glyphicon-class">بسکتبال</span>
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=1&f=117">
                <i class="icon-fum-badminton"></i> 
                <span class="glyphicon-class">بدمینتون</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=1&f=116">
                <i class="icon-fum-ping-pong"></i> 
                <span class="glyphicon-class">تنیس روی میز</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=1&f=38">
                <i class="icon-fum-taekwondo"></i> 
                <span class="glyphicon-class">تکواندو</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=1&f=178">
                <i class="icon-fum-swimming"></i> 
                <span class="glyphicon-class">شنا</span> 
            </a>
        </li> 
        <li>
            <a data-href="fieldinfo.php?e=1&f=4">
                <i class="icon-fum-track"></i>
                <span class="glyphicon-class">دوومیدانی</span> 
            </a>
        </li> 
        <li> 
            <a data-href="fieldinfo.php?e=1&f=159">
                <i class="icon-fum-football"></i> 
                <span class="glyphicon-class">فوتسال</span>
            </a>
        </li>
        <li> 
            <a data-href="fieldinfo.php?e=1&f=156">
                <i class="icon-fum-volleyball"></i>
                <span class="glyphicon-class">والیبال</span>
            </a>
        </li> 

    </ul> 
</div>


<script>
$(document).ready(function() {
    $(".flds").delegate("a", "click", function(e){
        e.preventDefault();
        $("#tab_fld").html('<div class="ajax-loader"><img src="img/ajax-loader.gif"></div>');
        fldurl = $(this).attr("data-href"); 
        $.ajax({
           type: 'GET',
           url: fldurl,
           success: function(data) {
               $("#tab_fld").html(data);
           }
        });
    });
    
});
</script>
