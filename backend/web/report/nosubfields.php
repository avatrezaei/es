<div class="col-md-3">

    <ul class="nav nav-pills nav-stacked nav-pills-<?php echo $eventID; ?>">
        <li class="active">
            <a href="#tab_a" data-toggle="pill">
                <i class="icon-hd-info font-size-20"></i> توضیحات
            </a>

        </li>
        <?php
        $withsubfield = array(4,178,38);
        if(!in_array($fieldID, $withsubfield)) {
            echo '<li>
                    <a href="#tab_b" data-toggle="pill">
                        <i class="icon-hd-chart font-size-22"></i> جدول گروه بندی
                    </a>
                </li>
            ';
        }
        
        ?>
<!--        <li>
            <a href="#tab_c" data-toggle="pill">
                <i class="glyphicon glyphicon-time font-size-22"></i> برنامه زمان بندی
            </a>
        </li>-->
        <li>
            <a href="#tab_d" data-toggle="pill">
                <i class="icon-hd-cup font-size-22"></i>  مسابقات
            </a>
        </li>

    </ul>
</div>
<div class="tab-content col-md-9">

    <div class="tab-pane active" id="tab_a">

        <div class="row">
            <!--  In/Out   -->
            <div class="inout cnttxt col-md-6">
                <h4 class="font-color-black font-bold">تاریخ ورود و خروج تیم ها</h4>
                <div>
                    <span>
                        <i class="glyphicon glyphicon-arrow-right font-color-green"></i> <?php echo @$entryDate; ?>
                    </span>
                    <span>
                        <i class="glyphicon glyphicon-arrow-left font-color-red"></i> <?php echo @$exitDate; ?>
                    </span>
                </div>
            </div>

            <!--  Facilities  -->
            <div class="facilities cnttxt col-md-6">
                <h4 class="font-color-black font-bold">محل برگزاری</h4>
                <div>
                    <?php echo @$locations[$fieldID];  ?>
                </div>
            </div>
        </div>

        <!--  teams  -->
        <div class="teams cnttxt">
            <h4 class="font-color-black font-bold">تیم های شرکت کننده</h4>

            <div>
                <div class="bs-glyphicons bg-color-purple"> 
                    <ul class="bs-glyphicons-list teamuniv"> 
                        <?php
                        foreach ($universities as $university) {
                            $logoFile = 'img/unis-logo/' . $university['id'] . '.jpg';
                            if(!file_exists($logoFile))
                                $logoFile = 'img/unis-logo/no-logo.jpg';
                            echo '<li>
                                        <a data-toggle="modal"  data-target="#teamodal"
                                        data-href="teammember.php?e=' . $eventID . '&f=' . $fieldID . '&u=' . $university['id'] . '&t=' . $university['teamid'] . '">
                                            <img src="'.$logoFile.'" class="unicon">
                                            <span>' . $university['name'] . '</span>
                                        </a>
                                  </li>';
                        }
                        ?>

                    </ul>
                </div>
            </div>
        </div>

        <!-- Regulation -->
        <div class="regulations cnttxt">
            <h4 class="font-color-black font-bold">قوانین و مقررات</h4>
            <?php
                $rfilename = 'files/rules/r-'.$eventPrefix[$eventID].'-'.$fieldID.'.html';
                
                if(file_exists($rfilename)) {
                    $rfile = fopen($rfilename, 'r');
                    $rcontent = fread($rfile, filesize($rfilename));
                    echo $rcontent;
                }
            ?>
        </div>




    </div>
    <?php
        
        if(!in_array($fieldID, $withsubfield)) {
           
    ?>
    <div class="tab-pane" id="tab_b">
        <h4 class="font-color-black font-bold">جدول گروه بندی</h4>
        <div class="scalendar">
            <?php
                $dfilename = 'files/draw/d-'.$eventPrefix[$eventID].'-'.$fieldID.'.html';
                if(file_exists($dfilename)) {
                    $dfile = fopen($dfilename, 'r');
                    $dcontent = fread($dfile, filesize($dfilename));
                    $dcontent = str_replace('tbl1', 'mtbl', $dcontent);
                    echo convertNum('enum', 'pnum',$dcontent);
                }
            ?>
            
        </div>
    </div>
    <?php } ?>

    <div class="tab-pane" id="tab_c">
        <h4 class="font-color-black font-bold">برنامه زمان بندی</h4>
        <div>
            <?php //include 'schedule.php'; ?>
        </div>
    </div>

    <div class="tab-pane" id="tab_d">
        <h4 class="font-color-black font-bold">نتایج  مسابقات</h4>
        <div>
            <?php 
               // if($eventID == 1)
                    include 'fresult.php'; 
            ?>
        </div>
    </div>


</div><!-- tab content -->

<!-- Modal -->
<div id="teamodal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header noborder">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body noborder"></div>
            <div class="modal-footer noborder">
                <button type="button" class="btn btn-info" data-dismiss="modal">بستن</button>
            </div>
        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $('#teamodal').on('shown.bs.modal', function () {
            $(this).find('.modal-dialog').css({width: '70%',
                height: 'auto'});
        });
        $(".teamuniv").delegate("a", "click", function (e) {
            e.preventDefault();
            fieldname = $(".flds .factive a span").text();
            $("#teamodal .modal-title").html('اعضای تیم '+fieldname+' '+$("span", this).text());
            $("#teamodal .modal-body").html('<div class="ajax-loader"><img src="img/ajax-loader.gif"></div>');
            fldurl = $(this).attr("data-href");
            $.ajax({
                type: 'GET',
                url: fldurl,
                success: function (data) {
                    $("#teamodal .modal-body").html(data);
                }
            });
        });

    });
</script>
