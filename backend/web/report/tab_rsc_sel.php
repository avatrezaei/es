<div class="bs-glyphicons bg-color-blue scr-sel"> 
    <ul class="bs-glyphicons-list smlsize-2 flds"> 
        <li class="gscr">
            <a data-href="tab_rsc.php?e=1">
                <i class="icon-hd-hijab"></i>
                <span class="glyphicon-class">دختران</span>
            </a>
        </li> 
        <li class="bscr"> 
            <a data-href="tab_rsc.php?e=5">
                <i class="icon-hd-boy"></i> 
                <span class="glyphicon-class">پسران</span> 
            </a>
        </li> 
    </ul>
</div>


<script>
$(document).ready(function() {
    $(".flds").delegate("a", "click", function(e){
        e.preventDefault();
        $("#tab_rsc_sel").html('<div class="ajax-loader"><img src="img/ajax-loader.gif"></div>');
        fldurl = $(this).attr("data-href"); 
        $.ajax({
           type: 'GET',
           url: fldurl,
           success: function(data) {
               $("#tab_rsc_sel").html(data);
           }
        });
    });
    
});
</script>