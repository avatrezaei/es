<?php
include 'functions.php';
?>

<?php
$eventID = (int) @$_GET['e'];
$fieldID = (int) @$_GET['f'];
$subfieldID = (int) @$_GET['s'];

$subfields = getFields($eventID, $fieldID);

$universities = getUniversities($eventID, $fieldID);

$locations['159'] = 'سالن ورزشی امام رضا(ع)';
$locations['156'] = 'سالن ورزشی ۲۲ بهمن ';
$locations['155'] = 'سالن ورزشی کوثر';
$locations['117'] = 'سالن ورزشی شهید فخار هیات بدمینتون';
$locations['116'] = 'سالن ورزشی امام علی (ع)';
$locations['4'] = 'پیست دوومیدانی استاد صابری';
$locations['178'] = 'مجتمع آبی پردیس دانشگاه';
$locations['38'] = 'سالن ورزشی امام علی(ع)';

$eventPrefix[1] = 'g';
$eventPrefix[5] = 'b'; 

$eventName[1] = 'دختران';
$eventName[5] =  'پسران';

$bgColor[1] = 'bg-color-purple';
$bgColor[5] = 'bg-color-blue';

?>
<h2><?php  echo $eventName[$eventID];  ?></h2>
<div class="bs-glyphicons  <?php echo 'smlsize-'.$eventID.' '.$bgColor[$eventID]; ?>"> 
    <ul class="bs-glyphicons-list flds"> 
        <li class="<?php $factive = ($fieldID == '155') ? 'factive' : '';
echo $factive; ?>">
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=155">
                <i class="icon-fum-basketball"></i>
                <span class="glyphicon-class">بسکتبال</span>
            </a>
        </li> 
        <li class="<?php $factive = ($fieldID == '117') ? 'factive' : '';
echo $factive; ?>">
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=117">
                <i class="icon-fum-badminton"></i> 
                <span class="glyphicon-class">بدمینتون</span> 
            </a>
        </li> 
        <li class="<?php $factive = ($fieldID == '116') ? 'factive' : '';
echo $factive; ?>">
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=116">
                <i class="icon-fum-ping-pong"></i> 
                <span class="glyphicon-class">تنیس روی میز</span> 
            </a>
        </li> 
        <li class="<?php $factive = ($fieldID == '38') ? 'factive' : '';
echo $factive; ?>"> 
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=38">
                <i class="icon-fum-taekwondo"></i> 
                <span class="glyphicon-class">تکواندو</span> 
            </a>
        </li> 
        <li class="<?php $factive = ($fieldID == '178') ? 'factive' : '';
echo $factive; ?>"> 
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=178">
                <i class="icon-fum-swimming"></i> 
                <span class="glyphicon-class">شنا</span> 
            </a>
        </li> 
        <li class="<?php $factive = ($fieldID == '4') ? 'factive' : '';
echo $factive; ?>">
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=4">
                <i class="icon-fum-track"></i>
                <span class="glyphicon-class">دوومیدانی</span> 
            </a>
        </li> 
        <li class="<?php $factive = ($fieldID == '159') ? 'factive' : '';
echo $factive; ?>"> 
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=159">
                <i class="icon-fum-football"></i> 
                <span class="glyphicon-class">فوتسال</span>
            </a>
        </li>
        <?php if($eventID == 5) { ?>
        <li> 
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=44">
                <i class="icon-fum-wrestling"></i> 
                <span class="glyphicon-class">کشتی آزاد</span>
            </a>
        </li>
        <li> 
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=45">
                <i class="icon-fum-wrestling"></i> 
                <span class="glyphicon-class">کشتی فرنگی</span>
            </a>
        </li>
        
        <?php } ?>
        <li class="<?php $factive = ($fieldID == '156') ? 'factive' : '';
echo $factive; ?>"> 
            <a data-href="fieldinfo.php?e=<?php echo $eventID; ?>&f=156">
                <i class="icon-fum-volleyball"></i>
                <span class="glyphicon-class">والیبال</span>
            </a>
        </li> 

    </ul> 
</div>
<?php if(count($subfields)) { ?>
<div class="bs-glyphicons bg-color-orange subfield"> 
    <ul class="bs-glyphicons-list flds"> 
        <?php
        foreach ($subfields as $subfield) {
            $sactive = ($subfield['id'] == $subfieldID) ? 'sactive' : '';
//            echo '<li class="'.$sactive.'">
//                    <a data-href="fieldinfo.php?e=' . $eventID . '&f='.$fieldID.'&s=' . $subfield['id'] . '">
//                        <span class="glyphicon-class">' . $subfield['name'] . '</span>
//                    </a>
//                  </li> ';
            echo '<li class="'.$sactive.'">
                    
                        <span class="glyphicon-class">' . convertNum('enum', 'pnum', $subfield['name']) . '</span>
                    
                  </li> ';
        }
        
        
        ?>

    </ul>
</div>
<?php  
        $entryDate = $subfield['entryDate'];
        $exitDate = $subfield['exitDate'];
        
        $endate = $entryDate;
        $exdate = $exitDate;
        
        $fmt = new IntlDateFormatter("fa_IR@calendar=persian", 
                                        IntlDateFormatter::FULL, 
                                        IntlDateFormatter::FULL, 
                                        'Asia/Tehran', 
                                        IntlDateFormatter::TRADITIONAL, 'd MMM yyyy');
        
        $entryDate = $fmt->format(strtotime($entryDate));
        $exitDate = $fmt->format(strtotime($exitDate));
        $today = $fmt->format(time());
        //print_r($today); 
       
        
    } 
    else {
        $fieldInfo = getField($eventID, $fieldID);
        
        $entryDate = $fieldInfo[0]['entryDate'];
        $exitDate = $fieldInfo[0]['exitDate'];
        
       
        $fmt = new IntlDateFormatter("fa_IR@calendar=persian", 
                                        IntlDateFormatter::FULL, 
                                        IntlDateFormatter::FULL, 
                                        'Asia/Tehran', 
                                        IntlDateFormatter::TRADITIONAL, 'd MMM yyyy');
        
        $entryDate = $fmt->format(strtotime($entryDate));
        $exitDate = $fmt->format(strtotime($exitDate));
        
        $today = $fmt->format(time());
       // print_r($today); die('****');
        
        
    }


?>


<div class="hdc">
    <?php
    if($subfieldID) {
        include 'subfields.php';
    }
    else {
       /* if(count($subfields)) {
            include 'withsubfields.php';
        }
        else
        */
            include 'nosubfields.php';
    }
    ?>
    
    
    
</div>

<script>
$(document).ready(function() {
    $(".flds").delegate("a", "click", function(e){
        e.preventDefault();
        $("#tab_fld").html('<div class="ajax-loader"><img src="img/ajax-loader.gif"></div>');
        fldurl = $(this).attr("data-href"); 
        $.ajax({
           type: 'GET',
           url: fldurl,
           success: function(data) {
               $("#tab_fld").html(data);
           }
        });
    });
    
});
</script>
