/**
 * Override the default yii confirm dialog. This function is 
 * called by yii when a confirmation is requested.
 *
 * @param string message the message to display
 * @param string ok callback triggered when confirmation is true
 * @param string cancelCallback callback triggered when cancelled
 */
yii.confirm = function (message, okCallback, cancelCallback) {
   swal({
		title: message,
		type: 'warning',
		showCancelButton: true,
		
		// closeOnConfirm: true,
		closeOnConfirm: false,
		showLoaderOnConfirm: true,
		
		confirmButtonColor: "#DD6B55",
		cancelButtonColor: "#8cd4f5",
		
		confirmButtonText: "بله",
		cancelButtonText: "خیر!",
		
		allowOutsideClick: true
   }, okCallback);
   // },  function(){ console.log(okCallback)});  //setTimeout(function(){	 swal("Ajax request finished!");   }, 2000); });
};

yii.handleAction = function ($e, event) {
	var method = $e.data('method'),
		$form = $e.closest('form'),
		$tr = $e.closest('tr'),
		action = $e.attr('href'),
		params = $e.data('params'),
		pjax = $e.data('pjax');

		if (pjax !== undefined && pjax) {
			$.ajax({
				'type':'POST',
				'url':action,
				'success':function(data){
					result = JSON.parse(data);
					if(result.success){
						$tr.remove();
						swal({title: 'موفقیت!', text: result.messages, type: "success", confirmButtonText: 'آفرین'});
					}
					else{
						swal({title: 'خطا!', text: result.messages, type: "error", confirmButtonText: 'بستن'});
					}
				}
			});

			return false;
		}
		else{
			window.location = action;
		}
	

};