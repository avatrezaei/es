$(function() {
    
   // $(".kartik-sheet-style").addClass("portlet box purple");
    
    
    //Close button of form
    $("body").delegate(".close-form-btn", "click", function() {
        
        $(this).closest(".portlet").fadeOut(200, function() {
            //$(this).closest(".portlet").find(".note-success").hide();
            $(".new-form-btn").removeClass("disabled-btn");
        });
    });
   
    //New button
    $(".new-form-btn").click(function() {
        $(".new-form").find("input[type=text], textarea").val("");
        $(".new-form").closest(".portlet").fadeIn(500);
        $(this).addClass("disabled-btn");
    });
    
    //Save and new button
    $(".save-n-new-btn").click(function() {
        /*
         * Here: send data to server using Ajax
         */
        
        //Display note
        //$(this).closest("form").find(".note-success").hide();
        //$(this).closest('form').find(".note-success").html("اطلاعات ذخیره شد").delay(200).fadeIn();
        
         
        //Clear form fields
        $(this).closest('form').find("input[type=text]").val("");
    });
    
    $(".save-n-close-btn").click(function() {
        /*
         * Here: send data to server using Ajax
         */
        
        //Display note
        //$(this).closest('form').find(".note-success").html("اطلاعات ذخیره شد").show();
        
        //Close form 
        setTimeout(function(){
            $(".close-form-btn").click();
        },800);
        
    });
    
    
   
    
});


function displayMsg(message) {
    $(".alert-box").html(message).delay(200).fadeIn().delay(2000).fadeOut();
}
    

