/* Created by avat rezaei */

(function($) {
    $.fn.avrstep = function(options) {
        options = $.extend({  
             
        }, options);
        
        var element = this;
        var curstep = 0;
        var steps = $(element).find("div.round");
        var count = steps.size();

        $(element).before("<div id='steps'></div>");
        steps.each(function(i) {

            var name = $(this).find(".leg").html();            
            $(this).wrap("<div id='step" + i + "'></div>");            
            $("#steps").append("<a id='stepDesc" + i + "'>مرحله " + (i + 1) + "<span>" + name + "</span></a>");
            
            Step(i);
            $("#step" + i ).hide();
             
        });
        $("#step" + curstep ).show();          
        selectStep(curstep);

         

         

        function Step(i) {

            var stepName = "stepDesc" + i;
            $("#" + stepName).bind("click", function(e) {
                
                
                $("#step" + (curstep)).hide();
                $("#step" + (i)).show();
                curstep = i;
                selectStep(i);
            });
        }

        function selectStep(i) {
            $("#steps a").removeClass("current");
            $("#stepDesc" + i).addClass("current");
        }

        

    }
})(jQuery); 