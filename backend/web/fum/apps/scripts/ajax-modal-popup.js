//get the click of modal button to create / update item

$(document).on('click', '#showModalButton', function(){
	if ($('#modal').data('bs.modal').isShown) {
		$('#modal').find('#modalContent').load($(this).attr('value'));

		//dynamiclly set the header for the modal via title tag
		document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
	}
	else {
		//if modal isn't open; open it and load content
		$('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
	}
});

$(document).on('click', '#showModalButton2', function(){
	if ($('#modal2').data('bs.modal').isShown) {
		$('#modal2').find('#modalContent2').load($(this).attr('value'));
		document.getElementById('modalHeader2').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
	}
	else {
		//if modal isn't open; open it and load content
		$('#modal2').modal('show').find('#modalContent2').load($(this).attr('value'));
	}
});

$(document).ready(function(){
	$(document).on('click','#bracket-modal-trigger-edit',function()	{
		$('#bracket-modal-edit').modal('show').find('#bracket-modal-edit-content').load($(this).attr('href'));
		return false;
	});

	$(document).on('click','#bracket-modal-trigger-show-info',function() {
		$('#bracket-modal-edit').modal('show').find('#bracket-modal-edit-content').load($(this).attr('href'));
		return false;
	});

	$(document).on('click','.game-modal-view-trigger',function() {
		$('#game-modal-view').modal('show').find('#game-modal-view-content').load($(this).attr('href'));
		return false;
	});

	$(document).on('click','.game-modal-result-trigger',function() {
		$('#game-modal-result').modal('show').find('#game-modal-result-content').load($(this).attr('value'));
		return false;
	}); 

	$(document).on('click','#bracket-view-trigger',function() {
		$('#bracket-modal-view').modal('show').find('#bracket-modal-view-content').load($(this).attr('value'));
		return false;
	}); 

	$(document).on('click','#ranking-modal-games-trigger',function() {
		$('#ranking-modal-games').modal('show').find('#ranking-modal-games-content').load($(this).attr('value'));
		return false;
	}); 
});