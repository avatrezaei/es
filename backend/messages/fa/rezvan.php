<?php
return [
	'Type is not supported' => 'نوع داده پشتیبانی نمی شود.',
	1 => 'شماره گیرنده (mobile) نامعتبر (خالی) است.',
	3 => 'پارامتر encoding نامعتبر است.',
	4 =>  'پارامتر messageClass نامعتبر است.',
	6 =>  'پارامتر udh نامعتبر است.',
	13 =>  'پیامک(ترکیب udh و message) خالی است.',
	14 =>  'حساب اعتبار مورد نیاز را دارا نیست.',
	15 =>  'سرور در هنگام ارسال پیام در حال برطرف نمودن ایراد داخلی بوده است. پیام ها دوباره ارسال شوند.',
	16 =>  'حساب غیر فعال می باشد.',
	17 =>  'حساب منقضی شده است(expired)',
	18 =>  'نام کاربری و یا کلمه عبور نامعتبر است.',
	19 =>  'درخواست دارای اعتبار نمی باشد(authentication failure)',
	22 =>  'استفاده از این سرویس برای حساب شما مقدور نمی باشد.',
	23 =>  'به دلیل ترافیک بالا، سرور آمادگی دریافت پیام جدید را ندارد. دوباره سعی کنید.',
	24 =>  'شناسه پیامک معتبر نیم باشد.',
	25 =>  'نوع سروریس درخواستی نامعتبر است.',
	101 =>  'طول آرایه پارامتر messages با طول آرایه گیرندگان تطابق ندارد',
	102 =>  'طول آرایه پارامتر messageClass با طول آرایه گیرندگان تطابق ندارد',
	104 =>  'طول آرایه پارامتر udhs با طول آرایه گیرندگان تطابق ندارد',
	105 =>  'طول آرایه پارامتر priorities با طول آرایه گیرندگان تطابق ندارد',
	106 =>  'آرایه گیرندگان خالی می باشد.',
	107 =>  'طول آرایه گیرندگان بیشتر از طول مجاز است.',
	108 =>  'آرایه فرستندگان خالی می باشد.',
	109 =>  'طول آرایه پارامتر priorities با طول آرایه گیرندگان تطابق ندارد',
];