<?php
//$params = array_merge ( require (Yii::getAlias('@common/config/params.php')), require (Yii::getAlias('@common/config/params-local.php')), require (__DIR__ . '/params.php'), require (__DIR__ . '/params-local.php') );

$params = array_merge ( require (Yii::getAlias('@common/config/params.php')), require (__DIR__ . '/params.php'), require (__DIR__ . '/params-local.php') );
return [
	'id' => 'app-backend',
	'name' => 'سیستم مدیریت رخداد',
	'basePath' => dirname ( __DIR__ ),
	'controllerNamespace' => 'backend\controllers',

	'bootstrap' => [
		'log'
	],
	// 'layout' => 'column2',
	'modules' => [
		        'admin' => [
            'class' => 'mdm\admin\Module',
             
        ],

		

		 

		/**
		 * kartig setting
		 */
		'gridview' => [
			'class' => '\kartik\grid\Module'
		],
		 
	],
	'components' => [
		
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'enableStrictParsing' => false,
			// 'suffix' => '.html',
			'rules' => [
				/**
				 * Summarize the url
				 */
				/*'login' => 'YumUsers/user-auth/login',
				'signup' => 'YumUsers/user-auth/signup',
				'forget-password' => 'YumUsers/user-auth/forget-password',
				'reset-password' => 'YumUsers/user-auth/reset-password',
				'profile' => 'YumUsers/members/profileinfo',
				'switch-user/<id:\d+>/<save:\w*>' => 'site/switch-user',*/
				[
					'pattern' => 'download/apk/<id:\d+>/<code:\d+>/<version>',
					'route' => 'download/apk',
					'defaults' => ['id' => 0, 'code' => 0, 'version' => 1],
				],

				'financial/change/<id:\d+>/<status:\w+>' => 'financial/change',
				// '<controller:[\w\-]+>/<action:\w+>/<type:\w+>' => '<controller>/<action>',

				/*'YumUsers/<controller:[\w\-?]+>/<id:\d+>' => 'YumUsers/<controller>/view',
				'YumUsers/<controller:[\w\-?]+>/<action:\w+>/<id:\d+>' => 'YumUsers/<controller>/<action>',
				'YumUsers/<controller:[\w\-?]+>/<action:\w+>' => 'YumUsers/<controller>/<action>',*/

				'<controller:[\w\-?]+>/<id:\d+>' => '<controller>/view',
				'<controller:[\w\-?]+>/<action:[\w\-?]+>/<id:\d+>' => '<controller>/<action>',
				'<controller:[\w\-?]+>/<action:[\w\-?]+>' => '<controller>/<action>',
			]
		],

		'jdate' => [
			'class' => 'jDate\DateTime'
		],

		'sms' => [
			'class' => 'faravaghi\rezvansms\Rezvansms',
			'login' => 'msport', // Username
			'password' => '35373537', // Password
			'url' => 'http://sms.3300.ir/almassms.asmx?wsdl'
		],

		'food' => [
			'class' => 'faravaghi\foodreserve\FoodReserve',
			'username' => 'test',
			'password' => '123',
			'url' => 'http://sadaf.um.ac.ir/nutrition/webservices/NutritionGuests.php'
		],

		'notif' => [
			'class' => 'faravaghi\notification\Notification',
			'serverKey' => 'AIzaSyAcHZER5bDJuqmIBIGByGJwriZFjLSViHk',
			'url' => 'https://fcm.googleapis.com/fcm'
		],

		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'viewPath' => '@common/mail',
			'useFileTransport' => false,
		],

		'i18n' => [
			'translations' => [
				'app*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages',
					'sourceLanguage' => 'fa',
					'fileMap' => [
						'app' => 'app.php'
					]
					// 'app/error' => 'error.php',
				]
			]
		],

		'user' => [
			'identityClass' => 'common\models\User',
			'enableAutoLogin' => true,
			'loginUrl' => ['login']
		],

		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => [
						'error',
						'warning'
					]
				]
			]
		],
		

		'errorHandler' => [
			'errorAction' => 'site/error'
		],

		'assetManager' => [
			'linkAssets' => false,
			'forceCopy' => true
		]
	],
	'params' => $params
];