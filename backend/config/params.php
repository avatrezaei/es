<?php
return [ 
	'adminEmail' => 'admin@example.com',
	'supportEmail' => 'h.ahmadian@um.ac.ir',
	'en_fa_filetr' => '/^([a-zA-Z0-9]|[\p{Arabic}]|[_])*$/u',
	'gender_list' => [ 
			'woman' => 'خانم',
			'man' => 'آقا',
			'both' => 'هردو'
	],
	'event_status' => [ 
			'past' => 'past',
			'now' => 'now',
			'future' => 'future',
			'canceled' => 'canceled' 
	],
	/**
	 * Form Field Class:
	 */
	'formGroupClass' => 'form-group', // form-md-line-input form-md-floating-label',
	'template' => '{label}<div class="col-md-9 col-sm-9">{input} {hint} {error}</div>',
	'template_4' => '{label}<div class="col-md-4 col-sm-4">{input} {hint} {error}</div>',
	'template_5' => '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>',
	'inputClass' => 'form-control input-sm',
	'labelClass' => 'col-md-3 col-sm-3 control-label',
	'errorClass' => 'help-inline error',
	'detalView'  => '<div class="row static-info"><div class="col-md-3 col-sm-3 col-xs-4 name"> {label}: </div><div class="col-md-9 col-sm-9 col-xs-8 value"> {value} </div></div>',
	'detalView_currency'  => '<div class="row static-info"><div class="col-md-3 col-sm-3 col-xs-4 name"> {label}: </div><div class="col-md-9 col-sm-9 col-xs-8 value currency"> {value} </div></div>',
	'detalView2'  => '<div class="row static-info">
						<div class="col-xs-4 name"> {label}: </div>
						<div class="col-xs-2 value"> {value} </div>
					 </div>',
];
