<?php
namespace backend\modules\YumUsers\components;

use Yii;

/**
 * @inheritdoc
 *
 * @property \app\models\User|\yii\web\IdentityInterface|null $identity The identity object associated with the currently logged-in user. null is returned if the user is not logged in (not authenticated).
 */
class User extends \yii\web\User
{
	 public function beforeLogin($identity, $cookieBased, $duration) {

		if (parent::beforeLogin($identity, $cookieBased, $duration)) {
			return true;
		} else {
			return false;
		}
	}  
}