<?php

namespace backend\modules\YumUsers\models;

use Yii;

/**
 * This is the model class for table "educational_sections".
 *
 * @property integer $EduSecCode
 * @property integer $unc_EduSecCode
 * @property string $EEduSecName
 * @property string $PEduSecName
 */
class EducationalSections extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'educational_sections';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['EduSecCode'], 'required'],
			[['EduSecCode', 'unc_EduSecCode'], 'integer'],
			[['EEduSecName', 'PEduSecName'], 'string', 'max' => 45]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'EduSecCode' => Yii::t('app', 'Edu Sec Code'),
			'unc_EduSecCode' => Yii::t('app', 'Unc  Edu Sec Code'),
			'EEduSecName' => Yii::t('app', 'Eedu Sec Name'),
			'PEduSecName' => Yii::t('app', 'Pedu Sec Name'),
		];
	}
}
