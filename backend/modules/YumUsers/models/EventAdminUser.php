<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\captcha\Captcha;
use yii\web\Session;

/* * *************
 * Yii validator
 */
use backend\models\jdf;
use yii\validators\Validator;
use yii\validators\StringValidator;
use yii\validators\NumberValidator;

use backend\models\EventMembers;
use backend\models\AuthorizedUniversity;
use backend\modules\YumUsers\models\YumPasswordValidator;
use backend\modules\YumUsers\models\ProActiveRecord;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use Yii\helpers\Url;

class EventAdminUser extends User {

   /*
	* Attribute in Modal Search shoud diferent name 
	* to prevent Conflict in Main grid in indexaction
	*/
	public $searchInputName;
	public $searchInputLastName;
	public $searchInputStuNo;
	
	
	public $userDetailId;
	public $userUniversityId;
	public $eventId;
	
	
	
   /*****SPORT EVENT FIELDS	 *******/

	
	public $fileAttr;

	
	
	
		   public function scenarios()
		{

			$scenarios = parent::scenarios();
			$scenarios['createEventAdminUser'] = ['username','name','last_name','email','tel','mobile','gender','sportNo', 'birthDate','passwordInput','passwordInput_repeat'];//Scenario Values Only Accepted
			$scenarios['updateEventAdminUser'] = ['username','name','last_name','email','tel','mobile','gender','sportNo', 'birthDate','passwordInput','passwordInput_repeat'];//Scenario Values Only Accepted
			return $scenarios;

		} 

	
	
public function rules() {
		//get setting of module
		$usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
		$passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
		$nameRequirements = Yum::module('YumUsers')->nameRequirements;

		/**Requirments***/
	   $rules[] = [['name','last_name','email','tel','mobile','username','gender','passwordInput','passwordInput_repeat'],'required','on'=>['createEventAdminUser']];
	   $rules[] = [['name','last_name','email','tel','mobile','username','gender'],'required'];
	   $rules[] = [['username'],'required'];
		
		/** password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator**/
		$passwordrule = array_merge(['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'], $passwordRequirements);
		$rules[] = $passwordrule;
		$rules[] =  ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];
		if($this->passwordInput!='')
		{
		$rules[] =  ['passwordInput_repeat','required' ,'on'=>['updateEventAdminUser']];
		}
		
		
		/** username validation from setting*/

		if ($usernameRequirements) {
			$rules[] = ['username', 'string',  'max' => $usernameRequirements['maxLen'],'min' => $usernameRequirements['minLen'],];
			$rules[] = ['username', 'match','pattern' => $usernameRequirements['match'],'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
		}
		$rules[] = ['username', 'checkUnique'];


		/** name validation from setting**/
		if ($nameRequirements) {
			$rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];
				   
			$rules[] = [['name', 'last_name'], 'match',  'pattern' => $nameRequirements['match'],'message' => Yum::t($nameRequirements['dontMatchMessage'])];
		}
		
		
		/** Emali validation**/
		$rules[] = ['email', 'email'];
		$rules[] = ['email', 'checkUnique'];
		$rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];


		/** date validation**/
		//$rules[] =  [['birthDate'], 'checkDateFormat']; 


		/** natinalId validation**/
		//$rules[] =  [['nationalId'], 'validateNationalId']; 
	     $rules[] = ['nationalId', 'checkUnique'];
	     $rules[] =array('nationalId', 'string', 'min'=>10, 'max'=>10);

	     $rules[] = array(['idNo','tel','mobile','nationalId'], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');



	/** Number validation**/
		$rules[] = array(['birthCityId','sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);
		
		/** String validation**/
		$rules[] = array([ 'univ', 'homepage', 'tel', 'mobile', ], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);
		
		/** Range validation**/
		$rules[] = array([ 'lock', 'valid'], 'in', 'range' => array(0, 1));
		$rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE,self::MALE));		
		

		/** Exist validation**/
		//$rules[] =  ['id', 'exist', 'targetClass' => '\backend\models\City'] ;
		
		
		
		/** Safe validation**/

		$rules[] = array(['verifyCode'], 'safe',);
		/** Safe validation for save in db**/
		$rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
		$rules[] = [['fileAttr','sportNo', 'eName', 'eFamily', 'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'], 'safe',];
		
		
		/** file validation**/
		$fileModel=new File;

	   $rules[] =  [['fileAttr'], 'file',
			'skipOnEmpty' => true,
			'extensions' =>'jpg,jpeg,png',
		   'checkExtensionByMimeType' => true,
			'maxSize' => File::MAX_FILE_SIZE,
			'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
			'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
			'minSize' => File::MIN_FILE_SIZE,
		   'mimeTypes' => ['image/jpeg','image/jpeg','image/png'],

			]; 




	   
   


		/** other vlidations*/
		//  $rules[] = array('role', 'RolesTypeValidation',);	
		return $rules;
	}


	public function getUsersDetails() {
		return $this->hasMany(UsersDetails::className(), ['usersId' => 'id'])
		->andOnCondition(['users_details.valid' => 1]);
	}  


	
	
	
		public function beforeSave($insert) {
		/*Creatuser */
		if (parent::beforeSave($insert)) {
		   // $this->setIsNewRecord(0);
			
			return true;
		} else {
			return false;
		}
	}  
	
	
	public function createAdminEventUser($eventId) {
		


		if ($this->validate()) {

			$this->setPassword($this->passwordInput);
			$this->generateAuthKey();
			
			
			if ($result=$this->save(1)) {

			/*- Insert Role info in Authassign table*/  
			  try {
				$this->addRole(self::Role_SeniorUserEvent,$eventId);
			  }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
			  } 
			  
			  
			  
			  
			/*- Insert userdetail info in userDetail table*/  
			  try {
				$this->insertUserDetailInfo($this->id,$eventId);
			  }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در ذخیره اطلاعات پیش آمده است.');
			  } 
				

			/*- Insert Info in members event table*/  
			  try {
				  
				$this->insertMemberEventInfo($eventId);
				  
			  }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در ذخیره اطلاعات پیش آمده است.');
			  }			   
				return TRUE;
			}
		}

		return false;
	}

	
	/*
	 * insert info in eventMember table 
	 */
	public function insertMemberEventInfo($eventId)
	{
		
		$eventMemberObj=new EventMembers;
		$eventMemberObj->eventId=$eventId;
		$eventMemberObj->usersDetailsId=$this->userDetailId;
		$eventMemberObj->rolename=self::Role_SeniorUserEvent;
		$eventMemberObj->user_type_id=UsersType::getUserTypeId('SeniorUserEvent');
		$eventMemberObj->userId=$this->id;
		
		return $eventMemberObj->save(0);
	}
	
	
  
	public function insertUserDetailInfo($userId,$eventId)
	{
		
		$userDetailObj=new UsersDetails();
		$userDetailObj->usersId=$userId;
		$userDetailObj->eventId=$eventId;
		
		$this->userDetailId= $userDetailObj->saveAdminEventDetails();
		return $this->userDetailId;
	} 
	
	
	public function updateAdminEventUser() {
	
		$eventId=Yii::$app->user->identity->eventId; 
		
		if ($this->validate()) {

			if(($this->passwordInput!=''))
			{
			   $this->setPassword($this->passwordInput);
			}
						   

			/*EventAdmin User just Update username before user login */

				if($this->isUserLoginEver()){ 
					/*dont save user if hacker set username by overposting*/
				  $result=$this->save(1,['name','last_name','email','tel','mobile','gender','birthDate','password_hash','nationalId']);
				}else{
					/*save username*/
				  $result=$this->save(1,['username','name','last_name','email','tel','mobile','gender','birthDate','password_hash','nationalId']);
				}
			return $result;

		}

		return false;
	}  
	  
  /*
   * diferent from createUser and modiy user:in modiy user dont call add role
   * that prevent integrity error db in auth_assignment in yii
   */  
	
	public function modifyAthleteUser() {
		if ($this->validate()) {

			/*-Set nationalId as Default username And password-*/	
			$this->username=isset($this->nationalId) ? $this->nationalId:'';
			$this->passwordInput=isset($this->nationalId)? $this->nationalId:'';

			$this->setPassword($this->passwordInput);
			$this->generateAuthKey();
			if ($result=$this->save(0)) {
			   
				return TRUE;
			}
		}

		return null;
	}	
	
	public function addRole($role,$eventId)
	{

		 $auth = Yii::$app->authManager;
		 $Role = $auth->getRole($role);
		 $auth->assignByEventId($Role,$this->id,$eventId);
	}
	
	
	public function getAuthassignment()
	{
		return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
	}
	
	/*
	 * 1- delete UserDetail row in UerDetail table
	 * 2- delete responsed role in authitemtable
	 * 3- delete EventmemberRow in EventMember table
	 * 3- Unset userDetailId field In autorizedtable
	 */
	public function deleteAdminEventUser($eventId)
	{


		$id=$this->id;
		$userDetailsCount=$this->getUsersDetailsCount();

		if($userDetailsCount==1){
			$deleteUserRecord=1;
		}
		else{
			$deleteUserRecord=0;
		}


		$userDetailId=$this->getUserDetailIdBaseOnEventIdUserId($eventId);

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();

		
		try {
			/**
			 * Delete User Record if one userdetail
			 */
			if($deleteUserRecord){
				if ($this->trulyDelete) {
					$result= (parent::delete()) ? true : FALSE;
				}
				else{
					parent::beforeDelete();

					try {
						$result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();
					}
					catch (\Exception $ex) {
						return false;
					}
				}
			}

			/**
			 * Start Delete UserDetails Record
			 */
			$userDetailsObj=new UsersDetails();
			$userDetailsObj->id=$userDetailId;

			if ($this->trulyDelete) {
				$resultUserDetails= ($userDetailsObj->delete()) ? true : FALSE;

			}
			else{
				parent::beforeDelete();

				try {
					$resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

				}
				catch (\Exception $ex) {
					return false;
				}
			}

		

			/** delete responsed role in authitemtable**/
			$authAssignObj=new AuthAssignment();
			$resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_SeniorUserEvent.'" And event_id='.(int)$eventId).'';



			/** delete EventmemberRow in EventMember table**/
			$result3=EventMembers::deleteAll('eventId = :eventId AND rolename=:role AND usersDetailsId=:userDetail', [':eventId' => (int)$eventId,':role' => self::Role_SeniorUserEvent,':userDetail'=>$userDetailId]);


			$transaction->commit();
			
			return true;
		} catch (\Exception $e) {
			$transaction->rollBack();
			//return false;
			throw $e;
		}
		return false;
	}
	

  
  
  }


