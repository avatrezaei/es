<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

use backend\models\EventMembers;
use backend\modules\YumUsers\models\UsersType;
use backend\modules\YumUsers\models\AthleteUser;
use backend\modules\YumUsers\models\UsersDetails;

/**
 * UserSearch represents the model behind the search form about `backend\modules\YumUsers\models\User`.
 */
class AthleteUserSearch extends AthleteUser
{


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$nameRequirements = Yum::module('YumUsers')->nameRequirements;

		$rules[] = [['id', 'gender', 'email', 'tel', 'mobile', 'status', 'created_at', 'lock', 'valid', 'searchInputStuNo'], 'integer'];
		$rules[] = [['username', 'fileId', 'name', 'last_name', 'searchInputName', 'searchInputStuNo', 'searchInputLastName'], 'safe'];
		$rules[] = [['name', 'last_name', 'searchInputLastName'], 'string'];
		if ($nameRequirements) {
//			$rules[] = array(['name', 'last_name'], 'string',
//				'max' => $nameRequirements['maxLen'],
//				'min' => $nameRequirements['minLen'],
//			);
			$rules[] = array(
				['searchInputLastName', 'searchInputName'],
				'match',
				'pattern' => $nameRequirements['match'],
				'message' => Yum::t($nameRequirements['dontMatchMessage']));
		}
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $eventId)
	{

		$query = AthleteUser::find();
		$query->andFilterWhere(['user.valid' => 1,]);
		$query->joinWith('eventMembers');

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
			'pagination' => [
				'pageSize' => \app\models\Setting::getPaginationSetting(),
			],]);

		$query->andFilterWhere([
		   'event_members.eventId' => (int)$eventId,

		]);
		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}


		$query->andFilterWhere([
			'id' => $this->id,
			'gender' => $this->gender,
			'status' => $this->status,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'login_attemp_count' => $this->login_attemp_count,
			'last_login_time' => $this->last_login_time,
			'last_login_attemp_time' => $this->last_login_attemp_time,
			'lock' => $this->lock,
			'event_members.user_type_id' => UsersType::getUserTypeId('Athlete'),
			'event_members.eventId' => (int)$eventId,
		]);

		$query->andFilterWhere(['like', 'username', $this->username])
			->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'last_name', $this->last_name])
			->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'tel', $this->tel])
			->andFilterWhere(['like', 'mobile', $this->mobile]);

		return $dataProvider;
	}


	public function searchTeamMembers($eventId, $authFieldId, $teamId)
	{
		$athleteType = (int)UsersType::getUserTypeId('Athlete');

		$tblUser = User::tableName();
		$tblemem = EventMembers::tableName();
		$tbldetl = UsersDetails::tableName();

		$query = User::find();
		$query->select([
			$tblUser.'.id',
			$tblUser.'.name',
			$tblUser.'.last_name',
			$tbldetl.'.educationalField',
			$tblUser.'.idNo',
			$tblUser.'.birthCityId',
			$tblUser.'.fatherName',
			$tblUser.'.nationalId',
			$tblUser.'.fileId',
			$tblUser.'.email',
			$tblUser.'.tel',
			$tblUser.'.mobile',
			$tblUser.'.birthDate',
			$tbldetl.'.stuNo',
			$tbldetl.'.sportMembershipHistory',
			$tbldetl.'.sportAssuranceNo',
		]);
		$query->joinWith(['eventMembers' => function (\yii\db\ActiveQuery $query) use ($eventId, $teamId, $athleteType) {
			$query->event($eventId)->team($teamId)->type($athleteType);
		},
		'usersDetails' => function (\yii\db\ActiveQuery $query) use ($eventId) {
			$query->event($eventId);
		}]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			 'sort' => [
				'attributes' => [
					'id' => [
						'default' => SORT_DESC,
					],
				],
			],
		   'pagination' => [
				'pageSize' => 20,
			],
		]);

		return $dataProvider;
	}
}
