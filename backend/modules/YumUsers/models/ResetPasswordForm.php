<?php
namespace backend\modules\YumUsers\models;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
	public $password;
	public $password_repeat;

	/**
	 * @var \common\models\User
	 */
	private $_user;


	/**
	 * Creates a form model given a token.
	 *
	 * @param  string						  $token
	 * @param  array						   $config name-value pairs that will be used to initialize the object properties
	 * @throws \yii\base\InvalidParamException if token is empty or not valid
	 */
	public function __construct($token, $config = [])
	{
		if (empty($token) || !is_string($token)) {
			throw new InvalidParamException('Password reset token cannot be blank.');
		}
		$this->_user = User::findByPasswordResetToken($token);
		if (!$this->_user) {
			throw new InvalidParamException('Wrong password reset token.');
		}
		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{


		//get setting of module
		$passwordRequirements = Yum::module('YumUsers')->passwordRequirements;

		/**
		 * password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator
		 */
		$passwordrule = array_merge(
			[
				'password', 'backend\modules\YumUsers\models\YumPasswordValidator',
			],
			$passwordRequirements
		);

		$rules[] = $passwordrule;

		$rules[] =  ['password', 'required'];
		$rules[] =  ['password_repeat', 'safe'];
		$rules[] =   ['password', 'compare'];

		return $rules;


	}
	public function attributeLabels()
	{
		return [
			'password' => Yii::t('app', 'Input New Password'),
			'password_repeat' => Yii::t('app', 'Confirm Password'),

		];
	}

	/**
	 * Resets password.
	 *
	 * @return boolean if password was reset.
	 */
	public function resetPassword()
	{
		$user = $this->_user;
		$user->setPassword($this->password);
		$user->removePasswordResetToken();

		return $user->save();
	}
}
