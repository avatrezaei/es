<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\YumUsers\models\AthleteUser;
use yii\helpers\ArrayHelper;

/**
 * UserSearch represents the model behind the search form about `backend\modules\YumUsers\models\User`.
 */
class VipMiscCaravanUserSearch extends VipMiscCaravanUser {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		$nameRequirements = Yum::module('YumUsers')->nameRequirements;

		$rules[] = [['id', 'gender',  'status', 'created_at', 'lock', 'valid'], 'integer'];
		$rules[] = [['username','fileId', 'name', 'last_name','searchInputName','searchInputLastName'], 'safe'];
		$rules[] = [[ 'name', 'last_name','searchInputLastName'], 'string'];
		if ($nameRequirements) {
//			$rules[] = array(['name', 'last_name'], 'string',
//				'max' => $nameRequirements['maxLen'],
//				'min' => $nameRequirements['minLen'],
//			);
			$rules[] = array(
				[ 'searchInputLastName','searchInputName'],
				'match',
				'pattern' => $nameRequirements['match'],
				'message' => Yum::t($nameRequirements['dontMatchMessage']));
		} return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params,$eventId) {

		$query = AthleteUser::find();
		$query->joinWith('eventMembers');
		$query->andFilterWhere(['user.valid' => 1,]);


		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
			'pagination' => [
				'pageSize' => \app\models\Setting::getPaginationSetting(),
			],]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'gender' => $this->gender,
			'status' => $this->status,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'login_attemp_count' => $this->login_attemp_count,
			'last_login_time' => $this->last_login_time,
			'last_login_attemp_time' => $this->last_login_attemp_time,
			'lock' => $this->lock,
			'event_members.eventId' => (int)$eventId,

		]);
		/*show all Vip users*/
		$user_type_id=\backend\modules\YumUsers\models\UsersType::getUserTypeId(Self::Type_VIPAttendant);
		$query->andFilterWhere(['event_members.user_type_id' =>  $user_type_id]);

		$query->andFilterWhere(['like', 'username', $this->username])
				->andFilterWhere(['like', 'name', $this->name])
				->andFilterWhere(['like', 'last_name', $this->last_name])
				->andFilterWhere(['like', 'homepage', $this->homepage])
				->andFilterWhere(['like', 'auth_key', $this->auth_key])
				->andFilterWhere(['like', 'password_hash', $this->password_hash])
				->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
				->andFilterWhere(['like', 'email', $this->email])
				->andFilterWhere(['like', 'tel', $this->tel])
				->andFilterWhere(['like', 'mobile', $this->mobile]);

		return $dataProvider;
	}


}
