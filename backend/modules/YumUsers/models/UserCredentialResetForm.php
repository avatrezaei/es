<?php
namespace backend\modules\YumUsers\models;

use backend\modules\YumUsers\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class UserCredentialResetForm extends Model
{
	public $oldUsername;

	public $newUsername;

	public $targetUser;
	public $inputOldPassword;
	public $inputNewPassword;
	public $inputNewPassword_repeat;
	public $validateOLdPassword;
	public $_user = false;

	public $username;
	public $email;
	public $mobile;

	public function rules()
	{
		//get setting of module
		$passwordRequirements = Yum::module('YumUsers')->passwordRequirements;

		/**
		 * password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator
		 */
		$passwordrule = array_merge(
			[
				'inputNewPassword', 'backend\modules\YumUsers\models\YumPasswordValidator',
				'except' => 'register'
			],
			$passwordRequirements
		);

		$rules[] = $passwordrule;

		$rules[] = [['email','mobile'], 'required'];
		$rules[] = [['inputNewPassword', 'inputNewPassword_repeat'], 'required'];
		$rules[] = [['inputOldPassword'], 'required'];
		$rules[] = [['inputOldPassword'], 'ValidateOldPassword'];
		$rules[] = [['inputNewPassword'], 'CheckNewPassword'];
		$rules[] = ['inputNewPassword_repeat', 'safe'];
		$rules[] = ['inputNewPassword', 'compare'];
		$rules[] = ['email', 'emailValidation'];
		$rules[] = ['username', 'usernameValidation'];

		$rules[] = [['username','email','mobile'], 'safe'];
		//$rules[] = ['email','email'];
		$rules[] = array(['mobile'], 'match','pattern' => '/^([0-9]|[-])*$/','message' => ' فقط از کاراکترهای عددی و علامت -استفاده شود.');

		return $rules;
	}

	public function emailValidation($attribute, $params)
	{
		if ($this->_user == NULL) {
			$this->_user = $this->getUserObj();
		}
		$this->_user->email=$this->$attribute;


		if(!$this->_user->validate(['email']))
		{

			$errors=$this->_user->errors['email'];
			foreach ( $errors as $error) {
				$this->addError($attribute,$error);

			}
			//die(var_dump($this->errors));

			return false;
		}

			return true;
	}






	public function usernameValidation($attribute, $params)
	{
		if ($this->_user == NULL) {
			$this->_user = $this->getUserObj();
		}

		$this->_user->username=$this->$attribute;
		if(!$this->_user->validate(['username']))
		{
			$errors=$this->_user->errors['username'];
			foreach ( $errors as $error) {
				$this->addError($attribute,$error);

			}

			return false;
		}

		return true;
	}






	/**
	 * newPassword shoudn't equal to oldPassword
	 */
	public function CheckNewPassword($attribute, $params)
	{
		$user = $this->getUser();

		if ($user && $user->validatePassword($this->inputNewPassword))
		{
			$this->addError($attribute,Yii::t('app', 'New password should not be like old password.'));
		}

		$this->_user = false;
		return;
	}





	public function ValidateOldPassword()
	{
		$userId=isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

		$userObj = User::findOne($userId);
		if ($userObj==null)
		{
			$this->addError('username',Yii::t('app', 'اشکالی در تغییر کلمه عور بوجود آمده است'));
			return false;
		} 

		if ($userObj && $userObj->validatePassword($this->inputOldPassword)) {
			return true;
		}
		else {
			$this->addError('inputOldPassword', Yii::t('app', 'The old password is incorrect. Please retype your password.'));
			return false;
		}

		return false;
	}

	public function attributeLabels()
	{
		return [
			'inputNewPassword' => Yii::t('app', 'Input New Password'),
			'inputOldPassword' => Yii::t('app', 'Input Old Password'),
			'inputNewPassword_repeat' => Yii::t('app', 'Confirm Password'),
			'passwordInput' => Yii::t('app', 'Admin Password Input'),
			'targetUser' => Yii::t('app', 'Target User'),
			'mobile' => Yii::t('app', 'Mobile'),
			'username' => Yii::t('app', 'Username'),
			'email' => Yii::t('app', 'Email'),
		];
	}

	public function changePassword($userId = null)
	{
		if ($userId !== NULL) {
			$this->targetUser = $userId;
		}
		else{
			$userId=isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		}

		$userObj = User::findOne($userId);
		if ($userObj==null)
		{
			$this->addError('username',Yii::t('app', 'اشکالی در تغییر کلمه عور بوجود آمده است'));
			return false;
		}		 

		$userObj->setPassword($this->inputNewPassword);
		$userObj->last_login_time = date('Y-m-d H:i:s');

		if ($userObj->save(0,['password_hash']))
		{
			$userObj->save(0,['last_login_time']);
			return true;
		}

		return false;
	}

	public function changeUsername($newUserName)
	{
		if ($this->_user == NULL) {
			$this->_user = $this->getUserObj();
		}

		$this->_user->username=$newUserName;
		if($this->_user->validate('username'))
		{
			if($this->_user->save(0,['username']))
				return true;
		}
		return false;
	}

	public function changeContactInfo($mobile,$email)
	{
		if ($this->_user == NULL) {
			$this->_user = $this->getUserObj();
		}

		$this->_user->mobile=$mobile;
		$this->_user->email=$email;

		if($this->_user->save(0,['email','mobile']))
				return true;

		return false;
	}

	/**
	 * Resets password.
	 *
	 * @return boolean if password was reset.
	 */
	public function resetPassword()
	{
		$user = $this->_user;
		$user->setPassword($this->password);
		$user->removePasswordResetToken();

		return $user->save();
	}
	
	
	
	public function getUser($userName = NULL)
	{
		if ($userName != NULL)
			$this->username = $userName;

		$this->_user = User::findByUsername($this->username);

		return $this->_user;
	}


	public function getUserObj($id = NULL)
	{
		if ($id == NULL)
			$userId=isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

		$this->_user= User::findOne($userId);
		if ( $this->_user==null)
		{
			$this->addError('username',Yii::t('app', 'اشکالی در تغییر کلمه عور بوجود آمده است'));
			return false;
		}
		return $this->_user;
	}
}
