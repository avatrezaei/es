<?php

namespace backend\modules\YumUsers\models;

use backend\models\EventMembers;
use backend\models\Teams;
use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\FileAction;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YumUsers\models\YumPasswordValidator;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;

/* * *************
 * Yii validator
 */

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property integer $gender
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $valid
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Membership[] $memberships
 */
class CoachUser extends User
{


    /*
     * Attribute in Modal Search shoud diferent name
     * to prevent Conflict in Main grid in indexaction
     */
    public $searchInputName;
    public $searchInputLastName;
    public $searchInputStuNo;


    /*****SPORT EVENT FIELDS     *******/


    public $fileAttr;

    public $teamId;
    public $caravanId;


    public function scenarios()
    {


        $scenarios = parent::scenarios();
        $scenarios['createCoachUserByAdmin'] = ['userType', 'teamId', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'fatherName', 'idNo', 'nationalId', 'birthDate', 'birthCityId', 'fileId'];//Scenario Values Only Accepted
        $scenarios['createCoachUser'] = ['userType', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'fatherName', 'idNo', 'nationalId', 'birthDate', 'birthCityId', 'fileId'];//Scenario Values Only Accepted
        $scenarios['updateCoachUser'] = ['name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'fatherName', 'idNo', 'nationalId', 'birthDate', 'birthCityId', 'fileId'];//Scenario Values Only Accepted
        $scenarios['createCouchByExistUser'] = ['userType', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'fatherName', 'idNo', 'nationalId', 'birthDate', 'birthCityId', 'fileId'];//Scenario Values Only Accepted
        return $scenarios;

    }

    public function rules()
    {

        //get setting of module
        $usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
        $passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
        $nameRequirements = Yum::module('YumUsers')->nameRequirements;

        /**Requirments***/
        $rules[] = [['gender', 'name', 'last_name', 'fatherName', 'mobile', 'nationalId'], 'required', 'on' => ['createCoachUser', 'createCouchByExistUser', 'updateCoachUser', 'createCoachUserByAdmin']];
        $rules[] = [['teamId', 'caravanId'], 'required', 'on' => ['createCoachUserByAdmin', 'createCouchByExistUser']];
        $rules[] = [['teamId'],'semanticTeamCaravanValidation','on'=>['createCoachUserByAdmin','createCouchByExistUser']];


        $rules[] = [['userType',], 'required', 'on' => ['createCoachUserByAdmin']];


        $rules[] = [['userType'], 'in', 'range' => [16, 17, 18, 19]];

        /** password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator**/
        $passwordrule = array_merge(['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'], $passwordRequirements);
        $rules[] = $passwordrule;
        $rules[] = ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];

        /** username validation from setting*/

        if ($usernameRequirements) {
            $rules[] = ['username', 'string', 'max' => $usernameRequirements['maxLen'], 'min' => $usernameRequirements['minLen'],];
            $rules[] = ['username', 'match', 'pattern' => $usernameRequirements['match'], 'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
        }
        $rules[] = ['username', 'checkUnique'];

        /**
         * natinalId validation
         */
        if(!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id, User::Role_Admin )&&!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id,  User::Role_SeniorUserEvent)) {
            $rules[] = [['nationalId'], 'validateNationalId'];
        }

        $rules[] = ['nationalId', 'checkUnique'];
        $rules[] = array('nationalId', 'string', 'min' => 10, 'max' => 10);

        /** name validation from setting**/
        if ($nameRequirements) {
            $rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];

            $rules[] = [['name', 'last_name'], 'match', 'pattern' => $nameRequirements['match'], 'message' => Yum::t($nameRequirements['dontMatchMessage'])];
        }


        /** Emali validation**/
        $rules[] = ['email', 'email'];
        $rules[] = ['email', 'checkUnique'];
        $rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];


        /** date validation**/
        //$rules[] =  [['birthDate'], 'checkDateFormat'];



        /**
         * Numeric validation Utf8
         */
        $rules[] = array(['idNo','tel','mobile','nationalId'], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');




        /** Number validation**/
        $rules[] = array(['birthCityId','sportNo', 'creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active', 'fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);

        /** String validation**/
        $rules[] = array(['univ', 'homepage', 'tel', 'mobile',], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

        /** Range validation**/
        $rules[] = array(['lock', 'valid'], 'in', 'range' => array(0, 1));
        $rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE, self::MALE));


        /** Exist validation**/
       // $rules[] = ['id', 'exist', 'targetClass' => '\backend\models\City'];


        /** Safe validation**/

        $rules[] = array(['verifyCode'], 'safe',);
        /** Safe validation for save in db**/
        $rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
        $rules[] = [['teamId', 'fileAttr', 'sportNo', 'eName', 'eFamily', 'fatherName', 'idNo', 'nationalId', 'birthDate', 'birthCityId', 'fileId'], 'safe',];


        $fileModel = new File;

        $rules[] = [['fileAttr'], 'file',
            'extensions' => $this->fileTyps,
            'checkExtensionByMimeType' => true,
            'maxSize' => $this->fileSize,
            'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
            'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
            'minSize' => File::MIN_FILE_SIZE,
            'mimeTypes' => $this->mimeFileTyps,

        ];

        $rules[] = [['fileAttr'], 'file',
            'skipOnEmpty' => false,
            'on' => ['createCoachUser', 'createCoachUserByAdmin']
        ];

        $rules[] = array(['fileAttr'], 'required');
        $rules[] = array(['fileAttr'], 'safe',);


        /** Captcha validation**/
        $rules[] = array('verifyCode', 'captcha',
            'isEmpty' => !Captcha::checkRequirements(),
            'on' => 'register,captch,forgetpass',
            'message' => ' تصویر امنیتی  صحیح را وارد کنید.'
        );

        /** other vlidations*/
        //  $rules[] = array('role', 'RolesTypeValidation',);
        return $rules;
    }


    public function getUsersDetails()
    {
        return $this->hasMany(UsersDetails::className(), ['usersId' => 'id'])
            ->andOnCondition(['users_details.valid' => 1]);
    }

    /* Autentication user model */

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    public function beforeSave($insert)
    {
        /*Creatuser */
        if (parent::beforeSave($insert)) {
            // $this->setIsNewRecord(0);

            return true;
        } else {
            return false;
        }
    }


    public function createCoachUser()
    {

        $saveAttributes = $this->createSaveAttributes;


        if ($this->validate()) {
            /* Set nationalId as Default username And password*/
            if($this->scenario!='createCouchByExistUser')
            {
                $this->username = isset($this->nationalId) ? $this->nationalId : '';
                $this->passwordInput = isset($this->mobile) ? $this->mobile : '';


                $this->setPassword($this->passwordInput);
                $this->generateAuthKey();

            }


            if($this->scenario=='createCouchByExistUser')
            {
                $saveAttributes=$this->createExistSaveAttributes;
            }


            if ($result = $this->save(0,$saveAttributes)) {

                try {
                    if ($this->scenario == 'createCouchByExistUser') {
                        if (!Yii::$app->getAuthManager()->checkAccess($this->id, self::Role_Coach)) {
                            $this->addRole(self::Role_Coach);

                        }

                    } else {

                        $this->addRole(self::Role_Coach);

                    }
                } catch (Exception $e) {
                    throw new \yii\web\HttpException(403, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
                }
                return TRUE;
            }
        }

        return false;
    }

    /**
     * diferent from createUser and modiy user:in modiy user dont call add role
     * that prevent integrity error db in auth_assignment in yii
     */
    public function modifyCoachUser()
    {
        if ($this->validate()) {

            /*-Set nationalId as Default username And password-*/
            $this->username = isset($this->nationalId) ? $this->nationalId : '';
            $this->passwordInput = isset($this->nationalId) ? $this->nationalId : '';

            $this->setPassword($this->passwordInput);
            $this->generateAuthKey();
            if ($result = $this->save(0)) {

                return TRUE;
            }
        }

        return null;
    }

    public function addRole($role = self::Role_Coach)
    {

        $auth = Yii::$app->authManager;
        $Role = $auth->getRole($role);
        $auth->assign($Role, $this->id);
    }

    public function getAuthassignment()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * insert info in eventMember table
     */
    public function insertMemberEventInfo($userDetailId, $eventId, $carvanId = 0, $teamId, $universityId)
    {
        $eventMemberObj = new EventMembers;
        $eventMemberObj->eventId = $eventId;
        $eventMemberObj->teamsId = $teamId;
        $eventMemberObj->usersDetailsId = $userDetailId;
        $eventMemberObj->universityId = $universityId;
        $eventMemberObj->caravanId = $carvanId;
        $eventMemberObj->rolename = self::Role_Coach;
        $eventMemberObj->user_type_id = $this->userType;
        $eventMemberObj->userId = $this->id;


        return $eventMemberObj->save(0);

    }



    public function getCoachTeamId($eventId,$caravanId)
    {
        $eventMemObj = $this->getCouchEventMemObj($eventId,$caravanId);
        return $eventMemObj->teamsId;
    }


    /*getAtheleteTeamObj*/
    public function getCouchEventMemObj($eventId,$caravanId)
    {
        $coachTypeModels = UsersType::getCouchType();
        $coachTypeIds = ArrayHelper::map($coachTypeModels, 'id', 'id');

        $userDetailId = $this->getUserDetailIdBaseOnEventIdUserId($eventId);
        $eventMemObj = new EventMembers;
        $eventMemresult = $eventMemObj->getUserEventCaravanMemObj($eventId, $userDetailId, $coachTypeIds,$caravanId);
        return $eventMemresult;
    }

    public function getCoachAuthField($eventId,$caravanId)
    {
        $teamId = $this->getCoachTeamId($eventId,$caravanId);

        $teamObj = Teams::find()->andWhere('id = :id', [':id' => ( int )$teamId])->one();

        return $teamObj->authFieldId;
    }
    public function deleteCoachByAdmin($id = NULL,$teamId=null)
    {

        $this->userEventMemArr=EventMembers::find()->Users($this->id)->all();
        $count=count($this->userEventMemArr);
        if($count==1){

            $result=$this->deleteUserByOneEventMem($teamId);
            if($result)
                return ['result'=>true,'msg'=>Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')])];
            else
                return ['result'=>false,'msg'=>Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')])];


        }elseif($count>1)
        {
            return ['result'=>false,'msg'=>Yii::t('app','User Have Several Role Login As University resp user then Delete it.')];

        }

    }

    public function deleteUserByOneEventMem($teamId=null)
    {

        $userId = $this->id ;
        $eventMemObj=$this->userEventMemArr[0];
        $eventId = Yii::$app->user->identity->eventId;
        $userDetailId=$eventMemObj->usersDetailsId;
        $teamId=$eventMemObj->teamsId;
        $picFileId=$this->fileId;

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            /** delete EventmemberRow in EventMember table**/
            $result3=EventMembers::deleteAll('teamsId=:teamsId And eventId = :eventId AND rolename=:role AND usersDetailsId=:userDetail', [':teamsId'=>$teamId,':eventId' => (int)$eventId,':role' => self::Role_Coach,':userDetail'=>$userDetailId]);

            /** delete responsed role in authitemtable**/
            $authAssignObj=new AuthAssignment();
            $resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_Coach.'" And event_id='.(int)$eventId).'';


            /** Delete User Record if one userdetail*/
            parent::beforeDelete();
            try {
                $result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();
            }
            catch (\Exception $ex)
            {
                return false;
            }

            /*Start Delete UserDetails Record*/
            $userDetailsObj=new UsersDetails();
            $userDetailsObj->id=$userDetailId;

            parent::beforeDelete();

            try {
                $resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

            }
            catch (\Exception $ex) {
                return false;
            }

            /*Delete User pic file*/
            if(isset($picFileId)&&$picFileId!=0)
            {

                if (($FileModel = FileAction::findOne($picFileId)) !== null) {
                    $FileModel->removeFile($picFileId);
                }
            }

            $transaction->commit();

            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
            throw $e;
        }
    }

}