<?php
//namespace backend\modules\YumUsers\models\SignupForm;
namespace backend\modules\YumUsers\models;

use backend\modules\YumUsers\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
	//public $username;
	//public $email;
	public $password;
	public $name;
	public $last_name;
	public $student_number;
	public $national_code;
	public $university_name;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['national_code', 'filter', 'filter' => 'trim'],
			['national_code', 'required'],
			['national_code', 'unique', 'targetClass' => '\backend\modules\YumUsers\models\User', 'message' => 'This national code has already been taken.'],
			['national_code', 'integer'],

			['name', 'filter', 'filter' => 'trim'],
			['name', 'required'],
			
			['last_name', 'filter', 'filter' => 'trim'],
			['last_name', 'required'],
			
			['student_number', 'filter', 'filter' => 'trim'],
			['student_number', 'required'],
			
			['university_name', 'filter', 'filter' => 'trim'],
			['university_name', 'required'],

			['password', 'required'],
			['password', 'string', 'min' => 6],
		];
	}

	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function signup()
	{
		if ($this->validate()) {
			$user = new User();
			$user->national_code = $this->national_code;
			$user->name = $this->name;
			$user->last_name = $this->last_name;
			$user->student_number = $this->student_number;
			$user->university_name = $this->university_name;			
			$user->setPassword($this->password);
			$user->generateAuthKey();
			//die(var_dump(errors()));
			//if(!$user->validate()){}
			//$user->save();
			if ($user->save()) {
				die;
		   // the following three lines were added:
			   $auth = Yii::$app->authManager;
			   $deafualtRole=Yum::module('YumUsers')->defaultRole;
			   $Role = $auth->getRole($deafualtRole);
			   $auth->assign($Role, $user->getId());
			   return $user;
			}
		}
		return null;
	}
}
