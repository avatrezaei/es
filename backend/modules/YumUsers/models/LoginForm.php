<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model {

	public $username;
	public $password;
	public $targetUser;
	public $rememberMe = true;
	private $_user = false;
	public $captcha;

	/**
	 * @inheritdoc
	 */
	public function rules() {

		//  die(var_dump($captchaRule));

		$rules = [
			// username and password are both required
			[['username', 'password'], 'required'],
			// rememberMe must be a boolean value
			['rememberMe', 'boolean'],
			// password is validated by validatePassword()
			['password', 'validateUserIsActive'],
			['password', 'validateIsUserBlock'],
			['password', 'validatePassword'],
				// ['captcha', 'captcha','captchaAction' => '/YumUsers/userauth/captcha'],
				// ['captcha', 'required']
		];


		$session = Yii::$app->session;
		if ($session->has('show_login_captcha') && $session->get('show_login_captcha') == TRUE) {
			$rules[] = ['captcha', 'captcha', 'captchaAction' => '/YumUsers/userauth/captcha'];
			$rules[] = ['captcha', 'required'];
		}
		return $rules;
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validateUserIsActive($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if ($user && (!$user->getActive() || !$user->getValid())) {
				$this->addError($attribute, Yii::t('app', Yii::t('app', 'User Is Deactive')));
			}
		}
	}

	public function validateIsUserBlock($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if ($user && ( $user->isUserBlock() && !$user->IsPassedLockTime())) {
				$blockTime = Yum::module('YumUsers')->BlockUserTime;
				$this->addError($attribute, Yii::t('app', Yii::t('app', 'User Is Block.Please Retry After {min} Minutues', array('min' => $blockTime))));
			}
		}
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user) {

				$this->addError($attribute, Yii::t('app', 'Incorrect username or password.'));
				return;
			}
			/* ---if password incorect check user has brutforce attack------ */
			if ($user && !$user->validatePassword($this->password)) {
				if ($user->isRepeatedBroutForcedUsername()) {
					$user->blockUserAccount();
					// Yum::log("BroutForce login of user: " . $user->username . ". and Blocked user", 'warning', 'application.modules.users.controllers.AuthController');
				} else if ($user->isBroutForcedUsername()) {
					$session = Yii::$app->session;
					$session->set('show_login_captcha', true);
				}

				$user->saveBadLoginAttempt();

				$this->addError($attribute, Yii::t('app', 'Incorrect username or password.'));
			}
		}
	}


	/**
	 * Logs in a user using the provided username and password.
	 *
	 * @return boolean whether the user is logged in successfully
	 */
	public function login() {

		if ($this->validate()) {
			/* -----clear brutforce history in success full login------------ */
			$user = $this->getUser();
			//$user->setLastLoginTime();
			$user->clearBrutForceHistory();
			/* -------Login proccess for set credential and sessions--------- */
			return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 2 * 1 : 0);
		} else {
			return false;
		}
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	public function getUser() {
		if ($this->_user === false) {
			$this->_user = User::findByUsername($this->username);
		}

		return $this->_user;
	}

	public function attributeLabels() {
		return [
			'captcha' => Yii::t('app','Captcha'),
			'username' => Yii::t('app','Username'),
			'password' =>Yii::t('app','Password'),
			'passwordInput' => Yii::t('app','passwordInput'),
			'rememberMe' => Yii::t('app','Remember Me'),
		];
	}

}
