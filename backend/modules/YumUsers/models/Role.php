<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * Login form
 */
class Role extends User
{
	public $user_id;
	public $_AuthAssignment;
	public $role;
	public $targetUser;

	public function attributeLabels()
	{
		return [
			'role' => Yii::t('app', 'Roles'),
			'name' => Yii::t('app', 'Name'),
		];
	}

//	   public function rules() {
//	   $rules[] = array(['targetUser,role'], 'required');
//	  $rules[] = array(['role'], 'RoleValidation');
//	 $rules[] = array('role', 'safe');
//
//		return $rules;
//	}
//	   public function RoleValidation($attribute, $params) {
//	
//		   if(Count($this->$attribute)<1)
//		   {
//				$this->addError($attribute,Yii::t('app', 'New password should not be like old password.'));
//				return ;
//		   }
//		   $roles=$this->getRolesAsListData();
//			
//	}
	//	   public function RoleValidation($attribute, $params) {
//	
//		   if(Count($this->$attribute)<1)
//		   {
//				$this->addError($attribute,Yii::t('app', 'New password should not be like old password.'));
//				return ;
//		   }
//		   $roles=$this->getRolesAsListData();
//			
//	}

	
	/*
	 * $model->validate dosent work 
	 * we have to write own validate input function
	 */
	public function validateInput()
	{
		if (empty($this->role)) {
			$this->addError('role', Yii::t('app', "Roles Can't be Empty"));
			return false;
		}
		if (empty($this->targetUser) || !is_numeric($this->targetUser)) {
			$this->addError('role', Yii::t('app', "The Security error"));
			return false;
		}

		$roles = $this->getRolesAsListData();
		foreach ($this->role as $key => $value) {

			if (!in_array($value, $roles)) {
				$this->addError('role', Yii::t('app', "The Security error"));
				return false;
			}
		}
		return TRUE;
	}

	public function changeRole($newRole, $userId)
	{
		$auth = Yii::$app->authManager;

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			$deletedItem = AuthAssignment::deleteAll('user_id=:userid', [':userid' => (int) $userId]);
			foreach ($newRole as $key => $role) {

				$Role = $auth->getRole($role);
				$auth->assign($Role, $userId);
			}
			$transaction->commit();
			return true;
		} catch (\Exception $e) {
			$transaction->rollBack();
			$this->addError('role', Yii::t('app', 'Failed To Change Role.'));
			return false;
		}
		return true;
	}

	public static function assignRole($newRole, $userId)
	{
		$userAssignment = Yii::app()->authManager->getRoles($userId);
		if (empty($userAssignment)) {
			Yii::app()->authManager->assign($newRole, $userId);
			return TRUE;
		}
		return FALSE;
	}

	public static function getRolesAsListData()
	{
		$roles = Yii::$app->authManager->getRoles();
		return ArrayHelper::map($roles, 'name', 'description');
	}

	public static function getRolesAsListDataPersian()
	{
		$roles = Yii::$app->authManager->getRoles();
		return ArrayHelper::map($roles, 'name', 'name');
	}

	public static function getUserRoleTittle($id)
	{
		$roleAssigned = Yii::app()->authManager->getRoles($id);

		if (is_array($roleAssigned))
			$roles = implode(",", $roleAssigned);
		else
			$roles = $roleAssigned;

		return (($roles) ? $roles : '------');
		//obtains all assigned roles for this user id	
	}

	public static function getUserRoles($userId = NULL)
	{
		$rolesArray = array();
		//this  code return arrays of object roles
		$rolesObjArray = Yii::$app->authManager->getRolesByUser((int) $userId);
		if (count($rolesObjArray) > 0) {
			foreach ($rolesObjArray as $index => $objRole) {
				if ($objRole->type == 1) {//type 1 is role ---type 2 is task --type 3 is operation
					$rolesArray[] = $objRole->name;
				}
			}
			return $rolesArray;
		}

		return $rolesArray;
	}

	public static function getPersianRole($role)
	{

		return Yii::t('app',$role);
	}

}
