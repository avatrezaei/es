<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\YumUsers\models\AthleteUser;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;

/**
 * UserSearch represents the model behind the search form about `backend\modules\YumUsers\models\User`.
 */
class MiscCaravanUserSearch extends MiscCaravanUser
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $nameRequirements = Yum::module('YumUsers')->nameRequirements;

        $rules[] = [['id', 'gender', 'email', 'tel', 'mobile', 'status', 'created_at', 'lock', 'valid', 'searchInputStuNo'], 'integer'];
        $rules[] = [['username', 'fileId', 'name', 'last_name', 'searchInputName', 'searchInputStuNo', 'searchInputLastName'], 'safe'];
        $rules[] = [['name', 'last_name', 'searchInputLastName'], 'string'];
        if ($nameRequirements) {
//			$rules[] = array(['name', 'last_name'], 'string',
//				'max' => $nameRequirements['maxLen'],
//				'min' => $nameRequirements['minLen'],
//			);
            $rules[] = array(
                ['searchInputLastName', 'searchInputName'],
                'match',
                'pattern' => $nameRequirements['match'],
                'message' => Yum::t($nameRequirements['dontMatchMessage']));
        }
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    

    public function searchMiscCaravanMembers($eventId, $caravanId)
    {

        /*show all Misc user*/
       // $miscTypeModels = UsersType::getMiscCaravanType();
        //$miscType = ArrayHelper::map($miscTypeModels, 'id', 'id');

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT u.id,u.name,u.last_name,em.user_type_id,u.idNo,u.nationalId,u.fileId,u.email,u.tel,u.mobile,u.birthDate,ud.sportMembershipHistory,ud.stuNo,ud.sportAssuranceNo,em.teamsId FROM user as u
	                 INNER JOIN event_members  as em
				       ON em.userId=u.id
                     INNER JOIN users_details  as ud
				       ON ud.usersId=u.id
				     WHERE
				         u.valid=1 AND em.valid=1 AND em.eventId=:eventId  AND  ud.eventId=:eventId AND em.user_type_id IN (23,24,25,26,3) And caravanId=:caravanId',
            'params' => [ ':eventId' => $eventId,':caravanId'=>$caravanId],
            'totalCount' => 200,
            'sort' => [
                'attributes' => [
                    'id' => [
                        'default' => SORT_DESC,
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);

        return $dataProvider;
    }
}
