<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\YumUsers\models\AthleteUser;
use yii\helpers\ArrayHelper;
use \backend\modules\YumUsers\models\UsersType;
/**
 * UserSearch represents the model behind the search form about `backend\modules\YumUsers\models\User`.
 */
class TechnicalCommitteeUserSearch extends TechnicalCommitteeUser {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		$nameRequirements = Yum::module('YumUsers')->nameRequirements;

		$rules[] = [['id', 'userField','gender',  'status', 'created_at', 'lock', 'valid'], 'integer'];
		$rules[] = [['userType','userField','username','fileId', 'name', 'last_name','searchInputName','searchInputLastName'], 'safe'];
		$rules[] = [[ 'name', 'last_name','searchInputLastName'], 'string'];
		if ($nameRequirements) {

			$rules[] = array(
				[ 'searchInputLastName','searchInputName'],
				'match',
				'pattern' => $nameRequirements['match'],
				'message' => Yum::t($nameRequirements['dontMatchMessage']));
		} return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params,$eventId) {

		$query = TechnicalCommitteeUser::find();
		$query->joinWith('eventMembers');
		$query->andFilterWhere(['user.valid' => 1,]);

		if(isset($params['columns'])){
			if(isset($params['columns'][7]['search'])&&$params['columns'][7]['search']['value'] != ''){
				$query->andFilterWhere(['=', 'event_members.user_type_id', (int)$params['columns'][7]['search']['value']]);
			}

			$query->joinWith('usersDetails');

			if(isset($params['columns'][7]['search'])&&$params['columns'][6]['search']['value'] != ''){
				$query->andFilterWhere(['=', 'users_details.sportFieldId', (int)$params['columns'][6]['search']['value']]);
			}
		}

		if(Yii::$app->user->can('AssociationPresident')){
			$query->andFilterWhere(['creatorUserId' => Yii::$app->user->identity->id]);
		}

		/*show all CommiteType user: referee,....*/
		$committeeTypeModels=UsersType::getTechnicalCommitteeType();
		$committeeType=ArrayHelper::map($committeeTypeModels, 'id','id');
		$query->andFilterWhere(['event_members.user_type_id' =>  $committeeType]);
		$this->load($params);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
			'pagination' => [
				'pageSize' => \app\models\Setting::getPaginationSetting(),
			],]);



		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'gender' => $this->gender,
			'status' => $this->status,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'login_attemp_count' => $this->login_attemp_count,
			'last_login_time' => $this->last_login_time,
			'last_login_attemp_time' => $this->last_login_attemp_time,
			'lock' => $this->lock,
			'event_members.eventId' => (int)$eventId,

		]);


		$query->andFilterWhere(['like', 'username', $this->username])
				->andFilterWhere(['like', 'name', $this->name])
				->andFilterWhere(['like', 'last_name', $this->last_name])
				->andFilterWhere(['like', 'email', $this->email])
				->andFilterWhere(['like', 'tel', $this->tel])
				->andFilterWhere(['like', 'mobile', $this->mobile]);


	

		return $dataProvider;
	}

	
	
}
