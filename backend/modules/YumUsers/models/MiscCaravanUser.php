<?php

namespace backend\modules\YumUsers\models;

use backend\models\AuthorizedUniversity;
use backend\models\EventMembers;
use backend\modules\YumUsers\models\YumPasswordValidator;
use yii\helpers\ArrayHelper;
use \yii\web\HttpException;
use Yii;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;
/* * *************
 * Yii validator
 */

class MiscCaravanUser extends User
{


    const Position_Minister='وزیر';
    const Position_AssistantMinister='معاون وزير';
    const Position_Chairman='رييس ستادي';
    const Position_HeadUniversity='رييس دانشگاه';
    const Position_AssistantUniversity='معاون دانشگاه';
    const Position_UniversityDirector='مدير دانشگاه';
    const Position_Headfaculty=' رييس دانشكده';
    const Position_DirectorfacultyDirector=' معاون دانشكده';
    const Position_Assistantfaculty=' مدير دانشكده';

    /*
     * Attribute in Modal Search shoud diferent name
     * to prevent Conflict in Main grid in indexaction
     */
    public $searchInputName;
    public $searchInputLastName;
    public $searchInputStuNo;


    public $userDetailId;
    public $userUniversityId;
    public $eventId;


    public $teamId;

    /*****SPORT EVENT FIELDS     *******/


    public $fileAttr;


    public function scenarios()
    {

        $scenarios = parent::scenarios();
			$scenarios['createMiscUserByExistUserDetail'] = ['teamId','userPosition','userType','fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			$scenarios['createMiscCaravanUserByExistUser'] = ['teamId','userPosition','userType','fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			$scenarios['createMiscCaravanUser'] = ['teamId','userPosition','userType','fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			$scenarios['UpdateMiscCaravanUser'] = ['teamId','idNo','fatherName','birthCityId','userPosition','userType','fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			return $scenarios;

		}


    public function rules()
    {
        //get setting of module
        $usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
        $passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
        $nameRequirements = Yum::module('YumUsers')->nameRequirements;

        /**Requirments***/
        $rules[] = [['userPosition','userType','nationalId','name', 'last_name',  'mobile'], 'required', 'on' => ['createMiscCaravanUser', 'createMiscCaravanUserByExistUser']];
        $rules[] = [['nationalId','name', 'last_name', 'mobile','gender'], 'required'];
        if($this->scenario!=='UpdateMiscCaravanUser')
        $rules[] = ['teamId', 'required'];

        /** password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator**/
        $passwordrule = array_merge(['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'], $passwordRequirements);
        $rules[] = $passwordrule;
        $rules[] = ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];


        /** username validation from setting*/

        if ($usernameRequirements) {
            $rules[] = ['username', 'string', 'max' => $usernameRequirements['maxLen'], 'min' => $usernameRequirements['minLen'],];
            $rules[] = ['username', 'match', 'pattern' => $usernameRequirements['match'], 'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
        }
        $rules[] = ['username', 'checkUnique'];

        /**
         * natinalId validation
         */
        if(!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id, User::Role_Admin )&&!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id,  User::Role_SeniorUserEvent)) {
            $rules[] = [['nationalId'], 'validateNationalId'];
        }

        $rules[] = ['nationalId', 'checkUnique'];
        $rules[] =array('nationalId', 'string', 'min'=>10, 'max'=>10);


        /** name validation from setting**/
        if ($nameRequirements) {
            $rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];

            $rules[] = [['name', 'last_name'], 'match', 'pattern' => $nameRequirements['match'], 'message' => Yum::t($nameRequirements['dontMatchMessage'])];
        }


        /** Emali validation**/
        $rules[] = ['email', 'email'];
        $rules[] = ['email', 'checkUnique'];
        $rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];


        /** date validation**/
        //$rules[] =  [['birthDate'], 'checkDateFormat'];



        /**
         * Numeric validation Utf8
         */
        //$rules[] =[['teamId','nationalId','tel','mobile','idNo',], 'integer'];
        $rules[] = array(['idNo','tel','mobile','nationalId'], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');


        /** Number validation**/
        $rules[] = array(['birthCityId','sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);

        /** String validation**/
        $rules[] = array(['univ', 'homepage', 'tel', 'mobile',], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

        /** Range validation**/
        $rules[] = array(['lock', 'valid'], 'in', 'range' => array(0, 1));
        $rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE, self::MALE));


        /** Exist validation**/
        //$rules[] = ['id', 'exist', 'targetClass' => '\backend\models\City'];


        /** Safe validation**/
        $rules[] = array(['teamId'], 'userTeamValidate',);

        $rules[] = array(['verifyCode'], 'safe',);
        /** Safe validation for save in db**/
        $rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
        $rules[] = [['fileAttr', 'sportNo', 'eName', 'eFamily', 'fatherName', 'idNo', 'nationalId', 'birthDate', 'birthCityId', 'fileId'], 'safe',];

        //$rules[] = array(['userPosition'], 'userPositionValidate',);

        /**
         * file validation
         */
        $fileModel=new File;

        $rules[] = [['fileAttr'], 'file',
          //  'skipOnEmpty' => false,
            'extensions' =>$this->fileTyps,
            'checkExtensionByMimeType' => true,
            'maxSize' => $this->fileSize,
            'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
            'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
            'minSize' => File::MIN_FILE_SIZE,
            'mimeTypes' =>$this->mimeFileTyps,

        ];

        $rules[] = [['fileAttr'], 'file',
            'skipOnEmpty' => false,
            'on'=>['createMiscUserByExistUserDetail','createMiscCaravanUserByExistUser','createMiscCaravanUser']
        ];

        $rules[] = array(['fileAttr'], 'required','on'=>['createMiscUserByExistUserDetail','createMiscCaravanUserByExistUser','createMiscCaravanUser']);
        $rules[] = array(['fileAttr'], 'safe',);

        /** other vlidations*/
        //  $rules[] = array('role', 'RolesTypeValidation',);
        return $rules;
    }


    public function userTeamValidate($attribute, $params) {
        $value = $this->$attribute;
        $teams= $dataTeams = ArrayHelper::map(\backend\models\Teams::find()->andWhere(['caravanId'=>$this->caravanId])->all(), 'id', 'id');
        if(isset($this->caravanId)&&isset($this->teamId))
        {

                if(!in_array($value,$teams))
                {
                    $this->addError($attribute, Yii::t('app', 'Invalid Teams'));
                    return false;
                }
        }
        return true;
    }

    public function userPositionValidate($attribute, $params) {
        $value = $this->$attribute;
        $VipUserPos=$this->getVIPPosisions();

        if($this->userType==UsersType::getUserTypeId(User::Type_VIPAttendant))
        {
          if(!in_array($value,$VipUserPos))
          {
              $this->addError($attribute, Yii::t('app', 'Invalid Input.'));
              return false;
          }
        }
        return true;

       }

 
    public function createMiscCarvanUser($carvanId = 0)
    {

        $eventId = Yii::$app->user->identity->eventId;

        $saveAttributes = $this->createSaveAttributes;



        if ($this->validate()) {

            /* Set nationalId as Default username And password*/
            if($this->scenario!='createMiscCaravanUserByExistUser'&&$this->scenario!='createMiscUserByExistUserDetail')
            {
                $this->username = isset($this->nationalId) ? $this->nationalId : '';
                $this->passwordInput = isset($this->mobile) ? $this->mobile : '';


                $this->setPassword($this->passwordInput);
                $this->generateAuthKey();

            }


            if($this->scenario=='createMiscCaravanUserByExistUser'&&$this->scenario=='createMiscUserByExistUserDetail')
            {
                $saveAttributes=$this->createExistSaveAttributes;
            }


            if ($result = $this->save(0,$saveAttributes)) {

                /*- Insert userdetail info in userDetail table*/
                try {
                    $auth = Yii::$app->authManager;
                    if (!Yii::$app->getAuthManager()->checkAccess($this->id, self::Role_Misc4Caravan)) {
                        $this->addRole(self::Role_Misc4Caravan);
                    }


                } catch (Exception $e) {
                    throw new HttpException(500, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
                }


                /*- Insert userdetail info in userDetail table*/
                try {
                    if ($this->scenario != 'createMiscUserByExistUserDetail') {
                        $this->insertUserDetailInfo($this->id, $eventId);
                    }
                } catch (Exception $e) {
                    throw new HttpException(500, 'مشکل در ذخیره اطلاعات پیش آمده است.');
                }


                /*- Insert Info in members event table*/
                try {

                    $this->insertMemberEventInfo($eventId, $carvanId);

                } catch (Exception $e) {
                    throw new HttpException(500, 'مشکل در ذخیره اطلاعات پیش آمده است.');
                }
                return TRUE;
            }
        }

        return false;
    }

    /*
     * insert info in eventMember table
     */
    public function insertMemberEventInfo($eventId, $carvanId)
    {

        $eventMemberObj = new EventMembers;
        $eventMemberObj->eventId = $eventId;
        $eventMemberObj->usersDetailsId = $this->userDetailId;
        $eventMemberObj->universityId = $this->userUniversityId;
        $eventMemberObj->caravanId = $carvanId;
        $eventMemberObj->rolename = self::Role_Misc4Caravan;
        $eventMemberObj->user_type_id =$this->userType;
        $eventMemberObj->userId = $this->id;
        $eventMemberObj->teamsId = $this->teamId;

        return $eventMemberObj->save();
    }


    public function insertUserDetailInfo($userId, $eventId)
    {

        $userDetailObj = new UsersDetails();
        $userDetailObj->usersId = $userId;
        $userDetailObj->eventId = $eventId;
        $userDetailObj->userPosition=$this->userPosition;

        $this->userDetailId = $userDetailObj->saveUniversityRespDetails();

        return $this->userDetailId;
    }


    public function updateMiscCarvanUser()
    {


        $eventId = Yii::$app->user->identity->eventId;

        if ($this->validate()) {

            $this->username = isset($this->nationalId) ? $this->nationalId : '';
            $this->passwordInput = isset($this->mobile) ? $this->mobile : '';
            $this->setPassword($this->passwordInput);
            $this->generateAuthKey();

            /*EventAdmin User just Update username before user login */

            if ($this->isUserLoginEver()) {
                /*dont save user if hacker set username by overposting*/
                $result = $this->save(1, ['name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'birthDate', 'password_hash', 'nationalId']);
            } else {
                /*save username*/
                $result = $this->save(1, ['username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'birthDate', 'password_hash', 'nationalId']);
            }
            return $result;
        }

        return false;
    }


    /*
     * diferent from createUser and modiy user:in modiy user dont call add role
     * that prevent integrity error db in auth_assignment in yii
     */

    public function modifyAthleteUser()
    {
        if ($this->validate()) {

            /*-Set nationalId as Default username And password-*/
            $this->username = isset($this->nationalId) ? $this->nationalId : '';
            $this->passwordInput = isset($this->nationalId) ? $this->nationalId : '';

            $this->setPassword($this->passwordInput);
            $this->generateAuthKey();
            if ($result = $this->save(0)) {

                return TRUE;
            }
        }

        return null;
    }

    public function addRole($role)
    {

        $auth = Yii::$app->authManager;
        $Role = $auth->getRole($role);
        $auth->assign($Role, $this->id);
    }


    public function getAuthassignment()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }


    public function deleteMiscUser1($id = NULL,$eventId) {

        parent::beforeDelete();

        if ($id != NULL)
            $this->id = $id;

        /* check user has how much User detail-if how one user details
        /* remove user record from user table if how multi user details
        /* just delete userdetails from userdetails table
        */

        $userDetailsCount=$this->getUsersDetailsCount();

        if($userDetailsCount==1)
        {
            $deleteUserRecord=1;
        }else
        {
            $deleteUserRecord=0;
        }

        $userDetailId=$this->getUserDetailIdBaseOnEventIdUserId($eventId);
        $picFileId=$this->fileId;

        /*******************-------BEGIN Transaction----------*****************/
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();



        try {
            /*******************-------BEGIN Transaction----------*****************/



            /*-------------------Delete User Record if one userdetail------------*/
            if($deleteUserRecord){
                if ($this->trulyDelete) {

                    $result= (parent::delete()) ? true : FALSE;

                } else {

                    parent::beforeDelete();

                    try {
                        $result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();

                    } catch (\Exception $ex)
                    {
                        return false;
                    }

                }
            }

            /*-------------------Start Delete UserDetails Record------------*/
            $userDetailsObj=new UsersDetails();
            $userDetailsObj->id=$userDetailId;

            if ($this->trulyDelete) {

                $resultUserDetails= ($userDetailsObj->delete()) ? true : FALSE;

            } else {

                parent::beforeDelete();

                try {
                    $resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

                } catch (\Exception $ex)
                {
                    return false;
                }

            }


            /*---------------------Delete Assigned Role-----------------*/
            $authAssignObj=new AuthAssignment();
            $resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_Misc4Caravan.'" And event_id='.(int)$eventId);


            /*------Delete User pic file-----------------*/
            if(isset($picFileId))
            {
                if (($FileModel = FileAction::findOne($picFileId)) !== null)
                {
                    $FileModel->removeFile($picFileId);
                }
            }

            /*------Delete related User Event Member-----*/
            $eventMemberObj=new EventMembers();
            $result2=$eventMemberObj->deleteAll('usersDetailsId ='.(int)$userDetailId.' AND rolename="'.self::Role_Misc4Caravan.'"');



            if( !$result2)
            {
                return false;
            }

            $transaction->commit();


            /*******************-------END Transaction----------*****************/

        } catch(Exception $e) {
            $transaction->rollback();
            return false;
        }

        /*******************-------END Transaction----------*****************/

        parent::afterDelete ();
        return ($result2);
    }




    public function getVIPPosisions()
{

    return  [
        self::Position_Minister=>self::Position_Minister,
        self::Position_AssistantMinister=>self::Position_AssistantMinister,
        self::Position_Chairman=>self::Position_Chairman,
        self::Position_HeadUniversity=>self::Position_HeadUniversity,
        self::Position_AssistantUniversity=>self::Position_AssistantUniversity,
        self::Position_UniversityDirector=>self::Position_UniversityDirector,
        self::Position_Headfaculty=>self::Position_Headfaculty,
        self::Position_DirectorfacultyDirector=>self::Position_DirectorfacultyDirector,
        self::Position_Assistantfaculty=>self::Position_Assistantfaculty,

    ];
}


    public function getTeamId($userDetailId,$caravanId,$userTypeId)
    {

        $eventMemberModel=EventMembers::find()->event(Yii::$app->user->identity->eventId)
            ->userdetails($userDetailId)
            ->type((int)$userTypeId)
            ->Users($this->id)
            ->caravan($caravanId)
            ->one();

        if($eventMemberModel!==null)
        {
            return $eventMemberModel->teamsId;
        }
        return false;
    }


    public function getTeamIdByUserIdCaravanId($caravanId)
    {

        $eventMemberModel=EventMembers::find()->event(Yii::$app->user->identity->eventId)
            ->Users($this->id)
            ->caravan($caravanId)
            ->one();

        if($eventMemberModel!==null)
        {
            return $eventMemberModel->teamsId;
        }
        return false;
    }
    public function updateTeamId($caravanId,$userTypeId,$newTeamId)
    {
        $eventMemberModel=EventMembers::find()->event(Yii::$app->user->identity->eventId)
           // ->userdetails($userDetailId)
            ->type((int)$userTypeId)
            ->Users($this->id)
            ->caravan($caravanId)
            ->one();
        $eventMemberModel->teamsId=(int)$newTeamId;
        $eventMemberModel->update(0);
 
    
    }
    public function deleteMiscUser($id = NULL,$eventId,$caravanId=null)
    {

        $this->userEventMemArr=EventMembers::find()->Users($this->id)->all();
        $count=count($this->userEventMemArr);
        if($count==1){

            return $this->deleteUserByOneEventMem();


        }elseif($count>1)
        {
            return $this->deleteUserBySeveralEventMem($caravanId);

        }

    }


    public function deleteUserByOneEventMem()
    {
        $picFileId=$this->fileId;

        $eventMemObj=$this->userEventMemArr[0];
        $eventId = Yii::$app->user->identity->eventId;
        $userDetailId=$eventMemObj->usersDetailsId;
        $teamId=$eventMemObj->teamsId;

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            /** delete EventmemberRow in EventMember table**/
            $result3=EventMembers::deleteAll('teamsId=:teamsId And eventId = :eventId AND rolename=:role AND usersDetailsId=:userDetail', [':teamsId'=>$teamId,':eventId' => (int)$eventId,':role' => self::Role_Misc4Caravan,':userDetail'=>$userDetailId]);

            /** delete responsed role in authitemtable**/
            $authAssignObj=new AuthAssignment();
            $resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_Misc4Caravan.'" And event_id='.(int)$eventId).'';


            /** Delete User Record if one userdetail*/
            parent::beforeDelete();
            try {
                $result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();
            }
            catch (\Exception $ex)
            {
                return false;
            }

            /*Start Delete UserDetails Record*/
            $userDetailsObj=new UsersDetails();
            $userDetailsObj->id=$userDetailId;

            parent::beforeDelete();

            try {
                $resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

            }
            catch (\Exception $ex) {
                return false;
            }

            /*Delete User pic file*/
            if(isset($picFileId)&&$picFileId!=0)
            {

                if (($FileModel = FileAction::findOne($picFileId)) !== null) {
                    $FileModel->removeFile($picFileId);
                }
            }


            $transaction->commit();

            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            //	return false;
            throw $e;
        }
    }
    public function deleteUserBySeveralEventMem($caravanId)
    {
        $countUniversityRespPosition=0;
        $countEventType=0;
        $eventMemObj=null;

        $eventId = Yii::$app->user->identity->eventId;

        $teamId=$this->getTeamIdByUserIdCaravanId($caravanId);

        /*get target event mem for this university universityResp role*/
        foreach ($this->userEventMemArr as $i=>$eventObj)
        {

            if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_Misc4Caravan && $eventObj->teamsId==$teamId)
                $eventMemObj=$eventObj;

            /*count  position of Role_Coach for user  */
            if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_Misc4Caravan)
                $countUniversityRespPosition++;

            /*count of user $countEventType in this event*/
            if($eventObj->eventId==$eventId)
                $countEventType++;
        }

        if($eventMemObj==null)
              return false;


        $userDetailId=$eventMemObj->usersDetailsId;
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            /** delete EventmemberRow in EventMember table**/
            $result3=$eventMemObj->delete();


            /** delete responsed role in authitemtable if just have one university resp position**/
            if($countUniversityRespPosition==1)
            {
                $authAssignObj=new AuthAssignment();
                $resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_Misc4Caravan.'" And event_id='.(int)$eventId).'';
            }


            if($countEventType==1)
            {

                /*Start Delete UserDetails Record*/
                $userDetailsObj=new UsersDetails();
                $userDetailsObj->id=$userDetailId;

                parent::beforeDelete();

                try {
                    $resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

                }
                catch (\Exception $ex) {
                    return false;
                }
            }



            $transaction->commit();

            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
            throw $e;
        }

    }

}
