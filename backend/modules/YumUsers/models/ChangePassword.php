<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class ChangePassword extends User
{

	public $targetUser;
	public $inputOldPassword;
	public $inputNewPassword;
	public $inputNewPassword_repeat;
	public $validateOLdPassword;
	public $_user = false;
	public $username;

	//for verify addmin
	public $passwordInput;

	public function rules()
	{
		//get setting of module
		$passwordRequirements = Yum::module('YumUsers')->passwordRequirements;

		/**
		 * password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator
		 */
		$passwordrule = array_merge(array('inputNewPassword', 'backend\modules\YumUsers\models\YumPasswordValidator', 'except' => 'register')
				, $passwordRequirements
		);

		$rules[] = $passwordrule;


	   $rules[] = array(['targetUser'], 'required','on' => ['change_password_byadmin']);
	   $rules[] = array(['inputNewPassword', 'inputNewPassword_repeat'], 'required','on' => ['change_user_password','change_password_byadmin']);
	   $rules[] = array(['passwordInput'], 'required','on' => ['verify_admin_pass','change_password_byadmin']);
	   $rules[] = array(['inputOldPassword'], 'required', 'on' => 'change_user_password');
	   $rules[] = array(['inputNewPassword'], 'CheckNewPassword', 'on' => ['change_user_password','change_password_byadmin']);
	   $rules[] = array('inputNewPassword_repeat', 'safe');
	   $rules[] = array('inputNewPassword', 'compare','on' => ['change_user_password','change_password_byadmin']);

		return $rules;
	}

   /*-------- newPassword shoudn't equal to oldPassword---------------------*/
	public function CheckNewPassword($attribute, $params)
	{
		$user = $this->getUser();
			
		if ($user && $user->validatePassword($this->inputNewPassword)) {
			$this->addError($attribute,Yii::t('app', 'New password should not be like old password.'));
		}
		
		$this->_user = false;
		return;
	}

	public function attributeLabels() {
		return [
			'inputNewPassword' => Yii::t('app', 'Input New Password'),
			'inputOldPassword' => Yii::t('app', 'Input Old Password'),
			'inputNewPassword_repeat' => Yii::t('app', 'Confirm Password'),
			'passwordInput' => Yii::t('app', 'Current Password Input'),
			'targetUser' => Yii::t('app', 'Target User'),
		];
	}

	public function changePassword($userId = null) {
		if ($userId !== NULL) {
			$this->targetUser = $userId;
		}
	  
		$userObj = User::findOne($this->targetUser);
		$userObj->setPassword($this->inputNewPassword);

		if ($userObj->save(false, ['password_hash'])) {
			return true;
		}
		return false;
	}

	public function ValidateOldPassword() {

		$user = $this->getUser();
		if ($user && $user->validatePassword($this->inputOldPassword)) {
			return true;
		}
		else{
			$this->addError('inputOldPassword', Yii::t('app', 'The old password is incorrect. Please retype your password.'));
			return false;	
		}
		return false;
	}

	public function ValidateCurrentPassword()
	{
		$user = $this->getUser();
		if ($user && $user->validatePassword($this->passwordInput)) {
			return true;
		}
		else {
			$this->addError('passwordInput', Yii::t('app', 'The password is incorrect. Please retype your password.'));
			return false;	
		}

		return false;
	}

	public function getUser($userName = NULL) {
		if ($userName != NULL)
			$this->username = $userName;
	
		$this->_user = User::findByUsername($this->username);
	   
		return $this->_user;
	}
}
