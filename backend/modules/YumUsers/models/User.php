<?php

namespace backend\modules\YumUsers\models;

use backend\models\Teams;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\captcha\Captcha;


/*
 * Yii validator
 */
use backend\models\jdf;
use yii\validators\Validator;
use yii\validators\StringValidator;
use yii\validators\NumberValidator;
use backend\modules\YumUsers\models\YumPasswordValidator;
use backend\modules\YumUsers\models\ProActiveRecord;
use backend\modules\YumUsers\models\UsersDetails;
use backend\modules\YumUsers\models\AuthAssignment;

use backend\models\EventMembers;
use backend\models\Messages;


use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "user".
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property integer $gender
 * @property integer $marital
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $active
 * @property integer $active_email
 * @property integer $valid
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Membership[] $memberships
 * @property integer $fileId
 * @property string $sportNo
 * @property string $eName
 * @property string $eFamily
 * @property string $fatherName
 * @property integer $idNo
 * @property integer $nationalId
 * @property string $birthDate
 * @property string $params
  * @property string $address
 * @property string $jobincome
 * @property integer $creatorUserId
 * @property integer $student_number;
	* @property integer $national_code
	* @property string $university_name
	* @property integer $city_location_id
	* @property integer $shift
	* @property integer $startingyear
	* @property integer $startingsemester
	* @property integer $units
	* @property string $major
 */
class User extends ProActiveRecord implements IdentityInterface
{
	public $existUserFlag;//related in exist natiionalId
	const CREATE_NEW_USER=1;
	const CREATE_NEW_USER_EVENT=2;
	const CREATE_EXIST_USER_EVENT=3;

	public $userField;
	public $userType;
	public $passwordInput;
	public $passwordInput_repeat;

	public $teamId;
	public $caravanId;
	public $userPosition;

	/**
	 * UserDetails Fields
	 */
	public $stuNo;
	public $sportAssuranceNo;
	public $educationalField;
	public $sportMembershipHistory;


	public $userEventId;
	/*for stay create page or redirect after save user*/
	public $saveAndNew=0;


	public $lang;
	public $edu;
	public $grade;
	public $univ;

	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;
	const Active = 1;
	const InActive = 0;
	const MALE = 'man';
	const FEMALE = 'woman';
	const SINGLE = 'single';
	const MARRIED = 'married';
	const GRADE_PHD = 1;
	const GRADE_PHD_STUDENT = 2;
	const GRADE_MASTER = 3;
	const GRADE_MASTER_STUDENT = 4;
	const GRADE_BACHELOR = 5;
	const GRADE_BACHELOR_STUDENT = 6;
	const GRADE_KARDANI = 7;
	const GRADE_KARDANI_STUDENT = 8;
	const GRADE_DIPLOMA = 9;

	public $organization_id;
	public $role;
	public $verifyCode;
	public $generatedPass;


	public $trulyDelete=0;
	public $fileAttr;
	public $fileAttr1;
	public $fileAttr2;
	public $fileAttr3;


	/* user pic files */
	public $fileTyps='png,jpeg,jpg';
	public $fileExtentionsArr=['png','jpg','jpeg'];
	public $mimeFileTyps=['image/jpeg','image/jpg','image/png'];
	public $fileSize=512000;//500 KB
	public $fileSizeKB=500;//500 KB

	/*need for delete user*/
	public $userEventMemArr=[];


	public $_userDetailObj;

	/*SPORT EVent roles*/
	const Role_Admin='admin';
	const Role_Athlete='Athlete';
	const Role_Coach='Coach';
	const Role_reader='reader';
	const Role_Referee='Referee';
	const Role_CarvanSupervisor='CarvanSupervisor';
	const Role_ResponsibleAccommodation='ResponsibleAccommodation';
	const Role_ResponsibleFinance='ResponsibleFinance';
	const Role_ResponsibleNutrition='ResponsibleNutrition';
	const Role_ResponsiblePlanningSportsHall='ResponsiblePlanningSportsHall';
	const Role_ResponsibleRecordResults='ResponsibleRecordResults';
	const Role_UniversityRepresentative='UniversityRepresentative';
	const Role_ResponsibleTransport='ResponsibleTransport';
	const Role_SeniorMemberSystem='SeniorMemberSystem';
	const Role_SeniorUserEvent='SeniorUserEvent';
	const Role_TechnicalCommittee='TechnicalCommittee';
	const Role_ExecutiveCommittee='ExecutiveCommittee';
	const Role_Misc4Caravan='MiscCaravan';
	const Role_LocalDriver='LocalDriver';
	const Role_MediaStaff='MediaStaff';
	const Role_VIPAttendant='VIPAttendant';




	/* SPORT EVent User type*/
	const Type_Admin='admin';
	const Type_Athlete='Athlete';
	const Type_Coach='Coach';
	const Type_CoachHead='CoachHead';
	const Type_CoachAssistant='CoachAssistant';
	const Type_TeamSupervisor='TeamSupervisor';
	const Type_reader='reader';
	const Type_Referee='Referee';
	const Type_CarvanSupervisor='CarvanSupervisor';
	const Type_ResponsibleAccommodation='ResponsibleAccommodation';
	const Type_ResponsibleFinance='ResponsibleFinance';
	const Type_ResponsibleNutrition='ResponsibleNutrition';
	const Type_ResponsiblePlanningSportsHall='ResponsiblePlanningSportsHall';
	const Type_ResponsibleRecordResults='ResponsibleRecordResults';
	const Type_UniversityRepresentative='UniversityRepresentative';
	const Type_ResponsibleTransport='ResponsibleTransport';
	const Type_SeniorMemberSystem='SeniorMemberSystem';
	const Type_SeniorUserEvent='SeniorUserEvent';
	const Type_TechnicalCommittee='TechnicalCommittee';
	const Type_ExecutiveCommittee='ExecutiveCommittee';
	const Type_Misc4Caravan='Misc4Caravan';
	const Type_Driver='Driver';
	const Type_Attendant='Attendant';
	const Type_SupportStaff='SupportStaff';
	const Type_VIPAttendant='VIPAttendant';
	const Type_MedicalStaff='MedicalStaff';
	const Type_LocalDriver='LocalDriver';
	const Type_MediaStaff='MediaStaff';


	/*For Prevent OverPosting Attack define attribute that save in createUser*/
	public $createSaveAttributes = ['id','username','name','last_name','fatherName','gender','nationalId','sportNo',
                              'idNo','birthDate','birthCityId','auth_key','password_hash','password_reset_token',
                              'email','tel','mobile','creatorUserId',  'fileId','created_at', 'updated_at'];



	public $createExistSaveAttributes = ['name','last_name','fatherName','gender','marital','sportNo',
		'idNo','birthDate','birthCityId','email','tel','mobile','fileId', 'updated_at'];
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%user}}';
	}

	/**
	 * @inheritdoc
	 * @return NutritionAllotmentQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new UserQuery(get_called_class());
	}

	public function scenarios()
	{
		$scenarios = parent::scenarios();

		$scenarios['createUser'] = ['userPosition','name','last_name','tel','mobile','birthDate','gender','email',  'username','fileId','passwordInput','passwordInput_repeat','nationalId','idNo','nationalId','fatherName','birthCityId','university_name','national_code','student_number','passwordInput','passwordInput_repeat'];
	//Scenario Values Only Accepted
		$scenarios['UpdateUser'] = ['userPosition','name','last_name','tel','mobile','birthDate','gender','marital','email',  'fileId','passwordInput','passwordInput_repeat','nationalId','idNo','national_code','fatherName','birthCityId'];//Scenario Values Only Accepted

		$scenarios['UpdateProfileInfo'] = ['userPosition','name','last_name','tel','mobile','birthDate','gender','marital','email',  'fileId','passwordInput','passwordInput_repeat','sportNo',  'fatherName', 'idNo','nationalId','birthCityId','university_name','national_code','student_number','city_location_id','address','jobincome','startingyear','startingsemester','units','major','shift'];

		return $scenarios;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		//get setting of module
		$usernameRequirements	= Yum::module('YumUsers')->usernameRequirements;
		$passwordRequirements 	= Yum::module('YumUsers')->passwordRequirements;
		$nameRequirements 		= Yum::module('YumUsers')->nameRequirements;

		/**
		 * Requirments
		 */
		$rules[] = [['name','last_name','university_name','student_number'],'required','on'=>['createUser']];
		$rules[] = [['name','last_name','email','mobile'],'required','on'=>['UpdateUser','UpdateProfileInfo']];


		/** password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator**/
		$passwordrule = array_merge(
			['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'],
			$passwordRequirements
		);

		$rules[] = $passwordrule;
		$rules[] = ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];

		/**
		 * username validation from setting
		 */
		if ($usernameRequirements) {
			$rules[] = ['username', 'string',  'max' => $usernameRequirements['maxLen'],'min' => $usernameRequirements['minLen'],];
			$rules[] = ['username', 'match','pattern' => $usernameRequirements['match'],'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
		}
		$rules[] = ['username', 'checkUnique'];

		/**
		 * name validation from setting
		 */
		if ($nameRequirements) {
			$rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];

			$rules[] = [['name', 'last_name'], 'match',  'pattern' => $nameRequirements['match'],'message' => Yum::t($nameRequirements['dontMatchMessage'])];
		}

		/**
		 * Emali validation
		 */
		$rules[] = ['email', 'email'];
		$rules[] = ['email', 'checkUnique'];

		//$rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];

		/**
		 * date validation
		 */
		//$rules[] =  [['birthDate'], 'checkDateFormat'];

		/**
		 * natinalId validation
		 */
		//$rules[] =  [['nationalId'], 'validateNationalId'];
		$rules[] = ['nationalId', 'checkUnique'];
		$rules[] =array('nationalId', 'string', 'min'=>10, 'max'=>10);

		/**
		 * Numeric validation Utf8
		 */

		//$rules[] = array(['tel','mobile'], 'match','pattern' => '/^([\u0660-\u0669]|[\u0030-\u0039]|[\u06F0-\u06F9]|[\d])*$/','message' => ' فقط از کاراکترهای عددی و علامت -استفاده شود.');
		$rules[] = array(['idNo','tel','mobile','nationalId',], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');


		/** Number validation**/
		$rules[] = array(['sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);



		/**
		 * Number validation
		 */
		$rules[] = array(['birthCityId','sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);

		/**
		 * String validation
		 */
		$rules[] = array([ 'univ', 'homepage', 'tel', 'mobile', ], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

		/**
		 * Range validation
		 */
		$rules[] = array([ 'lock', 'valid'], 'in', 'range' => array(0, 1));
		$rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE,self::MALE));
		$rules[] = array(['marital',], 'in', 'range' => array(self::SINGLE,self::MARRIED));

		/**
		 * Exist validation
		 */
		//$rules[] =  ['id', 'exist', 'targetClass' => '\backend\models\City'] ;

		/**
		 * Safe validation
		 */

		$rules[] = array(['verifyCode'], 'safe',);

		/**
		 * Safe validation for save in db
		 */
		$rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
		$rules[] = [['userType','fileAttr','fileAttr1','fileAttr2','fileAttr3','sportNo', 'eName', 'eFamily', 'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'], 'safe',];

		/**
		 * file validation
		 */
		$fileModel=new File;

		$rules[] = [['fileAttr','fileAttr1','fileAttr2','fileAttr3'], 'file',
			'skipOnEmpty' => false,
			'extensions' =>$this->fileTyps,
			'checkExtensionByMimeType' => true,
			'maxSize' => $this->fileSize,
			'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
			'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
			'minSize' => File::MIN_FILE_SIZE,
			'mimeTypes' =>$this->mimeFileTyps,
			'on'=>'createUser',
			];
		$rules[] = array(['fileAttr','fileAttr1','fileAttr2','fileAttr3'], 'required',);

		/**
		 * Captcha validation
		 */
		$rules[] = array('verifyCode', 'captcha',
			'isEmpty' => !Captcha::checkRequirements(),
			'on' => 'register,captch,forgetpass',
			'message' => ' تصویر امنیتی  صحیح را وارد کنید.'
		);

		/**
		 * other vlidations
		 */
		//  $rules[] = array('role', 'RolesTypeValidation',);
		return $rules;
	}





	public function semanticTeamCaravanValidation($attribute, $params) {

		$teamId=(int)$this->$attribute;
		$caravanId=(int)$this->caravanId;
		$teamObj=Teams::find()->andWhere(['id'=>$teamId,'caravanId'=>$caravanId])->one();

		if($teamObj==null)
		{

			$this->addError($attribute, Yii::t('app', 'Invalid Team and Caravan'));
			return false;
		}

		return true;
	}


	public  function checkUnique($attribute, $params)
	{

		$value = $this->$attribute;
		$userModels = $this->find()->where($attribute . '= :attribute And valid=1', [':attribute' => $value])->all();

		$result=1;
		foreach ($userModels as $userModel)
		{

			if (isset($this->isNewRecord)&&$this->getIsNewRecord()) {
				if ($userModel !== null) {
					$result= false;
				}
			} else {


				if ($userModel !== null && $this->id !== $userModel->id) {
					$result= false;

				}
			}
		}

		if(!$result)
		{
			$this->addError($attribute, Yii::t('app', 'The {item} Id is Repetitive',['item' => $this->getAttributeLabel($attribute)]));
			return false;
		}


		return true;
	}



	public  function validateNationalId($attribute, $params)
	{
		$param = $this->$attribute;

		if(strlen($param) < 8){
			$this->addError($attribute, Yii::t('app', 'فرمت کدملی صحیح  نیست'));
			return false;
		}

		if(strlen($param) == 8)
			$param = "00" . $param ;

		if(strlen($param) == 9)
			$param = "0" . $param ;

		if(strlen($param) > 10){
			$this->addError($attribute, Yii::t('app', 'فرمت کدملی صحیح  نیست'));
			return false;
		}

		$chk = substr($param,9,1) ;
		$s = 0;

		for($i=1;$i<=9;$i++)
			$s = $s + intval(substr($param,$i-1,1)) * (11 - $i);

		$r = $s % 11 ;

		if(($r<2 && $chk==$r) || ($r>=2 && $chk==11-$r)){
			return true;
		}
		else{
			$this->addError($attribute, Yii::t('app', 'فرمت کدملی صحیح  نیست'));
			return false;
		}

	}


	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app','ID'),
			//'username' => Yii::t('default', 'Username',Yum::module('users')->basePath.'\messages\fa'),
			'username' => Yii::t('app','Username'),
			'password' => Yii::t('app','Password'),
			'salt' => Yii::t('app','Salt'),
			'name' => Yii::t('app','Name'),
			'last_name' => Yii::t('app','Last Name'),
			'organization_id' => Yii::t('app','Organization'),
			'gender' => Yii::t('app','Gender'),
			'marital' => Yii::t('app','Marital'),
			'homepage' => Yii::t('app','Homepage'),
			'tel' => Yii::t('app','Tel'),
			'mobile' => Yii::t('app','Mobile'),
			'email' => Yii::t('app','Email'),
			'type_assist' => Yii::t('app','Type Assist'),
			'login_attemp_count' => Yii::t('app','Login Attemp Count'),
			'updated_at' => Yii::t('app','Update At'),
			'update_date' => Yii::t('app','Update Date'),
			'created_at' => Yii::t('app','Create At'),
			'create_date' => Yii::t('app','Create Date'),
			'last_login_time' => Yii::t('app','Last Login Time'),
			'last_login_attemp_time' => Yii::t('app','Last Login Attemp Time'),
			'lastaction' => Yii::t('app','Last Action'),
			'lock' => Yii::t('app','Lock'),
			'valid' => Yii::t('app','Valid'),
			'active' => Yii::t('app','Active'),
			'role' => Yii::t('app','Role'),
			'lang' => Yii::t('app','Language'),
			'passwordInput' => Yii::t('app','passwordInput'),
			'passwordInput_repeat' => Yii::t('app','passwordInput Repeat'),
			'sportNo' => Yii::t('app','Sport Number'),
			'eName' => Yii::t('app','English Name'),
			'eFamily' => Yii::t('app','English Family'),
			'fatherName' => Yii::t('app','Father Name'),
			'idNo' => Yii::t('app','Id Number'),
			'birthDate' => Yii::t('app','BirthDate'),
			'birthCityId' => Yii::t('app','BirthCity'),
			'fileAttr'=>Yii::t('app', 'Avatar Pic'),
			'fileAttr1'=>Yii::t('app', 'صفحه اول شناسنامه'),
			'fileAttr2'=>Yii::t('app', 'صفحه دوم شناسنامه'),
			'fileAttr3'=>Yii::t('app', 'تصویر کارت ملی'),
			'nationalId'=>Yii::t('app', 'National Id'),
			'creatorUserId'=>Yii::t('app', 'Creator User Id'),
			'teamId'=>Yii::t('app', 'Team'),
			'caravanId'=>Yii::t('app', 'Caravan'),
			'userType'=>Yii::t('app', 'User Type'),
			'userPosition'=>Yii::t('app', 'User Position'),
			'athleteSubFieldId'=>Yii::t('app', 'Athlete Sub Field Id'),
			'national_code'=>Yii::t('app', 'National Code'),
			'university_name'=>Yii::t('app', 'University Name'),
			'student_number'=>Yii::t('app', 'Student Number'),
			'city_location_id'=>Yii::t('app', 'شهر محل سکونت'),
			'address'=>Yii::t('app', 'Address'),
			'jobincome'=>Yii::t('app', 'Job in Come'),
			'marital'=>Yii::t('app', 'وضعیت تاهل'),
			'shift'=>Yii::t('app', 'دوره'),
			'startingyear'=>Yii::t('app', 'سال ورود'),
			'startingsemester'=>Yii::t('app', 'نیمسال ورود'),
			'units'=>Yii::t('app', 'تعداد واحد گذرانده شده'),
			'major'=>Yii::t('app', 'گرایش'),
			'universityId'=>Yii::t('app','University Name'),
		];
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = [
			'Gender' => [
				self::FEMALE => Yii::t('app', 'Female'),
				self::MALE => Yii::t('app', 'Male'),
			],
			'Marital' => [
				self::SINGLE => Yii::t('app', 'Single'),
				self::MARRIED => Yii::t('app', 'Married'),
			],
			'GenderText' => [
				self::FEMALE => Yii::t('app', 'Mrs.'),
				self::MALE => Yii::t('app', 'Mr.'),
			],
			'Grade' => [
				self::GRADE_PHD => Yii::t('app', 'PHD'),
				self::GRADE_PHD_STUDENT => Yii::t('app', 'PHD_STUDENT'),
				self::GRADE_MASTER => Yii::t('app', 'MASTER'),
				self::GRADE_MASTER_STUDENT => Yii::t('app', 'MASTER_STUDENT'),
				self::GRADE_BACHELOR => Yii::t('app', 'BACHELOR'),
				self::GRADE_BACHELOR_STUDENT => Yii::t('app', 'BACHELOR_STUDENT'),
				self::GRADE_KARDANI => Yii::t('app', 'kARDANI'),
				self::GRADE_KARDANI_STUDENT => Yii::t('app', 'kARDANI_STUDENT'),
				self::GRADE_DIPLOMA => Yii::t('app', 'DIPLOMA'),
			],
		];
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public function getFullName()
	{
		return $this->name . ' ' . $this->last_name;
	}

	public function getSex()
	{
		$class = '';

		switch ($this->gender) {
			case self::MALE:
				$class = 'success';
				break;

			case self::FEMALE:
				$class = 'info';
				break;

			default:
				$class = 'danger';
				break;
		}
		return '<span class="label label-' . $class . '">' . ($this->gender != NULL ? self::itemAlias('Gender', $this->gender) : Yii::t('app', 'not_find')) . '</span>';
	}
	public function getMarital()
	{
		$class = '';

		switch ($this->marital) {
			case self::SINGLE:
				$class = 'success';
				break;

			case self::MARRIED:
				$class = 'info';
				break;

			default:
				$class = 'danger';
				break;
		}
		return '<span class="label label-' . $class . '">' . ($this->gender != NULL ? self::itemAlias('Marital', $this->marital) : Yii::t('app', 'not_find')) . '</span>';
	}

	public function getGenderText()
	{
		if($this->gender == NULL)
			return NULL;

		return self::itemAlias('GenderText', $this->gender);
	}

	public function getShowGrade()
	{
		$class = '';

		switch ($this->grade) {
			case self::GRADE_PHD :
				$class = 'success';
				break;

			case self::GRADE_PHD_STUDENT :
				$class = 'brown';
				break;

			case self::GRADE_MASTER :
				$class = 'blue';
				break;

			case self::GRADE_MASTER_STUDENT :
				$class = 'green';
				break;

			case self::GRADE_BACHELOR :
				$class = 'info';
				break;

			case self::GRADE_BACHELOR_STUDENT  :
				$class = 'pink';
				break;

			case self::GRADE_KARDANI :
				$class = 'red';
				break;

			case self::GRADE_KARDANI_STUDENT :
				$class = 'warning';
				break;

			case self::GRADE_DIPLOMA :
				$class = 'danger';
				break;

			default:
				$class = 'default';
				break;
		}
		return '<span class="label label-' . $class . '">' . ($this->grade != NULL ? self::itemAlias('Grade', $this->grade) : Yii::t('app', 'not_find')) . '</span>';
	}

	public function getLastLoginTime()
	{
		return $this->last_login_time != '0000-00-00 00:00:00' ? Yii::$app->formatter->asDate($this->last_login_time) : '<span class="label label-warning">' . Yii::t('zii', 'Never') . '</span>';
	}


	public function getEventMembers() {
		return $this->hasMany(EventMembers::className(), ['userId' => 'id']);
	}

	public function getCreator() {
		return $this->hasOne(User::className(), ['id' => 'creatorUserId']);
	}


	public function getEventMembersByEvent() {
		$eventId = isset(Yii::$app->user->identity->eventId)?Yii::$app->user->identity->eventId:0;
		return $this->hasOne(EventMembers::className(), ['userId' => 'id'])->andOnCondition(['eventId' => (int)$eventId]);;
	}

	public function getUsersDetails() {
		return $this->hasMany(UsersDetails::className(), ['usersId' => 'id']);
	}

	public function getMessages() {
		return $this->hasMany(Messages::className(), ['smsUserId' => 'id']);
	}

	/* Autentication user model */

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}



	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE,'valid'=>1]);
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token) {
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}

		return static::findOne([
			'password_reset_token' => $token,
			'status' => self::STATUS_ACTIVE,
		]);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token) {
		if (empty($token)) {
			return false;
		}
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int) end($parts);
		return $timestamp + $expire >= time();
	}

	/**
	 * @inheritdoc
	 */
	public function getId() {
		return $this->getPrimaryKey();
	}

	public function getSMSCount() {
		return count($this->messages);
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
		return $this->auth_key;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey() {
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}

	public function createUser()
	{
		if ($this->validate()) {
			$this->username=$this->nationalId;
			$this->passwordInput=$this->mobile;
			$this->setPassword($this->passwordInput);
			$this->generateAuthKey();
			if ($this->save(0)) {

				try {
					//$this->addRole(self::Role_reader);

				}
				catch (Exception $e){
					throw new \yii\web\HttpException(403, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
				}

				return TRUE;
			}
		}

		return false;
	}



	public function signup()
	{
		if ($this->validate()) {
			$user = new User();
			$user->username = $this->username;
			$user->email = $this->email;
			$user->setPassword($this->password);
			$user->generateAuthKey();
			if ($user->save()){
				return $user;
			}
		}

		return null;
	}



	public function getActiveOption() {
		return array(
			self::Active => Yum::t('Yes', array(), 'button'),
			self::InActive => Yum::t('No', array(), 'button'),
		);
	}


	public function getActiveText($attribute) {
		$activeOption = $this->getActiveOption();
		return ((isset($activeOption[$this->$attribute])) ? $activeOption[$this->$attribute] : 'Unknown Value');
	}




	public function semanticDateValidation($attribute, $params) {
			/*
			 * if date format is ok and  has no errors this validate acuuure
			 */
			if (!empty($this->errors['from_date']) || !empty($this->errors['to_date'])) {
					return;
			}


			$jdf = new jdf();


			if (isset($from_date) && isset($to_date) && strtotime($to_date) < strtotime($from_date)) {
					$this->addError($attribute, Yii::t('app', 'Start date must be proportionate to the end date'));
					return;
			}
	}

	public function getUserInfo($userIdAttribute) {
		$user = $user = User::findOne($this->$userIdAttribute);

		if ($user) {
			$name = $user->name;
			$lastName = $user->last_name;
			$author = $name . " " . $lastName;
		}
		return(isset($author)) ? ($author) : "-----";
	}

	public function getJalalyDate($date) {
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d', strtotime($this->$date));
		return $jDateString;
	}

	public function getJalalyDateByDelimeter($date,$delimeter) {
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d', strtotime($this->$date));
		$jDateString=str_replace("-",$delimeter,$jDateString);
		return $jDateString;
	}

	public function getFullJalalyDate($date) {
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d  H:i:s', strtotime($this->$date));
		return $jDateString;
	}

	public function getEnDateFromTimeStamp($dateField) {
		$rows = (new \yii\db\Query())
				->select(['DATE_FORMAT( `' . $dateField . '` , "%Y-%m-%d" ) AS date'])
				->from($this->tableName())
				->where('id=:id', array(':id' => $this->id))
				->all();
		$gDateString = isset($rows[0]['date']) ? $rows[0]['date'] : '';
		$this->$dateField = $gDateString;
		return $gDateString;
	}

	public function getFaDateFromTimeStamp($dateField) {
		$rows = (new \yii\db\Query())
				->select(['DATE_FORMAT( `' . $dateField . '` , "%Y-%m-%d" ) AS date'])
				->from($this->tableName())
				->where('id=:id', array(':id' => $this->id))
				->all();
		$gDateString = isset($rows[0]['date']) ? $rows[0]['date'] : '';
		if($gDateString=='0000-00-00')
			return '0000-00-00';
		$this->$dateField = $gDateString;
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d', strtotime($gDateString));
		$jDateString=str_replace("-","/",$jDateString);
		return $jDateString;
	}

	/* -----Authentication validate--------- */

	public function getActive() {
		if(isset($this->active))
			return $this->active;
		return FALSE;
	}

	public function getValid() {
		if (isset($this->valid))
			return $this->valid;
		return FALSE;
	}

	public function isUserBlock() {
		return($this->lock) ? TRUE : FALSE;
	}

	public function IsPassedLockTime() {
		/* if 15 min after last login attemp return true */
		$blockTime = Yum::module('YumUsers')->BlockUserTime * 60;

		if (strtotime($this->last_login_attemp_time) < (time() - $blockTime)) {
			$this->unBlockUser();
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function unBlockUser() {

		$this->lock = 0;
		return $this->clearBrutForceHistory();
		// Yum::log(" user: " . $user->username . "is unbloucket.", 'info', 'application.modules.users.controllers.AuthController');
	}

	public function clearBrutForceHistory() {

		$this->login_attemp_count = 1;
		if ($this->update() != false) {
			$session = Yii::$app->session;
			if ($session->has('show_login_captcha'))
				$session->remove('show_login_captcha');
			return true;
		} else {
			return FALSE;
		}
		// Yum::log(" user: " . $user->username . "is unbloucket.", 'info', 'application.modules.users.controllers.AuthController');
	}

	public function saveBadLoginAttempt() {

		$this->login_attemp_count = (($this->login_attemp_count) + 1);
		$this->last_login_attemp_time = date('Y-m-d H:i:s');
		if ($this->update() !== false) {
			return true;
		} else {
			return FALSE;
		}
	}

	public function isBroutForcedUsername() {
		//if in 5 min 5 attemp
		$maximumCount = Yum::module('YumUsers')->captchaAfterUnsuccessfulLogins;
		return($this->login_attemp_count > $maximumCount && strtotime($this->last_login_attemp_time) > (time() - 300)) ? TRUE : FALSE;
	}

	public function isRepeatedBroutForcedUsername() {

		$maximumCountBlock = (Yum::module('YumUsers')->captchaAfterUnsuccessfulLogins * 2);
		return($this->login_attemp_count > $maximumCountBlock && strtotime($this->last_login_attemp_time) > (time() - 300)) ? TRUE : FALSE;
	}

	public function blockUserAccount() {

		$this->lock = 1;
		return ($this->update() !== false) ? true : FALSE;
	}

		public function setLastLoginTime() {

		$this->last_login_time = date('Y-m-d H:i:s');
		return ($this->update() !== false) ? true : FALSE;
	}


	public function getUserRolesText($userId=NULL)
	{
		if($userId==NULL)
			$userId = $this->id;

		$rolesStr = [];
		//this  code return arrays of object roles
		$roleArray = Yii::$app->authManager->getRolesByUser($userId);
		if(count($roleArray)>0)
		{
			foreach ($roleArray as $index=>$objRole)
			{
				if($objRole->type==1)//type 1 is role ---type 2 is task --type 3 is operation
				{
					$rolesStr[] = $objRole->description;
				}
			}
			return implode(' - ', $rolesStr);
		}
		else
		{
			return Yii::t('app', 'No Role');
		}
	}

	public function getIconThumbnail($fileId=null,$class='')
	{

		if(isset($fileId))
			$this->fileId=$fileId;

		$avatarPath = Yii::$app->request->BaseUrl . '/images/avatar-default.jpeg';

		/* find file of categories in db */
		if (($filemodel = File::findOne($this->fileId)) !== null)
		{
			$folderModel = new Folders;
			$destination = $folderModel->ProvideRealFolderPath(Folders::USERS_PIC_FOLDER_ID) . '/' . $filemodel->name . '.' . $filemodel->ext;
			$url = Url::to([ '/YiiFileManager/file/view-file', 'name' => $filemodel->hashed_name]);

			if($class=='')
				return Html::img($url, ['alt' => $this->FullName, 'class' => 'user-pic rounded', 'width' => '45px', 'height' => '45px']);
			elseif($class=='update')
			   return Html::img($url, ['alt' => $this->FullName,  'width' => '150px', 'height' => '130px']);
			elseif($class=='UpdatePreview')
				return Html::img($url, ['alt' => $this->FullName, 'class' => 'user-pic rounded pull-right bg-font-grey-silver', 'width' => '100px', 'height' => '100px']);
			else
				return Html::img($url, ['alt' => $this->FullName, 'class' => 'user-pic rounded ' . $class]);
		}
		else
		{
			if($class=='')
				return Html::img($avatarPath, ['alt' =>  $this->FullName, 'class' => 'user-pic rounded','width' => '45px', 'height' => '45px']);
			else
				return Html::img($avatarPath, ['alt' =>  $this->FullName, 'class' => 'user-pic rounded ' . $class,'width' => '45px', 'height' => '45px']);
		}
	}

	public function getAvatar()
	{
		if (($filemodel = File::findOne($this->fileId)) !== null)
		{
			$folderModel = new Folders;
			$destination = $folderModel->ProvideRealFolderPath(Folders::USERS_PIC_FOLDER_ID) . '/' . $filemodel->name . '.' . $filemodel->ext;
			$url = Url::to([ '/YiiFileManager/file/view-file', 'name' => $filemodel->hashed_name]);

			return $url;
		}

		return false;
	}



	public function beforeSave($insert) {


		/*Creatuser */
		if (parent::beforeSave($insert)) {
			$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
			if ($this->isNewRecord) {
				$this->creatorUserId = $userId;
				$this->created_at = date('Y-m-d H:i:s');

				$jdf = new jdf();
			$delimeter='-';
				if (!empty($this->birthDate)) {

					if( preg_match ('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $this->birthDate )) {
						$this->birthDate = str_replace('-', '/', $this->birthDate);
					}


					if(!is_null($this->birthDate) && !empty($this->birthDate) && trim($this->birthDate)!= '')
					{
						if(count(explode('/', $this->birthDate))== 3)
							$this->birthDate = Yii::$app->jdate->toGregorians($this->birthDate);
						else
							$this->birthDate = null;
					}

				}

		/*Edituser */
		} else {

			$this->updated_at = date('Y-m-d H:i:s');
			/*Modify birthDate if set as jalaly format in edit form*/
			$jdf = new jdf();
			$delimeter='-';

		    if (!empty($this->birthDate)) {
			         /*just persian date shold be convert to gorgian date .these i check persian format*/
			        if($this->birthDate=='0000-00-00'||$this->birthDate=='0000/00/00')
					{
						$this->birthDate='0000-00-00 00:00:00';
					}
					else if (preg_match ('/^[0-9]{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $this->birthDate )) {
							$delimeter='/';
						    $this->birthDate = $jdf->jalaliStr_to_gregorianStr($this->birthDate,$delimeter);
				    	}
						else if( preg_match ('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $this->birthDate))
						{
							$delimeter='-';
							$this->birthDate = $jdf->jalaliStr_to_gregorianStr($this->birthDate,$delimeter);

						}
		    }

		}
			/**replace arabic character by persian**/
			$arabicChar = array('ي','ك');
			$persianChar = array('ی','ک');
			$this->name = str_replace($arabicChar, $persianChar, $this->name);
			$this->last_name = str_replace($arabicChar, $persianChar, $this->last_name);

			/**replace Persian number character by english number**/
			$this->nationalId=$this->convertPersianNumToEnNum($this->nationalId);
			$this->mobile=$this->convertPersianNumToEnNum($this->mobile);
			$this->tel=$this->convertPersianNumToEnNum($this->tel);
			$this->idNo=$this->convertPersianNumToEnNum($this->idNo);

			return true;
		} else {
			return false;
		}
	}



	public function dateDisplayFormat($date=null)
	{
		$date=$this->birthDate;

		if ( preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date )) {

			return $date ;
		}
		else if(preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',$date))
		{

			return $date;
		}
		else if($date=='0000-00-00 00:00:00'|| $date=='0000-00-00') {
			return '0000-00-00';
		}


		return $date=$this->getJalalyDate( 'birthDate');
	}
	
	
	public function afterSave($insert, $changedAttributes)
	{
		if (!isset($this->sportNo)) {

			//set sportNo
			$this->sportNo = $this->id+100000000;
			return ($this->update() !== false) ? true : FALSE;
		}
		parent::afterSave($insert, $changedAttributes);
	}




	  /************ ***Add new attribute Principle**********/

	/*
	 * 1- define new Private attribute==>warning private attribute
	 * 2-define in top of class
	 */
	public function attributes() {
		$attributes = parent::attributes();
		$attributes = array_merge($attributes, [
			'active',
			'sportNo', 'eName', 'eFamily', 'fatherName', 'idNo', 'birthDate','birthCityId','fileId'


		]);
		return $attributes;
	}





	public function addRole($role=self::Role_reader)
	{
	   $auth = Yii::$app->authManager;
	  // $deafualtRole=Yum::module('YumUsers')->defaultRole;
	   $deafualtRole=self::Role_reader;
	   $Role = $auth->getRole($deafualtRole);
	   $auth->assign($Role, $this->id);
	}


	public function picFileExist()
	{
		if(isset($this->fileId)&&$this->fileId!=0)
			return true;

		return false;
	}


	public function uploadPicFile($fileInstance=null,$skipOnEmpty=false)
	{
				  $fileModel=new File;
				  $fileModel->scenario=  File::MODEL_FILE_UPLOAD_SCENARIO;
				  $fileModel->modelFileTyps=$this->fileTyps;
				  $fileModel->modelMimeFileTyps=$this->mimeFileTyps;
				  $fileModel->modelFileskipOnEmpty=$skipOnEmpty;
				  $fileModel->modelFileSize=$this->fileSize;

				   /** Add icon file**/

				   $fileModel->folder_id = Folders::USERS_PIC_FOLDER_ID;
				   $fileModel->model_id =$this->id;
				   $fileModel->model_type ='user';

				   $fileModel->temporaryFileAttr =  \yii\web\UploadedFile::getInstance($this, 'fileAttr');

		//if ($fileModel->temporaryFileAttr!==null) {

			if ($fileModel->validate()&&$fileModel->temporaryFileAttr!==null && $fileModel->uploadModelFile('user_' . $this->id)) {


				/* remove old file in edit action call*/
				//die(var_dump($this->fileId));
				if (isset($this->fileId)) {
					$oldFileId = $this->fileId;
					try{
						if (($oldFileModel = FileAction::findOne($oldFileId)) !== null) {
							$oldFileModel->removeFile($oldFileId);
						}
					}catch(Exception $e)
					{
						throw new NotFoundHttpException( "Problem in find old File" );
					}

				}

				/*save file id in categorie table */
				$this->fileId = (int)$fileModel->id;
				return $this->update(0);

			} else {
				if (count($fileModel->errors) > 0) {
					$msg = '';
					foreach ($fileModel->errors as $attribute => $errors) {
						foreach ($errors as $error) {
							$msg .= $error;
						}
					}
					$this->addError('fileAttr', $msg);

				}

				return false;
			}
		//}
				   return false;
			 }



		 public function getUserDetailInfo($eventId=null)
		 {
				if(!empty($this->usersDetails))
				{
						return $this->_userDetailObj=$this->usersDetails[0];
				}
		 }



	public function getUserDetailBaseOnEvent($eventId)
	{
			return $userDetailModel=UsersDetails::find()->where('eventId = :eventId AND usersId=:usersId And valid=1', [':eventId' => (int)$eventId,':usersId' => (int)$this->id])->one();
	}

	/**
	 * Get event id from user session
	 */
	public function getEventId()
	{
		if(Yii::$app->session->has('eventId')){
			return Yii::$app->session->get('eventId');
		}
		else{
			/**
			 * Get Params Data
			 */
			$_params = $this->Param;

			/**
			 * Check has eventId to Params
			 */
			if(!is_null($_params) && array_key_exists('eventId', $_params)){
				$this->EventId = $_params['eventId'];

				return $_params['eventId'];
			}
		}

		return NULL;
	}

	/**
	 * Get is set RFID for this user
	 */
	public function getIsRFID()
	{
		$out = '';

		if(isset($this->RFID) && ($this->RFID != NULL || $this->RFID != ''))
			$out = '<spna class="font-green"><i class="fa fa-check-circle"></i></span>';
		else
			$out = '<spna class="font-red"><i class="fa fa-times-circle"></i></span>';

		return $out;
	}

	/**
	 * check the user is admin
	 * @return  boolean when type of the user is admin true and otherwise false
	 */
	public function getIsAdmin()
	{
		$auth = Yii::$app->authManager;
		return $auth->AdminAccess($this->id, 'admin');
	}

	/**
	 * Set event id to user session
	 */
	public function setEventId($eventId)
	{
		/**
		 * Get Params Data
		 */
		$_params = $this->Param;

		/**
		 * Set eventId to Params
		 */
		$_params['eventId'] = $eventId;

		/**
		 * Save params
		 */
		$this->params = Json::encode($_params);
		$this->save(false);

		return Yii::$app->session->set('eventId', $eventId);
	}

	/**
	 * @return array string User params
	 */
	public function getParam()
	{
		return ($this->params != NULL ? Json::decode($this->params) : []);
	}

	/**
	 * Get Creator Full Name
	 */
	public function getCreatorName()
	{
		return $this->creator != NULL ? $this->creator->FullName : '***';
	}


	/**
	 * Get User detail id on event
	 */
	public function getUserDetailIdBaseOnEvent($eventId)
	{

		$userDetailModel = UsersDetails::find()->where('eventId = :eventId AND usersId=:usersId And valid=1', [':eventId' => (int)$eventId,':usersId' => (int)$this->id])->one();

		if($userDetailModel!=null)
		{
			return $userDetailModel->id;

		}
//		if($userDetailModel!=null)
//		{
//			return $userDetailModel->id;
//			Yii::$app->session->set('event.userDetailsId', $userDetailModel->id);
//			return Yii::$app->session->get('event.userDetailsId');
//		}
//		if(Yii::$app->session->get('event.userDetailsId')){
//			return Yii::$app->session->get('event.userDetailsId');
//		}
//		else
//		{
//			$userDetailModel = UsersDetails::find()->where('eventId = :eventId AND usersId=:usersId And valid=1', [':eventId' => (int)$eventId,':usersId' => (int)$this->id])->one();
//		 	if($userDetailModel!=null)
//		 	{
//		 		 Yii::$app->session->set('event.userDetailsId', $userDetailModel->id);
//				 return Yii::$app->session->get('event.userDetailsId');
//		 	}
//		 }
		 	return null;
	}



	public function getUserDetailIdBaseOnEventIdUserId($eventId,$userId=null)
	{
		if($userId==null){
			$userId=$this->id;
		}

			$userDetailModel = UsersDetails::find()->where('eventId = :eventId AND usersId=:usersId And valid=1', [':eventId' => (int)$eventId,':usersId' => (int)$userId])->one();
		 	if($userDetailModel!=null)
		 	{
		 		return $userDetailModel->id;
		 	}
		 	return false;
	}




	/**
	 * Set user dateil id
	 */
	public function setUserDetailIdBaseOnEvent($userDetailId)
	{
		return Yii::$app->session->set('event.userDetailsId', $userDetailId);
	}

	/*
	  * delete User
	  * 1- delete user(truly or logical)
	  * 2- delete asigned roles
	  * 3- delete related User Details
	  * 4- delete User pic file
	  */

		public function delete($id = NULL) {

			parent::beforeDelete();

			if ($id != NULL)
				$this->id = $id;

			$picFileId=$this->fileId;

			if ($this->trulyDelete) {

			   $result= (parent::delete()) ? true : FALSE;

			} else {

			   parent::beforeDelete();

			  try {
				  $result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();

				 } catch (\Exception $ex) {
				  return false;
				}

			}

			   if($result)
			   {

					/*------Delete Assigned Role-----------------*/
					$authAssignObj=new AuthAssignment();
					$result1=$authAssignObj->deleteByUserId( $this->id);


					/*------Delete related User Details Role-----*/
					$userDetailsObj=new UsersDetails();
					$result2=$userDetailsObj->deletebyUserId( $this->id);
					/*------Delete User pic file-----------------*/
					if(isset($picFileId))
					{
						if (($FileModel = FileAction::findOne($picFileId)) !== null)
						{
							 $FileModel->removeFile($picFileId);
						}
					}
					/*------Delete related User Details Role-----*/
					//$userDetailsObj=new EventMembers();
					//$result2=$userDetailsObj->deletebyUserId( $this->id);
				}
			parent::afterDelete ();
			return $result;
		}


	public  function getUserStuNoText($userId)
   {
	  $userDetailsObj = new UsersDetails();
	  $userDetailsResult=$userDetailsObj->find()->where("usersId=".(int)$userId)->one();
	  $stuNo=($userDetailsResult)?($userDetailsResult->stuNo) : ('-----');
	   return $stuNo;
   }



   public  function getTopUserDetailObj($userId)
   {
	  $userDetailsObj = new UsersDetails();
	  $userDetailsResult=$userDetailsObj->find()->where("valid =1 AND usersId=".(int)$userId)->orderBy('id DEsc')->one();
	  if(is_object($userDetailsResult))
	   return $userDetailsResult;

	  return $userDetailsObj;
   }



	public function setActiveVal($value)
	{
		$this->active =(int) $value;
		return ($this->update() !== false) ? true : FALSE;
	}


	public function isUserLoginEver()
	{
		if($this->last_login_time!='0000-00-00 00:00:00')
		{
		   return true;
		}else
		{
		   return false;
		}
	}

	public  function isUserMemberOfEvent($eventId,$usersId)
	{
		$Obj = new UsersDetails();
		$MemberObj=$Obj->findOne(['eventId' => (int)$eventId, 'usersId' => (int)$usersId]);

		if($MemberObj!==null)
		{
			return true;
		}
		   return false;
	}



  public function insertMemberEventInfo($userDetailId,$eventId,$carvanId=0,$teamId,$universityId)
	{
		$eventMemberObj=new EventMembers;
		$eventMemberObj->eventId=(int)$eventId;
		$eventMemberObj->teamsId=(int)$teamId;
		$eventMemberObj->usersDetailsId=(int)$userDetailId;
		$eventMemberObj->universityId=(int)$universityId;
		$eventMemberObj->caravanId=(int)$carvanId;
		$eventMemberObj->rolename=self::Role_reader;
		$eventMemberObj->userId=$this->id;
		$eventMemberObj->user_type_id=UsersType::getUserTypeId('Reader');

		return $eventMemberObj->save(0);
	}


	public function getUsersDetailsCount()
	{
	return usersDetails::find()->where(['usersId' => (int)$this->id])->count();
	}

	/*
	 * date functions
	 */

	public function getJBirthdate() {
		return ($this->birthDate != '0000-00-00 00:00:00') ? Yii::$app->jdate->date ( 'Y/m/d', strtotime ( $this->birthDate ) ) : Yii::t ( 'zii', 'Not set' );
	}


	public function getUserType($userId=null)
	{
		if($userId==null)
			$userId=$this->id;

		$eventMemObj=$this->eventMembersByEvent;

		if($eventMemObj!=null)
			return $eventMemObj->user_type_id;

		return 0;
	}

	public function setUserType($type)
	{
		$userId = $this->id;

		$eventMemObj=$this->eventMembersByEvent;

		if($eventMemObj != null)
			$eventMemObj->user_type_id = $type;
	}

	public function isDuplicateEventUser($userId,$userDetailId,$eventId,$typeId,$universityId=0,$teamId,$caravanId=null)
	{
		$eventMemberModel=EventMembers::find()->event(Yii::$app->user->identity->eventId)
			                                   ->type($typeId)
			                                   ->userdetails($userDetailId)
			                                   ->Users($this->id)
			                                   ->team($teamId)
			                                   //->University($universityId)
			                                   ->caravan($caravanId)
			                                   ->one();

		if($eventMemberModel!==null)
		{
			return true;
		}
		return false;
	}

	public function isDuplicateUniversityRespUser($userId,$userDetailId,$eventId,$typeId,$universityId=0,$teamId,$caravanId=null)
	{
		$eventMemberModel=EventMembers::find()->event(Yii::$app->user->identity->eventId)
			->type($typeId)
			->userdetails($userDetailId)
			->Users($this->id)
			->team($teamId)
			->University($universityId)
			->caravan($caravanId)
			->one();

		if($eventMemberModel!==null)
		{
			return true;
		}
		return false;
	}

	public function getUserTypeTittle($caravanId,$teamId=0)
	{
		if($typeId=$this->getUserTypeId($caravanId,$teamId))
		{
			return $typeTittle=UsersType::getUserTypeTitle($typeId);

		}
		return '---';

	}


	public function getUserTypeId($caravanId,$teamId=0)
	{
		$eventMemObj=EventMembers::find()
			->event(Yii::$app->user->identity->eventId)
			->Users($this->id)
			->team($teamId)
			->caravan($caravanId)
			->one();

		if(is_object($eventMemObj))
		{
			return $eventMemObj->user_type_id;
		}
		return 0;

	}



}

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends \yii\db\ActiveQuery
{
	public function init()
	{
		parent::init();
		return $this->andWhere([User::tableName() . '.`valid`' => 1]);
	}
}