<?php

namespace backend\modules\YumUsers\models;

use Yii;

/**
 * This is the model class for table "educational_groups".
 *
 * @property integer $EduGrpCode
 * @property string $EEduName
 * @property string $PEduName
 */
class EducationalGroups extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'educational_groups';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['EduGrpCode'], 'required'],
			[['EduGrpCode'], 'integer'],
			[['EEduName', 'PEduName'], 'string', 'max' => 55]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'EduGrpCode' => Yii::t('app', 'Edu Grp Code'),
			'EEduName' => Yii::t('app', 'Eedu Name'),
			'PEduName' => Yii::t('app', 'Pedu Name'),
		];
	}
}
