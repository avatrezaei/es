<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\captcha\Captcha;
use Yii\helpers\Url;

/* * *************
 * Yii validator
 */
use backend\models\jdf;
use yii\validators\Validator;
use yii\validators\StringValidator;
use yii\validators\NumberValidator;

use backend\models\EventMembers;

use backend\modules\YumUsers\models\YumPasswordValidator;
use backend\modules\YumUsers\models\ProActiveRecord;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property integer $gender
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $valid
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Membership[] $memberships
 */
class ExecutiveCommitteeUser extends User {


   /*
	* Attribute in Modal Search shoud diferent name
	* to prevent Conflict in Main grid in indexaction
	*/

	public $searchInputName;
	public $searchInputLastName;


	public $fileAttr;




   public function scenarios()
	{

		$scenarios = parent::scenarios();
		$scenarios['createExecutiveCommitteeUser'] = ['userType','name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted
		$scenarios['UpdateExecutiveCommitteeUser'] = ['userType','name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted
		$scenarios['creaExecutiveByExistUser'] = ['userType','name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted

		return $scenarios;

	}



	public function rules() {

		//get setting of module
		$usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
		$passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
		$nameRequirements = Yum::module('YumUsers')->nameRequirements;

		/**Requirments***/
		$rules[] = [['userType','name','last_name','nationalId','mobile'],'required','on'=>['creaExecutiveByExistUser','createExecutiveCommitteeUser']];
		$rules[] = [['name','last_name','nationalId', 'birthDate','mobile'],'required','on'=>['UpdateExecutiveCommitteeUser']];


		/** password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator**/
		$passwordrule = array_merge(['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'], $passwordRequirements);
		$rules[] = $passwordrule;
		$rules[] =  ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];

		/** username validation from setting*/

		if ($usernameRequirements) {
			$rules[] = ['username', 'string',  'max' => $usernameRequirements['maxLen'],'min' => $usernameRequirements['minLen'],];
			$rules[] = ['username', 'match','pattern' => $usernameRequirements['match'],'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
		}
		$rules[] = ['username', 'checkUnique'];




		/** name validation from setting**/
		if ($nameRequirements) {
			$rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];

			$rules[] = [['name', 'last_name'], 'match',  'pattern' => $nameRequirements['match'],'message' => Yum::t($nameRequirements['dontMatchMessage'])];
		}


		/** Emali validation**/
		$rules[] = ['email', 'email'];
		$rules[] = ['email', 'checkUnique'];
		$rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];


		/** date validation**/
		//$rules[] =  [['birthDate'], 'checkDateFormat'];


/**
		 * natinalId validation
		 */
		//$rules[] =  [['nationalId'], 'validateNationalId'];
		$rules[] = ['nationalId', 'checkUnique'];
		$rules[] =array('nationalId', 'string', 'min'=>10, 'max'=>10);

		/**
		 * Numeric validation Utf8
		 */
		//$rules[] =[['nationalId','tel','mobile','idNo',], 'integer'];
		$rules[] = array(['idNo','tel','mobile','nationalId'], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');


		/** Number validation**/
		$rules[] = array(['birthCityId','sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);

		/** String validation**/
		$rules[] = array([ 'univ', 'homepage', 'tel', 'mobile', ], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

		/** Range validation**/
		$rules[] = array([ 'lock', 'valid'], 'in', 'range' => array(0, 1));
		$rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE,self::MALE));




		/**Exist validation**/
		//$rules[] =  ['id', 'exist', 'targetClass' => '\backend\models\City'] ;



		/**Safe validation**/

		$rules[] = array(['verifyCode'], 'safe',);
		/**Safe validation for save in db**/
		$rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
		$rules[] = [['fileAttr','sportNo', 'eName', 'eFamily', 'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'], 'safe',];


		/** file validation**/
		$fileModel=new File;
		$fileModel=new File;

	   $rules[] =  [['fileAttr'], 'file',
			'skipOnEmpty' => true,
			'extensions' =>'jpg,jpeg,png',
		   'checkExtensionByMimeType' => true,
			'maxSize' => File::MAX_FILE_SIZE,
			'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
			'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
			'minSize' => File::MIN_FILE_SIZE,
		   'mimeTypes' => ['image/jpeg','image/jpeg','image/png'],

			];
		$rules[] = array(['fileAttr'], 'required');


		/** Captcha validation**/
		$rules[] = array('verifyCode', 'captcha',
			'isEmpty' => !Captcha::checkRequirements(),
			'on' => 'register,captch,forgetpass',
			'message' => ' تصویر امنیتی  صحیح را وارد کنید.'
		);

		/** other vlidations*/
		//  $rules[] = array('role', 'RolesTypeValidation',);
		return $rules;
	}


	/* Autentication user model */

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

		public function beforeSave($insert) {
		/*Creatuser */
		if (parent::beforeSave($insert)) {

			return true;
		} else {
			return false;
		}
	}

	public function createExecutiveCommitteeUser() {

		$saveAttributes = $this->createSaveAttributes;

		if ($this->validate()) {


			/*-Set nationalId as Default username And mobile as password-*/
			if($this->scenario!='creaExecutiveByExistUser')
			{
				$this->username=isset($this->nationalId) ? $this->nationalId:'';
				$this->passwordInput=isset($this->mobile)? $this->mobile:'';

				$this->setPassword($this->passwordInput);
				$this->generateAuthKey();

			}

			//to dont reset pass & important user
			if($this->scenario=='creaExecutiveByExistUser')
			{
				$saveAttributes=$this->createExistSaveAttributes;
			}


			if ($result=$this->save(0,$saveAttributes)) {
				try {
					if($this->scenario=='creaExecutiveByExistUser')
					{
						if(!Yii::$app->getAuthManager()->checkAccess($this->id,self::Role_ExecutiveCommittee)){
							$this->addRole(self::Role_ExecutiveCommittee);

						}

					}else{

						$this->addRole(self::Role_ExecutiveCommittee);

					}
				}catch (Exception $e) {
					throw new \yii\web\HttpException(403, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
				}
				return TRUE;
			}
		}
		return null;
	}

	public function getUserTypeTxt()
	{

		if(!empty($this->eventMembers))
		{
			foreach ($this->eventMembers as $ev) {
				if($ev->eventId==$eventId=Yii::$app->user->identity->eventId&& in_array($ev->user_type_id,[51,52,53]))
					return $ev->TypeName;
			}


		}
	}
	public static function getUserExecutiveCommitteeType()
	{
		$couchTypeModels=\backend\modules\YumUsers\models\UsersType::getExecutiveCommitteeType();

		return $couchType=ArrayHelper::map($couchTypeModels, 'id', 'title');
	}


	public function addRole($role=self::Role_ExecutiveCommittee)
	{
		 $auth = Yii::$app->authManager;
		 $Role = $auth->getRole($role);
		 $auth->assign($Role,$this->id);
	}
	public function getExecutiveCommitteRole()
	{
		$_event = EventMembers::find()->user($this->id)
			->event(Yii::$app->user->identity->eventId)
			->andWhere(['rolename'=>self::Role_ExecutiveCommittee])
			->one();
		if(is_object($_event))
			return $this->userType=$_event->user_type_id;
	}
	public function getAuthassignment()
	{
		return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
	}

	/**
	 * @return AcademicBackground[]
	 */

	public function insertMemberEventInfo($userDetailId,$eventId,$carvanId=0,$teamId,$universityId)
	{
		$eventMemberObj=new EventMembers;
		$eventMemberObj->eventId=$eventId;
		$eventMemberObj->teamsId=$teamId;
		$eventMemberObj->usersDetailsId=$userDetailId;
		$eventMemberObj->universityId=$universityId;
		$eventMemberObj->caravanId=$carvanId;
		$eventMemberObj->rolename=self::Role_ExecutiveCommittee;
		$eventMemberObj->user_type_id=$this->userType;
		$eventMemberObj->userId=$this->id;

		return $eventMemberObj->save(0);
	}





	public function deleteExecutiveCommitteeByAdmin($id = NULL)
	{

		$this->userEventMemArr=EventMembers::find()->Users($this->id)->all();
		$count=count($this->userEventMemArr);
		if($count==1){

			$result=$this->deleteUserByOneEventMem();
			if($result)
				return ['result'=>true,'msg'=>Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')])];
			else
				return ['result'=>false,'msg'=>Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')])];


		}elseif($count>1)
		{


			$result=$this->deleteUserBySeveralEventMem();
			if($result)
				return ['result'=>true,'msg'=>Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')])];
			else
				return ['result'=>false,'msg'=>Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')])];


		}

	}

	public function deleteUserByOneEventMem()
	{
		$picFileId=$this->fileId;

		$userId = $this->id ;
		$eventMemObj=$this->userEventMemArr[0];
		$eventId = Yii::$app->user->identity->eventId;
		$userDetailId=$eventMemObj->usersDetailsId;


		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();

		try {

			/** delete EventmemberRow in EventMember table**/
			$result3=EventMembers::deleteAll('eventId = :eventId AND rolename=:role AND usersDetailsId=:userDetail', [':eventId' => (int)$eventId,':role' => self::Role_ExecutiveCommittee,':userDetail'=>$userDetailId]);

			/** delete responsed role in authitemtable**/
			$authAssignObj=new AuthAssignment();
			$resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_ExecutiveCommittee.'" And event_id='.(int)$eventId).'';


			/** Delete User Record if one userdetail*/
			parent::beforeDelete();
			try {
				$result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();
			}
			catch (\Exception $ex)
			{
				return false;
			}

			/*Start Delete UserDetails Record*/
			$userDetailsObj=new UsersDetails();
			$userDetailsObj->id=$userDetailId;

			parent::beforeDelete();

			try {
				$resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

			}
			catch (\Exception $ex) {
				return false;
			}

			/*Delete User pic file*/
			if(isset($picFileId)&&$picFileId!=0)
			{

				if (($FileModel = FileAction::findOne($picFileId)) !== null) {
					$FileModel->removeFile($picFileId);
				}
			}

			$transaction->commit();

			return true;

		} catch (\Exception $e) {
			$transaction->rollBack();
			return false;
			throw $e;
		}
	}




	public function deleteUserBySeveralEventMem()
	{
		$countExecutivePosition=0;
		$countEventType=0;


		$eventId = Yii::$app->user->identity->eventId;

		/*get target event mem for this university universityResp role*/
		foreach ($this->userEventMemArr as $i=>$eventObj)
		{
			if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_ExecutiveCommittee)
				$eventMemObj=$eventObj;

			/*count  position of Role_ExecutiveCommittee for user  */
			if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_ExecutiveCommittee)
				$countExecutivePosition++;

			/*count of user $countEventType in this event*/
			if($eventObj->eventId==$eventId)
				$countEventType++;
		}



		$userDetailId=$eventMemObj->usersDetailsId;
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();

		try {

			/** delete EventmemberRow in EventMember table**/
			$result3=$eventMemObj->delete();


			/** delete responsed role in authitemtable if just have one university resp position**/
			if($countExecutivePosition==1)
			{
				$authAssignObj=new AuthAssignment();
				$resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_ExecutiveCommittee.'" And event_id='.(int)$eventId).'';
			}


			if($countEventType==1)
			{

				/*Start Delete UserDetails Record*/
				$userDetailsObj=new UsersDetails();
				$userDetailsObj->id=$userDetailId;

				parent::beforeDelete();

				try {
					$resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

				}
				catch (\Exception $ex) {
					return false;
				}
			}


			$transaction->commit();

			return true;

		} catch (\Exception $e) {
			$transaction->rollBack();
			return false;
			throw $e;
		}

	}


	public function moveToEvent($eventSource,$eventDestination)
	{
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();

		try {

			/*move user details*/
			$Obj = new UsersDetails();
			$MemberObj=$Obj->findOne(['eventId' => (int)$eventDestination, 'usersId' => (int)$this->id]);
			if($MemberObj==null)
			{
				$userDetailId=$this->moveUserDetailToEvent($eventSource,$eventDestination);
			}else
			{
				$userDetailId=$MemberObj->usersDetailsId;

			}

			$moveCount=0;
			/*move user eventMembers*/
			$this->userEventMemArr=EventMembers::find()->Users($this->id)->all();
			foreach ($this->userEventMemArr as $evMem)
			{
				$eventMemExit=EventMembers::find()->Users($evMem->userId)->event($eventDestination)->type($evMem->user_type_id)->role($evMem->rolename)->event($evMem->eventId)->one();
				if(in_array($evMem->user_type_id,[51,52,53])&&$eventMemExit==null)
				{

					$newEvMem=new EventMembers();
					$newEvMem->userId=$evMem->userId;
					$newEvMem->user_type_id=$evMem->user_type_id;
					$newEvMem->rolename=$evMem->rolename;
					$newEvMem->printStatus=0;
					$newEvMem->eventId=$eventDestination;
					$newEvMem->usersDetailsId=$userDetailId;
					$result=$newEvMem->save(0);
					if($result)
						$moveCount++;

					if(AuthAssignment::findOne(['user_id'=>$this->id,'item_name'=>$evMem->rolename,'event_id'=>$eventDestination])==null)
					{
						$auth = Yii::$app->authManager;
						$Role = $auth->getRole($evMem->rolename);
						$auth->assignByEventId($Role,$this->id,$eventDestination);
					}

				}
			}
			$transaction->commit();
			if($moveCount>0)
			    return true;
			else
				return false;

		} catch (\Exception $e) {
			$transaction->rollBack();
			return false;
			//throw $e;
		}
	}


	public function moveUserDetailToEvent($eventSource,$eventDestination)
	{
		$srcUserDetail=UsersDetails::findOne(['eventId' => (int)$eventSource, 'usersId' => (int)$this->id]);
		$newUserDetailObj = $srcUserDetail;

		$newUserDetailObj->id=null;
		$newUserDetailObj->eventId=$eventDestination;
		$newUserDetailObj->isNewRecord=1;

		$newUserDetailObj->save(0);
		return $newUserDetailObj->id;

	}


}
