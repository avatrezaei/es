<?php
namespace backend\modules\YumUsers\models;


use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
	public $email;

	/*
	 * @inheritdoc
	 */


	public function rules()
	{
		return [
			['email', 'filter', 'filter' => 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'exist',
				'targetClass' => 'backend\modules\YumUsers\models\User',
				'filter' => ['status' => User::STATUS_ACTIVE],
				'message' => Yii::t('app','There is no user with such email.')
			],
		];
	}



	public function attributeLabels()
	{
		return [

			'email' => Yii::t('app', 'Email'),
		];
	}

	/**
	 * Sends an email with a link, for resetting the password.
	 *
	 * @return boolean whether the email was send
	 */
	public function sendEmail()
	{
		/* @var $user User */
		$user = User::findOne([
			'valid' => 1,
			'status' => User::STATUS_ACTIVE,
			'email' => $this->email,
		]);

		if ($user)
		{
			if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
				$user->generatePasswordResetToken();
			}

			if ($user->save(false))
			{
				return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html'], ['user' => $user, 'email' => $this->email])
					->setFrom([\Yii::$app->params['supportEmail'] => Yii::t('app', 'Ferdowsi University of Mashhad')])
					->setTo($this->email)
					->setSubject(Yii::t('app', 'Please verify your email address.'))
					->send();
			}
		}

		return false;
	}
}
