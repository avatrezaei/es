<?php

namespace backend\modules\YumUsers\models;

use backend\models\TeamsSubfieldsMember;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\captcha\Captcha;
use backend\models\Teams;
use backend\models\EventMembers;
use backend\models\AuthorizedFields;
use backend\modules\YumUsers\models\UsersType;
use backend\models\TeamMembers;


/**
 * Yii validator
 */
use backend\models\jdf;
use yii\validators\Validator;
use yii\validators\StringValidator;
use yii\validators\NumberValidator;
use backend\modules\YumUsers\models\YumPasswordValidator;
use backend\modules\YumUsers\models\ProActiveRecord;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;

use Yii\helpers\Url;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property integer $gender
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $valid
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Membership[] $memberships
 */
class AthleteUser extends User {

   /*
	* Attribute in Modal Search shoud diferent name
	* to prevent Conflict in Main grid in indexaction
	*/
	public $searchInputName;
	public $searchInputLastName;
	public $searchInputStuNo;

   /**
	* SPORT EVENT FIELDS
	*/
	public $fileAttr;


	public $athleteSubFieldId;

	public $scenarioHaveSubFieldValidation=0;

	public $_teamObj;


	public function scenarios() {

		$scenarios = parent::scenarios();
		$scenarios['createAtheleteUserByAdmin'] = ['athleteSubFieldId','teamId','name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted
		$scenarios['createAtheleteUser'] = ['athleteSubFieldId','name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted
		$scenarios['updateAthleteUser'] = ['athleteSubFieldId','name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted
		$scenarios['updateExistAthleteUser'] = ['athleteSubFieldId','name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted

		return $scenarios;
	}

	/**
	 * Set Default scope for filter on every event
	 */
	public static function find()
	{
		return parent::find();
	}

	public function rules() {

		//get setting of module
		$usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
		$passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
		$nameRequirements = Yum::module('YumUsers')->nameRequirements;

		/**
		 * Requirments
		 */
		$rules[] = [['name','last_name','mobile',  'nationalId', 'birthCityId','fatherName','gender'],'required','on'=>['createAtheleteUser','updateExistAthleteUser','updateAthleteUser','createAtheleteUserByAdmin']];
		//$rules[] = [['name','last_name','mobile',  'nationalId', 'birthDate','birthCityId','fatherName'],'required','on'=>['createAtheleteUser','updateAthleteUser','createAtheleteUserByAdmin']];
		$rules[] = [['teamId','caravanId','gender'],'required','on'=>['createAtheleteUserByAdmin','updateExistAthleteUser']];
		$rules[] = [['teamId'],'semanticTeamCaravanValidation','on'=>['createAtheleteUserByAdmin','updateExistAthleteUser']];

		/**
		 * password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator
		 */
		$passwordrule = array_merge(['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'], $passwordRequirements);
		$rules[] = $passwordrule;
		$rules[] =  ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];

		/**
		 * username validation from setting
		 */
		if ($usernameRequirements) {
			$rules[] = ['username', 'string',  'max' => $usernameRequirements['maxLen'],'min' => $usernameRequirements['minLen'],];
			$rules[] = ['username', 'match','pattern' => $usernameRequirements['match'],'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
		}
		$rules[] = ['username', 'checkUnique'];

		/**
		 * natinalId validation
		 */
		if(!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id, User::Role_Admin )&&!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id,  User::Role_SeniorUserEvent)) {
			$rules[] = [['nationalId'], 'validateNationalId'];
		}
		$rules[] =array('nationalId', 'string', 'min'=>10, 'max'=>10);
		$rules[] = ['nationalId', 'checkUnique'];


		/**
		 * name validation from setting
		 */
		if ($nameRequirements) {
			$rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];
			$rules[] = [['name', 'last_name'], 'match',  'pattern' => $nameRequirements['match'],'message' => Yum::t($nameRequirements['dontMatchMessage'])];
		}

		/**
		 * Emali validation
		 */
		$rules[] = ['email', 'email'];
		$rules[] = ['email', 'checkUnique'];
		$rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];

		/**
		 * date validation
		*/
		//$rules[] =  [['birthDate'], 'checkDateFormat'];


		/**
		 * Numeric validation Utf8
		 */
		$rules[] = array(['idNo','tel','mobile','nationalId'], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');

		/**
		 * Number validation
		 */
		$rules[] = array(['birthCityId','sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);

		/**
		 * String validation
		 */
		$rules[] = array([ 'univ', 'homepage', 'tel', 'mobile', ], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

		/**
		 * Range validation
		 */
		$rules[] = array([ 'lock', 'valid'], 'in', 'range' => array(0, 1));
		$rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE,self::MALE));


		/**
		 * Exist validation
		 */
		//$rules[] =  ['id', 'exist', 'targetClass' => '\backend\models\City'] ;



		/**
		 * Safe validation
		 */
		$rules[] = array(['verifyCode'], 'safe',);

		/**
		 * Safe validation for save in db
		 */
		$rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
		$rules[] = [['teamId','fileAttr','sportNo', 'eName', 'eFamily', 'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'], 'safe',];


		/**
		 * file validation
		 */
		$fileModel=new File;

		$rules[] = [['fileAttr'], 'file',
			//'skipOnEmpty' => false,
			'extensions' =>$this->fileTyps,
			'checkExtensionByMimeType' => true,
			'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
			'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
			'minSize' => File::MIN_FILE_SIZE,
			'mimeTypes' =>$this->mimeFileTyps,
			//'maxSize' => $this->fileSize,


		];

		$rules[] = [['fileAttr'], 'file',
			'skipOnEmpty' => false,
			'on'=>['createAtheleteUser','createAtheleteUserByAdmin']
		];

		$rules[] = array(['fileAttr'], 'required','on'=>['createAtheleteUser','createAtheleteUserByAdmin']);
		$rules[] = array(['fileAttr'], 'safe',);

		/**
		 * Captcha validation
		 */
		$rules[] = array('verifyCode', 'captcha',
			'isEmpty' => !Captcha::checkRequirements(),
			'on' => 'register,captch,forgetpass',
			'message' => ' تصویر امنیتی  صحیح را وارد کنید.'
		);


		if($this->scenarioHaveSubFieldValidation)
		{
			$rules[] = array(['athleteSubFieldId'], 'required',);
			$rules[] = array(['athleteSubFieldId'], 'semanticCheckSubField',);

		}

		/**
		 * other vlidations
		 */
		//  $rules[] = array('role', 'RolesTypeValidation',);
		return $rules;
	}




	public function semanticCheckSubField($attribute, $params) {

		$subFielsId=(int)$this->$attribute;

		if(is_object($this->_teamObj))
		{
			/*put subield capacity check here in future*/
			$authorizedFields=$this->_teamObj->getAuthorizedSubFields();
			$authorizedSubFields = ArrayHelper::map($authorizedFields, 'id', 'id');
			if(!in_array($subFielsId,$authorizedSubFields))
			{
				$this->addError($attribute, Yii::t('app', 'Invalid SubFields'));
                return false;
			}
		}

		return true;
	}




	public function getUsersDetails() {
		return $this->hasMany(UsersDetails::className(), ['usersId' => 'id'])->andOnCondition(['users_details.valid' => 1]);
	}




	/**
	 * Autentication user model
	 */

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

	public function beforeSave($insert) {
		/**
		 * Creatuser
		 */
		if (parent::beforeSave($insert)) {
		   // $this->setIsNewRecord(0);
			return true;
		} else {
			return false;
		}
	}

	public function createAthleteUser() {

		$saveAttributes = $this->createSaveAttributes;


		if ($this->validate()) {

			/* Set nationalId as Default username And password*/
	   if($this->scenario!='updateExistAthleteUser')
		{
		  $this->passwordInput=isset($this->mobile)? $this->mobile:'';

				$this->setPassword($this->passwordInput);
				$this->generateAuthKey();

		}


		if($this->scenario=='updateExistAthleteUser')
		{
				$saveAttributes=$this->createExistSaveAttributes;
		}


			if ($result=$this->save(0,$saveAttributes)) {
				try {
					if($this->scenario=='updateExistAthleteUser')
					{
						if(!Yii::$app->getAuthManager()->checkAccess($this->id,self::Role_Athlete)){
							$this->addRole(self::Role_Athlete);
						}

					}else{

						$this->addRole(self::Role_Athlete);
					}
			 	}
			 	catch (Exception $e) {
					throw new \yii\web\HttpException(403, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
				}
				return true;
			}
		}

		return false;
	}

	/*
	 * diferent from createUser and modiy user:in modiy user dont call add role
	 * that prevent integrity error db in auth_assignment in yii
	 */

	public function modifyAthleteUser() {
		if ($this->validate()) {
			/**
			 * Set nationalId as Default username And password
			 */
			$this->username=isset($this->nationalId) ? $this->nationalId:'';
			$this->passwordInput=isset($this->nationalId)? $this->nationalId:'';

			$this->setPassword($this->passwordInput);
			$this->generateAuthKey();
			if ($result=$this->save(0)) {
				return true;
			}
		}

		return null;
	}

	public function addRole($role=Self::Role_Athlete)
	{
		$auth = Yii::$app->authManager;
		$Role = $auth->getRole($role);
		$auth->assign($Role,$this->id);
	}

	public function getAuthassignment()
	{
		return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
	}



	/**
	 * insert info in eventMember table
	 */
	public function insertMemberEventInfo($userDetailId,$eventId,$carvanId=0,$teamId,$universityId)
	{
		$eventMemberObj=new EventMembers;
		$eventMemberObj->eventId=$eventId;
		$eventMemberObj->teamsId=$teamId;
		$eventMemberObj->usersDetailsId=$userDetailId;
		$eventMemberObj->universityId=$universityId;
		$eventMemberObj->caravanId=$carvanId;
		$eventMemberObj->rolename=self::Role_Athlete;
		$eventMemberObj->user_type_id=UsersType::getUserTypeId('Athlete');
		$eventMemberObj->userId=$this->id;

		return $eventMemberObj->save(0);
	}

	/**
	 * delete User
	** 1- Find All Event mem if event mem record
	 * if event mem record>1
	 * 	 * delete User Eventmember record
	 * 	 * if athlete type of user in event==1
	 * 	 **delete User asigned roles
	 *
	 *   *if athlete type of user in event==1
	 *   **delete related User Details

	 ** if event mem record==1
	 *  * delete asigned roles
	 *  * delete related User Details
	 *  * delete User pic file
	 *  * delete User Eventmember record
	 *  * delete User Teammeberrecord
	 */


	public function deleteAthleteByAdmin($id = NULL,$teamId=null)
	{

		$this->userEventMemArr=EventMembers::find()->Users($this->id)->all();
		$count=count($this->userEventMemArr);
		if($count==1){

			$result=$this->deleteUserByOneEventMem($teamId);
			if($result)
				return ['result'=>true,'msg'=>Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')])];
            else
				return ['result'=>false,'msg'=>Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')])];


		}elseif($count>1)
		{
			return ['result'=>false,'msg'=>Yii::t('app','User Have Several Role Login As University resp user then Delete it.')];

		}

	}


	public function deleteAthlete($id = NULL,$eventId,$teamId=null)
	{

		$this->userEventMemArr=EventMembers::find()->Users($this->id)->all();
		$count=count($this->userEventMemArr);
		if($count==1){

			return $this->deleteUserByOneEventMem($teamId);


		}elseif($count>1)
		{
			return $this->deleteUserBySeveralEventMem($teamId);

		}

	}

	public function deleteUserByOneEventMem($teamId=null)
	{
		$picFileId=$this->fileId;

		$userId = $this->id ;
		$eventMemObj=$this->userEventMemArr[0];
		$eventId = Yii::$app->user->identity->eventId;
		$userDetailId=$eventMemObj->usersDetailsId;
		$teamId=$eventMemObj->teamsId;

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();

		try {

			/** delete EventmemberRow in EventMember table**/
			$result3=EventMembers::deleteAll('teamsId=:teamsId And eventId = :eventId AND rolename=:role AND usersDetailsId=:userDetail', [':teamsId'=>$teamId,':eventId' => (int)$eventId,':role' => self::Role_Athlete,':userDetail'=>$userDetailId]);

			/** delete responsed role in authitemtable**/
			$authAssignObj=new AuthAssignment();
			$resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_Athlete.'" And event_id='.(int)$eventId).'';


			/** Delete User Record if one userdetail*/
			parent::beforeDelete();
			try {
				$result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();
			}
			catch (\Exception $ex)
			{
				return false;
			}

			/*Start Delete UserDetails Record*/
			$userDetailsObj=new UsersDetails();
			$userDetailsObj->id=$userDetailId;

			parent::beforeDelete();

			try {
				$resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

			}
			catch (\Exception $ex) {
				return false;
			}

			/*Delete User pic file*/
			if(isset($picFileId)&&$picFileId!=0)
			{

				if (($FileModel = FileAction::findOne($picFileId)) !== null) {
					$FileModel->removeFile($picFileId);
				}
			}

			/*Delete subfield record i exist*/
			$result3=TeamsSubfieldsMember::deleteAll('teamsId=:teamsId And userId = :userId ', [':teamsId'=>$teamId,':userId' => (int)$userId,]);

			$transaction->commit();

			return true;

		} catch (\Exception $e) {
			$transaction->rollBack();
		    return false;
			throw $e;
		}
	}



	public function deleteUserBySeveralEventMem($teamId)
	{
		$countUniversityRespPosition=0;
		$countEventType=0;


		$eventId = Yii::$app->user->identity->eventId;

		/*get target event mem for this university universityResp role*/
		foreach ($this->userEventMemArr as $i=>$eventObj)
		{
			if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_Athlete&& $eventObj->teamsId==$teamId)
				$eventMemObj=$eventObj;

			/*count  position of Role_Athlete for user  */
			if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_Athlete)
				$countUniversityRespPosition++;

			/*count of user $countEventType in this event*/
			if($eventObj->eventId==$eventId)
				$countEventType++;
		}



		$userDetailId=$eventMemObj->usersDetailsId;
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();

		try {

			/** delete EventmemberRow in EventMember table**/
			$result3=$eventMemObj->delete();


			/** delete responsed role in authitemtable if just have one university resp position**/
			if($countUniversityRespPosition==1)
			{
				$authAssignObj=new AuthAssignment();
				$resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_Athlete.'" And event_id='.(int)$eventId).'';
			}


			if($countEventType==1)
			{

				/*Start Delete UserDetails Record*/
				$userDetailsObj=new UsersDetails();
				$userDetailsObj->id=$userDetailId;

				parent::beforeDelete();

				try {
					$resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

				}
				catch (\Exception $ex) {
					return false;
				}
			}


			/*delete info from user if exist in team subfield_member*/
			$result3=TeamsSubfieldsMember::deleteAll('teamsId=:teamsId And userId = :userId ', [':teamsId'=>$teamId,':userId' => (int)$this->id,]);

			$transaction->commit();

			return true;

		} catch (\Exception $e) {
			$transaction->rollBack();
			return false;
			throw $e;
		}

	}






	public function getAtheleteTeamId($eventId,$caravanId=0)
	{
		$eventMemObj=$this->getAtheleteEventMemObj($eventId,$caravanId);
		if($eventMemObj!=null)
			return $eventMemObj->teamsId;
		return false;
	}


 /*getAtheleteTeamObj*/
	public function getAtheleteEventMemObj($eventId,$caravanId)
	{
		$athleteTypeId=UsersType::getUserTypeId(User::Type_Athlete);

		$userDetailId=$this->getUserDetailIdBaseOnEventIdUserId($eventId);

		$eventMemObj=new EventMembers;
		$teamObj=$eventMemObj->getUserEventCaravanMemObj($eventId,$userDetailId,$athleteTypeId,$caravanId);
	   	return $teamObj;
	}


	public function getAtheleteAuthField($eventId,$caravanId=0)
	{

		$teamId=$this->getAtheleteTeamId($eventId,$caravanId);

		$teamObj = Teams::find()->andWhere('id = :id', [':id' => (int) $teamId])->one();

		return $teamObj->authFieldId;
	}

	public function getField()
	{	
		$eventId = Yii::$app->user->identity->eventId;

		$_athleteType = UsersType::getUserTypeId('Athlete');

		$userDetailId=$this->getUserDetailIdBaseOnEventIdUserId($eventId);
		$eventMemObj = new EventMembers;
		$eventMemresult = $eventMemObj->getUserEventMemObj($eventId,$userDetailId,$_athleteType);

		$teamId = $eventMemresult->teamsId;

		$_team = Teams::findOne(['id' => (int) $teamId]);

		if($_team !== NULL)
			return $_team->FieldName;
		else
			return '<i class="fa fa-ban font-red bigger-120"> </i>';
	}

	public function getCaravanName()
	{
		$eventId = Yii::$app->user->identity->eventId;

		$_athleteType = UsersType::getUserTypeId('Athlete');

		$userDetailId = $this->getUserDetailIdBaseOnEventIdUserId($eventId);
		$eventMemObj = new EventMembers;
		$eventMemresult = $eventMemObj->getUserEventMemObj($eventId,$userDetailId,$_athleteType);

		$teamId = $eventMemresult->teamsId;

		$_team = Teams::findOne(['id' => (int) $teamId]);

		if($_team !== NULL)
			return $_team->CaravanName;
		else
			return '<i class="fa fa-ban font-red bigger-120"> </i>';
	}

	public function insertTeamsSubfieldsMember($teamObj,$userDetailId)
	{
		if($teamObj==null)
			$teamObj=$this->_teamObj;

		$obj=new TeamsSubfieldsMember();
		$obj->fieldsId=$teamObj->fieldId;
		$obj->subFieldsId=$this->athleteSubFieldId;
		$obj->teamsId=$teamObj->id;
		$obj->userId=$this->id;
		$obj->userDetailsId=$userDetailId;

		return $obj->save(0);


	}
	public function UpdateTeamsSubfieldsMember($teamObj,$userDetailId)
	{
		if($teamObj==null)
			$teamObj=$this->_teamObj;

		$obj=new TeamsSubfieldsMember();
		$obj=TeamsSubfieldsMember::find()->where(['userId'=>$this->id,'teamsId'=>$teamObj->id,'fieldsId'=>$teamObj->fieldId,'userDetailsId'=>$userDetailId])->one();
		if($obj==null){
			$obj->addError('athleteSubFieldId',Yii::t('app','Not Found athleteSubFieldId'));
		}
		$obj->subFieldsId=$this->athleteSubFieldId;
		return $obj->Update(0);


	}

	public function getAthleteSubFieldIdValue($teamObj,$userDetailId)
	{
		if($teamObj==null)
			$teamObj=$this->_teamObj;

		$obj=new TeamsSubfieldsMember();
		$obj=TeamsSubfieldsMember::find()->where(['userId'=>$this->id,'teamsId'=>$teamObj->id,'fieldsId'=>$teamObj->fieldId,'userDetailsId'=>$userDetailId])->one();
		if($obj==null){
			$obj->addError('athleteSubFieldId',Yii::t('app','Not Found athleteSubFieldId'));
		}
		$this->athleteSubFieldId=$obj->subFieldsId;
		
		return  $this->athleteSubFieldId;
	}
}