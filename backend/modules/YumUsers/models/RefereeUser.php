<?php

namespace backend\modules\YumUsers\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\captcha\Captcha;
use Yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* * *************
 * Yii validator
 */
use backend\models\jdf;
use yii\validators\Validator;
use yii\validators\StringValidator;
use yii\validators\NumberValidator;

use backend\models\Teams;
use backend\models\EventMembers;

use backend\modules\YumUsers\models\YumPasswordValidator;
use backend\modules\YumUsers\models\ProActiveRecord;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property integer $gender
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $valid
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Membership[] $memberships
 */
class RefereeUser extends User {


   /*
	* Attribute in Modal Search shoud diferent name
	* to prevent Conflict in Main grid in indexaction
	*/
	public $searchInputName;
	public $searchInputLastName;


	public $fileAttr;




   public function scenarios()
	{

		$scenarios = parent::scenarios();
		$scenarios['createRefereeUser'] = ['name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted
		$scenarios['UpdateRefereeUser'] = ['name','last_name','email','tel','mobile','gender','sportNo',  'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'];//Scenario Values Only Accepted

		return $scenarios;

	}
public function rules() {

		//get setting of module
		$usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
		$passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
		$nameRequirements = Yum::module('YumUsers')->nameRequirements;

		/**Requirments***/
		$rules[] = [['name','last_name','nationalId', 'fatherName','birthDate','birthCityId'],'required','on'=>['createRefereeUser','UpdateRefereeUser']];


		/** password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator**/
		$passwordrule = array_merge(['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'], $passwordRequirements);
		$rules[] = $passwordrule;
		$rules[] =  ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];

		/** username validation from setting*/

		if ($usernameRequirements) {
			$rules[] = ['username', 'string',  'max' => $usernameRequirements['maxLen'],'min' => $usernameRequirements['minLen'],];
			$rules[] = ['username', 'match','pattern' => $usernameRequirements['match'],'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
		}
		$rules[] = ['username', 'checkUnique'];


		/** name validation from setting**/
		if ($nameRequirements) {
			$rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];

			$rules[] = [['name', 'last_name'], 'match',  'pattern' => $nameRequirements['match'],'message' => Yum::t($nameRequirements['dontMatchMessage'])];
		}


		/** Emali validation**/
		$rules[] = ['email', 'email'];
		$rules[] = ['email', 'checkUnique'];
		$rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];


		/** date validation**/
		$rules[] =  [['birthDate'], 'checkDateFormat'];



	/**
	 * natinalId validation
	 */
	//$rules[] =  [['nationalId'], 'validateNationalId'];
	$rules[] = ['nationalId', 'checkUnique'];
	$rules[] =array('nationalId', 'string', 'min'=>10, 'max'=>10);

	/**
	 * Numeric validation Utf8
	 */
	$rules[] = array(['idNo','tel','mobile','nationalId'], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');

	//$rules[] =[['nationalId','tel','mobile','idNo',], 'integer'];



	/** Number validation**/
	$rules[] = array(['sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);



		/** Number validation**/
		$rules[] = array(['birthCityId','sportNo','creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active','fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);

		/** String validation**/
		$rules[] = array([ 'univ', 'homepage', 'tel', 'mobile', ], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

		/** Range validation**/
		$rules[] = array([ 'lock', 'valid'], 'in', 'range' => array(0, 1));
		$rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE,self::MALE));




		/**Exist validation**/
		//$rules[] =  ['id', 'exist', 'targetClass' => '\backend\models\City'] ;



		/**Safe validation**/

		$rules[] = array(['verifyCode'], 'safe',);
		/**Safe validation for save in db**/
		$rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
		$rules[] = [['fileAttr','sportNo', 'eName', 'eFamily', 'fatherName', 'idNo','nationalId', 'birthDate','birthCityId','fileId'], 'safe',];


		/** file validation**/
		$fileModel=new File;
		$fileModel=new File;

	   $rules[] =  [['fileAttr'], 'file',
			'skipOnEmpty' => true,
			'extensions' =>'jpg,jpeg,png',
		   'checkExtensionByMimeType' => true,
			'maxSize' => File::MAX_FILE_SIZE,
			'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
			'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
			'minSize' => File::MIN_FILE_SIZE,
		   'mimeTypes' => ['image/jpeg','image/jpeg','image/png'],

			];


		/** Captcha validation**/
		$rules[] = array('verifyCode', 'captcha',
			'isEmpty' => !Captcha::checkRequirements(),
			'on' => 'register,captch,forgetpass',
			'message' => ' تصویر امنیتی  صحیح را وارد کنید.'
		);

		/** other vlidations*/
		//  $rules[] = array('role', 'RolesTypeValidation',);
		return $rules;
	}


	/* Autentication user model */

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}





		public function beforeSave($insert) {
		/*Creatuser */
		if (parent::beforeSave($insert)) {

			return true;
		} else {
			return false;
		}
	}




	public function createRefereeUser() {
		if ($this->validate()) {

		/*-Set nationalId as Default username And password-*/
		$this->username=isset($this->nationalId) ? $this->nationalId:'';
		$this->passwordInput=isset($this->nationalId)? $this->nationalId:'';

		$this->setPassword($this->passwordInput);
			$this->generateAuthKey();
			if ($result=$this->save(0)) {
			  try {
				$this->addRole(self::Role_Referee);
			  }catch (Exception $e) {
				throw new \yii\web\HttpException(403, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
			  }
				return TRUE;
			}
		}

		return null;
	}



  /*
   * diferent from createUser and modiy user:in modiy user dont call add role
   * that prevent integrity error db in auth_assignment in yii
   */

	public function modifyRefereeUser() {
		if ($this->validate()) {

		/*-Set nationalId as Default username And password-*/
		$this->username=isset($this->nationalId) ? $this->nationalId:'';
		$this->passwordInput=isset($this->nationalId)? $this->nationalId:'';

		$this->setPassword($this->passwordInput);
			$this->generateAuthKey();
			if ($result=$this->save(0)) {
				return TRUE;
			}
		}

		return null;
	}
	public function addRole($role=self::Role_Referee)
	{

		 $auth = Yii::$app->authManager;
		 $Role = $auth->getRole($role);
		 $auth->assign($Role,$this->id);
	}

	public function getAuthassignment()
	{
		return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
	}

		public function insertMemberEventInfo($userDetailId,$eventId,$carvanId=0,$teamId,$universityId)
	{
		$eventMemberObj=new EventMembers;
		$eventMemberObj->eventId=$eventId;
		$eventMemberObj->teamsId=$teamId;
		$eventMemberObj->usersDetailsId=$userDetailId;
		$eventMemberObj->universityId=$universityId;
		$eventMemberObj->caravanId=$carvanId;
		$eventMemberObj->user_type_id=UsersType::getUserTypeId('Referee');
		$eventMemberObj->userId=$this->id;

		return $eventMemberObj->save(0);
	}

	public function getCouchEventMemObj($eventId)
	{
		$coachTypeModels=UsersType::getCouchType();
		$coachTypeIds=ArrayHelper::map($coachTypeModels, 'id','id');

		$userDetailId=$this->getUserDetailIdBaseOnEventIdUserId($eventId);
		$eventMemObj=new EventMembers;
		$eventMemresult=$eventMemObj->getUserEventMemObj($eventId,$userDetailId,$coachTypeIds);
		return $eventMemresult;
	}

	public function getField()
	{	
		$eventId = Yii::$app->user->identity->eventId;

		$coachTypeModels=UsersType::getCouchType();
		$coachTypeIds=ArrayHelper::map($coachTypeModels, 'id','id');

		$userDetailId=$this->getUserDetailIdBaseOnEventIdUserId($eventId);
		$eventMemObj=new EventMembers;
		$eventMemresult=$eventMemObj->getUserEventMemObj($eventId,$userDetailId,$coachTypeIds);

		$teamId = $eventMemresult->teamsId;

		$_team = Teams::findOne(['id' => (int) $teamId]);

		if($_team !== NULL)
			return $_team->FieldName;
		else
			return '<i class="fa fa-ban font-red bigger-120"> </i>';
	}

	public function getCaravanName()
	{
		$eventId = Yii::$app->user->identity->eventId;

		$coachTypeModels=UsersType::getCouchType();
		$coachTypeIds=ArrayHelper::map($coachTypeModels, 'id','id');

		$userDetailId=$this->getUserDetailIdBaseOnEventIdUserId($eventId);
		$eventMemObj=new EventMembers;
		$eventMemresult=$eventMemObj->getUserEventMemObj($eventId,$userDetailId,$coachTypeIds);

		$teamId = $eventMemresult->teamsId;

		$_team = Teams::findOne(['id' => (int) $teamId]);

		if($_team !== NULL)
			return $_team->CaravanName;
		else
			return '<i class="fa fa-ban font-red bigger-120"> </i>';
	}
}