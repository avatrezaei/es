<?php

namespace backend\modules\YumUsers\models;

use backend\models\EventMembers;
use backend\models\Fields;
use backend\models\Subfields;
use backend\models\Teams;
use backend\models\TeamsSubfieldsMember;
use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\FileAction;
use backend\modules\YiiFileManager\models\Folders;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "users_details".
 *
 * @property integer $id
 * @property integer $usersId
 * @property integer $eventId
 * @property integer $educationalField
 * @property integer $educationalSection
 * @property string $stuNo
 * @property string $sportAssuranceNo
 * @property integer $weight
 * @property integer $length
 * @property string $mobileNo
 * @property string $phoneNo
 * @property string $address  
 * @property string $officeAddress  
 * @property string $beltColor
 * @property string $bankAccountNo
 * @property string $bankAcoountName
 * @property string $refereeDegree
 * @property integer $sportFieldId
 * @property string $assuranceCartFileId
 * @property string $stuCartFileId
 * @property string $sportMembershipHistory
 * @property string $userPosition
 * @property integer $valid
 */
class UsersDetails extends \yii\db\ActiveRecord
{

	public $fileAttrAssuranceCart;
	public $fileAttrStuCart;
	public $fileAttrjudgeCart;

	public $athleteSubFieldId;
	public $teamId;

    public $userType;
    public $validateExecutiveUserType=0;


	const sportMemHistory_National = 'National';
	const sportMemHistory_Provincial = 'Provincial';
	const sportMemHistory_SuperLeague = 'SuperLeague';
	const sportMemHistory_FirstLeague = 'FirstLeague';
	const sportMemHistory_Zonal = 'Zonal';
	const sportMemHistory_Nothing = 'Nothing';



public static function getSportMemShipText($val)
{
	if($val == NULL)
		return NULL;

	return self::itemAlias('sportMembershipHistory',$val);

	//$list=UsersDetails::itemAlias('sportMembershipHistory');


}


	
	private $trulyDelete=0;

	/**getEventNameText
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%users_details}}';
	}

	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios['CreateAthleteUserDetailse'] = ['userType','sportMembershipHistory','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'email','phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['CreateRefereeUserDetailse'] = ['userType','refereeDegree','judgeCartFileId','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['CreateTechCommiteeDetailse'] = ['userType','refereeDegree','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['ExecutiveCommitteeUserDetailse'] = ['userType','userPosition','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['MediaStaffUserDetailse'] = ['userType','userPosition','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['LocalDriverUserDetailse'] = ['userType','userPosition','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['VipMiscCaravanUserDetailse'] = ['userType','userPosition','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['UpdateUserDetailse'] = ['userType','sportMembershipHistory','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['UpdateMiscUserDetailse'] = ['userType','userPosition','sportMembershipHistory','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['UpdateAtheleteUserDetailse'] = ['userType','sportMembershipHistory','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['UpdateRefereeUserDetailse'] = ['userType','refereeDegree','judgeCartFileId','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['UpdateCommiteeUserDetailse'] = ['userType','refereeDegree','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted
		$scenarios['UpdateUserByPositionUserDetailse'] = ['userType','userPosition','refereeDegree','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName'];//Scenario Values Only Accepted

		return $scenarios;
	} 

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$fileModel=new File;

		$rules= [
			/*--Required Fields--*/
			[['eventId', 'usersId'],'required'],
			[['bankAccountNo','sportFieldId','bankAcoountName'],'required','on'=>['CreateTechCommiteeDetailse','CreateRefereeUserDetailse','UpdateRefereeUserDetailse']],
			[['sportFieldId'],'required','on'=>['CreateCoachUserDetailse']],
			[['refereeDegree'],'required','on'=>['CreateRefereeUserDetailse','UpdateRefereeUserDetailse']],
			[['userPosition'],'required','on'=>['UpdateMiscUserDetailse']],
			[['sportFieldId'],'required','on'=>['UpdateUserDetailse']],
			[['sportFieldId','sportMembershipHistory','educationalField', 'stuNo',],'required','on'=>['CreateAthleteUserDetailse','UpdateAtheleteUserDetailse']],
			[['sportAssuranceNo','educationalField', 'stuNo',],'required','on'=>['CreateAthleteUserDetailse','UpdateAtheleteUserDetailse']],
			[['eventId', 'usersId','sportAssuranceNo','educationalField', 'stuNo',],'required','on'=>['CreateAthleteUserDetailse','UpdateAtheleteUserDetailse']],
			[['sportFieldId'],'required','on'=>['UpdateCommiteeUserDetailse']],
			[['userPosition'],'required','on'=>['ExecutiveCommitteeUserDetailse','LocalDriverUserDetailse','MediaStaffUserDetailse','VipMiscCaravanUserDetailse','UpdateUserByPositionUserDetailse']],

			[['stuNo'],'checkUnique','on'=>['CreateAthleteUserDetailse','UpdateAtheleteUserDetailse']],



		[['userPosition','sportMembershipHistory','refereeDegree','officeAddress','sportFieldId','usersId', 'eventId', 'educationalField', 'educationalSection', 'stuNo', 'sportAssuranceNo', 'weight', 'length', 'mobileNo', 'phoneNo', 'address', 'beltColor', 'bankAccountNo', 'bankAcoountName','userType'], 'safe'],
			[['sportFieldId','valid','usersId', 'eventId', 'educationalField', 'educationalSection', 'weight', 'length','judgeCartFileId'], 'integer'],
			//[['sportAssuranceNo'], 'match','pattern' => '/^([\u0660-\u0669]|[\u0030-\u0039]|[\u06F0-\u06F9]|[\d])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.'],
	     	[['stuNo'], 'string', 'max' => 15],
			[['sportAssuranceNo'], 'string', 'max' => 11],
			//[['sportAssuranceNo'], 'match', 'max' => 11],
			[['sportAssuranceNo', 'beltColor', 'bankAcoountName'], 'string', 'max' => 45],
			[['mobileNo', 'phoneNo'], 'string', 'max' => 11],
			[['address'], 'string', 'max' => 255],
			[['bankAccountNo'], 'string', 'max' => 30],

			/* ---------file validation ---------- */
			[['fileAttrAssuranceCart','fileAttrStuCart'], 'file',
				'skipOnEmpty' => true,
				'extensions' =>'jpg,jpeg,png',
				'checkExtensionByMimeType' => true,
				'maxSize' => File::MAX_FILE_SIZE,
				'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
				'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
				'minSize' => File::MIN_FILE_SIZE,
				'mimeTypes' => ['image/jpeg','image/jpeg','image/png'],
			],
			[['fileAttrjudgeCart'], 'file',
				'skipOnEmpty' => false,
				'extensions' =>'jpg,jpeg,png',
				'checkExtensionByMimeType' => true,
				'maxSize' => File::MAX_FILE_SIZE,
				'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
				'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
				'minSize' => File::MIN_FILE_SIZE,
				'mimeTypes' => ['image/jpeg','image/jpeg','image/png'],
				'on'=>['CreateRefereeUserDetailse','UpdateRefereeUserDetailse'],

			],
			[['fileAttrAssuranceCart','fileAttrStuCart','assuranceCartFileId','stuCartFileId','fileAttrjudgeCart' ], 'safe'],
				
		];

		if($this->validateExecutiveUserType)
		{

			$rules[] = [['userType', ],'required'];
			$executiveUsersTypes=\backend\modules\YumUsers\models\UsersType::getExecutiveCommitteeType();
			$executiveUserTypeArr=ArrayHelper::map($executiveUsersTypes, 'id', 'id');
			$rules[] = array([ 'userType'], 'in', 'range' =>$executiveUserTypeArr);
		}


		return $rules;
	}


	public function UpdateUserType(){

		$_event = EventMembers::find()->user($this->usersId)
			->event(Yii::$app->user->identity->eventId)
			->role(User::Role_ExecutiveCommittee)
			->one();
		$_event->user_type_id=$this->userType;
		if(is_object($_event))
			return $_event->save(0);

		return 0;
	}
	public function getExecutiveCommitteRole()
	{
		$_event = EventMembers::find()->user($this->id)
			->event(Yii::$app->user->identity->eventId)
			->role(self::Role_ExecutiveCommittee)
			->one();
		if(is_object($_event))
			return $this->userType=$_event->user_type_id;
	}
	public  function checkUnique($attribute, $params)
	{

		$value = $this->$attribute;
		$userModels = $this->find()->where($attribute . '= :attribute And valid=1', [':attribute' => $value])->all();

		$result=1;
		foreach ($userModels as $userModel)
		{

			if (isset($this->isNewRecord)&&$this->getIsNewRecord()) {
				if ($userModel !== null) {
					$result= false;
				}
			} else {


				if ($userModel !== null && $this->id !== $userModel->id) {
					$result= false;

				}
			}
		}

		if(!$result)
		{
			$this->addError($attribute, Yii::t('app', 'The {item} Id is Repetitive',['item' => $this->getAttributeLabel($attribute)]));
            return false;
		}


		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'usersId' => Yii::t('app', 'Users ID'),
			'eventId' => Yii::t('app', 'Event ID'),
			'educationalField' => Yii::t('app', 'Educational Field'),
			'educationalSection' => Yii::t('app', 'Educational Section'),
			'stuNo' => Yii::t('app', 'Student No'),
			'sportAssuranceNo' => Yii::t('app', 'Sport Assurance No'),
			'weight' => Yii::t('app', 'Weight'),
			'length' => Yii::t('app', 'Length'),
			'mobileNo' => Yii::t('app', 'Mobile No'),
			'phoneNo' => Yii::t('app', 'Phone No'),
			'address' => Yii::t('app', 'Address'),
			'beltColor' => Yii::t('app', 'Belt Color'),
			'bankAccountNo' => Yii::t('app', 'Bank Account No'),
			'bankAcoountName' => Yii::t('app', 'Bank Acoount Name'),
			'fileAttrAssuranceCart' => Yii::t('app', 'AssuranceCart'),
			'fileAttrStuCart' => Yii::t('app', 'StuCart'),
			'sportFieldId'=>Yii::t('app', 'رشته ورزشی'),
			'officeAddress'=>Yii::t('app', 'آدرس  پستی محل کار'),
			'refereeDegree'=>Yii::t('app', 'درجه داوری'),
			'sportMembershipHistory'=>Yii::t('app','Sport Membership History'),
			'fileAttrjudgeCart'=>Yii::t('app', 'fileAttrjudgeCart'),
			'userType'=>Yii::t('app', 'User Type'),
			'userPosition'=>Yii::t('app', 'User Position'),
			'teamId'=>Yii::t('app', 'Team'),
			'athleteSubFieldId'=>Yii::t('app','Athlete Sub Field Id')

		];
	}
	
	/**
	 * @inheritdoc
	 * @return UsersDetails the active query used by this AR class.
	 */
	public static function find()
	{
		return new UsersDetailsQuery(get_called_class());
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert))
		{
			/*prevent Sql Error Property cannot be null*/
			if($this->educationalField==null)
				$this->educationalField=0;
			
			if($this->educationalSection==null)
				$this->educationalSection=0;

			/**replace arabic character by persian**/
			$arabicChar = array('ي','ك');
			$persianChar = array('ی','ک');
			$this->userPosition = str_replace($arabicChar, $persianChar, $this->userPosition);
			return true;
		}
		else
		{
			return false;
		}
	}  
	
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'usersId']);
	}	  

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEvent()
	{
		return $this->hasOne(Event::className(), ['id' => 'eventId']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventMember()
	{
		return $this->hasOne(EventMembers::className(), ['usersDetailsId' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSection()
	{
		return $this->hasOne(EducationalSections::className(), ['EduSecCode' => 'educationalSection']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup()
	{
		return $this->hasOne(EducationalGroups::className(), ['EduGrpCode' => 'educationalField']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSportField()
	{
		return $this->hasOne(Fields::className(), ['id' => 'sportFieldId']);
	}

	public function getEventNameText()
	{
	   $eventObj=\backend\models\Event::findOne((int)$this->eventId);
	  $eventName=($eventObj)?($eventObj->name) : ('-----'); 
	   return $eventName;
	}

	public function getUniversityName()
	{
	   return $this->eventMember->university != NULL ? $this->eventMember->university->name : NULL;
	}

	public function getSportFieldNameText()
	{
		return $this->sportField != NULL ? $this->sportField->name : NULL;
	}

	public function getEducationalGroupsNameText()
	{
		return $this->group != NULL ? $this->group->PEduName : NULL;
	}

	public function getEducationalSectionNameText()
	{
		return $this->section != NULL ? $this->section->PEduSecName : NULL;
	}

	public function saveUniversityRespDetails($userId=null)
	{
		if(isset($userId))
		{
			$this->usersId=$userId; 
		}

		if ($this->validate()){
			if ($this->save(0)){
				return $this->id;
			}
		}

		return false;
	}

	public function saveAdminEventDetails($userId=null)
	{
		if(isset($userId))
		{
			$this->usersId=$userId; 
		}

		if ($this->validate()){
			if ($this->save(0)){
				return $this->id;
			}
		}

		return false;
	}
	
	public function saveTechCommitDetails($userId=null) {

		if(isset($userId))
			{
			   $this->usersId=$userId; 
			}

		if ($this->validate()) {
			
			
			if ($this->save(0)) {
				try {
					$this->uploadFileJudgeCart() ;
				}catch (Exception $e) {
					throw new \yii\web\HttpException(500, 'Pro0blem in Judge Cart File');
				}

				return TRUE;
			}
		}

		return false;
	}
	

	public function saveRefereeDetails($userId=null) {

		if(isset($userId))
		{
			$this->usersId=$userId;
		}

		if ($this->validate()) {


			if ($this->save(0)) {
							return TRUE;
			}
		}

		return false;
	}
	public function saveUserDetails($userId=null)
	{
		if(isset($userId))
		{
		   $this->usersId=$userId; 
		}

		if ($this->validate())
		{
			if ($this->save(0)){
			  return true;
			}
		}

		return false;
	}

	public function saveNewRefereeDetails($userId=null) {

		if(isset($userId))
			{
			   $this->usersId=$userId; 
			}

		if ($this->validate()) {
			
			$this->setIsNewRecord(1);
			if ($this->save(0)) {
	 
			  return TRUE;
			}
		}

		return false;
	}



	
		public function saveAthleteDetails($userId=null) {

		if(isset($userId))
			{
			   $this->usersId=$userId; 
			}

		if ($this->validate()) {
			
			if ($this->save(0)) {
			  try {
				 $this->uploadFileStuCart() ;		
				 }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن کارت دانشجویی پیش آمده است.');
			  }  

			  try {
				  $this->uploadFileAssuranceCart();
				 }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن  کارت یمه پیش آمده است.');
			  }  
			  
			  return TRUE;
			}
		}

		return false;
	}






	public static function itemAlias($type, $code = NULL) {
		$_items = [
			'sportMembershipHistory' => [
				self::sportMemHistory_National => Yii::t ( 'app', 'National' ),
				self::sportMemHistory_FirstLeague => Yii::t ( 'app', 'FirstLeague' ),
				self::sportMemHistory_Provincial => Yii::t ( 'app', 'Provincial' ),
				self::sportMemHistory_SuperLeague => Yii::t ( 'app', 'SuperLeague' ),
				self::sportMemHistory_Zonal => Yii::t ( 'app', 'Zonal' ),
				self::sportMemHistory_Nothing => Yii::t ( 'app', 'Nothing' )
			],

		];
		if (isset ( $code ))
			return isset ( $_items [$type] [$code] ) ? $_items [$type] [$code] : false;
		else
			return isset ( $_items [$type] ) ? $_items [$type] : false;
	}


	public function getType() {
		$class = '';

		switch ($this->sportMembershipHistory) {
			case self::sportMemHistory_National :
				$class = 'success';
				break;

			case self::sportMemHistory_FirstLeague :
				$class = 'warning';
				break;

			case self::sportMemHistory_Provincial :
				$class = 'danger';
				break;

			case self::sportMemHistory_SuperLeague :
				$class = 'info';
				break;

			case self::sportMemHistory_Zonal :
				$class = 'default';
				break;

			case self::sportMemHistory_Nothing :
				$class = 'default';
				break;

		}

		return '<span class="label label-' . $class . '">' . self::itemAlias ( 'Type', $this->sportMembershipHistory ) . '</span>';
	}


	
	public function saveNewAthleteDetails($userId=null) {

		if(isset($userId))
			{
			   $this->usersId=$userId; 
			}

		if ($this->validate()) {
			
			$this->setIsNewRecord(1);
			
			if ($this->save(0)) {
			  try {
				 $this->uploadFileStuCart() ;		
				 }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن کارت دانشجویی پیش آمده است.');
			  }  

			  try {
				  $this->uploadFileAssuranceCart();
				 }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن  کارت یمه پیش آمده است.');
			  }  
			  
			  return TRUE;
			}
		}

		return false;
	}
	
	
	
	  public function uploadFileAssuranceCart()
	{
				  $fileModel=new File;
				  $fileModel->scenario=  File::MODEL_FILE_UPLOAD_SCENARIO;
				  $fileModel->modelFileTyps='jpg,jpeg,png';
				  $fileModel->modelMimeFileTyps=['image/jpeg','image/jpeg','image/png'];
				  $fileModel->modelFileskipOnEmpty=true;
				  $fileModel->modelFileSize=52428800;
					/** Add icon file**/ 
		          $fileModel->folder_id = Folders::USERS_PIC_FOLDER_ID;
		          $fileModel->model_id =$this->id;
				  $fileModel->model_type ='user_detail_assurance_cart';
				   
				   
									 
		          $fileModel->temporaryFileAttr =  \yii\web\UploadedFile::getInstance($this, 'fileAttrAssuranceCart');

				   if($fileModel->temporaryFileAttr!=null)
				   {
						if ($fileModel->validate()&&$fileModel->uploadModelFile('user_detail_assurance_cart'.$this->id)) {

							
							/* remove old file in edit action call*/
							if(isset($this->assuranceCartFileId))
							{
							$oldFileId=$this->assuranceCartFileId;
								if (($oldFileModel = FileAction::findOne($oldFileId)) !== null)
								{
									 $oldFileModel->removeFile($oldFileId);
								}
							}	   
							
						   /*save file id in detailuser table */
						   $this->assuranceCartFileId=(int)$fileModel->id;
						   $this->update(0);

							return true;

					   }else 
					   {
							if (count($fileModel->errors) > 0) {
								$msg = '';
								foreach ($fileModel->errors as $attribute => $errors) {
									foreach ($errors as $error) {
										$msg.= $error ;
									}
								}
							$this->addError('fileAttrAssuranceCart', $msg);
							return false;

				
							}


					   }
				   }
				   return true;
	}


	
	  public function uploadFileStuCart()
	{
				  $fileModel=new File;
				  $fileModel->scenario=  File::MODEL_FILE_UPLOAD_SCENARIO;
				  $fileModel->modelFileTyps='jpg,jpeg,png';
				  $fileModel->modelMimeFileTyps=['image/jpeg','image/jpeg','image/png'];
				  $fileModel->modelFileskipOnEmpty=true;
				  $fileModel->modelFileSize=52428800;
					/** Add icon file**/ 
				   $fileModel->folder_id = Folders::USERS_PIC_FOLDER_ID;
				   $fileModel->model_id =$this->id;
				   $fileModel->model_type ='user_detail_stu_cart';
				   
				   $fileModel->temporaryFileAttr =  \yii\web\UploadedFile::getInstance($this, 'fileAttrStuCart');


				   if($fileModel->temporaryFileAttr!=null)
				   {
						if ($fileModel->validate()&&$fileModel->uploadModelFile('user_detail_stu_cart'.$this->id)) {


							/* remove old file in edit action call*/
							if(isset($this->stuCartFileId))
							{
							$oldFileId=$this->stuCartFileId;
								if (($oldFileModel = FileAction::findOne($oldFileId)) !== null)
								{
									 $oldFileModel->removeFile($oldFileId);
								}
							} 
							
							   /*save file id in detailuser table */
							   $this->stuCartFileId=(int)$fileModel->id;
							   $this->update();
							   
								return true;

					   }else 
					   {
							if (count($fileModel->errors) > 0) {
								$msg = '';
								foreach ($fileModel->errors as $attribute => $errors) {
									foreach ($errors as $error) {
										$msg.= $error ;
									}
								}
							$this->addError('fileAttrStuCart', $msg);
							return false;

				
							}


					   }
				   }
		  return true;
	}


	public function uploadFileJudgeCart()
	{
		$fileModel=new File;
		$fileModel->scenario=  File::MODEL_FILE_UPLOAD_SCENARIO;
		$fileModel->modelFileTyps='jpg,jpeg,png';
		$fileModel->modelMimeFileTyps=['image/jpeg','image/jpeg','image/png'];
		$fileModel->modelFileskipOnEmpty=true;
		$fileModel->modelFileSize=52428800;
		/** Add icon file**/
		$fileModel->folder_id = Folders::USERS_PIC_FOLDER_ID;
		$fileModel->model_id =$this->id;
		$fileModel->model_type ='user_detail_judge_cart';



		$fileModel->temporaryFileAttr =  \yii\web\UploadedFile::getInstance($this, 'fileAttrjudgeCart');

		if($fileModel->temporaryFileAttr!=null)
		{
			if ($fileModel->validate()&&$fileModel->uploadModelFile('user_detail_judge_cart'.$this->id)) {


				/* remove old file in edit action call*/
				if(isset($this->judgeCartFileId))
				{
					$oldFileId=$this->judgeCartFileId;
					if (($oldFileModel = FileAction::findOne($oldFileId)) !== null)
					{
						$oldFileModel->removeFile($oldFileId);
					}
				}

				/*save file id in detailuser table */
				$this->judgeCartFileId=(int)$fileModel->id;
				$this->update(0);

				return true;

			}else
			{
				if (count($fileModel->errors) > 0) {
					$msg = '';
					foreach ($fileModel->errors as $attribute => $errors) {
						foreach ($errors as $error) {
							$msg.= $error ;
						}
					}
					$this->addError('fileAttrjudgeCart', $msg);
					return false;


				}

			}
		}
		return true;

	}


	public function getStuCartIconThumbnail($fileId = null)
	{
		if (isset($fileId))
			$this->stuCartFileId = $fileId;

		if (($filemodel = File::findOne($this->stuCartFileId)) !== null) {
			$url = Url::to(['/YiiFileManager/file/view-file', 'name' => $filemodel->hashed_name]);


			return $img = '<img ' . $this->getImgShowAttribute() . ' src=' . $url . '>';
		} else {
			return '---';
		}
	}



	public function getAssuranceCartIconThumbnail($fileId = null)
	{
		if (isset($fileId))
			$this->assuranceCartFileId = $fileId;

		if (($filemodel = File::findOne($this->assuranceCartFileId)) !== null) {

			$url = Url::to(['/YiiFileManager/file/view-file', 'name' => $filemodel->hashed_name]);

			return $img = '<img ' . $this->getImgShowAttribute() . ' src=' . $url . '>';
		} else {
			return '---';
		}
	}

	public function getJudgeCartIconThumbnail($fileId = null)
	{
		if (isset($fileId))
			$this->judgeCartFileId = $fileId;

		if (($filemodel = File::findOne($this->judgeCartFileId)) !== null) {
			$url = Url::to(['/YiiFileManager/file/view-file', 'name' => $filemodel->hashed_name]);
			return $img = '<img ' . $this->getImgShowAttribute() . ' src=' . $url . '>';
		} else {
			return '---';
		}
	}

	public function getImgShowAttribute()
	{
		return 'title="'.Yii::t('app','Click img  for show bigger').'"  data-toggle="modal" data-target="#myModal" style="width:64px;height:64px; cursor:pointer" alt="tt"';
	}
	public function delete($id = NULL) {


	if ($id != NULL)
		$this->id = $id;

	parent::beforeDelete();

	$stufileId=$this->stuCartFileId;
	$assurancefileId=$this->assuranceCartFileId;			   


	if ($this->trulyDelete) {

	   $result= (parent::delete()) ? true : FALSE;  

	} else {
	   $result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();
	}

	if($result)
	{

		/* ---Delete childs---- */
		try {
			$this->DeleteFile($assurancefileId);
		} catch (\Exception $ex) {
			$this->addError('stuCartFileId','error in delete Assurance cart file' );
			return false;
		}
		/* --------------------- */

		/* ---Delete childs---- */
		try {
			$this->DeleteFile($stufileId);
		} catch (\Exception $ex) {
			$this->addError('assuranceCartFileId','error in delete stu cartfiles' );
			return false;
		}
		/* --------------------- */
	} 
		parent::afterDelete ();
		return $result;
	}   
	
	
	
	
	public function DeleteFile($fileId)
	{
			/* remove relative file */
		if(isset($fileId))
		{
			if (($oldFileModel = FileAction::findOne($fileId)) !== null)
			{
				 $oldFileModel->removeFile($fileId);
			}
		}
	}

	 public function deletebyUserId($userid) {
		 
		try {
		$userDetails = $this->find()->where("usersId=".(int)$userid)->all();  
		} catch (\Exception $ex) {
			return false;
		}		 

		if(is_array($userDetails))
		{
			foreach($userDetails as $userDetail)
			{
			  $userDetail->delete();  
			}
		}
	
	
	 }   
	 



	public function fields()
	{
		return [
			'name' => function () {
				return $this->user->FullName;
			},
			'fatherName' => function () {
				return $this->user->fatherName;
			},
			'idNo' => function () {
				return $this->user->idNo;
			},
			'nationalId' => function () {
				return $this->user->nationalId;
			},
			'stuNo',
			'birthDate' => function () {
				return $this->user->birthDate;
			},
			'email' => function () {
				return $this->user->email;
			},
			'section' => function () {
				return $this->EducationalSectionNameText;
			},
			'group' => function () {
				return $this->EducationalGroupsNameText;
			},
			'university' => function () {
				return $this->UniversityName;
			},
			'sportfield' => function () {
				return $this->SportFieldNameText;
			},
			'sportAssuranceNo',
			'weight',
			'length',
			'mobile' => function () {
				return $this->mobileNo;
			},
			'phone' => function () {
				return $this->phoneNo;
			},
			'address',
			'officeAddress',
			'beltColor',
			'bankAccountNumber' => function () {
				return $this->bankAccountNo;
			},
			'bankAcoountName',
			'refereeDegree',
		];
	}


	public  function getFSubFieldId($teamId)
	{
		$teamObj=Teams::find()->andWhere(['id' =>$teamId ])->one();
		if(is_object($teamObj))
		{
			$fieldObj=$teamObj->fields;
			if($fieldObj->hasQuota)
			{
				$teamsSubfieldsMemberObj=TeamsSubfieldsMember::find()->where(['teamsId' =>$teamId,'userId' =>$this->usersId,'fieldsId' =>$fieldObj->id,'userDetailsId'=>$this->id ])->one();
                 return    $teamsSubfieldsMemberObj->subFieldsId;
			}

		}

	}

	public function getSubFieldName($teamId)
	{
		if($id=$this->getFSubFieldId($teamId))
		{

			$subFieldObj=Subfields::find()->where(['id'=>$id ])->one();
			return $subFieldObj->name;

		}

		return '---';

	}
}

/**
 * This is the ActiveQuery class for [[UsersDetails]].
 *
 * @see UsersDetails
 */
class UsersDetailsQuery extends \yii\db\ActiveQuery
{
	public function user($id)
	{
		return $this->andWhere(['usersId' => $id]);
	}

	public function event($id)
	{
		return $this->andWhere([UsersDetails::tableName() . '.eventId' => $id]);
	}

	public function init()
	{
		parent::init();
		return $this->andWhere([UsersDetails::tableName() . '.valid' => 1]);
	}

	/**
	 * @inheritdoc
	 * @return UsersDetails[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}
}