<?php

//namespace frontend\models;

namespace backend\modules\YumUsers\models;

use Yii;
use backend\models\jdf;

class ProActiveRecord extends \yii\db\ActiveRecord {

   // public $jPattern = "/^(?:138[0-9]|139[0-9])-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2][0-9]|3[0-1])$/";
  //public $gPattern = "/^([0-9][0-9][0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
  //  public $jPattern = "/^([1][1-4][0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
	public $gPattern = "/^([1-2][0-9][0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
   public $jPattern ='/^[0-9]{4}-(0[0-9]|1[0-2])-(0[0-9]|[1-2][0-9]|3[0-1])$/';
   public $jPattern_slash ='/^[0-9]{4}\/(0[0-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/';
	
	
   public  function getAttributeText($attribute)
   {
	  $Text=(strlen($this->$attribute)>0)?($this->$attribute) : ('-----'); 
	  return $Text;
   }
   
   
		 public function checkDateFormat($attribute, $params) {
		   $value = $this->$attribute;
 
			if (preg_match($this->jPattern_slash, $value)) {
				$value=str_replace("/","-",$value);
			}

			if (!preg_match($this->jPattern, $value)) {
				$this->addError($attribute, Yii::t('app', 'You entered an invalid date format.'));
			}
		
	}

	public function semanticDateValidation($attribute, $params) {
		/*
		 * if date format is ok and  has no errors this validate acuuure
		 */
		if (!empty($this->errors['from_date']) || !empty($this->errors['to_date'])) {
			return;
		}

		$jdf = new jdf();
		
		if (!empty($this->from_date)) {
			if ($_COOKIE['language'] == 'fa_IR') {
				$this->from_date=str_replace("/","-",$this->from_date);
				$from_date = $jdf->jalaliStr_to_gregorianStr($this->from_date);
			} else {
				$from_date = ($this->from_date);
			}
		}
		
		if (!empty($this->to_date)) {
			if ($_COOKIE['language'] == 'fa_IR') {
				$this->to_date=str_replace("/","-",$this->to_date);
				$to_date =$jdf->jalaliStr_to_gregorianStr($this->to_date);
			} else {
				$to_date =($this->to_date);
			}
		}

		if (isset($from_date) && isset($to_date) && strtotime($to_date) < strtotime($from_date)) {
			$this->addError($attribute, Yii::t('app', 'Start date must be proportionate to the end date'));
			return;
		}
	}

	public function getUserInfo($userIdAttribute) {
		$user = $user = User::findOne($this->$userIdAttribute);

		if ($user) {
			$name = $user->name;
			$lastName = $user->last_name;
			$author = $name . " " . $lastName;
		}
		return(isset($author)) ? ($author) : "-----";
	}

	public function getJalalyDate($date) {
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d', strtotime($this->$date));
		return $jDateString;
	}

	public function getFullJalalyDate($date) {
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d  H:i:s', strtotime($this->$date));
		return $jDateString;
	}

	public function getEnDateFromTimeStamp($dateField) {
		$rows = (new \yii\db\Query())
			->select(['DATE_FORMAT( `' . $dateField . '` , "%Y-%m-%d" ) AS date'])
			->from($this->tableName())
			->where('id=:id', array(':id' => $this->id))
			->all();
		$gDateString=isset($rows[0]['date'])?$rows[0]['date']:'';
		$this->$dateField =$gDateString;
		return $gDateString;
	}
	
	public function getFaDateFromTimeStamp($dateField) {
		$rows = (new \yii\db\Query())
			->select(['DATE_FORMAT( `' . $dateField . '` , "%Y-%m-%d" ) AS date'])
			->from($this->tableName())
			->where('id=:id', array(':id' => $this->id))
			->all();
		$gDateString=isset($rows[0]['date'])?$rows[0]['date']:'';
		$this->$dateField = $gDateString;
		$jdf=new jdf();
		$jDateString = $jdf->jdate('Y-m-d', strtotime($gDateString));
		$jDateString=str_replace("-","/",$jDateString);
		return $jDateString;
	}


	public function convertPersianNumToEnNum($string) {
		$persianDigits1= array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
		$persianDigits2= array('٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠');
		$allPersianDigits=array_merge($persianDigits1, $persianDigits2);
		$replaces = array('0','1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9');
		return str_replace($allPersianDigits, $replaces , $string);
	}
}
