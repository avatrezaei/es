<?php

namespace backend\modules\YumUsers\models;

use backend\models\AuthorizedUniversity;
use backend\models\EventMembers;
use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YumUsers\models\YumPasswordValidator;
use \yii\web\HttpException;
use Yii;

use backend\modules\YiiFileManager\models\FileAction;
/* * *************
 * Yii validator
 */

class CarvanSupervisorUser extends User
{

    /*
     * Attribute in Modal Search shoud diferent name
     * to prevent Conflict in Main grid in indexaction
     */
    public $searchInputName;
    public $searchInputLastName;
    public $searchInputStuNo;


    public $userDetailId;
    public $userUniversityId;
    public $eventId;


    /*****SPORT EVENT FIELDS     *******/


    public $fileAttr;


    public function scenarios()
    {

        $scenarios = parent::scenarios();
			$scenarios['createSoupervisorByExistUserDetail'] = ['fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			$scenarios['createSoupervisorByExistUser'] = ['fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			$scenarios['createCarvanSupervisor'] = ['fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			$scenarios['UpdateCarvanSupervisor'] = ['fileId','fileAttr','username', 'name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'sportNo', 'birthDate', 'passwordInput', 'passwordInput_repeat', 'nationalId'];//Scenario Values Only Accepted
			return $scenarios;

    }


    public function rules()
    {
        //get setting of module
        $usernameRequirements = Yum::module('YumUsers')->usernameRequirements;
        $passwordRequirements = Yum::module('YumUsers')->passwordRequirements;
        $nameRequirements = Yum::module('YumUsers')->nameRequirements;


        /**Requirments***/
        $rules[] = [['nationalId','name', 'last_name', 'mobile'], 'required', 'on' => ['createCarvanSupervisor', 'createSoupervisorByExistUser']];
        $rules[] = [['nationalId','name', 'last_name',  'mobile','gender',], 'required'];


        /** password validation from setting by YumPasswordValidator class place in /component/YumPasswordValidator**/
        $passwordrule = array_merge(['passwordInput', 'backend\modules\YumUsers\models\YumPasswordValidator'], $passwordRequirements);
        $rules[] = $passwordrule;
        $rules[] = ['passwordInput_repeat', 'compare', 'compareAttribute' => 'passwordInput'];

        /** username validation from setting*/
        if ($usernameRequirements) {
            $rules[] = ['username', 'string', 'max' => $usernameRequirements['maxLen'], 'min' => $usernameRequirements['minLen'],];
            $rules[] = ['username', 'match', 'pattern' => $usernameRequirements['match'], 'message' => Yum::t($usernameRequirements['dontMatchMessage'])];
        }
        $rules[] = ['username', 'checkUnique'];


        /**
         * natinalId validation
         */
        if(!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id, User::Role_Admin )&&!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id,  User::Role_SeniorUserEvent)) {
            $rules[] = [['nationalId'], 'validateNationalId'];
        }
        $rules[] =array('nationalId', 'string', 'min'=>10, 'max'=>10);
        $rules[] = ['nationalId', 'checkUnique'];

        /** name validation from setting**/
        if ($nameRequirements) {
            $rules[] = [['name', 'last_name'], 'string', 'max' => $nameRequirements['maxLen'], 'min' => $nameRequirements['minLen'],];

            $rules[] = [['name', 'last_name'], 'match', 'pattern' => $nameRequirements['match'], 'message' => Yum::t($nameRequirements['dontMatchMessage'])];
        }


        /** Emali validation**/
        $rules[] = ['email', 'checkUnique'];
        $rules[] = ['email', 'exist', 'on' => 'forgetpass', 'targetAttribute' => 'email', 'message' => 'آدرس ایمیل وارد شده در سایت وجود ندارد  .'];


        /** date validation**/
        //$rules[] =  [['birthDate'], 'checkDateFormat'];



        /**
         * Numeric validation Utf8
         */

        $rules[] = array(['idNo','tel','mobile','nationalId'], 'match','pattern' => '/^([0 1 2 3 4 5 6 7 8 9 0 ]|[۰ ۱ ۲ ۳ ۴ ۵ ۶ ۷ ۸ ۹ ]|[٤ ٦ ٥])*$/','message' => ' فقط از کاراکترهای عددی استفاده شود.');



        /** Number validation**/
        $rules[] = array(['birthCityId','sportNo', 'creatorUserId', 'login_attemp_count', 'lock', 'valid', 'lang', 'active', 'fileId'], 'yii\validators\NumberValidator', 'integerOnly' => true,);

        /** String validation**/
        $rules[] = array(['univ', 'homepage', 'tel', 'mobile',], 'string', 'max' => Yum::module('YumUsers')->publicMaxString);

        /** Range validation**/
        $rules[] = array(['lock', 'valid'], 'in', 'range' => array(0, 1));
        $rules[] = array(['gender',], 'in', 'range' => array(self::FEMALE, self::MALE));


        /** Exist validation**/
       // $rules[] = ['id', 'exist', 'targetClass' => '\backend\models\City'];


        /** Safe validation**/

        $rules[] = array(['verifyCode'], 'safe',);
        /** Safe validation for save in db**/
        $rules[] = [['auth_key', 'password_hash', 'password_reset_token', 'lang'], 'safe',];
        $rules[] = [['fileAttr', 'sportNo', 'eName', 'eFamily', 'fatherName', 'idNo', 'nationalId', 'birthDate', 'birthCityId', 'fileId'], 'safe',];


        /**
         * file validation
         */
        $fileModel=new File;

        $rules[] = [['fileAttr'], 'file',
            'extensions' =>$this->fileTyps,
            'checkExtensionByMimeType' => true,
            'maxSize' => $this->fileSize,
            'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize(File::MAX_FILE_SIZE) . '  باشد',
            'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize(File::MIN_FILE_SIZE) . '  باشد',
            'minSize' => File::MIN_FILE_SIZE,
            'mimeTypes' =>$this->mimeFileTyps,

        ];
      //  die(var_dump($this->picFileExist()));

        if(!$this->picFileExist()&&$this->scenario=='UpdateCarvanSupervisor')
        {
            $rules[] = [['fileAttr'], 'file',
                'skipOnEmpty' => false,
                'on'=>['UpdateCarvanSupervisor']
            ];
        }

        $rules[] = [['fileAttr'], 'file',
            'skipOnEmpty' => false,
            'on'=>['createSoupervisorByExistUserDetail','createSoupervisorByExistUser','createCarvanSupervisor']
        ];

        $rules[] = array(['fileAttr'], 'required','on'=>['createSoupervisorByExistUserDetail','createSoupervisorByExistUser','createCarvanSupervisor']);
        $rules[] = array(['fileAttr'], 'safe',);

        /** other vlidations*/
        //  $rules[] = array('role', 'RolesTypeValidation',);
        return $rules;
    }


    public function getUsersDetails()
    {
        return $this->hasMany(UsersDetails::className(), ['usersId' => 'id'])
            ->andOnCondition(['users_details.valid' => 1]);
    }


    public function beforeSave($insert)
    {
        /*Creatuser */
        if (parent::beforeSave($insert)) {
            // $this->setIsNewRecord(0);

            return true;
        } else {
            return false;
        }
    }


    public function createSupervisorCarvanUser($carvanId = 0)
    {


        $eventId = Yii::$app->user->identity->eventId;

        $saveAttributes = $this->createSaveAttributes;

        if ($this->validate()) {
            /* Set nationalId as Default username And password*/
            if($this->scenario!='createSoupervisorByExistUserDetail'&&$this->scenario!='createSoupervisorByExistUser')
            {
                $this->username = isset($this->nationalId) ? $this->nationalId : '';
                $this->passwordInput = isset($this->mobile) ? $this->mobile : '';


                $this->setPassword($this->passwordInput);
                $this->generateAuthKey();

            }


            if($this->scenario=='createSoupervisorByExistUser'&&$this->scenario=='createSoupervisorByExistUserDetail')
            {
                $saveAttributes=$this->createExistSaveAttributes;
            }


            if ($result = $this->save(0,$saveAttributes)) {

                /*- Insert userdetail info in userDetail table*/
                try {

                    if (!Yii::$app->getAuthManager()->checkAccess($this->id, self::Role_CarvanSupervisor)) {
                        $this->addRole(self::Role_CarvanSupervisor);
                    }


                } catch (Exception $e) {
                    throw new HttpException(500, 'مشکل در افزودن نقش به کاربر پیش آمده است.');
                }


                /*- Insert userdetail info in userDetail table*/
                try {
                    if ($this->scenario != 'createSoupervisorByExistUserDetail') {
                        $this->insertUserDetailInfo($this->id, $eventId);
                    }
                } catch (Exception $e) {
                    throw new HttpException(500, 'مشکل در ذخیره اطلاعات پیش آمده است.');
                }


                /*- Insert Info in members event table*/
                try {

                    $this->insertMemberEventInfo($eventId, $carvanId);

                } catch (Exception $e) {
                    throw new HttpException(500, 'مشکل در ذخیره اطلاعات پیش آمده است.');
                }
                return TRUE;
            }
        }

        return false;
    }

    /*
     * insert info in eventMember table
     */
    public function insertMemberEventInfo($eventId, $carvanId)
    {

        $eventMemberObj = new EventMembers;
        $eventMemberObj->eventId = $eventId;
        $eventMemberObj->usersDetailsId = $this->userDetailId;
        $eventMemberObj->universityId = $this->userUniversityId;
        $eventMemberObj->caravanId = $carvanId;
        $eventMemberObj->rolename = self::Role_CarvanSupervisor;
        $eventMemberObj->user_type_id = UsersType::getUserTypeId('CarvanSupervisor');
        $eventMemberObj->userId = $this->id;

        return $eventMemberObj->save();
    }


    public function insertUserDetailInfo($userId, $eventId)
    {

        $userDetailObj = new UsersDetails();
        $userDetailObj->usersId = $userId;
        $userDetailObj->eventId = $eventId;

        $this->userDetailId = $userDetailObj->saveUniversityRespDetails();
        return $this->userDetailId;
    }


    public function updateSupervisorCarvanUser()
    {


        $eventId = Yii::$app->user->identity->eventId;

        if ($this->validate()) {

            //$this->username = isset($this->nationalId) ? $this->nationalId : '';
            //$this->passwordInput = isset($this->mobile) ? $this->mobile : '';
            //$this->setPassword($this->passwordInput);
            //$this->generateAuthKey();

            /*EventAdmin User just Update username before user login */

            if ($this->isUserLoginEver()) {
                /*dont save user if hacker set username by overposting*/
                $result = $this->save(0, ['name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'birthDate', 'nationalId']);
            } else {
                /*save username*/
                $result = $this->save(0, ['name', 'last_name', 'email', 'tel', 'mobile', 'gender', 'birthDate', 'nationalId']);
            }
            return $result;
        }

        return false;
    }


    /*
     * diferent from createUser and modiy user:in modiy user dont call add role
     * that prevent integrity error db in auth_assignment in yii
     */

    public function modifyAthleteUser()
    {
        if ($this->validate()) {

            /*-Set nationalId as Default username And password-*/
            $this->username = isset($this->nationalId) ? $this->nationalId : '';
            $this->passwordInput = isset($this->nationalId) ? $this->nationalId : '';

            $this->setPassword($this->passwordInput);
            $this->generateAuthKey();
            if ($result = $this->save(0)) {

                return TRUE;
            }
        }

        return null;
    }

    public function addRole($role)
    {

        $auth = Yii::$app->authManager;
        $Role = $auth->getRole($role);
        $auth->assign($Role, $this->id);
    }


    public function getAuthassignment()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }



    public function deleteCaravanSupervisor($caravanId)
    {
        $eventId = Yii::$app->user->identity->eventId;

        $this->userEventMemArr=EventMembers::find()->Users($this->id)->all();
        $count=count($this->userEventMemArr);
        if($count==1){

            return $this->deleteUserByOneEventMem();


        }elseif($count>1)
        {
            return $this->deleteUserBySeveralEventMem($caravanId);

        }

    }


    public function deleteUserByOneEventMem()
    {
        $eventMemObj=$this->userEventMemArr[0];
        $eventId = Yii::$app->user->identity->eventId;
        $userDetailId=$eventMemObj->usersDetailsId;

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();


        try {

            /** delete EventmemberRow in EventMember table**/
            $result3=EventMembers::deleteAll('universityId=:universityId AND eventId = :eventId AND rolename=:role AND usersDetailsId=:userDetail', [':universityId'=>$eventMemObj->universityId,':eventId' => (int)$eventId,':role' => self::Role_CarvanSupervisor,':userDetail'=>$userDetailId]);

            /** delete responsed role in authitemtable**/
            $authAssignObj=new AuthAssignment();
            $resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_CarvanSupervisor.'" And event_id='.(int)$eventId).'';


            /** Delete User Record if one userdetail*/
            parent::beforeDelete();
            try {
                $result=\Yii::$app->db->createCommand( "UPDATE " . $this->tableName() . " SET valid=0 WHERE id=".(int)$this->id)->execute();
            }
            catch (\Exception $ex)
            {
                return false;
            }

            /*Start Delete UserDetails Record*/
            $userDetailsObj=new UsersDetails();
            $userDetailsObj->id=$userDetailId;

            parent::beforeDelete();

            try {
                $resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

            }
            catch (\Exception $ex) {
                return false;
            }


            $transaction->commit();

            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
            throw $e;
        }
    }


    public function deleteUserBySeveralEventMem($caravanId)
    {
        $countUniversityRespPosition=0;
        $countEventType=0;

        $eventId = Yii::$app->user->identity->eventId;

        /*get target event mem for this university universityResp role*/
        foreach ($this->userEventMemArr as $i=>$eventObj)
        {
            if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_CarvanSupervisor&& $eventObj->caravanId==$caravanId )
                $eventMemObj=$eventObj;

            /*count  position of Role_UniversityRepresentative for user  */
            if($eventObj->eventId==$eventId&&$eventObj->rolename==self::Role_CarvanSupervisor)
                $countUniversityRespPosition++;

            /*count of user $countEventType in this event*/
            if($eventObj->eventId==$eventId)
                $countEventType++;
        }



        $userDetailId=$eventMemObj->usersDetailsId;
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            /** delete EventmemberRow in EventMember table**/
            $result3=$eventMemObj->delete();


            /** delete responsed role in authitemtable if just have one university resp position**/
            if($countUniversityRespPosition==1)
            {
                $authAssignObj=new AuthAssignment();
                $resultRole=$authAssignObj->deleteAll('user_id ='.(int)$this->id.' AND item_name="'.self::Role_CarvanSupervisor.'" And event_id='.(int)$eventId).'';
            }


            if($countEventType==1)
            {

                /*Start Delete UserDetails Record*/
                $userDetailsObj=new UsersDetails();
                $userDetailsObj->id=$userDetailId;

                parent::beforeDelete();

                try {
                    $resultUserDetails=\Yii::$app->db->createCommand( "UPDATE " . $userDetailsObj->tableName() . " SET valid=0 WHERE id=".(int)$userDetailsObj->id)->execute();

                }
                catch (\Exception $ex) {
                    return false;
                }
            }


            $transaction->commit();

            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
            throw $e;
        }

    }


    public function uploadPicFile($fileInstance=null,$skipOnEmpty=false)
    {
       // die(var_dump($skipOnEmpty));
        $fileModel=new File;
        $fileModel->scenario=  File::MODEL_FILE_UPLOAD_SCENARIO;
        $fileModel->modelFileTyps=$this->fileTyps;
        $fileModel->modelMimeFileTyps=$this->mimeFileTyps;
        $fileModel->modelFileskipOnEmpty=$skipOnEmpty;
        $fileModel->modelFileSize=$this->fileSize;



        /** Add icon file**/

        $fileModel->folder_id = Folders::USERS_PIC_FOLDER_ID;
        $fileModel->model_id =$this->id;
        $fileModel->model_type ='user';

        $fileModel->temporaryFileAttr =  \yii\web\UploadedFile::getInstance($this, 'fileAttr');

        //if ($fileModel->temporaryFileAttr!==null) {

        if ($fileModel->validate()&&$fileModel->temporaryFileAttr!==null && $fileModel->uploadModelFile('user_' . $this->id)) {


            /* remove old file in edit action call*/
            //die(var_dump($this->fileId));
            if (isset($this->fileId)) {
                $oldFileId = $this->fileId;
                try{
                    if (($oldFileModel = FileAction::findOne($oldFileId)) !== null) {
                        $oldFileModel->removeFile($oldFileId);
                    }
                }catch(Exception $e)
                {
                    throw new NotFoundHttpException( "Problem in find old File" );
                }

            }

            /*save file id in categorie table */
            $this->fileId = (int)$fileModel->id;
            return $this->update(0);

        } else {
            if (count($fileModel->errors) > 0) {
                $msg = '';
                foreach ($fileModel->errors as $attribute => $errors) {
                    foreach ($errors as $error) {
                        $msg .= $error;
                    }
                }
                $this->addError('fileAttr', $msg);

            }

            return false;
        }
        //}
        return false;
    }



}
