<?php

namespace backend\modules\YumUsers\models;

use Yii;

/**
 * This is the model class for table "users_type".
 *
 * @property integer $id
 * @property integer $parentId
 * @property string $uniqueName
 * @property string $title
 * @property string $etitle
 * @property integer $valid
 */
class UsersType extends \yii\db\ActiveRecord
{

	/* SPORT EVent User type*/

	const Type_Admin='admin';
	const Type_Athlete='Athlete';
	const Type_reader='reader';
	const Type_Referee='Referee';
	const Type_CarvanSupervisor='CarvanSupervisor';
	const Type_ResponsibleAccommodation='ResponsibleAccommodation';
	const Type_ResponsibleFinance='ResponsibleFinance';
	const Type_ResponsibleNutrition='ResponsibleNutrition';
	const Type_ResponsiblePlanningSportsHall='ResponsiblePlanningSportsHall';
	const Type_ResponsibleRecordResults='ResponsibleRecordResults';
	const Type_UniversityRepresentative='UniversityRepresentative';
	const Type_ResponsibleTransport='ResponsibleTransport';
	const Type_SeniorMemberSystem='SeniorMemberSystem';
	const Type_SeniorUserEvent='SeniorUserEvent';
	const Type_TechnicalCommitteeAndSportsAssociation='TechnicalCommitteeAndSportsAssociation';
	//coach Type
	const Type_Coach='Coach';
	const Type_CoachHead='CoachHead';
	const Type_CoachAssistant='CoachAssistant';
	const TeamSupervisor='TeamSupervisor';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%users_type}}';
	}

	/**
	 * @inheritdoc
	 * @return UsersTypeQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new UsersTypeQuery(get_called_class());
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['parentId', 'valid'], 'integer'],
			[['uniqueName', 'title', 'etitle'], 'required'],
			[['uniqueName', 'title', 'etitle'], 'string', 'max' => 100]
		];
	}

	public function getParent()
	{
		return $this->hasOne(UsersType::className(), ['id' => 'parentId']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'parentId' => Yii::t('app', 'Parent ID'),
			'uniqueName' => Yii::t('app', 'Unique Name'),
			'title' => Yii::t('app', 'Title'),
			'etitle' => Yii::t('app', 'Etitle'),
			'valid' => Yii::t('app', 'Valid'),
		];

	}

	public function getParentName()
	{
		return ($this->parentId == 0 ? Yii::t('app', 'Main Group') : $this->parent->title);
	}

	public static  function getUserTypeId($userType)
	{
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => $userType])->one();

		if($usersTypeModel !==null)
			return  $usersTypeModel->id;

		return 0;
	}
	public static  function getUserTypeTitle($userTypeId)
	{
		$usersTypeModel=UsersType::find()->where('id=:id ',
			[':id' => $userTypeId])->one();

		if($usersTypeModel !==null)
			return  $usersTypeModel->title;

		return 0;


	}

	public static function getCouchType()
	{
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => 'CoachingStaff'])->one();

		if($usersTypeModel !==null)
			$CoachingStaffId=$usersTypeModel->id;

		return $coachTypeModels=UsersType::find()->where('parentId=:parentId ',
			[':parentId' => $CoachingStaffId])->all();
	}



	public static function getJustCouchType()
	{
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => 'CoachingStaff'])->one();

		if($usersTypeModel !==null)
			$CoachingStaffId=$usersTypeModel->id;
		$result=$coachTypeModels=UsersType::find()->where('parentId=:parentId AND id<>:teamSupervisorId',
			[':parentId' => $CoachingStaffId,':teamSupervisorId'=>19])->all();
		
		return $result;
	}

	public static function getTechnicalCommitteeType()
	{
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => 'TechnicalCommittee'])->one();

		if($usersTypeModel !==null)
			$technicalCommitteeId=$usersTypeModel->id;

		return $coachTypeModels=UsersType::find()->where('parentId=:parentId ',
			[':parentId' => $technicalCommitteeId])->all();
	}

	public static function getExecutiveCommitteeType()
	{
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => 'ExecutiveCommittee'])->one();

		if($usersTypeModel !==null)
			$technicalCommitteeId=$usersTypeModel->id;

		return $coachTypeModels=UsersType::find()->where('parentId=:parentId ',
			[':parentId' => $technicalCommitteeId])->all();
	}

	public static function getMiscCaravanType()
	{
		$misc4CaravanId=-1;
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => 'Misc4Caravan'])->one();

		if($usersTypeModel !==null)
			$misc4CaravanId=$usersTypeModel->id;
		return $miscTypeModels=UsersType::find()->where('parentId=:parentId ',
			[':parentId' => $misc4CaravanId])->all();
	}
/*all misc caravan expect attendant users*/
	public static function justGetMiscCaravanType()
	{
		$misc4CaravanId=-1;
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => 'Misc4Caravan'])->one();

		if($usersTypeModel !==null)
			$misc4CaravanId=$usersTypeModel->id;
		return $miscTypeModels=UsersType::find()->where('parentId=:parentId AND id<>:caravanAttendantId ',
			[':parentId' => $misc4CaravanId,':caravanAttendantId'=>26])->all();
	}


	public static function getTypeIds($type)
	{
		$parentId=-1;
		$usersTypeModel=UsersType::find()->where('uniqueName=:uniqueName ',
			[':uniqueName' => $type])->one();

		if($usersTypeModel !==null)
			$parentId=$usersTypeModel->id;

		$userTypesArr= $miscTypeModels=UsersType::find()->where('parentId=:parentId ',
			[':parentId' => $parentId])->all();
		$typeIds=[];
		foreach ($userTypesArr as  $typeRow) {
			$typeIds[] = $typeRow->id;
		}
		//die(var_dump($typeIds));

		return $typeIds;

	}
}

/**
 * This is the ActiveQuery class for [[UsersType]].
 *
 * @see UsersType
 */
class UsersTypeQuery extends \yii\db\ActiveQuery
{
	/**
	 * @return UsersType[]|array
	 */
	public function noroot()
	{
		return $this->andWhere(['NOT IN' , 'id', UsersType::find()->select(['DISTINCT(`parentId`)'])]);
	}
}