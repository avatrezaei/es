<?php

return array(
'ID' => 'شناسه',
'Username' => 'نام کاربری',
 'Password' => 'کلمه عبور',
 'passwordInput' => 'کلمه عبور',
 'Salt' => 'Salt',
 'Name' => 'نام',
 'Last Name' => 'نام خانوادگی',
 'Office' => 'سمت',
 'Gender' => 'جنسیت',
 'Age' => 'سن',
 'Grade' => 'مدرک تحصیلی',
 'Edu' => 'رشته تحصیلی',
 'Univercity' => 'دانشگاه',
 'Homepage' => 'صفحه خانگی',
 'Tel' => 'تلفن',
 'Mobile' => 'موبایل',
 'Email' => 'پست الکترونیکی',
 'Type Assist' => 'نوع همکاری',
 'Organization' => 'سازمان',
 'Login Attemp Count' => '',
 'Update Date' => 'تاریخ ویرایش',
 'Update At' => 'تاریخ ویرایش',
 'Create Date' => 'تاریخ ایجاد ',
 'Create At' => 'تاریخ ایجاد ',
 'Last Login Time' => 'زمان آخرین ورود',
 'Last Login Attemp Time' => 'زمان آخرین تلاش ورود',
 'Last Action' => 'آخرین فعالیت',
 'Lock' => 'مسدود',
 'Active' => 'فعال',
 'Valid' => 'معتبر',
 'Role'=>'سطح دسترسی',
 'Retype your new password'=>'تکرار کلمه عبور ',
 'Your current password'=>'  کلمه عبور فعلی',
   'Current Password'=>'  کلمه عبور فعلی',
	'password'=> 'کلمه عبور',
	'Language'=> 'زبان',
		'Lang' => 'زبان',
	'Remember Me'=>'مرا به خاطر بسپار',
		'Input New Password'=>'کلمه عبور جدید',
	'Confirm Password'=>'تایید کلمه عبور جدید',
		'Input Old Password'=>'کلمه عبور قبلی',
	'Captcha'=>'تصویر امنیتی',
	'Admin Password Input'=>' کلمه عبور مدیر',
'Target User'=>'کاربر '


);
