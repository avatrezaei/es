<?php

return array(
'Create' => 'ایجاد',
 'Save' => 'ذخیره',
 'Delete' => 'حذف',
 'Login' => 'ورود',
	'Reset PassWord'=>'بازنشانی کلمه عبور',
	'Search'=>'جستجو',
		'Yes'=>'بلی',
	'No'=>'خیر',
 );
