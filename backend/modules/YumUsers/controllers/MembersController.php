<?php

namespace backend\modules\YumUsers\controllers;


use Yii;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use \backend\modules\YumUsers\models\ChangePassword;


/**
 * UserController implements the CRUD actions for User model.
 */
class MembersController extends Controller
{

	/* ID for Access control */
	public $currentEventId;
	public $currentUserDetailId;
	public $currentEventName;
	public $userId;

	public $_eventObj;
	public $_carvanObj;
	public $_teamObj;
	public $_universityObj;
	public $_authorizedFieldObj;
	public $_universityRespUserObj;
	public $_universityId;

   public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
						[
								'allow' => true,
								'actions' => ['profileinfo','update-user'],
								'roles' => ['@'],
						],
					 [
						'allow' => true,
						'actions' => ['profileinfo','updateinfo','update-user'],
						'roles' => ['admin','SeniorUserEvent', User::Role_UniversityRepresentative,'Athlete','Referee'],
					],
				],
			],
		];
	}
  
public function provideSemanticAccessControlData($value='')
	{
		$session = Yii::$app->session;
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$eventId=Yii::$app->user->identity->eventId; 

		/**
		 * Get Current Event Id Selected User
		 */
		if (!empty($eventId)){
			$this->currentEventId=$session->get('eventId');
		}
		else{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}

		$userDetailId=Yii::$app->user->identity->getUserDetailIdBaseOnEventIdUserId($eventId,$userId);
		/**
		 * Get Current User Detail Id
		 */
		if (!empty($userDetailId)){
			$this->currentUserDetailId=$userDetailId;
		}
		else{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error222'));
		}
	}

	public function actionProfileinfo() {
		$message = '';
		
		/*-----View Info Code------*/
		$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		if (!$curentUserId) {
			throw new \yii\web\HttpException(400, Yii::t('app','You are not authorized to per-form this action.you just can view your Information'));
		}
		$userModel = $this->findModel($curentUserId);

		return $this->render('manage_profile_info', array(
					'model' => $userModel ,
				   // 'loginModel' => $loginModel,
					'message' => $message,
		));
	}



	public function actionUpdateUser($userId) {
	$message='';
		//$this->provideSemanticAccessControlData();

 /*-----View Info Code------*/
		$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		if (!$curentUserId) {
			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));
		}
		$userId= $curentUserId;

 
	$model=$this->findModel($userId);


	$userDetails=$model->getUserDetailBaseOnEvent($this->currentEventId);  
	$model->scenario='UpdateProfileInfo';  
		
		
		
	/**-------Update Personal info-------**/
	if ($model->load(Yii::$app->request->post())&&isset($_POST['update_prsonal_info_btn']) && $model->save()) { 

					$message=$this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]) );
	}
	/**----------------------------------**/
		
		
	/**--------Update Picture -----------**/
	if ($model->load(Yii::$app->request->post())&&isset($_POST['update_pic_btn'])) { 
			if($model->uploadPicFile()) 
				{
					$message=$this->createMessageCss('success',Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]));

				}
	}
	/**-----------------------------------**/	   
	  
	/**-----------------------------changePassword ------------------------------------------------**/
	$changePasswordModel = new ChangePassword(['scenario'=>'change_password_byadmin']);
	$changePasswordModel->username = Yii::$app->user->identity->username;
	$changePasswordModel->passwordInput=  isset($_POST['ChangePassword']['passwordInput'])?$_POST['ChangePassword']['passwordInput']:'';
	$changePasswordModel->targetUser = (int) $userId;

	if ($changePasswordModel->load(Yii::$app->request->post())&&isset($_POST['change_pass_btn'])) { 


		if ($changePasswordModel->load(Yii::$app->request->post()) ) {
			/*---check Password----*/
			if ($resultValidate=$changePasswordModel->validate() && $resultVerifyPass=$changePasswordModel->ValidateCurrentPassword())
			{
				if ($changePasswordModel->changePassword())
				{
					$message=$this->createMessageCss('success',Yii::t('app', 'Changed Password Successfully .'));

				}
				else{
					$message=$this->createMessageCss('error',Yii::t('app', 'Error in changed password!'));
				}
			}
			elseif (!$resultValidate||!$resultVerifyPass)
			{
					$message=$this->createMessageCss('error',Yii::t('app', 'Error in changed password!'));
			}
		}

	}
	/**-----------------------------------------------------------------------------------------------**/  




/*
 * if user didn't select event userdetail can not be update
 */
	/**-------Update User Detail Personal info-------**/
		if(!empty($userDetails)) {
			$userDetails->scenario = 'UpdateUserDetailse';
			if ($userDetails->load(Yii::$app->request->post()) && isset($_POST['update_user_detail_info_btn']) && $userDetails->save()) {

				try {
					$uploadStuCartResult = $userDetails->uploadFileStuCart();
				} catch (Exception $e) {
					throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن کارت دانشجویی پیش آمده است.');
				}

				try {
					$uploadStuAssuranceCartResult = $userDetails->uploadFileAssuranceCart();
				} catch (Exception $e) {
					throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن  کارت یمه پیش آمده است.');
				}

				$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));


			}
		}
	/**--------------------------------------------**/

		

		return $this->render('update_user', [
					'model' =>$model,
					'massage' => $message,
					'userDetails'=>$userDetails,
					'changePasswordModel'=>$changePasswordModel

		]);
	}
   









	

			public function createMessageCss($msgType,$string)
   {
 
	   switch ($msgType) {
		   case 'success':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
					break;

		   case 'danger':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
					break;		 
		   default:
				 $message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
			   break;
	   }

	   return $message;
	   
   }


   public  function setFlashMsg($name,$message)
   {
				$session = Yii::$app->session;
				$session->setFlash($name, $message);
   }




	 public function actionProfileinfo_old() {
		$message = '';
		
		/*-----View Info Code------*/
		$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		if (!$curentUserId) {
			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));
		}
		$userModel = $this->findModel($curentUserId);

		/*-----Change Password Code------*/
		$model = ChangePassword::findOne($curentUserId);
		$model->scenario = 'change_user_password';

		if ($model == null) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model->username = Yii::$app->user->identity->username;
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			/*---check Old Password----*/
			if ($model->ValidateOldPassword()) {
				if ($model->changePassword($curentUserId)) {
					$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
					<a href="#" class="alert-link">' . Yii::t('app', 'Changed Password Successfully .') . '</a></div>';
				} else {
				   $message = '<div class="alert alert-danger" role="alert" id="form_create_msg">
					<a href="#" class="alert-link">' . Yii::t('app', 'change password fails.') . '</a></div>';				}
			} 
		}

		return $this->render('manage_profile_info', array(
					'userModel' => $this->findModel($curentUserId),
					'model' => $model,
				   // 'loginModel' => $loginModel,
					'message' => $message,
		));
	}

   

 

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
