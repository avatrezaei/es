<?php


namespace backend\modules\YumUsers\controllers;


use Yii;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use \backend\modules\YumUsers\models\ChangePassword;
use yii\web\Session;
use yii\web\Response;
use backend\modules\YumUsers\models\Role;
use \backend\models\UserSetting;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserRoleController extends Controller
{

   public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [

//					 [
//						'allow' => true,
//						'actions' => ['ChangeUsersettingByadmin','changepasswordbyadmin','verifyadmin','changepassbyadmin','changerolebyadmin'],
//						'roles' => ['admin'],
//					],
				],
			],
		];
	}

 

	protected function findUserModel($id) {
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

   
//		public function actionChangerolebyadmin($usertargetid) {
//		$message = '';
//		$model = new Role;
//		$model->username = Yii::$app->user->identity->username;
//		$model->RoleInputArray = isset($_POST['Role']['RoleInputArray'])?$_POST['Role']['RoleInputArray']:array();
//		$model->targetUser = (int) $usertargetid;
//
//		if ($model->load(Yii::$app->request->post())) {
//			/* ---check Roles---- */
//		   if ($model->validateInput()) {
//		 //  if (1) {
//
//				if ($model->changeRole($model->RoleInputArray, $model->targetUser)) {
//					$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
//					<a href="#" class="alert-link">' . Yii::t('app', 'Changed Role Successfully .') . '</a>
//					</div>';
//					  $session = Yii::$app->session;
//			$session->setFlash('change_password_success', $message);
//					return $this->redirect(['/YumUsers/user/index', 'userTargetId' => $model->targetUser]);
//				}else{
//				Yii::$app->response->format = Response::FORMAT_JSON;
//
//				return [
//					'forceReload' => 'true',
//					'title' => 'test',
//					'content' => $this->render('change_role_admin', [
//						'model' => $model,
//					]),
//				];
//			}
//			} else {
//				 Yii::$app->response->format = Response::FORMAT_JSON;
//
//				return [
//					'forceReload' => 'true',
//					'title' => 'test',
//					'content' => $this->render('change_role_admin', [
//						'model' => $model,
//					]),
//				];
//			}
//		}
//
//		return $this->render('change_role_admin', [
//					'model' => $model,
//					'userTargetId' => isset($model->targetUser) ? $model->targetUser : 0,
//		]);
//	}

	
	
	
	
	function Rbac()
	{
//$auth = new \yii\rbac\DbManager;
//$auth->init();
//$role0 = $auth->createRole('admin');
//$auth->add($role0);
//$role = $auth->createRole('sub_admin');
//$auth->add($role);
//$role1 = $auth->createRole('teacher');
//$auth->add($role1);
//$role2 = $auth->createRole('student');
//$auth->add($role2);
//$role3 = $auth->createRole('staff');
//$auth->add($role3);
//$role4 = $auth->createRole('other');
//$auth->add($role4);
//$role5 = $auth->createRole('reader');
//$auth->add($role5);
//
//$auth->addChild($role0, $role);
//
//
//$auth->addChild($role, $role1);
//$auth->addChild($role, $role2);
//$auth->addChild($role, $role3);
//$auth->addChild($role, $role4);
//$auth->addChild($role, $role5);
//
//
//$auth->addChild($role1, $role5);
//$auth->addChild($role2, $role5);
//$auth->addChild($role3, $role5);
//$auth->addChild($role4, $role5);

//$auth->assign($role, $user_id);
	}
}
