<?php

namespace backend\modules\YumUsers\controllers;

use yii\helpers\ArrayHelper;
use backend\models\Fields;
use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YumUsers\models\ChangePassword;
use backend\modules\YumUsers\models\TechnicalCommitteeUser;
use backend\modules\YumUsers\models\TechnicalCommitteeUserSearch;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UsersDetails;
use backend\modules\YumUsers\models\UsersType;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * UserController implements the CRUD actions for User model.
 */
class TechnicalCommitteeController extends Controller
{
	public $_model;
	public $_fileModel;
	public $_fileAttribute;
	public $_userDetails;


	/*Access control properties*/
	public $currentEventId;
	public $currentEventName;
	public $userId;

	/*Create user properties*/
	public $message;
	public $failMessage;
	public $saveTypeUser = 0;//related to create by duplicate nationalid

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
					[
						'actions' => ['delete'],
						'roles' => ['deleteTechnicalCommittee'],
						'allow' => true,
					],
					[
						'actions' => ['create'],
						'roles' => ['createTechnicalCommittee'],
						'allow' => true,
					],
					[
						'actions' => ['index'],
						'roles' => ['adminTechnicalCommittee'],
						'allow' => true,
					],
					[
						'actions' => ['update-referee'],
						'roles' => ['updateTechnicalCommittee'],
						'allow' => true,
					],
					[
						'actions' => ['view'],
						'roles' => ['viewTechnicalCommittee'],
						'allow' => true,
					],
					[
						'allow' => true,
						'actions' => ['create', 'index', 'update-referee', 'delete','view'],
						'roles' => ['ManageTechnicalCommittee'],
					],

				],
			],
		];
	}

	public function provideSemanticAccessControlData($value='')
	{

		$eventId=Yii::$app->user->identity->eventId;


		/*-------------Get Current Event Id Selected User----------------------------*/
		if (!empty( $eventId) ) {
			$this->currentEventId=$eventId ;
		}else
		{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}

	}

	public function actionIndex()
	{
		/*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();


		$request = Yii::$app->request;
		$searchModel = new TechnicalCommitteeUserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->currentEventId);

		$columns = [
			['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'fileId',
				'header' => Yii::t('app', 'Picture'),
				'enableSorting' => false,
				'format' => 'raw',
				'filter' => false,
				'value' => function ($model) {
					return $model->getIconThumbnail($model->fileId);

				},
			],

			[
				'attribute' => 'last_name',
				'filter' => false,
				'header' => Yii::t('app', 'Full Name'),
				'enableSorting' => false,
				'format' => 'html',
				'value' => function ($model) {
					return Html::a(
						Html::encode($model->FullName),
						Url::to(['/YumUsers/technical-committee/view', 'id' => $model->id])
					);
				},
			],
			[
				'attribute' => 'username',
				'enableSorting' => false,
				'filter' => false,

			],
			[
				'attribute' => 'mobile',
				'enableSorting' => false,
				'filter' => false,

			],
			[
				'attribute' => 'gender',
				'format' => 'raw',
				'filter' => false,
				'enableSorting' => false,
				'value' => function ($model) {
					return $model->Sex;
				},
			],
			[
				'header' => Yii::t('app', 'Field'),
				'attribute' => 'userField',
				'enableSorting' => false,
				'filter' =>ArrayHelper::map(Fields::find()->all(), 'id', 'name'),
				'filterOptions' => ['id' => 'technical-committee-user-field'],
				'value' => function ($model) {
					$userDetailObj=$model->getUserDetailBaseOnEvent(Yii::$app->user->identity->eventId);
					if($userDetailObj!==null)
						$_field = Html::encode($userDetailObj->getSportFieldNameText());
					else
						$_field = '<i class="fa fa-exclamation-circle font-blue"></i>';

					return $_field;
				},
			],

			[
				'header' => Yii::t('app', 'User Title'),
				'attribute' => 'userType',
				'filter' =>TechnicalCommitteeUser::getUserTechnicalCommiteeType(),
				'filterOptions' => ['id' => 'technical-committee-user-type'],
				'format' => 'raw',
				'enableSorting' => false,
				'value' => function ($model){
					/*use joined data to show user type txt*/
				     return $model->getUserTypeTxt();
				},
			],

			[
				'attribute' => 'last_login_time',
				'format' => 'raw',
				'filter' => false,
				'enableSorting' => false,
				'value' => function ($model) {
					return $model->LastLogintime;
				},
			],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '140'],
				'template' => '{changepassbyadmin} {sendMessage} {view} {update} {delete}',
				'buttons' => [
					'changepassbyadmin' => function ($url, $model) {
						return Html::a(
							'<span class="glyphicon glyphicon-lock font-purple-plum"></span>',
							Url::to(['/YumUsers/admin/changepasswordbyadmin', 'usertargetid' => $model->id]),
							[
								'data-hint' => Yii::t('app', 'Change Password'),
								'class' => 'hint--top hint--rounded hint--info'
								// 'class' => 'showModalButton btn btn-success',
								// 'style'=>'padding:1px;'
							]
						);
					},
					'update' => function ($url, $model) {
						return Html::a(
							'<span class="glyphicon glyphicon-pencil font-yellow"></span>',
							Url::to(['/YumUsers/technical-committee/update-referee', 'userId' => $model->id]),
							[
								'data-hint' => Yii::t('app', 'Update'),
								'class' => 'hint--top hint--rounded hint--info'
								// 'class' => 'showModalButton btn btn-success',
								// 'style'=>'padding:1px;'
							]
						);
					},
					'sendMessage' => function ($url, $model){
						return Html::a(
							'<i class="fa fa-envelope font-yellow"></i>',
							Url::to(['/messages/send', 'id' => $model->id]),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Send Message'),
							]
						);
					},
				],
				'visibleButtons' => [
					'view' => \Yii::$app->user->can('viewTechnicalCommittee'),
					'delete' => \Yii::$app->user->can('deleteTechnicalCommittee'),
					'update' => \Yii::$app->user->can('updateTechnicalCommittee'),
					'sendMessage' => \Yii::$app->user->can('sendMessages'),
					'changepassbyadmin' => \Yii::$app->user->can('admin'),
				]
			],
		];

		$clientOptions = [
			'ajax' => [
				'url' => $request->url,
			],
			'order' => [
				[2, 'desc']
			],
			'columnDefs'=> [
				[
					'orderable' => false,
					'targets' => [0, 1, 6, 7, count($columns) - 1]
				]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = array(NULL, NULL, 'last_name', 'username', 'mobile', 'gender', NULL, NULL, 'last_login_time');
		$searchableColumn = array('event_members.user_type_id','name', 'last_name', 'username', 'mobile', 'email', 'sportNo', 'eName', 'eFamily', 'fatherName', 'idNo', 'nationalId');

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				//'filterModel' => NULL,
				'filterModel' => $searchModel,
				'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);
//		echo '<pre>';
//		die(var_dump($dataProvider->models));
		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		} else {
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}


	public function actionCreate()
	{

		/*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();

		$this->message = '';
		$saveTypeUser = 0;
		$this->failMessage = '';
		$this->_userDetails = new UsersDetails();
		$this->_model = new TechnicalCommitteeUser();

		$this->_model->scenario = 'createTechnicalCommitteeUser';
		$this->_userDetails->scenario = 'CreateTechCommiteeDetailse';


		if (isset($_POST['TechnicalCommitteeUser']['nationalId'])) {
			$nationalId = $_POST['TechnicalCommitteeUser']['nationalId'];
			$userModel = TechnicalCommitteeUser::find()->where('nationalId= :nationalId AND valid=1 ', [':nationalId' => $nationalId])
				->one();
			if ($userModel != null) {
				$userDetails = $userModel->getUserDetailBaseOnEvent(Yii::$app->user->identity->eventId);
				if ($userDetails != null) {
					$saveTypeUser = TechnicalCommitteeUser::CREATE_EXIST_USER_EVENT;
					$this->_model = $userModel;
					$this->_userDetails = $userDetails;


				} else {
					$saveTypeUser = TechnicalCommitteeUser::CREATE_NEW_USER_EVENT;
					$this->_model = $userModel;

				}
			} else {

				$saveTypeUser = TechnicalCommitteeUser::CREATE_NEW_USER;
			}
		}

		switch ($saveTypeUser) {


			case TechnicalCommitteeUser::CREATE_NEW_USER:
				$this->_model->scenario = 'createTechnicalCommitteeUser';

				if ($this->createNewTechCommit($saveTypeUser)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/technical-committee']);
				}
				break;


			case TechnicalCommitteeUser::CREATE_NEW_USER_EVENT:

				$this->_model->scenario = 'createtechCommiteByExistUser';

				if ($this->createNewTechCommit($saveTypeUser)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/technical-committee']);
				}
				break;


			case TechnicalCommitteeUser::CREATE_EXIST_USER_EVENT:
				$this->_model->userType=$_POST['TechnicalCommitteeUser']['userType'];

				if ($this->_model->isDuplicateEventUser(0, $this->_userDetails->id, 0, $this->_model->userType, 0,0,0)) {
					$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item} Already exsist.', array('item' => Yii::t('app', 'User'))));
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/technical-committee']);
					break;
				}

				$this->_model->scenario = 'createtechCommiteByExistUser';

				if ($this->createNewTechCommit($saveTypeUser)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/technical-committee']);
				}
				break;


			default:
				break;
		}


		return $this->render('create_referee', [
			'model' => $this->_model,
			'message' => $this->message,
			'userDetails' => $this->_userDetails,

		]);
	}


	public function createNewTechCommit($saveTypeUser)
	{
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			/*---BEGIN Transaction---*/

			/*--Save new createNewTechCommit--*/
			if ($this->_model->load(Yii::$app->request->post())) {

				//set for validate
				$this->_model->fileAttr = \yii\web\UploadedFile::getInstance($this->_model, 'fileAttr');

				/*-Set nationalId as Default username And password-*/
				if ($this->_model->scenario != 'createtechCommiteByExistUser') {
					$this->_model->username = isset($this->_model->nationalId) ? $this->_model->nationalId : '';
				}

				$loadDetailUser = $this->_userDetails->load(Yii::$app->request->post());
				/*set event id  that provided from session .set admin user event id for user event*/
				$this->_userDetails->eventId = $this->currentEventId;


				/*set useDetail Scenario*/
				if (($this->_model->userType == UsersType::getUserTypeId('Referee')||$this->_model->userType == UsersType::getUserTypeId('SupervisorOfReferee')) && $saveTypeUser == TechnicalCommitteeUser::CREATE_NEW_USER) {
					$this->_userDetails->scenario = 'CreateRefereeUserDetailse';
				} else if (($this->_model->userType != UsersType::getUserTypeId('Referee')||$this->_model->userType != UsersType::getUserTypeId('SupervisorOfReferee')) && $saveTypeUser == TechnicalCommitteeUser::CREATE_NEW_USER) {
					$this->_userDetails->scenario = 'CreateTechCommiteeDetailse';

				} else if (($this->_model->userType == UsersType::getUserTypeId('Referee')||$this->_model->userType == UsersType::getUserTypeId('SupervisorOfReferee'))  && $saveTypeUser != TechnicalCommitteeUser::CREATE_NEW_USER) {
					$this->_userDetails->scenario = 'UpdateRefereeUserDetailse';

				} else if (($this->_model->userType != UsersType::getUserTypeId('Referee')||$this->_model->userType != UsersType::getUserTypeId('SupervisorOfReferee'))  && $saveTypeUser != TechnicalCommitteeUser::CREATE_NEW_USER) {
					$this->_userDetails->scenario = 'UpdateCommiteeUserDetailse';

				}


				if ($this->_userDetails->validate(['eventId', 'sportFieldId','refereeDegree']) && $this->_model->validate('fileAttr') && $this->_model->createTechnicalCommitteeUser()) {

					if ($loadDetailUser) {


						/*-if Upload file Save Pic  User athlete add in team-*/
						//  if (1) {
						if ($this->_model->uploadPicFile()) {
							$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
						} else {
							$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item} Successfully Added User expect File.', array('item' => Yii::t('app', 'User'))));

						}


						if ($this->_userDetails->saveTechCommitDetails($this->_model->id)) {

							if ($this->_model->insertMemberEventInfo($this->_userDetails->id, $this->currentEventId, 0, 0, 0)) {
								$transaction->commit();
								return true;
							} else {
								throw new NotFoundHttpException("Problem in Add User Team info");


							}

						} else {
							  //die(var_dump( $this->_userDetails->errors));
							throw new NotFoundHttpException("Problem in Add User Details");


						}


					}

				}
			}


		} catch (Exception $e) {
			$transaction->rollback();
			$this->message .= $this->createMessageCss('success', Yii::t('app', '{item}  Fail to create .Try again.', array('item' => Yii::t('app', 'User'))));
			return false;
		}


	}




	//
	// public function actionCreate()
	// {
	//
	//	/*-----set EventId,EventName From Session----*/
	//	$this->provideSemanticAccessControlData();
	//
	// 	$message = '';
	// 	$failMessage = '';
	// 	$this->_userDetails = new UsersDetails();
	// 	$this->_model = new TechnicalCommitteeUser();
	//
	// 	$this->_model->scenario='createTechnicalCommitteeUser';
	//
	//
	// 	/*******************-------BEGIN Transaction----------*****************/
	// 	$connection = \Yii::$app->db;
	// 	$transaction = $connection->beginTransaction();
	//
	//
	//
	// 	try {
	// 	/*******************-------BEGIN Transaction----------*****************/
	//
	// 	/*******************-------Save new Athelete----------*****************/
	// 	if ($this->_model->load(Yii::$app->request->post())) {
	//
	// 		$this->_model->fileAttr=  \yii\web\UploadedFile::getInstance($this->_model, 'fileAttr');
	//
	// 		/*-Set nationalId as Default username And password-*/
	// 		$this->_model->username=isset($this->_model->nationalId) ? $this->_model->nationalId:'';
	//
	// 		$loadDetailUser=$this->_userDetails->load(Yii::$app->request->post());
	//
	// 		$this->_userDetails->fileAttrjudgeCart=  \yii\web\UploadedFile::getInstance($this->_userDetails, 'fileAttrjudgeCart');
	// 		$refereeTypeId=\backend\modules\YumUsers\models\UsersType::getUserTypeId('Referee');
	// 		if($this->_model->userType==$refereeTypeId)
	// 			$this->_userDetails->scenario='CreateRefereeUserDetailse';
	// 		else
	// 			$this->_userDetails->scenario='CreateTechCommiteeDetailse';
	//
	//
	//
	// 		/*set event id  that provided from session .set admin user event id for user event*/
	// 		$this->_userDetails->eventId=$this->currentEventId;
	// 		$this->_userDetails->validate();
	//
	// 		if ( $this->_userDetails->validate(['eventId','fileAttrjudgeCart','bankAccountNo']) &&$this->_model->validate('fileAttr')&& $this->_model->createTechnicalCommitteeUser() )
	// 		{
	//
	// 		if ($loadDetailUser) {
	//
	// 			/**********if Upload file Save Pic  User athlete add in team**************/
	// 			if($this->_model->uploadPicFile())
	// 			{
	// 				$message=$this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
	// 			}
	// 			else
	// 			{
	// 				$failMessage=$this->createMessageCss('danger', Yii::t('app', '{item} Successfully Added User expect File.', array('item' => Yii::t('app', 'User'))));
	//
	// 			}
	// 			/**********if Upload file Save Pic  User athlete add in team**************/
	//
	//
	// 			if ($this->_userDetails->saveTechCommitDetails($this->_model->id)) {
	//
	// 			/**********if created  User referee EventMember Info**************/
	//
	//
	// 					if($this->_model->insertMemberEventInfo($this->_userDetails->id,$this->_userDetails->eventId,0,0,0))
	// 					{
	// 					$transaction->commit();
	// 					$message=$this->createMessageCss('success',Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
	// 					$message=$message.$failMessage;
	// 					$this->setFlashMsg('manage_team_member', $message);
	// 					return $this->redirect(['/YumUsers/technical-committee/index']);
	// 					}else
	// 					{
	//
	//
	// 				   throw new NotFoundHttpException( "Problem in Add User Team info" );
	//
	//
	// 					}
	//
	//
	// 				/**********if created  User athlete add in team**************/
	// 			}
	// 			else
	// 			{
	// 			  throw new NotFoundHttpException( "Problem in Add User Details" );
	//
	// 			}
	//
	// 		  }
	//
	//
	//
	// 		}
	// 	}
	//
	//
	// } catch(Exception $e) {
	// 	$transaction->rollback();
	// 	$message.=$this->createMessageCss('success',Yii::t('app', '{item}  Fail to create .Try again.', array('item' => Yii::t('app', 'User'))));
	// }
	//
	//
	//
	// 	return $this->render('create_referee', [
	// 				'model' => $this->_model,
	// 				'message' => $message,
	// 				'userDetails'=>$this->_userDetails,
	//
	// 	]);
	// }
	//


	public function actionUpdateReferee($userId)
	{
		/*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();
		$message = '';
		$query = TechnicalCommitteeUser::find()->andWhere(['id' => (int)$userId, 'valid' => 1]);

		if(Yii::$app->user->can('AssociationPresident')){
			$query->andFilterWhere(['creatorUserId' => Yii::$app->user->identity->id]);
		}

		$model = $query->one();

		if ($model === null) {
			Yii::$app->session->setFlash('error', Yii::t('app', 'User not found'));
			return $this->redirect(['index']);
		}

		/*----------Semantic Access Control base on Event Id-------------*/
		if (!$model->isUserMemberOfEvent($this->currentEventId, $userId)) {
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}

		$userDetails = $model->getUserDetailBaseOnEvent($this->currentEventId);
		$model->scenario = 'UpdateRefereeUser';

		/**-------Update Personal info-------**/
		if ($model->load(Yii::$app->request->post()) && isset($_POST['update_prsonal_info_btn']))
		{
			$_eventmember = $model->eventMembersByEvent;
			if($_eventmember->user_type_id != $model->userType){
				$_eventmember->user_type_id = $model->userType;
				$_eventmember->save();

				if($model->userType == UsersType::getUserTypeId('AssociationPresident')){
					$model->ChangeRole('Referee', 'AssociationPresident');
				}
				else{
					$model->ChangeRole('AssociationPresident', 'Referee');
				}
			}
			if($model->save())
			{
				$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));
			}
		}

		/**--------Update Picture -----------**/
		if ($model->load(Yii::$app->request->post()) && isset($_POST['update_pic_btn'])) {
			if ($model->uploadPicFile()) {
				$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]));

			}
		}

		/**--------changePassword -----------**/
		$changePasswordModel = new ChangePassword(['scenario' => 'change_password_byadmin']);
		$changePasswordModel->username = Yii::$app->user->identity->username;
		$changePasswordModel->passwordInput = isset($_POST['ChangePassword']['passwordInput']) ? $_POST['ChangePassword']['passwordInput'] : '';
		$changePasswordModel->targetUser = (int)$userId;

		if ($changePasswordModel->load(Yii::$app->request->post()) && isset($_POST['change_pass_btn']))
		{
			if($changePasswordModel->load(Yii::$app->request->post()))
			{
				/*---check Password----*/
				if ($resultValidate = $changePasswordModel->validate() && $resultVerifyPass = $changePasswordModel->ValidateCurrentPassword())
				{
					if ($changePasswordModel->changePassword())
					{
						$message = $this->createMessageCss('success', Yii::t('app', 'Changed Password Successfully .'));
					}
					else
					{
						$message = $this->createMessageCss('error', Yii::t('app', 'Error in changed password!'));
					}
				}
				elseif (!$resultValidate || !$resultVerifyPass)
				{
					$message = $this->createMessageCss('error', Yii::t('app', 'Error in changed password!'));
				}
			}
		}

		/**-------Update User Detail Personal info-------**/
		$userTypeId = $model->getUserType();
		if (UsersType::getUserTypeId('Referee') == $userTypeId)
			$userDetails->scenario = 'UpdateRefereeUserDetailse';
		else
			$userDetails->scenario = 'UpdateCommiteeUserDetailse';

		if ($userDetails->load(Yii::$app->request->post()) && isset($_POST['update_user_detail_info_btn']) && $userDetails->save()) {

			try {
				$uploadStuCartResult = $userDetails->uploadFileJudgeCart();
			} catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل مدرک داوری کارت دانشجویی پیش آمده است.');
			}

			$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));
		}

		return $this->render('update_athelete', [
			'model' => $model,
			'massage' => $message,
			'userDetails' => $userDetails,
			'changePasswordModel' => $changePasswordModel
		]);
	}


	/**
	 * Displays a single User model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		/*--check Accesscontrole base on EventId--*/
		$this->provideSemanticAccessControlData();

		$query = TechnicalCommitteeUser::find()->andWhere(['id' => (int)$id, 'valid' => 1]);

		if(Yii::$app->user->can('AssociationPresident')){
			$query->andFilterWhere(['creatorUserId' => Yii::$app->user->identity->id]);
		}

		$model = $query->one();

		if ($model === null) {
			Yii::$app->session->setFlash('error', Yii::t('app', 'User not found'));
			return $this->redirect(['index']);
		}

		/*----------Semantic Access Control base on Event Id-------------*/
		if (!$model->isUserMemberOfEvent($this->currentEventId, $id)) {
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view_user', [
				'model' => $model,
				'eventId' => $this->currentEventId
			]);
		} else {
			return $this->render('view_user', [
				'model' => $model,
				'eventId' => $this->currentEventId
			]);
		}
	}


	public function actionDelete($id = NULL)
	{
		/*--check Accesscontrole base on EventId--*/
		$this->provideSemanticAccessControlData();

		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {

			if (isset($post['id']))
				$id = (int)$post['id'];
			else if (isset($_GET['id']))
				$id = (int)$_GET['id'];

			$query = TechnicalCommitteeUser::find()->andWhere(['id' => (int)$id, 'valid' => 1]);

			if(Yii::$app->user->can('AssociationPresident')){
				$query->andFilterWhere(['creatorUserId' => Yii::$app->user->identity->id]);
			}

			$model = $query->one();

			if ($model === null) {
				Yii::$app->session->setFlash('error', Yii::t('app', 'User not found'));
				return $this->redirect(['index']);
			}

			/*----------Semantic Access Control base on Event Id-------------*/
			if (!$model->isUserMemberOfEvent(Yii::$app->user->identity->eventId, $id)) {
				throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
			}
			$result=$model->deleteTechnicalCommitteeByAdmin($id);
			if ($result['result']) {

				echo Json::encode([
					'success' => true,
					'messages' => [$result['msg']]]);
			} else {
				echo Json::encode([
					'success' => false,
					'messages' => [$result['msg']]]);

			}
		}

	}



	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{

		$model = TechnicalCommitteeUser::find()->where('id = :id And valid=1', [':id' => (int)$id])->one();
		if ($model !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


	public function createMessageCss($msgType, $string)
	{

		switch ($msgType) {
			case 'success':
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;

			case 'danger':
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
			default:
				$message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
		}

		return $message;

	}


	public function setFlashMsg($name, $message)
	{
		$session = Yii::$app->session;
		$session->setFlash($name, $message);
	}
}
