<?php

namespace backend\modules\YumUsers\controllers;


use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\data\Pagination;
use yii\data\Sort;

use backend\models\Event;
use backend\models\EventSearch;
use backend\models\Eventtype;
use backend\models\University;


use backend\modules\YumUsers\models\ChangePassword;
use backend\modules\YiiFileManager\models\UserFolders;

use backend\models\AuthorizedUniversity;
use backend\models\AuthorizedUniversitySearch;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UsersDetails;
use backend\modules\YumUsers\models\EventAdminUser;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\FileAction;
use backend\modules\YiiFileManager\models\Folders;
/**
 * UserController implements the CRUD actions for User model.
 */
class EventAdminController extends Controller
{
	public $_model;
	public $_fileModel;
	public $_fileAttribute;
	public $_userDetails;
	
   public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','delete','view','create-event-admin','update-event-admin-user','set-active'],

				'rules' => [
					 [
						'allow' => true,
						'actions' => ['index','delete','view','create-event-admin','update-event-admin-user','set-active'],
						'roles' => [User::Role_Admin],
					],
					
				],
			],
		];
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */

	public function actionIndex() {


		$request = Yii::$app->request;

		$searchModel = new EventSearch();
		$dataProvider = $searchModel->searchEventUsers ( Yii::$app->request->queryParams );

		$columns = [
			[ 
				'class' => 'yii\grid\SerialColumn' 
			],

			[
				'attribute' => 'name',
				'enableSorting'=>false,
				'value' => 'name',
			],

			[
				'label' => Yii::t('app', 'Senior Event User'),
				'enableSorting'=>false,
				'value' => function ($model) {
					if(is_object($model))
					  $userName=$model->getAdminEventUserInfo();
					else
					$userName='-----';

					return Html::encode($userName);
				},
			],
			[
				'label' => Yii::t('app', 'Last Login Date'),
				'enableSorting'=>false,
				'value' => function ($model) {
					$date=$model->getAdminEventLogintime();
					return Html::encode($date);
				},
			],
			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '140'],
				'template' => '{createUserResp} {updateUserResp} {setActiveUserResp} {sendMessage}  {delete}',

				'buttons' => [
					'createUserResp' => function ($url, $model,$data){
						$createButtom = Html::a(
							'<i class="fa fa-user-plus font-purple-plum" ></i>',
							Url::to(['/YumUsers/event-admin/create-event-admin','eventId'=>$model->id],true),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Create Admin Event'),
							]
						);
						return (!$model->isSetAdminUser() ? $createButtom : '<i class="fa fa-user-plus font-grey-silver"></i>');
					},

					'updateUserResp' => function ($url, $model) {
						$updateBotton = Html::a(
							'<i class="fa fa-pencil"></i>',
							Url::to(['/YumUsers/event-admin/update-event-admin-user','eventId'=>$model->id],true),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Update Admin Event'),
							]
						);
						return ($model->isSetAdminUser() ? $updateBotton : '<i class="fa fa-pencil font-grey-silver"></i>');
					},

					/*'setActiveUserResp' => function ($url, $model){
						if($model->isSetAdminUser()){
							$userObj=$model->getUniversityRespUserObj();
							if($userObj->active){
								return \yii\helpers\Html::tag('span', '<i class="fa fa-circle-o font-red"></i>', ['onclick' => "setActiveItem(" . $model->id . ",0)"]);
							}
							else{
								return \yii\helpers\Html::tag('span', '<i class="fa fa-check-circle font-green"></i>', ['onclick' => "setActiveItem(" . $model->id . ",1)"]);
							}
						}
						else{
							return '<i class="fa fa-check-circle font-green"></i>';
						}
					},*/

					'sendMessage' => function ($url, $model){
						$hasUser = $model->isSetAdminUser();
						$sendEmailLink = Html::a(
							'<i class="fa fa-envelope font-yellow"></i>',
							Url::to(['/messages/send','id'=>$hasUser]),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Send Message'),
							]
						);
						return ($hasUser ? $sendEmailLink : '<i class="fa fa-envelope font-grey-silver"></i>');
					},

					// 'delete' => function ($url, $model){
					// 	$deleteLink = Html::a(
					// 		'<i class="fa fa-trash font-red-pink"></i>',
					// 		Url::to(['/YumUsers/event-admin/delete','id'=>$model->id]),
					// 		[
					// 			'class' => 'hint--top hint--rounded hint--info',
					// 			'data-hint' => Yii::t('app', 'Delete'),
				 //	 			'data-pjax' => 1,
					// 		]
					// 	);
					// 	return ($model->isSetAdminUser() ? $deleteLink : '<i class="fa fa-trash font-red-pink"></i>');
					// },

				]
			],
	
		];

		
		$clientOptions = [ 
				'ajax' => [ 
						'url' => $request->url 
				],
				'order' => [ 
						[ 
								1,
								'asc' 
						] 
				] 
		];
		
		$start = $request->get ( 'start', 0 );
		$length = $request->get ( 'length', 10 );
		
		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination ();
		$_pagination->pageSize = $length;
		$_pagination->page = floor ( $start / $length );
		
		$dataProvider->pagination = $_pagination;
		
		$sortableColumn = array (
				NULL,
				'name',
				'eventTypeId',
				'universityId',
				'gender' 
		);
		$searchableColumn = array (
				'name',
				'eventTypeId',
				'universityId',
				'gender' 
		);
		
		$widget = Yii::createObject ( [ 
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn 
		] );
		
		if ($request->isAjax) {
			$result = $widget->getFormattedData ( $request->post ( 'draw' ) );
			
			echo json_encode ( $result );
			Yii::$app->end ();
		} else {
			return $this->render ( 'index', [ 
					'widget' => $widget 
			] );
		}
	}
	

	

 /****------------------------------------------------------------------------------------------**/
   public function createMessageCss($msgType,$string)
   {
 
	   switch ($msgType) {
		   case 'success':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
					break;

		   case 'danger':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
					break;		   
		   default:
				 $message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
			   break;
	   }

	   return $message;
	   
   }





   public  function setFlashMsg($name,$message)
   {
				$session = Yii::$app->session;
				$session->setFlash($name, $message);
   }
	


 /**********************************************************************************8**/
 //Create Event Admin
 /**********************************************************************************8**/

	
	public function actionCreateEventAdmin($eventId) {
		$message = '';
		$model = new EventAdminUser();
		$model->scenario='createEventAdminUser';

		
		/*--------------------------check Access------------------------------------*/
		if (($eventObj = Event::findOne($eventId)) !== null) {
			
			/*check uniq user response for autorized university */
			if($eventObj->isSetAdminUser())
			{

				$message=$this->createMessageCss('danger',Yii::t('app',' Event Admin Response Exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);						   
			}
			
			
		} else {
			
				$message=$this->createMessageCss('danger',Yii::t('app','request page not exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);
		}		
		/*--------------------------check Access------------------------------------*/
		
		  if ($model->load(Yii::$app->request->post())) {

				
				if ($model->createAdminEventUser((int)$eventId)) {
		
					 $message=$this->createMessageCss('success',Yii::t('app', 'Create  Senior User Event  Successfuly.'));
					 $this->setFlashMsg('user_resp_msg', $message);
				 	return $this->redirect(['/YumUsers/event-admin/index']);


				} 
			}

			return $this->render('create_user_event_admin', [
						'model' => $model,
						'message'=>$message,
						'eventobj'=>$eventObj,
					   
			]);
	}
	
	
	
	
	




	
public function actionUpdateEventAdminUser($eventId) {

		
		if (($eventObj = Event::findOne($eventId)) !== null) {
			
			/*check uniq user response for autorized university */
			if(!$eventObj->isSetAdminUser())
			{

				$message=$this->createMessageCss('danger',Yii::t('app','Admin Event User Dosent Exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);
			}
			
		} else {
				$message=$this->createMessageCss('danger',Yii::t('app','request page not exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);

		}  
		
		
		/*Find Related UserDetail*/
		$message = '';
		if($eventObj->getAdminEventUserObj()!=null)
		{
		  $userAdminEventId=$eventObj->adminUserObj->id;  
		}else
		{

				$message=$this->createMessageCss('danger',Yii::t('app','User Not Found'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);

		}
		
		
		$model = $this->findModel($userAdminEventId);
		$model->scenario='updateEventAdminUser'; 
		
		/*  becuase passwordInput is not field of user table not set in $model->load()  */
		$model->passwordInput=(isset($_POST['EventAdminUser']['passwordInput']))?$_POST['EventAdminUser']['passwordInput']:'';		

		if ($model->load(Yii::$app->request->post())) {
				if ($model->updateAdminEventUser()) {

				$message=$this->createMessageCss('success',Yii::t('app', 'Update Admin Event Successfuly.'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);


				} 
			}		

				return $this->render('update_user_resp', [
					'model' => $model,
					'message'=>$message,
					'eventObj'=>$eventObj,
				]);
	}
	


public function actionSetActive($autorizedUniId,$value) {
	
		
		if (Yii::$app->request->isAjax) {
			
		if(!in_array($value,[0,1])||!is_numeric($autorizedUniId))
		{
			echo Json::encode([
				'success' => false,
				'messages' => Yii::t('app','Invalid Params'),

					   ]);
			return;
		}


		if (($autorizedUniObj = AuthorizedUniversity::findOne($autorizedUniId)) !== null) {
			
			/*check uniq user response for autorized university */
			if(!$autorizedUniObj->hasUniversityRespUser())
			{
				echo Json::encode([
					'success' => false,
					'messages' => Yii::t('app','University User Response Dosent Exist')
						   ]);
				return;
			}
			
			/*check Just deine User response for own event by EventAdmin */
		   //if($autorizedUniObj->eventId!=$_SESSION['eventId']) 
			if(0)
			{
				echo Json::encode([
					'success' => false,
					'messages' => Yii::t('app','You Just Access Your Event')
						   ]);
				return;

			}
				
			
		} else {
				echo Json::encode([
					'success' => false,
					'messages' => Yii::t('app','You Just Access Your Event')
						   ]);
				return;  
		}  
		
		
		/*Find Related UserDetail*/
		$message = '';
		if($autorizedUniObj->getUniversityRespUserObj()!=null)
		{
		  $userRespId=$autorizedUniObj->respUserObj->id;  
		}else
		{
				echo Json::encode([
					'success' => false, 
					'messages' => Yii::t('app','User Not Found')
						   ]);
				return;
		}
			$model = $this->findModel($userRespId);  
			if ($model->setActiveVal($value)) {
				
				if($value)
					$message=Yii::t('app', '{item} Successfully Actived.', ['item' => Yii::t('app', 'User')]);
				else
					$message=Yii::t('app', '{item} Successfully Deactived.', ['item' => Yii::t('app', 'User')]);				
			   
				echo Json::encode([
					'success' => true,
					'messages' =>$message ]);
		 
			} else {
				echo Json::encode([
					'success' => false,
					'messages' => Yii::t('app','Failed To Deactive Items To {item}.', ['item' => Yii::t('app', 'User')])]);
		  
			}
		} 

	}




	
	

public function actionDelete($id) {
	

		if (Yii::$app->request->isAjax) {

		if (($eventObj = Event::findOne($id)) !== null) {
			
			/*check uniq user response for autorized university */
			if(!$eventObj->isSetAdminUser())
			{

				$message=$this->createMessageCss('danger',Yii::t('app','Admin Event User Dosent Exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);
			}
			
		} else {
				$message=$this->createMessageCss('danger',Yii::t('app','request page not exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);

		}  
		
		
		/*Find Related UserDetail*/
		$message = '';
		if($eventObj->getAdminEventUserObj()!=null)
		{
		  $userAdminEventId=$eventObj->adminUserObj->id;  
		}else
		{

				$message=$this->createMessageCss('danger',Yii::t('app','User Not Found'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/event-admin/index']);

		}
		

	   $model = $this->findModel($userAdminEventId);  
	   if ($model->deleteAdminEventUser($id)) {
			   
                echo Json::encode(
					[
						'success' => true,
						'messages' => [
							Yii::t('app', '{item} Successfully Deleted.',
								[
									'item' => $model->name
								]
							)
						]
					]
				);			   
		
			} else {
				echo Json::encode([
					'success' => false,
					'messages' => Yii::t('app','Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')])]);
		  
			}
		} 

	}



	


	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		$model=EventAdminUser::find()->where('id = :id And valid=1', [':id' => (int)$id])->one();
		if ($model !== null) {
			return $model;
		} else {
		   throw new NotFoundHttpException(Yii::t('app','The  user does not exist.'));
		}
	}
	
	
	

}