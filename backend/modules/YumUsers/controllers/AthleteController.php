<?php

namespace backend\modules\YumUsers\controllers;


use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YumUsers\models\AthleteUser;
use backend\modules\YumUsers\models\AthleteUserSearch;
use backend\modules\YumUsers\models\ChangePassword;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UsersDetails;
use backend\modules\YumUsers\models\UsersType;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * UserController implements the CRUD actions for User model.
 */
class AthleteController extends Controller
{
	public $_model;
	public $_fileModel;
	public $_fileAttribute;
	public $_userDetails;


	/*Access control properties*/
	public $currentEventId;
	public $currentEventName;
	public $userId;

	/*Create user properties*/
	public $message;
	public $failMessage;
	public $saveTypeUser;//related to create by duplicate nationalid


	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['create', 'index', 'update-athlete',  'delete', 'view',],
				'rules' => [

					[
						'actions' => ['delete'],
						'roles' => ['deleteAthlete'],
						'allow' => true,
					],

					[
						'actions' => ['create'],
						'roles' => ['createAthlete'],
						'allow' => true,
					],


					[
						'actions' => ['index'],
						'roles' => ['adminAthlete'],
						'allow' => true,
					],


					[
						'actions' => ['update-athlete'],
						'roles' => ['UpdateAthlete'],
						'allow' => true,
					],


					[
						'actions' => ['view'],
						'roles' => ['viewAthlete'],
						'allow' => true,
					],
					[
						'actions' => ['create', 'index', 'update-athlete',  'delete', 'view',],
						'roles' => ['ManageAthlete'],
						'allow' => true,

					],

				],
			],
		];
	}

	public function provideSemanticAccessControlData($value='')
	{
		$eventId=Yii::$app->user->identity->eventId;
		/*-------------Get Current Event Id Selected User----------------------------*/
		if (!empty( $eventId) ) {
			$this->currentEventId=$eventId ;
		}else
		{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}
	}

	public function actionIndex()
	{
		/*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();


		$request = Yii::$app->request;
		$searchModel = new AthleteUserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->currentEventId);

		$columns = [
			['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'fileId',
				'header' => Yii::t('app', 'Picture'),
				'enableSorting' => false,
				'format' => 'raw',
				'value' => function ($model) {
					return $model->getIconThumbnail($model->fileId);

				},
			],
			[
				'attribute' => 'last_name',
				'header' => Yii::t('app', 'Full Name'),
				'enableSorting' => false,
				'format' => 'html',
				'value' => function ($model) {
					return Html::a(
						Html::encode($model->FullName),
						Url::to(['/YumUsers/athlete/view', 'id' => $model->id])
					);
				},
			],
            [
                'header' => Yii::t('app', 'fieldsId'),
                'enableSorting' => false,
                'format' => 'html',
                'value' => function ($model) {
                    return $model->Field;
                },
            ],
			[
				'header' => Yii::t('app', 'Caravan'),
				'enableSorting' => false,
				'format' => 'html',
				'value' => function ($model) {
					return $model->CaravanName;
				},
			],
			[
				'attribute' => 'mobile',
				'enableSorting' => false,
			],
			[
				'attribute' => 'email',
				'format' => 'email',
				'enableSorting' => false,
			],
			[
				'attribute' => 'gender',
				'format' => 'raw',
				'enableSorting' => false,
				'value' => function ($model) {
					return $model->Sex;
				},
			],

			[
				'attribute' => 'last_login_time',
				'format' => 'raw',
				'enableSorting' => false,
				'value' => function ($model) {
					return $model->LastLogintime;
				},
			],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '140'],
				'template' => '{changepassbyadmin} {sendMessage} {view} {update} {delete}',
				'buttons' => [

					'changepassbyadmin' => function ($url, $model) {
						return Html::a(
							'<span class="glyphicon glyphicon-lock font-purple-plum"></span>',
							Url::to(['/YumUsers/admin/changepasswordbyadmin', 'usertargetid' => $model->id]),
							[
								'data-hint' => Yii::t('app', 'Change Password'),
								'class' => 'hint--top hint--rounded hint--info'
								// 'class' => 'showModalButton btn btn-success',
								// 'style'=>'padding:1px;'
							]
						);
					},

					'update' => function ($url, $model) {
						return Html::a(
							'<span class="glyphicon glyphicon-pencil font-yellow"></span>',
							Url::to(['/YumUsers/athlete/update-athlete', 'userId' => $model->id]),
							[
								'data-hint' => Yii::t('app', 'Update'),
								'class' => 'hint--top hint--rounded hint--info'
								// 'class' => 'showModalButton btn btn-success',
								// 'style'=>'padding:1px;'
							]
						);
					},
					'sendMessage' => function ($url, $model){
						return Html::a(
							'<i class="fa fa-envelope font-yellow"></i>',
							Url::to(['/messages/send', 'id' => $model->id]),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Send Message'),
							]
						);
					},
				],
			],
		];

		$clientOptions = [
			'ajax' => [
				'url' => $request->url,
			],
			'order' => [
				[2, 'asc']
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = array(NULL, 'last_name', 'username', 'mobile', 'email', 'gender', 'grade');
		$searchableColumn = array('name', 'last_name', 'username', 'mobile', 'email', 'sportNo', 'eName', 'eFamily', 'fatherName', 'idNo', 'nationalId');

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		} else {
			return $this->render('index', [
				'widget' => $widget,
			]);
		}


	}


	public function actionCreate()
	{

		/*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();

		$saveTypeUser=0;
		$this->message = '';
		$this->failMessage = '';

		$this->_userDetails = new UsersDetails();
		$this->_model = new AthleteUser();

		$this->_model->scenario = 'createAtheleteUserByAdmin';
		$this->_userDetails->scenario = 'CreateAthleteUserDetailse';


		if(isset($_POST['AthleteUser']['nationalId']))
		{
			$nationalId=$_POST['AthleteUser']['nationalId'];
			$userModel = AthleteUser::find()->where('nationalId= :nationalId AND valid=1 ', [':nationalId' => $nationalId])
				->one();
			if($userModel!=null)
			{
				$userDetails = $userModel->getUserDetailBaseOnEvent(Yii::$app->user->identity->eventId);
				if($userDetails!=null)
				{
					$saveTypeUser=AthleteUser::CREATE_EXIST_USER_EVENT;
					$this->_model=$userModel;
					$this->_userDetails=$userDetails;
				}
				else
				{
					$saveTypeUser=AthleteUser::CREATE_NEW_USER_EVENT;
					$this->_model=$userModel;
				}
			}
			else{
				$saveTypeUser=AthleteUser::CREATE_NEW_USER;
			}
		}


		switch ($saveTypeUser) {

			case AthleteUser::CREATE_NEW_USER:
				$this->_model->scenario = 'createAtheleteUserByAdmin';
				$this->_userDetails->scenario = 'CreateAthleteUserDetailse';
				if ($this->createNewAthelete()) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/athlete']);
				}
				break;


			case AthleteUser::CREATE_NEW_USER_EVENT:

				$this->_model->scenario = 'updateExistAthleteUser';
				$this->_userDetails->scenario = 'UpdateAtheleteUserDetailse';
				if ($this->createNewAthelete()) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/athlete']);
				}
				break;


			case AthleteUser::CREATE_EXIST_USER_EVENT:

				$this->_model->teamId = (isset($_POST['AthleteUser']['teamId']) ? $_POST['AthleteUser']['teamId'] : null);
				$this->_model->caravanId = (isset($_POST['AthleteUser']['caravanId']) ? $_POST['AthleteUser']['caravanId'] : null);


				if ($this->_model->isDuplicateEventUser(null, $this->_userDetails->id, null, UsersType::getUserTypeId('Athlete'), 0, $this->_model->teamId, $this->_model->caravanId)) {
					$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item} Already exsist.', array('item' => Yii::t('app', 'User'))));
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/athlete']);
					break;
				}

				$this->_model->scenario = 'updateExistAthleteUser';
				$this->_userDetails->scenario = 'UpdateAtheleteUserDetailse';
				if ($this->createNewAthelete()) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/athlete']);
				}
				break;

			default:
				break;
		}


		return $this->render('create_athelete', [
			'model' => $this->_model,
			'message' => $this->message,
			'userDetails' => $this->_userDetails,

		]);
	}


	public function createNewAthelete()
	{
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			/*---BEGIN Transaction---*/

			/*--Save new Athelete--*/
			if ($this->_model->load(Yii::$app->request->post())) {

				//set for validate
				$this->_model->fileAttr = \yii\web\UploadedFile::getInstance($this->_model, 'fileAttr');

				/*-Set nationalId as Default username And password-*/
				$this->_model->username = isset($this->_model->nationalId) ? $this->_model->nationalId : '';
				$this->_model->teamId = (isset($_POST['AthleteUser']['teamId']) ? $_POST['AthleteUser']['teamId'] : null);
				$this->_model->caravanId = (isset($_POST['AthleteUser']['caravanId']) ? $_POST['AthleteUser']['caravanId'] : null);


				$loadDetailUser = $this->_userDetails->load(Yii::$app->request->post());

				/*set event id  that provided from session .set admin user event id for user event*/
				$this->_userDetails->eventId = $this->currentEventId;


				if ($this->_userDetails->validate(['eventId', 'sportAssuranceNo','stuNo']) && $this->_model->validate('fileAttr') && $this->_model->createAthleteUser()) {

					if ($loadDetailUser) {


						/*-if Upload file Save Pic  User athlete add in team-*/
						if ($this->_model->uploadPicFile()) {
							$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
						} else {
							$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item} Successfully Added User expect File.', array('item' => Yii::t('app', 'User'))));

						}


						if ($this->_userDetails->saveAthleteDetails($this->_model->id)) {

							if ($this->_model->insertMemberEventInfo($this->_userDetails->id, $this->currentEventId, $this->_model->caravanId, $this->_model->teamId, 0)) {
								$transaction->commit();
								return true;
							} else {
								throw new NotFoundHttpException(Yii::t('app', "Problem in Add User Team info"));

							}

						} else {
							throw new NotFoundHttpException(Yii::t('app',"Problem in Add User Details"));

						}


					}

				}
			}


		} catch (Exception $e) {
			$transaction->rollback();
			$this->message .= $this->createMessageCss('success', Yii::t('app', '{item}  Fail to create .Try again.', array('item' => Yii::t('app', 'User'))));
			return false;
		}


	}


	public function actionUpdateAthlete($userId)
	{
	  /*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();

		$message = '';
		$model = $this->findModel($userId);

		/*----------Semantic Access Control base on Event Id-------------*/
		if (!Yii::$app->user->can(User::Role_Admin) && !$model->isUserMemberOfEvent($this->currentEventId, $userId)) {
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}


		$userDetails = $model->getUserDetailBaseOnEvent($this->currentEventId);
		$model->scenario = 'updateAthleteUser';


		/**-------Update Personal info-------**/
		if ($model->load(Yii::$app->request->post()) && isset($_POST['update_prsonal_info_btn']) && $model->save()) {

			$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));
		}
		/**----------------------------------**/


		/**--------Update Picture -----------**/
		if ($model->load(Yii::$app->request->post()) && isset($_POST['update_pic_btn'])) {
			if ($model->uploadPicFile()) {
				$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]));

			}
		}
		/**-----------------------------------**/

		/**--------changePassword -----------**/
		$changePasswordModel = new ChangePassword(['scenario' => 'change_password_byadmin']);
		$changePasswordModel->username = Yii::$app->user->identity->username;
		$changePasswordModel->passwordInput = isset($_POST['ChangePassword']['passwordInput']) ? $_POST['ChangePassword']['passwordInput'] : '';
		$changePasswordModel->targetUser = (int)$userId;

		if ($changePasswordModel->load(Yii::$app->request->post()) && isset($_POST['change_pass_btn'])) {


			if ($changePasswordModel->load(Yii::$app->request->post())) {
				/*---check Password----*/
				if ($resultValidate = $changePasswordModel->validate() && $resultVerifyPass = $changePasswordModel->ValidateCurrentPassword()) {
					if ($changePasswordModel->changePassword()) {
						$message = $this->createMessageCss('success', Yii::t('app', 'Changed Password Successfully .'));

					} else {
						$message = $this->createMessageCss('error', Yii::t('app', 'Error in changed password!'));
					}
				} elseif (!$resultValidate || !$resultVerifyPass) {
					$message = $this->createMessageCss('error', Yii::t('app', 'Error in changed password!'));
				}
			}

		}
		/**-----------------------------------**/

		/**-------Update User Detail Personal info-------**/
		$userDetails->scenario = 'UpdateAtheleteUserDetailse';
		if ($userDetails->load(Yii::$app->request->post()) && isset($_POST['update_user_detail_info_btn']) && $userDetails->save()) {

			try {
				$uploadStuCartResult = $userDetails->uploadFileStuCart();
			} catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن کارت دانشجویی پیش آمده است.');
			}

			try {
				$uploadStuAssuranceCartResult = $userDetails->uploadFileAssuranceCart();
			} catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن  کارت یمه پیش آمده است.');
			}

			$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));

		}
		/**--------------------------------------------**/


		return $this->render('update_athlete', [
			'model' => $model,
			'massage' => $message,
			'userDetails' => $userDetails,
			'changePasswordModel' => $changePasswordModel

		]);
	}


	/**
	 * Displays a single User model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		/*--check Accesscontrole base on EventId--*/
		$this->provideSemanticAccessControlData();

		$model = $this->findModel($id);
		/*----------Semantic Access Control base on Event Id-------------*/
		if (!Yii::$app->user->can(User::Role_Admin) && !$model->isUserMemberOfEvent($this->currentEventId, $id)) {
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view_user', [
				'model' => $model,
				'eventId' => $this->currentEventId
			]);
		} else {
			return $this->render('view_user', [
				'model' => $model,
				'eventId' => $this->currentEventId
			]);
		}
	}


	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */

	public function actionDelete($id = NULL)
	{
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {

			if (isset($post['id']))
				$id = (int)$post['id'];
			else if (isset($_GET['id']))
				$id = (int)$_GET['id'];

			$model = $this->findModel($id);

			/*----------Semantic Access Control base on Event Id-------------*/
			if (!Yii::$app->user->can(User::Role_Admin) && !$model->isUserMemberOfEvent($this->currentEventId, $id)) {
				throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
			}
			$result=$model->deleteAthleteByAdmin($id);
			if ($result['result']) {

				echo Json::encode([
					'success' => true,
					'messages' => [$result['msg']]]);
			} else {
				echo Json::encode([
					'success' => false,
					'messages' => [$result['msg']]]);

			}
		}

	}


	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{


		$model = AthleteUser::find()->where('id = :id And valid=1', [':id' => (int)$id])->one();
		if ($model !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


	public function createMessageCss($msgType, $string)
	{

		switch ($msgType) {
			case 'success':
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;

			case 'danger':
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
			default:
				$message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
		}

		return $message;

	}


	public function setFlashMsg($name, $message)
	{
		$session = Yii::$app->session;
		$session->setFlash($name, $message);
	}


	public function actionCheckNationalIdExist($nationalId)
	{
		$model = AthleteUser::find()->select(['id', 'username', 'name', 'last_name', 'gender', 'homepage', 'tel', 'mobile', 'email', 'fatherName', 'idNo', 'birthDate', 'birthCityId'])
			->where('nationalId= :nationalId', [':nationalId' => $nationalId])
			->one();


		if ($model !== null) {

			/*prevent to load admin informations*/
			if (Yii::$app->getAuthManager()->checkAccess($model->id, AthleteUser::Role_Admin)) {
				echo json_encode(array('person' => null, 'userDetailResult' => null, 'exist' => false));
				return;
			}

			/*prevent to load SeniorUserEvent admin informations*/
			if (!Yii::$app->user->can(AthleteUser::Role_SeniorUserEvent)) {
				if (!Yii::$app->getAuthManager()->checkAccess($model->id, AthleteUser::Role_SeniorUserEvent)) {
					echo json_encode(array('person' => null, 'userDetailResult' => null, 'exist' => false));
					return;
				}

			}


			$eventMemObj = $model->eventMembersByEvent;

			$userDetailsObj = $model->getUserDetailBaseOnEvent(Yii::$app->user->identity->eventId);

			if ($eventMemObj != null) {
				$model->caravanId = $eventMemObj->caravanId;
				$model->teamId = $eventMemObj->teamsId;
			}

			$userDetailResult=null;
			if ($userDetailsObj != null) {
				$userDetailResult = $userDetailsObj->getAttributes(['sportMembershipHistory','educationalField','educationalSection','stuNo','sportAssuranceNo','sportFieldId','stuCartFileId','assuranceCartFileId']);
			}

			$userResult = $model->getAttributes([ 'username', 'name', 'last_name', 'gender', 'homepage', 'tel', 'mobile', 'email', 'fatherName', 'idNo', 'birthDate', 'birthCityId']);
			$userResult['caravanId'] = $model->caravanId;
			$userResult['teamId'] = $model->teamId;

			echo json_encode(array('person' => $userResult, 'userDetailResult' => $userDetailResult, 'exist' => true));
			return;

		} else {
			echo json_encode(array('person' => null, 'userDetailResult' => null, 'exist' => false));
			return;
		}
	}


}
