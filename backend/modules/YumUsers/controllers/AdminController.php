<?php

namespace backend\modules\YumUsers\controllers;


use Yii;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use \backend\modules\YumUsers\models\ChangePassword;
use yii\web\Session;
use yii\web\Response;
use backend\modules\YumUsers\models\Role;
use \backend\models\UserSetting;

/**
 * UserController implements the CRUD actions for User model.
 */
class AdminController extends Controller
{

   public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
					[
						'allow' => true,
						'actions' => [
							'ChangeUsersettingByadmin',
							'changepasswordbyadmin',
							'verifyadmin',
							'changepassbyadmin',
							//'changerolebyadmin' /*Disable because dont use change role , search nationalid and then create user*/
						],
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	protected function findUserModel($id)
	{
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionVerifyadmin($usertargetid)
	{
		$message = '';
		$model = new ChangePassword(['scenario'=>'verify_admin_pass']);
		$model->username = Yii::$app->user->identity->username;
		$model->passwordInput=$_POST['ChangePassword']['passwordInput'];
		  
		if($model->load(Yii::$app->request->post()))
		{
			/**
			 * check Password
			 */
			if($resultValidate=$model->validate()&&$resultVerifyPass=$model->ValidateCurrentPassword())
			{
				$session = Yii::$app->session;
				$session->set('verified_admin_pass', true);
				return $this->redirect(['/YumUsers/admin/changepassbyadmin', 'userTargetId' => $model->targetUser]);
			}
			elseif(!$resultValidate||!$resultVerifyPass)
			{
				Yii::$app->response->format = Response::FORMAT_JSON;
				return [
					'forceReload' => 'true',
					'title' => 'test',
					'content' => $this->renderAjax('verify_admin', [
						'model' => $model,
					]),
				];
			}
		}
		
		return $this->renderajax('verify_admin', [
			'model' => $model,
			'userTargetId' => isset($model->targetUser) ? $model->targetUser : 0,
		]);
	}

//	public function actionChangerolebyadmin($usertargetid)
//	{
//		$model = new Role;
//
//		if(Yii::$app->request->post())
//		{
//			$model->role = $_POST['Role']['role'];
//			$model->targetUser = $usertargetid;
//
//			/**
//			 * check Roles
//			 */
//				if ($model->changeRole($model->role, $model->targetUser))
//				{
//					$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
//					<a href="#" class="alert-link">' . Yii::t('app', 'Changed Role Successfully .') . '</a>
//					</div>';
//					$session = Yii::$app->session;
//					$session->setFlash('change_password_success', $message);
//					return $this->redirect(['/YumUsers/user/index', 'userTargetId' => $model->targetUser]);
//				}
//		}
//
//		return  $this->render('change_role_admin', [
//			'model' => $model,
//			'userTargetId' => $usertargetid,
//		]);
//		}



	
	
	public function actionChangeUsersettingByadmin($usertargetid)
	{
		$message = '';
		$model = new UserSetting;
		$model->targetUser = (int) $usertargetid;
		$model->scenario='update_filetype_setting';

		if ($model->load(Yii::$app->request->post()))
		{
			$model->extInputArray=$_POST['UserSetting']['extInputArray'];
			/**
			 * check Roles
			 */
			if($model->validate())
			{
				if($model->UpdateFileTypeUserSetting())
				{
					$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
					<a href="#" class="alert-link">' . Yii::t('app', 'Update Default File Type Successfully.') . '</a>
					</div>';
					
					$session = Yii::$app->session;
					$session->setFlash('change_password_success', $message);		 
					return $this->redirect(['/YumUsers/user/index', 'userTargetId' => $model->targetUser]);
				}
				else
				{
					Yii::$app->response->format = Response::FORMAT_JSON;
					return [
						'forceReload' => 'true',
						'title' => 'test',
						'content' => $this->renderAjax('change_user_setting_admin', [
							'model' => $model,
						]),
					];
				}
			}
			else
			{
				Yii::$app->response->format = Response::FORMAT_JSON;
				return [
					'forceReload' => 'true',
					'title' => 'test',
					'content' => $this->renderAjax('change_user_setting_admin', [
					'model' => $model,
					]),
				];
			}
		}

		return $this->renderajax('change_user_setting_admin', [
			'model' => $model,
			'userTargetId' => isset($model->targetUser) ? $model->targetUser : 0,
		]);
	}

	public function actionChangepasswordbyadmin($usertargetid)
	{
		$message = '';
		$model = new ChangePassword(['scenario'=>'change_password_byadmin']);
		$model->username = Yii::$app->user->identity->username;
		$model->passwordInput=  isset($_POST['ChangePassword']['passwordInput'])?$_POST['ChangePassword']['passwordInput']:'';
		$model->targetUser = (int) $usertargetid;

		if($model->load(Yii::$app->request->post()))
		{
			/**
			 * check Password
			 */
			if($resultValidate=$model->validate() && $resultVerifyPass=$model->ValidateCurrentPassword())
			{
				if($model->changePassword())
				{
					Yii::$app->session->setFlash('success', Yii::t('app', 'Changed Password Successfully .'));
					return $this->redirect(['/YumUsers/user/index', 'userTargetId' => $model->targetUser]);
				}
				else
				{
					Yii::$app->session->setFlash('error', Yii::t('app', 'Error in changed password!'));
				}
			}
			elseif(!$resultValidate||!$resultVerifyPass)
			{
				Yii::$app->session->setFlash('error', Yii::t('app', 'Error in changed password!'));
			}
		}

		return $this->render('verify_admin', [
			'model' => $model,
			'userTargetId' => isset($model->targetUser) ? $model->targetUser : 0,
		]);
	}

	/*this action use verify admin first*/
	public function actionChangepassbyadmin($userTargetId)
	{
		$message = '';
		$session = Yii::$app->session;

		if (!$session->has('verified_admin_pass') || $session->get('verified_admin_pass') != TRUE){
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}
		
		$model = ChangePassword::findOne($userTargetId);

		if($model == null){
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$model->targetUser = (int) $userTargetId;
		if($model->load(Yii::$app->request->post()) && $model->validate())
		{
			if($model->changePassword())
			{
				$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
					<a href="#" class="alert-link">' . Yii::t('app', 'Changed Password Successfully .') . '</a>
					</div>';
				$session = Yii::$app->session;
				$session->setFlash('change_password_success', $message);
			 	$session->remove('verified_admin_pass');
				return $this->redirect(['/YumUsers/user/index', 'userTargetId' => $model->targetUser]);
			}
		}

		return $this->render('change_password_admin', [
			'model' => $model,
			'message' => $message,
			'userTargetId' => isset($model->targetUser) ? $model->targetUser : 0,
		]);
	}
	
	
	function Rbac()
	{
//$auth = new \yii\rbac\DbManager;
//$auth->init();
//$role0 = $auth->createRole('admin');
//$auth->add($role0);
//$role = $auth->createRole('sub_admin');
//$auth->add($role);
//$role1 = $auth->createRole('teacher');
//$auth->add($role1);
//$role2 = $auth->createRole('student');
//$auth->add($role2);
//$role3 = $auth->createRole('staff');
//$auth->add($role3);
//$role4 = $auth->createRole('other');
//$auth->add($role4);
//$role5 = $auth->createRole('reader');
//$auth->add($role5);
//
//$auth->addChild($role0, $role);
//
//
//$auth->addChild($role, $role1);
//$auth->addChild($role, $role2);
//$auth->addChild($role, $role3);
//$auth->addChild($role, $role4);
//$auth->addChild($role, $role5);
//
//
//$auth->addChild($role1, $role5);
//$auth->addChild($role2, $role5);
//$auth->addChild($role3, $role5);
//$auth->addChild($role4, $role5);

//$auth->assign($role, $user_id);
	}
}
