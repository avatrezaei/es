<?php

namespace backend\modules\YumUsers\controllers;


use Yii;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UsersDetails;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use \backend\modules\YumUsers\models\ChangePassword;
use \backend\modules\YiiFileManager\models\UserFolders;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserDetailsController extends Controller
{

   public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
					 [
						'allow' => true,
						'actions' => ['index','detail','edit2','delete','view','update'],
						'roles' => ['admin'],
					],
				],
			],
		];
	}

   
   public function actionEdit2($id) {
	   
		$post = Yii::$app->request->post();
		if (Yii::$app->request->isAjax) {
			$id = (int) $_GET['id'];
			if ($model = $this->findModel($id)) {
		$model->scenario='UpdateUserDetailse';
				
				if ($model->load(Yii::$app->request->post()) && $model->save()) {  
				  $detailView=$this->renderAjax('expand_detail', ['model' => $model]);


				  
		  try {
				 $uploadStuCartResult=$model->uploadFileStuCart() ;		
				 }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن کارت دانشجویی پیش آمده است.');
			  }  

			  try {
				  $uploadStuAssuranceCartResult=$model->uploadFileAssuranceCart();
				 }catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن  کارت یمه پیش آمده است.');
			  }					

				if($uploadStuAssuranceCartResult&&$uploadStuCartResult)	
				{
					$detailView=$this->renderAjax('expand_detail', ['model' => $model]);
					echo json_encode(
							   [
								   'detailviewcontent'=>$detailView
							   ]
							   );
					return;
				}else
				{
					if (count($model->errors) > 0) {
								$msg = '';
								foreach ($model->errors as $attribute => $errors) {
									foreach ($errors as $error) {
										$msg.= $error . '<br>';
									}
								}
							   $detailView=$this->renderAjax('expand_detail', ['model' => $model]);
							   echo json_encode(
									   [
										   'errormessage'=>$msg,
										   'detailviewcontent'=>$detailView
									   ]
									   );
							   return;
							}		 
				}	  

				   $detailView=$this->renderAjax('expand_detail', ['model' => $model]);
				   echo json_encode([ 'detailviewcontent'=>$detailView ]);
				   return;
 
		  
				} else {
					if (count($model->errors) > 0) {
						$msg = '';
						foreach ($model->errors as $attribute => $errors) {
							foreach ($errors as $error) {
								$msg.= $error . '<br>';
							}
						}
					  
					   $detailView=$this->renderAjax('expand_detail', ['model' => $model]);
					   echo json_encode(
							   [
								   'errormessage'=>$msg,
								   'detailviewcontent'=>$detailView
							   ]
							   );
					}
				}
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		} else {
			throw new ForbiddenHttpException('The requested params forbidden');
		}
	}
	
 
	 public function actionEditRefereeInfo($id) {
	   
		$post = Yii::$app->request->post();
		if (Yii::$app->request->isAjax) {
			$id = (int) $_GET['id'];
			if ($model = $this->findModel($id)) {
		$model->scenario='UpdateUserDetailse';
				
				if ($model->load(Yii::$app->request->post()) && $model->save()) {  
				  $detailView=$this->renderAjax('expand_detail_refere', ['model' => $model]);

	  

				   $detailView=$this->renderAjax('expand_detail_refere', ['model' => $model]);
				   echo json_encode([ 'detailviewcontent'=>$detailView ]);
				   return;
 
		  
				} else {
					if (count($model->errors) > 0) {
						$msg = '';
						foreach ($model->errors as $attribute => $errors) {
							foreach ($errors as $error) {
								$msg.= $error . '<br>';
							}
						}
					  
					   $detailView=$this->renderAjax('expand_detail_refere', ['model' => $model]);
					   echo json_encode(
							   [
								   'errormessage'=>$msg,
								   'detailviewcontent'=>$detailView
							   ]
							   );
					}
				}
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		} else {
			throw new ForbiddenHttpException('The requested params forbidden');
		}
	}
	
	
	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
 
	public function actionDelete($id = NULL) {
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {

			if (isset($post['id']))
				$id = (int) $post['id'];
			else if (isset($_GET['id']))
				$id = (int) $_GET['id'];

			$model=$this->findModel($id);
			if ($model->delete()) {
			   
				echo Json::encode([
					'success' => true,
					'messages' => [Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')])]]);
			} else {
				$detailView=$this->renderAjax('expand_detail', ['model' => $model]);
				echo Json::encode([
					'success' => false,
					'detailviewcontentt'=>$detailView,
					'messages' => [Yii::t('app','Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')])]]);
		  
			}
		  // return;
		} 

	}


	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		
		
		$model=UsersDetails::find()->where('id = :id And valid=1', [':id' => (int)$id])->one();
		if ($model !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}


	}
	

}
