<?php

namespace backend\modules\YumUsers\controllers;

use backend\modules\YumUsers\models\UsersType;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\data\Pagination;
use yii\data\Sort;

use backend\modules\YumUsers\models\ChangePassword;
use backend\modules\YiiFileManager\models\UserFolders;

use backend\models\AuthorizedUniversity;
use backend\models\AuthorizedUniversitySearch;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UsersDetails;
use backend\modules\YumUsers\models\UniversityRespUser;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\FileAction;
use backend\modules\YiiFileManager\models\Folders;
/**
 * UserController implements the CRUD actions for User model.
 */
class UniversityRespController extends Controller
{
	public $_model;
	public $_fileModel;
	public $_fileAttribute;
	public $_userDetails;

	/*Access control properties*/
	public $currentEventId;
	public $currentEventName;
	public $userId;

	/*Create user properties*/
	public $message;
	public $failMessage;
	public $saveTypeUser;//related to create by duplicate nationalid



	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index','delete','view','create-user-resp','update-user-resp','set-active'],
						'roles' => [User::Role_SeniorUserEvent],
					],

				],
			],
		];
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */




	/*ID for Access control*/
	public $currentUserDetailId;

	public function provideSemanticAccessControlData($value='')
	{

		$session = Yii::$app->session;
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

		$eventId=Yii::$app->user->identity->eventId;


		/*-------------Get Current Event Id Selected User----------------------------*/
		if (!empty( $eventId) ) {
			$this->currentEventId=$session->get('eventId') ;
		}else
		{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Event Id did not set'));
		}

		$userDetailId=Yii::$app->user->identity->getUserDetailIdBaseOnEvent($eventId);


		//just admin can have no userdetail ID
		/*-------------Get Current User Detail Id ----------------------------*/
		if(!\Yii::$app->authManager->checkAccess ( \Yii::$app->user->identity->id, 'admin' ))
		{
			if (!empty( $userDetailId) ) {
				$this->currentUserDetailId=$userDetailId;
			}else
			{
				throw new \yii\web\HttpException(400, Yii::t('app','The Detail Id did not set'));
			}

		}



	}


	public function actionIndex()
	{
		$this->provideSemanticAccessControlData();

		$eventId= $this->currentEventId;
		$request = Yii::$app->request;
		$searchModel = new AuthorizedUniversitySearch();
		$dataProvider = $searchModel->respUserListSearch(Yii::$app->request->queryParams, $eventId);

		$columns = [
			[
				'class' => 'yii\grid\SerialColumn'
			],

			[
				'attribute' => 'universityId',
				'enableSorting'=>false,
				'value' => 'university.name',
			],
			[
				'attribute' => 'userDetailId',
				'enableSorting'=>false,
				'format' => 'html',
				'value' => function ($model) {
					$userName=$model->getUniversityRespUserInfo($model->universityId,$model->eventId);
					return $userName;
				},
			],
			[
				'label' => Yii::t('app', 'Last Login Date'),
				'enableSorting'=>false,
				'format' => 'html',
				'value' => function ($model) {
					return $model->getUserRespLastLogintime();
				},
			],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '140'],
				'template' => '{login} {createUserResp} {updateUserResp} {setActiveUserResp} {sendMessage} {archiveMessage} {delete}',

				'buttons' => [
					'createUserResp' => function ($url, $model,$data){
						$createButtom = Html::a(
							'<i class="fa fa-user-plus font-purple-plum" ></i>',
							Url::to(['/YumUsers/university-resp/create-user-resp','autorizedUniId'=>$model->id],true),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Create user resp'),
							]
						);
						return (!$model->hasUniversityRespUser() ? $createButtom : '');
					},

					'login' => function ($url, $model,$data){
						$createButtom = Html::a(
							'<i class="icon-login font-purple-plum" ></i>',
							Url::to(['/site/switch-user', 'id' => $model->UserId, 'save' => 'yes'], true),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Login with Agent'),
							]
						);
						return ($model->hasUniversityRespUser() ? $createButtom : '');
					},

					'updateUserResp' => function ($url, $model) {
						$updateBotton = Html::a(
							'<i class="fa fa-pencil"></i>',
							Url::to(['/YumUsers/university-resp/update-user-resp','autorizedUniId'=>$model->id],true),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Update user resp'),
							]
						);
						return ($model->hasUniversityRespUser() ? $updateBotton : '<i class="fa fa-pencil font-grey-silver"></i>');
					},

					/*'setActiveUserResp' => function ($url, $model){
						if($model->hasUniversityRespUser()){
							$userObj=$model->getUniversityRespUserObj();
							if($userObj->active){
								return \yii\helpers\Html::tag('span', '<i class="fa fa-circle-o font-red"></i>', ['onclick' => "setActiveItem(" . $model->id . ",0)"]);
							}
							else{
								return \yii\helpers\Html::tag('span', '<i class="fa fa-check-circle font-green"></i>', ['onclick' => "setActiveItem(" . $model->id . ",1)"]);
							}
						}
						else{
							return '<i class="fa fa-check-circle font-green"></i>';
						}
					},*/

					'sendMessage' => function ($url, $model){
						$sendEmailLink = Html::a(
							'<i class="fa fa-envelope font-yellow"></i>',
							Url::to(['/messages/send', 'id' => $model->UserId]),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Send Message'),
							]
						);
						return ($model->hasUniversityRespUser() ? $sendEmailLink : '<i class="fa fa-envelope font-grey-silver"></i>');
					},

					'archiveMessage' => function ($url, $model){
						$sendEmailLink = Html::a(
							'<i class="fa fa-file-archive-o" aria-hidden="true"></i>',
							Url::to(['/messages/archive', 'id' => $model->UserId]),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Archive Message'),
							]
						);
						return ($model->hasUniversityRespUser() ? $sendEmailLink : '<i class="fa fa-envelope font-grey-silver"></i>');
					},


				]
			],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = array(NULL, 'universityId','userDetailId', NULL);
		$searchableColumn = array('university.name','address','phone', 'fax');

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
				'eventId' => $eventId,
			]);
		}
	}





	public function actionCreateUserResp($autorizedUniId) {

		$this->provideSemanticAccessControlData();


		$message = '';
		$model = new UniversityRespUser();
		$model->scenario='createUserResp';



		if (($autorizedUniObj = AuthorizedUniversity::findOne($autorizedUniId)) !== null) {

			/*check uniq user response for autorized university */
			if($autorizedUniObj->hasUniversityRespUser())
			{

				$message=$this->createMessageCss('danger',Yii::t('app','University User Response Exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/university-resp/index']);
			}



		} else {

			$message=$this->createMessageCss('danger',Yii::t('app','You Just Access Your Event'));
			$this->setFlashMsg('user_resp_msg', $message);
			return $this->redirect(['/YumUsers/university-resp/index']);

		}




		$saveTypeUser=0;
		$this->message = '';
		$this->failMessage = '';

		$this->_userDetails = new UsersDetails();
		$this->_model = new UniversityRespUser();

		$this->_model->scenario = 'createUserResp';
		$this->_userDetails->scenario = 'CreateAthleteUserDetailse';


		if(isset($_POST['UniversityRespUser']['nationalId']))
		{
			$nationalId=$_POST['UniversityRespUser']['nationalId'];
			$userModel = UniversityRespUser::find()->where('nationalId= :nationalId AND valid=1 ', [':nationalId' => $nationalId])
				->one();
			if($userModel!=null)
			{
				$userDetails = $userModel->getUserDetailBaseOnEvent(Yii::$app->user->identity->eventId);
				if($userDetails!=null)
				{
					$saveTypeUser=UniversityRespUser::CREATE_EXIST_USER_EVENT;
					$this->_model=$userModel;
					$this->_userDetails=$userDetails;
				}
				else
				{
					$saveTypeUser=UniversityRespUser::CREATE_NEW_USER_EVENT;
					$this->_model=$userModel;
				}
			}
			else{
				$saveTypeUser=UniversityRespUser::CREATE_NEW_USER;
			}
		}

		/*check Just deine User response for own event by EventAdmin */
		if($autorizedUniObj->eventId==$this->currentEventId)
		{
			$this->_model->eventId= $autorizedUniObj->eventId;
			$this->_model->userUniversityId=$autorizedUniObj->universityId;
		}else
		{
			$message=$this->createMessageCss('danger',Yii::t('app','You Just Access Your Event'));
			$this->setFlashMsg('user_resp_msg', $message);
			return $this->redirect(['/YumUsers/university-resp/index']);

		}


		switch ($saveTypeUser) {

			case UniversityRespUser::CREATE_NEW_USER:

				$this->_model->scenario = 'createUserResp';
				$this->_userDetails->scenario = 'CreateAthleteUserDetailse';
				if ($this->createNewUserResp($autorizedUniObj)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('user_resp_msg', $this->message);
					return $this->redirect(['/YumUsers/university-resp']);
				}

				break;


			case UniversityRespUser::CREATE_NEW_USER_EVENT:

				$this->_model->scenario = 'updateExistUser';
				$this->_userDetails->scenario = 'UpdateAtheleteUserDetailse';
				if ($this->createNewUserResp($autorizedUniObj)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('user_resp_msg', $this->message);
					return $this->redirect(['/YumUsers/university-resp']);
				}

				break;


			case UniversityRespUser::CREATE_EXIST_USER_EVENT:


				if ($this->_model->isDuplicateUniversityRespUser(null, $this->_userDetails->id, null, UsersType::getUserTypeId(User::Type_UniversityRepresentative),$autorizedUniObj->universityId,0, 0)) {
					$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item} Already exsist.', array('item' => Yii::t('app', 'User'))));
					$this->setFlashMsg('user_resp_msg', $this->message);
					return $this->redirect(['/YumUsers/university-resp']);
					break;
				}


				$this->_model->scenario = 'updateExistUser';
				$this->_userDetails->scenario = 'UpdateAtheleteUserDetailse';
				if ($this->createNewUserResp($autorizedUniObj)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('user_resp_msg', $this->message);
					return $this->redirect(['/YumUsers/university-resp']);
				}

			default:
				break;
		}



		return $this->render('create_user_resp', [
			'model' => $this->_model,
			'message'=>$this->message ,
			'autorizedUniObj'=>$autorizedUniObj,

		]);
	}



	public function createNewUserResp($autorizedUniObj)
	{

		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			/*---BEGIN Transaction---*/

			if($this->_model->scenario=='updateExistUser'||$this->_model->scenario=='updateExistUser')
				$tempUsername=$this->_model->username;

			/*--Save new Athelete--*/
			if ($this->_model->load(Yii::$app->request->post())) {

                /*Dont use default username that set in interface*/
				if($this->_model->scenario=='updateExistUser'||$this->_model->scenario=='updateExistUser')
				$this->_model->username=$tempUsername;



				/*set event id  that provided from session .set admin user event id for user event*/
				$this->_userDetails->eventId = $this->currentEventId;


				if ($this->_model->validate() && $this->_model->createUniversityRespUser()) {


					/*save UserDetailId In autorized Uni table*/
					$autorizedUniObj->userDetailId=$this->_model->userDetailId;
					if($autorizedUniObj->update())
					{

						$transaction->commit();
						$this->message =$this->createMessageCss('success',Yii::t('app', 'Create  University User Responsed Successfuly.'));
						$this->setFlashMsg('user_resp_msg', 	$this->message);
						return true;

					}else
					{
						$this->message =$this->createMessageCss('danger',Yii::t('app', 'Create  University User Responsed Fail.') );
                        return false;
					}


				}else{
					if(count($this->_model->errors)<1)
					   $this->message =$this->createMessageCss('danger',Yii::t('app', 'Create  University User Responsed Fail.') );
					return false;
				}
			}


		} catch (Exception $e) {
			$transaction->rollback();
			$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item}  Fail to create .Try again.', array('item' => Yii::t('app', 'User'))));
			return false;
		}


	}





	public function actionUpdateUserResp($autorizedUniId) {


		if (($autorizedUniObj = AuthorizedUniversity::findOne($autorizedUniId)) !== null) {

			/*check uniq user response for autorized university */
			if(!$autorizedUniObj->hasUniversityRespUser())
			{


				$message=$this->createMessageCss('danger',Yii::t('app','University User Response Dosent Exist'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/university-resp/index']);

			}

			/*check Just deine User response for own event by EventAdmin */
			if($autorizedUniObj->eventId==$this->currentEventId)
			{

				$message=$this->createMessageCss('danger',Yii::t('app','You Just Access Your Event'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/university-resp/index']);
			}

		} else {

			$message=$this->createMessageCss('danger',Yii::t('app','You Just Access Your Event'));
			$this->setFlashMsg('user_resp_msg', $message);
			return $this->redirect(['/YumUsers/university-resp/index']);

		}


		/*Find Related UserDetail*/
		$message = '';
		if($autorizedUniObj->getUniversityRespUserObj()!=null)
		{
			$userRespId=$autorizedUniObj->respUserObj->id;
		}else
		{

			$message=$this->createMessageCss('danger',Yii::t('app','User Not Found'));
			$this->setFlashMsg('user_resp_msg', $message);
			return $this->redirect(['/YumUsers/university-resp/index']);

		}


		$model = $this->findModel($userRespId);
		$model->scenario='updateUserResp';

		/*becuase passwordInput is not field of user table not set in $model->load() */
		$model->passwordInput=(isset($_POST['UniversityRespUser']['passwordInput']))?$_POST['UniversityRespUser']['passwordInput']:'';


		if ($model->load(Yii::$app->request->post())) {
			if ($model->updateUniversityRespUser()) {

				$message=$this->createMessageCss('success',Yii::t('app', 'Update  University User Responsed Successfuly.'));
				$this->setFlashMsg('user_resp_msg', $message);
				return $this->redirect(['/YumUsers/university-resp/index']);


			}
		}

		return $this->render('update_user_resp', [
			'model' => $model,
			'message'=>$message,
			'autorizedUniObj'=>$autorizedUniObj,
			// 'targetId' => isset($model->targetUser) ? $model->targetUser : 0,
		]);
	}



//	public function actionSetActive($autorizedUniId,$value) {
//
//		Yii::$app->response->format = Response::FORMAT_JSON;
//
//		if (Yii::$app->request->isAjax) {
//
//			if(!in_array($value,[0,1])||!is_numeric($autorizedUniId))
//			{
//				echo Json::encode([
//					'success' => false,
//					'messages' => Yii::t('app','Invalid Params'),
//
//				]);
//				return;
//			}
//
//
//			if (($autorizedUniObj = AuthorizedUniversity::findOne($autorizedUniId)) !== null) {
//
//				/*check uniq user response for autorized university */
//				if(!$autorizedUniObj->hasUniversityRespUser())
//				{
//					echo Json::encode([
//						'success' => false,
//						'messages' => Yii::t('app','University User Response Dosent Exist')
//					]);
//					return;
//				}
//
//				/*check Just deine User response for own event by EventAdmin */
//				//if($autorizedUniObj->eventId!=$_SESSION['eventId'])
//				if(0)
//				{
//					echo Json::encode([
//						'success' => false,
//						'messages' => Yii::t('app','You Just Access Your Event')
//					]);
//					return;
//
//				}
//
//
//			} else {
//				echo Json::encode([
//					'success' => false,
//					'messages' => Yii::t('app','You Just Access Your Event')
//				]);
//				return;
//			}
//
//
//			/*Find Related UserDetail*/
//			$message = '';
//			if($autorizedUniObj->getUniversityRespUserObj()!=null)
//			{
//				$userRespId=$autorizedUniObj->respUserObj->id;
//			}else
//			{
//				echo Json::encode([
//					'success' => false,
//					'messages' => Yii::t('app','User Not Found')
//				]);
//				return;
//			}
//			$model = $this->findModel($userRespId);
//			if ($model->setActiveVal($value)) {
//
//				if($value)
//					$message=Yii::t('app', '{item} Successfully Actived.', ['item' => Yii::t('app', 'User')]);
//				else
//					$message=Yii::t('app', '{item} Successfully Deactived.', ['item' => Yii::t('app', 'User')]);
//
//				echo Json::encode([
//					'success' => true,
//					'messages' =>$message ]);
//
//			} else {
//				echo Json::encode([
//					'success' => false,
//					'messages' => Yii::t('app','Failed To Deactive Items To {item}.', ['item' => Yii::t('app', 'User')])]);
//
//			}
//		}
//
//	}




	public function actionDelete($id) {

		$autorizedUniId=$id;


		if (Yii::$app->request->isAjax) {

			if (($autorizedUniObj = AuthorizedUniversity::findOne($autorizedUniId)) !== null) {
				/*check uniq user response for autorized university */
				if(!$autorizedUniObj->hasUniversityRespUser())
				{
					echo Json::encode([
						'success' => false,
						'messages' =>[ Yii::t('app','University User Response Dosent Exist')]
					]);
					return;
				}

				/*check Just deine User response for own event by EventAdmin */
				if($autorizedUniObj->eventId==$this->currentEventId)
				{
					echo Json::encode([
						'success' => false,
						'messages' =>[ Yii::t('app','You Just Access Your Event')]
					]);
					return;

				}


			} else {
				echo Json::encode([
					'success' => false,
					'messages' => [Yii::t('app','You Just Access Your Event')]
				]);
				return;
			}


			/*Find Related UserDetail*/
			$message = '';
			if($autorizedUniObj->getUniversityRespUserObj()!=null)
			{
				$userRespId=$autorizedUniObj->respUserObj->id;
			}else
			{
				echo Json::encode([
					'success' => false,
					'messages' => Yii::t('app','User Not Found')
				]);
				return;
			}


			$model = $this->findModel($userRespId);
			if ($model->deleteUniversityResponsedUser($autorizedUniObj->id)) {
				echo Json::encode(
					[
						'success' => true,
						'messages' => [
							Yii::t('app', '{item} Successfully Deleted.',
								[
									'item' => Yii::t('app', 'User')
								]
							)
						]
					]
				);
			} else {
				echo Json::encode([
					'success' => false,
					'messages' => Yii::t('app','Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')])]);

			}
		}

	}





	public function createMessageCss($msgType,$string)
	{

		switch ($msgType) {
			case 'success':
				$message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;

			case 'danger':
				$message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
			default:
				$message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
		}

		return $message;

	}



	public  function setFlashMsg($name,$message)
	{
		$session = Yii::$app->session;
		$session->setFlash($name, $message);
	}



	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		$model=UniversityRespUser::find()->where('id = :id And valid=1', [':id' => (int)$id])->one();
		if ($model !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException(Yii::t('app','The  user does not exist.'));
		}
	}




}