<?php

namespace backend\modules\YumUsers\controllers;


use Yii;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\modules\YumUsers\models\ChangePassword;
use backend\modules\YumUsers\models\UsersDetails;

use yii\data\Pagination;
use yii\data\Sort;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;
use backend\modules\YiiFileManager\models\UserFolders;

use backend\models\EventMembers;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
  public $_model;
	public $_fileModel;
	public $_fileAttribute;
	public $_userDetails;


/*Access control properties*/
	public $currentEventId;
	public $currentEventName;
	public $userId;

   public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
					 [
						'allow' => true,
						'actions' => ['check-national-id-exist','create','index','update-user','detail','edit2','delete','search-referee','view','new-user-info','complete-edit'],
						'roles' => ['admin',User::Role_SeniorUserEvent],
					],
					[
						'allow' => true,
						'actions' => ['check-national-id-exist'],
						'roles' => [User::Role_UniversityRepresentative,User::Role_ExecutiveCommittee,'ManageTechnicalCommittee'],
					],

				],
			],
		];
	}





	public function provideSemanticAccessControlData($value='')
	{

		$eventId=Yii::$app->user->identity->eventId;

		/*-------------Get Current Event Id Selected User----------------------------*/
		if (!empty( $eventId) ) {
			$this->currentEventId=$eventId ;
		}else
		{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}

	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionIndex() {
	  /*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();



		$request = Yii::$app->request;
		$searchModel = new UserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams,$this->currentEventId);

		$columns = [
			['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'fileId',
				'header' => Yii::t('app', 'Picture'),
				'enableSorting'=>false,
				'format' => 'raw',
				'value' => function ($model) {
					 return $model->getIconThumbnail($model->fileId);

				},
			],

			[
				'attribute' => 'last_name',
				'header' => Yii::t('app', 'Full Name'),
				'enableSorting'=>false,
				'value' => function($model){
					return $model->FullName;
				},
			],
			[
				'attribute' => 'username',
				'enableSorting'=>false,
			],
			[
				'attribute' => 'mobile',
				'enableSorting'=>false,
			],
			[
				'attribute' => 'email',
				'format' => 'email',
				'enableSorting'=>false,
			],
			[
				'attribute' => 'gender',
				'format' => 'raw',
				'enableSorting'=>false,
				'value' => function($model){
					return $model->Sex;
				},
			],

			[
				'attribute' => 'last_login_time',
				'format' => 'raw',
				'enableSorting'=>false,
				'value' => function($model){
					return $model->LastLogintime;
				},
			],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '140'],
				'template' => '{changepassbyadmin} {view} {update} {delete}',
				//'template' => '{changepassbyadmin} {changerolebyadmin} {view} {update} {delete}',
				'buttons' => [

					'changepassbyadmin' => function ($url, $model) {
						return  Html::a(
							'<span class="glyphicon glyphicon-lock font-purple-plum"></span>',
							Url::to(['/YumUsers/admin/changepasswordbyadmin','usertargetid'=>$model->id]),
							[
								'data-hint' => Yii::t('app','Change Password'),
								'class' => 'hint--top hint--rounded hint--info'
								// 'class' => 'showModalButton btn btn-success',
								// 'style'=>'padding:1px;'
							]
						);
					},
//					'changerolebyadmin' => function ($url, $model) {
//						return  Html::a(
//							'<span class="glyphicon glyphicon-user font-yellow-gold"></span>',
//							Url::to(['/YumUsers/admin/changerolebyadmin','usertargetid'=>$model->id]),
//							[
//								'data-hint' => Yii::t('app','Change Role'),
//								'class' => 'hint--top hint--rounded hint--info'
//								// 'class' => 'showModalButton btn btn-success',
//								// 'style'=>'padding:1px;'
//							]
//						);
//					},
						 'update' => function ($url, $model) {
						return  Html::a(
							'<span class="glyphicon glyphicon-pencil font-yellow"></span>',
							Url::to(['/YumUsers/user/update-user','userId'=>$model->id]),
							[
								'data-hint' => Yii::t('app','Update'),
								'class' => 'hint--top hint--rounded hint--info'
								// 'class' => 'showModalButton btn btn-success',
								// 'style'=>'padding:1px;'
							]
						);
					},


							   ],


			],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 2, 'asc' ]
			],
			'columnDefs'=> [
				[
					'orderable' => false,
					'targets' => [0, 1, count($columns) - 1]
				]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = array(NULL, NULL, 'last_name','username','mobile', 'email','gender','grade');
		$searchableColumn = array('name','last_name','username','mobile', 'email','sportNo','eName','eFamily','fatherName','idNo','nationalId');

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}



	public function actionCreate()
	{

	   /*-----set EventId,EventName From Session----*/
	   $this->provideSemanticAccessControlData();

		$message = '';
		$failMessage = '';
		$this->_userDetails = new UsersDetails();
		$this->_model = new User();

		$this->_model->scenario='createUser';


		/*******************-------BEGIN Transaction----------*****************/
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();



		try {
		/*******************-------BEGIN Transaction----------*****************/

		/*******************-------Save new Athelete----------*****************/
		if ($this->_model->load(Yii::$app->request->post())) {


			$this->_model->fileAttr=  \yii\web\UploadedFile::getInstance($this->_model, 'fileAttr');


			/*set event id  that provided from session .set admin user event id for user event*/
			$this->_userDetails->eventId=$this->currentEventId;

			if ( $this->_userDetails->validate(['eventId','sportAssuranceNo']) &&$this->_model->validate('fileAttr')&& $this->_model->createUser() )
			{


				/**********if Upload file Save Pic  User **************/
				if($this->_model->uploadPicFile())
				{
					$message=$this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
				}
				else
				{
					$failMessage=$this->createMessageCss('danger', Yii::t('app', '{item} Successfully Added User expect File.', array('item' => Yii::t('app', 'User'))));

				}
				/**********save detail event id and user id**************/

				if ($this->_userDetails->saveUserDetails($this->_model->id)) {

				/**********if created  User EventMember Info**************/


						if($this->_model->insertMemberEventInfo($this->_userDetails->id,$this->_userDetails->eventId,0,0,0))
						{
						$transaction->commit();
						$message=$this->createMessageCss('success',Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
						$message=$message.$failMessage;
						$this->setFlashMsg('manage_team_member', $message);
						return $this->redirect(['/YumUsers/user/index']);
						}else
						{
					   throw new NotFoundHttpException( "Problem in Add User Team info" );
						}


					/**********if created  User athlete add in team**************/
				}
				else
				{
				  throw new NotFoundHttpException( "Problem in Add User Details" );

				}




			}
		}


	} catch(Exception $e) {
		$transaction->rollback();
		$message.=$this->createMessageCss('success',Yii::t('app', '{item}  Fail to create .Try again.', array('item' => Yii::t('app', 'User'))));
	}



		return $this->render('create_user', [
					'model' => $this->_model,
					'message' => $message,
					'userDetails'=>$this->_userDetails,

		]);
	}




	public function actionUpdateUser($userId) {

	/*-----set EventId,EventName From Session----*/
	$this->provideSemanticAccessControlData();
	$message='';
	$model=$this->findModel($userId);

	/*----------Semantic Access Control base on Event Id-------------*/
	if(!Yii::$app->user->can(User::Role_Admin)&&!$model->isUserMemberOfEvent($this->currentEventId,$userId))
	{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
	}



	$userDetails=$model->getUserDetailBaseOnEvent($this->currentEventId);
	$model->scenario='UpdateUser';



	/**-------Update Personal info-------**/
	if ($model->load(Yii::$app->request->post())&&isset($_POST['update_prsonal_info_btn']) && $model->save()) {

					$message=$this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]) );
	}
	/**----------------------------------**/


	/**--------Update Picture -----------**/
	if ($model->load(Yii::$app->request->post())&&isset($_POST['update_pic_btn'])) {
			if($model->uploadPicFile())
				{
					$message=$this->createMessageCss('success',Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]));

				}
	}
	/**-----------------------------------**/

	/**-----------------------------changePassword ------------------------------------------------**/
	$changePasswordModel = new ChangePassword(['scenario'=>'change_password_byadmin']);
	$changePasswordModel->username = Yii::$app->user->identity->username;
	$changePasswordModel->passwordInput=  isset($_POST['ChangePassword']['passwordInput'])?$_POST['ChangePassword']['passwordInput']:'';
	$changePasswordModel->targetUser = (int) $userId;

	if ($changePasswordModel->load(Yii::$app->request->post())&&isset($_POST['change_pass_btn'])) {


		if ($changePasswordModel->load(Yii::$app->request->post()) ) {
			/*---check Password----*/
			if ($resultValidate=$changePasswordModel->validate() && $resultVerifyPass=$changePasswordModel->ValidateCurrentPassword())
			{
				if ($changePasswordModel->changePassword())
				{
					$message=$this->createMessageCss('success',Yii::t('app', 'Changed Password Successfully .'));

				}
				else{
					$message=$this->createMessageCss('error',Yii::t('app', 'Error in changed password!'));
				}
			}
			elseif (!$resultValidate||!$resultVerifyPass)
			{
					$message=$this->createMessageCss('error',Yii::t('app', 'Error in changed password!'));
			}
		}

	}
	/**-----------------------------------------------------------------------------------------------**/





	/**-------Update User Detail Personal info-------**/
		//$userDetails->scenario='UpdateUserDetailse';
		if ($userDetails->load(Yii::$app->request->post())&&isset($_POST['update_user_detail_info_btn']) && $userDetails->save()) {

			try {
				 $uploadStuCartResult=$userDetails->uploadFileStuCart() ;
			}catch (Exception $e) {
				throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن کارت دانشجویی پیش آمده است.');
			}

			try {
				$uploadStuAssuranceCartResult=$userDetails->uploadFileAssuranceCart();
			}catch (Exception $e) {
			   throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن  کارت یمه پیش آمده است.');
			}

			$message=$this->createMessageCss('success',Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));



		}
	/**--------------------------------------------**/



		return $this->render('update_user', [
					'model' =>$model,
					'massage' => $message,
					'userDetails'=>$userDetails,
					'changePasswordModel'=>$changePasswordModel

		]);
	}





   /**
	 * Displays a single User model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		/*--check Accesscontrole base on EventId--*/
		$this->provideSemanticAccessControlData();

		 $model=$this->findModel($id);
		 /*----------Semantic Access Control base on Event Id-------------*/
		if(!Yii::$app->user->can(User::Role_Admin)&&!$model->isUserMemberOfEvent($this->currentEventId,$id))
		{
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		 }

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view_user', [
				'model' =>$model,
				'eventId'=>$this->currentEventId
			]);
		}
		else{
			return $this->render('view_user', [
				'model' =>$model,
				'eventId'=>$this->currentEventId
			]);
		}
	}



	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */

	public function actionDelete($id = NULL) {


		/*--check Accesscontrole base on EventId--*/
		$this->provideSemanticAccessControlData();

		 $model=$this->findModel($id);
		 /*----------Semantic Access Control base on Event Id-------------*/
	   if(!Yii::$app->user->can(User::Role_Admin)&&!$model->isUserMemberOfEvent($this->currentEventId,$id))
		{

				echo Json::encode([
					'success' => false,
					'messages' => [Yii::t('app', 'The Security error')]]);
				return;
		 }

		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {

			if (isset($post['id']))
				$id = (int) $post['id'];
			else if (isset($_GET['id']))
				$id = (int) $_GET['id'];

			$model=$this->findModel($id);
			if ($model->delete()) {

				echo Json::encode([
					'success' => true,
					'messages' => [Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')])]]);
			} else {
				echo Json::encode([
					'success' => false,
					'messages' => [Yii::t('app', '{item} Fail Delete.', ['item' => Yii::t('app', 'User')])]]);
				return;

			}
		}

	}


	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{

		$model=User::find()->where('id = :id And valid=1', [':id' => (int)$id])->one();
		if ($model !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}




			public function createMessageCss($msgType,$string)
   {

	   switch ($msgType) {
		   case 'success':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
					break;

		   case 'danger':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
					break;
		   default:
				 $message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
			   break;
	   }

	   return $message;

   }


   public  function setFlashMsg($name,$message)
   {
				$session = Yii::$app->session;
				$session->setFlash($name, $message);
   }


	public function actionCheckNationalIdExist($nationalId)
	{

//
//		if(!$this->validateNationalId($nationalId))
//		{
//			$msg= Yii::t('app', 'فرمت کدملی صحیح  نیست');
//			echo json_encode(array('person' => null, 'userDetailResult' => null, 'error_nationalID' => false));
//			return;
//		}

		$model = User::find()->select(['id', 'username', 'name', 'last_name', 'gender', 'homepage', 'tel', 'mobile', 'email', 'fatherName', 'idNo', 'birthDate', 'birthCityId'])
			->where('nationalId= :nationalId And valid=1', [':nationalId' => $nationalId])
			->one();

		if ($model !== null) {
			/*prevent to load admin informations*/
			if (Yii::$app->getAuthManager()->checkAccess($model->id, User::Role_Admin)) {
				echo json_encode(array('person' => null, 'userDetailResult' => null, 'exist' => false));
				return;
			}

			/*prevent to load SeniorUserEvent admin informations*/
			if (!Yii::$app->user->can(User::Role_SeniorUserEvent)) {
				if (Yii::$app->getAuthManager()->checkAccess($model->id, User::Role_SeniorUserEvent)) {
					echo json_encode(array('person' => null, 'userDetailResult' => null, 'exist' => false));
					return;
				}

			}


			$eventMemObj = $model->eventMembersByEvent;

			$userDetailsObj = $model->getUserDetailBaseOnEvent(Yii::$app->user->identity->eventId);

			if ($eventMemObj != null) {
				$model->caravanId = $eventMemObj->caravanId;
        $model->teamId = $eventMemObj->teamsId;
        $model->userType = $eventMemObj->user_type_id;
			}

			$userDetailResult=null;
			if ($userDetailsObj != null) {
				//$userDetailResult = $userDetailsObj->getAttributes(['sportMembershipHistory','educationalField','educationalSection','stuNo','sportAssuranceNo','sportFieldId','stuCartFileId','assuranceCartFileId']);
				$userDetailResult = $userDetailsObj->getAttributes();
			}

			$userResult = $model->getAttributes([ 'username', 'name', 'last_name', 'gender', 'homepage', 'tel', 'mobile', 'email', 'fatherName', 'idNo', 'birthDate', 'birthCityId']);
			$userResult['caravanId'] = $model->caravanId;
      $userResult['teamId'] = $model->teamId;
      $userResult['userType'] = $model->userType;

			echo json_encode(array('person' => $userResult, 'userDetailResult' => $userDetailResult, 'exist' => true));
			return;

		} else {
			echo json_encode(array('person' => null, 'userDetailResult' => null, 'exist' => false));
			return;
		}
	}
	public  function validateNationalId($nationalID)
	{
		$param =$nationalID;

		if(strlen($param) < 8){
			return false;
		}

		if(strlen($param) == 8)
			$param = "00" . $param ;

		if(strlen($param) == 9)
			$param = "0" . $param ;

		if(strlen($param) > 10){
			return false;
		}

		$chk = substr($param,9,1) ;
		$s = 0;

		for($i=1;$i<=9;$i++)
			$s = $s + intval(substr($param,$i-1,1)) * (11 - $i);

		$r = $s % 11 ;

		if(($r<2 && $chk==$r) || ($r>=2 && $chk==11-$r)){
			return true;
		}
		else{
			return false;
		}

		return false;

	}
}
