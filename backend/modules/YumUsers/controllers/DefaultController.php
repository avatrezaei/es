<?php

namespace backend\modules\YumUsers\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
	public function actionIndex()
	{
			return $this->redirect(['/YumUsers/userauth/signup']);
   }
   
}
