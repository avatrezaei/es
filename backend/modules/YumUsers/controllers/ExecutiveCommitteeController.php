<?php

namespace backend\modules\YumUsers\controllers;


use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;

use backend\modules\YumUsers\models\ChangePassword;
use backend\modules\YumUsers\models\ExecutiveCommitteeUser;
use backend\modules\YumUsers\models\ExecutiveCommitteeUserSearch;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UsersDetails;
use backend\modules\YumUsers\models\UsersType;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * UserController implements the CRUD actions for User model.
 */
class ExecutiveCommitteeController extends Controller
{

    public $_model;
    public $_fileModel;
    public $_fileAttribute;
    public $_userDetails;


    /*Create user properties*/
    public $message;
    public $failMessage;
    public $saveTypeUser;//related to create by duplicate nationalid


    /*Access control properties*/
    public $currentEventId;
    public $currentEventName;
    public $userId;

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['create', 'index', 'update-referee', 'delete', 'view'],
				'rules' => [
					[
						'actions' => ['delete'],
						'roles' => ['deleteExecutive'],
						'allow' => true,
					],
					[
						'actions' => ['create'],
						'roles' => ['createExecutive'],
						'allow' => true,
					],
					[
						'actions' => ['index'],
						'roles' => ['adminExecutive'],
						'allow' => true,
					],
					[
						'actions' => ['update-referee'],
						'roles' => ['updateExecutive'],
						'allow' => true,
					],
					[
						'actions' => ['view'],
						'roles' => ['viewExecutive'],
						'allow' => true,
					],
					[
						'actions' => ['user-move-event'],
						'roles' => ['moveEventExecutive'],
						'allow' => true,
					],
					[
						'actions' => ['create', 'index', 'update-referee', 'delete', 'view','user-move-event'],
						'roles' => ['ManageExecutive'],
						'allow' => true,

					],

				],
			],
		];
	}

    public function provideSemanticAccessControlData($value='')
    {

        $eventId=Yii::$app->user->identity->eventId;


        /*-------------Get Current Event Id Selected User----------------------------*/
        if (!empty( $eventId) ) {
            $this->currentEventId=$eventId ;
        }else
        {
            throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
        }

    }


    public function actionIndex()
    {
        /*-----set EventId,EventName From Session----*/
        $this->provideSemanticAccessControlData();


        $request = Yii::$app->request;
        $searchModel = new ExecutiveCommitteeUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->currentEventId);

        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'fileId',
                'header' => Yii::t('app', 'Picture'),
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getIconThumbnail($model->fileId);

                },
				'filter' => false,

			],

			[
				'header' => Yii::t('app', 'Full Name'),
				'attribute' => 'last_name',
				'enableSorting' => false,
				'filter' => false,
				'format' => 'html',
				'value' => function ($model) {
					return Html::a(
						Html::encode($model->FullName),
						Url::to(['/YumUsers/executive-committee/view', 'id' => $model->id])
					);
				},

			],
            [
                'attribute' => 'username',
                'enableSorting' => false,
				'filter' => false,

			],
            [
                'attribute' => 'mobile',
                'enableSorting' => false,
				'filter' => false,

			],
            [
                'attribute' => 'email',
                'format' => 'email',
                'enableSorting' => false,
				'filter' => false,

			],
            [
                'attribute' => 'gender',
                'format' => 'raw',
				'filter' => false,

				'enableSorting' => false,
                'value' => function ($model) {
                    return $model->Sex;
                },
            ],
			[
				'header' => Yii::t('app', 'User Title'),
				'attribute' => 'userType',
				'filter' =>ExecutiveCommitteeUser::getUserExecutiveCommitteeType(),
				'filterOptions' => ['id' => 'executive-committee-user-type'],
				'format' => 'raw',
				'enableSorting' => false,
				'value' => function ($model){
					/*use joined data to show user type txt*/
					return $model->getUserTypeTxt();
				},
			],

            [
                'attribute' => 'last_login_time',
                'format' => 'raw',
				'filter' => false,

				'enableSorting' => false,
                'value' => function ($model) {
                    return $model->LastLogintime;
                },
            ],

            [
                'class' => 'fedemotta\datatables\EActionColumn',
                'header' => Yii::t('app', 'Actions'),
                'headerOptions' => ['width' => '140'],

				'visibleButtons' => [
					'moveEvent' => (Yii::$app->user->identity->eventId==1),
				],
                'template' => '{moveEvent}{changepassbyadmin} {sendMessage} {view} {update} {delete}',
                'buttons' => [
                    'changepassbyadmin' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-lock font-purple-plum"></span>',
                            Url::to(['/YumUsers/admin/changepasswordbyadmin', 'usertargetid' => $model->id]),
                            [
                                'data-hint' => Yii::t('app', 'Change Password'),
                                'class' => 'hint--top hint--rounded hint--info'
                                // 'class' => 'showModalButton btn btn-success',
                                // 'style'=>'padding:1px;'
                            ]
                        );
                    },
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil font-yellow"></span>',
                            Url::to(['/YumUsers/executive-committee/update-referee', 'userId' => $model->id]),
                            [
                                'data-hint' => Yii::t('app', 'Update'),
                                'class' => 'hint--top hint--rounded hint--info'
                                // 'class' => 'showModalButton btn btn-success',
                                // 'style'=>'padding:1px;'
                            ]
                        );
                    },
					'sendMessage' => function ($url, $model){
						return Html::a(
							'<i class="fa fa-envelope font-yellow"></i>',
							Url::to(['/messages/send', 'id' => $model->id]),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'Send Message'),
							]
						);
					},
					'moveEvent' => function ($url, $model){

						if(!$model->isUserMemberOfEvent(5, $model->id))
						{
							return Html::a(
								'<i class="fa fa-exchange font-blue"   aria-hidden="true"></i>',
								Url::to(['/YumUsers/executive-committee/user-move-event', 'id' => $model->id]),
								[
									'class' => ' status hint--top hint--rounded hint--info',
									'data-hint' => Yii::t('app', 'Move to Event (son)'),
									'data' => [
										'confirm' => Yii::t('app', 'Are you sure you want to Move this item?'),
										'method' => 'post',
									],
								]
							);
						}
						else{
							return '<i class="fa fa-check font-grean"   aria-hidden="true" title="'.Yii::t('app','').'"></i>';
						}

					},

                ],
            ],
        ];

        $clientOptions = [
            'ajax' => [
                'url' => $request->url,
            ],
            'order' => [
                [2, 'desc']
            ],
        ];

        $start = $request->get('start', 0);
        $length = $request->get('length', 10);

        /**
         * Create Pagination Object for paging:
         */
        $_pagination = new Pagination;
        $_pagination->pageSize = $length;
        $_pagination->page = floor($start / $length);

        $dataProvider->pagination = $_pagination;

        $sortableColumn = array(NULL, 'last_name', 'username', 'mobile', 'email', 'gender', 'grade');
        $searchableColumn = array('name', 'last_name', 'username', 'mobile', 'email', 'sportNo', 'eName', 'eFamily', 'fatherName', 'idNo', 'nationalId');

        $widget = Yii::createObject([
                'class' => 'fedemotta\datatables\DataTables',
                'dataProvider' => $dataProvider,
                'filterModel' => NULL,
                'columns' => $columns,
				'filterModel' => $searchModel,
				'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
                'clientOptions' => $clientOptions,
                'sortableColumn' => $sortableColumn,
                'searchableColumn' => $searchableColumn,
            ]
        );

        if ($request->isAjax) {
            $result = $widget->getFormattedData($request->post('draw'));

            echo json_encode($result);
            Yii::$app->end();
        } else {
            return $this->render('index', [
                'widget' => $widget,
            ]);
        }
    }


    public function actionCreate()
    {

        /*-----set EventId,EventName From Session----*/
        $this->provideSemanticAccessControlData();

        $this->message = '';
        $saveTypeUser = 0;
        $this->failMessage = '';
        $this->_userDetails = new UsersDetails();
        $this->_model = new ExecutiveCommitteeUser();

        $this->_model->scenario = 'createExecutiveCommitteeUser';
        $this->_userDetails->scenario = 'CreateTechCommiteeDetailse';


		if (isset($_POST['ExecutiveCommitteeUser']['nationalId'])) {
			$nationalId = $_POST['ExecutiveCommitteeUser']['nationalId'];
			$userModel = ExecutiveCommitteeUser::find()->where('nationalId= :nationalId AND valid=1 ', [':nationalId' => $nationalId])
				->one();
			if ($userModel != null) {
				$userDetails = $userModel->getUserDetailBaseOnEvent(Yii::$app->user->identity->eventId);
				if ($userDetails != null) {
					$saveTypeUser = ExecutiveCommitteeUser::CREATE_EXIST_USER_EVENT;
					$this->_model = $userModel;
					$this->_userDetails = $userDetails;


				} else {
					$saveTypeUser = ExecutiveCommitteeUser::CREATE_NEW_USER_EVENT;
					$this->_model = $userModel;

				}
			} else {

				$saveTypeUser = ExecutiveCommitteeUser::CREATE_NEW_USER;
			}
		}


		switch ($saveTypeUser) {
			case ExecutiveCommitteeUser::CREATE_NEW_USER:
				$this->_model->scenario = 'createExecutiveCommitteeUser';
				$this->_userDetails->scenario = 'ExecutiveCommitteeUserDetailse';

				if ($this->createNewExecutive($saveTypeUser)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/executive-committee']);
				}
				break;


			case ExecutiveCommitteeUser::CREATE_NEW_USER_EVENT:

				$this->_model->scenario = 'creaExecutiveByExistUser';
				$this->_userDetails->scenario = 'ExecutiveCommitteeUserDetailse';

				if ($this->createNewExecutive($saveTypeUser)) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/executive-committee']);
				}
				break;


			case ExecutiveCommitteeUser::CREATE_EXIST_USER_EVENT:
				$this->_model->userType=$_POST['ExecutiveCommitteeUser']['userType'];
				if ($this->_model->isDuplicateEventUser(0, $this->_userDetails->id, 0, $this->_model->userType, 0,0,0)) {
					$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item} Already exsist.', array('item' => Yii::t('app', 'User'))));
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/executive-committee']);
					break;
				}

				$this->_model->scenario = 'creaExecutiveByExistUser';
				$this->_userDetails->scenario = 'ExecutiveCommitteeUserDetailse';

				if ($this->createNewExecutive()) {
					$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
					$this->message = $this->message . $this->failMessage;
					$this->setFlashMsg('manage_team_member', $this->message);
					return $this->redirect(['/YumUsers/executive-committee']);
				}
				break;


			default:
				break;
		}


		return $this->render('create_referee', [
			'model' => $this->_model,
			'message' => $this->message,
			'userDetails' => $this->_userDetails,

		]);
	}


	public function createNewExecutive()
	{
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			/*---BEGIN Transaction---*/

			/*--Save new createNewExecutive--*/
			if ($this->_model->load(Yii::$app->request->post())) {

				//set for validate
				$this->_model->fileAttr = \yii\web\UploadedFile::getInstance($this->_model, 'fileAttr');

				/*-Set nationalId as Default username And password-*/
				if($this->_model->scenario!='creaExecutiveByExistUser'){
					$this->_model->username = isset($this->_model->nationalId) ? $this->_model->nationalId : '';
				}

				$loadDetailUser = $this->_userDetails->load(Yii::$app->request->post());
				/*set event id  that provided from session .set admin user event id for user event*/
				$this->_userDetails->eventId = $this->currentEventId;


			   if ($this->_userDetails->validate(['eventId', 'userPosition']) && $this->_model->validate('fileAttr') && $this->_model->createExecutiveCommitteeUser()) {

					if ($loadDetailUser) {


						/*-if Upload file Save Pic  User athlete add in team-*/
						//  if (1) {
						if ($this->_model->uploadPicFile()) {
							$this->message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'User'))));
						} else {
							$this->message .= $this->createMessageCss('danger', Yii::t('app', '{item} Successfully Added User expect File.', array('item' => Yii::t('app', 'User'))));

						}


						if ($this->_userDetails->saveUserDetails($this->_model->id)) {

							if ($this->_model->insertMemberEventInfo($this->_userDetails->id, $this->currentEventId, 0, 0, 0)) {
								$transaction->commit();
								return true;
							} else {
								throw new NotFoundHttpException("Problem in Add User Team info");

							}

						} else {
							// die(var_dump( $this->_userDetails->scenario));
							throw new NotFoundHttpException("Problem in Add User Details");

						}


					}

				}
			}


		} catch (Exception $e) {
			$transaction->rollback();
			$this->message .= $this->createMessageCss('success', Yii::t('app', '{item}  Fail to create .Try again.', array('item' => Yii::t('app', 'User'))));
			return false;
		}


	}



	public function actionUpdateReferee($userId)
	{

		/*-----set EventId,EventName From Session----*/
		$this->provideSemanticAccessControlData();
		$message = '';
		$model = $this->findModel($userId);

		/*----------Semantic Access Control base on Event Id-------------*/
		if (!$model->isUserMemberOfEvent($this->currentEventId, $userId)) {
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}


		$userDetails = $model->getUserDetailBaseOnEvent($this->currentEventId);


		$model->scenario = 'UpdateExecutiveCommitteeUser';


		/**-------Update Personal info-------**/
		if ($model->load(Yii::$app->request->post()) && isset($_POST['update_prsonal_info_btn']) && $model->save()) {

			$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));
		}
		/**----------------------------------**/


		/**--------Update Picture -----------**/
		if ($model->load(Yii::$app->request->post()) && isset($_POST['update_pic_btn'])) {
			if ($model->uploadPicFile()) {
				$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]));

			}
		}
		/**-----------------------------------**/

		/**--------changePassword -----------**/
		$changePasswordModel = new ChangePassword(['scenario' => 'change_password_byadmin']);
		$changePasswordModel->username = Yii::$app->user->identity->username;
		$changePasswordModel->passwordInput = isset($_POST['ChangePassword']['passwordInput']) ? $_POST['ChangePassword']['passwordInput'] : '';
		$changePasswordModel->targetUser = (int)$userId;

		if ($changePasswordModel->load(Yii::$app->request->post()) && isset($_POST['change_pass_btn'])) {


			if ($changePasswordModel->load(Yii::$app->request->post())) {
				/*---check Password----*/
				if ($resultValidate = $changePasswordModel->validate() && $resultVerifyPass = $changePasswordModel->ValidateCurrentPassword()) {
					if ($changePasswordModel->changePassword()) {
						$message = $this->createMessageCss('success', Yii::t('app', 'Changed Password Successfully .'));

					} else {
						$message = $this->createMessageCss('error', Yii::t('app', 'Error in changed password!'));
					}
				} elseif (!$resultValidate || !$resultVerifyPass) {
					$message = $this->createMessageCss('error', Yii::t('app', 'Error in changed password!'));
				}
			}

		}
		/**-----------------------------------**/


		/**-------Update User Detail Personal info-------**/
		$userDetails->scenario = 'UpdateUserByPositionUserDetailse';
		$userDetails->validateExecutiveUserType = true;

		if ($userDetails->load(Yii::$app->request->post()) && isset($_POST['update_user_detail_info_btn']))
		{

			$_data = Yii::$app->request->post('UsersDetails');
			if($userDetails->validate(['userPosition','userType']))
			{
				$userDetails->userPosition = $_data['userPosition'];

				if($userDetails->save()&&$userDetails->UpdateUserType()){
					$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));

				}

			}


		}
		/**--------------------------------------------**/


		return $this->render('update_athelete', [
			'model' => $model,
			'massage' => $message,
			'userDetails' => $userDetails,
			'changePasswordModel' => $changePasswordModel

		]);
	}





	/**
	 * Displays a single User model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		/*--check Accesscontrole base on EventId--*/
		$this->provideSemanticAccessControlData();

		$model = $this->findModel($id);
		$this->currentEventId=Yii::$app->user->identity->eventId;

		/*----------Semantic Access Control base on Event Id-------------*/
		if (!$model->isUserMemberOfEvent($this->currentEventId, $id)) {
			throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
		}

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view_user', [
				'model' => $model,
				'eventId' => $this->currentEventId
			]);
		} else {
			return $this->render('view_user', [
				'model' => $model,
				'eventId' => $this->currentEventId
			]);
		}
	}

	public function actionDelete($id = NULL)
	{
		$this->provideSemanticAccessControlData();

		$post = Yii::$app->request->post();
		if (Yii::$app->request->isAjax) {

			if (isset($post['id']))
				$id = (int)$post['id'];
			else if (isset($_GET['id']))
				$id = (int)$_GET['id'];

			$model = $this->findModel($id);

			/*----------Semantic Access Control base on Event Id-------------*/
			if (!$model->isUserMemberOfEvent($this->currentEventId, $id)) {
				throw new \yii\web\HttpException(400, Yii::t('app', 'The Security error'));
			}
			$result=$model->deleteExecutiveCommitteeByAdmin($id);
			if ($result['result']) {

				echo Json::encode([
					'success' => true,
					'messages' => [$result['msg']]]);
			} else {
				echo Json::encode([
					'success' => false,
					'messages' => [$result['msg']]]);

			}
		}

	}

	public function actionUserMoveEvent($id = NULL)
	{
		$this->provideSemanticAccessControlData();

		$model = $this->findModel($id);
		$eventSource =  $this->currentEventId;
		$eventDestination=5;


		$result=$model->moveToEvent($eventSource,$eventDestination);


		if($result)
			$Message = Yii::t('app', 'User Successfully moved to Event');
		else
			$Message = Yii::t('app', 'Failed To Move user.');



		echo Json::encode(array(
			'enable' =>(bool) $result,
			'message' => $Message,
			'content' => '<i class="fa fa-check font-grean"   aria-hidden="true"></i>',
			'hint' => Yii::t('zii',($result? 'Active' : 'Deactive'))
		));

		Yii::$app->end();

	}


	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{

		$model = ExecutiveCommitteeUser::find()->where('id = :id And valid=1', [':id' => (int)$id])->one();
		if ($model !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


	public function createMessageCss($msgType, $string)
	{

		switch ($msgType) {
			case 'success':
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;

			case 'danger':
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
			default:
				$message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
		}

		return $message;

	}


	public function setFlashMsg($name, $message)
	{
		$session = Yii::$app->session;
		$session->setFlash($name, $message);
	}
}
