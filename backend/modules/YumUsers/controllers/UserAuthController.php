<?php

namespace backend\modules\YumUsers\controllers;


use backend\modules\YumUsers\models\LoginForm;
use backend\modules\YumUsers\models\PasswordResetRequestForm;
use backend\modules\YumUsers\models\ResetPasswordForm;
use backend\modules\YumUsers\models\SignupForm;
use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UserCredentialResetForm;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserAuthController extends Controller
{
	public $layout = '@app/views/layouts/column3';

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['login', 'signup','forget-password','reset-password'],
						'roles' => ['?'],
					],

					[
						'allow' => true,
						'actions' => ['logout', 'force-change-pass','signup'],
						'roles' => ['@'],
					],

					[
						'allow' => true,
						'actions' => ['index', 'detail', 'edit2', 'delete', 'signup'],
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				// 'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}


	public function actionForceChangePass()
	{
		$this->layout ='@app/views/layouts/column5';

		$model = new UserCredentialResetForm();
		$userModel = $model->getUserObj();

		/*check access just if last_login_time=='0000-00-00 00:00:00' do this action*/
		if ($userModel->last_login_time !== '0000-00-00 00:00:00' || isset($_POST['cancle-button'])) {
			Yii::$app->user->logout();
			return $this->redirect(['login']);
			exit();

		}


		if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			/*if username equal to nationalId then can edit */
			if (isset($model->username) && $userModel->username == $userModel->nationalId) {

				if ($model->changeUsername($model->username)) {
					$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
					<a href="#" class="alert-link">' . Yii::t('app', 'Changed Username Successfully .') . '</a>
					</div>';

				}

			}

			$model->changeContactInfo($model->mobile, $model->email);


			if ($model->changePassword()) {
				$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
					<a href="#" class="alert-link">' . Yii::t('app', 'Changed Password Successfully .') . '</a>
					</div>';

				$session = Yii::$app->session;
				$session->setFlash('change_password_success', $message);

				Yii::$app->user->logout();
				return $this->redirect(['login']);
			} else {
				$model->addError('inputNewPassword', Yii::t('app', 'اشکالی در تغییر کلمه عور بوجود آمده است'));
			}
		}

		return $this->render('force_change_pass', [
			'model' => $model,
			'userModel' => $userModel,
		]);
	}


	public function actionLogin()
	{
	  // $this->layout ='@app/views/layouts/column5';

		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		// Yii::$app->user->registerLogoutHook();
		if ($model->load(Yii::$app->request->post())) {
			if ($model->login()) {
				/*in first login redirect to orce change password*/
				if ($model->getUser()->last_login_time == '0000-00-00 00:00:00') {
					Yii::$app->session->set('newbie', true);
					return $this->redirect(['force-change-pass']);
				} else {
					$userObj = $model->getUser();
					$userObj->setLastLoginTime();
					$session = Yii::$app->session;
					$session->set('last_login_time', $userObj->last_login_time);
					return $this->goBack();
				}
			} else {
				return $this->render('login', [
					'model' => $model,]);
			}
		} else {

			return $this->render('login', [
				'model' => $model,
			]);
		}
	}



	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}


	public function actionSignup()
	{
//		 // $this->layout='main-login';
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				if (Yii::$app->getUser()->login($user)) {
					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}



	public function actionForgetPassword($email=null)
	{

		$message='';
		$this->layout ='@app/views/layouts/column3';


		/*Cancel Bottom*/
		if ( isset($_POST['cancle-button'])) {
			return $this->redirect(['login']);
		}

		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()))
		{
			if($model->validate())
			{
				if ($model->sendEmail())
				{
					Yii::$app->getSession()->setFlash('success', Yii::t('app',  'Check your email for further instructions.'));
					// return $this->redirect(['login']);
				}
				else
				{
					Yii::$app->getSession()->setFlash('error', Yii::t('app',  'Sorry, we are unable to reset password for email provided.'));
				}
			}
			else{
				Yii::$app->getSession()->setFlash('error', Yii::t('app',  'Sorry, we are unable to reset password for email provided.'));
			}
		}

		return $this->render('forget_password', [
			'model' => $model,
			'message' => $message,
		]);
	}


//	public function actionRequestPasswordReset()
//	{
//		$model = new PasswordResetRequestForm();
//		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//			if ($model->sendEmail()) {
//				Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
//
//				return $this->goHome();
//			} else {
//				Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
//			}
//		}
//
//		return $this->render('requestPasswordResetToken', [
//			'model' => $model,
//		]);
//	}
//
	
	
	

	public function actionResetPassword($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', 'New password was saved.');

			return $this->goHome();
		}

		return $this->render('resetPassword', [
			'model' => $model,
		]);
	}


	protected function findModel($id)
	{
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function createMessageCss($msgType, $string)
	{
		switch ($msgType) {
			case 'success' :
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;

			case 'danger' :
				$message = '<div class="alert alert-' . $msgType . '" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
			default :
				$message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
				break;
		}

		return $message;
	}

	public function setFlashMsg($name, $message)
	{
		$session = Yii::$app->session;
		$session->setFlash($name, $message);
	}

}
