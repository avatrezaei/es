<?php

namespace backend\modules\YumUsers;

use Yii;
//Yii::setPathOfAlias('YumModule', dirname(__FILE__));
//Yii::setPathOfAlias('YumComponents', dirname(__FILE__) . '/components/');
//Yii::setPathOfAlias('YumAssets', dirname(__FILE__) . '/assets/');
//
//Yii::import('YumModule.models.*');


class YumUsers extends \yii\base\Module {

	public $controllerNamespace = 'backend\modules\YumUsers\controllers';
	public $layout = '@app/views/layouts/column2';

	public $mainPage = '';
	public $publicMaxString=150;
	//layout related control vars
	public $baseLayout = 'application.views.layouts.main';
	public $loginLayout = 'application.modules.users.views.layouts.login';
	public $adminLayout = 'application.modules.users.views.layouts.yum';
	public $loginView = 'application.modules.users.views.yumUsers.login';
	public $adminEmail = 'cert@um.ac.ir';
	public $enableBootstrap = true;
	public $trulyDelete = false;
// Cost for Password generation. See
	// http://www.yiiframework.com/wiki/425/use-crypt-for-password-storage/
	// for details 
	public $passwordHashCost = 13;
	// Show an Captcha after how many unsuccessful logins? Set to 0 to 
	// always display an captcha, set to false to disable this function
	public $captchaAfterUnsuccessfulLogins = 5;
	public $BlockUserTime = 15; //min
	// After how much seconds without an action does a user gets indicated as
	// offline? Note that, of course, clicking the Logout button will indicate
	// him as offline instantly anyway.
	public $offlineIndicationTime = 300; // 5 Minutes
	// set to false to enable case insensitive users.
	// for example, demo and Demo would be the same user then
	public $caseSensitiveUsers = true;
	public $enableLogging = true;
	public $enableOnlineStatus = true;
	// Default cookie Duration is 3600*24*30 (30 days)
	public $cookieDuration = 2592000;
	public $password_expiration_time = 30; // days
	public $activationPasswordSet = false;
	public $autoLogin = false;
	// set to swift to active emailing by swiftMailer or 
	// PHPMailer to use PHPMailer as emailing lib.
	public $mailer = 'yum';
	public $phpmailer = null; // PHPMailer array options.
	// set avoidSql to true if you intent do use yii-user-management on a non
	// mysql database. All places where a SQL query would be used for performance
	// reason are overwritten with a ActiveRecord implementation. This should
	// make it more compatible with other rdbms. Experimental of course.
	public $avoidSql = false;
	// valid callback function that executes after user login
	public $afterLogin = false;
	// Set this to true to really remove users instead of setting the status
	// to -2 (YumUser::REMOVED)	
	// Handle with care. User and Profile will get removed physically from the db.
	// When the auditTrail extension
	// (http://www.yiiframework.com/extension/audittrail)
	// is installed, use it. It simply appends the LoggableBehavior to
	// all Yum-classes, so they get logged.
	public $enableAuditTrail = false;
	public static $dateFormat = "m-d-Y";  //"d.m.Y H:i:s"
	public $dateTimeFormat = 'm-d-Y G:i:s';  //"d.m.Y H:i:s"
	private $_urls = array(
		'login' => array('//user/user'),
		'return' => array('//user/user/index'),
		'firstVisit' => array('//user/privacy/update'),
		'delete' => array('//user/user/delete'),
		// Page to go after admin logs in
		'returnAdmin' => false,
		// Page to go to after logout
		'returnLogout' => array('//user/user/login'));
	private $_views = array(
		'login' => '/user/login',
		'menu' => '/user/menu',
		'registration' => '/registration/registration',
		'activate' => '/user/resend_activation',
		'message' => '/user/message',
		'passwordForm' => '/user/_activation_passwordform',
		'messageCompose' => 'application.modules.message.views.message.compose');
	public $pageSize = 10;
	public $userTable = '{{users}}';
	public $translationTable = '{{translation}}';
	public $usernameRequirements = array(
		'minLen' => 3,
		'maxLen' => 30,
		'match' => '/^[A-Za-z0-9_-]+$/u',
		'dontMatchMessage' => 'نام کاربری باید تنها  شامل کاراکترها ی حرفی و عددی انگلیسی [_-] باشد  ',
	);
	  public $nameRequirements = array(
	   'minLen' => 3,
	   'maxLen' => 30,
	   'match' => '/^([\x{0600}-\x{06FF}]|[_ -])*$/u',//en & persian character   
	   'dontMatchMessage' =>' فقط از کاراکترهای حرفی وعددی فارسی استفاده شود.',
	);
	public $passwordRequirements = array(
		'minLen' => 7,
		'maxLen' => 128,
		'minLowerCase' => 1,
		'minUpperCase' => 0,
		'minDigits' => 1,
		'maxRepetition' => 0,
	);
	public $requiredFieldsRegister = array(
		array(
			'required' => [ 'username', 'name', 'last_name', 'email',  'mobile',  'gender'],
			'on' => ['register'],
		),
		array(
			 'required' =>[ 'username','name','last_name','email','gender','tel','mobile','birthDate','passwordInput'],
			'on' => ['createUser'],
		),
		array(
			'required' => [ 'username', 'name', 'last_name', 'email',  'mobile', 'gender'],
			'on' => [ 'update'],
		),
	);
	//this role must least privilaged role for defualt assign roles
	public $defaultRole = 'reader';

	protected function checkModuleProperties() {
		// set use fields based on required fields
		if ($this->requireEmail) {
			$this->useEmail = true;
		}
		if ($this->requireUsername) {
			$this->useUsername = true;
		}

		// get class name for error messages
		$className = get_called_class();

		// check required fields
		if (!$this->requireEmail && !$this->requireUsername) {
			throw new InvalidConfigException("{$className}: \$requireEmail and/or \$requireUsername must be true");
		}
		// check login fields
		if (!$this->loginEmail && !$this->loginUsername) {
			throw new InvalidConfigException("{$className}: \$loginEmail and/or \$loginUsername must be true");
		}
		// check email fields with emailConfirmation/emailChangeConfirmation is true
		if (!$this->useEmail && ($this->emailConfirmation || $this->emailChangeConfirmation)) {
			$msg = "{$className}: \$useEmail must be true if \$email(Change)Confirmation is true";
			throw new InvalidConfigException($msg);
		}

		// ensure that the "user" component is set properly
		// this typically causes problems in the yii2-advanced app
		// when people set it in "common/config" instead of "backend/config" and/or "backend/config"
		//   -> this results in users failing to login without any feedback/error message
		if (!Yii::$app->request->isConsoleRequest && !Yii::$app->get("user") instanceof \amnah\yii2\user\components\User) {
			throw new InvalidConfigException('Yii::$app->user is not set properly. It needs to extend \amnah\yii2\user\components\User');
		}
	}

	public function init() {



		// check for valid email/username properties
		// $this->checkModuleProperties();
		// set up i8n
		if (empty(Yii::$app->i18n->translations['yum'])) {
			Yii::$app->i18n->translations['yum'] = [
				//  'language'=>'fa-IR',
				'sourceLanguage' => 'fa',
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@backend/messages',
			];
		}
		if (empty(Yii::$app->i18n->translations['attribute'])) {
			Yii::$app->i18n->translations['attribute'] = [
				//  'language'=>'fa-IR',
				'sourceLanguage' => 'fa',
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@backend/messages',
			];
		}
		if (empty(Yii::$app->i18n->translations['menu'])) {
			Yii::$app->i18n->translations['menu'] = [
				//  'language'=>'fa-IR',
				'sourceLanguage' => 'fa',
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@backend/messages',
			];
		}
		if (empty(Yii::$app->i18n->translations['app'])) {
			Yii::$app->i18n->translations['app'] = [
				//  'language'=>'fa-IR',
				'sourceLanguage' => 'fa',
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@backend/messages',
			];
		}
	}

//	public function beforeControllerAction($controller, $action) {
//		if (parent::beforeControllerAction($controller, $action)) {
//			// this method is called before any module controller action is performed
//			// you may place customized code here
//			return true;
//		} else
//			return false;
//	}

}

/*
 * view not created 
 * solution :absolute path:
 * 
 * C:/wamp/www/advanced/backend/modules/YumUsers/views/user
 * 
 * http://stackoverflow.com/questions/27896281/yii-2-gii-is-generating-view-files-on-different-directory
*
 * 
 * 
 * chalenges:
 * 
 * add new property to attributes in load function:
 * I am added passwordInput in user model class
 * https://github.com/yiisoft/yii2/issues/1967
 * 
 * 
 * ============================
 * CCaptcha ------>captcha
 * 
 * ===================================
 * translator
 * ================================
 * translator1-----------------
 * ================================
 * jahat moarefi har file jadid ke dar masire
 * mesages/fa/
 * hast bayad yek index dar translate sakhte:
 * 
 * 
 *		 if (empty(Yii::$app->i18n->translations['yum'])) {
			Yii::$app->i18n->translations['yum'] = [
	  'sourceLanguage'=>'fa',
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => __DIR__ . '/messages',

			];
		}
 * 
 * ================================
 * translator2-----------------
 * ================================
 * 
 * parameter ferestadan besorat zie taghir karde ast
 old  :	  Yii::t('app', 'Hello, {username}!', ['{username}' => $username,]);
 new  :	 \Yii::t('app', 'Hello, {username}!', [	'username' => $username,]);
 1-dige {} faghat dar matn miayad dar meghdardehi nemidahim: {username}======>username
 * 
 * 
 * 
 * 
 * 
 *  /
 */