<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];


$this->title = Yii::t('app', 'Change Password');

$fieldOptions1 = [
	'options' => ['class' => 'input-group form-group has-feedback'],
	'inputTemplate' => "{input}<span class='input-icon right'><i class='fa fa-user'></i></span>"
];

$fieldOptions2 = [
	'options' => ['class' => 'input-group form-group has-feedback'],
	'inputTemplate' => "{input}<span class='input-icon right'><i class='fa fa-lock'></i></span>"
];
?>

<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['class' => 'forget-form', 'novalidate'=>'novalidate']]); ?>
	<h3 class="font-green"><?= Yii::t('app', 'Forget Password ?') ?></h3>
	<?php echo Alert::widget(); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<p> <?= Yii::t('app', 'Enter your e-mail address below to reset your password.') ?> </p>
	<div class="form-group">
		<?php
			echo Html::activeInput(
				'text',
				$model,
				'email',
				[
					'placeholder' => Yii::t('app', 'Email'),
					'class' => 'form-control placeholder-no-fix',
					'autocomplete' => 'off'
				]
			);
		?>
	</div>
	<div class="form-actions">
		<?= Html::a(Yii::t('zii', 'Back'), ['/login'], ['class' => 'btn btn-default pull-right', 'name' => 'cancle-button']) ?>
		<?= Html::submitButton(Yii::t('app', 'Confirm'), ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
	</div>
<?php ActiveForm::end(); ?>