<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */


$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];


$this->title = Yii::t('app', 'Change Password');

$fieldOptions1 = [
	'options' => ['class' => 'input-group form-group has-feedback'],
	'inputTemplate' => "{input}<span class='input-icon right'><i class='fa fa-user'></i></span>"
];

$fieldOptions2 = [
	'options' => ['class' => 'input-group form-group has-feedback'],
	'inputTemplate' => "{input}<span class='input-icon right'><i class='fa fa-lock'></i></span>"
];
?>


	<div class="pull-left forget-password-block">
		<span class="font-red"><?php echo Yii::t('app', 'name') . ':'; ?></span>
		<span class="font-blue"><?php echo Html::encode($userModel->fullName); ?></span>
	</div>


	<div class="pull-right forget-password-block">
		<span class="font-red"><?php echo Yii::t('app', 'National Id') . ':'; ?></span>
		<span class="font-blue"><?php echo Html::encode($userModel->nationalId); ?></span>
	</div>

	<br>
	<br>
	<div class="complete-info">
		<h5 class="font-green"><?php echo Yii::t('app', 'Please complete the following information.'); ?></h5>
	</div>


<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['class' => 'form-horizontal']]); ?>

<?php //$form = ActiveForm::begin(['class' => 'login-form', 'id' => 'force-change-pass-form', 'enableClientValidation' => false]); ?>


<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>


<?php

if ($userModel->username == $userModel->nationalId) {

	echo $form->field(
		$model,
		'username',
		[
			'options' => ['class' => $formGroupClass],
			'template' => $template
		]
	)
		->textInput(
			['value' => Html::encode($userModel->username), 'maxlength' => 255, 'class' => $inputClass]
		)
		->label(Yii::t('app', 'Username'), ['class' => $labelClass]);
}


?>

<?php
echo $form->field(
	$model,
	'mobile',
	[
		'options' => ['class' => $formGroupClass],
		'template' => $template
	]
)
	->textInput(
		['value' => Html::encode($userModel->mobile), 'maxlength' => 255, 'class' => $inputClass]
	)
	->label(Yii::t('app', 'Mobile'), ['class' => $labelClass])
	->hint(Yii::t('app', 'Mobile numbers to use the mobile version and some information is essential.'));

?> 

<?php
echo $form->field(
	$model,
	'email',
	[
		'options' => ['class' => $formGroupClass],
		'template' => $template
	]
)
	->textInput(
		['value' => Html::encode($userModel->email), 'maxlength' => 255, 'class' => $inputClass]
	)
	->label(Yii::t('app', 'Email'), ['class' => $labelClass])
	->hint(Yii::t('app', 'E-mail to retrieve passwords and some information is essential.'));

?>

<?php
echo $form->field(
	$model,
	'inputOldPassword',
	[
		'options' => ['class' => $formGroupClass],
		'template' => $template
	]
)
	->passwordInput(
		['maxlength' => 255, 'class' => $inputClass]
	)
	->label(NULL, ['class' => $labelClass]);

?>


<?php
echo $form->field(
	$model,
	'inputNewPassword',
	[
		'options' => ['class' => $formGroupClass],
		'template' => $template
	]
)
	->passwordInput(
		['maxlength' => 255, 'class' => $inputClass]
	)
	->label(NULL, ['class' => $labelClass]);

?>


<?php
echo $form->field(
	$model,
	'inputNewPassword_repeat',
	[
		'options' => ['class' => $formGroupClass],
		'template' => $template
	]
)
	->passwordInput(
		['maxlength' => 255, 'class' => $inputClass]
	)
	->label(NULL, ['class' => $labelClass]);

?>


	<div class="form-actions">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn green uppercase pull-right', 'name' => 'login-button']) ?>
		<?= Html::submitButton(Yii::t('app', 'Cancel'), ['class' => 'btn default uppercase pull-right', 'name' => 'cancle-button']) ?>

	</div>


<?php ActiveForm::end(); ?>