<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

?>

	<h3 class="font-green">
		<?php echo Yii::t('app', 'Sing up') ?>
	</h3>

        <!--[if IE]>
            <div class="alert font-red bold center">
                <?= Yii::t('app','To use the system easy to use Firefox or Chrome browser.') ?>
            </div>
        <![endif]-->

	<p class="hint"> <?= Yii::t('app', 'Enter the following information carefully'); ?> : </p>

	<?php yii\widgets\Pjax::begin(['id' => 'login_pjax']) ?>

		<?php $form = ActiveForm::begin(['id' => 'signup-form', 'enableClientValidation' => false]); ?>

		
			<?=
				$form
				->field($model, 'national_code')
				->textInput(['placeholder' => $model->getAttributeLabel('national_code'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>			
			<?=
				$form
				->field($model, 'name')
				->textInput(['placeholder' => $model->getAttributeLabel('name'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'last_name')
				->textInput(['placeholder' => $model->getAttributeLabel('last_name'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'university_name')
				->textInput(['placeholder' => $model->getAttributeLabel('university_name'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>
			<?=
				$form
				->field($model, 'student_number')
				->textInput(['placeholder' => $model->getAttributeLabel('student_number'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>

			<?=
				$form
				->field($model, 'password')
				->textInput(['placeholder' => $model->getAttributeLabel('password'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>

			<div class="form-actions">
			<?= Html::a(Yii::t('zii', 'Back'), ['/login'], ['class' => 'btn btn-default pull-right', 'name' => 'cancle-button']) ?>
				<?= Html::submitButton(Yii::t('app', 'Confirm'), ['class' => 'btn btn-success uppercase pull-right', 'name' => 'login-button']) ?>
			</div>

		<div class="row">
			<?php
			$session = Yii::$app->session;
			if ($session->has('show_login_captcha') && $session->get('show_login_captcha') == TRUE) {
				echo $form->field($model, 'captcha')->widget(Captcha::className(), array(
					'options' => array('class' => 'input-medium'),
					'captchaAction' => '/YumUsers/userauth/captcha',
					'template' => "{input} \n{image} ",
					'imageOptions' => "",
				));
			}
			?>
		</div>

	<?php ActiveForm::end(); ?>
	<?php yii\widgets\Pjax::end() ?>