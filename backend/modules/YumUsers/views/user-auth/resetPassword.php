<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$template = '{label} {input} {hint}';
$this->title = Yii::t('app', 'Change Password');

?>

<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['class' => 'register-form']]); ?>
<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
	<h3 class="font-green"><?= $this->title ?></h3>
	<p class="hint"> <?php echo Yii::t('app', 'Please choose your new password:'); ?> </p>

	<?php
		echo $form->field(
			$model,
			'password',
			[
				'options' => ['class' => 'form-group'],
				'template' => $template
			]
		)
		->passwordInput(
			['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control placeholder-no-fix']
		)
		->label(Yii::t('app', 'Input New Password'), ['class' => 'control-label visible-ie8 visible-ie9']);
	?>

	<?php
		echo $form->field(
			$model,
			'password_repeat',
			[
				'options' => ['class' => 'form-group'],
				'template' => $template
			]
		)
		->passwordInput(
			['placeholder' => $model->getAttributeLabel('password_repeat'), 'class' => 'form-control placeholder-no-fix']
		)
		->label(NULL, ['class' => 'control-label visible-ie8 visible-ie9']);
	?>

	<div class="form-actions">
		<?= Html::a(Yii::t('zii', 'Back'), ['/login'], ['class' => 'btn btn-default pull-right', 'name' => 'cancle-button']) ?>
		<?= Html::submitButton(Yii::t('app', 'Confirm'), ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
	</div>

<?php ActiveForm::end(); ?>