<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Change Password');

?>

	<?php $form = ActiveForm::begin(['class' => 'login-form', 'id' => 'force-change-pass-form', 'enableClientValidation' => false]); ?>
	<h3 class="form-title font-green"><?php echo Yii::t('app', 'Please complete the following information'); ?></h3>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

		<?=
			$form->field(
				$model,
				'mobile'
			)
			->textInput(
				[
					'value' => Html::encode($userModel->mobile),
					'placeholder' => Yii::t('app','Mobile'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				]
			)
			->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			->hint(Yii::t('app', 'Mobile numbers to use the mobile version and some information is essential.'))
		?>

		<?=
			$form->field(
				$model,
				'email'
			)
			->textInput(
				[
					'value' => Html::encode($userModel->email),
					'placeholder' => Yii::t('app','Email'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				]
			)
			->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			->hint(Yii::t('app', 'E-mail to retrieve passwords and some information is essential.'))
		?>

		<p class="hint"> <?= Yii::t('app', 'Enter your account details below') ?>: </p>

		<?php if($userModel->username == $userModel->nationalId): ?>
		<?=
			$form->field(
				$model,
				'username'
			)
			->textInput(
				[
					'value' => Html::encode($userModel->username),
					'placeholder' => Yii::t('app','Username'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				]
			)
			->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
		?>
		<?php endif; ?>

		<?=
			$form->field(
				$model,
				'inputOldPassword'
			)
			->passwordInput(
				[
					'placeholder' => $model->getAttributeLabel('inputOldPassword'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				]
			)
			->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
		?>

		<?=
			$form->field(
				$model,
				'inputNewPassword'
			)
			->passwordInput(
				[
					'placeholder' => $model->getAttributeLabel('inputNewPassword'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				]
			)
			->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
		?>

		<?=
			$form->field(
				$model,
				'inputNewPassword_repeat'
			)
			->passwordInput(
				[
					'placeholder' => $model->getAttributeLabel('inputNewPassword_repeat'),
					'class'=>'form-control form-control-solid placeholder-no-fix'
				]
			)
			->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
		?>

		<div class="row">
			<?php
				$session = Yii::$app->session;
				if ($session->has('show_login_captcha') && $session->get('show_login_captcha') == TRUE) {
					echo $form->field($model, 'captcha')->widget(Captcha::className(), array(
						'options' => array('class' => 'input-medium'),
						'captchaAction' => '/YumUsers/userauth/captcha',
						'template' => "{input} \n{image} ",
						'imageOptions' => "",
					));
				}
			?>
		</div>

		<div class="form-actions">
			<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
			<?= Html::submitButton(Yii::t('app', 'Cancel'), ['class' => 'btn default pull-right', 'name' => 'cancle-button']) ?>
		</div>
	<?php ActiveForm::end(); ?>