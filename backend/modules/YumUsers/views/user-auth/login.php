<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use backend\assets\iCheckAsset;

iCheckAsset::register($this);

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Sign in to start your session');

$fieldOptions2 = [
	'options' => ['class' => 'input-group form-group has-feedback'],
	'inputTemplate' => "{input}<span class='input-icon right'><i class='fa fa-lock'></i></span>"
];

/* ------------show flash messages----------------- */
$session = Yii::$app->session;
// check the availability
$result = $session->hasFlash('change_password_success');
// get and display the message
echo $session->getFlash('change_password_success');
?>

		<h3 class="form-title font-green"><?php echo Yii::t('app', 'Sign in to start your session') ?></h3>
				
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span> Enter any username and password. </span>
		</div>

		<?php yii\widgets\Pjax::begin(['id' => 'login_pjax']) ?>

			<?php $form = ActiveForm::begin(['class' => 'login-form', 'id' => 'login-form', 'enableClientValidation' => false]); ?>

			<?=
				$form
				->field($model, 'username')
				->textInput(['placeholder' => $model->getAttributeLabel('username'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>

			<?=
				$form
				->field($model, 'password')
				->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class'=>'form-control form-control-solid placeholder-no-fix'])
				->label(NULL, ['class'=>'control-label visible-ie8 visible-ie9'])
			?>

			<div class="form-group rem-field">
				<?php
					echo Html::activeCheckbox(
						$model,
						'rememberMe',
						[
							'class' => 'icheck',
							'data-checkbox' => 'icheckbox_flat-blue',
							'labelOptions' => [
								'class' => 'rememberme check'
							]
						]
					);
				?>
			</div>
			<div class="form-actions pull-right">
				<?= Html::submitButton(Yii::t('app', 'Sing in'), ['class' => 'btn green uppercase ', 'name' => 'login-button']) ?>

				 

				<?= Html::a(Yii::t('app','Sign up'), [
					'/YumUsers/user-auth/signup'], 
					['class' => 'btn green uppercase ',
				]) ?>


				 


			</div>

			<div class="row">
				<?php
				$session = Yii::$app->session;
				if ($session->has('show_login_captcha') && $session->get('show_login_captcha') == TRUE) {
					echo $form->field($model, 'captcha')->widget(Captcha::className(), array(
						'options' => array('class' => 'input-medium'),
						'captchaAction' => '/YumUsers/userauth/captcha',
						'template' => "{input} \n{image} ",
						'imageOptions' => "",
					));
				}
				?>
			</div>

		<?php ActiveForm::end(); ?>
		<?php yii\widgets\Pjax::end() ?>

		<div class="create-account">
			<p>
				<a href="<?= \yii\helpers\Url::to(['/YumUsers/user-auth/forget-password']) ?>"><?php echo Yii::t('app', 'I forgot my password'); ?></a><br>
			</p>
		</div>

		<div class="modal fade" id="guidance" tabindex="-1" role="basic" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="fa fa-exclamation-circle"></i><?= Yii::t('app', 'guidance') ?></h4>
					</div>
					<div class="modal-body">
						<?php
						$link='<i class="fa-lg fa fa fa-download font-blue"></i> '.Html::a(
							Yii::t('app','Video Tutorial on how to work with the system'),
							\yii\helpers\Url::to('http://stream.um.ac.ir/olympiad13.flv'),
							[
								'class'=>"font-blue",
								]
						);
						echo  Yii::t('app', 'loginGuisdance').'<br>'.$link;
						?></div>
					<div class="modal-footer">
						<button type="button" class="btn dark " data-dismiss="modal"><?= Yii::t('app','Close');  ?></button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>