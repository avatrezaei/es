<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Senior Event User Management');
$this->params['page_title'] = false;
$this->params['breadcrumbs'][] = $this->title;
/*$this->params['actions'] = [
	[
		'label' => '<i class="ace-icon fa fa-plus bigger-120 orange"></i> ' . Yii::t('app', 'Create Event'),
		'url' => ['create']
	]
];*/

?>
 <?php
	/* ------------show flash messages----------------- */
	$session = Yii::$app->session;
   // check the availability
	$result = $session->hasFlash('user_resp_msg');
   // get and display the message
	echo $session->getFlash('user_resp_msg');
	?>
	
<div class="user-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-user font-dark"></i>
				<span class="caption-subject bold uppercase"><?= $this->title ?></span>
			</div>
		</div>
		<div class="portlet-body">
			<?= $widget->run() ?>
		</div>
	</div>
</div>