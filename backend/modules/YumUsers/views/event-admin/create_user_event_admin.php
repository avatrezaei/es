<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app',  'Create Senior Event User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Senior Event User Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if(is_object($eventobj))
{
	$eventname=$eventobj->name;
}else
{
	$eventname='---';
}

?>

	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet ">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
			 <?php
			 if(is_object($model))
			 {
			   echo $model->getIconThumbnail(null,"img-responsive");
			 }
			 ?>			 
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle" id="user_quick_info">
				<div class="profile-usertitle-name"> <?php echo Yii::t('app','Event Admin').Html::encode($eventname)?> </div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!-- SIDEBAR BUTTONS -->
			<div class="profile-userbuttons">
				
			</div>
			<!-- END SIDEBAR BUTTONS -->
		
		</div>
		<!-- END PORTLET MAIN -->
		
	</div>

	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				
				<?= $message ?>
				<div id="results_expand_msg" style="display: none " ></div>										
				
				
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'Admin Event User Info')?></span>
						</div>

					</div>
					<div class="portlet-body">
						<div class="tab-content">
								<?php
									echo $this->render('_create_event_admin_personal_info', [
										'model' => $model,
										'message' => $message,
										'eventobj'=>$eventobj,
									]);
								?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>