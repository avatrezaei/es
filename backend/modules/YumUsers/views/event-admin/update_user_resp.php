<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app','Update  Admin Event User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Senior Event User Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$space='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		;

if(is_object($eventObj))
{
	$universityName=$eventObj->name;
}else
{
	$universityName='---';
}

?>

					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<!-- PORTLET MAIN -->
								<div class="portlet light profile-sidebar-portlet ">
									<!-- SIDEBAR USERPIC -->
									<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
									   echo $model->getIconThumbnail(null,"img-responsive");
									 }
									 ?>			 
									</div>
									<!-- END SIDEBAR USERPIC -->
									<!-- SIDEBAR USER TITLE -->
									<div class="profile-usertitle" id="user_quick_info">
										<div class="profile-usertitle-name"> <?php echo Yii::t('app','Senior Event User').Html::encode($universityName)?> </div>
										<div class="profile-usertitle-name"> <?php echo Html::encode($model->FullName)?></div>
										<div class="profile-usertitle-job"> <?php echo Html::encode($model->nationalId)?> </div>
									</div>
									<!-- END SIDEBAR USER TITLE -->
									<!-- SIDEBAR BUTTONS -->
									<div class="profile-userbuttons">
										<?php // echo Html::a($space.Yii::t('app','Edit').$space, \yii\helpers\Url::to(['carvan-register/update-athelete','userId'=>$model->id],true),['class'=>"btn green btn-sm"]);?>
										<?php //echo Html::a($space.Yii::t('app','Delete').$space, \yii\helpers\Url::to(['carvan-register/main-register','userId'=>$model->id],true),['class'=>"btn red btn-sm"]);?>
									</div>
									<!-- END SIDEBAR BUTTONS -->
									<!-- SIDEBAR MENU -->
									<div class="profile-usermenu">
										<ul class="nav">

										</ul>
									</div>
									<!-- END MENU -->
								</div>
								<!-- END PORTLET MAIN -->
								
							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">
								<div class="row">
									<div class="col-md-12">
										
										 <?= $message ?>
							<div id="results_expand_msg" style="display: none " >
							</div>										
										
										
										<div class="portlet light ">
											<div class="portlet-title tabbable-line">
												<div class="caption caption-md">
													<i class="icon-globe theme-font hide"></i>
													
													<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'Admin Event User Info')?></span>
												</div>

											</div>
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
														<!-- Render Personal info  form -->	
														<?php

														echo $this->render('_update_athlete_personal_info', [
															'model' => $model,
															'message' => $message,
															'eventObj'=>$eventObj,


														])

														?>  
																										   <!-- END PERSONAL INFO FORM -->
												   
													
												  
												 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>