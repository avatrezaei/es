<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use app\models\User;
use yii\widgets\ActiveForm;


/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;
use kartik\file\FileInput;
use kartik\switchinput\SwitchInput;/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Create  University User Responsed');
$this->params['breadcrumbs'][] = $this->title;
?>
 <div id="results" style="display: none" >
	
</div>

<div class="portlet box blue ">
	<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i> <?php echo Html::encode($this->title); ?>
			</div>
	</div>
	
	
   <div class="portlet-body">
	<?= $message ?>
								  
   </div>
 </div>