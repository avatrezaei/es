<?php
use kartik\detail\DetailView;
use kartik\detail\DetailViewTiny;
use kartik\detail\tiny;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\AppLanguage;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;

$userDetailObj=$model;
/* * ********************************************* */
/*
 * $model->subfields returnarrays of object
 */


			echo'<div id =wrapped_detail_view_items_' . $model->id . '_' . $userDetailObj->id . '> ';
			echo DetailViewTiny::widget([
				'model' => $userDetailObj,
				'id' => 'detail_view_items_' . $model->id . '_' . $userDetailObj->id,
				'condensed' => true,
				//'hover' => true,
				'saveBottomType' => 'Bottom', /* this attribute Added by alizadeh to create buttom insted submit buttom  */
				'panel' => [
					'heading' => Yii::t('app', 'Project') . ' # ' . $userDetailObj->id,
					'type' => DetailView::TYPE_INFO,
				],
				'deleteexpandOptions' => [
					'label' => Html::tag('span', '<span id="edit" class="glyphicon glyphicon-trash"></span>'),
				],
				'buttons1' => '{deleteexpand} {update}',
				'buttons2' => '{view} {reset} {save}',
				'formOptions' => ['id' => 'itemform_' . $userDetailObj->id, 'options' => array("accept-charset" => "UTF-8"), 'action' => ['/YumUsers/user-details/edit2', 'id' => $userDetailObj->id], 'enableClientValidation' => TRUE],
				'hover' => true,
				'mode' => DetailView::MODE_VIEW,
				'panel' => [
					'heading' => Yii::t('app', 'UserDetails') . '  رخداد' .' '.Html::encode($userDetailObj->getEventNameText()),
					'type' => DetailView::TYPE_INFO,
					'headingOptions' => [
					// 'template'=>'{title}',
					]
				],
				'attributes' => [
			 

					[
						'attribute' => 'eventId',
						'value' => Html::encode($userDetailObj->getEventNameText()),
						'type' => DetailView::INPUT_SELECT2,
						'widgetOptions' => [
							'model' => $userDetailObj,
							'attribute' => 'eventId',
							'data' => ArrayHelper::map(Event::find()->all(), 'id', 'name') ,
							'options' => ['placeholder' => Yii::t('app', 'Select a ...'), 'id' => 'event_detail_form_' . $userDetailObj->id],
							'pluginOptions' => ['allowClear' => true],
						],
						'inputWidth' => '40%'
					],					

					[
						'attribute' => 'educationalField',
						'value' => Html::encode($userDetailObj->getEducationalGroupsNameText()),
						'type' => DetailView::INPUT_SELECT2,
						'widgetOptions' => [
							'model' => $userDetailObj,
							'attribute' => 'educationalField',
							'data' => ArrayHelper::map(EducationalGroups::find()->all(), 'EduGrpCode', 'PEduName') ,
							'options' => ['placeholder' => Yii::t('app', 'Select a ...'), 'id' => 'edugroup_detail_form_' . $userDetailObj->id],
							'pluginOptions' => ['allowClear' => true],
						],
						'inputWidth' => '40%'
					],					

					[
						'attribute' => 'educationalSection',
						'value' => Html::encode($userDetailObj->getEducationalSectionNameText()),
						'type' => DetailView::INPUT_SELECT2,
						'widgetOptions' => [
							'model' => $userDetailObj,
							'attribute' => 'educationalSection',
							'data' => ArrayHelper::map(EducationalSections::find()->all(), 'EduSecCode', 'PEduSecName') ,
							'options' => ['placeholder' => Yii::t('app', 'Select a ...'), 'id' => 'edusec_detail_form_' . $userDetailObj->id],
							'pluginOptions' => ['allowClear' => true],
						],
						'inputWidth' => '40%'
					],					


					[
						'attribute' => 'stuNo',
						'value' =>  Html::encode($userDetailObj->stuNo),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
						'attribute' => 'weight',
						'value' =>  Html::encode($userDetailObj->weight),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
						'attribute' => 'address',
						'value' =>  Html::encode($userDetailObj->address),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
						'attribute' => 'beltColor',
						'value' =>  Html::encode($userDetailObj->beltColor),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
						'attribute' => 'bankAccountNo',
						'value' =>  Html::encode($userDetailObj->bankAccountNo),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
					'attribute' => 'fileAttrStuCart',
					'value' => $userDetailObj->getStuCartIconThumbnail(),
					'type' => DetailView::INPUT_FILE,
					'inputWidth' => '40%',
							'format'=>'raw',
					],
					[
					'attribute' => 'fileAttrAssuranceCart',
					'value' => $userDetailObj->getAssuranceCartIconThumbnail(),
					'type' => DetailView::INPUT_FILE,
					'inputWidth' => '40%',
							'format'=>'raw',
					],					
				   ],
			]);
			echo '</div>';

?>

