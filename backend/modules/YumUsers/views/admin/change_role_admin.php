<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use app\models\User;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Change Role');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];

?>
<div class="fields-create">
	<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-green">
					<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
				</div>
			</div>
			<div class="portlet-body form">

				<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal', 'id'=>'form_change_pass']]); ?>
				<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>
				<div class="form-body">

					<?php 
						//set selected roles--if we set value of role this is display as checked in checkbox
						$model->role = $model->getUserRoles((int)$userTargetId);
					?>
					<?php 
						echo $form->field(
								$model, 
								'role',
								[
									'options' => ['class' => $formGroupClass],
									'template' => $template
								]
							)
							->widget(Select2::classname(), [
								'data' => $model->getRolesAsListData(),
								'theme' => Select2::THEME_BOOTSTRAP,
								'options' => [
									'placeholder' => Yii::t('app', 'Please select Field Type'),
									'dir' => 'rtl',
								],
							])
							->label(NULL, ['class'=>$labelClass]); 
					?>
					<?php
						// echo $form->field(
						// 		$model,
						// 		'role',
						// 		[
						// 			'options' => ['class' => $formGroupClass],
						// 			'template' => $template
						// 		]
						// 	)
						// 	->checkboxList(
						// 		$model->getRolesAsListData()
						// 		/*'labelOptions' => [
						// 			'class' => $inputClass,
						// 			'style' => 'display: inline-block'
						// 		]*/
						// 	)
						// 	->label(NULL, ['class'=>$labelClass]);
					?>

					<?php
						$model->targetUser =  (int)$userTargetId;// better put it on controller
						$form->field($model, 'targetUser')->hiddenInput()->label(false);
					?>
					 
				</div>

				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<?= Html::submitButton(Yii::t('app', 'Confirm'), ['class' => 'btn green']) ?>
						</div>
					</div>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>