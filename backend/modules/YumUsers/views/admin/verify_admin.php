<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];

if(!isset($userTargetId))
	$userTargetId=$model->targetUser;

?>

<div class="change-password-admin">
	<div class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-green">
					<span class="caption-subject bold uppercase"> <?= Html::encode($this->title) ?> </span>
				</div>
			</div>
			<div class="portlet-body form">

				<?php $form = ActiveForm::begin(['options' => ['id'=>'form_change_pass', 'class' => 'form-horizontal']]); ?>
				<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>
				<div class="form-body">

					<?php
						echo $form->field(
								$model,
								'passwordInput',
								[
									'options' => ['class' => $formGroupClass],
									'template' => $template
								]
							)
							->passwordInput(
								['maxlength' => 255, 'class' => $inputClass]
							)
							->label(NULL, ['class'=>$labelClass]);
					?>

					<?php
						echo $form->field(
								$model,
								'inputNewPassword',
								[
									'options' => ['class' => $formGroupClass],
									'template' => $template
								]
							)
							->passwordInput(
								['maxlength' => 255, 'class' => $inputClass]
							)
							->label(NULL, ['class'=>$labelClass]);
					?>

					<?php
						echo $form->field(
								$model,
								'inputNewPassword_repeat',
								[
									'options' => ['class' => $formGroupClass],
									'template' => $template
								]
							)
							->passwordInput(
								['maxlength' => 255, 'class' => $inputClass]
							)
							->label(NULL, ['class'=>$labelClass]);
					?>

					<?php
						$model->targetUser =  (int)$userTargetId;// better put it on controller
						$form->field($model, 'targetUser')->hiddenInput()->label(false);
					?>
				</div>

				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<?= Html::submitButton(Yii::t('app', 'Confirm'), ['class' => 'btn green', 'id'=>'btn_change_user_pass']) ?>
						</div>
					</div>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
