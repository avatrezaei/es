<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['/YumUsers/user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

	<h1><?= Html::encode($this->title) ?></h1>

  
	<?= $this->render('_form_changepass_admin', [
		'model' => $model,
						'message'=>$message,

		
	]) ?>

<br>
<hr>
<br>