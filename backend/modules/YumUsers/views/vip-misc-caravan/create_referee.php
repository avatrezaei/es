<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app', 'Add VIP User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'VIP User Management'), 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;

$space='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
?>

<?php
$ajaxURL = \yii\helpers\Url::to(['/YumUsers/user/check-national-id-exist']);
$this->registerJs(
	<<<SCRIPT
var nationalidVal = $("#vipmisccaravanuser-nationalid").val();
	if(nationalidVal==''){
		$("input").attr("disabled","true");
		$("#create_executiveuser").find(':input(:disabled)').prop('disabled',true)
		$("#vipmisccaravanuser-nationalid").removeAttr('disabled');
		$('[name="_csrf"]').removeAttr('disabled');

	}


	$('body').on('keypress','#vipmisccaravanuser-nationalid',
		function(event){
			if (event.which == 13 || event.keyCode == 13) {
				nationalIdCheck();
				$("#vipmisccaravanuser-name").focus();

				return false;


			}
		});

	$('body').on('blur','#vipmisccaravanuser-nationalid',
		function(event){
			nationalIdCheck();
			$("#vipmisccaravanuser-name").focus();

			return false;

		});

	function nationalIdCheck()
	{


		$("input").removeAttr('disabled');
		$("#vipmisccaravanuser-nationalid").removeAttr('disabled');

		var nationalidValue = $("#vipmisccaravanuser-nationalid").val();

		jQuery.ajax({
			type:'GET',
			url:'$ajaxURL',
			data: ({nationalId: nationalidValue}),
			cache:false,
			success:function(response){
				var parsed = JSON.parse(response);



				if(parsed.exist==true)
				{
					$("input").removeAttr('disabled');
					$("#create_executiveuser").find(':input(:disabled)').prop('disabled',false);
					$('option').attr('selected', false);
					
					/*remove banned ckass from file input interfaces*/
					$(".btn-file").on("myRemoveAttrEvent", function(event){
                     $(this).removeAttr("disabled");
                     });
                    $(".btn-file").trigger("myRemoveAttrEvent");
                    $(".kv-fileinput-caption").removeClass("file-caption-disabled");
	                /*remove banned ckass from file input interfaces*/

					$("#vipmisccaravanuser-name").val(parsed.person.name);
					$("#vipmisccaravanuser-last_name").val(parsed.person.last_name);
					$("#vipmisccaravanuser-tel").val(parsed.person.tel);
					$("#vipmisccaravanuser-idno").val(parsed.person.idNo);
					$("#vipmisccaravanuser-mobile").val(parsed.person.mobile);
					$("#vipmisccaravanuser-email").val(parsed.person.email);
					$("#vipmisccaravanuser-birthdate").val(parsed.person.birthdate);
					$("#vipmisccaravanuser-fathername").val(parsed.person.fatherName);
					$('#vipmisccaravanuser-caravanid').val(parsed.person.caravanId).trigger('change');
					$('#vipmisccaravanuser-teamid').val(parsed.person.teamId).trigger('change');
					$('#vipmisccaravanuser-birthcityid').val(parsed.person.birthCityId).trigger('change');
					$('#vipmisccaravanuser-usertype').val(parsed.person.userType).trigger('change');

					$("#file_input_assurancecart").removeAttr('disabled');
					$("#file_input_stucart").removeAttr('disabled');


					if(parsed.userDetailResult!==null)
					{

						$('#usersdetails-userposition').val(parsed.userDetailResult.userPosition);
					}

					$("#vipmisccaravanuser-name").focus();

				}else
				{
					$("input").removeAttr('disabled');
					$("#create_executiveuser").find(':input(:disabled)').prop('disabled',false);
					$('option').attr('selected', false);
                    
                    /*remove banned ckass from file input interfaces*/
					$(".btn-file").on("myRemoveAttrEvent", function(event){
                     $(this).removeAttr("disabled");
                     });
                    $(".btn-file").trigger("myRemoveAttrEvent");
                    $(".kv-fileinput-caption").removeClass("file-caption-disabled");
	                /*remove banned ckass from file input interfaces*/

					$("input[type=text]").val('');
					// $("input[type=text]").val('');
					$("#vipmisccaravanuser-nationalid").val(nationalidValue);


				}




			},
		});
		return false;


	}
SCRIPT
);

?>






<div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<!-- PORTLET MAIN -->
								<div class="portlet light profile-sidebar-portlet ">
									<!-- SIDEBAR USERPIC -->
									<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
										echo $model->getIconThumbnail(null,"img-responsive");
									 }
									 ?>			 
									</div>
									<!-- END SIDEBAR USERPIC -->
									<!-- SIDEBAR USER TITLE -->
									<div class="profile-usertitle" id="user_quick_info">
										<div class="profile-usertitle-name"> <?php // echo Html::encode($fieldObj->name)?> </div>
										<div class="profile-usertitle-job"> <?php //echo Yii::t('app','Team Name').$space.":".$space.Html::encode($teamObj->name)?>  </div>
										<div class="profile-usertitle-job"> <?php //echo Yii::t('app','Gender').$space.":".$space.Html::encode($teamObj->getGenderText())?>  </div>
									</div>
									<!-- END SIDEBAR USER TITLE -->


								</div>
					
							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">
								<div class="row">
									<div class="col-md-12">
										
										 <?= $message ?>
											<div id="results_expand_msg" style="display: none " >
											</div>										
										
										
										<div class="portlet light ">
										
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
														<!-- Render Personal info  form -->	
														<?php

														echo $this->render('_create_athlete_personal_info', [
															'model' => $model,
															'message' => $message,
															'userDetails'=>$userDetails,
														   // 'form'=>$form,


														])

														?>  
																										   <!-- END PERSONAL INFO FORM -->
												   
													
												  
												 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>
