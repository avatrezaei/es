<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\City;
use app\models\Language;
use  yii\web\View;

use kartik\file\FileInput;
use yii\helpers\Url;

 
/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
div.required label.control-label:after {
	content: " *";
	color: red;
}
	</style>
<div id="results" style="display: none" >
	
</div>

<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#new_users").on("pjax:end", function() {
	  
$.pjax.reload({container:"#Users"});  //Reload GridView)
		});
	});'
);
?>


<div class="data-form">
 
<?php yii\widgets\Pjax::begin(['id' => 'new_users']) ?>
	<?= $massage ?>
	<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true,'id'=>'form3' ]]); ?>
	<?php echo $form->errorSummary([$model,$userDetails]);  ?>
	<div class="form-group col-xs-4"> 
	<?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>
	</div>



	<div class="form-group col-xs-4"> 
		<?= $form->field($model, 'last_name')->textInput(['maxlength' => 100]) ?>
	</div>
		  

	<div class="form-group col-xs-4"> 
		<?= $form->field($model, 'nationalId')->textInput(['maxlength' =>10]) ?>
	</div>

	<div class="form-group col-xs-4"> 
	<?= $form->field($model, 'fatherName')->textInput(['maxlength' => 255]) ?>
   </div>

	<div class="form-group col-xs-4">
		<?php
		if(isset($model->birthDate))
		{
		 $model->birthDate=$model->getFaDateFromTimeStamp('birthDate');
		}
		
		echo $form->field($model, 'birthDate')->widget(jDate\DatePicker::className()) ?>
	</div>

  

	
	
		<div class="form-group col-xs-4"> 
		<?php 
 $dataSelectCity = ArrayHelper::map(City::find()->all(), 'id', 'name');   
 echo $form->field($model, 'birthCityId')->widget(Select2::classname(), [
	'data' => $dataSelectCity,
	'language' => 'en',
	'options' => ['placeholder' =>Yii::t('app','Select ...')],
	'pluginOptions' => [
	   'allowClear' => true
	],
]);
		?>
	</div>  
		<div class="form-group col-xs-4"> 


	</div>
	<div class="form-group col-xs-4"> 
	<?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
	</div>
	<div class="form-group col-xs-4"> 
		<?= $form->field($model, 'tel')->textInput(['maxlength' => 40]) ?>
	</div>
	<div class="form-group col-xs-4"> 
		<?= $form->field($model, 'mobile')->textInput(['maxlength' => 40]) ?>
	</div>
		<!-- Render create form -->	
		<?=
		$this->render('_form_user_details', [
			'model' => $model,
			'massage' => $massage,
			'userDetails'=>$userDetails,
			'form'=>$form,

		])
		?>



			  <div class="form-group col-xs-2"> 
 
<?php 

echo $form->field($model, 'fileAttr')->widget(FileInput::classname(), [
	'id' => 'file_input_user_pic',
	'language' => 'fa',
	'options' => [
		'multiple' => true,
		'layoutTemplates' => 'modal',
	],

	'pluginOptions' => [
		'showUpload' => false,
		'maxFileCount' => 1
	]
		
		]);


//echo $form->field($fileModel, 'temporaryFileAttr')->fileInput() ?>
	</div>  
 
	

			  <div class="form-group col-xs-12"> 
 
	<div class="form-group">
		<input type="submit" name="submitBtn" class = "btn btn-sb" value="<?= Yii::t('app', 'Create')?>"id="submit_btn">
		<input type="button" name="submitBtn" class = "btn btn-sb" id="close-btn-1" value="<?= Yii::t('app', 'Close')?>"id="submit_btn">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
	</div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>
</div>
