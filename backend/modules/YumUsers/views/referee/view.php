<?php
/*
 * moshkel update nashodan dar expand:
 * rah hal: tarif id barayr har detail view dar view
 *		  estefadeh az render ajax dar action detailview
 * 
 * 
 * we modify kartik\detail\DetailView.php  line: 938,43
 * to submit form by click on "kv-btn-save"  insted submit form
 * 
 */

use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\AppLanguage;

/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


/****set User detail info base on event Id**/
$model->getUserDetailInfo();

/* set birthDate by jdate for dont problem in beforsave in user model*/
$model->birthDate=$model->getFaDateFromTimeStamp('birthDate');

		   $birthDateAttr=   [
		'attribute' => 'birthDate',
		 'value' => $model->birthDate,

	   // 'format' => 'date',
		'type' => DetailView::INPUT_WIDGET,
		'widgetOptions' => [
			'class' => jDate\DatePicker::className(),
			'options' => [ 'id' => 'from_date_detail_form_' . $model->id,'value'=>$model->birthDate,
],
		],
		'inputWidth' => '40%'
	];
		
				$createDateAttr=	[
		'attribute' => 'created_at',
		 'value' => $model->getFaDateFromTimeStamp('created_at'),
		//'format' => 'date',
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
	$modifiedDateAttr=  [
		'attribute' => 'updated_at',
		 'value' => $model->getFaDateFromTimeStamp('updated_at'),
		//'format' => 'date',
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
		$lastLoginTimeAttr=  [
		'attribute' => 'last_login_time',
		 'value' => $model->getFaDateFromTimeStamp('last_login_time'),
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
$attributes = [
	[
		'attribute' => 'sportNo',
		'displayOnly' => true,
	], 
	[
		'attribute' => 'name',
		'value' => $model->name,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
		[
		'attribute' => 'last_name',
		'value' => $model->last_name,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
					[
		'attribute' => 'nationalId',
		'value' => $model->nationalId,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
	[
		'attribute' => 'idNo',
		'value' => $model->idNo,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
	[
		'attribute' => 'fatherName',
		'value' => $model->fatherName,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],   
	  [
		'attribute' => 'tel',
		'value' => $model->tel,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
						[
		'attribute' => 'mobile',
		'value' => $model->mobile,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
	
		[
		'attribute' => 'gender',
		'value' => Html::encode($model->getSex()),
		'type' => DetailView::INPUT_SWITCH,
		'widgetOptions' => [
			'model' => $model,
			'type'=>2,
			'indeterminateToggle'=>1,
			'tristate' => false,
			'items'=>[['label'=>'زن','value'=>'woman'],['label'=>'مرد','value'=>'man']],
			'attribute' => 'gender',
			'pluginOptions'=>[
				'handleWidth'=>70,
				'onText'=>'انتخاب ',
				'offText'=>'لغو اتنخاب'
			],
			'options' => [ 'id' => 'dtail_gender_form_' . $model->id]
 
		],
		'inputWidth' => '40%'
	],	
	
	 [
		'attribute' => 'homepage',
		'value' => $model->homepage,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
	[
		'attribute' => 'creatorUserId',
		'displayOnly' => true,
		'inputWidth' => '40%',
		'value' => $model->getUserInfo('creatorUserId')
	],
	
	[
		'attribute' => 'email',
		'inputWidth' => '40%',
	],		
 
		[
		'attribute' => 'fileAttr',
		'value' => $model->getIconThumbnail(),
		'type' => DetailView::INPUT_FILE,
		'inputWidth' => '40%',
				'format'=>'raw',
	],
	   $birthDateAttr,

	   [
		'attribute'=>'role',
		'value'=> $model->getUserRolesText(),// var_dump(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())),
		'displayOnly' => true,

	],
	
   $createDateAttr,
   $modifiedDateAttr,
	$lastLoginTimeAttr,
  
];
?>

<div id="results_expand_msg_<?php echo $model->id ?>" style="display: none " >


</div>

<?php
			echo'<div id =wrapped_user_view> ';

echo DetailView::widget([
	'model' => $model,
	'id' => 'detail_view_item_' . $model->id,
	'condensed' => true,
	//'hover' => true,
	'saveBottomType' => 'Bottom', /* this attribute Added by alizadeh to create buttom insted submit buttom  */
	'panel' => [
		'heading' => Yii::t('app', 'User') . ' # ' . $model->id,
		'type' => DetailView::TYPE_INFO,

	],
	'buttons1' => '{deleteexpand} {update}',
	'buttons2' => '{view} {reset} {save}',
	'formOptions' => ['id' => 'form_' . $model->id, 'options' => array("accept-charset" => "UTF-8"), 'action' => ['/YumUsers/referee/complete-edit', 'id' => $model->id], 'enableClientValidation' => TRUE],
	'attributes' => $attributes,
//	'deleteOptions' => [ // your ajax delete parameters
//		'params' => ['id' => $model->id, 'custom_param' => true],
//		'url' => Url::to(['project/delete', 'id' => $model->id]),
//		
//	],
	'saveOptions' => [ // your ajax delete parameters
		'label' => Html::tag('span', '<span id="edit" class="glyphicon glyphicon-floppy-disk"></span>', ['onclick' => "saveItem(" . $model->id . ")"]),
	],
	
	
/* this attribute Added by alizadeh to create expanded buttom insted delete link
 * problem : delete link ajax setting in kartik/detail view didn't work and 
 * we coudn't control response.
 * to coustom control in delete ajax response we add new BTN deleteexpand to  kartik\detail\DetailView.php class
 * find change by search : 'deleteexpand
 */	
	'deleteexpandOptions' => [ 
		'label' => Html::tag('span', '<span id="edit" class="glyphicon glyphicon-trash"></span>', ['onclick' => "saveItem(" . $model->id . ")"]),
	],
]);
			echo'</div> ';

?>

<div id="user_detailes_info">
  <?php
		  echo $this->render('_userdetailView', [
			'model' => $model,

		   ]) ;
  
  ?>
</div>




	<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/js/jquery-1.11.3.min.js', array('position' => $this::POS_HEAD), 'jquery'); ?>

<script type="text/javascript">
	$('.kv-btn-save').click(function () {
		formSTR = $(this).parents('form').attr('id');
		FormId = formSTR.replace('form_', '');
		saveItem(FormId);
	});


	$('.kv-btn-deleteexpand').click(function () {

		formSTR = $(this).parents('form').attr('id');
		FormId = formSTR.replace('form_', '');
		urldelete = "<?php echo Url::to([ '/YumUsers/referee/delete']) ?>" + "&id=" + FormId;
		successmsg = "<?php echo Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')]) ?>"
		failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')]) ?>"
		confMsg = "<?php echo Yii::t('app', 'Are you sure you want to delete this item?', ['item' => Yii::t('app', 'User')]) ?>"
		deleteExpandedItem(FormId,urldelete,confMsg,failmsg);
	});



 



	function deleteExpandedItem(id, urldelete, confMsg, failmsg)
	{

		if (confirm('<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>'))
		{
			$.ajax({
				type: 'get',
				url: urldelete,
				dataType: "JSON",
			  
				success: function (data) {

					/**success delete**/
					if ((typeof data.success !== 'undefined') && data.success == true)
					{
						/*Refresh detail gridview by ajax response data */
						$('#wrapped_user_view' ).html('');
						$('#user_detailes_info' ).html('');
						

						/*provide success Message */
						$("#results_expand_msg_" + id).html("<div class=\"alert alert-success\" role=\"alert\"> " + data.messages + " </div>");


						$("#results_expand_msg_" + id).fadeIn(3500, function () {
						$("#results_expand_msg_" + id).fadeOut( 4000 );
						});

					} else if ((typeof data.success !== 'undefined') && data.success == false) /***********fail save *****************/
					{

						/*Refresh detail gridview by ajax response data */
						$("tr[class^='kv-expand-detail-row'][data-key=" + id + "] td:first-child .kv-expanded-row").html(data.detailviewcontentt);

						/*provide success Message */
						$("#results_expand_msg_" + id).html("<div class=\"alert alert-danger\" role=\"alert\"> " + data.messages + " </div>");


						$("#results_expand_msg_" + id).fadeIn(1300, function () {
						});
					}



				},
				error: function (data) {

					alert(failmsg);
				}
			});
		}


	}

	function saveItem(id)
	{

		var _form = $('#form_' + id);
		var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]) ?>"
		var _failmsg = "<?php echo Yii::t('app', 'Failed To Edit Items To {item}.', ['item' => Yii::t('app', 'User')]) ?>"

		var formData = new FormData($('#form_' + id)[0]);
		formData.append('file', $('input[type=file]')[0].files[0]);

		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: formData,
			processData: false,
			contentType: false,
			dataType: "JSON",
			success: function (data) {

				/**success save**/
				if ((typeof data.errormessage === 'undefined'))
				{
					/*Remove loade class*/
					$('.kv-detail-view').removeClass('kv-detail-loading');
					/*Refresh detail gridview by ajax response data */
					$('#wrapped_user_view' ).html(data.detailviewcontent);
		   
					/*provide success Message */
					$("#results_expand_msg_" + id).html("<div class=\"alert alert-success\" role=\"alert\"> " + _successmsg + " </div>");

					/*
					 *1-fade success message
					 *2-close detailview div slidly
					 *3-reload gridview by pjax 
					 */
					$("#results_expand_msg_" + id).fadeIn(3500, function () {
					$("#results_expand_msg_" + id).fadeOut( 4000 );
					});

				} else  /***********fail save *****************/
				{
					/*Remove loade class*/
					$('.kv-detail-view').removeClass('kv-detail-loading');
					/*Refresh detail gridview by ajax response data */
					$("tr[class^='kv-expand-detail-row'][data-key=" + id + "] td:first-child .kv-expanded-row").html(data.detailviewcontent);

					/*provide success Message */
					$("#results_expand_msg_" + id).html("<div class=\"alert alert-danger\" role=\"alert\"> " + data.errormessage + " </div>");


					$("#results_expand_msg_" + id).fadeIn(1300, function () {
//					   // $.pjax.reload({container: "#Projects"});
					});
				}



			},
			error: function (data) {

				alert(_failmsg);

			}
		});


	}


</script>
