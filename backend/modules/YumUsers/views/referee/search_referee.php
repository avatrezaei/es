<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding"><?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Change Role');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="resultmsg"></div>
<?php



?>
  <?php
	Pjax::begin(['id' => 'SearchAthlete']);
		echo \kartik\grid\GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
  
	[
	'attribute' => 'searchInputName',
		'value' => function ($data) {
	 return $data->name;
		},
	],
	[
	'attribute' => 'searchInputLastName',
		'value' => function ($data) {
	 return $data->last_name;
		},
	],
	[
		'format' => 'raw',
		'value' => function ($data) {
	 return Html::a('انتخاب',Url::to(['/YumUsers/referee/new-user-info','id'=>$data->id]), [
				'title' => Yii::t('yii', 'tttttt'), ]);
		},
	],				 
	],
			// 'layout' => '{items}{summary}',
			'summary' => '',
			'hover' => true,
			'panel' => [
				// 'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th"></i> Projects</h3>',
				'type' => 'primary',
			//'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
			//'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> ' . Yii::t('app', 'Reset Grid'), ['index'], ['class' => 'btn btn-info']),
			//'footer' => true
			],
			'toolbar' => [
				[
//					'content' =>
//					Html::button('<i class="glyphicon glyphicon-plus"></i>', [
//						'type' => 'button',
//						// 'title'=>Yii::t('kvgrid', 'Add Book'), 
//						'title' => 'Reset Grid',
//						'class' => 'btn btn-success'
//					]) . ' ' .
//					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
//						'class' => 'btn btn-default',
//						//'title' => Yii::t('kvgrid', 'Reset Grid')
//						'title' => 'Reset Grid'
//					]),
//					'options' => ['class' => 'btn-group-sm']
				],
				'{export}',
				'{toggleData}',
			],
			'toggleDataContainer' => ['class' => 'btn-group-sm'],
			'exportContainer' => ['class' => 'btn-group-sm'],
			'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
			'headerRowOptions' => ['class' => 'kartik-sheet-style'],
			'filterRowOptions' => ['class' => 'kartik-sheet-style'],
		]);
		Pjax::end();
		?>  