<?php
use kartik\detail\DetailView;
use kartik\detail\DetailViewTiny;
use kartik\detail\tiny;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\AppLanguage;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;

use backend\models\Fields;

/* * ********************************************* */
/*
 * $model->subfields returnarrays of object
 */
   $detailObjarray=$model->usersDetails;

if (!empty($detailObjarray)) {

	foreach ($detailObjarray as $userDetailObj) {
		if (is_object($userDetailObj)) {
			
			echo'<div id =results_user_detail_msg_' . $userDetailObj->id .'></div>';
			echo'<div id =wrapped_detail_view_items_' . $model->id . '_' . $userDetailObj->id . '> ';

			echo DetailViewTiny::widget([
				'model' => $userDetailObj,
				'id' => 'detail_view_items_' . $model->id . '_' . $userDetailObj->id,
				'condensed' => true,
				//'hover' => true,
				'saveBottomType' => 'Bottom', /* this attribute Added by alizadeh to create buttom insted submit buttom  */
				'panel' => [
					'heading' => Yii::t('app', 'Project') . ' # ' . $userDetailObj->id,
					'type' => DetailView::TYPE_INFO,
				],
				'deleteexpandOptions' => [
					'label' => Html::tag('span', '<span id="edit" class="glyphicon glyphicon-trash"></span>'),
				],
				'buttons1' => '{deleteexpand} {update}',
				'buttons2' => '{view} {reset} {save}',
				'formOptions' => ['id' => 'itemform_' . $userDetailObj->id, 'options' => array("accept-charset" => "UTF-8"), 'action' => ['/YumUsers/user-details/edit2', 'id' => $userDetailObj->id], 'enableClientValidation' => TRUE],
				'hover' => true,
				'mode' => DetailView::MODE_VIEW,
				'panel' => [
					'heading' => Yii::t('app', 'UserDetails') . '  رخداد' .' '.Html::encode($userDetailObj->getEventNameText()),
					'type' => DetailView::TYPE_INFO,
					'headingOptions' => [
					// 'template'=>'{title}',
					]
				],
				'attributes' => [
					[
						'attribute' => 'eventId',
						'value' => Html::encode($userDetailObj->getEventNameText()),
						'type' => DetailView::INPUT_SELECT2,
						'widgetOptions' => [
							'model' => $userDetailObj,
							'attribute' => 'eventId',
							'data' => ArrayHelper::map(Event::find()->all(), 'id', 'name') ,
							'options' => ['placeholder' => Yii::t('app', 'Select a ...'), 'id' => 'event_detail_form_' . $userDetailObj->id],
							'pluginOptions' => ['allowClear' => true],
						],
						'inputWidth' => '40%'
					],					

					[
						'attribute' => 'sportFieldId',
						'value' => Html::encode($userDetailObj->getSportFieldNameText()),
						'type' => DetailView::INPUT_SELECT2,
						'widgetOptions' => [
							'model' => $userDetailObj,
							'attribute' => 'educationalField',
							'data' => ArrayHelper::map(Fields::find()->all(), 'id', 'name') ,
							'options' => ['placeholder' => Yii::t('app', 'Select a ...'), 'id' => 'sportFieldId_detail_form_' . $userDetailObj->id],
							'pluginOptions' => ['allowClear' => true],
						],
						'inputWidth' => '40%'
					],					

					[
						'attribute' => 'address',
						'value' =>  Html::encode($userDetailObj->address),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
						'attribute' => 'officeAddress',
						'value' =>  Html::encode($userDetailObj->officeAddress),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
						'attribute' => 'bankAccountNo',
						'value' =>  Html::encode($userDetailObj->bankAccountNo),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],
					[
						'attribute' => 'bankAcoountName',
						'value' =>  Html::encode($userDetailObj->bankAcoountName),
						'type' => DetailView::INPUT_TEXT,
						'inputWidth' => '40%',
					],				   ],
			]);
			echo '</div>';
		}
	}
}
?>


<script type="text/javascript">
	var parent_id =<?php echo $model->id; ?>;
	$('#btn_task_list_' + parent_id).click(function () {
		$('#add_item_ajax_' + parent_id).slideToggle(1000);
		return false;
	});


	$(document).delegate(".tiny-kv-btn-save", 'click', function (e) {
		e.preventDefault();
		formSTR = $(this).parents('form').attr('id');
		FormId = formSTR.replace('itemform_', '');
		itemsuccessmsg = "<?php echo Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'Experience Item')]) ?>"
		itemfailmsg = "<?php echo Yii::t('app', 'Failed To Edit Items To {item}.', ['item' => Yii::t('app', 'Experience Item')]) ?>"

		saveChildItem(FormId, itemsuccessmsg, itemfailmsg);
	});
	
	

	$(document).delegate(".tiny-btn-kv-deleteexpand", 'click', function (e) {
		e.preventDefault();
		formSTR = $(this).parents('form').attr('id');
		FormId = formSTR.replace('itemform_', '');
		urldelete = "<?php echo Url::to([ '/YumUsers/user-details/delete']) ?>" + "&id=" + FormId;
		successmsg = "<?php echo Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Experience')]) ?>"
		failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Experience')]) ?>"
		confMsg = "<?php echo Yii::t('app', 'Are you sure you want to delete this item?', ['item' => Yii::t('app', 'Experience')]) ?>"
		deleteChildItem(FormId, urldelete, confMsg, failmsg);
	});



  
	function saveChildItem(id, itemsuccessmsg, itemfailmsg)
	{
		var _form = $('#itemform_' + id);

		

		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: _form.serialize(),
			dataType: "JSON",		
			success: function (data) {

				/**success save**/
				if ((typeof data.errormessage === 'undefined'))
				{
					/*Remove loade class*/
					$('.kv-detail-view').removeClass('kv-detail-loading');
					$('#wrapped_detail_view_items_' + parent_id + '_' + id).html(data.detailviewcontent);
					$("#results_user_detail_msg_" + id).html("<div class=\"alert alert-success\" role=\"alert\"> اطلاعات با موفقیت ویرایش شد </div>");
					$("#results_user_detail_msg_" + id).fadeOut( 4000 );

				} else  /***********fail save *****************/
				{
					/*Remove loade class*/
					$('.kv-detail-view').removeClass('kv-detail-loading');
					/*Refresh detail gridview by ajax response data */
					$('#wrapped_detail_view_items_' + parent_id + '_' + id).html(data.detailviewcontent);

					/*provide success Message */
					$("#results_user_detail_msg_" + id).html("<div class=\"alert alert-danger\" role=\"alert\"> " + data.errormessage + " </div>");
					$("#results_user_detail_msg_" + id).fadeOut( 4000 );


				}
			},
			error: function (data) {
				$('.kv-detail-view').removeClass('kv-detail-loading');
				alert(itemfailmsg);

			}
		});


	}

	function deleteChildItem(id, urldelete, confMsg, failmsg)
	{
		if (confirm('<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>'))
		{
			$.ajax({
				type: 'get',
				url: urldelete,
				dataType: "JSON",
				success: function (data) {
					/**success delete**/
					if ((typeof data.success !== 'undefined') && data.success == true)
					{
						$('#wrapped_detail_view_items_' + parent_id + '_' + id).slideUp("slow", function () {
							$('#wrapped_detail_view_items_' + parent_id + '_' + id).remove();
						});

					} else if ((typeof data.success !== 'undefined') && data.success == false) /***********fail save *****************/
					{

						$("<div class=\"alert alert-danger\" role=\"alert\"> " + data.messages + " </div>").insertBefore('#wrapped_detail_view_items_' + parent_id + '_' + id).delay(3000).fadeOut();
						;
					}

				},
				error: function (data) {

					alert(failmsg);

				}
			});
		}


	}



   
</script>