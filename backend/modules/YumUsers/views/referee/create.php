<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

	<h1><?= Html::encode($this->title) ?></h1>

			<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true,'id'=>'form3' ]]); ?>
  	<?php echo $form->errorSummary($model);  ?>
	<div class="form-group col-xs-6"> 
 <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>
			</div>
	<div class="form-group col-xs-6"> 
	<?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>
			</div>
   
	
  
	
	
	
 
	<div class="form-group">
		<input type="submit" name="submitBtn" class = "btn btn-sb" value="<?= Yii::t('app', 'Create')?>"id="submit_btn">
		<input type="button" name="submitBtn" class = "btn btn-sb" id="close-btn-1" value="<?= Yii::t('app', 'Close')?>"id="submit_btn">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>


</div>
