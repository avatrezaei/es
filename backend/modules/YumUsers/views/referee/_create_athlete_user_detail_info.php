<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;
use backend\models\Fields;

use kartik\file\FileInput;




$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-4 col-sm-4 control-label';
$errorClass = Yii::$app->params ['errorClass'];

?>


<div class="col-md-6 col-sm-6 col-xs-12">

	<?php
			echo $form->field(
					$userDetails,
					'address',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 








	<?php
 $dataSelectField= ArrayHelper::map(Fields::find()->all(), 'id', 'name');   
			echo $form->field(
					$userDetails, 
					'sportFieldId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectField,
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Select ...'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
		?> 

		 	<?php
			echo $form->field(
					$userDetails,
					'bankAcoountName',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 15, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 


</div>
<div class="col-md-6 col-sm-6 col-xs-12">


	<?php
			echo $form->field(
					$userDetails,
					'officeAddress',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 



	<?php
			echo $form->field(
					$userDetails,
					'refereeDegree',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 


 

 	<?php
			echo $form->field(
					$userDetails,
					'bankAccountNo',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 15, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 

  
</div>
  

