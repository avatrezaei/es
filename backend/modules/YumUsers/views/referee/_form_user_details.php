<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\City;
use backend\models\Fields;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;


use kartik\file\FileInput;


 
/* @var $this yii\web\View */
/* @var $userDetails app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>



		<div class="form-group col-xs-12"> 
		<?php 
 $dataSelectEvent = ArrayHelper::map(Event::find()->all(), 'id', 'name');   
 echo $form->field($userDetails, 'eventId')->widget(Select2::classname(), [
	'data' => $dataSelectEvent,
	'language' => 'fa',
	'options' => ['placeholder' =>Yii::t('app','Select ...')],
	'pluginOptions' => [
	   'allowClear' => true
	],
]);
		?>
	</div>  

	<div class="form-group col-xs-6"> 
		<?= $form->field($userDetails, 'address')->textInput(['maxlength' =>150]) ?>
	</div> 
	<div class="form-group col-xs-6"> 
		<?= $form->field($userDetails, 'officeAddress')->textInput(['maxlength' =>150]) ?>
	</div> 


		<div class="form-group col-xs-12"> 
		<?php 
 $dataSelectEvent = ArrayHelper::map(Fields::find()->all(), 'id', 'name');   
 echo $form->field($userDetails, 'sportFieldId')->widget(Select2::classname(), [
	'data' => $dataSelectEvent,
	'language' => 'fa',
	'options' => ['placeholder' =>Yii::t('app','Select ...')],
	'pluginOptions' => [
	   'allowClear' => true
	],
]);
		?>
	</div> 
	<div class="form-group col-xs-6"> 
		<?= $form->field($userDetails, 'refereeDegree')->textInput(['maxlength' =>150]) ?>
	</div> 
	<div class="form-group col-xs-6"> 
		<?= $form->field($userDetails, 'bankAccountNo')->textInput(['maxlength' =>15]) ?>
	</div> 
	<div class="form-group col-xs-6"> 
		<?= $form->field($userDetails, 'bankAcoountName')->textInput(['maxlength' =>15]) ?>
	</div> 

