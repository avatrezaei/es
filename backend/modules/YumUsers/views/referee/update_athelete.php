<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app', 'Update Referee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
					
					<!-- BEGIN PAGE HEADER-->
					<!-- BEGIN THEME PANEL -->
					<div class="theme-panel hidden-xs hidden-sm">
						
						
						
					</div>
					<!-- END THEME PANEL -->
				  
					<!-- BEGIN PAGE TITLE-->
					<h3 class="page-title"> <?php echo Yii::t('app','Update Referee')?>
					</h3>
					<!-- END PAGE TITLE-->
					<!-- END PAGE HEADER-->
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<!-- PORTLET MAIN -->
								<div class="portlet light profile-sidebar-portlet ">
									<!-- SIDEBAR USERPIC -->
									<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
										echo $model->getIconThumbnail(null,"img-responsive");
									 }
									 ?>			 
									</div>
									<!-- END SIDEBAR USERPIC -->
									<!-- SIDEBAR USER TITLE -->
									<div class="profile-usertitle" id="user_quick_info">
										<div class="profile-usertitle-name"> <?php echo Html::encode($model->FullName)?></div>
										<div class="profile-usertitle-job"> <?php echo Html::encode($model->nationalId)?> </div>
									</div>
									<!-- END SIDEBAR USER TITLE -->
									<!-- SIDEBAR BUTTONS -->
									<div class="profile-userbuttons">
										<?php $space='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'?>
										<?php echo Html::a($space.Yii::t('app','View').$space, \yii\helpers\Url::to(['/YumUsers/referee/view','id'=>$model->id],true),['class'=>"btn green btn-sm"]);?>
										<?php echo Html::a($space.Yii::t('app','Management').$space, \yii\helpers\Url::to(['/YumUsers/referee/index'],true),['class'=>"btn blue btn-sm"]);?>
									</div>
									<!-- END SIDEBAR BUTTONS -->
									<!-- SIDEBAR MENU -->
									<div class="profile-usermenu">
										<ul class="nav">
											<li>
												<a href="#">
													<i class="icon-info"></i> <?php echo Yii::t('app','Sport Resume')?></a>
											</li>
											<li class="active">
												<a href="#">
													<i class="icon-settings"></i>  <?php echo Yii::t('app','Medal of honor')?> </a>
											</li>
											<li>
												<a href="#">
													<i class="icon-info"></i> <?php echo Yii::t('app','Matches')?>  </a>
											</li>
										</ul>
									</div>
									<!-- END MENU -->
								</div>
								<!-- END PORTLET MAIN -->
								<!-- PORTLET MAIN -->
							  
								<!-- END PORTLET MAIN -->
							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">
								<div class="row">
									<div class="col-md-12">
										
										 <?= $massage ?>
<div id="results_expand_msg" style="display: none " >
</div>										
										
										
										<div class="portlet light ">
											<div class="portlet-title tabbable-line">
												<div class="caption caption-md">
													<i class="icon-globe theme-font hide"></i>
													
													<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'Profile Account')?></span>
												</div>
												<ul class="nav nav-tabs">
													<li class="active">
														<a href="#tab_1_1" data-toggle="tab"><?php echo Yii::t('app', 'Personal Info')?></a>
													</li>
													<li>
														<a href="#tab_1_2" data-toggle="tab"><?php echo Yii::t('app', 'Change Avatar')?></a>
													</li>
													<li>
														<a href="#tab_1_3" data-toggle="tab"><?php echo Yii::t('app', 'Change Password')?> </a>
													</li>
													<li>
														<a href="#tab_1_4" data-toggle="tab"><?php echo Yii::t('app', 'More Personal Info')?> </a>
													</li>
												</ul>
											</div>
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
														<!-- Render Personal info  form -->	
														<?php

														echo $this->render('_update_athlete_personal_info', [
															'model' => $model,
															'massage' => $massage,
															'userDetails'=>$userDetails,
														   // 'form'=>$form,


														])

														?>													   
														<!-- END PERSONAL INFO FORM -->
													<!-- CHANGE Picture TAB -->
														<?php

														echo $this->render('_update_athlete_change_pic', [
															'model' => $model,
															'massage' => $massage,
														   'changePasswordModel'=>$changePasswordModel
														   // 'form'=>$form,


														])

														?>   
													<!-- END CHANGE Picture TAB -->
													
													<!-- CHANGE PASSWORD TAB -->
													
													   <?php

														echo $this->render('_update_athlete_change_pass', [
															'model' => $model,
															'massage' => $massage,
														   'changePasswordModel'=>$changePasswordModel
														   // 'form'=>$form,
														])

														?>													

													<!-- END CHANGE PASSWORD TAB -->
													<!-- User Detail Info TAB -->

													   <?php

														echo $this->render('_update_athlete_use_detail_info', [
															'model' => $model,
															'massage' => $massage,
															'userDetails'=>$userDetails,
														   // 'form'=>$form,
														])

														?> 
																										
												  
													<!-- User Detail Info TAB -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>
