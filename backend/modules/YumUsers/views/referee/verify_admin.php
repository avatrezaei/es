<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Verify Password');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
	<h1><?= Html::encode($this->title) ?></h1>
 <div id="results" style="display: none" >
	
</div>
<div >

		
	<?= $massage ?>
	<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'id' => 'verify_pass']]); ?>
	<?php echo $form->errorSummary($model); ?>

	
	
	
	
	
	
	<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword')->passwordInput(['maxlength' => 255]) ?>
			</div>
		<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword_repeat')->passwordInput(['maxlength' => 255]) ?>
			</div>

		 
	
	
 
	<div class="form-group">
		<input type="submit" id="btn-verify-pass" name="submitBtn" class = "btn btn-sb" value="<?= Yii::t('app', 'Create')?>"id="submit_btn">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>
</div>

	<script>
//		$(document).delegate("#btn-verify-pass", 'click', function (e) {
//		e.preventDefault();
//	  // formSTR = $(this).parents('form').attr('id');
// var _form = $(this).parents('form');
//
//		$.ajax({
//			type: _form.attr('method'),
//			url: _form.attr('action'),
//			data: _form.serialize(),
//			dataType: "JSON",
//			success: function (data) {
//
//				/**success save**/
//				if ((typeof data.errormessage === 'undefined'))
//				{
//					/*Remove loade class*/
//					$('.kv-detail-view').removeClass('kv-detail-loading');
//					$('#wrapped_detail_view_items_' + parent_id + '_' + id).html(data.detailviewcontent);
//
//
//				} else  /***********fail save *****************/
//				{
//					/*Remove loade class*/
//					$('.kv-detail-view').removeClass('kv-detail-loading');
//					// alert('save fail')
//
//				}
//			},
//			error: function (data) {
//				$('.kv-detail-view').removeClass('kv-detail-loading');
//				alert(itemfailmsg);
//
//			}
//		});
//
//
//	});
		</script>
