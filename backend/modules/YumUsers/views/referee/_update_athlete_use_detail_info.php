<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;
use backend\models\Fields;


$formGroupClass = "form-group";
$template = '{label}{input} {hint} {error}';
$inputClass = "form-control";
$labelClass ="control-label";
$errorClass = Yii::$app->params['errorClass'];

?>



<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#update_user").on("pjax:end", function() {
		
$("#user_quick_info").load(document.URL +  " #user_quick_info");
	  
  //Reload GridView)
		});
	});'
);
?>
<div class="tab-pane" id="tab_1_4">
	<?php //yii\widgets\Pjax::begin(['id' => 'update_user']) ?>
   
	<?php $form_user_detail = ActiveForm::begin(['options' => ['data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>
	<?php
		
		//if(isset($_POST['update_prsonal_info_btn']))
		echo $form_user_detail->errorSummary([$model,$userDetails], ['class' => 'alert alert-danger']);
		
		?>

	
	<?php
			echo $form_user_detail->field(
					$userDetails,
					'address',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 




	<?php
			echo $form_user_detail->field(
					$userDetails,
					'officeAddress',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 


	<?php
			$dataSelectField= ArrayHelper::map(Fields::find()->all(), 'id', 'name');   
			echo $form_user_detail->field(
					$userDetails, 
					'sportFieldId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectField,
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Select ...'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
	?>  

 	<?php
			echo $form_user_detail->field(
					$userDetails,
					'refereeDegree',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
	?> 

	<?php
			echo $form_user_detail->field(
					$userDetails,
					'bankAccountNo',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
	?> 

	<?php
			echo $form_user_detail->field(
					$userDetails,
					'bankAcoountName',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 150, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
	?> 


	
	
		<div class="margiv-top-10">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn green','name'=>'update_user_detail_info_btn'])	?>
		<?= Html::submitButton(Yii::t('app', 'Cancel') , ['class' => 'btn default'])	?>
		</div>

   <?php ActiveForm::end(); ?>
<?php //yii\widgets\Pjax::end() ?>	 
</div>