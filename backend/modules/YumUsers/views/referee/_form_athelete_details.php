<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;


use kartik\file\FileInput;


 
/* @var $this yii\web\View */
/* @var $userDetails app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>


	<div class="form-group col-xs-12"> 
		<?php 
 $dataSelectEvent = ArrayHelper::map(Event::find()->all(), 'id', 'name');   
 echo $form->field($userDetails, 'eventId')->widget(Select2::classname(), [
	'data' => $dataSelectEvent,
	'language' => 'en',
	'options' => ['placeholder' =>Yii::t('app','Select ...')],
	'pluginOptions' => [
	   'allowClear' => true
	],
]);
		?>
	</div>  

	

	<div class="form-group col-xs-6"> 
		<?= $form->field($userDetails, 'sportAssuranceNo')->textInput(['maxlength' =>15]) ?>
	</div> 
	<div class="form-group col-xs-6"> 
		<?= $form->field($userDetails, 'stuNo')->textInput(['maxlength' =>15]) ?>
	</div> 





	<div class="form-group col-xs-6"> 
		<?php 
 $dataSelectEducationalSections= ArrayHelper::map(EducationalSections::find()->all(), 'EduSecCode', 'PEduSecName');   
 echo $form->field($userDetails, 'educationalSection')->widget(Select2::classname(), [
	'data' => $dataSelectEducationalSections,
	'language' => 'en',
	'options' => ['placeholder' =>Yii::t('app','Select ...')],
	'pluginOptions' => [
	   'allowClear' => true
	],
]);
		?>
	</div> 

	<div class="form-group col-xs-6"> 
		<?php 
 $dataSelectEducationalGroups = ArrayHelper::map(EducationalGroups::find()->all(), 'EduGrpCode', 'PEduName');   
 echo $form->field($userDetails, 'educationalField')->widget(Select2::classname(), [
	'data' => $dataSelectEducationalGroups,
	'language' => 'en',
	'options' => ['placeholder' =>Yii::t('app','Select ...')],
	'pluginOptions' => [
	   'allowClear' => true
	],
]);
		?>
	</div> 




	<div class="form-group col-xs-2"> 


<?php 

echo $form->field($userDetails, 'fileAttrAssuranceCart')->widget(FileInput::classname(), [
	'id' => 'file_input_assurancecart',
	'language' => 'fa',
	'options' => [
		'multiple' => true,
		'layoutTemplates' => 'modal',
	],

	'pluginOptions' => [
		'showUpload' => false,
		'maxFileCount' => 1
	] 
	]);
?>
	</div>

	<div class="form-group col-xs-2"> 


<?php 

echo $form->field($userDetails, 'fileAttrStuCart')->widget(FileInput::classname(), [
 
	'id' => 'file_input_stucart',
	'language' => 'fa',
	'options' => [
		'multiple' => true,
		'layoutTemplates' => 'modal',
	],

	'pluginOptions' => [
		'showUpload' => false,
		'maxFileCount' => 1
	]
		
		]);

?>
	</div>


	<div class="form-group col-xs-4"> 

	   <?php //echo $form->field($userDetails, 'fileAttrAssuranceCart')->fileInput() ?>
	</div>

	<div class="form-group col-xs-4"> 

	   <?php //echo $form->field($userDetails, 'fileAttrStuCart')->fileInput() ?>
	</div>
