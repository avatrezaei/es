<?php

use yii\helpers\Html;
use backend\assets\UserProfile;

UserProfile::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app', 'View Referee');

?>
<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
			<div class="profile-sidebar">
				<div class="portlet light profile-sidebar-portlet ">
					<div class="profile-userpic">
					<?php
						if(is_object($model))
						{
							echo $model->getIconThumbnail(null,"img-responsive");
						}
					?>
					</div>

					<div class="profile-usertitle" id="user_quick_info">
						<div class="profile-usertitle-name"> <?php echo Html::encode($model->FullName)?></div>
						<div class="profile-usertitle-job"> <?php echo Html::encode($model->nationalId)?> </div>
					</div>

					<div class="profile-userbuttons">
						<?php echo Html::a(Yii::t('app','Edit'), \yii\helpers\Url::to(['/YumUsers/referee/update-referee','userId'=>$model->id],true),['class'=>"btn green btn-sm"]);?>
						<?php echo Html::a(Yii::t('app','Change Password'), \yii\helpers\Url::to(['/YumUsers/referee/update-referee','userId'=>$model->id,'#' => 'tab_1_3'],true),['class'=>"btn red btn-sm"]);?>
					</div>

					<div class="profile-usermenu">

					</div>
				</div>

				<div class="portlet light ">
					<div class="row list-separated profile-stat">
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="uppercase profile-stat-title"> 37 </div>
							<div class="uppercase profile-stat-text"> <?php echo Yii::t('app','Medal of honor')?></div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="uppercase profile-stat-title"> 51 </div>
							<div class="uppercase profile-stat-text"><?php echo Yii::t('app','Matches')?> </div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="uppercase profile-stat-title"> 2 </div>
							<div class="uppercase profile-stat-text"> <?php echo Yii::t('app','Cart')?> </div>
						</div>
					</div>
					<div>
						<h4 class="profile-desc-title"><?php echo Yii::t('app', 'About User')?></h4>
						<span class="profile-desc-text"> <?php echo Yii::t('app','Athelete Resume Default')?> </span>
						<div class="margin-top-20 profile-desc-link">
							<i class="fa fa-globe"></i>
							<a href="http://www.keenthemes.com">www.keenthemes.com</a>
						</div>
						<div class="margin-top-20 profile-desc-link">
							<i class="fa fa-twitter"></i>
							<a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
						</div>
					 
					</div>
				</div>
			</div>
			<?php
				echo $this->render('user_info_view', [
					'model' => $model,
					'eventId'=>$eventId

				])
			?>
		</div>
	</div>
</div>