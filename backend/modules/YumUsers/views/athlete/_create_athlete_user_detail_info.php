<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;
use backend\models\Fields;

use kartik\file\FileInput;




$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-4 col-sm-4 control-label';
$errorClass = Yii::$app->params ['errorClass'];

?>
<style>
hr {
	display: block;
	height: 1px;
	border: 0;
	border-top: 1px solid #ccc;
	margin: 1em 0;
	padding: 0; 
}
</style>
<div class="col-md-12 col-sm-12 col-xs-12">
<br>
<br>

<div class="caption">
  <i class="icon-user font-green-sharp"></i>
  <span class="caption-subject font-green-sharp bold uppercase"><?php echo Yii::t('app','More Info')?></span>
 </div>
   <hr>

 </div>



<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">

		<?php
		echo $form->field(
			$userDetails,
			'sportAssuranceNo',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->textInput(
				['maxlength' => 15, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">



		<?php
		echo $form->field(
			$userDetails,
			'stuNo',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->textInput(
				['maxlength' => 15, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>

	</div>
</div>




<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">


		<?php
		$dataSelectEducationalSections= ArrayHelper::map(EducationalSections::find()->all(), 'EduSecCode', 'PEduSecName');
		echo $form->field(
			$userDetails,
			'educationalSection',
			[
				'options' => ['class' => $formGroupClass,'disabled'=>true],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => $dataSelectEducationalSections,
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Select ...'),
					'dir' => 'rtl',

				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>



	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
		$dataSelectEducationalGroups = ArrayHelper::map(EducationalGroups::find()->all(), 'EduGrpCode', 'PEduName');
		echo $form->field(
			$userDetails,
			'educationalField',
			[
				'options' => ['class' => $formGroupClass,'disabled'=>true],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => $dataSelectEducationalGroups,
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Select ...'),
					'dir' => 'rtl',

				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
</div>


<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">

		<?php
		$dataSelectField= ArrayHelper::map(Fields::find()->all(), 'id', 'name');
		echo $form->field(
			$userDetails,
			'sportFieldId',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => $dataSelectField,
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Select ...'),
					'dir' => 'rtl',
				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>


	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
		echo $form->field(
			$userDetails,
			'sportMembershipHistory',
			[
				'options' => ['class' => $formGroupClass,'disabled'=>true],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => \backend\modules\YumUsers\models\UsersDetails::itemAlias('sportMembershipHistory'),
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Select ...'),
					'dir' => 'rtl',

				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
</div>




 <div class="col-md-12 col-sm-12 col-xs-12">
<br>
<br>

<div class="caption">
  <i class="icon-doc font-green-sharp"></i>
  <span class="caption-subject font-green-sharp bold uppercase"><?php echo Yii::t('app','Upload File')?></span>
 </div>
   <hr>

 </div>

<div class="col-md-6 col-sm-6"> 
 
<?php 

 echo $form->field($userDetails, 'fileAttrAssuranceCart')->widget(FileInput::classname(), [
	 'id' => 'file_input_assurancecart',
	 'language' => 'fa',
	 'options' => [
		 'multiple' => true,
		 'layoutTemplates' => 'modal',

	 ],
	 'pluginOptions' => [
		 'showUpload' => false,
		 'maxFileCount' => 1,
		 'msgInvalidFileExtension'=>Yii::t('app','Invalid extension for file . Only "{extensions}" files are supported.'),
		 'allowedFileExtensions'=>$model->fileExtentionsArr,
		 'msgSizeTooLarge'=>Yii::t('app','File  ({size} KB) exceeds maximum allowed upload size of {maxSize} KB. Please retry your upload!'),
		 'maxFileSize'=>$model->fileSizeKB,
		 'previewFileType' => 'image',
		 'showRemove'=>false,
	 ]
		
		 ])->label(NULL, ['class'=>$labelClass]); ;
?>

	</div>  





<div class="col-md-6 col-sm-6"> 
 
<?php 

 echo $form->field($userDetails, 'fileAttrStuCart')->widget(FileInput::classname(), [
	 'id' => 'file_input_stucart',
	 'language' => 'fa',
	 'options' => [
		 'multiple' => true,
		 'layoutTemplates' => 'modal',

	 ],
	 'pluginOptions' => [
		 'showUpload' => false,
		 'maxFileCount' => 1,
		 'msgInvalidFileExtension'=>Yii::t('app','Invalid extension for file . Only "{extensions}" files are supported.'),
		 'allowedFileExtensions'=>$model->fileExtentionsArr,
		 'msgSizeTooLarge'=>Yii::t('app','File  ({size} KB) exceeds maximum allowed upload size of {maxSize} KB. Please retry your upload!'),
		 'maxFileSize'=>$model->fileSizeKB,
		 'previewFileType' => 'image',
		 'showRemove'=>false,
	 ]
		
		 ])->label(NULL, ['class'=>$labelClass]); ;
?>

	</div>  





