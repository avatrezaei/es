<?php

use yii\widgets\DetailView;
use kartik\detail\DetailViewTiny;
use kartik\detail\tiny;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\AppLanguage;
use yii\base\View;
use backend\assets\ImgShowModal;
ImgShowModal::register($this);


$userDetailObj=$model->getUserDetailBaseOnEvent($eventId);
if($userDetailObj!==null):

?>

<?php
	echo DetailView::widget([
		'model' => $userDetailObj,
		'template' => Yii::$app->params['detalView'],
		'options' => [
		'tag' => 'div'],
		'id' => 'detail_view_item_' . $model->id,
		'attributes' => [
			[
				'attribute' => 'eventId',
				'value' => Html::encode($userDetailObj->getEventNameText()),		
			],
			[
				'attribute' => 'sportAssuranceNo',
				'value' => Html::encode($userDetailObj->sportAssuranceNo),	
			],
			[
				'attribute' => 'stuNo',
				'value' =>  Html::encode($userDetailObj->stuNo),
			],
			[
				'attribute' => 'educationalSection',
				'value' => Html::encode($userDetailObj->getEducationalSectionNameText()),
			],
			[
				'attribute' => 'educationalField',
				'value' => Html::encode($userDetailObj->getEducationalGroupsNameText()),

			],
			[
				'attribute' => 'fileAttrStuCart',
				'value' => $userDetailObj->getStuCartIconThumbnail(),
				'format'=>'raw',
			],
			[
				'attribute' => 'fileAttrAssuranceCart',
				'value' => $userDetailObj->getAssuranceCartIconThumbnail(),
				'format'=>'raw',
			],
		],
	]);
?>
<?php endif; ?>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<img class="img-responsive-modal" src="" />
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app','Close')?></button>
			</div>
		</div>
	</div>
</div>

	