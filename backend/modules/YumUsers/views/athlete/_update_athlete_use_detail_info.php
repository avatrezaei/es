<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;
use backend\models\Fields;
use kartik\file\FileInput;


$formGroupClass = "form-group";
$template = '{label}{input} {hint} {error}';
$inputClass = "form-control";
$labelClass ="control-label";
$errorClass = Yii::$app->params['errorClass'];

?>



<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#update_user").on("pjax:end", function() {
		
$("#user_quick_info").load(document.URL +  " #user_quick_info");
	  
  //Reload GridView)
		});
	});'
);
?>
<div class="tab-pane" id="tab_1_4">
	<?php //yii\widgets\Pjax::begin(['id' => 'update_user']) ?>
   
	<?php $form_user_detail = ActiveForm::begin(['options' => ['data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>
	<?php
		
		//if(isset($_POST['update_prsonal_info_btn']))
		echo $form_user_detail->errorSummary([$model,$userDetails], ['class' => 'alert alert-danger']);
		
		?>

	
	<?php
			echo $form_user_detail->field(
					$userDetails,
					'sportAssuranceNo',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 15, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 




	<?php
			echo $form_user_detail->field(
					$userDetails,
					'stuNo',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 15, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 


	<?php
			$dataSelectEducationalSections= ArrayHelper::map(EducationalSections::find()->all(), 'EduSecCode', 'PEduSecName');   
			echo $form_user_detail->field(
					$userDetails, 
					'educationalSection',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectEducationalSections,
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Select ...'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
	?>

  	<?php
 $dataSelectEducationalGroups = ArrayHelper::map(EducationalGroups::find()->all(), 'EduGrpCode', 'PEduName');   
			echo $form_user_detail->field(
					$userDetails, 
					'educationalField',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectEducationalGroups,
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Select ...'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
	?>

	<?php
	$dataSelectField= ArrayHelper::map(Fields::find()->all(), 'id', 'name');
	echo $form_user_detail->field(
		$userDetails,
		'sportFieldId',
		[
			'options' => ['class' => $formGroupClass],
			'template' => $template
		]
	)
		->widget(Select2::classname(), [
			'data' => $dataSelectField,
			'theme' => Select2::THEME_BOOTSTRAP,
			'options' => [
				'placeholder' => Yii::t('app', 'Select ...'),
				'dir' => 'rtl',
			],
		])
		->label(NULL, ['class'=>$labelClass]);
	?>
	<?php
	echo $form_user_detail->field(
		$userDetails,
		'sportMembershipHistory',
		[
			'options' => ['class' => $formGroupClass],
			'template' => $template
		]
	)
		->widget(Select2::classname(), [
			'data' => \backend\modules\YumUsers\models\UsersDetails::itemAlias('sportMembershipHistory'),
			'theme' => Select2::THEME_BOOTSTRAP,
			'options' => [
				'placeholder' => Yii::t('app', 'Select ...'),
				'dir' => 'rtl',
			],
		])
		->label(NULL, ['class'=>$labelClass]);
	?>


	 <div class="form-group col-xs-12"> 
							<?php
							echo $form_user_detail->field($userDetails, 'fileAttrAssuranceCart')->widget(FileInput::classname(),
										[
											'id' => 'file_input_assurancecart',
											'language' => 'fa',
											'options' => [
												'multiple' => true,
												'layoutTemplates' => 'modal',
											],

											'pluginOptions' => [
												'showUpload' => false,
												'maxFileCount' => 1
											]

										]);
							?>
		</div>
		 <div class="form-group col-xs-12"> 
							<?php
							echo $form_user_detail->field($userDetails, 'fileAttrStuCart')->widget(FileInput::classname(),
										[
											'id' => 'file_input_stucart',
											'language' => 'fa',
											'options' => [
												'multiple' => true,
												'layoutTemplates' => 'modal',
											],

											'pluginOptions' => [
												'showUpload' => false,
												'maxFileCount' => 1
											]

										]);
							?>
		</div>
	
		<div class="margiv-top-10">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn green','name'=>'update_user_detail_info_btn'])	?>
		<?php // echo Html::submitButton(Yii::t('app', 'Cancel') , ['class' => 'btn default'])	?>
		</div>

   <?php ActiveForm::end(); ?>
<?php //yii\widgets\Pjax::end() ?>	 
</div>