<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use backend\assets\ToastrAsset;
ToastrAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app','Media Staff Management');


$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Add').' '.Yii::t('app', 'Media Staff'),
		'url' => ['create'],
		'options' => [
			'class' => 'btn btn-success'
		]
	]
];
?>
<?php

$updateText = Yii::t('app', 'Change Event');

$this->registerJs(
		<<<SCRIPT
            $('body').on('click','.status', function(){
        var a = $(this);
        var response = null;

        jQuery.ajax({
            type:'GET',
            url:a.attr('href'),
            cache:false,
            'success':function(data){
                var obj = jQuery.parseJSON(data);
                response = obj;

                toastr.options = {
                    'closeButton': true,
                    'positionClass': "toast-top-right",
                }
                toastr[response.message.search("!") != -1 ? 'error' : 'success'](response.message, '$updateText')
            },
            'complete': function(){
                if(response != null)
                {
                    a.attr('data-hint', response.hint)
                    a.html(response.content);

                    if(response.enable)
                    {
                        a.removeClass('hint--error');
                        a.addClass('hint--success');
                    }
                    else
                    {
                        a.removeClass('hint--success');
                        a.addClass('hint--error');
                    }
                }
            }
        });
        return false;
    });
SCRIPT
		, yii\web\View::POS_END);


?>
<?php
	/* ------------show flash messages----------------- */
	$session = Yii::$app->session;
   // check the availability
	$result = $session->hasFlash('manage_team_member');
   // get and display the message
	echo $session->getFlash('manage_team_member');
	?>
<div class="user-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-user font-dark"></i>
				<span class="caption-subject bold uppercase"><?= Yii::t('app','Media Staff') ?></span>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">
			<?= $widget->run() ?>
		</div>
	</div>
</div>