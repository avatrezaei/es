<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\City;
use app\models\Language;
use  yii\web\View;

use kartik\switchinput\SwitchInput;
use kartik\file\FileInput;
use yii\helpers\Url;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;



$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3';
$errorClass = Yii::$app->params ['errorClass'];



?>



<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#update_user").on("pjax:end", function() {
		
//$("#user_quick_info").load(document.URL +  " #date-format");
	  
  //Reload GridView)
		});
	});'
);
?>

<div class="tab-pane active" id="tab_1_1">
	<?php yii\widgets\Pjax::begin(['id' => 'update_user']) ?>


	<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>

	 <?= $massage ?>

	<?php

	if(!empty($userDetails)) {
		echo $form->errorSummary([$model, $userDetails], ['class' => 'alert alert-danger']);
	}else
	{
		echo $form->errorSummary([$model], ['class' => 'alert alert-danger']);

	}
		
		?>

		<div class="form-body">

	
	
	<div class="row">
			<div class="col-md-6">
	
		<?php
			echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 52, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>	
</div>	
			<div class="col-md-6">	
			<?php
			echo $form->field(
					$model,
					'last_name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 52, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
	</div>	
	
	
	
	<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'national_code',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
	</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
	
		<?php
			echo $form->field(
					$model,
					'idNo',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 10, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
</div></div>
		<?php
			/*echo $form->field(
					$model,
					'nationalId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 10, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);*/
		?>	
		
		
	<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'fatherName',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>	
		</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'university_name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		
		</div></div>
		
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'student_number',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
			<div id="date-format">
				<?php
				$model->birthDate =$model->dateDisplayFormat();
				?>
	<?=$form->field ( $model, 'birthDate',
		[ 'options' => [ 'class' => $formGroupClass,
			//'value' => $model->start,
		],
											   'template' => $template
											   ] 
											   )->widget ( jalaliDatePicker::className (), [ 'options' => ['format' => 'yyyy/mm/dd','viewformat' => 'yyyy/mm/dd','placement' => 'right','todayBtn' => 'linked','id' => 'event-birthDate','class' => 'form-control'  ] ] )->label ( NULL, [ 'class' => $labelClass ] )?>
	
		</div>
		</div></div>
		
		
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
			echo $form->field(
					$model,
					'email',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>  
			</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'tel',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>  
		
</div></div>


<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">		
		<?php
			echo $form->field(
					$model,
					'mobile',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 
		</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'address',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 
		
	</div></div>	
		
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'jobincome',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'shift',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		
		</div></div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'startingyear',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'startingsemester',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		
	</div></div>	
		
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'units',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
		</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
			echo $form->field(
					$model,
					'major',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>



</div></div>
			
			
			
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">	
		<?php
					   $dataSelectCity = ArrayHelper::map(City::find()->all(), 'id', 'name');   
			echo $form->field(
					$model, 
					'birthCityId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectCity,
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Select ...'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
		?>
</div>	
<div class="col-md-6 col-sm-6 col-xs-12">		
			<?php
					   $dataSelectCity = ArrayHelper::map(City::find()->all(), 'id', 'name');   
			echo $form->field(
					$model, 
					'city_location_id',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->widget(Select2::classname(), [
					'data' => $dataSelectCity,
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => [
						'placeholder' => Yii::t('app', 'Select ...'),
						'dir' => 'rtl',
					],
				])
				->label(NULL, ['class'=>$labelClass]); 
		?>
	</div></div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
	<?php
				echo $form->field($model, 'gender',
					['template'=>'{label}<div class="col-md-6"><div class="btn-group" data-toggle="buttons" id="user-gender">{input}</div> {hint} {error}</div>']
				)
					->radioList(
						$model->itemAlias('Gender'),
						[
							'unselect' => NULL,
							'tag' => false,
							'item' => function($index, $label, $name, $checked, $value) {
								return '<label class="btn btn-info '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value]) . $label . '</label>';
							}
						]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>  
		</div>	
<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
				echo $form->field($model, 'marital',
					['template'=>'{label}<div class="col-md-6"><div class="btn-group" data-toggle="buttons" id="user-marital">{input}</div> {hint} {error}</div>']
				)
					->radioList(
						$model->itemAlias('Marital'),
						[
							'unselect' => NULL,
							'tag' => false,
							'item' => function($index, $label, $name, $checked, $value) {
								return '<label class="btn btn-info '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value]) . $label . '</label>';
							}
						]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>  
		
	</div></div>
	</div>
		<div class="margiv-top-10">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn green','name'=>'update_prsonal_info_btn'])	?>
		<?php //echo Html::submitButton(Yii::t('app', 'Cancel') , ['class' => 'btn default'])	?>
		</div>

   <?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>
</div>