<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app', 'Profile');

$this->params['breadcrumbs'][] = $this->title;
?>
					
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<!-- PORTLET MAIN -->
								<div class="portlet light profile-sidebar-portlet ">
									<!-- SIDEBAR USERPIC -->
									<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
										echo $model->getIconThumbnail(null,"img-responsive");
									 }
									 ?>			 
									</div>
									<!-- END SIDEBAR USERPIC -->
									<!-- SIDEBAR USER TITLE -->
									<div class="profile-usertitle" id="user_quick_info">
										<div class="profile-usertitle-name"> <?php echo Html::encode($model->FullName)?></div>
										<div class="profile-usertitle-job"> <?php echo Html::encode($model->nationalId)?> </div>
									</div>
									<!-- END SIDEBAR USER TITLE -->
									<!-- SIDEBAR BUTTONS -->
									<div class="profile-userbuttons">
										<?php $space='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'?>
										<?php echo Html::a($space.Yii::t('app','Edit').$space, \yii\helpers\Url::to(['/YumUsers/members/update-user','userId'=>$model->id],true),['class'=>"btn green btn-sm"]);?>
										<?php //echo Html::a($space.Yii::t('app','Change Password').$space, \yii\helpers\Url::to(['YumUsers/members/update-user','userId'=>$model->id],true),['class'=>"btn red btn-sm"]);?>
									</div>
									<!-- END SIDEBAR BUTTONS -->
									<!-- SIDEBAR MENU -->
									<div class="profile-usermenu">

									</div>
									<!-- END MENU -->
								</div>
								<!-- END PORTLET MAIN -->

							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
													   <?php

														echo $this->render('user_info_view', [
															'model' => $model,
													 
														])

														?>  
								 <!-- END PROFILE CONTENT -->
						</div>
					</div>
