<?php

use kartik\detail\DetailView;
use kartik\detail\DetailViewTiny;
use kartik\detail\tiny;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\AppLanguage;

//$this->title = Yii::t('app', 'Profile');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => '#'];
//$this->params['breadcrumbs'][] = $this->title;

//die(var_dump($model->getEnDateFromTimeStamp('created_at')));
if (isset($_COOKIE[AppLanguage::cookieName]) && $_COOKIE[AppLanguage::cookieName] == AppLanguage::en_US) {

   
	$createDateAttr=	[
		'attribute' => 'created_at',
		 'value' => $model->getEnDateFromTimeStamp('created_at'),
		//'format' => 'date',
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
	$modifiedDateAttr=  [
		'attribute' => 'updated_at',
		 'value' => $model->getEnDateFromTimeStamp('updated_at'),
		//'format' => 'date',
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
			$lastLoginTimeAttr=[
		'attribute' => 'last_login_time',
		 'value' => $model->getEnDateFromTimeStamp('last_login_time'),
		//'format' => 'date',
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
	/**----------Provide Persion data------------------------**/
} else {
   
		$createDateAttr=	[
		'attribute' => 'created_at',
		 'value' => $model->getFaDateFromTimeStamp('created_at'),
		//'format' => 'date',
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
	$modifiedDateAttr=  [
		'attribute' => 'updated_at',
		 'value' => $model->getFaDateFromTimeStamp('updated_at'),
		//'format' => 'date',
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
		$lastLoginTimeAttr=  [
		'attribute' => 'last_login_time',
		 'value' => $model->getFaDateFromTimeStamp('last_login_time'),
		'displayOnly' => true,
		'widgetOptions' => [
			'pluginOptions' => ['format' => 'yyyy-mm-dd']
		],
		'inputWidth' => '40%'
	];
}
$attributes = [
	[
		'attribute' => 'name',
		'value' => $model->name,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
		[
		'attribute' => 'last_name',
		'value' => $model->last_name,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
	   
			[
		'attribute' => 'homepage',
		'value' => $model->homepage,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
			[
		'attribute' => 'tel',
		'value' => $model->tel,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
						[
		'attribute' => 'mobile',
		'value' => $model->mobile,
		'type' => DetailView::INPUT_TEXT,
		'inputWidth' => '40%',
	],
	   
		
			'email:email',
	   
   $createDateAttr,
   $modifiedDateAttr,
	$lastLoginTimeAttr
   
   

   
];

?>
<div id="result_view_msg">
</div>
<div id="user_view_info">
<?php
echo DetailView::widget([
	'model' => $model,
	'id' => 'detail_view_item_' . $model->id,
	'condensed' => true,
	//'hover' => true,
	'saveBottomType' => 'Bottom', /* this attribute Added by alizadeh to create buttom insted submit buttom  */
	'panel' => [
		'heading' =>Yii::t('app','Profile') ,
		'type' => DetailView::TYPE_INFO,
	],
	'buttons1' => '{update}',
	'buttons2' => '{view} {save}',
	'formOptions' => ['id' => 'form_' . $model->id, 'options' => array("accept-charset" => "UTF-8"), 'action' => ['/YumUsers/members/updateinfo'], 'enableClientValidation' => TRUE],
	'attributes' => $attributes,
	'saveOptions' => [ // your ajax delete parameters
		'label' => Html::tag('span', '<span id="edit" class="glyphicon glyphicon-floppy-disk"></span>', ['onclick' => "saveItem(" . $model->id . ")"]),
	],
	/* this attribute Added by alizadeh to create expanded buttom insted delete link
	 * problem : delete link ajax setting in kartik/detail view didn't work and 
	 * we coudn't control response.
	 * to coustom control in delete ajax response we add new BTN deleteexpand to  kartik\detail\DetailView.php class
	 * find change by search : 'deleteexpand
	 */
	'deleteexpandOptions' => [
		'label' => Html::tag('span', '<span id="edit" class="glyphicon glyphicon-trash"></span>', ['onclick' => "saveItem(" . $model->id . ")"]),
	],
]);


/* * ********************************************* */
?>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">


	$('.kv-btn-save').click(function () {

		formSTR = $(this).parents('form').attr('id');
		FormId = formSTR.replace('form_', '');
		saveItem(FormId);
	});

//	$(document).delegate(".kv-btn-save", 'click', function (e) {
//		 formSTR = $(this).parents('form').attr('id');
//		FormId = formSTR.replace('form_', '');
//		saveItem(FormId);
////		itemsuccessmsg = "<?php echo Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User Item')]) ?>"
//		itemfailmsg = "<?php echo Yii::t('app', 'Failed To Edit Items To {item}.', ['item' => Yii::t('app', 'User Item')]) ?>"
////
////		saveChildItem(FormId, itemsuccessmsg, itemfailmsg);
//	});
	

	
   function saveItem(id)
	{
		var _form = $('#form_' + id);
		var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]) ?>"
		var _failmsg = "<?php echo Yii::t('app', 'Failed To Edit Items To {item}.', ['item' => Yii::t('app', 'User')]) ?>"


		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: _form.serialize(),
			dataType: "JSON",
			success: function (data) {

				/**success save**/
				if ((typeof data.errormessage === 'undefined'))
				{
					/*Remove loade class*/
					$('.kv-detail-view').removeClass('kv-detail-loading');
					/*Refresh detail gridview by ajax response data */
					$("#user_view_info").html(data.detailviewcontent);

					/*provide success Message */
					$("#result_view_msg" ).html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\"> " + data.message + " </a></div>");


				
				} else  /***********fail save *****************/
				{
					/*Remove loade class*/
					$('.kv-detail-view').removeClass('kv-detail-loading');
					/*Refresh detail gridview by ajax response data */
					$("#user_view_info").html(data.detailviewcontent);

					/*provide success Message */
					$("#result_view_msg").html("<div class=\"alert alert-danger\" role=\"alert\">" + data.errormessage + " </div>");


					$("#result_view_msg" ).fadeIn(1300, function () {
//					   // $.pjax.reload({container: "#Experiences"});
					});
				}



			},
			error: function (data) {

				alert(_failmsg);

			}
		});


	}


</script>






	<?php /* DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'username',
			'name',
			'last_name',
			'office',
			'gender',
			'age',
			'grade',
			'edu',
			'univ',
			'homepage',
			'auth_key',
			'password_hash',
			'password_reset_token',
			'email:email',
			'tel',
			'mobile',
			'status',
			'created_at',
			'updated_at',
			'login_attemp_count',
			'last_login_time',
			'last_login_attemp_time',
			'lock',
			'valid',
		],
	])*/ ?>

