<?php

use yii\helpers\Html;
use kartik\tabs\TabsX;


/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;

	$items = [
		[
			'label'=>'<i class="glyphicon glyphicon-home"></i> Home',
			'content'=>$this->render('_change_password_user_form', [
		'model' => $model,'loginModel' => $loginModel,
	]) ,
			//'active'=>true
		],
		[
			'label'=>'<i class="glyphicon glyphicon-user"></i> Profile',
			'content'=>$content2,
			'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/site/tabs-data'])]
		],
		[
			'label'=>'<i class="glyphicon glyphicon-list-alt"></i> Dropdown',
			'items'=>[
				 [
					 'label'=>'<i class="glyphicon glyphicon-chevron-right"></i> Option 1',
					 'encode'=>false,
					 'content'=>$content3,
				 ],
				 [
					 'label'=>'<i class="glyphicon glyphicon-chevron-right"></i> Option 2',
					 'encode'=>false,
					 'content'=>$content4,
				 ],
			],
		],
		[
			'label'=>'<i class="glyphicon glyphicon-king"></i> Disabled',
			'headerOptions' => ['class'=>'disabled']
		],
	];
echo TabsX::widget([
	'items'=>$items,
	'position'=>TabsX::POS_ABOVE,
	'encodeLabels'=>false
]);
	?>


<div class="user-create">


	<?php
	
//	echo $this->render('_change_password_user_form', [
//		'model' => $model,'loginModel' => $loginModel,
//	]) ;
			?>

</div>
