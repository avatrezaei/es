<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;


 
/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >
	
</div>


 
<div >

		   <?php $form = ActiveForm::begin(['options' => ['id'=>'form_change_pass' ]]); ?>
  	<?php echo $form->errorSummary($model);  ?>

	<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword')->passwordInput(['maxlength' => 255]) ?>
			</div>
		<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword_repeat')->passwordInput(['maxlength' => 255]) ?>
			</div>

		 
	
	
 
	<div class="form-group">
		<input type="submit" name="submitBtn" class = "btn btn-sb" value="<?= Yii::t('app', 'Create')?>"id="submit_btn">
		<input type="button" name="submitBtn" class = "btn btn-sb" id="close-btn-1" value="<?= Yii::t('app', 'Close')?>"id="submit_btn">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>
</div>
