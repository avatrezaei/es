<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Add').' '.Yii::t('app', 'User'),
		'url' => ['create'],
		'options' => [
			'class' => 'btn btn-success'
		]
	]
];
?>

<div class="user-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-user font-dark"></i>
				<span class="caption-subject bold uppercase"><?= Yii::t('app', 'User') ?></span>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">
			<?= $widget->run() ?>
		</div>
	</div>
</div>