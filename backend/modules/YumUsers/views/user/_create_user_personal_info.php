<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\ArrayHelper;
use backend\models\City;
use app\models\Language;

use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use kartik\file\FileInput;
use yii\helpers\Url;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-4 col-sm-4 control-label';
$errorClass = Yii::$app->params ['errorClass'];



?>

<div class="tab-pane active form" id="tab_1_1">
  
	<?php //$form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]) ?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
	<?= $form->errorSummary([$model], ['class' => 'alert alert-danger']) ?>

	<div class="form-body">





		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">

				<?php
				echo $form->field(
					$model,
					'last_name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 100, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>













		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">

				<?php
				echo $form->field(
					$model,
					'tel',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">

				<?php
				echo $form->field(
					$model,
					'mobile',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>





		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?=
				$form->field(
					$model,
					'birthDate',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->widget(jalaliDatePicker::className (),[
						'options' => [
							'value'=>$model->getJBirthdate(),
							'format' => 'yyyy/mm/dd',
							'viewformat' => 'yyyy/mm/dd',
							'placement' => 'right',
							'todayBtn' => 'linked',
							'id' => 'user-birthDate',
							'class' => 'form-control'
						],

					])
					->label(NULL, ['class' => $labelClass])
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'fatherName',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>


			</div>
		</div>





		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'email',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'nationalId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 10, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>


			</div>
		</div>



		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">

				<?php
				echo $form->field(
					$model,
					'idNo',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 10, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>



			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				$dataSelectCity = ArrayHelper::map(City::find()->all(), 'id', 'name');
				echo $form->field(
					$model,
					'birthCityId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->widget(Select2::classname(), [
						'data' => $dataSelectCity,
						'theme' => Select2::THEME_BOOTSTRAP,
						'options' => [
							'placeholder' => Yii::t('app', 'Select ...'),
							'dir' => 'rtl',
						],
					])
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field($model, 'gender',
					['template'=>'{label}<div class="col-md-6"><div class="btn-group" data-toggle="buttons" id="user-gender">{input}</div> {hint} {error}</div>']
				)
					->radioList(
						$model->itemAlias('Gender'),
						[
							'unselect' => NULL,
							'tag' => false,
							'item' => function($index, $label, $name, $checked, $value) {
								return '<label class="btn btn-info '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value]) . $label . '</label>';
							}
						]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">


			</div>
		</div>


		<br>  

		<div class="col-md-6 col-sm-6"> 
 
		<?php 

		   echo $form->field($model, 'fileAttr')->widget(FileInput::classname(), [
				 'id' => 'file_input_user_pic1',
				 'language' => 'fa',
				 'options' => [
						'multiple' => false,
						'layoutTemplates' => 'modal',
						],
				 'pluginOptions' => [
				 'showUpload' => false,
				 'maxFileCount' => 1
				]
			])->label(NULL, ['class'=>$labelClass]); ;
	   ?>

	   </div>  

		</div>
	</div>





	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn green','name'=>'update_prsonal_info_btn'])	?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>