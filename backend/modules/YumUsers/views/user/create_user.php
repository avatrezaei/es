<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app', 'Add User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;

$space='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
?>

					
					<!-- BEGIN PAGE HEADER-->
					<!-- BEGIN THEME PANEL -->
					<div class="theme-panel hidden-xs hidden-sm">
						
						
						
					</div>
					<!-- END THEME PANEL -->
				   
					<!-- BEGIN PAGE TITLE-->
					<h3 class="page-title"> <?php echo Yii::t('app','Add User')?>
					</h3>
					<!-- END PAGE TITLE-->
					<!-- END PAGE HEADER-->
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<!-- PORTLET MAIN -->
								<div class="portlet light profile-sidebar-portlet ">
									<!-- SIDEBAR USERPIC -->
									<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
										echo $model->getIconThumbnail(null,"img-responsive");
									 }
									 ?>			 
									</div>
									<!-- END SIDEBAR USERPIC -->
									<!-- SIDEBAR USER TITLE -->
									<div class="profile-usertitle" id="user_quick_info">
										<div class="profile-usertitle-name"> <?php // echo Html::encode($fieldObj->name)?> </div>
										<div class="profile-usertitle-job"> <?php //echo Yii::t('app','Team Name').$space.":".$space.Html::encode($teamObj->name)?>  </div>
										<div class="profile-usertitle-job"> <?php //echo Yii::t('app','Gender').$space.":".$space.Html::encode($teamObj->getGenderText())?>  </div>
									</div>
									<!-- END SIDEBAR USER TITLE -->

									<!-- SIDEBAR MENU -->
									<div class="profile-usermenu">
										<ul class="nav">
											<li>
												<a href="#">
													<i class="icon-info"></i> <?php echo Yii::t('app','Sport Resume')?></a>
											</li>
											<li class="active">
												<a href="#">
													<i class="icon-settings"></i>  <?php echo Yii::t('app','Medal of honor')?> </a>
											</li>
											<li>
												<a href="#">
													<i class="icon-info"></i> <?php echo Yii::t('app','Matches')?>  </a>
											</li>
										</ul>
									</div>
									<!-- END MENU -->
								</div>
								<!-- END PORTLET MAIN -->
								<!-- PORTLET MAIN -->
							  
								<!-- END PORTLET MAIN -->
							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">
								<div class="row">
									<div class="col-md-12">
										
										 <?= $message ?>
<div id="results_expand_msg" style="display: none " >
</div>										
										
										
										<div class="portlet light ">
										
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
														<!-- Render Personal info  form -->	
														<?php

														echo $this->render('_create_user_personal_info', [
															'model' => $model,
															'message' => $message,
															'userDetails'=>$userDetails,
														   // 'form'=>$form,


														])

														?>  
																										   <!-- END PERSONAL INFO FORM -->
												   
													
												  
												 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>
