<?php

use yii\widgets\DetailView;
use kartik\detail\DetailViewTiny;
use kartik\detail\tiny;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\AppLanguage;

//$this->title = Yii::t('app', 'Profile');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => '#'];
//$this->params['breadcrumbs'][] = $this->title;


$model->birthDate=$model->getFaDateFromTimeStamp('birthDate');

   $birthDateAttr = [
		'attribute' => 'birthDate',
		'value' => $model->birthDate,
	];
		
	$createDateAttr = [
		'attribute' => 'created_at',
		'value' => $model->getFaDateFromTimeStamp('created_at'),
		//'format' => 'date',
		'displayOnly' => true,
	];

	$modifiedDateAttr = [
		'attribute' => 'updated_at',
		'value' => $model->getFaDateFromTimeStamp('updated_at'),
		//'format' => 'date',
		'displayOnly' => true,
	];
	
	$lastLoginTimeAttr = [
		'attribute' => 'last_login_time',
		'value' => $model->getFaDateFromTimeStamp('last_login_time'),
		'displayOnly' => true,
	];

$attributes = [
	[
		'attribute' => 'sportNo',
		'displayOnly' => true,
	],	
	[
		'attribute' => 'name',
		'value' => $model->name,
	],
	[
		'attribute' => 'last_name',
		'value' => $model->last_name,
	],
	[
		'attribute' => 'nationalId',
		'value' => $model->nationalId,
	],
	[
		'attribute' => 'idNo',
		'value' => $model->idNo,
	],
	[
		'attribute' => 'fatherName',
		'value' => $model->fatherName,
	],   
	[
		'attribute' => 'tel',
		'value' => $model->tel,
	],
	[
		'attribute' => 'mobile',
		'value' => $model->mobile,
	],
	
	[
		'attribute' => 'gender',
		'format' => 'raw',
		'value' => $model->Sex,
	],
	[
		'attribute' => 'homepage',
		'value' => $model->homepage,
	],
	[
		'attribute' => 'creatorUserId',
		'displayOnly' => true,
		'value' => $model->getUserInfo('creatorUserId')
	],
	[
		'attribute' => 'email',
	],
	
	$birthDateAttr,
	[
		'attribute'=>'role',
		'value'=> $model->getUserRolesText(),// var_dump(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())),
		'displayOnly' => true,
	],
	
	$createDateAttr,
	$modifiedDateAttr,
	$lastLoginTimeAttr,
];

?>


<div class="user-info-view">
		<div class="profile-content">
			<div class="portlet light ">
				<?php
					echo DetailView::widget([
						'model' => $model,
						'template' => Yii::$app->params['detalView'],
						'options' => [
							'tag' => 'div'
						],
						'id' => 'detail_view_item_' . $model->id,
						'attributes' => $attributes,
					]);
				?>
			</div>
			<div class="portlet light ">
				<?php
					echo $this->render('_user_detail_info', [
						'model' => $model,
						'eventId'=>$eventId
					]);
				?>
			</div>
			<div class="portlet light ">
				<?php
					echo Yii::$app->runAction('/messages/user-archive', ['id' => $model->id]);
				?>
			</div>
		</div>
	</div>
</div>