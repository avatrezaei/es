<?php

use yii\widgets\DetailView;
use kartik\detail\DetailViewTiny;
use kartik\detail\tiny;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\AppLanguage;

$userDetailObj=$model->getUserDetailBaseOnEvent($eventId);
if($userDetailObj!==null): ?>
<?php
	echo DetailView::widget([
		'model' => $userDetailObj,
		'template' => Yii::$app->params['detalView'],
		'options' => [
		'tag' => 'div'],
		'id' => 'detail_view_item_' . $model->id,
		'attributes' => [
			[
				'attribute' => 'userPosition',
				'value' => Html::encode($userDetailObj->userPosition),
			],
			[
				'attribute' => 'address',
				'value' =>  Html::encode($userDetailObj->address),
			],
			[
				'attribute' => 'officeAddress',
				'value' =>  Html::encode($userDetailObj->officeAddress),
			],
			[
				'attribute' => 'bankAccountNo',
				'value' =>  Html::encode($userDetailObj->bankAccountNo),
			],
			[
				'attribute' => 'bankAcoountName',
				'value' =>  Html::encode($userDetailObj->bankAcoountName),
			],

		],
	]);
?>
<?php endif; ?>




