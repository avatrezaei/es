<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app','Update  University User Responsed');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'University Response Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$space='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		;

if(is_object($autorizedUniObj->university))
{
	$universityName=$autorizedUniObj->university->name;
}else
{
	$universityName='---';
}

?>

<style>
	.lowercase_force{
		text-transform: lowercase !important;
	}
</style>
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<!-- PORTLET MAIN -->
								<div class="portlet light profile-sidebar-portlet ">
									<!-- SIDEBAR USERPIC -->
									<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
									   echo $model->getIconThumbnail(null,"img-responsive");
									 }
									 ?>			 
									</div>
									<!-- END SIDEBAR USERPIC -->
									<!-- SIDEBAR USER TITLE -->
									<div class="profile-usertitle" id="user_quick_info">
										<div class="profile-usertitle-name"> <?php echo Yii::t('app','User Response').Html::encode($universityName)?> </div>
										<div class="profile-usertitle-name"> <?php echo Html::encode($model->FullName)?></div>
										<div class="profile-usertitle-job"><span class="mt-step-title  font-red-pink"><?php echo Yii::t('app','National Id')?> </span>:<?php echo Html::encode($model->nationalId)?> </div>
										<div class="profile-usertitle-job"><span class="mt-step-title lowercase_force font-red-pink"><?php echo Yii::t('app','Username')?> </span>:<span class=" lowercase_force "> <?php echo Html::encode($model->username)?> </span></div>
									</div>
									<!-- END SIDEBAR USER TITLE -->
									<!-- SIDEBAR BUTTONS -->
									<div class="profile-userbuttons">
										<?php  //echo Html::a($space.Yii::t('app','Management').$space, \yii\helpers\Url::to(['/YumUsers/university-resp/index','userId'=>$model->id],true),['class'=>"btn green btn-sm"]);?>
									</div>
									<!-- END SIDEBAR BUTTONS -->
									<!-- SIDEBAR MENU -->
									<div class="profile-usermenu">

									</div>
									<!-- END MENU -->
								</div>
								<!-- END PORTLET MAIN -->

							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">
								<div class="row">
									<div class="col-md-12">
										
										 <?= $message ?>
							<div id="results_expand_msg" style="display: none " >
							</div>										
										
										
										<div class="portlet light ">
											<div class="portlet-title tabbable-line">
												<div class="caption caption-md">
													<i class="icon-globe theme-font hide"></i>
													
													<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'University User Info')?></span>
												</div>

											</div>
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
														<!-- Render Personal info  form -->	
														<?php

														echo $this->render('_update_athlete_personal_info', [
															'model' => $model,
															'message' => $message,
															'autorizedUniObj'=>$autorizedUniObj,


														])

														?>  
																										   <!-- END PERSONAL INFO FORM -->
												   
													
												  
												 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>