<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use app\models\User;
use yii\widgets\ActiveForm;


/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;
use kartik\file\FileInput;
use kartik\switchinput\SwitchInput;/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Create  University User Responsed');
$this->params['breadcrumbs'][] = $this->title;
?>
 <div id="results" style="display: none" >
	
</div>

<div class="portlet box blue ">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-gift"></i> <?php echo Html::encode($this->title); ?></div>
								</div>
	
	
   <div class="portlet-body">
	<?= $message ?>
	<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'id' => 'create_user_resp','role'=>'form']]); ?>
	<?php echo $form->errorSummary($model); ?>
	   
	   
	   

			<div class="form-group row">
					<div class='col-md-6'>
					   <?=$form->field ( $model, 'name', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>{input}</div> {hint} {error}</div>','labelOptions' =>  [ 'class' => 'col-md-4 control-label' ] ] )->textInput ( [ 'maxlength' => true ] )?>
					</div>
					<div class='col-md-6'>
					  <?=$form->field ( $model, 'last_name', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>{input}</div> {hint} {error}</div>','labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->textInput ( [ 'maxlength' => true ] )?>
					</div>
			</div>	   

			<div class="form-group row">
					<div class='col-md-6'>
					  <?=$form->field ( $model, 'gender', [ 'template' => "{label}\n<div class='col-md-6'>{input}<span class='help-block'>{hint}</span></div>",'labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->radioList ( ['man'=>Yii::t('app','man'),'woman'=>Yii::t('app','woman')])?>
					</div>
					<div class='col-md-6'>
					  <?php
						echo $form->field ( $model, 'birthDate', [ 
										'template' => "{label}\n<div class='col-md-6'>{input}<span class='help-block'>{hint}</span></div>",
										'labelOptions' => [ 
														'class' => 'col-md-4 control-label' 
										] 
						] )->widget ( jDate\DatePicker::className (), [ 
										'id' => 'event-startEventDate',
										'options' => [ 
														'id' => 'event-startEventDate',
														'formatDate' => 'dd-m-yyyy',
														//'value' => Event::getJalaliTimeByTimeStamp ( $model->startEventDate ) 
										] 
						] );
					?>	
					</div>
			</div>		 
	   

		   
			<div class="form-group row">
			<div class='col-md-6'>
				<?=$form->field ( $model, 'tel', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-phone"></i></span>{input}</div> {hint} {error}</div>','labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->textInput ( [ 'maxlength' => true ] )?>
				</div>
			<div class='col-md-6'>
				<?=$form->field ( $model, 'mobile', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-mobile"></i></span>{input}</div> {hint} {error}</div>','labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->textInput ( [ 'maxlength' => true ] )?>
				</div>
			</div>   

		   <?php
				/*Create Default UserName for University Response base On University prefix in universijty table*/
			   if(isset($autorizedUniObj)&&is_object($autorizedUniObj))
			   {
				  if(isset($autorizedUniObj->university)&&is_object($autorizedUniObj->university))
				  {

					  $model->username=$autorizedUniObj->university->preffixName.'_user';

				  }
			   }
		   
		   ?>

			<div class="form-group row">
			<div class='col-md-6'>
				<?=$form->field ( $model, 'username', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>{input}</div> {hint} {error}</div>','labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->textInput ( [ 'maxlength' => 80 ] )?>
				</div>
			<div class='col-md-6'>
				<?=$form->field ( $model, 'email', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>{input}</div> {hint} {error}</div>','labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->textInput ( [ 'maxlength' => 80 ] )?>
				</div>
			</div>		  


			<div class="form-group row">
			<div class='col-md-6'>
				<?=$form->field ( $model, 'passwordInput', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>{input}</div> {hint} {error}</div>','labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->passwordInput(['maxlength' => 255])?>
				</div>
			<div class='col-md-6'>
				<?=$form->field ( $model, 'passwordInput_repeat', [ 'template' => '{label}<div class="col-md-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>{input}</div> {hint} {error}</div>','labelOptions' => [ 'class' => 'col-md-4 control-label' ] ] )->passwordInput(['maxlength' => 255])?>
				</div>
			</div>   


	   
 <div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<input class="btn green save-n-close-btn" type="submit"
						id="submit_btn_create_user_resp" value="<?= Yii::t('app', 'Create')?>" />
									  
			</div>
		</div>
	</div>	   
	   
	   
	   
	   

 
<?php ActiveForm::end(); ?>	   
								  
	</div>
 </div>

<script>
		$('#submit_btn_create_user_resp').click(function (e) {

			  e.preventDefault();
			  var _form = $(this).parents('form');
		   //	alert(_form.attr('action'));

		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: _form.serialize(),
			success: function (data) {
				if(data.forceReload !== undefined && data.forceReload){
				 
				$('#modal').find('#modalContent').html(data.content);
				$.pjax.reload({container:"#Users"});  //Reload GridView)
		}

			},
			error: function (data) {
			  //  alert('fail');

			}
		});


	});
	
	
	
	
	
	
	
	
	
   
		</script>