<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app',  'Create University User Responsed');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'University Response Management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if(is_object($autorizedUniObj->university))
{
	$universityName=$autorizedUniObj->university->name;
}else
{
	$universityName='---';
}

?>
<style>
	hr {
		display: block;
		height: 1px;
		border: 0;
		border-top: 1px solid #ccc;
		margin: 1em 0;
		padding: 0;
	}
</style>
	<div class="profile-sidebar">
		<!-- PORTLET MAIN -->
		<div class="portlet light profile-sidebar-portlet ">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
			 <?php
			 if(is_object($model))
			 {
			   echo $model->getIconThumbnail(null,"img-responsive");
			 }
			 ?>			 
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle" id="user_quick_info">
				<div class="profile-usertitle-name"> <?php echo Yii::t('app','User Response').Html::encode($universityName)?> </div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			
			
		</div>
		<!-- END PORTLET MAIN -->
		<!-- PORTLET MAIN -->
	
		<!-- END PORTLET MAIN -->
	</div>





<?php

$dontShowPass=($model->scenario=='updateExistUser')?true:false;

$ajaxURL = \yii\helpers\Url::to(['/YumUsers/user/check-national-id-exist']);
$this->registerJs(
	<<<SCRIPT
var defaultUsername=$("#universityrespuser-username").val();
var nationalidVal = $("#universityrespuser-nationalid").val();
	if(nationalidVal==''){
		$("input").attr("disabled","true");
		$("#create_athelete").find(':input(:disabled)').prop('disabled',true)
		$("#universityrespuser-nationalid").removeAttr('disabled');
		$('[name="_csrf"]').removeAttr('disabled');

	}


	$('body').on('keypress','#universityrespuser-nationalid',
		function(event){
			if (event.which == 13 || event.keyCode == 13) {
				nationalIdCheck();
                $("#universityrespuser-name").focus();
                document.getElementById("password_info").style.display = "block";
		        document.getElementById("user_name_info").style.display = "block";
		  		//disabe attribute to not client validations
				document.getElementById("universityrespuser-passwordinput").disabled = false;
				document.getElementById("universityrespuser-passwordinput_repeat").disabled = false;
				//document.getElementById("universityrespuser-username").disabled = false;
				
				return false;


			}
		});

	$('body').on('blur','#universityrespuser-nationalid',
		function(event){
			nationalIdCheck();
            $("#universityrespuser-name").focus();
      
            document.getElementById("password_info").style.display = "block";
		    document.getElementById("user_name_info").style.display = "block";
		  	//disabe attribute to not client validations
			document.getElementById("universityrespuser-passwordinput").disabled = false;
			document.getElementById("universityrespuser-passwordinput_repeat").disabled = false;
			//document.getElementById("universityrespuser-username").disabled = false;
					return false;

		});
    if('$dontShowPass')
	{
             document.getElementById("password_info").style.display = "none";
			 document.getElementById("user_name_info").style.display = "none";
			 //disabe attribute to not client validations
			 document.getElementById("universityrespuser-passwordinput").disabled = true;
			 document.getElementById("universityrespuser-passwordinput_repeat").disabled = true;
	}
	function nationalIdCheck()
	{


		$("input").removeAttr('disabled');
		$("#universityrespuser-nationalid").removeAttr('disabled');

		var nationalidValue = $("#universityrespuser-nationalid").val();

		jQuery.ajax({
			type:'GET',
			url:'$ajaxURL',
			data: ({nationalId: nationalidValue}),
			cache:false,
			success:function(response){
				var parsed = JSON.parse(response);



				if(parsed.exist==true)
				{
					$("input").removeAttr('disabled');
					$("#create_athelete").find(':input(:disabled)').prop('disabled',false);
					$('option').attr('selected', false);
					
					
					document.getElementById("password_info").style.display = "none";
					document.getElementById("user_name_info").style.display = "none";
					//disabe attribute to not client validations
				    document.getElementById("universityrespuser-passwordinput").disabled = true;
					document.getElementById("universityrespuser-passwordinput_repeat").disabled = true;
					//document.getElementById("universityrespuser-username").disabled = true;


					$("#universityrespuser-name").val(parsed.person.name);
					$("#universityrespuser-last_name").val(parsed.person.last_name);
					$("#universityrespuser-tel").val(parsed.person.tel);
					$("#universityrespuser-idno").val(parsed.person.idNo);
					$("#universityrespuser-mobile").val(parsed.person.mobile);
					$("#universityrespuser-email").val(parsed.person.email);
					$("#universityrespuser-birthdate").val(parsed.person.birthdate);
					$("#universityrespuser-fathername").val(parsed.person.fatherName);
					$('#universityrespuser-caravanid').val(parsed.person.caravanId).trigger('change');
					$('#universityrespuser-teamid').val(parsed.person.teamId).trigger('change');
					$('#universityrespuser-birthcityid').val(parsed.person.birthCityId).trigger('change');

					$("#file_input_assurancecart").removeAttr('disabled');
					$("#file_input_stucart").removeAttr('disabled');

		

					$("#universityrespuser-name").focus();

				}else
				{
					$("input").removeAttr('disabled');
					$("#create_athelete").find(':input(:disabled)').prop('disabled',false);
					$('option').attr('selected', false);

					$("input[type=text]").val('');
					//set default username again after reset all input values
					$("#universityrespuser-username").val(defaultUsername);
					$("#universityrespuser-nationalid").val(nationalidValue);
				}
			},
		});
		return false;
	}
SCRIPT
);

?>

	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				
				<?= $message ?>
				<div id="results_expand_msg" style="display: none " ></div>										
				
				
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'University User Info')?></span>
						</div>

					</div>
					<div class="portlet-body">
						<div class="tab-content">
								<?php
									echo $this->render('_create_athlete_personal_info', [
										'model' => $model,
										'message' => $message,
										'autorizedUniObj'=>$autorizedUniObj,
									]);
								?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>