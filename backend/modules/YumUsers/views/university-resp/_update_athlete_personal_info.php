<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\City;
use app\models\Language;
use  yii\web\View;

use kartik\switchinput\SwitchInput;
use kartik\file\FileInput;
use yii\helpers\Url;

 use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;



$formGroupClass = "form-group";
$template = '{label}{input} {hint} {error}';
$inputClass = "form-control";
$labelClass ="control-label";
$errorClass = Yii::$app->params['errorClass'];




?>



<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#update_user").on("pjax:end", function() {
		
$("#user_quick_info").load(document.URL +  " #user_quick_info");
	  
  //Reload GridView)
		});
	});'
);
?>

<div class="tab-pane active" id="tab_1_1">
	<?php yii\widgets\Pjax::begin(['id' => 'update_user']) ?>

	<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>
	<?php
		
		//if(isset($_POST['update_prsonal_info_btn']))
		echo $form->errorSummary([$model], ['class' => 'alert alert-danger']);
		
		?>

	

		<?php
			echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>	  
	
	
			<?php
			echo $form->field(
					$model,
					'last_name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 100, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>

	
	
		<?php
				/*EventAdmin User just Update username before user login */

				if(!$model->isUserLoginEver()){
					
			echo $form->field(
					$model,
					'username',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 20, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
				}				
		?>

	  
			
		  
			   <?php
			echo $form->field(
					$model,
					'email',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>  
			
	
			   <?php
			echo $form->field(
					$model,
					'passwordInput',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->passwordInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 

	
			   <?php
			echo $form->field(
					$model,
					'passwordInput_repeat',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->passwordInput(
					['maxlength' => 255, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?> 

  
	
		<?php
			echo $form->field(
					$model,
					'tel',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>  
			
		<?php
			echo $form->field(
					$model,
					'mobile',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
				->textInput(
					['maxlength' => 40, 'class' => $inputClass]
				)
				->label(NULL, ['class'=>$labelClass]);
		?>
	<?php
	$model->birthDate =$model->dateDisplayFormat();
	?>

		   <?=$form->field ( $model, 'birthDate', [ 'options' => [ 'class' => $formGroupClass ],
											   'template' => $template
											   ] 
											   )->widget ( jalaliDatePicker::className (),
												[ 'options' => array ('format' => 'yyyy/mm/dd','viewformat' => 'yyyy/mm/dd','placement' => 'right','todayBtn' => 'linked' ,'id' => 'event-birthDate','class' => 'form-control'),
												 ] )->label ( NULL, [ 'class' => $labelClass ] )?>
	
			
	
	<?php
//		echo $form->field ( $model, 'gender', [
//				//'template' => '{label}<div class="col-md-6"><div class="btn-group" data-toggle="buttons">{input}</div> {hint} {error}</div>'
//		] )->radioList ( $model->itemAlias('Gender'), [
//				'tag' => false,
//				'item' => function ($index, $label, $name, $checked, $value) {
//					return '<label class=" ' . ($checked ? 'active' : '') . '">' . Html::radio ( $name, $checked, [
//							'value' => $value,
//							'autocomplete' => 'off'
//					] ) . $label . '</label>';
//				}
//		] )->label ( NULL, [
//				'class' => $labelClass
//		] );
		?>


	<?php

	echo $form->field(
		$model,
		'gender',
		[
			'options' => ['class' => $formGroupClass],
			//'template' => '<div class="col-md-6 col-sm-6"><div class="btn-group" data-toggle="buttons" id="athleteuser-gender">{input} </div> {hint} {error}</div>'
			'template' => '<div class="col-md-6 col-sm-6">{label}<div class="btn-group" data-toggle="buttons" id="universityrespuser-gender">{input} </div> {hint} {error}</div>'
		]
	)
		->radioList(
			$model->itemAlias('Gender'),
			[
				'tag' => false,
				'unselect' => NULL,
				'item' => function($index, $label, $name, $checked, $value) {
					return '<label class="btn green '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value, 'autocomplete'=>'off']) . $label . '</label>';
				}
			]
		)
		->label(NULL, ['class'=>$labelClass]);
	?>
	
	
		<div class="margiv-top-10">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn green','name'=>'update_prsonal_info_btn'])	?>
		<?php // echo Html::submitButton(Yii::t('app', 'Cancel') , ['class' => 'btn default'])	?>
		</div>

   <?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>	 
</div>