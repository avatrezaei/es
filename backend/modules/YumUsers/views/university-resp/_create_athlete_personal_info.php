<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\ArrayHelper;
use backend\models\City;
use app\models\Language;

use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use kartik\file\FileInput;
use yii\helpers\Url;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-4 col-sm-4 control-label';
$errorClass = Yii::$app->params ['errorClass'];

/*Create Default UserName for University Response base On University prefix in universijty table*/
if(isset($autorizedUniObj)&&is_object($autorizedUniObj))
{
  if(isset($autorizedUniObj->university)&&is_object($autorizedUniObj->university))
  {

	  $model->username=$autorizedUniObj->university->preffixName.'_user';

  }
}

?>

<div class="tab-pane active form" id="tab_1_1">
  
	<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]) ?>
	<?= $form->errorSummary([$model], ['class' => 'alert alert-danger']) ?>

	<div class="form-body">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">

				<?php


				echo $form->field(
					$model,
					'nationalId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 10, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass])
					->hint(Yii::t('app', 'Please Enter NationalId ,Then Press Enter'));
				?>
			</div>

		</div>


		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="caption">
				<hr>
			</div>
		</div>

		<div class="info_fields">

		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'last_name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 100, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>






		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'tel',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'mobile',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>

			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<?=
					$form->field(
						$model,
						'birthDate',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
						->widget(jalaliDatePicker::className (),[
							'options' => [
								'value'=>$model->getJBirthdate(),
								'format' => 'yyyy/mm/dd',
								'viewformat' => 'yyyy/mm/dd',
								'placement' => 'right',
								'todayBtn' => 'linked',
								'id' => 'event-birthDate',
								'class' => 'form-control'
							],

						])
						->label(NULL, ['class' => $labelClass])
					?>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<?php
					echo $form->field(
						$model,
						'email',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
						->textInput(
							['maxlength' => 255, 'class' => $inputClass]
						)
						->label(NULL, ['class'=>$labelClass]);
					?>
				</div>
			</div>





			<div class="row" id="password_info">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<?php
					 $r=$form->field(
						$model,
						'passwordInput',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
						->passwordInput(
							['maxlength' => 255, 'class' => $inputClass]
						)
						->label(NULL, ['class'=>$labelClass]);
					echo $r;
					?>

					<?php
					//$activeField = $form->field($model, 'passwordInput');
					$r->enableClientValidation=false;
					?>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<?php
					echo $form->field(
						$model,
						'passwordInput_repeat',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
						->passwordInput(
							['maxlength' => 255, 'class' => $inputClass]
						)
						->label(NULL, ['class'=>$labelClass]);
					?>
				</div>
			</div>









			<div class="row">




				<div class="col-md-6 col-sm-6 col-xs-12" id="user_name_info" >
					<?php
					echo $form->field(
						$model,
						'username',
						[
							'options' => ['class' => $formGroupClass],
							'template' => $template
						]
					)
						->textInput(
							['maxlength' => 20, 'class' => $inputClass]
						)
						->label(NULL, ['class'=>$labelClass]);
					?>

				</div>







				<div class="col-md-6 col-sm-6 col-xs-12">



				<?php
				echo $form->field($model, 'gender',
					['template'=>'{label}<div class="col-md-6"><div class="btn-group" data-toggle="buttons" id="universityrespuser-gender">{input}</div> {hint} {error}</div>']
				)
					->radioList(
						$model->itemAlias('Gender'),
						[
							'unselect' => NULL,
							'tag' => false,
							'item' => function($index, $label, $name, $checked, $value) {
								return '<label class="btn btn-info '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value]) . $label . '</label>';
							}
						]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

			</div>
		</div>


		

	</div>

	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn green','name'=>'update_prsonal_info_btn'])	?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>