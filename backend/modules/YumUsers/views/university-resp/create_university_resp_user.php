<?php

use yii\helpers\Html;
use backend\assets\UserProfile;





UserProfile::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app', 'Add Athelete');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$space='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
?>

					
					<!-- BEGIN PAGE HEADER-->
					<!-- BEGIN THEME PANEL -->
					<div class="theme-panel hidden-xs hidden-sm">
						
						
						
					</div>
					<!-- END THEME PANEL -->
					<!-- BEGIN PAGE BAR -->
					<div class="page-bar">
						<ul class="page-breadcrumb">
							<li>
								<a href="#"><?php echo Yii::t('app','Dashboard')?></a>
								<i class="fa fa-circle"></i>
							</li>
							<li>
								<span><?php echo Yii::t('app','Add Athelete')?></span>
							</li>
						</ul>
				
					</div>
					<!-- END PAGE BAR -->
					<!-- BEGIN PAGE TITLE-->
					<h3 class="page-title"> <?php echo Yii::t('app','Add Athelete')?>
					</h3>
					<!-- END PAGE TITLE-->
					<!-- END PAGE HEADER-->
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<!-- PORTLET MAIN -->
								<div class="portlet light profile-sidebar-portlet ">
									<!-- SIDEBAR USERPIC -->
									<div class="profile-userpic">
									 <?php
									 if(is_object($model))
									 {
									   echo $fieldObj->getIconThumbnail("img-responsive",'');
									 }
									 ?>			 
									</div>
									<!-- END SIDEBAR USERPIC -->
									<!-- SIDEBAR USER TITLE -->
									<div class="profile-usertitle" id="user_quick_info">
										<div class="profile-usertitle-name"> <?php echo Html::encode($fieldObj->name)?> </div>
										<div class="profile-usertitle-job"> <?php echo Yii::t('app','Team Name').$space.":".$space.Html::encode($teamObj->name)?>  </div>
										<div class="profile-usertitle-job"> <?php echo Yii::t('app','Gender').$space.":".$space.Html::encode($teamObj->getGenderText())?>  </div>
									</div>
									<!-- END SIDEBAR USER TITLE -->
									<!-- SIDEBAR BUTTONS -->
									<div class="profile-userbuttons">
										<?php  echo Html::a($space.Yii::t('app','Edit').$space, \yii\helpers\Url::to(['carvan-register/update-athelete','userId'=>$model->id],true),['class'=>"btn green btn-sm"]);?>
										<?php echo Html::a($space.Yii::t('app','Delete').$space, \yii\helpers\Url::to(['carvan-register/main-register','userId'=>$model->id],true),['class'=>"btn red btn-sm"]);?>
									</div>
									<!-- END SIDEBAR BUTTONS -->
									<!-- SIDEBAR MENU -->
									<div class="profile-usermenu">
										<ul class="nav">
											<li>
												<a href="#">
													<i class="icon-info"></i> <?php echo Yii::t('app','Sport Resume')?></a>
											</li>
											<li class="active">
												<a href="#">
													<i class="icon-settings"></i>  <?php echo Yii::t('app','Medal of honor')?> </a>
											</li>
											<li>
												<a href="#">
													<i class="icon-info"></i> <?php echo Yii::t('app','Matches')?>  </a>
											</li>
										</ul>
									</div>
									<!-- END MENU -->
								</div>
								<!-- END PORTLET MAIN -->
								<!-- PORTLET MAIN -->
								<div class="portlet light ">
									<!-- STAT -->
									<div class="row list-separated profile-stat">
										<div class="col-md-4 col-sm-4 col-xs-6">
											<div class="uppercase profile-stat-title"> 37 </div>
											<div class="uppercase profile-stat-text"> <?php echo Yii::t('app','Medal of honor')?></div>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-6">
											<div class="uppercase profile-stat-title"> 51 </div>
											<div class="uppercase profile-stat-text"><?php echo Yii::t('app','Matches')?> </div>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-6">
											<div class="uppercase profile-stat-title"> 2 </div>
											<div class="uppercase profile-stat-text"> <?php echo Yii::t('app','Cart')?> </div>
										</div>
									</div>
									<!-- END STAT -->
									<div>
										<h4 class="profile-desc-title"><?php echo Yii::t('app', 'About User')?></h4>
										<span class="profile-desc-text"> <?php echo Yii::t('app','Athelete Resume Default')?> </span>
										<div class="margin-top-20 profile-desc-link">
											<i class="fa fa-globe"></i>
											<a href="http://www.keenthemes.com">www.keenthemes.com</a>
										</div>
										<div class="margin-top-20 profile-desc-link">
											<i class="fa fa-twitter"></i>
											<a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
										</div>
									 
									</div>
								</div>
								<!-- END PORTLET MAIN -->
							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">
								<div class="row">
									<div class="col-md-12">
										
										 <?= $massage ?>
<div id="results_expand_msg" style="display: none " >
</div>										
										
										
										<div class="portlet light ">
											<div class="portlet-title tabbable-line">
												<div class="caption caption-md">
													<i class="icon-globe theme-font hide"></i>
													
													<span class="caption-subject font-blue-madison bold uppercase"><?php echo Yii::t('app', 'Athelete Info')?></span>
												</div>

											</div>
											<div class="portlet-body">
												<div class="tab-content">
													<!-- PERSONAL INFO TAB -->
														<!-- Render Personal info  form -->	
														<?php

														echo $this->render('_create_athlete_personal_info', [
															'model' => $model,
															'massage' => $massage,
															'userDetails'=>$userDetails,
														   // 'form'=>$form,


														])

														?>  
																										   <!-- END PERSONAL INFO FORM -->
												   
													
												  
												 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>
