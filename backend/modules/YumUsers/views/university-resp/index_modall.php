<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'نمایندگان دانشگاه';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
/**-------start Modal----------*/

yii\bootstrap\Modal::begin([
	'header' => '<span id="modalHeaderTitle">'. Yii::t('app', 'Change Password').'</span>',
	'headerOptions' => ['id' => 'modalHeader'],
	'id' => 'modal',
	'size' => 'modal-lg',
	//keeps from closing modal with esc key or by clicking out of the modal.
	// user must click cancel or X to close
	'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo '<div id="modalContent"><div style="text-align:center"><img src="'.Url::to('/images/modal-loader.gif').'"></div></div>';
yii\bootstrap\Modal::end();

/**-------end Modal----------*/

?>




<!--<h1><?= Html::encode($this->title) ?></h1>-->

	<?php
	/* ------------show flash messages----------------- */
	$session = Yii::$app->session;
   // check the availability
	$result = $session->hasFlash('change_password_success');
   // get and display the message
	echo $session->getFlash('change_password_success');
	?>
<?php
$gridColumns = [
	['class' => 'yii\grid\SerialColumn'],


	[
	'label' => Yii::t('app','University'),
	'attribute' => 'university',
	'value' => 'university.name',
	],
	[
	'label' => Yii::t('app','University User'),
	  'value' => function ($model) {
			  $userName=$model->getUniversityRespUserInfo($model->universityId,$model->eventId);
//							  echo'<pre>';
//			   print_r(var_dump($model));
//							   echo'</pre>';
//die();
			  return Html::encode($userName);

		},		
		
	],   
	[
	'label' => Yii::t('app','Last Login Date'),
	  'value' => function ($model) {
			  $date=$model->getUserRespLastLogintime();
			  return Html::encode($date);
		},		
		
	],	
	  [
		'class' => 'kartik\grid\ActionColumn',
		'header' => '',
		'mergeHeader' => false,
		'vAlign' => 'middle',
	  // 'template' =>  ($model->hasUniversityRespUser?'{delete}{createUserResp}{updateUserResp}{setActiveUserResp}{sendEmail}':'{createUserResp}'),
	   'template' =>  '{createUserResp}{updateUserResp}{delete}{setActiveUserResp}{sendEmail}',

		
		'buttons' => [

			'delete' => function ($url, $model,$data) {
				$deleteLink= \yii\helpers\Html::tag('span', '<span class="glyphicon glyphicon-trash"></span>', ['onclick' => "deleteItem(" . $model->id . ")"]);
				return ($model->hasUniversityRespUser()?$deleteLink:'');  
			},

			'createUserResp' => function ($url, $model,$data) { 

				$createButtom= Html::button('<span class="glyphicon glyphicon-plus"></span>', ['value' => Url::to(['/YumUsers/university-resp/create-user-resp','autorizedUniId'=>$model->id]), 'title' => Yii::t('app',''), 'class' => 'showModalButton btn btn-success','style'=>'padding:1px;']);
				return (!$model->hasUniversityRespUser()?$createButtom:'');
			},

			'updateUserResp' => function ($url, $model) { 
				$updateBotton= Html::button('<span class="glyphicon glyphicon-pencil"></span>', ['value' => Url::to(['/YumUsers/university-resp/update-user-resp','autorizedUniId'=>$model->id]), 'title' => Yii::t('app',''), 'class' => 'showModalButton btn btn-success','style'=>'padding:1px;']);
				 return ($model->hasUniversityRespUser()?$updateBotton:'');			

			},   

//			'setActiveUserResp' => function ($url, $model) {
//			   return '';
//				return \yii\helpers\Html::tag('span', '<span class="glyphicon glyphicon-off"></span>', ['onclick' => "setActiveItem(" . $model->id . ")"]);
//
//			},   
					
			'setActiveUserResp' => function ($url, $model) 
			{
				if($model->hasUniversityRespUser())
				{
					$userObj=$model->getUniversityRespUserObj();
					if($userObj->active)
					{
						return \yii\helpers\Html::tag('span', '<span class=" glyphicon glyphicon-ban-circle "></span>', ['onclick' => "setActiveItem(" . $model->id . ",0)"]);
					}else
					{
						return \yii\helpers\Html::tag('span', '<span class="glyphicon glyphicon-ok-circle"></span>', ['onclick' => "setActiveItem(" . $model->id . ",1)"]);
					}
				}else
				{
					return '';
				}

			},					

			'sendEmail' => function ($url, $model) { 
					 $sendEmailLink=Html::a('<span class="glyphicon  glyphicon-envelope"></span>',Url::to(['/YumUsers/university-resp/update-user-resp','id'=>$model->id]));
					 return ($model->hasUniversityRespUser()?$sendEmailLink:'');			

			},				 
		 ],

			],
		];
		?>



 
<?= Html::tag('p', '<i class="icon-social-dribbble font-purple"></i> <span style="color:purple">نام رویداد</span> :'.Html::encode($model->getEventName($eventId)), ['class' => 'username']) ?>
<!-- BEGIN GRID -->

		<div class="portlet-body">
			<div class="table-toolbar dataTables_wrapper">
				<div class="row">
					<div class="col-md-12">
						<div class="dt-buttons">
							<a class="dt-button buttons-print btn dark" tabindex="0" >
								<i class="fa fa-print"></i>
								<span>چاپ</span>
							</a>
							<a class="dt-button buttons-pdf buttons-html5 btn green" tabindex="0" >
								<i class="fa fa-file-pdf-o"></i>
								<span>PDF</span>
							</a>
							<a class="dt-button buttons-csv buttons-html5 btn purple" tabindex="0" >
								<i class="fa fa-file-excel-o"></i>
								<span>CSV</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="results_grid_msg" style="display: none" >

		</div>

		<div class="dataTables_wrapper no-footer">
			<div class="row">
				<div class="col-md-12 col-sm-12">
			<!-- Display grid -->
		
			<div class="data-index">

		<?php
		Pjax::begin(['id' => 'Users']);
		echo \kartik\grid\GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => $gridColumns,
			'summary' => '',
			'hover' => true,
			'panel' => [
				'type' => 'primary',
			],
			'toolbar' => [
				'{export}',
				'{toggleData}',
			'exportConfig' => [
					GridView::EXCEL => [
						'filename' => Yii::t('app', 'Users'),
						'showConfirmAlert' => false,
					],
					GridView::PDF => [
						'filename' => Yii::t('app', 'Users'),
						'showConfirmAlert' => false,
					]
				],
			],
				'toggleDataContainer' => ['class' => 'btn-group-sm'],
				'exportContainer' => ['class' => 'btn-group-sm', 'showConfirmAlert' => FALSE],
				'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
				'headerRowOptions' => ['class' => 'kartik-sheet-style portlet box purple'],
				//'filterRowOptions' => ['class' => 'kartik-sheet-style portlet box purple-plum'],
				'filterRowOptions' => ['class' => 'kartik-sheet-style portlet box '],
		]);
		Pjax::end();
		?>	  

		</div>
				</div>
			</div>
		</div>

		<script>

			
			function deleteItem(id)
			{
				var _url = "<?php echo Url::to([ '/YumUsers/university-resp/delete']) ?>" + "&autorizedUniId=" + id;
				var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'User')]) ?>"
				var _failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')]) ?>"

				bootbox.confirm({
					buttons: {
						confirm: {
							label: '<?= Yii::t('yii', 'Yes'); ?>',
							className: 'confirm-button-class'
						},
						cancel: {
							label: '<?= Yii::t('yii', 'No'); ?>',
							className: 'cancel-button-class'
						}
					},
					message: '<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>',
					callback: function (result) {
						if (result) 
						{
							$.ajax({
								type: 'POST',
								cache: false,
								url: _url,
								success: function (response) {
									
								  if(response.success)
									{
										$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"> " + response.messages + " </div>");
										$("#results_grid_msg").show("slow");
										$('#results_grid_msg').delay(2000).fadeOut(1000);
										$.pjax.reload({container: '#Users'});   
									}else
									{
									   $("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> " + response.messages + " </div>");
										$("#results_grid_msg").show("slow");
										$('#results_grid_msg').delay(2000).fadeOut(1000);
									}									
//									$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"> " + _successmsg + " </div>");
//									$("#results_grid_msg").show("slow");
//									$('#results_grid_msg').delay(2000).fadeOut(1000);
//									$.pjax.reload({container: '#Users'});

								},
								error: function (data) {
									$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> " + _failmsg + " </div>");
									$("#results_grid_msg").show("slow");
									$('#results_grid_msg').delay(3000).fadeOut(1500);
								}
							});
						}
						
					},
				});



		return false;


	}
	
	
	function setActiveItem(id,activevalue)
	{

	   if(activevalue==1)
	   {
		var _url = "<?php echo Url::to(['/YumUsers/university-resp/set-active']) ?>" + "&autorizedUniId=" + id +"&value=1";
	   }else 
	   {
		var _url = "<?php echo Url::to(['/YumUsers/university-resp/set-active']) ?>" + "&autorizedUniId=" + id +"&value=0";
	   }
	   
	   
		var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Actived.', ['item' => Yii::t('app', 'User')]) ?>"
		var _failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'User')]) ?>"

			bootbox.confirm({
				buttons: {
					confirm: {
						label: '<?= Yii::t('yii', 'Yes'); ?>',
						className: 'confirm-button-class'
					},
					cancel: {
						label: '<?= Yii::t('yii', 'No'); ?>',
						className: 'cancel-button-class'
					}
				},
				message: '<?php echo Yii::t('app',  'Are you sure you want to DeActive this UserResponse?') ?>',
				callback: function (result) {
					if (result) 
					{
						$.ajax({
							type: 'POST',
							url: _url,
							dataType: "json",
							contentType: "application/json; charset=utf-8",
							success: function (response) {
								if(response.success)
								{
									$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"> " + response.messages + " </div>");
									$("#results_grid_msg").show("slow");
									$('#results_grid_msg').delay(2000).fadeOut(1000);
									$.pjax.reload({container: '#Users'});   
								}else
								{
								   $("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> " + response.messages + " </div>");
									$("#results_grid_msg").show("slow");
									$('#results_grid_msg').delay(2000).fadeOut(1000);
								}
							},
							error: function (data) {
								$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> " + _failmsg + " </div>");
								$("#results_grid_msg").show("slow");
								$('#results_grid_msg').delay(3000).fadeOut(1500);
							}
						});
					}

				},
			});



		return false;


	}

</script>
