<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use  yii\web\View;

use kartik\file\FileInput;
use yii\helpers\Url;

?>

<div class="tab-pane" id="tab_1_2">

    <p><?php echo Yii::t('app', 'In this page, you can change your profile picture.')
            .Yii::t('app', 'Keep in mind that allowed file formats include ').': '.implode(',',$model->fileExtentionsArr)
            .Yii::t('app','Maximum file size is:  {size}',['size'=>$model->fileSizeKB.' '.Yii::t('app','KB')]);
        ?>
    </p>
														
	<?php $uploadForm = ActiveForm::begin(['options' => ['id'=>'form_change_pic','data-pjax' => true,  'role'=>'form','enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]); ?>
	<?php echo $uploadForm->errorSummary([$model], ['class' => 'alert alert-danger']);  ?>
														
        <div class="row" style="padding-right:30px;">											
            <div class="form-group"> 
                <div class="col-md-8"> 
                    <?php
                    echo $uploadForm->field($model, 'fileAttr')->widget(FileInput::classname(), [
                        'id' => 'file_input_user_pic',
                        'language' => 'fa',
                        'options' => [
                            'multiple' => true,
                            'layoutTemplates' => 'modal',
                        ],
                        'pluginOptions' => [
                            'showUpload' => false,
                            'maxFileCount' => 1,
                            'msgInvalidFileExtension'=>Yii::t('app','Invalid extension for file . Only "{extensions}" files are supported.'),
                            'allowedFileExtensions'=>$model->fileExtentionsArr,
                            'msgSizeTooLarge'=>Yii::t('app','File  ({size} KB) exceeds maximum allowed upload size of {maxSize} KB. Please retry your upload!'),
                            'maxFileSize'=>$model->fileSizeKB,
                            'previewFileType' => 'image',
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
		<div class="margiv-top-10">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['name'=>'update_pic_btn','class' => 'btn green'])	?>
		<?php // echo Html::submitButton(Yii::t('app', 'Cancel') , ['class' => 'btn default'])	?>
		</div>
        </div>
   <?php ActiveForm::end(); ?>
	
</div>

<!-- 
<script>
	 function savePicture()
	{

		var _form = $('#form_change_pic');
		var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]) ?>"
		var _failmsg = "<?php echo Yii::t('app', 'Failed To Edit Items To {item}.', ['item' => Yii::t('app', 'File')]) ?>"

		var formData = new FormData($('#form_change_pic')[0]);
		formData.append('file', $('input[type=file]')[0].files[0]);

		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: formData,
			processData: false,
			contentType: false,
			dataType: "JSON",
			success: function (data) {

				/**success save**/
				if ((typeof data.success === true))
				{

		   
					/*provide success Message */
					$("#results_expand_msg" ).html("<div class=\"alert alert-success\" role=\"alert\"> " + _successmsg + " </div>");

					/*
					 *1-fade success message
					 *2-close detailview div slidly
					 *3-reload gridview by pjax 
					 */
					$("#results_expand_msg").fadeIn(3500, function () {
					$("#results_expand_msg").fadeOut( 4000 );
					});

				} else  /***********fail save *****************/
				{
			   
					/*provide success Message */
					$("#results_expand_msg" ).html("<div class=\"alert alert-danger\" role=\"alert\"> " + data.errormessage + " </div>");


					$("#results_expand_msg" ).fadeIn(1300, function () {
//					   // $.pjax.reload({container: "#Projects"});
					});
				}



			},
			error: function (data) {

				alert(_failmsg);

			}
		});


	}
	</script> -->

<!--														<form action="#" role="form">
															<div class="form-group">
																<div class="fileinput fileinput-new" data-provides="fileinput">
																	<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																		<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
																	<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
																	<div>
																		<span class="btn default btn-file">
																			<span class="fileinput-new"> Select image </span>
																			<span class="fileinput-exists"> Change </span>
																			<input type="file" name="..."> </span>
																		<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
																	</div>
																</div>
																<div class="clearfix margin-top-10">
																	<span class="label label-danger">NOTE! </span>
																	<span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
																</div>
															</div>
															<div class="margin-top-10">
																<a href="javascript:;" class="btn green"> Submit </a>
																<a href="javascript:;" class="btn default"> Cancel </a>
															</div>
														</form>-->