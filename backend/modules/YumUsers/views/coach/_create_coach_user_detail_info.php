<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;
use backend\models\Fields;

use kartik\file\FileInput;




$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-4 col-sm-4 control-label';
$errorClass = Yii::$app->params ['errorClass'];

?>
<style>
hr {
	display: block;
	height: 1px;
	border: 0;
	border-top: 1px solid #ccc;
	margin: 1em 0;
	padding: 0; 
}
</style>
<div class="col-md-12 col-sm-12 col-xs-12">
<br>
<br>

<div class="caption">
  <i class="icon-user font-green-sharp"></i>
  <span class="caption-subject font-green-sharp bold uppercase"><?php echo Yii::t('app','More Info')?></span>
 </div>
   <hr>

 </div>




<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">

		<?php
		$dataSelectEducationalGroups = ArrayHelper::map(EducationalGroups::find()->all(), 'EduGrpCode', 'PEduName');
		echo $form->field(
			$userDetails,
			'educationalField',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => $dataSelectEducationalGroups,
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Select ...'),
					'dir' => 'rtl',
				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">

		<?php
		$dataSelectField= ArrayHelper::map(Fields::find()->all(), 'id', 'name');
		echo $form->field(
			$userDetails,
			'sportFieldId',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => $dataSelectField,
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Select ...'),
					'dir' => 'rtl',
				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>

	</div>
</div>










 <div class="col-md-12 col-sm-12 col-xs-12">
<br>

<div class="caption">
  <i class="icon-doc font-green-sharp"></i>
  <span class="caption-subject font-green-sharp bold uppercase"><?php echo Yii::t('app','Upload File')?></span>
 </div>
   <hr>

 </div>











