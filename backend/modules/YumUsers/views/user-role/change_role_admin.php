<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding"><?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Change Role');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="resultmsg"></div>
<div >
	<?php yii\widgets\Pjax::begin(['id' => 'new_experiences']) ?>
	<?php // $massage ?>
		   <?php $form = ActiveForm::begin(['options' => ['id'=>'form_change_pass' ]]);
		   //$form->inline=1;
		   ?>
  	<?php echo $form->errorSummary($model);  ?>


		<div class="form-group col-xs-6"> 
			<?php 
			$roles=$model->getRolesAsListData();
			//set selected roles--if we set value of RoleInputArray this is display as checked in checkbox
			$model->RoleInputArray = $model->getUserRoles((int)$userTargetId);
			
//			   echo $form->field($model, 'RoleInputArray')
//				  ->inline(true)->checkboxList(
//					$model->getRolesAsListData(),		  
//					['labelOptions' => ['style' => "display: inline-block" ]]	// options
//				  );
				  echo $form->field($model, 'RoleInputArray')
				  ->checkboxList(
					$model->getRolesAsListData(),		  
					['labelOptions' => ['style' => "display: inline-block" ]]	// options
				  );

					?>
		   
			</div>
	 
	
  <div class="form-group col-xs-6"> 
		<?php
	   
				$model->targetUser =  (int)$userTargetId;// better put it on controller
$form->field($model, 'targetUser')->hiddenInput()->label(false);
				?>
		   
		</div>
		 
	
 
	<div class="form-group">
		<input type="submit" name="submitBtn" class="btn btn-sb" value="<?= Yii::t('app', 'Confirm')?> " id="btn_change_user_pass">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>

</div>

<br>
<hr>
<br>
<script>
		$('#btn_change_user_pass').click(function (e) {

			  e.preventDefault();
 var _form = $(this).parents('form');
		   //	alert(_form.attr('action'));

		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: _form.serialize(),
			success: function (data) {
				if(data.forceReload !== undefined && data.forceReload){
				 
				$('#modal').find('#modalContent').html(data.content);
					 
		}

			},
			error: function (data) {
			  //  alert('fail');

			}
		});


	});
	
	
	
	
	
	
	
	
	
   
		</script>