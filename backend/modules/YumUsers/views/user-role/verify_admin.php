<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding"><?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Verify Admin Password');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;



 if(!isset($userTargetId))
	{
		$userTargetId=$model->targetUser;
	}
?>
<div id="resultmsg"></div>
<div >
	<?php yii\widgets\Pjax::begin(['id' => 'new_experiences']) ?>
	<?php // $massage ?>
		   <?php $form = ActiveForm::begin(['options' => ['id'=>'form_change_pass' ]]); ?>
  	<?php echo $form->errorSummary($model);  ?>

	<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'passwordInput')->passwordInput(['maxlength' => 255]) ?>
			</div>
		<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword')->passwordInput(['maxlength' => 255,'autocomplete'=>'off']) ?>
			</div>
		<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword_repeat')->passwordInput(['maxlength' => 255,'autocomplete'=>'off']) ?>
			</div>
  <div class="form-group col-xs-6"> 
		<?php
				$model->targetUser =  (int)$userTargetId;// better put it on controller
$form->field($model, 'targetUser')->hiddenInput()->label(false);
				?>
		   
		</div>
		 
	
 
	<div class="form-group">
		<input type="submit" name="submitBtn" class="btn btn-sb" value="<?= Yii::t('app', 'Confirm')?> " id="btn_change_user_pass">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>

</div>

<script>
		$('#btn_change_user_pass').click(function (e) {

			  e.preventDefault();
 var _form = $(this).parents('form');
		   //	alert(_form.attr('action'));

		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: _form.serialize(),
			//dataType: "JSON",
			success: function (data) {
			  //  alert(4444444);
				if(data.forceReload !== undefined && data.forceReload){
				 // document.getElementById('modalHeaderTitle').innerHTML = '<h4>ppppppppppppppp</h4>';
//alert(222);
//$('#modal').modal('toggle');
				$('#modal').find('#modalContent').html(data.content);
					  // $('#modal').modal('hide');
					 //  $("#resultmsg" ).html("<div class=\"alert alert-success\" role=\"alert\"> " + data.message + " </div>");

						//$("#resultmsg").fadeIn(1300, function () {
			   
					   // });
					  // $('#resultmsg').innerHtml='44444444';
			//$.pjax.reload({container:'#new_experiences'});
		}
				/**success save**/
			   //  $('#modal').modal('hide');
			  // alert(data.content);
//				if (( data.forceReload ==true))
//				{
//
//					 $('#modal').modal('hide');
//			   alert(data.content);
//
//				} 
			},
			error: function (data) {
			  //  alert('fail');

			}
		});


	});
	
	
	
	
	
	
	
	
	
	
//		$(document).delegate("#btn-verify-pass", 'click', function (e) {
//		e.preventDefault();
// var _form = $(this).parents('form');
//alert(44444);
//
//		$.ajax({
//			type: _form.attr('method'),
//			url: _form.attr('action'),
//			data: _form.serialize(),
//			dataType: "JSON",
//			success: function (data) {
//				/**success save**/
//				if ((typeof data.status === 'failure'))
//				{
//					$('#dialogSignup div.divForForm').html(data.div);
//					
//						   $('#modal').find('#modalContent')
//					.html(data.div);
//			//dynamiclly set the header for the modal
//		   // document.getElementById('modalHeaderTitle').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
//	  
//				
//
//				} 
//			},
//			error: function (data) {
//				alert('fail');
//
//			}
//		});
//
//
//	});
		</script>