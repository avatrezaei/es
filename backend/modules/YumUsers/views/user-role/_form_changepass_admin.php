<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;


 
/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >

</div>

 
<div >
	<?php yii\widgets\Pjax::begin() ?>

			<?php  //echo $message ?>
		   <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true,'id'=>'form_change_pass' ]]); ?>
  	<?php echo $form->errorSummary($model);  ?>
	<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword')->passwordInput(['maxlength' => 255,'autocomplete'=>'off']) ?>
			</div>
		<div class="form-group col-xs-6"> 
		<?= $form->field($model, 'inputNewPassword_repeat')->passwordInput(['maxlength' => 255,'autocomplete'=>'off']) ?>
			</div>
  <div class="form-group col-xs-6"> 
		<?php
				$model->targetUser =  (int)$userTargetId;// better put it on controller
$form->field($model, 'targetUser')->hiddenInput()->label(false);
				?>
		   
		</div>
		 
	
	
 
	<div class="form-group">
		<input type="submit" name="submitBtn" class = "btn btn-sb" value="<?= Yii::t('app', 'Confirm')?>"id="submit_btn">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>
<?php  yii\widgets\Pjax::end() ?>

</div>
