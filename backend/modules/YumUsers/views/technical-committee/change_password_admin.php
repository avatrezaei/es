<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\YumUsers\models\User */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
	<?php
yii\bootstrap\Modal::begin([
	'header' => '<span id="modalHeaderTitle"></span>',
	'headerOptions' => ['id' => 'modalHeader'],
	'id' => 'modal',
	'size' => 'modal-lg',
	//keeps from closing modal with esc key or by clicking out of the modal.
	// user must click cancel or X to close
	'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo '<div id="modalContent"><div style="text-align:center"><img src="'.Url::to('/images/modal-loader.gif').'"></div></div>';
yii\bootstrap\Modal::end();
?>
	<h1><?= Html::encode($this->title) ?></h1>
	   <?= Html::button('Create New Company', ['value' => Url::to(['/YumUsers/user/create']), 'title' => 'Creating New Company', 'class' => 'showModalButton btn btn-success']); ?>

	<?= $this->render('_form_changepass_admin', [
		'model' => $model,
	]) ?>

</div>
