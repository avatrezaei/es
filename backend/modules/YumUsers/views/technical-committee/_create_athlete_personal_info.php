<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\ArrayHelper;
use backend\models\City;
use app\models\Language;

use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use kartik\file\FileInput;
use yii\helpers\Url;

use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-4 col-sm-4 control-label';
$errorClass = Yii::$app->params ['errorClass'];

$refereeTypeId=\backend\modules\YumUsers\models\UsersType::getUserTypeId('Referee');
$SupervisorOfRefereeTypeId=\backend\modules\YumUsers\models\UsersType::getUserTypeId('SupervisorOfReferee');

?>

<?php

$this->registerJs(
	<<<SCRIPT
	var userTypeValfirst = $("#technicalcommitteeuser-usertype").val();
	if(userTypeValfirst==$refereeTypeId || userTypeValfirst==$SupervisorOfRefereeTypeId )
	  $('.Referee_fields').show();
	else
	   $('.Referee_fields').hide();
	$('body').on('change','#technicalcommitteeuser-usertype',
		function(){

			var userTypeVal = $("#technicalcommitteeuser-usertype").val();
			if(userTypeVal==$refereeTypeId || userTypeVal==$SupervisorOfRefereeTypeId)
			{
			 $('.Referee_fields').show();
			 var div0= document.getElementById("usersdetails-refereedegree");
			 parentDiv1 = div0.parentNode;
			 parentDiv2 = parentDiv1.parentNode;
			 parentDiv2.className += " required";

			 var divfile0= document.getElementById("file_input_judgeCart");
			 divfile1 = divfile0.parentNode;
			 divfile2 = divfile1.parentNode;
			 divfile3 = divfile2.parentNode;
			 divfile4 = divfile3.parentNode;
			 divfile4.className += " required";

			}else
			{
			 $('.Referee_fields').hide();

			}
			return false;
	});
SCRIPT
	, View::POS_END);
?>



<style>
	hr {
		display: block;
		height: 1px;
		border: 0;
		border-top: 1px solid #ccc;
		margin: 1em 0;
		padding: 0;
	}


</style>

<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="caption">
		<i class="icon-user font-green-sharp"></i>
		<span class="caption-subject font-green-sharp bold uppercase"><?php echo Yii::t('app','Technical Committee Info')?></span>
		<hr>
	</div>
</div>


<div class="tab-pane active form" id="tab_1_1">

	<?php //$form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]) ?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'create_technicalcommitteeuser','class' => 'form-horizontal']]) ?>
	<div class="row">

	<?= $form->errorSummary([$model,$userDetails], ['class' => 'alert alert-danger']) ?>
	</div>

	<div class="form-body">

		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">

				<?php
				echo $form->field(
					$model,
					'nationalId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 10, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass])
					->hint(Yii::t('app', 'Please Enter NationalId ,Then Press Enter'));
				?>
			</div>

		</div>


		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="caption">
				<hr>
			</div>
		</div>



		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>



			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'last_name',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 100, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'tel',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'email',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 255, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>




		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?=
				$form->field(
					$model,
					'birthDate',
					[
						'options' => ['class' => $formGroupClass],
						//'template'=>'{label}<div class="col-md-6"><div class="btn-group" data-toggle="buttons">{input}</div> {hint} {error}</div>'

						'template' => $template
					]
				)
					->widget(jalaliDatePicker::className (),[
						'options' => [
							//'value'=>$model->getJBirthdate(),
							'format' => 'yyyy/mm/dd',
							'viewformat' => 'yyyy/mm/dd',
							'placement' => 'right',
							'todayBtn' => 'linked',
							'id' => 'event-birthDate',
							'class' => 'form-control'
						],

					])
					->label(NULL, ['class' => $labelClass])
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'mobile',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 40, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field(
					$model,
					'idNo',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->textInput(
						['maxlength' => 10, 'class' => $inputClass]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">

								<?php
								echo $form->field(
									$model,
									'fatherName',
									[
										'options' => ['class' => $formGroupClass],
										'template' => $template
									]
								)
									->textInput(
										['maxlength' => 255, 'class' => $inputClass]
									)
									->label(NULL, ['class'=>$labelClass]);
								?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php

				$couchTypeModels=\backend\modules\YumUsers\models\UsersType::getTechnicalCommitteeType();

				$couchType=ArrayHelper::map($couchTypeModels, 'id', 'title');

				echo $form->field(
					$model,
					'userType',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->widget(Select2::classname(), [
						'data' => $couchType,
						'theme' => Select2::THEME_BOOTSTRAP,
						'options' => [
							'placeholder' => Yii::t('app', 'Select ...'),
							'dir' => 'rtl',
						],
					])
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				$dataSelectCity = ArrayHelper::map(City::find()->all(), 'id', 'name');
				echo $form->field(
					$model,
					'birthCityId',
					[
						'options' => ['class' => $formGroupClass],
						'template' => $template
					]
				)
					->widget(Select2::classname(), [
						'data' => $dataSelectCity,
						'theme' => Select2::THEME_BOOTSTRAP,
						'options' => [
							'placeholder' => Yii::t('app', 'Select ...'),
							'dir' => 'rtl',
						],
					])
					->label(NULL, ['class'=>$labelClass]);
				?>
			</div>
		</div>


		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php
				echo $form->field($model, 'gender',
					['template'=>'{label}<div class="col-md-6"><div class="btn-group" data-toggle="buttons" id="user-gender">{input}</div> {hint} {error}</div>']
				)
					->radioList(
						$model->itemAlias('Gender'),
						[
							'unselect' => NULL,
							'tag' => false,
							'item' => function($index, $label, $name, $checked, $value) {
								return '<label class="btn btn-info '. ($checked ? 'active' : '') .'">' . Html::radio($name, $checked, ['value'  => $value]) . $label . '</label>';
							}
						]
					)
					->label(NULL, ['class'=>$labelClass]);
				?>

			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">

			</div>
		</div>




	   <?php

		echo $this->render('_create_athlete_user_detail_info', [
			'model' => $model,
			'message' => $message,
			'userDetails'=>$userDetails,
			'form'=>$form,
		])

		?>

		<br>
<div class="col-md-6 col-sm-6">

<?php
$fileHint='<i class="fa fa-check-square-o" aria-hidden="true"></i>'.Yii::t('app', '4 × 3 full-face photo that was taken this year.').'<br>';
$fileHint.='<i class="fa fa-check-square-o" aria-hidden="true"></i>'.Yii::t('app','The image must be clear, specific and ineffective stamp, punch and is dirt.').'<br>';
$fileHint.= '<i class="fa fa-check-square-o" aria-hidden="true"></i>'. Yii::t('app','Valid File Type is:').' : '.'('.'<span >'.implode(',',$model->fileExtentionsArr).'</span>'.')'.'&nbsp;&nbsp'.Yii::t('app','Max allowed File Size').' : '.'('.'<span >'.($model->fileSizeKB).'</span>'.' '.Yii::t('app','KB').')';

 echo $form->field($model, 'fileAttr')->widget(FileInput::classname(), [
	 'id' => 'file_input_user_pic1',
	 'language' => 'fa',
	 'options' => [
		 'multiple' => true,
		 'layoutTemplates' => 'modal',

	 ],
	 'pluginOptions' => [
		 'showUpload' => false,
		 'maxFileCount' => 1,
		 'msgInvalidFileExtension'=>Yii::t('app','Invalid extension for file . Only "{extensions}" files are supported.'),
		 'allowedFileExtensions'=>$model->fileExtentionsArr,
		 'msgSizeTooLarge'=>Yii::t('app','File  ({size} KB) exceeds maximum allowed upload size of {maxSize} KB. Please retry your upload!'),
		 'maxFileSize'=>$model->fileSizeKB,
		 'previewFileType' => 'image',
		 'showRemove'=>false,
	 ]

		 ])->label(NULL, ['class'=>$labelClass])->hint( Yii::t('app',$fileHint),['class'=>"help-block" ,'style'=>'padding-right:230px;']) ;
?>

	</div>





		</div>
	</div>





	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn green','name'=>'update_prsonal_info_btn'])	?>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>
