<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app','Technical Management');


$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => Yii::t('app', 'Add').' '.Yii::t('app', 'Technical Management'),
		'url' => ['create'],
		'options' => [
			'class' => 'btn btn-success'
		]
	]
];


$this->registerJs(
	<<<SCRIPT
	$('#technical-committee-user-type').on('keyup change', function() {
		table.columns(7).search($(this).find(':selected').val()).draw();
	});
	
	$('#technical-committee-user-field').on('keyup change', function() {
		table.columns(6).search($(this).find(':selected').val()).draw();
	});
	
SCRIPT
	, \yii\web\View::POS_END);
?>


<?php
	/* ------------show flash messages----------------- */
	$session = Yii::$app->session;
   // check the availability
	$result = $session->hasFlash('manage_team_member');
   // get and display the message
	echo $session->getFlash('manage_team_member');
	?>
<div class="user-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-user font-dark"></i>
				<span class="caption-subject bold uppercase"><?= Yii::t('app', 'Technical Management') ?></span>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">
			<?= $widget->run() ?>
		</div>
	</div>
</div>