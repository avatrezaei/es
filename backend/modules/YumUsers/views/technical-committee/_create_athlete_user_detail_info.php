<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Language;
use backend\models\Event;
use backend\modules\YumUsers\models\EducationalGroups;
use backend\modules\YumUsers\models\EducationalSections;
use  yii\web\View;
use backend\models\Fields;

use kartik\file\FileInput;




$formGroupClass = Yii::$app->params ['formGroupClass'];
$formGroupClassShowHide = Yii::$app->params ['formGroupClass']." "."Referee_fields";
$template = '{label}<div class="col-md-8 col-sm-8">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-4 col-sm-4 control-label';
$errorClass = Yii::$app->params ['errorClass'];

?>
<div class="col-md-12 col-sm-12 col-xs-12">
	<br>
	<br>

	<div class="caption">
		<i class="icon-user font-green-sharp"></i>
		<span class="caption-subject font-green-sharp bold uppercase"><?php echo Yii::t('app','More Info')?></span>
	</div>
	<hr>

</div>


<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
		echo $form->field(
			$userDetails,
			'address',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->textInput(
				['maxlength' => 150, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">

		<?php
		echo $form->field(
			$userDetails,
			'officeAddress',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->textInput(
				['maxlength' => 150, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
</div>





<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
		echo $form->field(
			$userDetails,
			'bankAcoountName',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->textInput(
				['maxlength' => 15, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">

		<?php
		echo $form->field(
			$userDetails,
			'bankAccountNo',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->textInput(
				['maxlength' => 30, 'class' => $inputClass]
			)->label(Yii::t('app','Bank Account No & prefer shaba'), ['class'=>$labelClass]);
		//	->label(NULL, ['class'=>$labelClass]);
		?>

	</div>
</div>

<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?php
		$dataSelectField= ArrayHelper::map(Fields::find()->all(), 'id', 'name');
		echo $form->field(
			$userDetails,
			'sportFieldId',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->widget(Select2::classname(), [
				'data' => $dataSelectField,
				'theme' => Select2::THEME_BOOTSTRAP,
				'options' => [
					'placeholder' => Yii::t('app', 'Select ...'),
					'dir' => 'rtl',
				],
			])
			->label(NULL, ['class'=>$labelClass]);
		?>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 Referee_fields">

		<?php
		echo $form->field(
			$userDetails,
			'refereeDegree',
			[
				'options' => ['class' => $formGroupClass],
				'template' => $template
			]
		)
			->textInput(
				['maxlength' => 15, 'class' => $inputClass]
			)
			->label(NULL, ['class'=>$labelClass]);
		?>


	</div>
</div>


<div class="col-md-12 col-sm-12 col-xs-12">
	<br>
	<br>

	<div class="caption">
		<i class="icon-doc font-green-sharp"></i>
		<span class="caption-subject font-green-sharp bold uppercase"><?php echo Yii::t('app','Upload File')?></span>
	</div>
	<hr>

</div>

<div class="col-md-6 col-sm-6 Referee_fields">

	<?php

	echo $form->field($userDetails, 'fileAttrjudgeCart')->widget(FileInput::classname(), [
		'id' => 'file_input_judgeCart',
		'language' => 'fa',
		'options' => [
			'multiple' => true,
			'layoutTemplates' => 'modal',
			'id' => 'file_input_judgeCart',


		],
		'pluginOptions' => [
			'showUpload' => false,
			'maxFileCount' => 1
		]
	])->label(NULL, ['class'=>$labelClass]); ;
	?>

</div>
