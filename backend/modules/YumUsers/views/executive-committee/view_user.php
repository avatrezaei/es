<?php

use yii\helpers\Html;
use backend\assets\UserProfile;

UserProfile::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Fields */

$this->title = Yii::t('app', 'View Referee');

?>
<?php if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php endif; ?>

<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
			<div class="profile-sidebar">
				<div class="portlet light profile-sidebar-portlet ">
					<div class="profile-userpic">
					<?php
						if(is_object($model))
						{
							echo $model->getIconThumbnail(null,"img-responsive");
						}
					?>
					</div>

					<div class="profile-usertitle" id="user_quick_info">
						<div class="profile-usertitle-name"> <?php echo Html::encode($model->FullName)?></div>
						<div class="profile-usertitle-job"> <?php echo Html::encode($model->nationalId)?> </div>
					</div>

					<div class="profile-userbuttons">
						<?php echo Html::a(Yii::t('app','Edit'), \yii\helpers\Url::to(['/YumUsers/executive-committee/update-referee','userId'=>$model->id],true),['class'=>"btn green btn-sm"]);?>
						<?php echo Html::a(Yii::t('app','Change Password'), \yii\helpers\Url::to(['/YumUsers/executive-committee/update-referee','userId'=>$model->id,'#' => 'tab_1_3'],true),['class'=>"btn red btn-sm"]);?>
					</div>

					<div class="profile-usermenu">

					</div>
				</div>

			</div>
			<?php
				echo $this->render('user_info_view', [
					'model' => $model,
					'eventId'=>$eventId

				])
			?>
		</div>
	</div>
</div>