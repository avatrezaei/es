<?php

namespace backend\modules\YiiFileManager\models;

use Yii;

/**
 * This is the model class for table "folders".
 *
 * @property string $id
 * @property integer $parent_id
 * @property string $name
 * @property string $hashed_name
 * @property string $path
 * @property integer $max_quota_volume
 * @property integer $max_depth
 * @property integer $total_size
 * @property string $permision
 * @property integer $user_id
 * @property integer $folder_type
 * @property string $folder_desc
 * @property integer $active
 * @property integer $valid
 */
class Folders extends \yii\db\ActiveRecord
{
	
	const SCENARIO_MKDIR='mkDir';
	
	
	
	
	
	
   public $fullFolderPath;
	public $rootDir;
	public $currentFolderId;
	public $parentPath;
	public $dirSeparator;
	public $realPath;
	public $error;
	public $fileModel;	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'folders';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[[ 'name', 'folder_desc'], 'required'],
		   [['name'],'dirNameValidation', 'on' => Folders::SCENARIO_MKDIR],
			[['parent_id', 'max_quota_volume', 'max_depth', 'total_size', 'user_id', 'folder_type', 'active', 'valid'], 'integer'],
			[['name'], 'string', 'max' => 150],
			[['hashed_name'], 'string', 'max' => 100],
			[['path', 'folder_desc'], 'string', 'max' => 1000],
			[['permision'], 'string', 'max' => 5],
			[['currentFolderId'],'safe']
		];
	}
	
	/*-------------------validation--------------------*/
	 public function dirNameValidation($attribute, $params) {

		if (!empty($this->$attribute) || $this->$attribute !== '') {

			$pattern = '/^([a-zA-Z0-9]|[\p{Arabic}]|[_])*$/u';

			if (!preg_match($pattern, $this->$attribute)) {
				$this->addError($attribute, 'فقط از کاراکترهای حرفی وعددی استفاده شود.');
			}
		}
		return TRUE;
	}
	/*--------------------------------------------------*/

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'parent_id' => Yii::t('app', 'Parent ID'),
			'name' => Yii::t('app', 'Name'),
			'hashed_name' => Yii::t('app', 'Hashed Name'),
			'path' => Yii::t('app', 'Path'),
			'max_quota_volume' => Yii::t('app', 'Max Quota Volume'),
			'max_depth' => Yii::t('app', 'Max Depth'),
			'total_size' => Yii::t('app', 'Total Size'),
			'permision' => Yii::t('app', 'Permision'),
			'user_id' => Yii::t('app', 'User ID'),
			'folder_type' => Yii::t('app', 'Folder Type'),
			'folder_desc' => Yii::t('app', 'Description'),
			'active' => Yii::t('app', 'Active'),
			'valid' => Yii::t('app', 'Valid'),
		];
	}
	/*---------relation with File--------------*/
	   public function getFiles()
	{
		return $this->hasMany(File::className(), ['folder_id' => 'id']);
	}
	/*-----------------Create Directory------------------------*/

	
	   public function beforeSave($insert ) {
		if (parent::beforeSave($insert)) {
				  if ($this->exsistsFolder($this->currentFolderId)) {
			$this->parent_id = $this->currentFolderId;
		}
		if (!$this->makeDir()) {
			return false;
		}
		$this->path = $this->getPathSequence($this->parent_id)
				. $this->getDirSeparator()
				. $this->name;
			$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
			if ($this->isNewRecord) {
				$this->created_date_time = date('Y-m-d H:i:s');
				$this->user_id = $userId;
			} else {
				$this->modified_date_time = date('Y-m-d H:i:s');
			}
			return true;
		} else {
			return false;
		}
	}
	
	
}
