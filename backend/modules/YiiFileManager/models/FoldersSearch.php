<?php

namespace backend\modules\YiiFileManager\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\YiiFileManager\models\Folders;

/**
 * FoldersSearch represents the model behind the search form about `backend\modules\YiiFileManager\models\Folders`.
 */
class FoldersSearch extends Folders {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'parent_id', 'max_quota_volume', 'max_depth', 'total_size', 'user_id', 'folder_type', 'active', 'valid'], 'integer'],
			[['name', 'hashed_name', 'path', 'permision', 'folder_desc'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$this->load($params);


		$queryFolder = Folders::find();
		$queryFile = File::find();
		
		if (!$this->validate()) {

			return $dataProvider;
		}
		
	 
		
		
				   $queryFile->andFilterWhere([
			//'id' => $this->id,
			'folder_id' => (int)$this->currentFolderId,
			'user_id' => $this->user_id,
			'active' =>1,
			'valid' => 1,
		]);   
		
		
		if (isset($this->name)&&!empty($this->name)) {
			$queryFolder->andFilterWhere(['like', 'name', $this->name]);
			$queryFile->andFilterWhere(['like', 'name', $this->name]);
			 $queryFolder->andFilterWhere([
		   // 'parent_id' =>(int) $this->currentFolderId,
			'user_id' => $this->user_id,
			'valid' => 1,
		]); 
		 $queryFolder->andFilterWhere(['=', 'parent_id', (int) $this->currentFolderId]); 
		// $queryFolder->OrFilterWhere(['=', 'parent_id', (int) $this->currentFolderId]); 
		}else
		{
		 $queryFolder->andFilterWhere([
			//'id' => $this->id,
			'parent_id' =>(int) $this->currentFolderId,
			'user_id' => $this->user_id,
			//'active' =>1,
			'valid' => 1,
		]);	 
		 }

		 /*
		  * Pagination in $dataProviderFolder & $dataProviderFile 
		  * must be very big .beacuse pagination center is in $dataProvider line 109
		  */
		$dataProviderFolder = new ActiveDataProvider([
			'query' => $queryFolder,
			 'pagination' => [
				'pageSize' => 200,
			],
		]);
  
		$dataProviderFile = new ActiveDataProvider([
			'query' => $queryFile,
			  'pagination' => [
				'pageSize' => 200,
			],
		]);

		$foldersModels = $dataProviderFolder->getModels();
		$fileModels=$dataProviderFile->getModels();

		$records = array();
		$records = array_merge($foldersModels, $fileModels);

		$dataProvider = new \yii\data\ArrayDataProvider([
			'allModels' => $records,
//			'sort' => [
//				'attributes' => ['id'],
//			],
			'pagination' => [
				'pageSize' => 20,
			],]
		);
		//die(var_dump($dataProvider));
		//  $this->load($params);
//		if (!$this->validate()) {
//			// uncomment the following line if you do not want to return any records when validation fails
//			// $query->where('0=1');
//			return $dataProvider;
//		}
//	  $queryFolder->andFilterWhere([
//			'id' => $this->id,
//			'parent_id' => $this->currentFolderId,
//			'user_id' => $this->user_id,
//			'active' =>1,
//			'valid' => 1,
//		]);
//
//		$queryFolder->andFilterWhere(['like', 'name', $this->name])
//					->andFilterWhere(['like', 'folder_desc', $this->folder_desc]);

		return $dataProvider;
	}
 public function searchByUser($params) {
		$this->load($params);


		$queryFolder = Folders::find();
		$queryFile = File::find();
		
 
		
	 
		
		
				   $queryFile->andFilterWhere([
			//'id' => $this->id,
			'folder_id' => (int)$this->currentFolderId,
			'user_id' => $this->user_id,
			'active' =>1,
			'valid' => 1,
		]);   
		
		
		if (isset($this->name)&&!empty($this->name)) {
			$queryFolder->andFilterWhere(['like', 'name', $this->name]);
			$queryFile->andFilterWhere(['like', 'name', $this->name]);
			 $queryFolder->andFilterWhere([
		   // 'parent_id' =>(int) $this->currentFolderId,
			'user_id' => $this->user_id,
			'valid' => 1,
		]); 
		 $queryFolder->andFilterWhere(['=', 'parent_id', (int) $this->currentFolderId]); 
		// $queryFolder->OrFilterWhere(['=', 'parent_id', (int) $this->currentFolderId]); 
		}else
		{
		 $queryFolder->andFilterWhere([
			//'id' => $this->id,
			'parent_id' =>(int) $this->currentFolderId,
			'user_id' => $this->user_id,
			//'active' =>1,
			'valid' => 1,
		]);	 
		 }

		$dataProviderFolder = new ActiveDataProvider([
			'query' => $queryFolder,
		]);
  
		$dataProviderFile = new ActiveDataProvider([
			'query' => $queryFile,
		]);

		$foldersModels = $dataProviderFolder->getModels();
		$fileModels=$dataProviderFile->getModels();

		$records = array();
		$records = array_merge($foldersModels, $fileModels);

		$dataProvider = new \yii\data\ArrayDataProvider([
			'allModels' => $records,
			'sort' => [
				'attributes' => ['id'],
			],
			'pagination' => [
				'pageSize' => 20,
			],]
		);
		//die(var_dump($dataProvider));
		//  $this->load($params);
//		if (!$this->validate()) {
//			// uncomment the following line if you do not want to return any records when validation fails
//			// $query->where('0=1');
//			return $dataProvider;
//		}
//	  $queryFolder->andFilterWhere([
//			'id' => $this->id,
//			'parent_id' => $this->currentFolderId,
//			'user_id' => $this->user_id,
//			'active' =>1,
//			'valid' => 1,
//		]);
//
//		$queryFolder->andFilterWhere(['like', 'name', $this->name])
//					->andFilterWhere(['like', 'folder_desc', $this->folder_desc]);

		return $dataProvider;
	}
}
