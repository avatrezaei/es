<?php

namespace backend\modules\YiiFileManager\models;

use yii\helpers\Html;
use Yii;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $name
 * @property string $hashed_name
 * @property string $ext
 * @property string $mimetype
 * @property integer $size
 * @property string $hash
 * @property string $path
 * @property integer $folder_id
 * @property integer $model_id
 * @property integer $user_id
 * @property string $model_type
 * @property string $file_desc
 * @property string $access_level
 * @property integer $is_directory
 * @property string $created_date_time
 * @property string $modified_date_time
 * @property integer $valid
 * @property integer $active
 */
class File extends \yii\db\ActiveRecord {

	const RANDOM_BYTES = 12;
	const Active = 1;
	const InActive = 0;
	const MAX_PIC_SIZE = 2097159; //1*1024*1024=1 MG
	const MIN_PIC_SIZE = 512; //512 KB
	const MAX_PDF_FILE_SIZE = 5242880; //5*1024*1024=5 MG
	const MIN_PDF_FILE_SIZE = 1024; //1024=1 KB
	const MAX_PPT_FILE_SIZE = 5242880; //5*1024*1024=5 MG
	const MIN_PPT_FILE_SIZE = 51200; //50*1024=50 KB
	const MAX_MOVIE_FILE_SIZE = 104857600; //100*1024*1024=100 MG
	const MIN_MOVIE_FILE_SIZE = 1048576; //1*1024*1024=1 MG
	//range file size for any type
	const MAX_FILE_SIZE = 10485760000; //100*1024*1024=100 MG
	const MIN_FILE_SIZE = 1024; //1*1024=1KB

	//file types varible for pdf,image,doc,..

	public $file = array();
	public $allAllowedFileType; //all file type in $allowedExtMimeTypeArray
	public $pdfFile;
	public $limitedPic; //for guest users
	public $limitedPicEmpty; //for admin for example upload in galery
	public $allAllowedPic; //for admin for example upload in galery
	public $docFile;
	public $swfFile;
	/*
	  for example pic file of gallery model -> $fileModelOwner=Gallery_album1
	  student card for user register => $fileModelOwner=Register model_id=12 file_desc=card file decs
	  for find card of user register by id=12 condition is:
	  $model_id=12 and fileModelOwner=Register and $file_desc=card
	 * 
	 */

	/*
	 * maax multi file upload
	 */
	public $maxFileCount;
	public $allowEmpty;
	/*
	 * allow file be empty if value is true
	 */
	public $error;
	/*
	 * file full path
	 */
	public $fullPath;
//	public $allowedMimeTypeArray = array(
//		'allAllowedPic' => array(
//			"mimeTypes" => array('image/gif', 'image/jpeg', 'image/tiff', 'image/png', 'application/x-shockwave-flash'),
//			"ext" => array('gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'cab'),
//		),
//		'limitedPicFile' => array(
//			"mimeTypes" => array('image/gif', 'image/jpeg'),
//			"ext" => array('gif', 'jpeg', 'jpg'),
//		),
//		'pdf' => array(
//			"mimeTypes" => array('application/pdf', 'application/octetstream'),
//			"ext" => array('pdf'),
//		),
//		'doc' => array(
//			"mimeTypes" => array('application/mspowerpoint',
//				'application/powerpoint',
//				'application/vnd.ms-powerpoint',
//				'application/x-mspowerpoint',
//				'application/msword',
//				'application/msword',
//				'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
//			),
//			"ext" => array('doc', 'docx'),
//		),
//		'allAllowedFileType' => array(
//			"mimeTypes" => array(
//				//pdf file
//				'application/pdf', 'application/pdf',
//				//pic file
//				'image/gif', 'image/jpeg', 'image/tiff', 'image/png', //pdf file
//				//movie mp3 file
//				'video/x-ms-wmv', 'video/x-ms-asf', 'audio/x-ms-wmv',
//				// 'application/octet-stream',
//				'audio/x-mpeg-3', 'audio/mpeg3',
//				//ppt file
//				'application/zip', //pptx
//				'application/vnd.ms-office',
//				'application/mspowerpoint',
//				'application/powerpoint',
//				'application/vnd.ms-powerpoint',
//				'application/x-mspowerpoint',
//				'application/msword',
//				'application/msword',
//				'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
//			),
//			"ext" => array('doc', 'docx', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'pdf', 'ppt', 'wmv', 'mp3', 'pptx'),
//		),
//		['allAllowedFileType', 'safe']
//	);
 public $allowedMimeTypeArray = array(
		'allAllowedPic' => array(
			"mimeTypes" => array('image/gif', 'image/jpeg', 'image/tiff', 'image/png', 'application/x-shockwave-flash'),
			"ext" => array('gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'cab'),
		),
		'limitedPicFile' => array(
			"mimeTypes" => array('image/gif', 'image/jpeg'),
			"ext" => array('gif', 'jpeg', 'jpg'),
		),
		'pdf' => array(
			"mimeTypes" => array('application/pdf', 'application/octetstream'),
			"ext" => array('pdf'),
		),
		'doc' => array(
			"mimeTypes" => array('application/mspowerpoint',
				'application/powerpoint',
				'application/vnd.ms-powerpoint',
				'application/x-mspowerpoint',
				'application/msword',
				'application/msword',
				'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
			),
			"ext" => array('doc', 'docx'),
		),
		'allAllowedFileType' => array(
			"mimeTypes" => array(
				//pdf file
				'application/pdf', 'application/pdf',
				//pic file
				'image/gif', 'image/jpeg', 'image/tiff', 'image/png', //pdf file
				//movie mp3 file
		   
			),
			"ext" => array( 'jpg', 'jpeg',),
		),
		['allAllowedFileType', 'safe']
	);
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'file';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			//[['name', 'ext', 'mimetype', 'size', 'path', 'folder_id'], 'required'],
			[['size', 'folder_id', 'user_id', 'valid', 'active'], 'integer'],
			[['created_date_time', 'modified_date_time'], 'safe'],
			[['name'], 'string', 'max' => 200],
			[['hashed_name', 'hash', 'model_type', 'access_level'], 'string', 'max' => 100],
			[['ext'], 'string', 'max' => 5],
			[['mimetype'], 'string', 'max' => 200],
			[['path'], 'string', 'max' => 1000],
			[['file_desc'], 'string', 'max' => 1000],
 
	   //   [['allAllowedFileType'], 'string', 'max' => 1000],
//			[['allAllowedFileType'], 'file',
//				'extensions'=>'jpg, gif, png,docx,ppt',
//				'mimeTypes' => 'image/jpeg'],
			[['allAllowedFileType'], 'safe'],
			[['allAllowedFileType'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg', 'checkExtensionByMimeType' => true],
			
//			array('allAllowedFileType', 'file',
//				'message' => '{attribute} نمی تواند خالی باشد',
//				'extensions' => 'docx,ppt',
//				'wrongExtension' => "فقط  فایل های با پسوند  " . implode(',', $this->allowedMimeTypeArray['allAllowedFileType']['ext']) . ' مجاز می باشد',
//				'maxSize' => self::MAX_FILE_SIZE,
//				'tooBig' => " حداکثر اندازه فایل باید " . $this->humanReadableFileSize(self::MAX_FILE_SIZE) . '  باشد',
//				'tooSmall' => "حداقل اندازه فایل باید  " . $this->humanReadableFileSize(self::MIN_FILE_SIZE) . '  باشد',
//				'minSize' => self::MIN_FILE_SIZE,
//				'mimeTypes' => $this->allowedMimeTypeArray['allAllowedFileType']['mimeTypes'],
//				'wrongMimeType' => 'نوع فایل غیر مجاز است.',
//				//'allowEmpty' => false,
//				'maxFiles' => 10,
//				'on' => 'createAllFile'
//			),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'hashed_name' => Yii::t('app', 'Hashed Name'),
			'ext' => Yii::t('app', 'Ext'),
			'mimetype' => Yii::t('app', 'Mimetype'),
			'size' => Yii::t('app', 'Size'),
			'hash' => Yii::t('app', 'Hash'),
			'path' => Yii::t('app', 'Path'),
			'folder_id' => Yii::t('app', 'Folder ID'),
			'model_id' => Yii::t('app', 'Model ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'model_type' => Yii::t('app', 'Model Type'),
			'file_desc' => Yii::t('app', 'File Desc'),
			'access_level' => Yii::t('app', 'Access Level'),
			'is_directory' => Yii::t('app', 'Is Directory'),
			'created_date_time' => Yii::t('app', 'Create Date'),
			'modified_date_time' => Yii::t('app', 'Update Date'),
			'valid' => Yii::t('app', 'Valid'),
			'active' => Yii::t('app', 'Active'),
		];
	}

	public function humanReadableFileSize($size_in_bytes) { // Function 2
		$value = 0;
		$round = 1;
		if ($size_in_bytes >= 1073741824) {
			$value = round($size_in_bytes / 1073741824 * 10) / 10;
			return ($round) ? round($value) . 'Gb' : "{$value} Gb";
		} else if ($size_in_bytes >= 1048576) {
			$value = round($size_in_bytes / 1048576 * 10) / 10;
			return ($round) ? round($value) . 'Mb' : "{$value} Mb";
		} else if ($size_in_bytes >= 1024) {
			$value = round($size_in_bytes / 1024 * 10) / 10;
			return ($round) ? round($value) . 'Kb' : "{$value} Kb";
		} else {
			return "{$size_in_bytes} Bytes";
		}
	}

	/* ---------relation with F-------------- */

	public function getFolders() {
		return $this->hasOne(Folders::className(), ['id' => 'folder_id']);
	}

	/* ----------------------------------------- */

	public function uploadFile($fileInstance, $folderId) {
		//die(var_dump($fileInstance->type));
		$preFixFileName = rand(1, 100000); //for rename file name input
		$fileName = $fileInstance->getBaseName() . '_' . $preFixFileName;
		$fullFileName = $fileName . '.' . $fileInstance->getExtension();
		$this->setAttributes(array(
			'name' => $fileName,
			'ext' => $fileInstance->getExtension(),
			'size' => $fileInstance->size,
			'mimetype' => $fileInstance->type,
			'folder_id' => $folderId,
		));
		if (!$this->validate(array('name', 'mimetype', 'ext', 'size'))) {
			die(var_dump($this->getErrors()));

			return FALSE;
		}
		//get folder path
		$folderModel = Folders::findOne($folderId);
		if ($folderModel == NULL) {
			die(var_dump($folderModel));

			$this->addError('folder_id', 'فایلی با  نا م' . Html::encode($fileInstance->name) . ' موجود است.');
			return FALSE;
		}
		$folderpath = $folderModel->ProvideRealFolderPath();
		$newFilePath = $folderpath . $folderModel->getDirSeparator() . $fullFileName;
		if (file_exists($newFilePath)) {
			$this->addError('name', 'فایلی با  نا م' . Html::encode($fileInstance->name) . ' موجود است.');
			return FALSE;
		} else {
			$this->path = $newFilePath;
		}
		// die(var_dump($newFilePath));
		//file az tmp be mahale asli miravad $_FILES 'C:\wamp\tmp\phpB33C.tmp  to  maghsade ma
		if (!$fileInstance->saveAs($newFilePath)) {

			$this->addError('name', 'مشکلی در ذخیره فایلی با  نا م ' . Html::encode($fileInstance->name) . ' بوجودآمده است.');
			return FALSE;
		}

		if (!$this->save()) {
			die(var_dump($this->errors));

			$this->addError('error', 'مشکلی در ذخیره اطلاعات فایل بوجود آمده است');
			return FALSE;
		}
		return TRUE;
	}

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

			if ($this->isNewRecord) {
				$this->created_date_time = date('Y-m-d H:i:s');
				$this->active = 1;
				$this->user_id = $userId;
			} else {
				$this->modified_date_time = date('Y-m-d H:i:s');
			}
			return true;
		} else {
			$this->addError('name', Yii::t('app', 'The problem has occurred in the storage Folder'));
			return false;
		}
	}

	/* ------------------------------------------ */

	public function getFileNameWithouExt($fileName) {
		$baseFileName = (explode('.', $fileName));
		return $baseFileName[0];
	}

	/* ----------------------------------------- */

	public function loadFolderModel($id) {
		$model = Folders::model()->findByPk($id);
		if ($model === null)
			return NULL;
		return $model;
	}

	/* ----------------------------------------- */

	public function loadFileModel($id) {
		$model = File::model()->findByPk($id);
		if ($model === null)
			return NULL;
		return $model;
	}

}
