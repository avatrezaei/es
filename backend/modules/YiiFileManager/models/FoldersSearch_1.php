<?php

namespace backend\modules\YiiFileManager\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\YiiFileManager\models\Folders;

/**
 * FoldersSearch represents the model behind the search form about `backend\modules\YiiFileManager\models\Folders`.
 */
class FoldersSearch extends Folders
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'parent_id', 'max_quota_volume', 'max_depth', 'total_size', 'user_id', 'folder_type', 'active', 'valid'], 'integer'],
			[['name', 'hashed_name', 'path', 'permision', 'folder_desc'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Folders::find();
	   //  $query = Folders::find()->with(['file']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'parent_id' => $this->currentFolderId,
			'max_quota_volume' => $this->max_quota_volume,
			'max_depth' => $this->max_depth,
			'total_size' => $this->total_size,
			'user_id' => $this->user_id,
			'folder_type' => $this->folder_type,
			'active' => $this->active,
			'valid' => $this->valid,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'hashed_name', $this->hashed_name])
			->andFilterWhere(['like', 'path', $this->path])
			->andFilterWhere(['like', 'permision', $this->permision])
			->andFilterWhere(['like', 'folder_desc', $this->folder_desc]);

		return $dataProvider;
	}
}
