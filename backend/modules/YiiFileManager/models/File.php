<?php

namespace backend\modules\YiiFileManager\models;

use yii\helpers\Html;
use Yii;
use backend\models\UserSetting;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $name
 * @property string $hashed_name
 * @property string $ext
 * @property string $mimetype
 * @property integer $size
 * @property string $hash
 * @property string $path
 * @property integer $folder_id
 * @property integer $model_id
 * @property integer $user_id
 * @property string $model_type
 * @property string $file_desc
 * @property string $access_level
 * @property integer $is_directory
 * @property string $created_date_time
 * @property string $modified_date_time
 * @property integer $valid
 * @property integer $active
 */
class File extends \yii\db\ActiveRecord {

	const RANDOM_BYTES = 12;
	const Active = 1;
	const InActive = 0;
	const MAX_PIC_SIZE = 2097159; //1*1024*1024=1 MG
	const MIN_PIC_SIZE = 256; //256 KB
	const MAX_PDF_FILE_SIZE = 5242880; //5*1024*1024=5 MG
	const MIN_PDF_FILE_SIZE = 1024; //1024=1 KB
	const MAX_PPT_FILE_SIZE = 5242880; //5*1024*1024=5 MG
	const MIN_PPT_FILE_SIZE = 51200; //50*1024=50 KB
	const MAX_MOVIE_FILE_SIZE = 104857600; //100*1024*1024=100 MG
	const MIN_MOVIE_FILE_SIZE = 1048576; //1*1024*1024=1 MG
	//range file size for any type
	const MAX_FILE_SIZE = 10485760000; //100*1024*1024=100 MG
	const MIN_FILE_SIZE = 300; //1*1024=1KB
	/* ----use in gallery thumbnail----*/
	const THUMBNAIL_FOLDER_Preix='thumbnail_';
	const  User_SUB_GALLERY_TUMBNAIL_FOLDER='user_sub_gallery_tumbnail_folder';
	/*
	 * Scenarios
	 */
	const MODEL_FILE_UPLOAD_SCENARIO='model_file_upload';
	const MODEL_GALLERY_FILE_UPLOAD_SCENARIO='model_gallery_file_upload';
	
/*
 * for create hash name file--to download safe and dont use id
 */	
	const HASH_ALGORITHM = 'md5';


	public $_UserSettingfileType;
	//file types varible for pdf,image,doc,..

	public $file = array();
	public $allAllowedFileType; //all file type in $allowedExtMimeTypeArray
	public $imageType;//all file type in image
	public $pdfFile;
	public $limitedPic; //for guest users
	public $limitedPicEmpty; //for admin for example upload in galery
	public $allAllowedPic; //for admin for example upload in galery
	public $docFile;
	public $swfFile;
	public $dirSeparator;
	/*some models need upload file image, this attribute get this*/
	public $temporaryFileAttr;
	/*Model set this attribute for validations ext*/
	public $modelFileTyps;
	/*Model set this attribute for validations mimetype*/
	public $modelMimeFileTyps;
	/*Model set this attribute for validations */
	public $modelFileskipOnEmpty=false;   
	/*Model set this attribute for validations size*/
	public $modelFileSize=2097159; //1*1024*1024=1 MG;
	
	



	/*
	  for example pic file of gallery model -> $fileModelOwner=Gallery_album1
	  student card for user register => $fileModelOwner=Register model_id=12 file_desc=card file decs
	  for find card of user register by id=12 condition is:
	  $model_id=12 and fileModelOwner=Register and $file_desc=card
	 * 
	 */

	/*
	 * maax multi file upload
	 */
	public $maxFileCount;
	public $allowEmpty;
	/*
	 * allow file be empty if value is true
	 */
	public $error;
	/*
	 * file full path
	 */
	public $fullPath;
	/*	 * ***************All Allowed file type ***************************** */
	public $AllFileExtentionArray = [ 'jpg', 'jpeg', 'png', 'pdf', 'doc', 'mp4', 'mp3'];
	public $AllFileExtMimeTypeArray = [
		'jpeg' => [
			"mimeTypes" => ['image/jpeg'],
			"ext" => ['jpeg'],
		],
		'jpg' => [
			"mimeTypes" => ['image/jpeg'],
			"ext" => ['jpg'],
		],
		'png' => [
			"mimeTypes" => ['image/png'],
			"ext" => ['png'],
		],
		'pdf' => [
			"mimeTypes" => ['application/pdf', 'application/octetstream'],
			"ext" => ['pdf'],
		],
		'doc' => [
			"mimeTypes" => [
				'application/mspowerpoint',
				'application/powerpoint',
				'application/vnd.ms-powerpoint',
				'application/x-mspowerpoint',
				'application/msword',
				'application/msword',
				'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
			],
			"ext" => ['doc', 'docx'],
		],
		'ppt' => [
			"mimeTypes" => [
				'application/mspowerpoint',
				'application/powerpoint',
				'application/vnd.ms-powerpoint',
				'application/x-mspowerpoint',
			],
			"ext" => ['ppt', 'pptx'],
		],
		'mp3' => [
			"mimeTypes" => [
				'audio/x-mpeg-3',
				'audio/mpeg3',
			],
			"ext" => ['mp3'],
		],
		'mp4' => [
			"mimeTypes" => [
				'video/x-ms-wmv',
				'video/x-ms-asf'
			],
			"ext" => ['mp4'],
		],
	];
	

/*
 * check Uploads folder root exist
 * by new Folder Run constructor of folder
 */	
	public function __construct($config = []) {
		$path = $_SERVER['DOCUMENT_ROOT'] . $this->getDirSeparator() . 'uploads';
		if (!is_dir($path)) {
		 $folderModel=new Folders;   
		 
		}

		parent::__construct($config);
	}   
	
	
	
	public function getDirSeparator() {

		if (!empty($this->dirSeparator)) {
			return $this->dirSeparator;
		} else {
			if (strncasecmp(PHP_OS, 'win', 3))
				return ($this->dirSeparator = "/");
			else
				return ($this->dirSeparator = "/");
		}
	}
	
	/*	 * ***********Deafult allowed file type for user setting **************************** */
	public $allowedMimeTypeArray = array(
		"mimeTypes" => array(
			//pdf file
			0 => 'application/pdf',
			1 => 'application/pdf',
			//pic file
			2 => 'image/gif', 3 => 'image/jpeg', 4 => 'image/png', //pdf file
			//movie mp3 file
			5 => 'application/powerpoint',
			6 => 'application/vnd.ms-powerpoint',
			7 => 'application/x-mspowerpoint',
			8 => 'application/msword',
			9 => 'application/msword',
			10 => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
		),
		"ext" => array(
			0 => 'jpg', 1 => 'jpeg', 2 => 'png', 3 => 'pdf', 4 => 'doc', 5 => 'docx'),
	);

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'file';
	}

	/**
	 * @inheritdoc
	 */public $imageFiles;

	public function rules() {
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$userFileTypeSetttingArray = $this->getUserFileTypeSetting($userId);
		$userFileTypeMimeType = $userFileTypeSetttingArray['mimeTypes'];
		$userFileTypeEXT = $userFileTypeSetttingArray['ext'];
//die(var_dump($userFileTypeEXT));
		return [
			[['folder_id'], 'required', 'on' => 'file_manager_upload'],
			[['size', 'folder_id', 'user_id', 'valid', 'active','model_id'], 'integer'],
			[['created_date_time', 'modified_date_time'], 'safe'],
			[['name'], 'string', 'max' => 200],
			[['hashed_name', 'hash', 'model_type', 'access_level'], 'string', 'max' => 100],
			[['ext'], 'string', 'max' => 5],
			[['mimetype'], 'string', 'max' => 200],
			[['path'], 'string', 'max' => 1000],
			[['file_desc'], 'string', 'max' => 1000],
			[['model_id','model_type'], 'safe'],
			[['allAllowedFileType','temporaryFileAttr'], 'safe'],
		   // [['allAllowedFileType','temporaryFileAttr'], 'uploadVolumLimitation'],
			/* ---------file validation ---------- */
			[['allAllowedFileType'], 'file',
				'skipOnEmpty' => false,
				'extensions' => $userFileTypeEXT,
				'checkExtensionByMimeType' => true,
				'maxSize' => self::MAX_FILE_SIZE,
				'tooBig' => " حداکثر اندازه فایل باید " . $this->humanReadableFileSize(self::MAX_FILE_SIZE) . '  باشد',
				'tooSmall' => "حداقل اندازه فایل باید  " . $this->humanReadableFileSize(self::MIN_FILE_SIZE) . '  باشد',
				'minSize' => self::MIN_FILE_SIZE,
			   // 'mimeTypes' =>'application/pdf',
				'mimeTypes' => $userFileTypeMimeType,
				'on' => 'file_manager_upload',
				//xss vulnerability for {file}--->example file name : <img src=33 onerror=alert(1)>
				// 'wrongMimeType'=>Yii::t('app','Invalid MIME type for {file}'),
				'wrongMimeType'=>  Html::encode(Yii::t('app','Invalid MIME type')),
			],
			
			/* ---------Image file validation ---------- */
			[['imageType'], 'file',
				'skipOnEmpty' => false,
				'extensions' => $userFileTypeEXT,
				'checkExtensionByMimeType' => true,
				'maxSize' => self::MAX_FILE_SIZE,
				'tooBig' => " حداکثر اندازه فایل باید " . $this->humanReadableFileSize(self::MAX_PIC_SIZE) . '  باشد',
				'tooSmall' => "حداقل اندازه فایل باید  " . $this->humanReadableFileSize(self::MIN_PIC_SIZE) . '  باشد',
				'minSize' => self::MIN_FILE_SIZE,
				'mimeTypes' => $userFileTypeMimeType,
				'on' => self::MODEL_GALLERY_FILE_UPLOAD_SCENARIO,
				//xss vulnerability for {file}--->example file name : <img src=33 onerror=alert(1)>
				// 'wrongMimeType'=>Yii::t('app','Invalid MIME type for {file}'),
				'wrongMimeType'=>  Html::encode(Yii::t('app','Invalid MIME type')),
			],	 
			
			
			
			/*-------Model file type validations-----*/
			/* ---------file validation ---------- */
			[['temporaryFileAttr'], 'file',
				'skipOnEmpty' => $this->modelFileskipOnEmpty,
				'extensions' => $this->modelFileTyps,
				'checkExtensionByMimeType' => true,
				'maxSize' => $this->modelFileSize,
				'tooBig' => " حداکثر اندازه فایل باید " . $this->humanReadableFileSize( $this->modelFileSize) . '  باشد',
				'tooSmall' => "حداقل اندازه فایل باید  " . $this->humanReadableFileSize(self::MIN_FILE_SIZE) . '  باشد',
				'minSize' => self::MIN_FILE_SIZE,
				//'mimeTypes' =>'application/pdf',
			   'mimeTypes' => $this->modelMimeFileTyps,
				'on' =>  self::MODEL_FILE_UPLOAD_SCENARIO,
				//xss vulnerability for {file}--->example file name : <img src=33 onerror=alert(1)>
				// 'wrongMimeType'=>Yii::t('app','Invalid MIME type for {file}'),
				'wrongMimeType'=>  Html::encode(Yii::t('app','Invalid MIME type')),
				],
		];
	}

  public function uploadVolumLimitation($attribute, $params) {
	  
	  $userRootFolderId=3;
	  if(isset($userRootFolderId))
	  {
		$modelFolder=new Folders;	
		$size=$modelFolder->dirSize($userRootFolderId);//return byte format
		/*
		 * beter code   $size + $this->$attribute->size;
		 */
		
		//die(var_dump($size).'--------'.  var_dump(Folders::FOLDER_MAX_QUOTA_VOLUME));
		if($size > Folders::FOLDER_MAX_QUOTA_VOLUME)
		{
			
			//die(var_dump($size).'---'.var_dump(Folders::FOLDER_MAX_QUOTA_VOLUME));
			$hsize=$modelFolder->humanReadableFileSize($size);

			$this->addError($attribute,Yii::t('app','All your {totalSize} is available volume amount of {usedSize} of it is used.',
					array('totalSize' =>$modelFolder->humanReadableFileSize(Folders::FOLDER_MAX_QUOTA_VOLUME),'usedSize' =>$modelFolder->humanReadableFileSize($size) )));
			
	   return FALSE;
	   }
		
		return TRUE;
		  
	  }
	  
  }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'hashed_name' => Yii::t('app', 'Hashed Name'),
			'ext' => Yii::t('app', 'Ext'),
			'mimetype' => Yii::t('app', 'Mimetype'),
			'size' => Yii::t('app', 'Size'),
			'hash' => Yii::t('app', 'Hash'),
			'path' => Yii::t('app', 'Path'),
			'folder_id' => Yii::t('app', 'Folder ID'),
			'model_id' => Yii::t('app', 'Model ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'model_type' => Yii::t('app', 'Model Type'),
			'file_desc' => Yii::t('app', 'File Desc'),
			'access_level' => Yii::t('app', 'Access Level'),
			'is_directory' => Yii::t('app', 'Is Directory'),
			'created_date_time' => Yii::t('app', 'Create Date'),
			'modified_date_time' => Yii::t('app', 'Update Date'),
			'valid' => Yii::t('app', 'Valid'),
			'active' => Yii::t('app', 'Active'),
			'temporaryFileAttr' => Yii::t('app', 'File Upload'),
		];
	}

	public function humanReadableFileSize($size_in_bytes) { // Function 2
		$value = 0;
		$round = 1;
		if ($size_in_bytes >= 1073741824) {
			$value = round($size_in_bytes / 1073741824 * 10) / 10;
			return ($round) ? round($value) . 'Gb' : "{$value} Gb";
		} else if ($size_in_bytes >= 1048576) {
			$value = round($size_in_bytes / 1048576 * 10) / 10;
			return ($round) ? round($value) . 'Mb' : "{$value} Mb";
		} else if ($size_in_bytes >= 1024) {
			$value = round($size_in_bytes / 1024 * 10) / 10;
			return ($round) ? round($value) . 'Kb' : "{$value} Kb";
		} else {
			return "{$size_in_bytes} Bytes";
		}
	}

	/* ---------relation with F-------------- */

	public function getFolders() {
		return $this->hasOne(Folders::className(), ['id' => 'folder_id']);
	}

/* ----------uploadImageFile------------------------------- */
 public function uploadImageFile($folderId = null) {
		if ($folderId == NULL)
			$folderId = $this->folder_id;

		$preFixFileName = rand(1, 100000); //for rename file name input
		$fileName = $this->imageType->getBaseName() . '_' . $preFixFileName;
		$fullFileName = $fileName . '.' . $this->imageType->getExtension();
		$this->setAttributes(array(
			'name' => $fileName,
			'ext' => $this->imageType->getExtension(),
			'size' => $this->imageType->size,
			'mimetype' => $this->imageType->type,
			'folder_id' => $folderId,
		));

		//get folder path
		$folderModel = Folders::findOne($folderId);
		if ($folderModel == NULL) {
			die(var_dump($folderModel));

			$this->addError('folder_id', 'فایلی با  نا م' . Html::encode($this->imageType->name) . ' موجود است.');
			return FALSE;
		}
		$folderpath = $folderModel->ProvideRealFolderPath();
		$newFilePath = $folderpath . $folderModel->getDirSeparator() . $fullFileName;
		if (file_exists($newFilePath)) {
			$this->addError('name', 'فایلی با  نا م' . Html::encode($this->imageType->name) . ' موجود است.');
			return FALSE;
		} else {
			$this->path = $newFilePath;
		}
		if (!$this->imageType->saveAs($newFilePath)) {

			$this->addError('name', 'مشکلی در ذخیره فایلی با  نا م ' . Html::encode($this->imageType->name) . ' بوجودآمده است.');
			return FALSE;
		}
		/* chon etelat as masir temp bardashteshode nabayad dobare validate beshe...chon khata mide===>parametr vorodi save=o */
		if (!$this->save(0)) {
			$this->addError('error', 'مشکلی در ذخیره اطلاعات فایل بوجود آمده است');
			return FALSE;
		}

		return TRUE;
	}
/*--------------------------------------------------*/
	public function uploadFile($folderId = null) {
		if ($folderId == NULL)
			$folderId = $this->folder_id;

		$preFixFileName = rand(1, 100000); //for rename file name input
		$fileName = $this->allAllowedFileType->getBaseName() . '_' . $preFixFileName;
		$fullFileName = $fileName . '.' . $this->allAllowedFileType->getExtension();
		$this->setAttributes(array(
			'name' => $fileName,
			'ext' => $this->allAllowedFileType->getExtension(),
			'size' => $this->allAllowedFileType->size,
			'mimetype' => $this->allAllowedFileType->type,
			'folder_id' => $folderId,
		));

		//get folder path
		$folderModel = Folders::findOne($folderId);
		if ($folderModel == NULL) {
			die(var_dump($folderModel));

			$this->addError('folder_id', 'فایلی با  نا م' . Html::encode($this->allAllowedFileType->name) . ' موجود است.');
			return FALSE;
		}
		$folderpath = $folderModel->ProvideRealFolderPath();
		$newFilePath = $folderpath . $folderModel->getDirSeparator() . $fullFileName;
		if (file_exists($newFilePath)) {
			$this->addError('name', 'فایلی با  نا م' . Html::encode($this->allAllowedFileType->name) . ' موجود است.');
			return FALSE;
		} else {
			$this->path = $newFilePath;
		}
		if (!$this->allAllowedFileType->saveAs($newFilePath)) {

			$this->addError('name', 'مشکلی در ذخیره فایلی با  نا م ' . Html::encode($this->allAllowedFileType->name) . ' بوجودآمده است.');
			return FALSE;
		}
		/* chon etelat as masir temp bardashteshode nabayad dobare validate beshe...chon khata mide===>parametr vorodi save=o */
		if (!$this->save(0)) {
			$this->addError('error', 'مشکلی در ذخیره اطلاعات فایل بوجود آمده است');
			return FALSE;
		}

		return TRUE;
	}

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

			if ($this->isNewRecord) {
				$this->created_date_time = date('Y-m-d H:i:s');
				$this->hashed_name = $this->generatehashBaseOnCreateDate(); //set folder hash name///mk dir by hash name by display by name
				$this->active = 1;
				$this->user_id = $userId;
				
			} else {
				$this->modified_date_time = date('Y-m-d H:i:s');
			}
			return true;
		} else {
			$this->addError('name', Yii::t('app', 'The problem has occurred in the storage Folder'));
			return false;
		}
	}

	/* ------------------------------------------ */

	public function getFileNameWithouExt($fileName) {
		$baseFileName = (explode('.', $fileName));
		return $baseFileName[0];
	}

	/* ----------------------------------------- */

	public function loadFolderModel($id) {
		$model = Folders::model()->findByPk($id);
		if ($model === null)
			return NULL;
		return $model;
	}

	/* ----------------------------------------- */

	public function loadFileModel($id) {
		$model = File::model()->findByPk($id);
		if ($model === null)
			return NULL;
		return $model;
	}

	public function getUserFileTypeSetting($userId = NULL) {
		$userSettingOBJ = new UserSetting;
		$fileTypeArray = $userSettingOBJ->getFileTypeByUserID($userId);
		if (empty($fileTypeArray)) {
			$fileTypeArray = $this->allowedMimeTypeArray;
		}

		return $fileTypeArray;
	}

	public function getIconBaseOnExt() {
		switch ($this->ext) {
			case 'jpg':
				return Html::img('@web/images/files/jpg-icon.png');
				break;
			case 'jpeg':
				return Html::img('@web/images/files/jpg-icon.png');
				break;	
			case 'png':
				return Html::img('@web/images/files/png-icon.png');
				break;
			case 'pdf':
				return Html::img('@web/images/files/pdf-icon.png');
				break;

			case 'mp3':
				return Html::img('@web/images/files/mp3-icon.png');
				break;
			
			case 'mp4':
				return Html::img('@web/images/files/mp4-icon.png');
				break;
			
			default :
				return Html::img('@web/images/files/file.png');
		}
	}
	
	/*------------------------*/
		public function generatehashBaseOnCreateDate() {

		if ($this->created_date_time== NULL) {
			$this->created_date_time = date('Y-m-d H:i:s');;
		}
		$string=	$this->created_date_time.rand(1000000,9999999);
		$this->hashed_name = hash(self::HASH_ALGORITHM,  $string);

		return ($sHash = $this->hashed_name);
	}
	
	/*-------------upload files in spechial folders--------------*/
		public function uploadModelFile($modelFileName=null,$folderId = null) {
		if ($folderId == NULL)
			$folderId = $this->folder_id;
		if($modelFileName==NULL)
		{
		$preFixFileName = rand(1, 100000); //for rename file name input
		$fileName = $this->temporaryFileAttr->getBaseName() . '_' . $preFixFileName;
		$fullFileName = $fileName . '.' . $this->temporaryFileAttr->getExtension();
		}  else
		{
		$preFixFileName = rand(1, 100000); //for rename file name input
		$fileName =$modelFileName . '_' . $preFixFileName;
		$fullFileName = $fileName . '.' . $this->temporaryFileAttr->getExtension();
		}
		
		$this->setAttributes(array(
			'name' => $fileName,
			'ext' => $this->temporaryFileAttr->getExtension(),
			'size' => $this->temporaryFileAttr->size,
			'mimetype' => $this->temporaryFileAttr->type,
			'folder_id' => $folderId,
		));
		


		//get folder path
		$folderModel = Folders::findOne($folderId);
		if ($folderModel == NULL) {

			$this->addError('temporaryFileAttr', 'پوشه مربوطه موجود نمی باشد');
			return FALSE;
		}

		$folderpath = $folderModel->ProvideRealFolderPath();
		$newFilePath = $folderpath . $folderModel->getDirSeparator() . $fullFileName;

		if (file_exists($newFilePath)) {
			$this->addError('temporaryFileAttr', 'فایلی با  نا م' . Html::encode($this->temporaryFileAttr->name) . ' موجود است.');
			return FALSE;
		} else {
			$this->path = $newFilePath;
		}

		if (!$this->temporaryFileAttr->saveAs($newFilePath)) {

			$this->addError('temporaryFileAttr', 'مشکلی در ذخیره فایلی با  نا م ' . Html::encode($this->temporaryFileAttr->name) . ' بوجودآمده است.');
			return FALSE;
		}
		/* chon etelat as masir temp bardashteshode nabayad dobare validate beshe...chon khata mide===>parametr vorodi save=o */
		if (!$this->save(0)) {
			$this->addError('temporaryFileAttr', 'مشکلی در ذخیره اطلاعات فایل بوجود آمده است');
			return FALSE;
		}

		return $this->id;
	}
		
		
		

		
  public function make_thumb($src, $dest, $desired_width) {
	  $mime=mime_content_type($src);

	  switch ($mime) {
		  case 'image/jpeg':
			  $imageCreateFunction='imagecreatefromjpeg';
			  $imageendFunction='imagejpeg';
			  break;
		  
		  case 'image/png':
			  $imageCreateFunction='ImageCreateFromPNG';
			  $imageendFunction='ImagePng';
			  break;
		  
		  case 'image/gif':
			  $imageCreateFunction='ImageCreateFromGIF';
			  $imageendFunction='ImageJPEG';
			  break;
		  default:
	   $imageCreateFunction='ImageCreateFromPNG';
			  $imageendFunction='ImagePng';
			  
			  
	  }
	/* read the source image */
	$source_image = $imageCreateFunction($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	
	/* create the physical thumbnail image to its destination */
	$imageendFunction($virtual_image, $dest);
}

	public function getIconThumbnail() {

			$url = yii\helpers\Url::to([ '/YiiFileManager/user-filemanager/view-tumbnail-file', 'name' =>  $this->hashed_name]);
			return $img = '<img alt="image" src=' . $url . '>';
		
	}
	
	
	public function getTumbnailFolderId($mainFolderId) {
		
		$mainFolderObj = Folders::findOne($mainFolderId);  
		if($mainFolderObj==NULL)
		{
			return FALSE;
		}
		
		$folderResult = Folders::find()
				->where(['user_id' => (int) $mainFolderObj->user_id, 'name'=>self::THUMBNAIL_FOLDER_Preix.$mainFolderObj->name,'folder_desc'=> self::User_SUB_GALLERY_TUMBNAIL_FOLDER])
				->all();
		
	   //die(var_dump($folderResult));
		if (count($folderResult) == 1) {
			$folderObj = $folderResult[0];
			return $folderObj->id;
		} else {
			
			return false;
		}
		
	}

}
