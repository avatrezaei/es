<?php

namespace backend\modules\YiiFileManager\models;

use Yii;
use yii\data\ArrayDataProvider;
use \backend\modules\YumUsers\models\User;

/**
 * This is the model class for table "folders".
 *
 * @property string $id
 * @property integer $parent_id
 * @property string $name
 * @property string $hashed_name
 * @property string $path
 * @property integer $max_quota_volume
 * @property integer $max_depth
 * @property integer $total_size
 * @property string $permision
 * @property integer $user_id
 * @property integer $folder_type
 * @property string $folder_desc
 * @property string $created_date_time
 * @property string $modified_date_time
 * @property integer $active
 * @property integer $valid
 */
class UserFolders extends Folders {

	public $userId;
	public $username;
	public $userFolderName;
	public $userFolderPath;
	public $userFolderParentPath;
	public $userFolderParentID;
	
	const User_ROOT_FOLDER='user_root_Folder';
	const User_CONTENT_FOLDER='user_Content_Folder';
	const User_GALLERY_FOLDER='user_Gallery_Folder';
	const User_SUB_GALLERY_FOLDER='user_sub_Gallery_Folder';
	const User_THUMBNAIL_GALLERY_FOLDER='user_Thumbnail_Gallery_Folder';

	const HASH_ALGORITHM = 'md5';
	const USER_MAX_QUOTA_VOLUME = 2048000; //2048000 kb =2 gig
	const USER_MAX_DEPTH = 6;
	const USERFOLDERID = 3;

	
	  public function attributes() {
		$attributes = parent::attributes();
		$attributes = array_merge($attributes, [
			'is_user_root_dir'
		]);
		return $attributes;
	}
	/* -----hash seuense=id.username.id---------- */

	/*
	  baraye ijade folder nabayad az save  beforesave
	 */

	public function CreateUserRootFolder($userId) {

		if (!$this->getParentForUserRootFolder()) {//set $userFolderParentID 
			return false;
		}

		$userObj = User::findOne($userId);
		if ($userObj == NULL) {
			return false;
		}
		/* ---userfolder obj attribute--- */
		$this->userId = $userObj->id;
		$this->username = $userObj->username;
		/* ---folder obj attribute--- */
		$this->active =1;
		$this->is_user_root_dir = 1;
		$this->user_id = $userObj->id;
		$this->name = $userObj->username;
		$this->folder_desc = self::User_ROOT_FOLDER;
		$this->folder_type=self::PRIVATE_FOLDER;
		$this->max_depth = self::USER_MAX_DEPTH;
		$this->parent_id = $this->userFolderParentID;
		$this->currentFolderId = $this->userFolderParentID;
		$this->max_quota_volume = self::USER_MAX_QUOTA_VOLUME;
		$this->hashed_name = $this->generatehashBaseOnUserInfo(); //set folder hash name///mk dir by hash name by display by name

		if ($this->save(false)) {
			/*----is_user_root_dir is added attribute after creation Model===> problem in save in in Save() function---*/
			$result = \Yii::$app->db->createCommand()->update($this->tableName(), ['is_user_root_dir' =>1], 'id='.(int)$this->id)->execute();

			//log check exceptions????
			return TRUE;
		} else {
			//die(var_dump($this->errors));
			//log check exceptions????
			return FALSE;
		}
	}

	public function getParentForUserRootFolder() {

		if ($this->exsistsFolder(self::USERFOLDERID)) {
			return $this->userFolderParentID = self::USERFOLDERID;
		}
		return FALSE;
	}

	
	/* Output = 64 characther */

	public function generatehashBaseOnUserInfo($userId = null, $userName = null) {

		if ($userId != NULL) {
			$this->userId = $userId;
		}

		if ($userName != NULL) {
			$this->username = $userName;
		}
		$stringSequence = $this->username . $this->userId . $this->username . $this->userId;
		$this->userFolderName = hash(self::HASH_ALGORITHM, $stringSequence);

		return ($sHash = $this->userFolderName);
	}

	  public function CreateUserFolder($userId,$folderName,$folderDesc) {

		if (!$this->getParentForUserRootFolder()) {//set $userFolderParentID 
			return false;
		}

		$userFoderId= $this->getUserRootFolderId($userId);
		if ($userFoderId==0) {
			return false;
		} 
		
		$userObj = User::findOne($userId);

		if ($userObj == NULL) {
			return false;
		}
		
		/* ---userfolder obj attribute--- */
		$this->userId = $userObj->id;
		$this->username = $userObj->username;
		/* ---folder obj attribute--- */
		$this->active =1;
		$this->is_user_root_dir = 0;
		$this->user_id = $userObj->id;
		$this->name = $folderName;
		$this->folder_desc =$folderDesc;
		$this->max_depth = self::USER_MAX_DEPTH-1;
		$this->parent_id =  $userFoderId;
		$this->currentFolderId =  $userFoderId;
		$this->max_quota_volume = self::USER_MAX_QUOTA_VOLUME;
		/*--set folder hash name///mk dir by hash name by display by name--*/
		
		$this->hashed_name = $this->generatehashBaseOnUserInfo(); 
		
		

		if ($this->save(0)) {
			//log check exceptions????
			return TRUE;
		} else {
			//log check exceptions????
			return FALSE;
		}
	}
	
	/* --------check access control-------------------- */

	public function getUserRootFolderId($userID) {
		$folderResult = Folders::find()
			   // ->where(['user_id' => (int) $userID, 'is_user_root_dir' => 1])
				->where(['user_id' => (int) $userID, 'folder_desc' => self::User_ROOT_FOLDER])
				->all();
		if (count($folderResult) == 1) {
			$folderObj = $folderResult[0];
			return $folderObj->id;
		} else {
			return false;
		}
	}
	public function getUserSpecialUserFolderId($userID,$folderType) {
		
		switch ($folderType) {
			case self::User_CONTENT_FOLDER:
				$type=self::User_CONTENT_FOLDER;
				break;

		   case self::User_GALLERY_FOLDER:
			   $type=self::User_GALLERY_FOLDER;
						  

			default:
				break;
		}
		
				//die(var_dump($type));

		$folderResult = Folders::find()
			   // ->where(['user_id' => (int) $userID, 'is_user_root_dir' => 1])
				->where(['user_id' => (int) $userID, 'folder_desc' => $type])
				->all();
		
		if (count($folderResult) == 1) {
			$folderObj = $folderResult[0];
			return $folderObj->id;
		} else {
			return false;
		}
	}
	
	
	
	
	public function userCheckAccessToFolder($userID, $folderID) {
		$folderResult = Folders::find()
				->where(['user_id' => (int) $userID, 'id' => (int) $folderID])
				->all();
		if (count($folderResult) == 1) {
			$folderObj = $folderResult[0];
			return $folderObj->id;
		} else {
			return false;
		}
	}
 public function getUserFolderBreadcrumbs($id=NULL) {
	 
	$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
	$userId=75;
	$userRootFolderId = $this->getUserRootFolderId($userId);
	if($id==NULL)
		$id=  $this->currentFolderId;
	
	$homePage=  \yii\helpers\Html::a(Yii::t('app','Home'),['/YiiFileManager/user-filemanager']).'>>';
	$fullBreadcrumbs=$homePage;
	
	$currentFolderModel = $this->find()->where(['id' => (int) $id])->one();
	$pathSeq=$currentFolderModel->path;
	
	if(!empty($pathSeq))
	{
		$arraySequence=explode('/', $pathSeq);
		/*dont show users branch and userfolder root such:
		 *home/users/dskfjhsdofjslfkjsdflkjsdf(rootuserfolder)
		 *so===>remove users/userfolderroot from  top of $arraySequence 
		 * by calling array_shift 3 times
		 */
		$arraySequence1=array_shift($arraySequence);
		$arraySequence1=array_shift($arraySequence);
		$arraySequence1=array_shift($arraySequence);
		$count=  count($arraySequence);

		$parentModels=array();

		
		for ($i=0;$i<$count;$i++) 
		 {
		   //die(var_dump($arraySequence));

			if(!empty($arraySequence[$i]))
			{
				/*
				 * hirearchical is top-down  => root->folder1->folder2.....
				 * so folderi has root parent by id=1
				 */
				$parentId=(is_object ($parentModels[($i-1)]))?$parentModels[($i-1)]->id:$userRootFolderId;//use previos folder model that is parent id

				$parentModels[$i] = $this->findOne(['name'=>$arraySequence[$i],'parent_id'=>$parentId]);
			   // $parentModels[$i] = $this->findOne(['user_id'=>$currentFolderModel->user_id,'name'=>$arraySequence[$i],'parent_id'=>$parentId]);
			  

				if($parentModels[$i]->id !=3&&$parentModels[$i]->id !=$userRootFolderId)
				 $fullBreadcrumbs .=  \yii\helpers\Html::a($arraySequence[$i],['/YiiFileManager/folders','id'=> $parentModels[$i]->id]).'>>';
			}
		}

		return $fullBreadcrumbs;

	}else
	{
		return $homePage;
	}
	}

	public function getUserGalleryFolderId($userID) {
		$folderResult = Folders::find()
			   // ->where(['user_id' => (int) $userID, 'is_user_root_dir' => 1])
				->where(['user_id' => (int) $userID, 'folder_desc' => self::User_GALLERY_FOLDER])
				->all();

		if (count($folderResult) == 1) {
			$folderObj = $folderResult[0];
			return $folderObj->id;
		} else {
			return false;
		}
	}
	
}
