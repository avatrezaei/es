<?php

namespace backend\modules\YiiFileManager\models;

use yii\helpers\Html;
use Yii;
use backend\models\UserSetting;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $name
 * @property string $hashed_name
 * @property string $ext
 * @property string $mimetype
 * @property integer $size
 * @property string $hash
 * @property string $path
 * @property integer $folder_id
 * @property integer $model_id
 * @property integer $user_id
 * @property string $model_type
 * @property string $file_desc
 * @property string $access_level
 * @property integer $is_directory
 * @property string $created_date_time
 * @property string $modified_date_time
 * @property integer $valid
 * @property integer $active
 */
class FileAction extends File {

		public function setRealFilePath($folderID = null) {
		$folderID = (is_null($folderID)) ? $this->folder_id : $folderID;
		if (($folderModel = Folders::findOne($folderID))== null) {
			$this->addError('name','Inval');
		}
		$fullPath = $folderModel->ProvideRealFolderPath() . $folderModel->getDirSeparator() . $this->name . '.' . $this->ext;
		if (!file_exists($fullPath)) {
			throw new \yii\base\Exception( 'مسیر شاخه مورد نظر موجود نمی باشد');
		}
		return $this->fullPath = $fullPath;
	}
	
	
		public function getFile() {
//		if ($this->access_level !== 'reader') {
//			if (!Yii::app()->user->checkAccess($this->access_level)) {
//				  throw new \yii\base\Exception('شما اجازه دسترسی به این  فایل را ندارید');
//			}
//		}
		if (headers_sent()) {
		   throw new NotFoundHttpException('header sent.');
			exit();
		}
		$this->setRealFilePath();
		//die(var_dump($this));

		// File Exists?
		if (file_exists($this->fullPath) && is_file($this->fullPath)) {
				// Required for some browsers
				if (ini_get('zlib.output_compression'))
					ini_set('zlib.output_compression', 'Off');

				header("Pragma: public"); // required
				header("Expires: 0");

				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-type: " . mime_content_type($this->fullPath));
				header("Content-length: " . filesize($this->fullPath));
				header("Content-disposition: attachment; filename=\"" . ($this->name . '.' . $this->ext) . "\"");
				header('Content-Transfer-Encoding: binary');
				ob_end_clean();
				flush();
				readfile($this->fullPath);
				exit;
		   
		} else {
			throw new \yii\base\Exception('فایل درخواست شده موجود نمی باشد');
			exit();
		}
	}
	public function getAllowedMimeToViwe() {

		return $allowed = array(
			//image
			'gif' => 'image/gif',
			'jpe' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'jpg' => 'image/jpeg',
			'png' => 'image/png',
			'tif' => 'image/tiff',
			'tiff' => 'image/tiff',
			'xbm' => 'image/x-xbitmap',
			'swf'=>'application/x-shockwave-flash',
			//documen
			'pdf' => 'application/pdf',
		);
	}
	public function viewFile() {
//		if ($this->access_level !== 'reader') {
//			if (!Yii::app()->user->checkAccess($this->access_level)) {
//				  throw new \yii\base\Exception('شما اجازه دسترسی به این  فایل را ندارید');
		  
//			}
//		}
		if (headers_sent()) {
			throw new ChtmlException(404, 'header sent.');
			exit();
		}

		$this->setRealFilePath();

		if (!in_array(mime_content_type($this->fullPath), $this->getAllowedMimeToViwe())) {
			throw new \yii\base\Exception( 'امکان مشاهده این فایل  وجود ندارد.' );
		}

		// File Exists?
		if (file_exists($this->fullPath) && is_file($this->fullPath)) {
				// Required for some browsers
				if (ini_get('zlib.output_compression'))
					ini_set('zlib.output_compression', 'Off');

				header("Content-type: " . mime_content_type($this->fullPath));
				ob_end_clean();
				flush();
				readfile($this->fullPath);
		  
		} else {
			throw new \yii\base\Exception( 'فایل درخواست شده موجود نمی باشد');
			exit();
		}
	}
	
	
 public function viewTumbnailFile() {

		if (headers_sent()) {
			throw new ChtmlException(404, 'header sent.');
			exit();
		}

		$this->setRealFilePath();
		//die(var_dump($this->fullPath));
					$folderModel = new Folders;

		$tumbnailfolderId=$this->getTumbnailFolderId($this->folder_id);
			$destination = $folderModel->ProvideRealFolderPath($tumbnailfolderId) . '/' . $this->name . '.' . $this->ext;
		$this->fullPath=$destination;

		if (!in_array(mime_content_type($this->fullPath), $this->getAllowedMimeToViwe())) {
			throw new \yii\base\Exception('امکان مشاهده این فایل  وجود ندارد.');
		}

		// File Exists?
		if (file_exists($this->fullPath) && is_file($this->fullPath)) {
				// Required for some browsers
				if (ini_get('zlib.output_compression'))
					ini_set('zlib.output_compression', 'Off');

				header("Content-type: " . mime_content_type($this->fullPath));
				ob_end_clean();
				flush();
				readfile($this->fullPath);
		  
		} else {
			throw new \yii\base\Exception( 'فایل درخواست شده موجود نمی باشد');
			exit();
		}
	}	
	
	
	
	
	public function exsistsFile($id = NULL) {
		if (is_null($id)) {
			$id = $this->id;
		}

		$folder = File::find()->where(['id' => (int) $id])->one();

		if ($folder == NULL) {
			return false;
			// throw new \yii\web\HttpException(404, 'مسیر درخواست شده موجود نمی باشد.');
		}
		return true;
	}
	public function removeFile($id = null) {
		if (!is_null($id)) {
			$this->id = $id;
		}
		
		if ($this->exsistsFile()) {

		  $this->setRealFilePath();
		if (!file_exists($this->fullPath) || !is_file($this->fullPath)) {
			$this->addError('error', Yii::t('app', 'The File does not exist.'));
			return FALSE;
		}


		if (unlink($this->fullPath)) {
			if (!$this->delete()) {
				$this->addError('error', Yii::t('app','The problem in removing data file, please try again.'));
				return FALSE;
			}
		} else {
				$this->addError('error', Yii::t('app', 'The problem in removing file, please try again.'));
			return FALSE;
		}
				}else
		{
			$this->addError('error', Yii::t('app', 'The File does not exist.'));
			return false;
		}
		return true;
	 
	}
	  public function userCheckAccessToFile($userID, $fileID) {
		$fileResult = File::find()
				->where(['user_id' => (int) $userID, 'id' => (int) $fileID])
				->all();
		if (count($fileResult) == 1) {
			$fileObj = $fileResult[0];
			return $fileObj->id;
		} else {
			return false;
		}
	}
		public function getUserRootFolderId($userID) {
		$folderResult = Folders::find()
				->where(['user_id' => (int) $userID, 'is_user_root_dir' => 1])
				->all();

		if (count($folderResult) == 1) {
			$folderObj = $folderResult[0];
			return $folderObj->id;
		} else {
			return false;
		}
	}
	
	
	
		protected function findModelFileAction($id) {
		if (($model = \backend\modules\YiiFileManager\models\FileAction::findOne($id)) !== null) {
			return $model;
		} else {
			return null;
		}
	}
}
