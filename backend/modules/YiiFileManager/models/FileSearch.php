<?php

namespace backend\modules\YiiFileManager\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\YiiFileManager\models\File;

/**
 * FileSearch represents the model behind the search form about `backend\modules\YiiFileManager\models\File`.
 */
class FileSearch extends File
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'size', 'folder_id', 'model_id', 'user_id', 'is_directory', 'valid', 'active'], 'integer'],
			[['name', 'hashed_name', 'ext', 'mimetype', 'hash', 'path', 'model_type', 'file_desc', 'access_level', 'create_date', 'update_date'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = File::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'size' => $this->size,
			'folder_id' => $this->folder_id,
			'model_id' => $this->model_id,
			'user_id' => $this->user_id,
			'is_directory' => $this->is_directory,
			'create_date' => $this->create_date,
			'update_date' => $this->update_date,
			'valid' => $this->valid,
			'active' => $this->active,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'hashed_name', $this->hashed_name])
			->andFilterWhere(['like', 'ext', $this->ext])
			->andFilterWhere(['like', 'mimetype', $this->mimetype])
			->andFilterWhere(['like', 'hash', $this->hash])
			->andFilterWhere(['like', 'path', $this->path])
			->andFilterWhere(['like', 'model_type', $this->model_type])
			->andFilterWhere(['like', 'file_desc', $this->file_desc])
			->andFilterWhere(['like', 'access_level', $this->access_level]);

		return $dataProvider;
	}
}
