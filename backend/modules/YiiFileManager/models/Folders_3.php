<?php

namespace backend\modules\YiiFileManager\models;

use Yii;
use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "folders".
 *
 * @property string $id
 * @property integer $parent_id
 * @property string $name
 * @property string $hashed_name
 * @property string $path
 * @property integer $max_quota_volume
 * @property integer $max_depth
 * @property integer $total_size
 * @property string $permision
 * @property integer $user_id
 * @property integer $folder_type
 * @property string $folder_desc
 * @property string $created_date_time
 * @property string $modified_date_time
 * @property integer $active
 * @property integer $valid
 */
class Folders extends \yii\db\ActiveRecord {

	const SCENARIO_MKDIR = 'mkDir';
	const ADMINSTRATE_FOLDER_NAME = 'adminestrator';
	const USERS_FOLDER_NAME = 'users';
	const DIR_CHMODE = 0700;

	//const FOLDER_MAX_QUOTA_VOLUME = 2048000; //2048000 kb =2 gig
	//const FOLDER_MAX_DEPTH = 5;
	public $fullFolderPath;
	public $rootDir;
	public $currentFolderId;
	public $parentPath;
	public $dirSeparator;
	public $realPath;
	public $error;
	public $fileModel;
	
	public $_parentFolderObj;


	/* ------Added Attribute-------- */
	public $is_user_root_dir;

	/*	 * *********** ***Add new attribute */

	public function attributes() {
		$attributes = parent::attributes();
		$attributes = array_merge($attributes, [
			'is_user_root_dir'
		]);
		return $attributes;
	}

	/**
	 * @inheritdoc
	 */
	public function __construct($config = []) {
		$path = $_SERVER['DOCUMENT_ROOT'] . $this->getDirSeparator() . 'uploads';

		// if (!is_dir($path) || is_null($this->find()->where(['id' => 1])->one())) {
		if (!is_dir($path)) {
			$this->initialBaseDir();
		}

		parent::__construct($config);
	}

	public static function tableName() {
		return 'folders';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['name', 'folder_desc'], 'required'],
			[['name'], 'dirNameValidation', 'on' => Folders::SCENARIO_MKDIR],
			[['parent_id', 'is_user_root_dir', 'max_quota_volume', 'max_depth', 'total_size', 'user_id', 'folder_type', 'active', 'valid'], 'integer'],
			[['name'], 'string', 'max' => 150],
			[['hashed_name'], 'string', 'max' => 100],
			[['path', 'folder_desc'], 'string', 'max' => 1000],
			[['permision'], 'string', 'max' => 5],
			[['currentFolderId', 'is_user_root_dir'], 'safe']
		];
	}

	/* -------------------validation-------------------- */

	public function dirNameValidation($attribute, $params) {

		if (!empty($this->$attribute) || $this->$attribute !== '') {

			$pattern = '/^([a-zA-Z0-9]|[\p{Arabic}]|[_])*$/u';

			if (!preg_match($pattern, $this->$attribute)) {
				$this->addError($attribute, 'فقط از کاراکترهای حرفی وعددی استفاده شود.');
			}
		}
		return TRUE;
	}

	/* -------------------------------------------------- */

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'parent_id' => Yii::t('app', 'Parent ID'),
			'name' => Yii::t('app', 'Name'),
			'hashed_name' => Yii::t('app', 'Hashed Name'),
			'path' => Yii::t('app', 'Path'),
			'max_quota_volume' => Yii::t('app', 'Max Quota Volume'),
			'max_depth' => Yii::t('app', 'Max Depth'),
			'total_size' => Yii::t('app', 'Total Size'),
			'permision' => Yii::t('app', 'Permision'),
			'user_id' => Yii::t('app', 'User ID'),
			'folder_type' => Yii::t('app', 'Folder Type'),
			'folder_desc' => Yii::t('app', 'Description'),
			'active' => Yii::t('app', 'Active'),
			'valid' => Yii::t('app', 'Valid'),
		];
	}

	/* ---------relation with File-------------- */

	public function getFiles() {
		return $this->hasMany(File::className(), ['folder_id' => 'id']);
	}

	/*	 * *******************initial------------------------------- */

	public function getDirSeparator() {

		if (!empty($this->dirSeparator)) {
			return $this->dirSeparator;
		} else {
			if (strncasecmp(PHP_OS, 'win', 3))
				return ($this->dirSeparator = "/");
			else
				return ($this->dirSeparator = "/");
		}
	}

	public function getRootDir() {
		$path = $_SERVER['DOCUMENT_ROOT'] . $this->getDirSeparator() . 'uploads';
		if (!is_dir($path) || is_null($this->find()->where(['id' => 1])->one())) {
			$this->initialBaseDir();
		}
		return $path;
	}

	public function initialBaseDir() {
		$path = $_SERVER['DOCUMENT_ROOT'] . $this->getDirSeparator() . 'uploads';
		if (!is_dir($path)) {
			if (!@mkdir($path, self::DIR_CHMODE, true))
				throw new \yii\web\HttpException(403, 'اشکال در ایجاد مدیریت فایل.لطفا دوباره سعی کنید');
		}


		if (is_null($this->find()->where(['id' => 1])->one())) {

			$query = "INSERT INTO `folders` (`id`, `parent_id`, `name`, `path`, `max_quota_volume`, `max_depth`, `valid`) VALUES 
			   ('1', '0', 'root', '', '', '', '1'),
			   ('2', '1', '" . self::ADMINSTRATE_FOLDER_NAME . "', '/" . self::ADMINSTRATE_FOLDER_NAME . "', '', '','0'),
			   ('3', '1', '" . self::USERS_FOLDER_NAME . "', '/" . self::USERS_FOLDER_NAME . "', '', '','0')
			   ;";
			$command = Yii::$app->db->createCommand($query);

			try {
				$result = $command->execute();
				$this->mkBaseDir($path . $this->getDirSeparator());
			} catch (Exception $result) {
				//die(var_dump($result));
				throw new \yii\web\HttpException(403, 'مشکل در ایجاد شاخه های اولیه بوجود آمده است.');
			}
		}
	}

	public function mkBaseDir($rootPath) {
		$dirSeprator = $this->getDirSeparator();
		if (!is_dir($rootPath . self::ADMINSTRATE_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::ADMINSTRATE_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه کاربران ثبت نامی  لطفا دوباره سعی کنید');
		}

		if (!is_dir($rootPath . self::USERS_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::USERS_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه کاربران ثبت نامی  لطفا دوباره سعی کنید');
		}
	}

	/* -----------------Create Directory------------------------ */

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
							if ($this->exsistsFolder($this->currentFolderId)) {
				$this->parent_id = $this->currentFolderId;
			} else {
				$this->addError('parent_id', Yii::app('app', 'The Folder Parent does not exist.'));
				return FALSE;
			}
			
			/* ----if is user folder root must new folder by hashed name in file system-------- */
			if (isset($this->is_user_root_dir) && $this->is_user_root_dir) {
				$folderName = $this->hashed_name;
			} else {
				$folderName = $this->name;
			}
			
			if (!$this->makeDir($folderName)) {
				$this->addError('name', Yii::app('The problem has occurred in the creation of Folder'));
				return false;
			}
			
			$this->path = $this->getPathSequence($this->parent_id)
					. $this->getDirSeparator()
					. $folderName;
			
			$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
			if ($this->isNewRecord) {
				$this->created_date_time = date('Y-m-d H:i:s');
				if (isset($this->is_user_root_dir) && !$this->is_user_root_dir)/* ----if user floder root setted before  */ {
							$this->active = 1;
		$this->is_user_root_dir = 0;
		$this->user_id = $userId;
		$this->max_depth = $this->getMaxDepth($this->parent_id)-1;

				}
				
			  
				
	 
			} else {
				$this->modified_date_time = date('Y-m-d H:i:s');
			}
			return true;
		} else {
			$this->addError('name', Yii::app('The problem has occurred in the storage Folder'));
			return false;
		}
	}

	/*
	 * check exsist folder or not 
	 */

	public function exsistsFolder($id = NULL) {
		if (is_null($id)) {
			$id = $this->id;
		}

		// the following will retrieve the user 'CeBe' from the database
		$folder = Folders::find()->where(['id' => (int) $id])->one();

		if ($folder == NULL) {
			return false;
			// throw new \yii\web\HttpException(404, 'مسیر درخواست شده موجود نمی باشد.');
		}
		return true;
	}

	/* ------------------------------------------------------- */

	public function ProvideRealFolderPath($id = NULL) {

		$id = (is_null($id)) ? $this->id : $id;

		$dirSeprator = $this->getDirSeparator();
		$folderPathSeq = $this->getPathSequence($id);

		/* ---just root folder with id=1 has empty path */
//		if (empty($folderPathSeq) && $id != 1) {
//			return false;
//		}

		//if directory seprator stored sequence is not compatible by server
		if (($dirSeprator) !== "/") {
			$folderPathSeq = str_replace("/", $dirSeprator, $folderPathSeq);
		}

		$fullFolderPath = $this->getRootDir() . $folderPathSeq;

		if (!is_dir($fullFolderPath)) {
			if (!@mkdir($fullFolderPath, self::DIR_CHMODE, true)) {
				return false;
				//throw new \yii\web\HttpException(404, 'مسیر داده شده موجود نمی باشد.');
			}
		}
		return ($this->fullFolderPath = $fullFolderPath);
	}

	/*
	 * create new directory
	 * return true if created new dir
	 */

	public function makeDir($folderName = null) {

		if ($folderName == NULL) {
			$folderName = $this->name;
		}
		//provideNewFolderPath  

		if (!( $realfolderPath = $this->ProvideRealFolderPath($this->parent_id))) {
			$this->addError('name', Yii::app('path is not correct'));
			return FALSE;
		}
		$newFolderPath = $realfolderPath . $this->getDirSeparator() . $folderName;

		if (is_dir($newFolderPath)) {
			$this->addError('name', Yii::app('The path to the directory with this name already exists.'));
			return FALSE;
		}

		if (!@mkdir($newFolderPath, self::DIR_CHMODE, true)) {
			$this->addError('name', Yii::app('The problem has occurred in the creation of new folder'));
			return FALSE;
		}

		return TRUE;
	}

	/*
	 * get sequence path stored in db 
	 * if folder dosent exist return null
	 */

	public function getPathSequence($id = null) {
		$id = (is_null($id)) ? $this->id : $id;
		if ($this->exsistsFolder($id)) {
			$pathSeq = $this->find()->where(['id' => (int) $id])->one()->path;
			return $pathSeq;
		}
		return false;
	}
	
	public function getMaxDepth($id = null) {
		$id = (is_null($id)) ? $this->id : $id;
		if ($this->exsistsFolder($id)) {
			$maxDepth = $this->find()->where(['id' => (int) $id])->one()->max_depth;
			return $maxDepth;
		}
		return false;
	}

	
	
	/* -----------------------Display Folder-------------------------------- */

	public function getChildFolders($folderId = NULL) {
		if ($folderId == NULL) {
			$folderId = $this->currentFolderId;
		}
		if ($this->exsistsFolder($folderId)) {
			$folderSql = "select id as folder_id,name as folder_name from  "
					. $this->tableName() .
					" where parent_id=" . (int) $folderId . " And 
						  valid=1 ";
			$command = Yii::$app->db->createCommand($folderSql);
			return $folderRows = $command->queryAll();	  // query and return all rows of result

			try {
				return $folderRows = $command->queryAll();	  // query and return all rows of result
			} catch (Exception $exc) {
				 return array();
			   // throw new CHttpException(400, 'Problem in request.Try again.');
			}
		} else {
			return array();
			// throw new CHttpException(400, 'شا');
		}
	}

	public function getChildFiles($folderId = NULL) {
		if ($folderId == NULL) {
			$folderId = $this->currentFolderId;
		}
		$fileObj = new File();
		if ($this->exsistsFolder($folderId)) {
			$fileSql = "select id as file_id,name as file_name,ext as file_ext,size"
					. " as file_size,file_desc as file_desc from  " . $fileObj->tableName() . " 
						where folder_id=" . (int) $folderId . "  ORDER BY id ASC";
			$command = Yii::$app->db->createCommand($fileSql);

			try {
				return $fileRows = $command->queryAll();	  // query and return all rows of result
			} catch (Exception $exc) {
				 return array();
				//throw new CHttpException(400, 'Problem in request.Try again.');
			}
		} else {
			return array();
		}
	}

	public function provideContentToDisplay($folderId = NULL) {
		if ($folderId == NULL) {
			$folderId = $this->currentFolderId;
		}

		$folders = $this->getChildFolders($folderId);
		$files = $this->getChildFiles($folderId);
		//die(var_dump( $folders));

		$i = 1;			//baraye namayesh dar grid view bayad index id dar array ersaly bashad
		$arrayData = array();
		foreach ($folders as $key => $value) {
			$arrayData[$i] = array(
				'id' => $i,
				'item_id' => $value['folder_id'],
				'name' => $value['folder_name'],
				'size' => '-',
				'is_dir' => true
			);
			$i++;
		}

		foreach ($files as $key => $value) {
			$arrayData[$i] = array(
				'id' => $i,
				'item_id' => $value['file_id'],
				'name' => $value['file_name'] . '.' . $value['file_ext'],
				'size' => $value['file_size'],
				'is_dir' => FALSE
			);
			$i++;
		}
		$dataprovider = new ArrayDataProvider([
			'key' => 'id', //or whatever you id actually is of these models.
			'allModels' => array_values($arrayData),
			'pagination' => array('pageSize' => 20),
		]);

		return $dataprovider;
	}

}
