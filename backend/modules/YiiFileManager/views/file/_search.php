<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\YiiFileManager\models\FileSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'name') ?>

	<?= $form->field($model, 'hashed_name') ?>

	<?= $form->field($model, 'ext') ?>

	<?= $form->field($model, 'mimetype') ?>

	<?php // echo $form->field($model, 'size') ?>

	<?php // echo $form->field($model, 'hash') ?>

	<?php // echo $form->field($model, 'path') ?>

	<?php // echo $form->field($model, 'folder_id') ?>

	<?php // echo $form->field($model, 'model_id') ?>

	<?php // echo $form->field($model, 'user_id') ?>

	<?php // echo $form->field($model, 'model_type') ?>

	<?php // echo $form->field($model, 'file_desc') ?>

	<?php // echo $form->field($model, 'access_level') ?>

	<?php // echo $form->field($model, 'is_directory') ?>

	<?php // echo $form->field($model, 'create_date') ?>

	<?php // echo $form->field($model, 'update_date') ?>

	<?php // echo $form->field($model, 'valid') ?>

	<?php // echo $form->field($model, 'active') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
