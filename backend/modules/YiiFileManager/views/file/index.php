<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\YiiFileManager\models\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Yii::t('app', 'Create File'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'name',
			'hashed_name',
			'ext',
			'mimetype',
			// 'size',
			// 'hash',
			// 'path',
			// 'folder_id',
			// 'model_id',
			// 'user_id',
			// 'model_type',
			// 'file_desc',
			// 'access_level',
			// 'is_directory',
			// 'create_date',
			// 'update_date',
			// 'valid',
			// 'active',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
