<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* * *select2 uses*** */
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use yii\web\View;
use yii\helpers\Url;
?>

<div class="file-form">
	<?php
	$form = ActiveForm::begin(['enableClientValidation' => false,
				'options' => ['enctype' => 'multipart/form-data'] // important
	]);
	echo $form->field($model, 'name');

  //  echo $form->field($model, "name");
	echo $form->field($model, "allAllowedFileType")->fileInput();
	?>

	<div class="form-group">
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

		<?php ActiveForm::end(); ?>

</div>
