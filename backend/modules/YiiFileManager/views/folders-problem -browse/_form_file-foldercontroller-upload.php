<?php

/*
	You need to use kartik\widgets\FileInput namespace if you have installed the yii2-widgets extension.
	You need to use kartik\file\FileInput namespace if you have ONLY installed the yii2-widget-fileinput extension.
 */
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;




/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form_file yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >
	
</div>

<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#new_file").on("pjax:end", function() {
		  //	$("#results").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\">t</a></div>");
			//   $("#results").show("slow");

$.pjax.reload({container:"#Folders"});  //Reload GridView)
		});
	});'
);
?>
 
<div class="data-form">
 
<?php yii\widgets\Pjax::begin(['id' => 'new_file']) ?>
	   <?= $massage ?>
		   <?php $form_file = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data','data-pjax' => true,'id'=>'form4' ]]); ?>
  	<?php echo $form_file->errorSummary($model);  ?>
	
	<?php
//
//echo FileInput::widget([
//	'name' => 'allAllowedFileType',
//	'options'=>[
//		'multiple'=>true
//	],
//	'pluginOptions' => [
//	   'uploadUrl' => Url::to(['/YiiFileManager/file/file-upload']),
//		'uploadExtraData' => [
//			'folder_id' => $model->currentFolderId,
//		   // 'cat_id' => 'Nature'
//		],
//		'maxFileCount' => 4
//	]
//]);
//  echo $form->field( , "file")->fileInput()->label(false) 

echo $form_file->field($fileModel, 'allAllowedFileType[]')->widget(FileInput::classname(), [
	'options'=>[
		'multiple'=>true
	],
	'pluginOptions' => [
	   // 'allowedFileExtensions'=>['jpg','gif','png'],
		'uploadUrl' => Url::to(['/YiiFileManager/folders/index']),
		'uploadExtraData' => [
			'folder_id' => $model->currentFolderId,
		   // 'cat_id' => 'Nature'
		],
		'maxFileCount' => 4
	]
		
		]);
	?>
	<?php //$model->currentFolderId=$model->currentFolderId;?>
	<?= $form_file->field($model, 'currentFolderId')->hiddenInput()->label(false) ?>

	
	

	
  
	
	
	
 
	<div class="form-group">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>
</div>
