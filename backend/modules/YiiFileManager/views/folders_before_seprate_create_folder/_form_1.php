<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;
use dosamigos\ckeditor\CKEditor;




 
/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >
	
</div>

<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#new_folders").on("pjax:end", function() {
		  //	$("#results").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\">t</a></div>");
			//   $("#results").show("slow");

$.pjax.reload({container:"#Folders"});  //Reload GridView)
		});
	});'
);
?>
 
<div class="data-form">
 
<?php yii\widgets\Pjax::begin(['id' => 'new_folders']) ?>
	   <?= $massage ?>
		   <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true,'id'=>'form3' ]]); ?>
  	<?php echo $form->errorSummary($model);  ?>
	<div class="form-group col-xs-12"> 

	<?= $form->field($model, 'name')->textInput(['maxlength' => 200]) ?>
	</div>
	<div class="form-group col-xs-12"> 

	<?= $form->field($model, 'folder_desc')->textInput(['maxlength' => 200]) ?>
	</div>

	
	

	
  
	
	
	
 
	<div class="form-group">
		<input type="submit" name="submitBtn" class = "btn btn-sb" value="<?= Yii::t('app', 'Create')?>"id="submit_btn">
		<input type="button" name="submitBtn" class = "btn btn-sb" id="close-btn-1" value="<?= Yii::t('app', 'Close')?>"id="submit_btn">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end() ?>
</div>
