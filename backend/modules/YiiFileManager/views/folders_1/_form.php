<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\YiiFileManager\models\Folders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="folders-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'parent_id')->textInput() ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'hashed_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'path')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'max_quota_volume')->textInput() ?>

	<?= $form->field($model, 'max_depth')->textInput() ?>

	<?= $form->field($model, 'total_size')->textInput() ?>

	<?= $form->field($model, 'permision')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'user_id')->textInput() ?>

	<?= $form->field($model, 'folder_type')->textInput() ?>

	<?= $form->field($model, 'folder_desc')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'active')->textInput() ?>

	<?= $form->field($model, 'valid')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
