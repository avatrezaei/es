<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\YiiFileManager\models\FoldersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="folders-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'parent_id') ?>

	<?= $form->field($model, 'name') ?>

	<?= $form->field($model, 'hashed_name') ?>

	<?= $form->field($model, 'path') ?>

	<?php // echo $form->field($model, 'max_quota_volume') ?>

	<?php // echo $form->field($model, 'max_depth') ?>

	<?php // echo $form->field($model, 'total_size') ?>

	<?php // echo $form->field($model, 'permision') ?>

	<?php // echo $form->field($model, 'user_id') ?>

	<?php // echo $form->field($model, 'folder_type') ?>

	<?php // echo $form->field($model, 'folder_desc') ?>

	<?php // echo $form->field($model, 'active') ?>

	<?php // echo $form->field($model, 'valid') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
