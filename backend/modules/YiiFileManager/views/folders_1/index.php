
<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = Yii::t('app', 'Folder');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--<h1><?= Html::encode($this->title) ?></h1>-->



<?php
$gridColumns = [
	['class' => 'yii\grid\SerialColumn'],
	[
		//'id'=>'tttttttt',
		'class' => 'kartik\grid\ExpandRowColumn',
		'header' => '',
		'mergeHeader' => false,
		//'expandAll' => false,
		// 'expandAllTitle' => 'Expand all',
		//'collapseTitle' => 'Collapse all',
		//'expandIcon' => '<span class="glyphicon glyphicon-expand"></span>',
		'value' => function ($model, $key, $index, $column) {
			return GridView::ROW_COLLAPSED;
		},
		'detailUrl' => Url::to(['/YiiFileManager/folders/detail']),
		'detailRowCssClass' => GridView::TYPE_DEFAULT,
		'pageSummary' => false,
	],
	[
		'attribute' => 'name',
	],
//	[
//		'attribute' => 'user_id',
//		'value' => function($data) {
//			return $data->getUserInfo('user_id');
//		},
//	],

//	[
//		'format' => 'html',
//		'attribute' => 'date',
//	//  'headerOptions' => ['width' => '80%',],
//	],
   
	[
		'class' => 'kartik\grid\ActionColumn',
		'header' => '',
		'mergeHeader' => false,
		'vAlign' => 'middle',
		'template' => '{delete}',
		'buttons' => [
			'delete' => function ($url, $model) {

				return \yii\helpers\Html::tag('span', '<span class="glyphicon glyphicon-trash"></span>', ['onclick' => "deleteItem(" . $model->id . ")"]);
			}
//		'expand' => function ($url, $model) {
//
//				return '<div style="width:50px;" title="گسترش" class="skip-export kv-align-center kv-expand-icon-cell">		
//		<div class="kv-expand-row">
//			 <div class="kv-expand-icon kv-state-expanded"><span class="glyphicon glyphicon-expand"></span></div>
//			<div style="display:none;" class="kv-expand-detail skip-export">
//				<div data-key="274" data-index="0" class="skip-export kv-expanded-row"></div>
//			</div>
//		</div></div>';
//			},
				],
			],
		];
		?>


		<div class="nav-button-bar">
			<?= Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', "Project") . ' ' . Yii::t('app', 'New'), null, ['class' => 'btn btn-default', 'id' => 'create-btn']) ?>
			<?= Html::a('<i class="glyphicon glyphicon-eye-open"></i> ' . Yii::t('app', 'Preview'), ['preview'], ['class' => 'btn btn-default']) ?>
		</div>

		<style>
			.box {
				height:0;
			}

			.data-form {
				overflow:hidden;
				max-height:0;
				opacity: 0;
				transition: max-height 2s linear, opacity 2s;

			}
			.box-change {
				max-height:700px;
				opacity: 1;
				margin-bottom: 30px;
				margin-top: 40px;
				padding:20px;
			}




		</style>

		<?php
		$script = ' $(function() {

			  $("#create-btn").click(function() {
				 $(".data-form").addClass("box-change");
				 $("i", this).addClass("glyphicon-chevron-down");
				 
			  });
			  
			  $("#close-btn-1").click(function() {
				 $(".data-form").removeClass("box-change");
			  });
			  
			});
		  ';

		$this->registerJs($script);
		?>


		<!-- Render create form -->	
		<?=
		$this->render('_form', [
			'model' => $model,
			'massage' => $massage
		])
		?>
		<?php ?>

		<div id="results_grid_msg" style="display: none" >

		</div>

		<div class="data-index">


			<?php
			Pjax::begin(['id' => 'Projects']);
			echo \kartik\grid\GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => $gridColumns,
				// 'layout' => '{items}{summary}',
				'summary' => '',
				'hover' => true,
				'panel' => [
					// 'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th"></i> Projects</h3>',
					'type' => 'primary',
				//'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
				//'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> ' . Yii::t('app', 'Reset Grid'), ['index'], ['class' => 'btn btn-info']),
				//'footer' => true
				],
				'toolbar' => [
					[
//					'content' =>
//					Html::button('<i class="glyphicon glyphicon-plus"></i>', [
//						'type' => 'button',
//						// 'title'=>Yii::t('kvgrid', 'Add Book'), 
//						'title' => 'Reset Grid',
//						'class' => 'btn btn-success'
//					]) . ' ' .
//					Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
//						'class' => 'btn btn-default',
//						//'title' => Yii::t('kvgrid', 'Reset Grid')
//						'title' => 'Reset Grid'
//					]),
//					'options' => ['class' => 'btn-group-sm']
					],
					'{export}',
					'{toggleData}',
				],
				'exportConfig' => [
					GridView::EXCEL => [
						'filename' => Yii::t('app', 'Experience'),
						'showConfirmAlert' => false,
					],
					GridView::PDF => [
						'filename' => Yii::t('app', 'Experience'),
						'showConfirmAlert' => false,
					]
				],
				'toggleDataContainer' => ['class' => 'btn-group-sm'],
				'exportContainer' => ['class' => 'btn-group-sm', 'showConfirmAlert' => FALSE],
				'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
				'headerRowOptions' => ['class' => 'kartik-sheet-style'],
				'filterRowOptions' => ['class' => 'kartik-sheet-style'],
					//  'showPageSummary' => true,
					//  'pageSummaryRowOptions'=>['class' => 'kv-page-summary warning']
//	'floatHeaderOptions'=>['scrollingTop'=>'50'],
//	'hover'=>true
			]);
			Pjax::end();
			?>	  

		</div>

		<script>


			function deleteItem(id)
			{
				var _url = "<?php echo Url::to([ '/YiiFileManager/folders/delete']) ?>" + "&id=" + id;
				var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Folder')]) ?>"
				var _failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')]) ?>"

				//if (confirm('<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>'))
				bootbox.confirm({
					buttons: {
						confirm: {
							label: '<?= Yii::t('yii', 'Yes'); ?>',
							className: 'confirm-button-class'
						},
						cancel: {
							label: '<?= Yii::t('yii', 'No'); ?>',
							className: 'cancel-button-class'
						}
					},
					message: '<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>',
					callback: function (result) {
						if (result)
						{
							$.ajax({
								type: 'POST',
								cache: false,
								url: _url,
								success: function (data) {
									$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"> " + _successmsg + " </div>");
									$("#results_grid_msg").show("slow");
									$('#results_grid_msg').delay(2000).fadeOut(1000);
									$.pjax.reload({container: '#Projects'});

								},
								error: function (data) {
									$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> " + _failmsg + " </div>");
									$("#results_grid_msg").show("slow");
									$('#results_grid_msg').delay(3000).fadeOut(1500);
								}
							});
						}

					},
					//title: "You can also add a title",
				});
		//	bootbox.confirm("<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>", function(result) {
//		if(result) {
//			{
//				$.ajax({
//					type: 'POST',
//					cache: false,
//					url: _url,
//		
//					success: function (data) {
//							$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"> " + _successmsg + " </div>");
//							$("#results_grid_msg").show("slow");
//							$('#results_grid_msg').delay(2000).fadeOut(1000);
//							$.pjax.reload({container: '#Projects'});
//							
//					},
//					error : function (data) {
//							$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> " + _failmsg + " </div>");
//							$("#results_grid_msg").show("slow");
//							$('#results_grid_msg').delay(3000).fadeOut(1500);
//					}
//				});
//			}
//		}
//	});


		return false;


	}

</script>

