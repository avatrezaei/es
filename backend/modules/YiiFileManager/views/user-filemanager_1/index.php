
<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = Yii::t('app', 'Folder');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--<h1><?= Html::encode($this->title) ?></h1>-->



<?php
$gridColumns = [
	['class' => 'yii\grid\SerialColumn'],
	[
		'header' => '',
		'format' => 'raw',
		'value' => function ($data) {
			$className = get_class($data);
			if ($className == 'backend\modules\YiiFileManager\models\Folders') {
				return Html::img('@web/images/files/folder-icon.png');
			} else if ($className == 'backend\modules\YiiFileManager\models\File') {
				return $data->getIconBaseOnExt();
			}
		},
	],
	[
		//'header' => 'نام',
		'format' => 'raw',
		'attribute' => 'name',
		'value' => function ($data) {
			$className = get_class($data);
			if ($className == 'backend\modules\YiiFileManager\models\Folders') {

				return Html::a(Html::encode($data["name"]), array("/YiiFileManager/folders/user-filemanager", "id" => $data["id"]));
			} else if ($className == 'backend\modules\YiiFileManager\models\File') {
				$fuulName = $data["name"] . '.' . $data["ext"];
				// return Html::a(Html::encode($fuulName), array("/YiiFileManager/file/get-file", "id" => $data["id"]),["onclick"=>"downloadFile(".$data['id'].")","target"=>"_self"]);
				return Html::tag('span', '<span style="cursor:pointer">' . Html::encode($fuulName) . '</span>', ["onclick" => "viewFile(" . $data['id'] . ")", "target" => "_self"]);
			}
		},
			],
			[
				'class' => 'kartik\grid\ActionColumn',
				'header' => '',
				'mergeHeader' => false,
				'vAlign' => 'middle',
				'template' => '{delete}{download}',
				'buttons' => [
					'delete' => function ($url, $data) {

						return \yii\helpers\Html::tag('span', '<span class="glyphicon glyphicon-trash"></span>', ['onclick' => "deleteItem(" . $data["id"] . ")"]);
					},
					'download' => function ($url, $data) {
						$className = get_class($data);
						if ($className == 'backend\modules\YiiFileManager\models\Folders')
							{
							return '';
							} else if ($className == 'backend\modules\YiiFileManager\models\File')
							{
							return Html::tag('span', '<span class="glyphicon glyphicon-download" style="cursor:pointer"></span>', ["cursor" => "pointer", "onclick" => "downloadFile(" . $data['id'] . ")", "target" => "_self"]);
							}
					},
						],
					],
				];
				?>




				<style>
					.box {
						height:0;
					}

					.data-form {
						overflow:hidden;
						max-height:0;
						opacity: 0;
						transition: max-height 2s linear, opacity 2s;
						max-height:700px;
						opacity: 1;
						margin-bottom: 30px;
						margin-top: 40px;
						padding:20px;

					}
					.box-change {

					}




				</style>

				<?php
				$script = ' $(function() {

			  $("#create-btn").click(function() {
				 $(".data-form").addClass("box-change");
				 $("i", this).addClass("glyphicon-chevron-down");
				 
			  });
			  
			  $("#close-btn-1").click(function() {
				 $(".data-form").removeClass("box-change");
			  });
			  
			});
		  ';

				$this->registerJs($script);
				?>


				<!-- Render create form -->

				<?=
				$this->render('_form', [
					'model' => $model,
					'massage' => $massage
				])
				?>

				<?php
				/*
				 * agar in khat zir ra bebarim balatar ke shamele _form ham shavad moshkel ijad mishavad
				 */
				Pjax::begin(['id' => 'Folders']);
				?>
				<?php
				echo $this->render('_form_file', [
					'model' => $model,
					'massage' => $massage,
				])
				?>
				<div id="results_grid_msg" style="display: none" >

				</div>

				<div class="data-index">


					<?php
					echo \kartik\grid\GridView::widget([
						//'dataProvider' => $model->provideContentToDisplay(),
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'columns' => $gridColumns,
						// 'layout' => '{items}{summary}',
						'summary' => '',
						'hover' => true,
						'panel' => [
							'type' => 'primary',
						],
						'toolbar' => [
							[
							],
							'{export}',
							'{toggleData}',
						],
						'exportConfig' => [
							GridView::EXCEL => [
								'filename' => Yii::t('app', 'Experience'),
								'showConfirmAlert' => false,
							],
							GridView::PDF => [
								'filename' => Yii::t('app', 'Experience'),
								'showConfirmAlert' => false,
							]
						],
						'toggleDataContainer' => ['class' => 'btn-group-sm'],
						'exportContainer' => ['class' => 'btn-group-sm', 'showConfirmAlert' => FALSE],
						'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
						'headerRowOptions' => ['class' => 'kartik-sheet-style'],
						'filterRowOptions' => ['class' => 'kartik-sheet-style'],
							//  'showPageSummary' => true,
							//  'pageSummaryRowOptions'=>['class' => 'kv-page-summary warning']
//	'floatHeaderOptions'=>['scrollingTop'=>'50'],
//	'hover'=>true
					]);
					Pjax::end();
					?>	  
<?php
$this->registerJs(
'$("#Folders").on("pjax:end", function() {
var arr = document.URL.match(/id=([0-9]+)/)
if(arr!=null&&arr.length>0)
{
var id_url_param = arr[1];
if(is_numeric(id_url_param))
{
document.getElementById("userfolders-currentfolderid").setAttribute("value",id_url_param);
}
else
{
id_url_param=0;
document.getElementById("userfolders-currentfolderid").setAttribute("value",id_url_param);
}
}
});

');
?>

					<script>
						function is_numeric(str) {
							return /^\d+$/.test(str);
						}
				//							$(document).delegate("Folders", 'pjax:end', function (e) {
				//		e.preventDefault();
				//		alert(222);
				//	});
				//		$(document).delegate(funct  ion () {
				//	 $("#Folders").on("pjax:end", function() {
				//alert(5555);
				//	});
				//	});


					</script>

				</div>

				<script>

					function downloadFile(id)
					{
						var _url = "<?php echo Url::to([ '/YiiFileManager/user-filemanager/get-file']) ?>" + "&id=" + id;
						window.location.href = _url;

						//alert(id);
					}
					function viewFile(id)
					{
						var _url = "<?php echo Url::to([ '/YiiFileManager/user-filemanager/view-file']) ?>" + "&id=" + id;
						window.location.href = _url;

						//alert(id);
					}
					function deleteItem(id)
					{
						var _url = "<?php echo Url::to([ '/YiiFileManager/user-filemanager/rm-dir']) ?>" + "&id=" + id;
						//var _successmsg = "<?php echo Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Folder')]) ?>"
						var _failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')]) ?>"

						//if (confirm('<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>'))
						bootbox.confirm({
							buttons: {
								confirm: {
									label: '<?= Yii::t('yii', 'Yes'); ?>',
									className: 'confirm-button-class'
								},
								cancel: {
									label: '<?= Yii::t('yii', 'No'); ?>',
									className: 'cancel-button-class'
								}
							},
							message: '<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>',
			callback: function (result) {
				if (result)
				{
					$.ajax({
						type: 'POST',
						cache: false,
						url: _url,
						success: function (data) {
							if (data.success == true)
							{
								$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\"> " + data.messages + "</a> </div>");
								$("#results_grid_msg").show("slow");
								$('#results_grid_msg').delay(3000).fadeOut(3000);
								$.pjax.reload({container: '#Folders'});
							} else if (data.success == false)
							{
								$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> <a href=\"#\" class=\"alert-link\">" + data.messages + "</a>  </div>");
								$("#results_grid_msg").show("slow");
								$('#results_grid_msg').delay(3000).fadeOut(3000);
							}

						},
						error: function (data) {
							$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"><a href=\"#\" class=\"alert-link\"> " + _failmsg + " </a> </div>");
							$("#results_grid_msg").show("slow");
							$('#results_grid_msg').delay(3000).fadeOut(2000);
						}
					});
				}

			},
//title: "You can also add a title",
		});



		return false;


	}

</script>

