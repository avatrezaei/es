<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\YiiFileManager\models\Folders */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Folders',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Folders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="folders-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
