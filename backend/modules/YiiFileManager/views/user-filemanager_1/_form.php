<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* * *select2 uses*** */
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use yii\web\View;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >

</div>

<?php
$this->registerJs(
		'$("document").ready(function(){ 
		$("#new_folders").on("pjax:end", function() {
		  //	$("#results").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\">t</a></div>");
			//   $("#results").show("slow");

$.pjax.reload({container:"#Folders"});  //Reload GridView)
		});
	});'
);
?>

<div class="data-form">

	<?php yii\widgets\Pjax::begin(['id' => 'new_folders']) ?>
	<?= $massage ?>
	<?php $form = ActiveForm::begin(['action' => ['/YiiFileManager/user-filemanager/newfolder'], 'options' => ['data-pjax' => true, 'id' => 'form_create_folder']]); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="form-group col-xs-6"> 

		<?= $form->field($model, 'name')->textInput(['maxlength' => 200]) ?>
	</div>
	<div class="form-group col-xs-6"> 

		<?= $form->field($model, 'folder_desc')->textInput(['maxlength' => 200]) ?>
			<?= $form->field($model, 'currentFolderId')->hiddenInput()->label(false) ?>

	</div>

	<div class="form-group col-xs-4" >
		<span style="background-color: #f4f4f4;	border-color: #ddd;	color: #444;" id="submit_btn_create_folder"  class = "btn btn-sb" ><?php echo Yii::t('app', 'Create') ?></span>
	</div>

	<?php ActiveForm::end(); ?>
	<?php yii\widgets\Pjax::end() ?>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
	$(document).delegate("#submit_btn_create_folder", 'click', function (e) {
		e.preventDefault();
		formSTR = $(this).parents('form').attr('id');
		createNewFolder();
	});
	function createNewFolder()
	{
		var _form = $('#form_create_folder');
		$.ajax({
			type: _form.attr('method'),
			url: _form.attr('action'),
			data: _form.serialize(),
			dataType: "JSON",
			success: function (data) {
				errorsArray=data.errors;
				/**success save**/
				if ((typeof data.success !== 'undefined') && data.success == 1)
				{
					$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"> " + data.messages + " </div>");

					$("#results_grid_msg").fadeIn(1100, function () {

						$.pjax.reload({container: "#Folders"});
					});
				} else  if(errorsArray.length > 0)/***********fail save *****************/
				{
					
					 $("#results_grid_msg").fadeOut(10);//clear sucess message
					$.each(data.errors, function (i, v) {
						//error same as ===> ['name'=>['ttest error 1','ttest error 2']]
						if (i == 'name')
						{
							$("#folders-name").parent('div').addClass('required has-error');
							$msg = '';
							var length = v.length;
							for (var i = 0; i < length; i++) {
								$msg += v[i] + '-';
							}
							$msg = "<div class='help-block'>" + $msg + "</div>";
							$('#folders-name').after($msg);

						} else if (i == 'folder_desc')
						{
							$("#folders-folder_desc").parent('div').addClass('required has-error');
							$msg = '';
							var length = v.length;
							for (var i = 0; i < length; i++) {
								$msg += v[i] + '-';
							}
							$msg = "<div class='help-block'>" + $msg + "</div>";
							$('#folders-name').after($msg);
						}
					});

				} else
						{

							$("#folders-name").parent('div').addClass('required has-error');
							$msg = "<div class='help-block'>" + data.messages + "</div>";
							$('#folders-name').after($msg);
						}
			},
			error: function (data) {
			   alert('bad request');
			}
		});


	}

</script>