
<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use \kartik\grid\DataColumn;
use yii\helpers\Url;
use kartik\detail\DetailView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Language;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = Yii::t('app', 'Folder');
$this->params['breadcrumbs'][] = $this->title;
?>

<!--<h1><?= Html::encode($this->title) ?></h1>-->



<?php
$gridColumns = [
	['class' => 'yii\grid\SerialColumn'],
	
		[
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => function ($model, $key, $index, $column) {
			$className = get_class($model);
			if ($className == 'backend\modules\YiiFileManager\models\Folders') {
				return ['value' => 'folder_' . $model->id];
			} else if ($className == 'backend\modules\YiiFileManager\models\File') {
				return ['value' => 'file_' . $model->hashed_name];
			}
		}
	 ],
			 
			 
	[
		'header' => '',
		'format' => 'raw',
		'value' => function ($data) {
			$className = get_class($data);
			if ($className == 'backend\modules\YiiFileManager\models\Folders') {
				return Html::img('@web/images/files/folder-icon.png');
			} else if ($className == 'backend\modules\YiiFileManager\models\File') {
				return $data->getIconBaseOnExt();
			}
		},
	],
   
	   [
		'format' => 'raw',
		'attribute' => 'name',
		'value' => function ($data) {
			$className = get_class($data);
			if ($className == 'backend\modules\YiiFileManager\models\Folders') {

				return Html::a(Html::encode($data["name"]), array("/YiiFileManager/user-filemanager/", "id" => $data["id"]));
			} else if ($className == 'backend\modules\YiiFileManager\models\File') {
				$fuulName = $data["name"] . '.' . $data["ext"];
				// return Html::a(Html::encode($fuulName), array("/YiiFileManager/file/get-file", "id" => $data["id"]),["onclick"=>"downloadFile(".$data['id'].")","target"=>"_self"]);
				return Html::tag('span', '<span style="cursor:pointer">' . Html::encode($fuulName) . '</span>', ["onclick" => "viewFile('" . $data['hashed_name'] . "')", "target" => "_self"]);
			}
		},
		],
		[
			'class' => 'kartik\grid\ActionColumn',
			'header' => '',
			'mergeHeader' => false,
			'vAlign' => 'middle',
			'template' => '{delete}{download}',
			'buttons' => [
				'delete' => function ($url, $data) {
					$className = get_class($data);
					if ($className == 'backend\modules\YiiFileManager\models\Folders') {
						return \yii\helpers\Html::tag('span', '<span class="glyphicon glyphicon-trash"></span>', ['onclick' => "deleteItem(" . $data["id"] . ",'folder')"]);
					} else if ($className == 'backend\modules\YiiFileManager\models\File') {
						return \yii\helpers\Html::tag('span', '<span class="glyphicon glyphicon-trash"></span>', ['onclick' => "deleteItem('" . $data["hashed_name"] . "','file')"]);
					}
				},
				'download' => function ($url, $data) {
					$className = get_class($data);
					if ($className == 'backend\modules\YiiFileManager\models\Folders') {
						return '';
					} else if ($className == 'backend\modules\YiiFileManager\models\File') {
						return Html::tag('span', '<span class="glyphicon glyphicon-download" style="cursor:pointer"></span>', ["cursor" => "pointer", "onclick" => "downloadFile('" . $data['hashed_name'] . "')", "target" => "_self"]);
					}
				},
					],
				],
				];
				?>




				<style>
					.box {
						height:0;
					}

					.data-form {
						overflow:hidden;
						max-height:0;
						opacity: 0;
						transition: max-height 2s linear, opacity 2s;
						max-height:700px;
						opacity: 1;
						margin-bottom: 30px;
						margin-top: 40px;
						padding:20px;

					}
					.box-change {

					}




				</style>

				<?php
				$script = ' $(function() {

			  $("#create-btn").click(function() {
				 $(".data-form").addClass("box-change");
				 $("i", this).addClass("glyphicon-chevron-down");
				 
			  });
			  
			  $("#close-btn-1").click(function() {
				 $(".data-form").removeClass("box-change");
			  });
			  
			});
		  ';
   $this->registerJs($script);
?>

<?php
	/* ------------show flash messages----------------- */
	$session = Yii::$app->session;
   // check the availability
	$result = $session->hasFlash('security_error_authority');
   // get and display the message
	echo $session->getFlash('security_error_authority');
 $this->registerJs(' $(function() {$(".alert-danger").fadeOut(5000);});');

?>


<!-- Render create form -->

				<?=
				$this->render('_form', [
					'model' => $model,
					'massage' => $massage
				])
				?>
  <div id="results_grid_msg" style="display: none" >

				</div>
				<?php
				/*
				 * agar in khat zir ra bebarim balatar ke shamele _form ham shavad moshkel ijad mishavad
				 */
				Pjax::begin(['id' => 'Folders']);
				?>
				<?php
				echo $this->render('_form_file', [
					'model' => $model,
					'massage' => $massage,
				])
				?>
			  
<?= Html::beginForm(['controller/bulk'], 'post', ['id' => 'bulk_action_form']) ?>

<span onclick="bulkAction('delete')" style="background-color: #f4f4f4;	border-color: #ddd;	color: #444;" id="bulk_act_delete"  class = "btn btn-sb" ><?php echo Yii::t('app', 'Delete') ?></span>
				<div class="data-index">


<?php
//echo $model->getUserFolderBreadcrumbs();

echo \kartik\grid\GridView::widget([
	//'dataProvider' => $model->provideContentToDisplay(),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $gridColumns,
	'summary' => '',
	'hover' => true,
	'panel' => [
		'type' => 'primary',
	],
	'toolbar' => [
		[
		],
	   // '{export}',
		'{toggleData}',
	],

	'toggleDataContainer' => ['class' => 'btn-group-sm'],
	'exportContainer' => ['class' => 'btn-group-sm', 'showConfirmAlert' => FALSE],
	'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
	'headerRowOptions' => ['class' => 'kartik-sheet-style'],
	'filterRowOptions' => ['class' => 'kartik-sheet-style'],

]);
echo Html::endForm();
Pjax::end();
?>	  
<?php
$this->registerJs(
'$("#Folders").on("pjax:end", function() {
var arr = document.URL.match(/id=([0-9]+)/)
if(arr!=null&&arr.length>0)
{
var id_url_param = arr[1];
if(is_numeric(id_url_param))
{
document.getElementById("userfolders-currentfolderid").setAttribute("value",id_url_param);
}
else
{
id_url_param=0;
document.getElementById("userfolders-currentfolderid").setAttribute("value",id_url_param);
}
}
});

');
?>

			 

				</div>

				<script>
/*--------------------------------*/
function is_numeric(str) {
	return /^\d+$/.test(str);
}
/*--------------------------------*/
function downloadFile(name)
{
var _url = "<?php echo Url::to([ '/YiiFileManager/user-filemanager/get-file']) ?>" + "&name=" + name;
window.location.href = _url;

}
function viewFile(name)
{
var _url = "<?php echo Url::to([ '/YiiFileManager/user-filemanager/view-file']) ?>" + "&name=" + name;
window.location.href = _url;

}
function deleteItem(id, type)
{
if (type == 'file')
{
var _url = "<?php echo Url::to([ '/YiiFileManager/user-filemanager/delete-file']) ?>" + "&name=" + id;
var _failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')]) ?>"

} else
{
var _url = "<?php echo Url::to([ '/YiiFileManager/user-filemanager/rm-dir']) ?>" + "&id=" + id;
var _failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')]) ?>"
}
bootbox.confirm({
	buttons: {
		confirm: {
			label: '<?= Yii::t('yii', 'Yes'); ?>',
			className: 'confirm-button-class'
		},
		cancel: {
			label: '<?= Yii::t('yii', 'No'); ?>',
			className: 'cancel-button-class'
		}
	},
	message: '<?php echo Yii::t('yii', 'Are you sure you want to delete this item?') ?>',
	callback: function (result) {
		if (result)
		{
		$.ajax({
		type: 'POST',
		cache: false,
		url: _url,
		success: function (data) {
		if (data.success == true)
		{
			$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\"> " + data.messages + "</a> </div>");
			$("#results_grid_msg").show("slow");
			$('#results_grid_msg').delay(4000).fadeOut(4000);
			$.pjax.reload({container: '#Folders'});
		} else if (data.success == false)
		{
			$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> <a href=\"#\" class=\"alert-link\">" + data.messages + "</a>  </div>");
			$("#results_grid_msg").show("slow");
			$('#results_grid_msg').delay(4000).fadeOut(4000);
		}

		},
		error: function (data) {
		$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"><a href=\"#\" class=\"alert-link\"> " + _failmsg + " </a> </div>");
		$("#results_grid_msg").show("slow");
		$('#results_grid_msg').delay(4000).fadeOut(4000);
		}
		});
		}

	},
	//title: "You can also add a title",
});



return false;


}
/*---------------------------------------------------------------------*/

function bulkAction(act) {
						var _failmsg = "<?php echo Yii::t('app', 'Failed To Delete Items To') ?>"
						switch (act) {
							case 'delete':
								var _url = "<?php echo Url::to([ '/YiiFileManager/user-file-manager-bulk-actions/delete']) ?>";
								break;
							case 'zip':
								var _url = "<?php echo Url::to([ '/YiiFileManager/file/delete']) ?>";
								break;
							default:
								var _url = "";

						}

						bootbox.confirm({
							buttons: {
								confirm: {
									label: '<?= Yii::t('yii', 'Yes'); ?>',
									className: 'confirm-button-class'
								},
								cancel: {
									label: '<?= Yii::t('yii', 'No'); ?>',
									className: 'cancel-button-class'
								}
							},
							message: '<?php echo Yii::t('app', 'Are you sure you want to delete this items?') ?>',
	callback: function (result) {
		if (result)
		{
			var msg='-';
			$.ajax({
				type: 'POST',
				data: $('#bulk_action_form').serializeArray(),
				url: _url,
				success: function (data) {
					if (data.success == true)
					{
						$("#results_grid_msg").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\"> " + data.messages + "</a> </div>");
						$("#results_grid_msg").show("slow");
						$('#results_grid_msg').delay(4000).fadeOut(2000);
						$.pjax.reload({container: '#Folders'});
					} else if (data.success == false)
					{
						  $.each(data.messages, function (i, v) {
						//error same as ===> ['name'=>['ttest error 1','ttest error 2']]
						if (i == 'notfoundfiles')
						{
						  var length = v.length;
							for (var i = 0; i < length; i++) {
							   // alert(v[i]);
								msg += v[i] + '-';
							}

						}  else if (i=='notfoundfolder')
						{
						  var length = v.length;
							for (var i = 0; i < length; i++) {
								msg += v[i] + '-';
							}							
						} 
						else if (i=='deletefileerror')
						{
						  var length = v.length;
							for (var i = 0; i < length; i++) {
								msg += v[i] + '-';
							}
						} else if(i == 'deletefoldererror')
						{
							var length = v.length;
							for (var i = 0; i < length; i++) {
								msg += v[i] + '-';
							}
						}
					  
					});

						$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"> <a href=\"#\" class=\"alert-link\">" + msg + "</a>  </div>");
						$("#results_grid_msg").show("slow");
						$('#results_grid_msg').delay(4000).fadeOut(2000);
						$.pjax.reload({container: '#Folders'});

					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					$("#results_grid_msg").html("<div class=\"alert alert-danger\" role=\"alert\"><a href=\"#\" class=\"alert-link\"> " + _failmsg + " </a> </div>");
					$("#results_grid_msg").show("slow");
					$('#results_grid_msg').delay(4000).fadeOut(2000);
				}
			});
		}

	},
	//title: "You can also add a title",
});


return false;

}

</script>

