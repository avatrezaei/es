<?php

/*
	You need to use kartik\widgets\FileInput namespace if you have installed the yii2-widgets extension.
	You need to use kartik\file\FileInput namespace if you have ONLY installed the yii2-widget-fileinput extension.
 */
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/***select2 uses****/
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\Language;
use  yii\web\View;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use app\models\AppLanguage;




/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form_file yii\widgets\ActiveForm */
?>
<div id="results" style="display: none" >
	
</div>

<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
		$("#new_file").on("pjax:end", function() {
		  //	$("#results").html("<div class=\"alert alert-success\" role=\"alert\"><a href=\"#\" class=\"alert-link\">t</a></div>");
			//   $("#results").show("slow");

$.pjax.reload({container:"#Folders"});  //Reload GridView)
		});
	});'
);
?>
 
<div class="data-form">
 
	   <?= $massage ?>
	  
	
	<?php
//   echo \yii\bootstrap\ActiveForm::beginForm(array('/YiiFileManager/file/file-upload','id'=>$model->id), 'post',array('enctype'=>'multipart/form-data'))
//		.'<span>بارگذاری فایل :</span> ' 
//		.Html::fileField('file')
//		.Html::submitButton('Upload', array('name' => 'upload','style'=>'width:80px;'))
//		.Html::endForm('');
if (isset($_COOKIE[AppLanguage::cookieName]) && $_COOKIE[AppLanguage::cookieName] == AppLanguage::en_US) {
$lang='en';
	
}  else {
	$lang='fa';
}
echo FileInput::widget([
	'name' => 'allAllowedFileType',
	'id'=>'file_input_',
	'language'=>$lang,
	
	'options'=>[
		'multiple'=>true,
		'layoutTemplates'=>'modal',
	],
	'pluginOptions' => [
	   //'allowedFileExtensions'=>['jpg','gif','png'],
	   'uploadUrl' => Url::to(['/YiiFileManager/file/user-file-upload']),
		'uploadExtraData' => [
		   'folder_id' => $model->currentFolderId,
		  
		],
		'maxFileCount' => 4
	],
	'pluginEvents'=>[
	 'fileuploaded'=> "function() { $.pjax.reload({container: '#Folders'}); }",
//		'fileuploaded'=>"function(event, data, previewId, index, jqXHR) {
//	if (data.sucess) {
//	   return {
//		   message: 'You are not allowed to do that',
//		   data: {key1: 'Key 1', detail1: 'Detail 1'}
//	   };
//   }}",
	],
]);

//echo $form_file->field($fileModel, 'allAllowedFileType[]')->widget(FileInput::classname(), [
//	'options'=>[
//		'multiple'=>true
//	],
//	'pluginOptions' => [
//	   // 'allowedFileExtensions'=>['jpg','gif','png'],
//		'uploadUrl' => Url::to(['/YiiFileManager/file/file-upload']),
//		'uploadExtraData' => [
//			'folder_id' => $model->currentFolderId,
//		   // 'cat_id' => 'Nature'
//		],
//		'maxFileCount' => 4
//	]
//		
//		]);
	?>
	<?php //$model->currentFolderId=$model->currentFolderId;?>

	
	

	
  
	
	
	
 
	<div class="form-group">
		<?php  //Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success','id'=>'submit_btn','name'=>'btnSubmit']) ?>
	</div>
 
</div>
