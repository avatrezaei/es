<?php

namespace backend\modules\YiiFileManager;
use Yii;

class YiiFileManager extends \yii\base\Module
{
	public $controllerNamespace = 'backend\modules\YiiFileManager\controllers';
	
   

	public function init()
	{
		parent::init();
				if (empty(Yii::$app->i18n->translations['app'])) {
			Yii::$app->i18n->translations['app'] = [
				//  'language'=>'fa-IR',
				'sourceLanguage' => 'fa',
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => __DIR__ . '/messages',
			];
		}

		// custom initialization code goes here
	}
}
