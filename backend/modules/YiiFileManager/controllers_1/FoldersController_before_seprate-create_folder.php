<?php

namespace backend\modules\YiiFileManager\controllers;

use Yii;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FoldersSearch;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\web\Session;
use yii\web\Response;

/**
 * FoldersController implements the CRUD actions for Folders model.
 */
class FoldersController extends Controller {

	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function actionIndex($id = NULL) {
		//die(var_dump($_POST));
		$model = new Folders();
		$model->scenario = Folders::SCENARIO_MKDIR;
		/* ------validate is numeric value------------ */

		if ($id == NULL || !is_numeric($id)) {
		  if(isset($_POST['currentFolderId'])&&is_numeric($_POST['currentFolderId']))
			{
			   $id=$_POST['currentFolderId'];
			}else
			{
			   // die(var_dump($_POST['currentFolderId']));
			$id = 1; //root of folder for user admin is 1 but user is??
			}
		} else if(isset ($_POST['Folders'])){
		  //$id=$_POST['currentFolderId'];
		}
		$massage = '';
		
		/*----currentFolderId be onvan parent mahsoob mishavad ham baraye folder va file ke 
		 ----- mikhahad ijad shavad -----*/
		$model->currentFolderId = $id;

		$model->is_user_root_dir = 0;
		if ($model->load(Yii::$app->request->post())) {
			
			if(isset($_POST['currentFolderId'])&& $id!=$_POST['currentFolderId'])
			{
				//die(var_dump($_REQUEST));
				 $model->addError ('name','problem in browse file');
			}
		   //die(var_dump($_REQUEST));
			if ($model->save()) {
				/*				 * *******unset attributes to show empty after create item*********** */
				foreach ($model->attributes as $key => $value) {
					unset($model->$key);
				}
				/*				 * ********************************* */

				$massage = '<div class="alert alert-success" role="alert" id="form_create_msg">
					<a href="#" class="alert-link">' . Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'Folder'))) . '</a>
					</div>';
			} else {
				$model->addError('title', Yii::t('app', 'Failed To Add Items To {item}.', ['item' => Yii::t('app', 'Folder')]));
			}
		}

		$searchModel = new FoldersSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
					'model' => $model,
					'massage' => $massage,
		]);
	}

	public function actionView($id) {

		$model = $this->findModel($id);
		if ($model->load(Yii::$app->request->post())) {
			$model->save(); //validate and then save model
		}

		return $this->render('view', [
					'model' => $model,
		]);
	}

	public function actionDetail() {
		if (isset($_POST['expandRowKey']))
			$id = Yii::$app->request->post('expandRowKey');
		else
			die(('Not Found'));


		$model = $this->findModel($id);

		return $this->renderAjax('expand_detail', [
					'model' => $model,
		]);
	}

	public function actionRmDir($id) {
	  $post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {
			if (isset($post['id']))
				$id = (int) $post['id'];
			else if (isset($_GET['id']))
				$id = (int) $_GET['id'];


			$model = $this->loadFolderModel($id);
			$parent_id = $model->parent_id;
			if ($model->removeDir()) {
						   // die(var_dump($model));

				if ($model->delete()) {
							   
  Yii::$app->response->format = Response::FORMAT_JSON;
				  return[
				   'success' => true,
				   'messages' =>Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Folder')])]
				  ;					
				} else {
  Yii::$app->response->format = Response::FORMAT_JSON;
				  return[
				   'success' => false,
				   'messages' =>Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')])]
				  ;
			  }
			} else {
				if (is_array($model->errors)) {
					$msg = '';
					foreach ($model->errors as $itemerror) {
						foreach ($itemerror as $error)
							 $msg .=$error . '-';
					}
				} else {
					$msg = Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')]);
				}
				  Yii::$app->response->format = Response::FORMAT_JSON;
				  return[
				   'success' => false,
				   'messages' => $msg]
				  ;


//			   echo Json::encode([
//				   'success' => false,
//				   'messages' => $msg]);
			}
			//return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $parent_id]);
		}
	   // throw new InvalidCallException("You are not allowed to do this operation. Contact the administrator.");
	}

	public function loadFolderModel($id) {
		$model = Folders::findOne($id);

		if ($model === null) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder does not exist.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}
		if (empty($model->valid) || $model->valid == 0) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder was deleted.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}
		
		return $model;
	}

	public function actionDelete1($id = NULL) {
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {

			if (isset($post['id']))
				$id = (int) $post['id'];
			else if (isset($_GET['id']))
				$id = (int) $_GET['id'];

			$model = $this->findModel($id);
			if ($model->delete()) {

				echo Json::encode([
					'success' => true,
					'messages' => [Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Folder')])]]);
			} else {
				$detailView = $this->renderAjax('expand_detail', ['model' => $model]);
				echo Json::encode([
					'success' => false,
					'detailviewcontentt' => $detailView,
					'messages' => [Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')])]]);
			}
			// return;
		}
//		else if (isset($_GET['id'])) {
//			$id = (int) $_GET['id'];
//			$this->findModel($id)->delete();
//			return;
//		}
		throw new InvalidCallException("You are not allowed to do this operation. Contact the administrator.");
	}

	/**
	 * Finds the Folders model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return Folders the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Folders::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
