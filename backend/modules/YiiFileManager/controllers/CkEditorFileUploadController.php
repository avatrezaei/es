<?php

namespace backend\modules\YiiFileManager\controllers;

use Yii;
use \backend\modules\YiiFileManager\models\File;
use \backend\modules\YiiFileManager\models\Folders;
use \backend\modules\YiiFileManager\models\UserFolders;
use \backend\modules\YiiFileManager\models\FileSearch;
use \yii\web\Controller;
use \yii\web\NotFoundHttpException;
use \yii\filters\VerbFilter;
use \yii\web\UploadedFile;
use \yii\web\Response;
use \yii\filters\AccessControl;
use \yii\helpers\Url;

/**
 * FileController implements the CRUD actions for File model.
 */
class CkEditorFileUploadController extends Controller {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
					'only' => ['upload', 'uplsoad', 'ViewFile'],
				'rules' => [

					[
						'actions' => ['upload', 'GetFile', 'ViewFile'],
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	  public function beforeAction()
	{	  
		if ($this->action->id == 'upload') {
			Yii::$app->controller->enableCsrfValidation = false;
		}
		return true;
	}
	
//	public function actionUpload($CKEditor=null,$CKEditorFuncNum=NULL,$langCode=NULL) {
//		$message='';
//		$url='';
//		$fileModel=new File;
//		$fileModel->scenario=  File::MODEL_FILE_UPLOAD_SCENARIO;
//		$fileModel->modelFileTyps='doc,docx,jpeg,png,pdf';
//		$fileModel->modelMimeFileTyps=['application/pdf,image/jpeg','image/jpeg','image/png'];
//		$massage = '';
//	   if (isset($_POST)) {
//				
//				/*----------Save Main Pic & thumbNail Pic in Category & Category_timenail Folder ------------*/
//			   $fileModel->folder_id = Folders::CATEGORIES_FOLDER_ID;
//			   $fileModel->model_id =2;
//			   $fileModel->model_type ='testttt';
//			   $fileModel->temporaryFileAttr =  \yii\web\UploadedFile::getInstanceByName('upload');
//			   $name='testr_'.  rand(444,44444);
//			   
//			   if ($fileModel->validate()&&$id=$fileModel->uploadModelFile($name)) {
//
//	 
//					$folderModel = new Folders;
//					$destination = $folderModel->ProvideRealFolderPath(Folders::CATEGORIES_FOLDER_ID) . '/' . $fileModel->name . '.' . $fileModel->ext;
//					$url = Url::to([ '/YiiFileManager/file/view-file', 'name' => $fileModel->hashed_name]);
//					   
//		 
//					}else
//					{
//						if(!empty($fileModel->errors))
//						{
//							foreach ($fileModel->errors as $key => $error) {
//								foreach ($error as $key => $value) {
//									 $message.=$value;
//								}
//								
//							}
//						}  else {
//								  $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
//						}
//
//					   // die(var_dump($fileModel->errors));
//					}
//						  $funcNum = $_GET['CKEditorFuncNum'] ;
//echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
//				
//		}
//
//		   
//		}
	
		public function actionUpload($CKEditor=null,$CKEditorFuncNum=NULL,$langCode=NULL) {
		$message='';
		$url='';
		$fileModel=new File;
		$fileModel->scenario=  File::MODEL_FILE_UPLOAD_SCENARIO;
		$fileModel->modelFileTyps='doc,docx,jpeg,png,pdf,jpg';
		$fileModel->modelMimeFileTyps=['application/pdf,image/jpg','image/jpeg','image/png'];
		
		$model = new UserFolders();
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$userFoderId = $model->getUserSpecialUserFolderId($userId,UserFolders::User_CONTENT_FOLDER);

		if(!isset($userFoderId)||!$model->userCheckAccessToFolder($userId, $userFoderId))
		{
			 $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
		}
 
	  
		$massage = '';
	   if (isset($_POST)) {
				
				/*----------Save Main Pic & thumbNail Pic in Category & Category_timenail Folder ------------*/
			   $fileModel->folder_id =$userFoderId;
			   $fileModel->model_id =2;
			   $fileModel->model_type ='Content-Upload';
			   $fileModel->temporaryFileAttr =  \yii\web\UploadedFile::getInstanceByName('upload');
			   
			   if ($fileModel->validate()&&$id=$fileModel->uploadModelFile()) {

					$folderModel = new Folders;
					$destination = $folderModel->ProvideRealFolderPath(Folders::CATEGORIES_FOLDER_ID) . '/' . $fileModel->name . '.' . $fileModel->ext;
					$url = Url::to([ '/YiiFileManager/file/view-file', 'name' => $fileModel->hashed_name]);
					   
		 
					}else
					{
						if(!empty($fileModel->errors))
						{
							foreach ($fileModel->errors as $key => $error) {
								foreach ($error as $key => $value) {
									 $message.=$value;
								}
								
							}
						}  else {
								  $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
						}

					   // die(var_dump($fileModel->errors));
					}
						  $funcNum = $_GET['CKEditorFuncNum'] ;
echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
				
		}

		   
		}

}
