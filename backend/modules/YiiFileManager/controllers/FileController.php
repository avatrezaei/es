<?php

namespace backend\modules\YiiFileManager\controllers;

use Yii;
use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\UserFolders;
use backend\modules\YiiFileManager\models\FileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\filters\AccessControl;

/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller {

	public function behaviors() {
		return [
//			'access' => [
//				'class' => AccessControl::className(),
//					'only' => ['FileUpload', 'GetFile', 'ViewFile'],
//				'rules' => [
//
//					[
//						'actions' => ['FileUpload', 'GetFile', 'ViewFile'],
//						'allow' => true,
//						'roles' => ['admin'],
//					],
//				],
//			],
//			'verbs' => [
//				'class' => VerbFilter::className(),
//				'actions' => [
//					'delete' => ['post'],
//				],
//			],
		];
	}

	public function actionFileUpload() {
		if (isset($_POST)) {
			$fileInstanceArray = UploadedFile::getInstancesByName('allAllowedFileType');
			if (isset($fileInstanceArray) && is_array($fileInstanceArray)) {
				foreach ($fileInstanceArray as $fileInstance) {
					$fileModel = new File;
					$fileModel->scenario = 'file_manager_upload';
					$fileModel->folder_id = $_POST['folder_id'];

					$fileModel->allAllowedFileType = $fileInstance; //$fileInstance is object that output of UploadedFile::getInstancesByName
					if ($fileModel->validate()) {
						if (!$fileModel->uploadFile()) {
							Yii::$app->response->format = Response::FORMAT_JSON;

							return[
								'success' => TRUE,
								'messages' => Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'Folder')))]
							;
							//die(var_dump($fileModel->errors));
						} else {
							return TRUE;
						}
					} else {

						$errMsg = '';
						foreach ($fileModel->errors as $key => $attributeErr) {
							foreach ($attributeErr as $key => $error)
								$errMsg.=$error;
						}
						$output = ['error' => $errMsg];

						echo json_encode($output);
						return;
					}
				}
			} else {
				$output = ['error' => Yii::t('app', 'Bad Request')];

				echo json_encode($output);
				return;
			}
		} else {
			$output = ['error' => Yii::t('app', 'Bad Request')];

			echo json_encode($output);
			return;
		}
		$output = ['error' => Yii::t('app', 'Please Select File Again')];

		echo json_encode($output);
	}

		
	 public function actionUserFileUpload() {
		if (isset($_POST)) {

	/* -----------------Folder Access Control-------------- */
		$folderID=isset($_POST['folder_id'])?($_POST['folder_id']):NULLl;
		$model = $this->loadFolderModel($folderID);
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		if (!$model->userCheckAccessToFolder($userId, $folderID)) {
			 $output = ['error' =>  Yii::t('app', 'You do not have access to the desired path')];

						echo json_encode($output);
						return;
	  
		}

	/* ---------------------------------------------------- */  
		
		$fileInstanceArray = UploadedFile::getInstancesByName('allAllowedFileType');
		if (isset($fileInstanceArray) && is_array($fileInstanceArray)) {
			foreach ($fileInstanceArray as $fileInstance) {
				$fileModel = new File;
				$fileModel->scenario = 'file_manager_upload';
				$fileModel->folder_id = $_POST['folder_id'];

				$fileModel->allAllowedFileType = $fileInstance; //$fileInstance is object that output of UploadedFile::getInstancesByName
				if ($fileModel->validate()) {
					if (!$fileModel->uploadFile()) {
						Yii::$app->response->format = Response::FORMAT_JSON;

						return[
							'success' => TRUE,
							'messages' => Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'Folder')))]
						;
						//die(var_dump($fileModel->errors));
					} else {
						return TRUE;
					}
				} else {

					$errMsg = '';
					foreach ($fileModel->errors as $key => $attributeErr) {
						foreach ($attributeErr as $key => $error)
							$errMsg.=$error;
					}
					$output = ['error' => $errMsg];

					echo json_encode($output);
					return;
				}
				}
			} else {
				$output = ['error' => Yii::t('app', 'Bad Request')];

				echo json_encode($output);
				return;
			}
		} else {
			$output = ['error' => Yii::t('app', 'Bad Request')];

			echo json_encode($output);
			return;
		}
		$output = ['error' => Yii::t('app', 'Please Select File Again')];

		echo json_encode($output);
	}
	
	
	public function actionGetFile($name) {

		if (isset($name) && ($fileActonModel = $this->findModelFileActionByHashedName($name)) != NULL) {
			$fileActonModel->getFile();
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

//	public function actionGetFile($id) {
//		if (isset($id) && is_numeric($id) && ($fileActonModel = $this->findModelFileAction($id)) != NULL) {
//			$fileActonModel->getFile();
//		} else {
//			throw new NotFoundHttpException('The requested page does not exist.');
//		}
//	}


	
	  public function actionViewFile($name) {
		if (isset($name) && ($fileActonModel = $this->findModelFileActionByHashedName($name)) != NULL) {
			$fileActonModel->viewFile();
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}  
	
	

	public function actionDelete($name) {
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {


			$model = $this->findModelFileActionByHashedName($name);
			if ($model->removeFile()) {

				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => true,
					'messages' => Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'File')])]
				;
			} else {
				if (is_array($model->errors)) {
					$msg = '';
					foreach ($model->errors as $itemerror) {
						foreach ($itemerror as $error)
							$msg .=$error . '-';
					}
				} else {
					$msg = Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')]);
				}
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => false,
					'messages' => $msg]
				;
			}
		}
		Yii::$app->response->format = Response::FORMAT_JSON;
		return[
			'success' => false,
			'messages' => Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')])]
		;
	}

	
	/**
	 * Updates an existing File model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */

	/**
	 * Finds the File model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return File the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = File::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findModelFileAction($id) {
		if (($model = \backend\modules\YiiFileManager\models\FileAction::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException(Yii::t('app', 'The File does not exist.'));
		}
	}

	protected function findModelFileActionByHashedName($hashedName) {
		$model = \backend\modules\YiiFileManager\models\FileAction::findBySql('SELECT * FROM file WHERE hashed_name=:hashed_name', array(':hashed_name' => $hashedName))->one();
		//$model=\backend\modules\YiiFileManager\models\FileAction::find()->where(['hashed_name' => $hashedName])->one();
		if ($model != null) {
			return $model;
		} else {
			throw new NotFoundHttpException(Yii::t('app', 'The File does not exist.'));
		}
	}

/**
	 * Creates a new File model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
//	public function actionCreate() {
//		$model = new File();
//		// die(var_dump($_POST));
//
//		if ($model->load(Yii::$app->request->post())) {
//			//if (isset($_POST['File'])) {
//			// get the uploaded file instance. for multiple file uploads
//			// the following data will return an array
//			$model->allAllowedFileType = UploadedFile::getInstance($model, 'allAllowedFileType');
//			//die(var_dump($model->allAllowedFileType));
//			// $model->allAllowedFileType=$_POST['File']['allAllowedFileType'];
//			// store the source file name
//			$model->name = $model->allAllowedFileType->name;
//			$ext = end((explode(".", $model->allAllowedFileType->name)));
//			// die(var_dump($model->allAllowedFileType));
//			// generate a unique file name
//			$avatar = Yii::$app->security->generateRandomString() . ".{$ext}";
//
//			// die(var_dump($model->errors));
//			$path = 'c:wamp/test/' . $avatar;
//			if ($model->validate()) {
//				$model->allAllowedFileType->saveAs($path);
//				//die(var_dump($model->errors));
//			} else {
//				die(var_dump($model->errors));
//			}
//		}
//		return $this->render('create', [
//					'model' => $model,
//		]);
//	}
	
	
	
	
//	public function acionBrowse($CKEditor,$CKEditorFuncNum,$langCode) {
//		die('ppppp');
//		return $this->render('browse');
//		  }
	
	 
		public function loadFolderModel($id) {
		$model = UserFolders::findOne($id);
		if ($model === null) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder does not exist.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}
		if (empty($model->valid) || $model->valid == 0) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder was deleted.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}

		return $model;
	}




















}
