<?php

namespace backend\modules\YiiFileManager\controllers;

use Yii;
use backend\modules\YiiFileManager\models\File;
use \backend\modules\YiiFileManager\models\Folders;
use \backend\modules\YiiFileManager\models\FileAction;
use backend\modules\YiiFileManager\models\FileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Html;
use \backend\modules\YiiFileManager\models\UserFolders;

/**
 * FileController implements the CRUD actions for File model.
 */
class UserFileManagerBulkActionsController extends Controller {

	public $_allErrors = array();

	const MAX_BULK_ITEM = 10;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['Delete'],
				'rules' => [
					[
						'actions' => ['Delete'],
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
//			'verbs' => [
//				'class' => VerbFilter::className(),
//				'actions' => [
//					'delete' => ['post'],
//				],
//			],
		];
	}

	/*
	 * Delete group file or folder that selected in checkbox row in grid
	 */

	public function actionDelete() {
		$post = Yii::$app->request->post();
		$allErrors = array();
		if (Yii::$app->request->isAjax) {
			$selectionArray = isset($_POST['selection']) ? $_POST['selection'] : array();
			$folderIdArray = array();
			$fileHashedNameArray = array();
			if (count($selectionArray) == 0) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => false,
					'messages' => Yii::t('app', Yii::t('app', 'Please Select Options.'))]
				;
			} else if (count($selectionArray) > self::MAX_BULK_ITEM) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => false,
					'messages' => Yii::t('app', 'Please Select less than {item} item.', ['item' => self::MAX_BULK_ITEM])]

				;
			}

			foreach ($selectionArray as $key => $value) {
				/*
				 * item is file_id or folder_id that id is numeric such file_34 or folder_4
				 */
				$item = explode("_", $value);
				if ($item[0] == 'file') {
					$fileHashedNameArray[] = $item[1];
				} else if ($item[0] == 'folder') {
					$folderIdArray[] = (int) $item[1];
				}
			}
			/*
			 * First Delete Files base on hashed name by check access control
			 */
			$this->deleteGroupFilesByHashedName($fileHashedNameArray);
			/*
			 * secound Delete Folders base on id 
			 */
			$this->deleteGroupFolderById($folderIdArray);

			if (empty($this->_allErrors)) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => true,
					'messages' => Yii::t('app', 'items Successfully Deleted.')]
				;
			} else {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => false,
					'messages' => $this->_allErrors]
				;
			}
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		return[
			'success' => false,
			'messages' => Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')])]
		;
	}

	public function deleteGroupFolderById($folderIdArray) {
		
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		foreach ($folderIdArray as $key => $id) {
			$model = UserFolders:: findOne($id);
			if ($model == null) {
				$this->_allErrors['notfoundfolder'][] = Yii::t('app', 'Not found selected folder');
			} else if ($model != NULL) {
				$parent_id = $model->parent_id;
				/* -----------------Folder Access Control-------------- */
				if ($model->userCheckAccessToFolder($userId, $model->id)) {
					
					if (!$model->removeDir() || !$model->delete()) {
						if (is_array($model->errors)) {
							$msg = '<br>' . Yii::t('app', 'Folder') . ' ' . Html::encode($model->name) . Yii::t('app', 'has errors:') . '<br>';
							foreach ($model->errors as $itemerror) {
								foreach ($itemerror as $error)
									$msg .=$error . '-';
							}
						}
						else 
						{
							$msg = Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')]);
						}
						$this->_allErrors['deletefoldererror'][] = $msg;
					}
					
				} 
				else
				{
					$this->_allErrors['notfoundfolder'][] = Yii::t('app', 'Not found selected folder');
				}
			}
		}
		return TRUE;
	}

	public function deleteGroupFilesByHashedName($fileHashedNameArray) {
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		foreach ($fileHashedNameArray as $key => $hashedName) {
			$model = FileAction::findBySql('SELECT * FROM file WHERE hashed_name=:hashed_name', array(':hashed_name' => $hashedName))->one();
			if ($model == null) {
				$this->_allErrors['notfoundfiles'][] = Yii::t('app', 'Not found selected file');
			} else if ($model != NULL) {
				/* -------------Access Control---------- */
				if ($model->userCheckAccessToFile($userId, $model->id)) {
					if (!$model->removeFile()) {
						if (is_array($model->errors)) {
							$msg = '<br>' . Yii::t('app', 'File') . ' ' . Html::encode($model->name) . Yii::t('app', 'has errors:') . '<br>';
							foreach ($model->errors as $itemerror) {
								foreach ($itemerror as $error)
									$msg .=$error . '-';
							}
						} else {
							$msg = Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')]);
						}
						$this->_allErrors['deletefileerror'][] = $msg;
					}
				} else {
					$this->_allErrors['notfoundfiles'][] = Yii::t('app', 'You do not have access to the desired file');
				}
			}
		}
		return TRUE;
	}

	protected function findModel($id) {
		if (($model = File::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findModelFileAction($id) {
		if (($model = \backend\modules\YiiFileManager\models\FileAction::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException(Yii::t('app', 'The File does not exist.'));
		}
	}

	protected function findModelFileActionByHashedName($hashedName) {
		$model = FileAction::findBySql('SELECT * FROM file WHERE hashed_name=:hashed_name', array(':hashed_name' => $hashedName))->one();
		//$model=\backend\modules\YiiFileManager\models\FileAction::find()->where(['hashed_name' => $hashedName])->one();
		if ($model != null) {
			return $model;
		} else {
			throw new NotFoundHttpException(Yii::t('app', 'The File does not exist.'));
		}
	}

	public function loadFolderModel($id) {
		$model = Folders::findOne($id);
		if ($model === null) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder does not exist.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}
		if (empty($model->valid) || $model->valid == 0) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder was deleted.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}

		return $model;
	}

}
