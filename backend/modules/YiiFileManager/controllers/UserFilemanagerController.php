<?php

namespace backend\modules\YiiFileManager\controllers;

use Yii;
use backend\modules\YiiFileManager\models\UserFolders;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FoldersSearch;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\web\Session;
use yii\web\Response;

/**
 * FoldersController implements the CRUD actions for Folders model.
 */
class UserFilemanagerController extends Controller {

	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function actionIndex($id = NULL) {
		$model = new UserFolders();
		$model->scenario = Folders::SCENARIO_MKDIR;
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

		/* ------validate is numeric value------------ */
		if ($id == NULL || !is_numeric($id)) {
			/* -----set user folder root------------ */
			$id = $model->getUserRootFolderId($userId);
		} else if (is_numeric($id))/* ------access control check---------- */ {

			if (!$model->userCheckAccessToFolder($userId, $id)) {


				$msg = '<div class="alert alert-danger" id="flash_error" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'You do not have access to the desired path') . '</a>
					</div>';
				$session = Yii::$app->session;
				$session->setFlash('security_error_authority', $msg);
				$id = $model->getUserRootFolderId($userId);

				return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
			}
		}

		$massage = '';

		/* ----currentFolderId be onvan parent mahsoob mishavad ham baraye folder va file ke 
		  ----- mikhahad ijad shavad ----- */
		$model->currentFolderId = $id;

		$model->is_user_root_dir = 0;


		$searchModel = new FoldersSearch();
		$searchModel->currentFolderId = $model->currentFolderId;
		$searchModel->user_id = $userId;
		$dataProvider = $searchModel->searchByUser(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
					'model' => $model,
					'massage' => $massage,
		]);
	}


	public function actionNewfolder() {
		$model = new UserFolders();
		$model->scenario = Folders::SCENARIO_MKDIR;
		/* ------validate is numeric value------------ */
		$currentId = isset($_POST['UserFolders']['currentFolderId']) ? $_POST['UserFolders']['currentFolderId'] : NULL;
		if ($currentId == NULL || !is_numeric($currentId) || $this->loadFolderModel($currentId) == NULL) {
			$model->addError('name', Yii::t('app', 'bad request. please try again'));
		}
		/* -----------------Access Control-------------- */
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$userId=75;
		if (!$model->userCheckAccessToFolder($userId, $currentId)) {
			Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => FALSE,
					'errors' =>['error'=>[0=>Yii::t('app','You do not have access to the desired path')]]]
				;
		}
		/* --------------------------------------------- */
		$massage = '';
		/* ----currentFolderId be onvan parent mahsoob mishavad ham baraye folder va file ke 
		  ----- mikhahad ijad shavad ----- */
		$model->currentFolderId = $currentId;
		$model->is_user_root_dir = 0;
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save()) {
				/*				 * ******unset attributes to show empty after create item*********** */
				foreach ($model->attributes as $key => $value) {
					unset($model->$key);
				}
				/* --------------------------------------------- */
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => TRUE,
					'messages' => Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'Folder')))]
				;
			} else {
				$message = '';
				if (!is_array($model->errors) && empty($model->errors)) {
					$message = Yii::t('app', 'Failed To Add Items To {item}.', ['item' => Yii::t('app', 'Folder')]);
				}

				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => FALSE,
					'messages' => $message,
					'errors' => $model->errors]
				;
			}
		} else {
			$message = Yii::t('app', 'Failed To Add Items To {item}.', ['item' => Yii::t('app', 'Folder')]);
			Yii::$app->response->format = Response::FORMAT_JSON;
			return[
				'success' => FALSE,
				'messages' => $message,
				'errors' => $model->errors]
			;
		}
	}

	public function actionRmDir($id) {
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {
			if (isset($post['id']))
				$id = (int) $post['id'];
			else if (isset($_GET['id']))
				$id = (int) $_GET['id'];
			
		$model = $this->loadFolderModel($id);

		/* -----------------Folder Access Control-------------- */
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		if (!$model->userCheckAccessToFolder($userId, $id)) {
			$msg = '<div class="alert alert-danger" id="flash_error" role="alert" >
						<a href="#" class="alert-link">' . Yii::t('app', 'You do not have access to the desired path') . '</a>
						</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$rootFolderid = $model->getUserRootFolderId($userId);
			return $this->redirect(['/YiiFileManager/user-filemanager/index']);
		}
		/* --------------------------------------------- */  
			$parent_id = $model->parent_id;
			if ($model->removeDir()) {
				// die(var_dump($model));

				if ($model->delete()) {

					Yii::$app->response->format = Response::FORMAT_JSON;
					return[
						'success' => true,
						'messages' => Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Folder')])]
					;
				} else {
					Yii::$app->response->format = Response::FORMAT_JSON;
					return[
						'success' => false,
						'messages' => Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')])]
					;
				}
			} else {
				if (is_array($model->errors)) {
					$msg = '';
					foreach ($model->errors as $itemerror) {
						foreach ($itemerror as $error)
							$msg .=$error . '-';
					}
				} else {
					$msg = Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')]);
				}
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => false,
					'messages' => $msg]
				;

			}
		}
							Yii::$app->response->format = Response::FORMAT_JSON;
					return[
						'success' => false,
						'messages' => Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Folder')])]
					;
	}



   
	
	 public function actionGetFile($name) {
		if (isset($name) && ($fileActonModel = $this->findModelFileActionByHashedName($name)) != NULL) {
		/* -----------------File Access Control-------------- */
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0; 
		if (!$fileActonModel->userCheckAccessToFile($userId, $fileActonModel->id)) {
			$msg = '<div class="alert alert-danger" id="flash_error" role="alert" >
						<a href="#" class="alert-link">' . Yii::t('app', 'You do not have access to the desired file') . '</a>
						</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$rootFolderid = $fileActonModel->getUserRootFolderId($userId);
			return $this->redirect(['/YiiFileManager/user-filemanager/index']);
			}else
			{
			$fileActonModel->getFile();
			}
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


	public function actionViewFile($name) {
		  if (isset($name) && ($fileActonModel = $this->findModelFileActionByHashedName($name)) != NULL) {
		/* -----------------File Access Control-------------- */
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0; 
		if (!$fileActonModel->userCheckAccessToFile($userId, $fileActonModel->id)) {
			$msg = '<div class="alert alert-danger" id="flash_error" role="alert" >
						<a href="#" class="alert-link">' . Yii::t('app', 'You do not have access to the desired file') . '</a>
						</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$rootFolderid = $fileActonModel->getUserRootFolderId($userId);
			return $this->redirect(['/YiiFileManager/user-filemanager/index']);
			}else
			{
			$fileActonModel->viewFile();
			}
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	
		public function actionViewTumbnailFile($name) {
		  if (isset($name) && ($fileActonModel = $this->findModelFileActionByHashedName($name)) != NULL) {
		/* -----------------File Access Control-------------- */
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0; 
		if (!$fileActonModel->userCheckAccessToFile($userId, $fileActonModel->id)) {
			$msg = '<div class="alert alert-danger" id="flash_error" role="alert" >
						<a href="#" class="alert-link">' . Yii::t('app', 'You do not have access to the desired file') . '</a>
						</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$rootFolderid = $fileActonModel->getUserRootFolderId($userId);
			return $this->redirect(['/YiiFileManager/user-filemanager/index']);
			}else
			{
			$fileActonModel->viewTumbnailFile();
			}
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	
	
	
	
	
	public function actionDeleteFile($name) {
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {
			$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0; 
			$model = $this->findModelFileActionByHashedName($name);
			if (!$model->userCheckAccessToFile($userId, $model->id)) {
		
			   Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => false,
					'messages' =>Yii::t('app', 'You do not have access to the desired file')]
				;			
			}	 
			if ($model->removeFile()) {

				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => true,
					'messages' => Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'File')])]
				;
			} else {
				if (is_array($model->errors)) {
					$msg = '';
					foreach ($model->errors as $itemerror) {
						foreach ($itemerror as $error)
							$msg .=$error . '-';
					}
				} else {
					$msg = Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')]);
				}
				Yii::$app->response->format = Response::FORMAT_JSON;
				return[
					'success' => false,
					'messages' => $msg]
				;
			}
		}
		Yii::$app->response->format = Response::FORMAT_JSON;
		return[
			'success' => false,
			'messages' => Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'File')])]
		;
	}

	/*-----------------------------*/
	protected function findModel($id) {
		if (($model = Folders::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function loadFolderModel($id) {
		$model = UserFolders::findOne($id);
		if ($model === null) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder does not exist.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}
		if (empty($model->valid) || $model->valid == 0) {
			$msg = '<div class="alert alert-danger" role="alert" >
					<a href="#" class="alert-link">' . Yii::t('app', 'The Folder was deleted.') . '</a>
					</div>';
			$session = Yii::$app->session;
			$session->setFlash('security_error_authority', $msg);
			$id = 1;
			return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
		}

		return $model;
	}
		protected function findModelFileActionByHashedName($hashedName) {
		$model = \backend\modules\YiiFileManager\models\FileAction::findBySql('SELECT * FROM file WHERE hashed_name=:hashed_name', array(':hashed_name' => $hashedName))->one();
		//$model=\backend\modules\YiiFileManager\models\FileAction::find()->where(['hashed_name' => $hashedName])->one();
		if ($model != null) {
			return $model;
		} else {
			throw new NotFoundHttpException(Yii::t('app', 'The File does not exist.'));
		}
	}


//	 public function actionIndex($id=NULL) {
//		$model = new UserFolders();
//		$model->scenario=  Folders::SCENARIO_MKDIR;
//		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
//
//		
//
//		/*------validate is numeric value------------*/
//		if($id==NULL||!is_numeric($id))
//		{
//			/*-----set user folder root------------*/
//			$id=$model->getUserRootFolderId($userId);
//		   
//		}  else if(is_numeric($id))/*------access control check----------*/
//		{
//
//			if(!$model->userCheckAccessToFolder($userId,$id))
//			{  
//				
//				
//			$msg = '<div class="alert alert-danger" role="alert" >
//					<a href="#" class="alert-link">' . Yii::t('app',  'You do not have access to the desired path') . '</a>
//					</div>';
//					  $session = Yii::$app->session;
//			$session->setFlash('security_error_authority', $msg);   
//				$id=$model->getUserRootFolderId($userId);
//
//					return $this->redirect(['/YiiFileManager/user-filemanager/index', 'id' => $id]);
//		   }
//		}
//
//		$massage = '';
//		$model->currentFolderId = $id;
//		$model->is_user_root_dir=0;
//	   if ($model->load(Yii::$app->request->post())) {
//			if ($model->save()) {
//				/*********unset attributes to show empty after create item************/
//				foreach ($model->attributes as $key => $value) {
//					unset($model->$key);
//				}
//				/************************************/
//
//				$message = '<div class="alert alert-success" role="alert" id="form_create_msg">
//					<a href="#" class="alert-link">' . Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'Folder'))) . '</a>
//					</div>';
//			} else {
//				$model->addError('name', Yii::t('app', 'Failed To Add Items To {item}.', ['item' => Yii::t('app', 'Folder')]));
//			}
//		}
//
//		$searchModel = new FoldersSearch();
//		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//		return $this->render('index', [
//					'searchModel' => $searchModel,
//					'dataProvider' => $dataProvider,
//					'model' => $model,
//					'message' => $message,
//		]);
//	}
	
		}
