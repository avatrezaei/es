<?php

return array(
/********************Foldesr********************/
	'Folder'=>'شاخه',
	'Description'=>'توضیحات',
	/************************************** */
	/*****************Buttoms************** */
	'Create' => 'ایجاد',
	'Delete' => 'حذف',
	'Close'=>'بستن',
	'View' => 'مشاهده',
	'Reset Grid' => 'به روز رسانی ',
	'Select a Language' => 'انتخاب زبان...',
	'Select a Sicence Level' => 'انتخاب سطح علمی...',
	'Select a Country' => 'انتخاب کشور ...',
	'Select a Grade' => 'انتخاب مقطع تحصیلی ...',
	'Select a ..' => 'انتخاب  ...',
	'Extera Info' => 'اطلاعات بیشتر',
	'Add Child' => 'افزودن مورد',
	'Delete' => 'حذف',
	'New'=>'جدید',
	'Preview'=>'پیش نمایش',
	'Users Management'=>'مدیریت کاربران',
	/************************************** */
	/********Country attibutes************** */
	'English Title' => 'عنوان انگلیسی',
	'Persian  Title' => 'عنوان فارسی',
	'Content' => 'محتوا',
	'Username' => 'نام کاربری',
	/********Experience attibutes************** */
	'Subject' => 'موضوع',
	'Position' => 'وضعیت',
	'Item' => 'موارد',	
		'Experience Items' => ' آیتم تجربیات',
	'Exp ID'=>'شناسه تجربیات',

	/*******Academic Background Attribute********** */
	'Supervisor' => 'ناظر',
	'From Date' => 'از تاریخ',
	'To Date' => 'تا تاریخ',
	'Theses Title' => 'عنوان پایان نامه ',
	'Science Level' => 'سطح علمی',
	'City' => 'شهر',
	/*********Setting Attributes**************** */
	'Category' => 'دسته',
	'Key' => 'عنوان تنظیم',
	'Value' => 'مقدار',
	'Description' => 'توضیحات',
	/*********Common Attributes**************** */
	 'ID' => 'شناسه',
	'Title' => 'عنوان',
	'Date' => 'تاریخ',
	'Description' => 'توضیحات',
	'Lang' => 'زبان',
	'Created Date Time' => 'تاریخ ایجاد',
	'Modified Date Time' => 'تاریخ ویرایش',
	'User ID' => ' کاربر',
	'Username' => 'نام کاربری',
	'Password' => 'کلمه عبور',
	'Salt' => 'Salt',
	'Name' => 'نام',
	'Lock' => 'مسدود',
	'Active' => 'فعال',
	'Valid' => 'معتبر',
	'Role' => 'سطح دسترسی',

	/********************User Attribute***********************/
	'ID' => 'شناسه',
'Username' => 'نام کاربری',
 'Password' => 'کلمه عبور',
 'passwordInput' => 'کلمه عبور',
 'Salt' => 'Salt',
 'Name' => 'نام',
 'Last Name' => 'نام خانوادگی',
 'Office' => 'سمت',
 
 'Update Date' => 'تاریخ ویرایش',
 'Update At' => 'تاریخ ویرایش',
 'Create Date' => 'تاریخ ایجاد ',
 'Create At' => 'تاریخ ایجاد ',
 'Last Login Time' => 'زمان آخرین ورود',
 'Last Login Attemp Time' => 'زمان آخرین تلاش ورود',
 'Last Action' => 'آخرین فعالیت',
 'Lock' => 'مسدود',
 'Active' => 'فعال',
 'Valid' => 'معتبر',
 'Role'=>'سطح دسترسی',
 'Captcha'=>'تصویر امنیتی',

	/*****************************************/
	/****************** messages*************/
	'ADD New {item}' => 'افزودن {item} جدید',
	'{item} Successfully Added.' => '{item} با موفقیت اضافه شد',
	'{item} Successfully Deleted.' => '{item} با موفقیت حذف شد',
	'{item} Successfully Edited.' => '{item} با موفقیت ویرایش شد',
	'Failed To Add Items To {item}.' => 'عملیات اضافه کردن {item} با مشکل مواجه شده است.',
	'Failed To Edit Items To {item}.' => 'عملیات ویرایش کردن {item} با مشکل مواجه شده است.',
	'Failed To Delete Items To {item}.' => 'عملیات حذف کردن {item} با مشکل مواجه شده است.',
	
	'Are you sure you want to delete this item?' => 'آیا از حذف این آیتم اطمینان دارید؟',
	'The {item} # {id} was successfully deleted.{url} to proceed.' => '{item} شماره با موفقیت حذف شد',
	'Error occured while saving children.'=>'در هنگام افزودن آیتم ها خطا رخ داده است.',
	'You are only allowed to define {num} {item}'=>'شما تنها مجاز به تعریف  {num}  {item} می باشید',
/**********************************************/
	'You entered an invalid date format.'=>'فرمت تاریخ صحیح نمی باشد.',
	'Start date must be proportionate to the end date'=>'تاریخ شروع باید متناسب با تاریخ پایان باشد',
	
		'Select Language'=>'انتخاب زبان',
	'Persian'=>'فارسی',
	'English'=>'انگلیسی',
	'Current Language'=>'فارسی',		
	'Dashboard'=>'داشبورد',
	'Search'=>'جستجو',
	'English'=>'انگلیسی',
	'Sign out'=>'خروج',
	'Sing up'=>'ثبت نام',
	'Confirm'=>'تایید',
	
/*********************************************/
	'Home Page'=>'سیستم صفحات خانگی',
			'Actions'=>'عملیات',
   /*------------------Error message----------------------------*/
   );
