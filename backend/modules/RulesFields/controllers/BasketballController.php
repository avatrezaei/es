<?php

namespace backend\modules\RulesFields\controllers;

use Yii;
use backend\modules\RulesFields\models\Basketball;
use backend\modules\RulesFields\models\BasketballSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use \backend\models\Rules;
use \backend\models\RulesSearch;

/**
 * BasketballController implements the CRUD actions for Basketball model.
 */
class BasketballController extends Controller {
	public $layout = '@app/views/layouts/column2';
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'post' 
								],
								'bulk-delete' => [ 
										'post' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Basketball models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new BasketballSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Basketball model.
	 *
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionView($id) {
		$request = Yii::$app->request;
		if ($request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return [ 
					'title' => "Basketball #" . $id,
					'content' => $this->renderPartial ( 'view', [ 
							'model' => $this->findModel ( $id ) 
					] ),
					'footer' => Html::button ( 'Close', [ 
							'class' => 'btn btn-default pull-left',
							'data-dismiss' => "modal" 
					] ) . Html::a ( 'Edit', [ 
							'update',
							'id' => $id 
					], [ 
							'class' => 'btn btn-primary',
							'role' => 'modal-remote' 
					] ) 
			];
		} else {
			return $this->render ( 'view', [ 
					'model' => $this->findModel ( $id ) 
			] );
		}
	}
	
	/**
	 * Creates a new Basketball model.
	 * For ajax request will return json object
	 * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate() {
		$_Rule_Id = 0;
		$eventId = 0;
		$fieldsId = 0;
		$subFieldsId = 0;
		// ////////////////////////////////////////
		
		// ////////////////////////////////////////
		$model = new Basketball ();
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			
			\backend\models\Rules::Save_SpecialSEMS ( new Basketball (), $model, $_FILES );
		}
		if (! isset ( $_POST ["Basketball"] )) {
			if (isset ( $_REQUEST ['Rule_Id'] ))
				$_Rule_Id = ( int ) $_REQUEST ["Rule_Id"];
			else {
				$this->redirect ( [ 
						'/rules/index' 
				] );
			}
			// ///////////////////////////////////
			$rules = Rules::findOne ( $_Rule_Id );
			if (is_null ( $rules )) {
				$this->redirect ( [ 
						'/rules/index' 
				] );
			} else {
				$eventId = $rules->eventId;
				$fieldsId = $rules->fieldsId;
				$subFieldsId = $rules->subFieldsId;
			}
			
			$model->eventId = $eventId;
			$model->fieldsId = $fieldsId;
			$model->subFieldsId = $subFieldsId;
			$model = $this->getValues ( $model );
		}
		
		return $this->render ( 'create', [ 
				'model' => $model 
		] );
	}
	
	/**
	 * Updates an existing Basketball model.
	 * For ajax request will return json object
	 * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$request = Yii::$app->request;
		$model = $this->findModel ( $id );
		
		if ($request->isAjax) {
			/*
			 * Process for ajax request
			 */
			Yii::$app->response->format = Response::FORMAT_JSON;
			if ($request->isGet) {
				return [ 
						'title' => "Update Basketball #" . $id,
						'content' => $this->renderPartial ( 'update', [ 
								'model' => $this->findModel ( $id ) 
						] ),
						'footer' => Html::button ( 'Close', [ 
								'class' => 'btn btn-default pull-left',
								'data-dismiss' => "modal" 
						] ) . Html::button ( 'Save', [ 
								'class' => 'btn btn-primary',
								'type' => "submit" 
						] ) 
				];
			} else if ($model->load ( $request->post () ) && $model->save ()) {
				return [ 
						'forceReload' => 'true',
						'title' => "Basketball #" . $id,
						'content' => $this->renderPartial ( 'view', [ 
								'model' => $this->findModel ( $id ) 
						] ),
						'footer' => Html::button ( 'Close', [ 
								'class' => 'btn btn-default pull-left',
								'data-dismiss' => "modal" 
						] ) . Html::a ( 'Edit', [ 
								'update',
								'id' => $id 
						], [ 
								'class' => 'btn btn-primary',
								'role' => 'modal-remote' 
						] ) 
				];
			} else {
				return [ 
						'title' => "Update Basketball #" . $id,
						'content' => $this->renderPartial ( 'update', [ 
								'model' => $this->findModel ( $id ) 
						] ),
						'footer' => Html::button ( 'Close', [ 
								'class' => 'btn btn-default pull-left',
								'data-dismiss' => "modal" 
						] ) . Html::button ( 'Save', [ 
								'class' => 'btn btn-primary',
								'type' => "submit" 
						] ) 
				];
			}
		} else {
			/*
			 * Process for non-ajax request
			 */
			if ($model->load ( $request->post () ) && $model->save ()) {
				return $this->redirect ( [ 
						'view',
						'id' => $model->id 
				] );
			} else {
				return $this->render ( 'update', [ 
						'model' => $model 
				] );
			}
		}
	}
	
	/**
	 * Delete an existing Basketball model.
	 * For ajax request will return json object
	 * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionDelete($id) {
		$request = Yii::$app->request;
		$this->findModel ( $id )->delete ();
		
		if ($request->isAjax) {
			/*
			 * Process for ajax request
			 */
			Yii::$app->response->format = Response::FORMAT_JSON;
			return [ 
					'forceClose' => true,
					'forceReload' => true 
			];
		} else {
			/*
			 * Process for non-ajax request
			 */
			return $this->redirect ( [ 
					'index' 
			] );
		}
	}
	
	/**
	 * Delete multiple existing Basketball model.
	 * For ajax request will return json object
	 * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionBulkDelete() {
		$request = Yii::$app->request;
		$pks = $request->post ( 'pks' ); // Array or selected records primary keys
		foreach ( Basketball::findAll ( json_decode ( $pks ) ) as $model ) {
			$model->delete ();
		}
		
		if ($request->isAjax) {
			/*
			 * Process for ajax request
			 */
			Yii::$app->response->format = Response::FORMAT_JSON;
			return [ 
					'forceClose' => true,
					'forceReload' => true 
			];
		} else {
			/*
			 * Process for non-ajax request
			 */
			return $this->redirect ( [ 
					'index' 
			] );
		}
	}
	
	/**
	 * Finds the Basketball model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id			
	 * @return Basketball the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Basketball::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	/**
	 * *
	 */
	private function getValues($model) {
		$eventId = $model->eventId;
		$fieldsId = $model->fieldsId;
		$subFieldsId = $model->subFieldsId;

		$_rules = Rules::getValues ($model->fieldsId, $model->subFieldsId );
		$model = new Basketball ();
		$model->eventId = $eventId;
		$model->fieldsId = $fieldsId;
		$model->subFieldsId = $subFieldsId;
		foreach ( $_rules as $_rule ) {
			$_atr = trim ( $_rule->name );
			$_val = trim ( $_rule->value );
			try {
				$model->$_atr = $_val;
			} catch ( \Exception $e ) {
			}
		}
		return $model;
	}
	
}
