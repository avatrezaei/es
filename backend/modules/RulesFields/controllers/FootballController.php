<?php

namespace backend\modules\RulesFields\controllers;

use Yii;
use backend\modules\RulesFields\models\Football;
use backend\modules\RulesFields\models\FootballSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \backend\models\Rules;
use \backend\models\RulesSearch;

/**
 * FootballController implements the CRUD actions for Football model.
 */
class FootballController extends Controller {
	public $layout = '@app/views/layouts/column2';
	public function behaviors() {
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'post' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Football models.
	 * 
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new FootballSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Football model.
	 * 
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render ( 'view', [ 
				'model' => $this->findModel ( $id ) 
		] );
	}
	
	/**
	 * Creates a new Football model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @return mixed
	 */
	public function actionCreate() {
		$_Rule_Id = 0;
		$eventId = 0;
		$fieldsId = 0;
		$subFieldsId = 0;
		// ////////////////////////////////////////
		
		// ////////////////////////////////////////
		$model = new Football ();
		if ($model->load ( Yii::$app->request->post () )) {
			
			\backend\models\Rules::Save_SpecialSEMS ( new Football (), $model, $_FILES );
		}
		if (! isset ( $_POST ["Football"] )) {
			if (isset ( $_REQUEST ['Rule_Id'] ))
				$_Rule_Id = ( int ) $_REQUEST ["Rule_Id"];
			else {
				$this->redirect ( [ 
						'/rules/index' 
				] );
			}
			// ///////////////////////////////////
			$rules = Rules::findOne ( $_Rule_Id );
			if (is_null ( $rules )) {
				$this->redirect ( [ 
						'/rules/index' 
				] );
			} else {
				$eventId = $rules->eventId;
				$fieldsId = $rules->fieldsId;
				$subFieldsId = $rules->subFieldsId;
			}
			
			$model->eventId = $eventId;
			$model->fieldsId = $fieldsId;
			$model->subFieldsId = $subFieldsId;
			$model = $this->getValues ( $model );
		}
		
		return $this->render ( 'create', [ 
				'model' => $model 
		] );
	}
	
	/**
	 * Updates an existing Football model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		} else {
			return $this->render ( 'update', [ 
					'model' => $model 
			] );
		}
	}
	
	/**
	 * Deletes an existing Football model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * 
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Football model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * 
	 * @param integer $id			
	 * @return Football the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Football::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	
	/**
	 * *
	 */
	private function getValues($model) {
		$eventId = $model->eventId;
		$fieldsId = $model->fieldsId;
		$subFieldsId = $model->subFieldsId;

		$_rules = Rules::getValues ($model->fieldsId, $model->subFieldsId );
		$model = new Football ();
		$model->eventId = $eventId;
		$model->fieldsId = $fieldsId;
		$model->subFieldsId = $subFieldsId;
		foreach ( $_rules as $_rule ) {
			$_atr = trim ( $_rule->name );
			$_val = trim ( $_rule->value );
			try {
				$model->$_atr = $_val;
			} catch ( \Exception $e ) {
			}
		}
		return $model;
	}
	/**
	 * *
	 */

}
