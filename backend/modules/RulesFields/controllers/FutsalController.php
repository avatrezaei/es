<?php

namespace backend\modules\RulesFields\controllers;

use Yii;
use backend\modules\RulesFields\models\Futsal;
use backend\modules\RulesFields\models\FutsalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \backend\models\Rules;
use \backend\models\RulesSearch;

/**
 * FutsalController implements the CRUD actions for Futsal model.
 */
class FutsalController extends Controller
{
	public $layout = '@app/views/layouts/column2';

	/**
	 * Lists all Futsal models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new FutsalSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );

		return $this->render ( 'index', [ 
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Futsal model.
	 *
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render ( 'view', [ 
				'model' => $this->findModel ( $id ) 
		] );
	}
	
	/**
	 * Creates a new Futsal model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate() {
		$_Rule_Id = 0;
		$eventId = 0;
		$fieldsId = 0;
		$subFieldsId = 0;
		// ////////////////////////////////////////
		
		// ////////////////////////////////////////
		$model = new Futsal ();
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			
			\backend\models\Rules::Save_SpecialSEMS ( new Futsal (), $model, $_FILES );
		}
		if (! isset ( $_POST ["Futsal"] )) {
			if (isset ( $_REQUEST ['Rule_Id'] ))
				$_Rule_Id = ( int ) $_REQUEST ["Rule_Id"];
			else {
				$this->redirect ( [ 
						'/rules/index' 
				] );
			}
			// ///////////////////////////////////
			$rules = Rules::findOne ( $_Rule_Id );
			if (is_null ( $rules )) {
				$this->redirect ( [ 
						'/rules/index' 
				] );
			} else {
				$eventId = $rules->eventId;
				$fieldsId = $rules->fieldsId;
				$subFieldsId = $rules->subFieldsId;
			}
			
			$model->eventId = $eventId;
			$model->fieldsId = $fieldsId;
			$model->subFieldsId = $subFieldsId;
			$model = $this->getValues ( $model );
		}
		
		return $this->render ( 'create', [ 
				'model' => $model 
		] );
	}
	
	/**
	 * Updates an existing Futsal model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			return $this->redirect ( [ 
					'view',
					'id' => $model->id 
			] );
		} else {
			return $this->render ( 'update', [ 
					'model' => $model 
			] );
		}
	}
	
	/**
	 * Deletes an existing Futsal model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id			
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Futsal model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id			
	 * @return Futsal the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Futsal::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	
	/**
	 * *
	 */
	private function getValues($model) {
		$eventId = $model->eventId;
		$fieldsId = $model->fieldsId;
		$subFieldsId = $model->subFieldsId;

		$_rules = Rules::getValues ($model->fieldsId, $model->subFieldsId );
		$model = new Futsal ();
		$model->eventId = $eventId;
		$model->fieldsId = $fieldsId;
		$model->subFieldsId = $subFieldsId;
		foreach ( $_rules as $_rule ) {
			$_atr = trim ( $_rule->name );
			$_val = trim ( $_rule->value );
			try {
				$model->$_atr = $_val;
			} catch ( \Exception $e ) {
			}
		}
		return $model;
	}
	
}
