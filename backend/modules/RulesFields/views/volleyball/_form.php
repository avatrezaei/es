<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\assets\UserProfile;

/**
 * *select2 uses***
 */
use kartik\select2\Select2;
use kartik\sortinput\SortableInput;
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\Rules;
use backend\models\RulesSettings;
use backend\models\Event;
use backend\models\Fields;
use backend\models\Subfields;
use yii\web\View;
use backend\modules\RulesFields\models\Volleyball;
use yii\helpers\Url;

use kartik\file\FileInput;
use yii\base\Widget;

UserProfile::register ( $this );
$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>

<?php $Question_No=1;?>
<div id="results" style="display: none"></div>

<?php $fields = Fields::findOne ( $model->fieldsId );?>

<div class="row">
	<div class="col-md-12">
				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','class'=>'form-horizontal']]); ?>
			<?php
			echo $this->render ( '../default/_formRight', [ 
					'model' => $model,
					'form' => $form 
			] );
			?>
			<div class='profile-content'>
			<div class="row">
				<div class='col-md-12'>
					<div id="results_expand_msg" style="display: none"></div>
					<div class='portlet light'>
						<div class='portlet-body'>
							<div class='form-body'>
								<div class='row'>
									<div class="col-md-11 col-md-offset-1 col-sm-12 col-xs-12">
										<div class='form-group'>
								<?=  $form->errorSummary($model);  ?>
				<?= Html::activeHiddenInput($model, 'eventId')?>
				<?=  Html::activeHiddenInput($model, 'fieldsId')?>
				<?=  Html::activeHiddenInput($model, 'subFieldsId')?>
			 </div>
		<?php
		echo $this->render ( '../default/_formUploadFile', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
		
		<?php
		echo $this->render ( '../default/_formLatestRules', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>
		
		<?php
		echo $this->render ( '../default/_formNumSportsman', [ 
				'model' => $model,
				'form' => $form ,
				'Min_Num_Sportsman'=>(empty($model->Min_Num_Sportsman)|| is_null($model->Min_Num_Sportsman))?6:$model->Min_Num_Sportsman,
				'Max_Num_Sportsman'=>(empty($model->Max_Num_Sportsman)|| is_null($model->Max_Num_Sportsman))?12:$model->Max_Num_Sportsman,
		] );
		?>	
		
		<?php
		echo $this->render ( '../default/_formMember', [ 
				'model' => $model,
				'form' => $form,
				'showMessage' => true ,
				'Num_Coach'=>(empty($model->Num_Coach)|| is_null($model->Num_Coach))?1:$model->Num_Coach,
				'Num_Supervisor'=>(empty($model->Num_Supervisor)|| is_null($model->Num_Supervisor))?1:$model->Num_Supervisor,
				'Num_CoachAssistant'=>(empty($model->Num_CoachAssistant)|| is_null($model->Num_CoachAssistant))?'':$model->Num_CoachAssistant,
				'Num_HeadCoach'=>(empty($model->Num_HeadCoach)|| is_null($model->Num_HeadCoach))?'':$model->Num_HeadCoach,
				
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
										<div class="row">
											<div class='form-group'>
												<?= Html::label(Volleyball::Labels["Period"][0],'',['class'=>'control-label pull-left'])?>
												<div class='<?= $class_tochSpin_min ?>'>
													<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'FirstPeriod_Set','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												</div>
												<?= Html::label(Volleyball::Labels["Period"][1],'',['class'=>'control-label pull-left'])?>
												
											</div>
											<div class='form-group'>
												<?= Html::label(Volleyball::Labels["Period"][2],'',['class'=>'control-label pull-left'])?>
											<div class='<?= $class_tochSpin_min ?>'>
													<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'PlayOff_Set','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												</div>
												<?= Html::label(Volleyball::Labels["Period"][3],'',['class'=>'control-label pull-left'])?>
											</div>
											<div class='form-group'>
											<?= Html::label(Volleyball::Labels["Period"][4],'',['class'=>'control-label pull-left'])?>
												<div class='<?= $class_tochSpin_min ?>'>
													<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Period_ResultsSet','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												</div>
												<?= Html::label(Volleyball::Labels["Period"][5],'',['class'=>'control-label pull-left'])?>
											</div>
										</div>
		<?= $this->render ( '../default/_sections' );?>	
										<div class="row">
											<div class='form-group'>
												<?= Html::label(Volleyball::Labels["Score_3Set"][0],'',['class'=>'control-label'])?>
												</div>

											<div class='row'>
												<div class='col-md-12'><?= Html::label(Volleyball::Labels["Score_3Set"]["20"],'',['class'=>'control-label col-md-3'])?>
														<div class='col-md-3'>
															<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_3Set_20','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
														</div>
													<?= Html::error($model, 'Score_3Set_20')?>
													</div>
												<div class='col-md-12'>
													<?= Html::label(Volleyball::Labels["Score_3Set"]["21"],'',['class'=>'control-label col-md-3'])?>
														<div class='col-md-3'>
															<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_3Set_21','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
														</div>
													<?= Html::error($model, 'Score_3Set_21')?>
													</div>
												<div class='col-md-12'>
													<?= Html::label(Volleyball::Labels["Score_3Set"]["GiveUp"],'',['class'=>'control-label col-md-3'])?>
														<div class='col-md-3'>
															<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_3Set_GiveUp','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
														</div>
													<?= Html::error($model, 'Score_3Set_GiveUp')?>
													</div>
											</div>

										</div>

		<?= $this->render ( '../default/_sections' );?>	
										<div class="row">

											<div class='form-group'><?= Html::label(Volleyball::Labels["Score_5Set"][0],'',['class'=>'control-label'])?></div>

											<div class='form-group'>
												<div class='col-md-12'>
													<?= Html::label(Volleyball::Labels["Score_5Set"]["30"],'',['class'=>'control-label col-md-3'])?>
														<div class='col-md-3'>
															<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_5Set_30','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
														</div>
													<?= Html::error($model, 'Score_5Set_30')?>
													</div>
												<div class='col-md-12'>
														<?= Html::label(Volleyball::Labels["Score_5Set"]["31"],'',['class'=>'control-label col-md-3'])?>
															<div class='col-md-3'>
																<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_5Set_31','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
															</div>
													<?= Html::error($model, 'Score_5Set_31')?>
														</div>
												<div class='col-md-12'>
														<?= Html::label(Volleyball::Labels["Score_5Set"]["32"],'',['class'=>'control-label col-md-3'])?>
															<div class='col-md-3'>
																<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_5Set_32','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
															</div>
													<?= Html::error($model, 'Score_5Set_32')?>
													</div>

												<div class='col-md-12'>
														<?= Html::label(Volleyball::Labels["Score_5Set"]["GiveUp"],'',['class'=>'control-label col-md-3'])?>
															<div class='col-md-3'>
																<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_5Set_GiveUp','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
															</div>
													<?= Html::error($model, 'Score_5Set_GiveUp')?>
												</div>
											</div>


										</div>

		<?= $this->render ( '../default/_sections' );?>	
										<div class='row'>
											<div class='form-group'>
												<?=Html::label(Volleyball::Labels["Equality"][0],'',['class'=>'control-label'])?>
												</div>
											<div class="row">
												<div class=col-md-5>
													<?php
													$string_value=implode ( ',', array_diff ( array (
																	1,
																	2,
																	3,
																	4 
															), explode ( ",", $model->Equality ) ) );
													echo SortableInput::widget ( [ 
															'name' => 'kv-conn-1',
															'items' =>(is_null($string_value)||empty($string_value))?array(): Volleyball::ArrayList_Equality,
															'value' => $string_value,
															'hideInput' => false,
															'sortableOptions' => [ 
																	'connected' => true 
															],
															'options' => [ 
																	'class' => 'form-control',
																	'readonly' => false 
															] 
													] );
													?>
												</div>
												<div class="col-md-5">
												<?php
												echo $form->field ( $model, 'Equality' )->widget ( SortableInput::classname (), [ 
														'name' => 'Volleyball[Equality]',
														'items' => (is_null ( $model->Equality ) || empty ( $model->Equality )) ? array () : Volleyball::ArrayList_Equality,
														'value' => $model->Equality,
														'hideInput' => false,
														'sortableOptions' => [ 
																'itemOptions' => [ 
																		'class' => 'alert alert-warning' 
																],
																'connected' => true 
														],
														'options' => [ 
																'class' => 'form-control',
																'readonly' => false 
														] 
												] );
												?>
												</div>
											</div>
										</div>
		<?= $this->render ( '../default/_sections' );?>	
		
			<?php
		echo $this->render ( '../default/_formCoach', [ 
				'model' => $model,
				'form' => $form ,
					'NumberOfTeams_Coach' => (empty ( $model->NumberOfTeams_Coach ) || is_null ( $model->NumberOfTeams_Coach )) ? 1 : $model->NumberOfTeams_Coach ,
		] );
		?>		
			
		<?= $this->render ( '../default/_sections' );?>	
		
			
		<?php
		echo $this->render ( '../default/_formTechnicalCommittee', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRate', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	

		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRateMedal', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	

									</div>
								</div>
							</div>
						</div>
					</div>



				</div>
			</div>
		</div>
				<?php ActiveForm::end(); ?>
	</div>
</div>


























