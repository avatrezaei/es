<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\Basketball */
?>
<div class="basketball-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
