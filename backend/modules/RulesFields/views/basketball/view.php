<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\Basketball */
?>
<div class="basketball-view">
 
	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'eventId',
			'fieldsId',
			'subFieldsId',
			'name',
			'value',
		],
	]) ?>

</div>
