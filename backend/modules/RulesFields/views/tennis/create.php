<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\Tennis */

$this->title = Yii::t('app', 'Create Tennis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tennis')];
?>
<div class="tennis-create">

   
	 <div class='row'><div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?= $this->render('_form', [
					'model' => $model,
				]) ?>
	</div></div>

</div>
