<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\Tennis */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Tennis',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tennis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tennis-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
