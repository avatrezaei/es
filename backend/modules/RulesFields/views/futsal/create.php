<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\Futsal */

$this->title = Yii::t ( 'app', 'Create Futsal' );
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Futsals' ) 
];
?>
<div class="futsal-create">


	<div class='row'>
		<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?=$this->render ( '_form', [ 'model' => $model ] )?>
		</div>
	</div>
</div>
