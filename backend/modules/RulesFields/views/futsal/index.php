<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\RulesFields\models\FutsalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Futsals');
$this->params['breadcrumbs'][] = $this->title;

$this->params['actions'] = [
		[
				'label' => '<i class="ace-icon fa fa-plus bigger-120 orange"></i> ' . Yii::t('app', 'Create Futsals'),
				'url' => ['create']
		]
];
?>
<div class="user-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="icon-user font-dark"></i>
				<span class="caption-subject bold uppercase"><?= $this->title ?></span>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">
			<?= $widget->run() ?>
		</div>
	</div>
</div>