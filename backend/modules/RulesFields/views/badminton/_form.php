<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\assets\UserProfile;

/**
 * *select2 uses***
 */
use kartik\select2\Select2;
use kartik\sortinput\SortableInput;
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\Rules;
use backend\models\RulesSettings;
use backend\models\Event;
use backend\models\Fields;
use backend\models\Subfields;
use yii\web\View;
use backend\modules\RulesFields\models\Badminton;
use yii\helpers\Url;

use kartik\file\FileInput;
use yii\base\Widget;

UserProfile::register ( $this );
$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>

<?php $Question_No=1;?>
<div id="results" style="display: none"></div>

<?php $fields = Fields::findOne ( $model->fieldsId );?>

<div class="row">
	<div class="col-md-12">
				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','class'=>'form-horizontal']]); ?>
			<?php
			echo $this->render ( '../default/_formRight', [ 
					'model' => $model,
					'form' => $form 
			] );
			?>	
			<div class='profile-content'>
			<div class="row">
				<div class='col-md-12'>

					<div id="results_expand_msg" style="display: none"></div>
					<div class='portlet light'>
						<div class='portlet-body'>
							<div class='form-body'>
								<div class='row'>
									<div class="col-md-11 col-md-offset-1 col-sm-12 col-xs-12">
										<div class='form-group'>
								<?=  $form->errorSummary($model);  ?>
				<?= Html::activeHiddenInput($model, 'eventId')?>
				<?=  Html::activeHiddenInput($model, 'fieldsId')?>
				<?=  Html::activeHiddenInput($model, 'subFieldsId')?>
			 </div>

											
		<?php
		echo $this->render ( '../default/_formUploadFile', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
		
		<?php
		echo $this->render ( '../default/_formLatestRules', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formNumTypeOfMatch', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>
		
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formNumSportsman', [ 
				'model' => $model,
				'form' => $form ,
				'Min_Num_Sportsman'=>(empty($model->Min_Num_Sportsman)|| is_null($model->Min_Num_Sportsman))?1:$model->Min_Num_Sportsman,
				'Max_Num_Sportsman'=>(empty($model->Max_Num_Sportsman)|| is_null($model->Max_Num_Sportsman))?2:$model->Max_Num_Sportsman,
		] );
		?>	
		<?php
		echo $this->render ( '../default/_formMember', [ 
				'model' => $model,
				'form' => $form,
				'showMessage' => true ,
				'Num_Coach'=>(empty($model->Num_Coach)|| is_null($model->Num_Coach))?1:$model->Num_Coach,
				'Num_Supervisor'=>(empty($model->Num_Supervisor)|| is_null($model->Num_Supervisor))?1:$model->Num_Supervisor,
				'Num_CoachAssistant'=>(empty($model->Num_CoachAssistant)|| is_null($model->Num_CoachAssistant))?'':$model->Num_CoachAssistant,
				'Num_HeadCoach'=>(empty($model->Num_HeadCoach)|| is_null($model->Num_HeadCoach))?'':$model->Num_HeadCoach,
				
		] );
		?>	
		<?php
		echo $this->render ( '../default/_formPeriodTypeOfMatch', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>					
		
		<?= $this->render ( '../default/_sections' );?>	
		
										<div class="row">
											<div class='form-group'>
												<?= Html::label(Badminton::Labels['Periodic_Classification'],'',['class'=>'control-label']);?>
												</div>
											<div class="row">
												<div class="col-sm-5">
															<?php
															echo SortableInput::widget ( [ 
																	'name' => 'kv-conn-1',
																	'items' => Badminton::Periodic_Classification_array,
																	'value' => implode ( ',', array_diff ( array (
																			1,
																			2,
																			3,
																			4 
																	), explode ( ",", $model->Periodic_Classification ) ) ),
																	'hideInput' => false,
																	'sortableOptions' => [ 
																			'connected' => true 
																	],
																	'options' => [ 
																			'class' => 'form-control',
																			'readonly' => true 
																	] 
															] );
															?>
													</div>
												<div class="col-sm-5">
																<?php
																echo $form->field ( $model, 'Periodic_Classification' )->widget ( SortableInput::className (), [ 
																		'id' => 'badminton-periodic_classification',
																		'items' => (is_null ( $model->Periodic_Classification ) || empty ( $model->Periodic_Classification )) ? array () : Badminton::Periodic_Classification_array,
																		'value' => $model->Periodic_Classification,
																		'hideInput' => false,
																		'sortableOptions' => [ 
																				'itemOptions' => [ 
																						'class' => 'alert alert-warning' 
																				],
																				'connected' => true 
																		],
																		'options' => [ 
																				'class' => 'form-control',
																				'readonly' => false 
																		] 
																] );
																?>
													</div>

											</div>
										</div>
										
										
		<?= $this->render ( '../default/_sections' );?>	

											<div class="row">
											<div class='form-group'>
												<?= Html::label(Badminton::Labels['Equality_Classification'],'',['class'=>'control-label']);?><br>
											</div>
											<div class='row'>
												<div class="col-sm-5">
													<?php
													echo SortableInput::widget ( [ 
															'name' => 'kv-conn-1',
															'items' => Badminton::Equality_Classification_array,
															'value' => implode ( ',', array_diff ( array (
																	1,
																	2,
																	3 
															), explode ( ",", $model->Equality_Classification ) ) ),
															'hideInput' => false,
															'sortableOptions' => [ 
																	'connected' => true 
															],
															'options' => [ 
																	'class' => 'form-control',
																	'readonly' => true 
															] 
													] );
													?>
												</div>
												<div class="col-sm-5">
														<?php
														echo $form->field ( $model, 'Equality_Classification' )->widget ( SortableInput::className (), [ 
																'id' => 'badminton-equality_classification',
																'items' => (is_null ( $model->Equality_Classification ) || empty ( $model->Equality_Classification )) ? array () : Badminton::Equality_Classification_array,
																'value' => $model->Equality_Classification,
																'hideInput' => false,
																'sortableOptions' => [ 
																		'itemOptions' => [ 
																				'class' => 'alert alert-warning' 
																		],
																		'connected' => true 
																],
																'options' => [ 
																		'class' => 'form-control',
																		'readonly' => false 
																] 
														] );
														?>
													</div>
											</div>
										</div>
											
		<?= $this->render ( '../default/_sections' );?>	
									<div class="row">
											<div class='form-group'>
											<?= Html::activeCheckbox($model, 'Allow_As_Single_Double'); ?>
											<?= Html::error($model, 'Allow_As_Single_Double')?>
										</div>
											<div class='form-group'>
											<?= Html::activeCheckbox($model, 'Allow_Sportsman_SingleOrDouble'); ?>
											<?= Html::error($model, 'Allow_Sportsman_SingleOrDouble')?>
										</div>
											<div class='form-group'>
											<?= Html::label(Badminton::Labels["Num_University"][0],'',['class'=>'control-label pull-left']);?>
											<div class='<?= $class_tochSpin_min ?>'>
												<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Num_Sportsman_Single_University','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
											</div> 	
											<?= Html::label(Badminton::Labels["Num_University"][1],'',['class'=>'control-label pull-left']);?>
											<div class='<?= $class_tochSpin_min ?>'>
												<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Num_Team_Double_University','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
											</div> 	
											<?= Html::label(Badminton::Labels["Num_University"][2],'',['class'=>'control-label pull-left']);?>
											
											<?= Html::error($model, 'Num_Sportsman_Single_University')?>
											<?= Html::error($model, 'Num_Team_Double_University')?>
										</div>
											<div class='form-group'>
											<?= Html::label(Badminton::Labels["TypeOfMatch_Single_Double"][0],'',['class'=>'control-label pull-left']);?>
											<div class='col-md-2'><?= Html::activeTextInput($model, 'TypeOfMatch_Single_Double',['class'=>'form-control']); ?></div>
											<?= Html::label(Badminton::Labels["TypeOfMatch_Single_Double"][1],'',['class'=>'control-label pull-left']);?>
											<?= Html::error($model, 'TypeOfMatch_Single_Double')?>
										</div>


										</div>								
										

		<?= $this->render ( '../default/_sections' );?>	
										
			<?php
		echo $this->render ( '../default/_formCoach', [ 
				'model' => $model,
				'form' => $form ,
					'NumberOfTeams_Coach' => (empty ( $model->NumberOfTeams_Coach ) || is_null ( $model->NumberOfTeams_Coach )) ? 1 : $model->NumberOfTeams_Coach ,
		] );
		?>		
				
		<?= $this->render ( '../default/_sections' );?>	
								
		<?php
		echo $this->render ( '../default/_formTechnicalCommittee', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRateTeam', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRate', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	

		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRateMedal', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	

										

										</div>
								</div>
							</div>
						</div>
					</div>



				</div>
			</div>
		</div>
				<?php ActiveForm::end(); ?>
	</div>
</div>