<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\BadmintonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="badminton-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'eventId') ?>

	<?= $form->field($model, 'fieldsId') ?>

	<?= $form->field($model, 'subFieldsId') ?>

	<?= $form->field($model, 'name') ?>

	<?php // echo $form->field($model, 'value') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
