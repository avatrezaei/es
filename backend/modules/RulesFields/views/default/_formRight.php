<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use backend\models\Event;
use backend\models\Fields;
use yii\web\View;
use yii\helpers\Url;
?>
<div class='profile-sidebar'>
	<div class="portlet light profile-sidebar-portlet ">

		<div class="profile-userpic">

<?php $fields = Fields::findOne ( $model->fieldsId );?>
<?php echo $fields->getImage($fields,'user-pic rounded img-responsive');?>
			</div>
		<div class="profile-usertitle">
			<div class="profile-usertitle-name"><?php Event::findByPK($model->eventId)->name; ?></div>
			<div class="profile-usertitle-job"><?= Yii::t('app','Field').': '.Fields::findByPK($model->fieldsId)->name; ?></div>
		</div>
		<div class="profile-usermenu"></div>
		<div class="profile-userbuttons">
						<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn  green btn-sm'])?>
						<?= Html::a(Yii::t('app', 'Close'),Url::to(['/rules/index']), ['class' => 'btn  red btn-sm'])?>
						
					</div>
		<div class="profile-usermenu"></div>
	</div>

</div>