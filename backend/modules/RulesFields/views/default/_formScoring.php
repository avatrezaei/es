	<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	
	/**
	 * *select2 uses***
	 */
	use kartik\touchspin\TouchSpin;
	use yii\web\JsExpression;
	use yii\web\View;
	use backend\modules\RulesFields\models\MessageRules;
	
	$class_tochSpin_min = 'col-md-2';
	$input_class = 'col-md-3';
	$formGroupClass = Yii::$app->params ['formGroupClass'];
	$template = Yii::$app->params ['template'];
	$inputClass = Yii::$app->params ['inputClass'];
	$labelClass = 'col-md-3 col-sm-3 control-label';
	$errorClass = Yii::$app->params ['errorClass'];
	$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
	$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
	?>
<div class='form-group'>
			
	<?= Html::label(MessageRules::Labels["Scoring"][0],'',['class'=>'control-label'])?>
	</div>
<div class='row col-md-12'>

	<div class='form-group'>
												
 	<?= Html::label(' اول: ','',['class'=>'control-label col-md-1'])?>
	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_1' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_1 ) ? $Scoring_1 : $model->Scoring_1 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>
 	<?= Html::label(' دوم: ','',['class'=>'control-label col-md-1'])?>
 	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_2' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_2 ) ? $Scoring_2 : $model->Scoring_2 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>
 	<?= Html::label(' سوم: ','',['class'=>'control-label col-md-1'])?>
 	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_3' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_3 ) ? $Scoring_3 : $model->Scoring_3 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>
 	<?= Html::label(' چهارم: ','',['class'=>'control-label col-md-1'])?>
 	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_4' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_4 ) ? $Scoring_4 : $model->Scoring_4 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>

	</div>
</div>
<div class='row col-md-12'>
	<div class='form-group'>
													
 	<?= Html::label(' پنجم: ','',['class'=>'control-label col-md-1'])?>
 	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_5' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_5 ) ? $Scoring_5 : $model->Scoring_5 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>
 	<?= Html::label(' ششم: ','',['class'=>'control-label col-md-1'])?>
 	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_6' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_6 ) ? $Scoring_6 : $model->Scoring_6 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>
 	<?= Html::label(' هفتم: ','',['class'=>'control-label col-md-1'])?>
 	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_7' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_7 ) ? $Scoring_7 : $model->Scoring_7 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>
 	<?= Html::label(' هشتم: ','',['class'=>'control-label col-md-1'])?>
 	<div class='<?= $class_tochSpin_min ?>'>
				<?php
				
				echo $form->field ( $model, 'Scoring_8' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Scoring_8 ) ? $Scoring_8 : $model->Scoring_8 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
	</div>
	</div>
</div>
<div class='form-group'>
 	<?= Html::error($model,'Scoring_1');?>
 	<?= Html::error($model,'Scoring_2');?>
 	<?= Html::error($model,'Scoring_3');?>
 	<?= Html::error($model,'Scoring_4');?>
 	<?= Html::error($model,'Scoring_5');?>
 	<?= Html::error($model,'Scoring_6');?>
 	<?= Html::error($model,'Scoring_7');?>
 	<?= Html::error($model,'Scoring_8');?>
 </div>