<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'pull-left control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>
<div class="row">
	<div class='form-group'>
		<?php echo Html::label(MessageRules::Labels["Rate"][0],'',['class'=>'control-label']);?>
	</div>
	<div class='form-group'>
	
	<div class="col-md-1"></div>
	<div class='col-md-11'>
		<div class='col-md-6'>
			<?=Html::label(MessageRules::Labels["Rate"][1],'',['class'=>'control-label col-md-2'])?>
			<div class='col-md-6'>
			<?php
			echo $form->field ( $model, 'rate_1' )->widget ( TouchSpin::className (), [ 
					'options' => [ 
							'class' => 'form-control ' ,
							'value' => isset ( $rate_1 ) ? $rate_1 : $model->rate_1 
					],
					'pluginOptions' => [ 
							'verticalbuttons' => true,
							'verticalupclass' => 'glyphicon glyphicon-plus',
							'verticaldownclass' => 'glyphicon glyphicon-minus' 
					] 
			] )->label ( false );
			?>
	 		</div>
		</div>
		<div class='col-md-6'>
			<?=Html::label(MessageRules::Labels["Rate"][2],'',['class'=>'control-label col-md-2'])?>
			<div class='col-md-6'>
			<?php
			echo $form->field ( $model, 'rate_2' )->widget ( TouchSpin::className (), [ 
					'options' => [ 
							'class' => 'form-control ' ,
							'value' => isset ( $rate_2 ) ? $rate_2 : $model->rate_2 
					],
					'pluginOptions' => [ 
							'verticalbuttons' => true,
							'verticalupclass' => 'glyphicon glyphicon-plus',
							'verticaldownclass' => 'glyphicon glyphicon-minus' 
					] 
			] )->label ( false );
			?>
	 		</div>
		</div>
		<div class='col-md-6'>
			<?=Html::label(MessageRules::Labels["Rate"][3],'',['class'=>'control-label col-md-2'])?>
			<div class='col-md-6'>
			<?php
			echo $form->field ( $model, 'rate_3' )->widget ( TouchSpin::className (), [ 
					'options' => [ 
							'class' => 'form-control ' ,
							'value' => isset ( $rate_3 ) ? $rate_3 : $model->rate_3 
					],
					'pluginOptions' => [ 
							'verticalbuttons' => true,
							'verticalupclass' => 'glyphicon glyphicon-plus',
							'verticaldownclass' => 'glyphicon glyphicon-minus' 
					] 
			] )->label ( false );
			?>
	 		</div>
		</div>
		<div class='col-md-6'>
				<?=Html::label(MessageRules::Labels["Rate"][4],'',['class'=>'control-label col-md-2'])?>
				<div class='col-md-6'>
				<?php
				echo $form->field ( $model, 'rate_4' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ' ,
							'value' => isset ( $rate_4 ) ? $rate_4 : $model->rate_4 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
		 		</div>
			</div>
			<div class='col-md-6'>
				<?=Html::label(MessageRules::Labels["Rate"][5],'',['class'=>'control-label col-md-2'])?>
				<div class='col-md-6'>
				<?php
				echo $form->field ( $model, 'rate_5' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ' ,
							'value' => isset ( $rate_5 ) ? $rate_5 : $model->rate_5 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
		 		</div>
			</div>
			<div class='col-md-6'>
				<?=Html::label(MessageRules::Labels["Rate"][6],'',['class'=>'control-label col-md-2'])?>
				<div class='col-md-6'>
				<?php
				echo $form->field ( $model, 'rate_6' )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ' ,
							'value' => isset ( $rate_6 ) ? $rate_6 : $model->rate_6 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] )->label ( false );
				?>
		 		</div>
			</div>
		</div>
	</div>
</div>