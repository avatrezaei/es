<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '<div class="col-md-12 col-sm-12">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'pull-left control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>
<div class='row'>
	<div class='form-group '>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Single'][0],'',['class'=>'control-label pull-left'])?>
				<div class='<?=$input_class?>'>
				<?php
				echo Html::activeTextInput ( $model, 'TypeOfMatch_Single', [ 
						'class' => 'form-control',
						'value' => isset ( $TypeOfMatch_Single ) ? $TypeOfMatch_Single : $model->TypeOfMatch_Single 
				] );
				?>
				</div>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Single'][1],'',['class'=>'control-label pull-left'])?>
				<div class='<?= $class_tochSpin_min ?>'>
					<?php
					echo $form->field ( $model, 'Single_Set1', [ 
							'template' => $template 
					] )->widget ( TouchSpin::className (), [ 
							'options' => [ 
									'class' => 'form-control ',
									'value' => isset ( $Single_Set1 ) ? $Single_Set1 : $model->Single_Set1 
							],
							'pluginOptions' => [ 
									'verticalbuttons' => true,
									'verticalupclass' => 'glyphicon glyphicon-plus',
									'verticaldownclass' => 'glyphicon glyphicon-minus' 
							] 
					] );
					?>
				</div>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Single'][2],'',['class'=>'control-label pull-left'])?>
					<div class='<?= $class_tochSpin_min ?>'>
					<?php
					echo $form->field ( $model, 'Single_Set2', [ 
							'template' => $template 
					] )->widget ( TouchSpin::className (), [ 
							'options' => [ 
									'class' => 'form-control ',
									'value' => isset ( $Single_Set2 ) ? $Single_Set2 : $model->Single_Set2 
							],
							'pluginOptions' => [ 
									'verticalbuttons' => true,
									'verticalupclass' => 'glyphicon glyphicon-plus',
									'verticaldownclass' => 'glyphicon glyphicon-minus' 
							] 
					] );
					?>
				</div>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Single'][3],'',['class'=>'control-label pull-left'])?>
			</div>
</div>