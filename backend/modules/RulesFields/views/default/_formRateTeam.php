<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3 col-sm-3 control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>

<div class="row">
	<div class='form-group'>
	<?= Html::label(MessageRules::Labels["Rate_Team"][0],'',['class'=>'control-label pull-left']);?>
	</div>
	<div class='form-group '>
		<div class='col-md-1'></div>
		<div class='col-md-11'>
			<div class='col-md-12'>
					<?=Html::label(MessageRules::Labels["Rate_Team"][1],'',['class'=>'control-label col-md-4'])?>
					<div class='col-md-3'>
					<?php
					echo $form->field ( $model, 'Score_Winning_Team' )->widget ( TouchSpin::className (), [ 
							'options' => [ 
									'class' => 'form-control ',
									'value' => isset ( $Score_Winning_Team ) ? $Score_Winning_Team : $model->Score_Winning_Team 
							],
							'pluginOptions' => [ 
									'verticalbuttons' => true,
									'verticalupclass' => 'glyphicon glyphicon-plus',
									'verticaldownclass' => 'glyphicon glyphicon-minus' 
							] 
					] )->label ( false );
					?></div>
			</div>
			<div class='col-md-12'>
					<?=Html::label(MessageRules::Labels["Rate_Team"][2],'',['class'=>'control-label col-md-4'])?>
					<div class='col-md-3'>
					<?php
					echo $form->field ( $model, 'Score_Losers_Team' )->widget ( TouchSpin::className (), [ 
							'options' => [ 
									'class' => 'form-control ',
									'value' => isset ( $Score_Losers_Team ) ? $Score_Losers_Team : $model->Score_Losers_Team 
							],
							'pluginOptions' => [ 
									'verticalbuttons' => true,
									'verticalupclass' => 'glyphicon glyphicon-plus',
									'verticaldownclass' => 'glyphicon glyphicon-minus' 
							] 
					] )->label ( false );
					?>
						</div>
			</div>
			<div class='col-md-12'>
					<?=Html::label(MessageRules::Labels["Rate_Team"][3],'',['class'=>'control-label col-md-4'])?>
					<div class='col-md-3'>
					<?php
					echo $form->field ( $model, 'Score_GiveUp_Team' )->widget ( TouchSpin::className (), [ 
							'options' => [ 
									'class' => 'form-control ',
									'value' => isset ( $Score_GiveUp_Team ) ? $Score_GiveUp_Team : $model->Score_GiveUp_Team 
							],
							'pluginOptions' => [ 
									'verticalbuttons' => true,
									'verticalupclass' => 'glyphicon glyphicon-plus',
									'verticaldownclass' => 'glyphicon glyphicon-minus' 
							] 
					] )->label ( false );
					?>
					</div>
			</div>
		</div>
	</div>
</div>