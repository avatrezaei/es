<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'pull-left control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>
<div class='row'>

	<div class='form-group '>
			<?= Html::label(MessageRules::Labels["Period"][0],'',['class'=>'control-label pull-left'])?>
		 	<div class='<?=$input_class?>'>
		 	<?php
				
				echo Html::activeTextInput ( $model, 'FirstPeriod_TypeOfMatch', [ 
						'class' => 'form-control' ,
						'value' => isset ( $FirstPeriod_TypeOfMatch ) ? $FirstPeriod_TypeOfMatch : $model->FirstPeriod_TypeOfMatch 
				] );
				?>
		 	</div>
			<?= Html::label(MessageRules::Labels["Period"][2],'',['class'=>'control-label pull-left'])?>
		</div>

	<div class='form-group '>
			<?= Html::label(MessageRules::Labels["Period"][1],'',['class'=>'control-label pull-left'])?>
		 	<div class='<?=$input_class?>'>
		 	<?php
				
				echo Html::activeTextInput ( $model, 'NextPeriod_TypeOfMatch', [ 
						'class' => 'form-control' ,
						'value' => isset ( $NextPeriod_TypeOfMatch ) ? $NextPeriod_TypeOfMatch : $model->NextPeriod_TypeOfMatch 
				] );
				?>
		 	</div>
			<?= Html::label(MessageRules::Labels["Period"][2],'',['class'=>'control-label pull-left'])?>
		</div>

</div>