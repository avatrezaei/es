	<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	
	/**
	 * *select2 uses***
	 */
	use kartik\touchspin\TouchSpin;
	use yii\web\JsExpression;
	use yii\web\View;
	use backend\modules\RulesFields\models\MessageRules;
	
	$class_tochSpin_min = 'col-md-2';
	$input_class = 'col-md-3';
	$formGroupClass = Yii::$app->params ['formGroupClass'];
	$template = Yii::$app->params ['template'];
	$inputClass = Yii::$app->params ['inputClass'];
	$labelClass = 'col-md-3 col-sm-3 control-label';
	$errorClass = Yii::$app->params ['errorClass'];
	$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
	$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
	?>
<div class='row'>
	<div class='form-group '>
			<?= Html::label(MessageRules::Labels["Latest_Rules"][0],'',['class'=>'control-label pull-left'])?>
		 	<div class='<?=$input_class?>'>
		 	<?php
				echo Html::activeInput ( 'text', $model, 'Latest_Rules', [ 
						'class' => 'form-control',
						'value' => isset ( $Latest_Rules ) ? $Latest_Rules : $model->Latest_Rules 
				] );
				?>
		 	</div>
			<?= Html::label(MessageRules::Labels["Latest_Rules"][1],'',['class'=>'control-label pull-left'])?>
		 	<?= Html::error($model,'Latest_Rules');?>
		 </div>

	<div class='form-group '>
			<?= Html::label(MessageRules::Labels["Ways_Of_Holding"][0],'',['class'=>'control-label pull-left'])?>
		 	<div class='<?=$input_class?>'>
		 	<?php
				echo Html::activeTextInput ( $model, 'Ways_Of_Holding', [ 
						'class' => 'form-control',
						'value' => isset ( $Ways_Of_Holding ) ? $Ways_Of_Holding : $model->Ways_Of_Holding 
				] );
				?>
		 	</div>
			<?= Html::label(MessageRules::Labels["Ways_Of_Holding"][1],'',['class'=>'control-label pull-left'])?>
		 	<?= Html::error($model,'Ways_Of_Holding');?>
		</div>
</div>