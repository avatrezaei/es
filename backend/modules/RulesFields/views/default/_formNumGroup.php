<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '<div class="col-md-12 col-sm-12">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'pull-left control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>
<div class="row">
	<div class='form-group '>
			<?= Html::label(MessageRules::Labels["Num_Group"][0],'',['class'=>'control-label pull-left'])?>
			<div class='<?= $class_tochSpin_min ?>'>
				<?php
				echo $form->field ( $model, 'Num_Promoted_Team', [ 
						'template' => $template 
				] )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Num_Promoted_Team ) ? $Num_Promoted_Team : $model->Num_Promoted_Team 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] );
				?>
			</div>
			<?= Html::label(MessageRules::Labels["Num_Group"][1],'',['class'=>'control-label pull-left'])?>
			<div class='<?= $class_tochSpin_min ?>'>
				<?php
				echo $form->field ( $model, 'Num_Rate_NextStage', [ 
						'template' => $template 
				] )->widget ( TouchSpin::className (), [ 
						'options' => [ 
								'class' => 'form-control ',
								'value' => isset ( $Num_Rate_NextStage ) ? $Num_Rate_NextStage : $model->Num_Rate_NextStage 
						],
						'pluginOptions' => [ 
								'verticalbuttons' => true,
								'verticalupclass' => 'glyphicon glyphicon-plus',
								'verticaldownclass' => 'glyphicon glyphicon-minus' 
						] 
				] );
				?>
			</div>
			<?= Html::label(MessageRules::Labels["Num_Group"][2],'',['class'=>'control-label pull-left'])?>
</div>
</div>
