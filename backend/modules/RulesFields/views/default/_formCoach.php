	<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	
	/**
	 * *select2 uses***
	 */
	use kartik\touchspin\TouchSpin;
	use yii\web\JsExpression;
	use yii\web\View;
	use backend\modules\RulesFields\models\MessageRules;
	
	$class_tochSpin_min = 'col-md-2';
	$input_class = 'col-md-3';
	$formGroupClass = Yii::$app->params ['formGroupClass'];
	$template = '<div class="col-md-12 col-sm-12">{input} {hint} {error}</div>';
	$inputClass = Yii::$app->params ['inputClass'];
	$labelClass = 'col-md-3 col-sm-3 control-label';
	$errorClass = Yii::$app->params ['errorClass'];
	$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
	$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
	?>
<div class="row">

	<div class='form-group'>
 	<?php
		echo Html::activeCheckbox ( $model, 'Coach_Is_Player', [ 
				'template' => "{label}\n<div class='col-md-1'>{input}<span class='help-block'>{hint}</span></div>",
				'labelOptions' => [ 
						'class' => 'pull-left control-label' 
				] 
		] );
		?>
 	<?= Html::error($model, 'Coach_Is_Player');?>
	</div>



	<div class='form-group'>
	<?= Html::label(MessageRules::Labels["NumberOfTeams_Coach"][0],'',['class'=>'control-label pull-left']);?>
	<div class='<?= $class_tochSpin_min ?>'>
	<?php
	echo $form->field ( $model, 'NumberOfTeams_Coach', [ 
			'template' => $template 
	] )->widget ( TouchSpin::className (), [ 
			'options' => [ 
			'class' => 'form-control ' ,
			'value'=>isset ( $NumberOfTeams_Coach ) ? $NumberOfTeams_Coach : $model->NumberOfTeams_Coach ,
			],
			'pluginOptions' => [ 
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus' 
			] 
	] )->label ( false );
	?>
	</div>
	<?= Html::label(MessageRules::Labels["NumberOfTeams_Coach"][1],'',['class'=>'control-label pull-left']);?>
 	<?= Html::error($model, 'NumberOfTeams_Coach');?>
</div>
</div>