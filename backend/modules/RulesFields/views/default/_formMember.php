	<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	
	/**
	 * *select2 uses***
	 */
	use kartik\touchspin\TouchSpin;
	use yii\web\JsExpression;
	use yii\web\View;
	use backend\modules\RulesFields\models\MessageRules;
	
	$class_tochSpin_min = 'col-md-4';
	$input_class = 'col-md-3';
	$formGroupClass = Yii::$app->params ['formGroupClass'];
	$template = '<div class="col-md-11 col-sm-11">{input} {hint} {error}</div>';
	$inputClass = Yii::$app->params ['inputClass'];
	$labelClass = 'col-md-3 col-sm-3 control-label';
	$errorClass = Yii::$app->params ['errorClass'];
	$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
	$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
	?>
<div class='row'>
	<?php
	if (isset ( $showMessage ) && $showMessage == true) {
		?>
	<div class='form-group'>
	<?=  Html::label(MessageRules::Labels["Num_Sportsman"][0],'',['class'=>'control-label pull-left'])?>
	</div>
	<?php
	}
	?>
	<div class='col-md-1'></div>
	<div class='col-md-11'>
		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
					<div class="col-md-2"></div>
					<div class='<?= $class_tochSpin_min ?>'>
						<?php
						echo $form->field ( $model, 'Num_Coach' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $Num_Coach ) ? $Num_Coach : $model->Num_Coach 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>
				 	<?= Html::label(MessageRules::Labels["Num_Sportsman"][5],'',['class'=>'control-label col-md-1'])?>
				</div>
			</div>
			<div class='col-md-6'>
				<div class='form-group'>
					<div class="col-md-2"></div>
					<div class='<?= $class_tochSpin_min ?>'>
						<?php
						echo $form->field ( $model, 'Num_Supervisor' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $Num_Supervisor ) ? $Num_Supervisor : $model->Num_Supervisor 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>
				 	<?= Html::label(MessageRules::Labels["Num_Sportsman"][6],'',['class'=>'control-label col-md-1'])?>
			</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
					<div class="col-md-2"></div>
					<div class='<?= $class_tochSpin_min ?>'>
						<?php
						echo $form->field ( $model, 'Num_CoachAssistant' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $Num_CoachAssistant ) ? $Num_CoachAssistant : $model->Num_CoachAssistant 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>
				 	<?= Html::label(MessageRules::Labels["Num_Sportsman"][7],'',['class'=>'control-label col-md-2'])?>
				</div>
			</div>
			<div class='col-md-6'>
				<div class='form-group'>
					<div class="col-md-2"></div>
					<div class='<?= $class_tochSpin_min ?>'>
						<?php
						echo $form->field ( $model, 'Num_HeadCoach' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $Num_HeadCoach ) ? $Num_HeadCoach : $model->Num_HeadCoach 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>
				 	<?= Html::label(MessageRules::Labels["Num_Sportsman"][8],'',['class'=>'control-label  col-md-2'])?>
			</div>
			</div>
		</div>
	</div>
</div>
