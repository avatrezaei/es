	<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	
	/**
	 * *select2 uses***
	 */
	use kartik\touchspin\TouchSpin;
	use yii\web\JsExpression;
	use yii\web\View;
	use backend\modules\RulesFields\models\MessageRules;
	
	$class_tochSpin_min = 'col-md-4';
	$input_class = 'col-md-3';
	$formGroupClass = Yii::$app->params ['formGroupClass'];
	$template = Yii::$app->params ['template'];
	$inputClass = Yii::$app->params ['inputClass'];
	$labelClass = 'col-md-3 col-sm-3 control-label';
	$errorClass = Yii::$app->params ['errorClass'];
	$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
	$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
	?>

<div class='row'>
	<div class='form-group'>
	<?=  Html::label(MessageRules::Labels["Num_Sportsman"][0],'',['class'=>'control-label pull-left'])?>
	</div>
	<div class='col-md-1'></div>

	<div class='col-md-11'>
		<div class='row'>
			<div class='col-md-6'>
				<div class='form-group'>
		 			<?= Html::label(MessageRules::Labels["Num_Sportsman"][1],'',['class'=>'control-label col-md-2'])?>
					<div class='<?= $class_tochSpin_min ?>'>
						<?php
						echo $form->field ( $model, 'Min_Num_Sportsman' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $Min_Num_Sportsman ) ? $Min_Num_Sportsman : $model->Min_Num_Sportsman 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>
				 	<?= ' '.Html::label(MessageRules::Labels["Num_Sportsman"][2],'',['class'=>'control-label col-md-1'])?>
	 			</div>
			</div>
			<div class='col-md-6'>
				<div class='form-group'>
		 			<?= Html::label(MessageRules::Labels["Num_Sportsman"][4],'',['class'=>'control-label col-md-2'])?>
				 	<div class='<?= $class_tochSpin_min ?>'>
						<?php
						echo $form->field ( $model, 'Max_Num_Sportsman' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $Max_Num_Sportsman ) ? $Max_Num_Sportsman : $model->Max_Num_Sportsman 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>
				 	<?= Html::label(MessageRules::Labels["Num_Sportsman"][2],'',['class'=>'control-label col-md-1'])?>
	 			</div>
			</div>

		</div>

	</div>

</div>