<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '<div class="col-md-12 col-sm-12">{input} {hint} {error}</div>';
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'pull-left control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';

$template_checkbox = "<div class='col-md-3 col-sm-3'>{input}<span class='help-block'>{hint}</span></div>";
?>
<div class="row">
	<div class='form-group '>
		<?= Html::label(MessageRules::Labels["Num_TypeOfMatch"][0],'',['class'=>'control-label pull-left'])?>
		<div class='<?= $class_tochSpin_min ?>'>
			<?php
			echo $form->field ( $model, 'Num_TypeOfMatch', [ 
					'template' => $template 
			] )->widget ( TouchSpin::className (), [ 
					'options' => [ 
							'class' => 'form-control ',
							'value' => isset ( $Num_TypeOfMatch ) ? $Num_TypeOfMatch : $model->Num_TypeOfMatch 
					],
					'pluginOptions' => [ 
							'verticalbuttons' => true,
							'verticalupclass' => 'glyphicon glyphicon-plus',
							'verticaldownclass' => 'glyphicon glyphicon-minus' 
					] 
			] );
			?>
		</div>
		<?= Html::label(MessageRules::Labels["Num_TypeOfMatch"][1],'',['class'=>$labelClass])?>
	</div>
	<div class='row'>
		<div class='col-md-1'></div>
		<div class='col-md-11'>
			<div class='form-group '>
				<div class='checkbox-list'>
			<?php
			echo $form->field ( $model, 'Is_Single', [ 
					'options' => [ ],
					// 'class' => $formGroupClass . ' form-control'
					'template' => $template_checkbox 
			] )->checkbox ()->label ( null, [ 
					'class' => $labelClass 
			] )?>

						<?php
						echo $form->field ( $model, 'Is_Double', [ 
								'options' => [ ],
								// 'class' => $formGroupClass . ' form-control'
								'template' => $template_checkbox 
						] )->checkbox ()->label ( null, [ 
								'class' => $labelClass 
						] )?>

						<?php
						echo $form->field ( $model, 'Is_Team', [ 
								'options' => [ ],
								// 'class' => $formGroupClass . 'form-control'
								'template' => $template_checkbox 
						] )->checkbox ()->label ( null, [ 
								'class' => $labelClass 
						] )?>

						</div>
			</div>
		</div>
	</div>

</div>