<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'pull-left control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>
<div class="row">
	<div class='form-group'><?=Html::label(MessageRules::Labels["Technical_Committee"][0],'',['class'=>'control-label ']) ?></div>
	<div class='form-group'>
		<div class="col-md-1"></div>
		<div class='col-md-11'>
			<div class='col-md-6'><?= Html::activeCheckbox($model, 'Head_Association')?></div>
			<div class='col-md-6'><?= Html::activeCheckbox($model, 'Technical_Supervisor')?></div>
			<div class='col-md-6'><?= Html::activeCheckbox($model, 'One_Committee')?></div>
			<div class='col-md-6'><?= Html::activeCheckbox($model, 'Two_From_Coach_Supervisor')?></div>
		</div>
	</div>
</div>
