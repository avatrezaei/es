<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * *select2 uses***
 */
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\web\View;
use backend\modules\RulesFields\models\MessageRules;

$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3 col-sm-3 control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>
<div class="row">
	<div class='form-group'>
 		<?=Html::activeCheckbox ( $model, 'RateIsMedal', [ 'template' => "{label}\n<div class='col-md-1'>{input}<span class='help-block'>{hint}</span></div>",'labelOptions' => [ 'class' => 'pull-left control-label' ] ] );?>
	</div>
	<div class='form-group'>
		<div class="col-md-1"></div>
		<div class='col-md-11'>
			<div class='col-md-6'>
					<?=Html::label(MessageRules::Labels["Rate_Medal"][0],'',['class'=>'control-label col-md-2'])?>
					<div class='col-md-6'>
						<?php
						echo $form->field ( $model, 'rate_gold' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $rate_gold ) ? $rate_gold : $model->rate_gold 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>

			</div>
			<div class='col-md-6'>

					<?=Html::label(MessageRules::Labels["Rate_Medal"][1],'',['class'=>'control-label col-md-2'])?>
					
					<div class='col-md-6'>
						<?php
						echo $form->field ( $model, 'rate_silver' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $rate_silver ) ? $rate_silver : $model->rate_silver 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>

			</div>
			<div class='col-md-6'>

					<?=Html::label(MessageRules::Labels["Rate_Medal"][2],'',['class'=>'control-label col-md-2'])?>
					<div class='col-md-6'>
						<?php
						echo $form->field ( $model, 'rate_bronze' )->widget ( TouchSpin::className (), [ 
								'options' => [ 
										'class' => 'form-control ',
										'value' => isset ( $rate_bronze ) ? $rate_bronze : $model->rate_bronze 
								],
								'pluginOptions' => [ 
										'verticalbuttons' => true,
										'verticalupclass' => 'glyphicon glyphicon-plus',
										'verticaldownclass' => 'glyphicon glyphicon-minus' 
								] 
						] )->label ( false );
						?>
					</div>

			</div>
		</div>
	</div>
</div>