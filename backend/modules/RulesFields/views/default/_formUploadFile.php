<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\assets\UserProfile;

/**
 * *select2 uses***
 */
use kartik\select2\Select2;
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\Rules;
use backend\models\RulesSettings;
use backend\models\Event;
use backend\models\Fields;
use backend\models\Subfields;
use yii\web\View;
use backend\modules\RulesFields\models\Athletics;
use yii\helpers\Url;

use kartik\file\FileInput;
use yii\base\Widget;

UserProfile::register ( $this );
$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3 col-sm-3 control-label';
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>
<div class='row'>
	<div class='form-group'>
	
											<?php echo Html::label(Athletics::Labels["file"][0],'DocFile_Rules',['class'=>'control-label pull-left'])?>
											<?php
											
											echo $form->field ( $model, 'DocFile_Rules', [ 
													'options' => [ 
															'class' => $formGroupClass . 'form-control' 
													],
													'template' => $template_medium 
											] )->widget ( FileInput::classname (), [ 
													'id' => 'file_input_rules_DocFile_Rules',
													'language' => 'fa',
													'options' => [ 
															'multiple' => true,
															'layoutTemplates' => 'modal' 
													],
													'pluginOptions' => [ 
															'showUpload' => false,
															'showPreview' => false,
															'maxFileCount' => 1 
													] 
											] )->label ( false );
											?>
											<span><?php echo Rules::getDocumentFile($model->eventId,$model->fieldsId) ;?></span>
											<?= Html::error($model, 'DocFile_Rules');?>
</div>
</div>

<div class='row'>
	<div class='form-group'>
								<?php echo Html::label(Athletics::Labels["file"][1],'',['class'=>'control-label pull-left'])?>
								<?php
								
								echo $form->field ( $model, 'PdfFile_Rules', [ 
										'options' => [ 
												'class' => $formGroupClass . 'form-control' 
										],
										'template' => $template_medium 
								] )->widget ( FileInput::classname (), [ 
										'id' => 'file_input_rules_PdfFile_Rules',
										'language' => 'fa',
										'options' => [ 
												'multiple' => true,
												'layoutTemplates' => 'modal' 
										],
										'pluginOptions' => [ 
												'showUpload' => false,
												'showPreview' => false,
												'maxFileCount' => 1 
										] 
								] )->label ( false );
								?>
										<span><?php echo Rules::getPdfFile($model->eventId,$model->fieldsId) ;?></span>
								<?= Html::error($model, 'PdfFile_Rules');?>
</div>
</div>
