	<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	
	/**
	 * *select2 uses***
	 */
	use kartik\touchspin\TouchSpin;
	use yii\web\JsExpression;
	use yii\web\View;
	use backend\modules\RulesFields\models\MessageRules;
	
	$class_tochSpin_min = 'col-md-2';
	$input_class = 'col-md-3';
	$formGroupClass = Yii::$app->params ['formGroupClass'];
	$template = '<div class="col-md-12 col-sm-12">{input} {hint} {error}</div>';
	$inputClass = Yii::$app->params ['inputClass'];
	$labelClass = 'pull-left control-label';
	$errorClass = Yii::$app->params ['errorClass'];
	$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
	$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
	?>
<div class='row'>

	<div class='form-group '>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Double'][0],'',['class'=>'control-label pull-left'])?>
				<div class='<?=$input_class?>'>
				<?php
				echo Html::activeTextInput ( $model, 'TypeOfMatch_Double', [ 
						'class' => 'form-control',
						'value' => isset ( $TypeOfMatch_Double ) ? $TypeOfMatch_Double : $model->TypeOfMatch_Double 
				] )?>
				</div>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Double'][1],'',['class'=>'control-label pull-left'])?>
				<div class='<?= $class_tochSpin_min ?>'>
					<?php
					
					echo $form->field ( $model, 'Double_Set1', [ 
							'template' => $template 
					] )->widget ( TouchSpin::className (), [ 
							'options' => [ 
									'class' => 'form-control ',
									'value' => isset ( $Double_Set1 ) ? $Double_Set1 : $model->Double_Set1 
							],
							'pluginOptions' => [ 
									'verticalbuttons' => true,
									'verticalupclass' => 'glyphicon glyphicon-plus',
									'verticaldownclass' => 'glyphicon glyphicon-minus' 
							] 
					] );
					?>
				</div>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Double'][2],'',['class'=>'control-label pull-left'])?>
				<div class='<?= $class_tochSpin_min ?>'>
					<?php
					
					echo $form->field ( $model, 'Double_Set2', [ 
							'template' => $template 
					] )->widget ( TouchSpin::className (), [ 
							'options' => [ 
									'class' => 'form-control ',
									'value' => isset ( $Double_Set2 ) ? $Double_Set2 : $model->Double_Set2 
							],
							'pluginOptions' => [ 
									'verticalbuttons' => true,
									'verticalupclass' => 'glyphicon glyphicon-plus',
									'verticaldownclass' => 'glyphicon glyphicon-minus' 
							] 
					] );
					?>
				</div>
				<?= Html::label(MessageRules::Labels['TypeOfMatch_Double'][3],'',['class'=>'control-label pull-left'])?>
			</div>
</div>