<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\assets\UserProfile;

/**
 * *select2 uses***
 */
use kartik\select2\Select2;
use kartik\sortinput\SortableInput;
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\Rules;
use backend\models\RulesSettings;
use backend\models\Event;
use backend\models\Fields;
use backend\models\Subfields;
use yii\web\View;
use backend\modules\RulesFields\models\Football;
use yii\helpers\Url;

use kartik\file\FileInput;
use yii\base\Widget;

UserProfile::register ( $this );
$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>

<?php $Question_No=1;?>
<div id="results" style="display: none"></div>

<?php $fields = Fields::findOne ( $model->fieldsId );?>

<div class="row">
	<div class="col-md-12">
		
				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','class'=>'form-horizontal']]); ?>
				<?php
				echo $this->render ( '../default/_formRight', [ 
						'model' => $model,
						'form' => $form 
				] );
				?>	
			<div class='profile-content'>
			<div class="row">
				<div class='col-md-12'>
					<div id="results_expand_msg" style="display: none"></div>
					<div class='portlet light'>
						<div class='portlet-body '>
							<div class='form-body'>
								<div class='row'>
									<div class="col-md-11 col-md-offset-1 col-sm-12 col-xs-12">
										<div class='form-group'>
								<?=  $form->errorSummary($model);  ?>
				<?= Html::activeHiddenInput($model, 'eventId')?>
				<?=  Html::activeHiddenInput($model, 'fieldsId')?>
				<?=  Html::activeHiddenInput($model, 'subFieldsId')?>
			 </div>

		<?php
		echo $this->render ( '../default/_formUploadFile', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
		
		<?php
		echo $this->render ( '../default/_formLatestRules', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formNumSportsman', [ 
				'model' => $model,
				'form' => $form ,
				'Min_Num_Sportsman'=>(empty($model->Min_Num_Sportsman)|| is_null($model->Min_Num_Sportsman))?11:$model->Min_Num_Sportsman,
				'Max_Num_Sportsman'=>(empty($model->Max_Num_Sportsman)|| is_null($model->Max_Num_Sportsman))?18:$model->Max_Num_Sportsman,
		] );
		?>		
		<?php
		echo $this->render ( '../default/_formMember', [ 
				'model' => $model,
				'form' => $form,
				'showMessage' => true ,
				'Num_Coach'=>(empty($model->Num_Coach)|| is_null($model->Num_Coach))?1:$model->Num_Coach,
				'Num_Supervisor'=>(empty($model->Num_Supervisor)|| is_null($model->Num_Supervisor))?1:$model->Num_Supervisor,
				'Num_CoachAssistant'=>(empty($model->Num_CoachAssistant)|| is_null($model->Num_CoachAssistant))?'':$model->Num_CoachAssistant,
				'Num_HeadCoach'=>(empty($model->Num_HeadCoach)|| is_null($model->Num_HeadCoach))?'':$model->Num_HeadCoach,
				
		] );
		?>	
		<?= $this->render ( '../default/_sections' );?>	
			

										<div class='row'>
											<div class='form-group'>
												<?= Html::label(Football::Labels["Time_Palay"][0],'',['class'=>'control-label pull-left']);?>
												<div class='<?= $class_tochSpin_min ?>'>
											 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Palay_Time','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
											 	</div>
												<?= Html::label(Football::Labels["Time_Palay"][1],'',['class'=>'control-label pull-left']);?>
												<?= Html::label(Football::Labels["Time_Palay"][2],'',['class'=>'control-label pull-left']);?>
												<div class='<?= $class_tochSpin_min ?>'>
											 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Break_Time','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
											 	</div>
												<?= Html::label(Football::Labels["Time_Palay"][3],'',['class'=>'control-label pull-left']);?>
											</div>
										</div>

										<div class='row'>
											<div class='form-group'>
														<?= Html::label(Football::Labels["Num_Group"][0],'',['class'=>'control-label pull-left']);?>
														<div class='<?= $class_tochSpin_min ?>'>
													 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Groups','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
													 	</div>
														<?= Html::label(Football::Labels["Num_Group"][1],'',['class'=>'control-label pull-left']);?>
														<div class='<?= $class_tochSpin_min ?>''>
													 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Team_Group','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
													 	</div>
														<?= Html::label(Football::Labels["Num_Group"][2],'',['class'=>'control-label pull-left']);?>
													</div>
										</div>


										<div class='row'>
											<div class='form-group'>
														<?= Html::label(Football::Labels["Num_Group"][3],'',['class'=>'control-label pull-left']);?>
														<div class='<?= $class_tochSpin_min ?>''>
													 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Num_Promoted_Team','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
													 	</div>
														<?= Html::label(Football::Labels["Num_Group"][4],'',['class'=>'control-label pull-left']);?>
														<div class='<?= $class_tochSpin_min ?>'>
													 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Num_Rate_NextStage','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
													 	</div>
														<?= Html::label(Football::Labels["Num_Group"][5],'',['class'=>'control-label pull-left']);?>
													</div>
										</div>


										<div class='row'>
											<div class='form-group'>
													<?= Html::label(Football::Labels["Scoring"][0],'',['class'=>'control-label pull-left']);?>
													<div class='<?= $class_tochSpin_min ?>'>
												 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_Winning_Team','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												 	</div>
													<?= Html::label(Football::Labels["Scoring"][1],'',['class'=>'control-label pull-left']);?>
													<div class='<?= $class_tochSpin_min ?>'>
												 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_Equal_Team','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												 	</div>
													<?= Html::label(Football::Labels["Scoring"][2],'',['class'=>'control-label pull-left']);?>
													<div class='<?= $class_tochSpin_min ?>'>
												 	<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Score_Losers_Team','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												 	</div>
													<?= Html::label(Football::Labels["Scoring"][3],'',['class'=>'control-label pull-left']);?>
												</div>
										</div>
										
										
		<?= $this->render ( '../default/_sections' );?>	
		
										<div class='row'>
											<div class='form-group'>
														<?= Html::label(Football::Labels["BasicCourse_Equality"][0],'',['class'=>'control-label']);?>
													</div>
											<div class="row">
												<div class="col-sm-5">
															<?php
															echo SortableInput::widget ( [ 
																	'name' => 'kv-conn-1',
																	'items' => Football::ArrayList_BasicCourse_Equality_Classification,
																	'value' => implode ( ',', array_diff ( array (
																			1,
																			2,
																			3 
																	), explode ( ",", $model->BasicCourse_Equality_Classification ) ) ),
																	'hideInput' => false,
																	'sortableOptions' => [ 
																			'connected' => true 
																	],
																	'options' => [ 
																			'class' => 'form-control',
																			'readonly' => false 
																	] 
															] );
															?>
															</div>
												<div class="col-sm-5">
															<?php
															echo $form->field ( $model, 'BasicCourse_Equality_Classification' )->widget ( SortableInput::classname (), [ 
																	'name' => 'Football[BasicCourse_Equality_Classification]',
																	'items' => (is_null ( $model->BasicCourse_Equality_Classification ) || empty ( $model->BasicCourse_Equality_Classification )) ? array () : Football::ArrayList_BasicCourse_Equality_Classification,
																	'value' => $model->BasicCourse_Equality_Classification,
																	'hideInput' => false,
																	'sortableOptions' => [ 
																			'itemOptions' => [ 
																					'class' => 'alert alert-warning' 
																			],
																			'connected' => true 
																	],
																	'options' => [ 
																			'class' => 'form-control',
																			'readonly' => false 
																	] 
															] );
															?>
													</div>
											</div>

										</div>


		<?= $this->render ( '../default/_sections' );?>	
										<div class='row'>
											<div class='form-group'>
												<?= Html::label(Football::Labels["PlayOff_Equality"][0],'',['class'=>'control-label pull-left']);?>
												<div class='<?=$input_class?>'><?= Html::activeTextInput($model,'PlayOff_Equality_Classification',['class'=>'form-control ']);?></div>
												<?= Html::label(Football::Labels["PlayOff_Equality"][1],'',['class'=>'control-label pull-left']);?>
											</div>
										</div>


		<?= $this->render ( '../default/_sections' );?>	
											<div class='row'>
											<div class='form-group'>
												<?= Html::label(Football::Labels["Final_Equality"][0],'',['class'=>'control-label pull-left']);?>
												<div class='<?=$input_class?>'><?= Html::activeTextInput($model,'Final_Equality_Classification1',['class'=>'form-control ']);?></div>
												<?= Html::label(Football::Labels["Final_Equality"][1],'',['class'=>'control-label pull-left']);?>
											</div>
											<div class='form-group'>
												<?= Html::label(Football::Labels["Final_Equality"][2],'',['class'=>'control-label pull-left']);?>
												<div class='<?=$input_class?>'><?= Html::activeTextInput($model,'Final_Equality_Classification2',['class'=>'form-control ']);?></div>
												<?= Html::label(Football::Labels["Final_Equality"][3],'',['class'=>'control-label pull-left']);?>
											</div>
										</div>

		<?= $this->render ( '../default/_sections' );?>	
				<?php
		echo $this->render ( '../default/_formCoach', [ 
				'model' => $model,
				'form' => $form ,
					'NumberOfTeams_Coach' => (empty ( $model->NumberOfTeams_Coach ) || is_null ( $model->NumberOfTeams_Coach )) ? 1 : $model->NumberOfTeams_Coach ,
		] );
		?>		
				
		<?= $this->render ( '../default/_sections' );?>							
		<?php
		echo $this->render ( '../default/_formTechnicalCommittee', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>						
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRate', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRateMedal', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		


									</div>
								</div>
							</div>
						</div>
					</div>




				</div>
			</div>
		</div>
				<?php ActiveForm::end(); ?>
	</div>
</div>





























