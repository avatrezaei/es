<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\assets\UserProfile;

/**
 * *select2 uses***
 */
use kartik\select2\Select2;
use kartik\sortinput\SortableInput;
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\Rules;
use backend\models\RulesSettings;
use backend\models\Event;
use backend\models\Fields;
use backend\models\Subfields;
use yii\web\View;
use backend\modules\RulesFields\models\Swimming;
use yii\helpers\Url;

use kartik\file\FileInput;
use yii\base\Widget;

UserProfile::register ( $this );
$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>

<?php $Question_No=1;?>
<div id="results" style="display: none"></div>

<?php $fields = Fields::findOne ( $model->fieldsId );?>

<div class="row">
	<div class="col-md-12">
				<?php  $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','class'=>'form-horizontal']]); ?>
			<div class='profile-sidebar'>
			<div class="portlet light profile-sidebar-portlet ">

				<div class="profile-userpic">
					<?php echo $fields->getImage($fields,'user-pic rounded img-responsive');?>
					</div>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"><?php Event::findByPK($model->eventId)->name; ?></div>
					<div class="profile-usertitle-job"><?= Yii::t('app','Field').': '.Fields::findByPK($model->fieldsId)->name; ?></div>
				</div>
				<div class="profile-usermenu"></div>
				<div class="profile-userbuttons">
						<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn  green btn-sm'])?>
						<?= Html::a(Yii::t('app', 'Close'),Url::to(['/rules/index']), ['class' => 'btn  red btn-sm'])?>
						
					</div>
				<div class="profile-usermenu"></div>
			</div>

		</div>
		<div class='profile-content'>
			<div class="row">
				<div class='col-md-12'>
					<div id="results_expand_msg" style="display: none"></div>
					<div class='portlet light'>
						<div class='portlet-body'>
							<div class='form-body'>
								<div class='row'>
									<div class="col-md-11 col-md-offset-1 col-sm-12 col-xs-12">
										<div class='form-group'>
								<?=  $form->errorSummary($model);  ?>
				<?= Html::activeHiddenInput($model, 'eventId')?>
				<?=  Html::activeHiddenInput($model, 'fieldsId')?>
				<?=  Html::activeHiddenInput($model, 'subFieldsId')?>
			 </div>

											<?php
											echo $this->render ( '../default/_formUploadFile', [ 
													'model' => $model,
													'form' => $form 
											] );
											?>	
		<?= $this->render ( '../default/_sections' );?>	
										
											<?php
											echo $this->render ( '../default/_formLatestRules', [ 
													'model' => $model,
													'form' => $form 
											] );
											?>	
		
		<?= $this->render ( '../default/_sections' );?>	
										
										<div class="row">
											<div class='form-group '>
												<!-- <label> تعداد نفرات شرکت کندده در هر دانشگاه طبق سهمیه های مندرج در جداول ارسال تعیین گردیده است</label> -->
												 <?= Html::activeCheckbox($model,'Accept_People_According_Quotas'); ?>
												 <?= Html::error($model, 'Accept_People_According_Quotas');?>
									 	</div>
										</div>

										<div class="row">
											<div class='form-group '>
													<?= Html::label(Swimming::Labels["University"][0],'',['class'=>'control-label pull-left'])?>
														<div class='<?= $class_tochSpin_min ?>'>
															<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'University_Single','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
														</div>
										 
													<?= Html::label(Swimming::Labels["University"][1],'',['class'=>'control-label pull-left'])?>
													<div class='<?= $class_tochSpin_min ?>'>
														<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'University_Team','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
													</div>
													<?= Html::label(Swimming::Labels["University"][2],'',['class'=>'control-label pull-left'])?>
													 <?= Html::error($model, 'University_Single');?>
													 <?= Html::error($model, 'University_Team');?>
												</div>
										</div>

										<div class="row">
											<div class='form-group '>
												<?= Html::label(Swimming::Labels["Sportsman"][0],'',['class'=>'control-label pull-left'])?>
													<div class='<?= $class_tochSpin_min ?>'>
														<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Sportsman_Single','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
													</div>
									 
												<?= Html::label(Swimming::Labels["Sportsman"][1],'',['class'=>'control-label pull-left'])?>
												<div class='<?= $class_tochSpin_min ?>'>
													<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Sportsman_Team','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												</div>
									 
									 
												<?= Html::label(Swimming::Labels["Sportsman"][2],'',['class'=>'control-label pull-left'])?>
												 <?= Html::error($model, 'Sportsman_Single');?>
												 <?= Html::error($model, 'Sportsman_Team');?>
											 </div>
										</div>

										<div class="row">
											<div class='form-group '>
													<?= Html::label(Swimming::Labels["Game"][0],'',['class'=>'control-label pull-left']);?>
										 			<div class='<?= $class_tochSpin_min ?>'>
														<?php
														echo TouchSpin::widget ( [ 
																'model' => $model,
																'attribute' => 'game_Single',
																'options' => [ 
																		'class' => 'form-control ' 
																],
																'pluginOptions' => [ 
																		'verticalbuttons' => true,
																		'verticalupclass' => 'glyphicon glyphicon-plus',
																		'verticaldownclass' => 'glyphicon glyphicon-minus' 
																] 
														] );
														?>
													</div>
													<?= Html::label(Swimming::Labels["Game"][1],'',['class'=>'control-label pull-left']);?>
										 			<div class='<?= $class_tochSpin_min ?>'>
														<?php
														echo TouchSpin::widget ( [ 
																'model' => $model,
																'attribute' => 'game_Team',
																'options' => [ 
																		'class' => 'form-control ' 
																],
																'pluginOptions' => [ 
																		'verticalbuttons' => true,
																		'verticalupclass' => 'glyphicon glyphicon-plus',
																		'verticaldownclass' => 'glyphicon glyphicon-minus' 
																] 
														] );
														?>
													</div>
													<?= Html::label(Swimming::Labels["Game"][2],'',['class'=>'control-label pull-left']);?>
												<?= Html::error($model, 'game_Single');?>
											 	<?= Html::error($model, 'game_Team');?>
												</div>
										</div>
										
										<?php
		echo $this->render ( '../default/_formMember', [ 
				'model' => $model,
				'form' => $form,
				'showMessage' => true ,
				'Num_Coach'=>(empty($model->Num_Coach)|| is_null($model->Num_Coach))?1:$model->Num_Coach,
				'Num_Supervisor'=>(empty($model->Num_Supervisor)|| is_null($model->Num_Supervisor))?1:$model->Num_Supervisor,
				'Num_CoachAssistant'=>(empty($model->Num_CoachAssistant)|| is_null($model->Num_CoachAssistant))?'':$model->Num_CoachAssistant,
				'Num_HeadCoach'=>(empty($model->Num_HeadCoach)|| is_null($model->Num_HeadCoach))?'':$model->Num_HeadCoach,
				
		] );
		?>	

		<?= $this->render ( '../default/_sections' );?>	
										
			<?php
		echo $this->render ( '../default/_formCoach', [ 
				'model' => $model,
				'form' => $form ,
					'NumberOfTeams_Coach' => (empty ( $model->NumberOfTeams_Coach ) || is_null ( $model->NumberOfTeams_Coach )) ? 1 : $model->NumberOfTeams_Coach ,
		] );
		?>			

								
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRate', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRateMedal', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
		
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
				<?php ActiveForm::end(); ?>
	</div>
</div>









