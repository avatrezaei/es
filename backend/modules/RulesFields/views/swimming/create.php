<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\Swimming */

$this->title = Yii::t ( 'app', 'Create Swimming' );
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Swimmings' ) 
];
?>
<div class="swimming-create">


	<div class='row'>
		<div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
				<?=$this->render ( '_form', [ 'model' => $model ] )?>
	</div>
	</div>
</div>
