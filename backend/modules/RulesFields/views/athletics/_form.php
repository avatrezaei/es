<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\assets\UserProfile;

/**
 * *select2 uses***
 */
use kartik\select2\Select2;
use kartik\touchspin\TouchSpin;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\models\Rules;
use backend\models\RulesSettings;
use backend\models\Event;
use backend\models\Fields;
use backend\models\Subfields;
use yii\web\View;
use backend\modules\RulesFields\models\Athletics;
use yii\helpers\Url;

use kartik\file\FileInput;
use yii\base\Widget;

UserProfile::register ( $this );
$class_tochSpin_min = 'col-md-2';
$input_class = 'col-md-3';
$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = Yii::$app->params ['template'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = Yii::$app->params ['labelClass'];
$errorClass = Yii::$app->params ['errorClass'];
$template_small = '{label}<div class="col-md-3 col-sm-3">{input} {hint} {error}</div>';
$template_medium = '{label}<div class="col-md-5 col-sm-5">{input} {hint} {error}</div>';
?>

<?php $Question_No=1;?>
<div id="results" style="display: none"></div>

<?php $fields = Fields::findOne ( $model->fieldsId );?>

<div class="row">
	<div class="col-md-12">
		
			<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','class'=>'form-horizontal']]); ?>
			<?php
			echo $this->render ( '../default/_formRight', [ 
					'model' => $model,
					'form' => $form 
			] );
			?>	
			<div class='profile-content'>
			<div class="row">
				<div class='col-md-12'>
					<div id="results_expand_msg" style="display: none"></div>
					<div class='portlet light'>
						<div class='portlet-body'>
							<div class='form-body'>
								<div class='row'>
									<div class="col-md-11 col-md-offset-1 col-sm-12 col-xs-12">
										<div class='form-group'>
												<?=  $form->errorSummary($model);  ?>
												<?= Html::activeHiddenInput($model, 'eventId')?>
												<?=  Html::activeHiddenInput($model, 'fieldsId')?>
												<?=  Html::activeHiddenInput($model, 'subFieldsId')?>
											 </div>

		<?php
		echo $this->render ( '../default/_formUploadFile', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>
											
		<?= $this->render ( '../default/_sections' );?>	
		
		<?php
		echo $this->render ( '../default/_formLatestRules', [ 
				'model' => $model,
				'form' => $form ,
				'Latest_Rules'=>(empty($model->Latest_Rules)|| is_null($model->Latest_Rules))?'':$model->Latest_Rules,
				'Ways_Of_Holding'=>(empty($model->Ways_Of_Holding)|| is_null($model->Ways_Of_Holding))?'':$model->Ways_Of_Holding,
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
			
											<div class="row">
											<div class='form-group'>
											 	<?= Html::label(Athletics::Labels["Allowed_University_Fields"][0],'',['class'=>'control-label pull-left'])?>
											 	<div class='<?= $class_tochSpin_min ?>'>
													<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Allowed_University_Fields','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												</div>
											 	<?= Html::label(Athletics::Labels["Allowed_University_Fields"][1],'',['class'=>'control-label pull-left'])?>
											 	<?= Html::error($model,'Allowed_University_Fields');?>
											 </div>
											<div class='form-group'>
												<?= Html::label(Athletics::Labels["Allowed_Sportman_Fields"][0],'',['class'=>'control-label pull-left'])?>
												<div class='<?= $class_tochSpin_min ?>'>
													<?=TouchSpin::widget ( [ 'model' => $model,'attribute' => 'Allowed_Sportman_Fields','options' => [ 'class' => 'form-control ' ],'pluginOptions' => [ 'verticalbuttons' => true,'verticalupclass' => 'glyphicon glyphicon-plus','verticaldownclass' => 'glyphicon glyphicon-minus' ] ] );?>
												</div>
											 	<?= Html::label(Athletics::Labels["Allowed_Sportman_Fields"][1],'',['class'=>'control-label pull-left'])?>
											 	<?= Html::error($model,'Allowed_Sportman_Fields');?>
											</div>
										</div>
		<?php
		echo $this->render ( '../default/_formMember', [ 
				'model' => $model,
				'form' => $form,
				'showMessage' => true ,
				'Num_Coach'=>(empty($model->Num_Coach)|| is_null($model->Num_Coach))?1:$model->Num_Coach,
				'Num_Supervisor'=>(empty($model->Num_Supervisor)|| is_null($model->Num_Supervisor))?1:$model->Num_Supervisor,
				'Num_CoachAssistant'=>(empty($model->Num_CoachAssistant)|| is_null($model->Num_CoachAssistant))?'':$model->Num_CoachAssistant,
				'Num_HeadCoach'=>(empty($model->Num_HeadCoach)|| is_null($model->Num_HeadCoach))?'':$model->Num_HeadCoach,
				
		] );
		?>	
		
		<?= $this->render ( '../default/_sections' );?>	
										
		<?php
		echo $this->render ( '../default/_formScoring', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	
							
		<?= $this->render ( '../default/_sections' );?>	
										
		<?php
		echo $this->render ( '../default/_formCoach', [ 
				'model' => $model,
				'form' => $form ,
					'NumberOfTeams_Coach' => (empty ( $model->NumberOfTeams_Coach ) || is_null ( $model->NumberOfTeams_Coach )) ? 1 : $model->NumberOfTeams_Coach ,
		] );
		?>					
		
		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRate', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	

		<?= $this->render ( '../default/_sections' );?>	
			
		<?php
		echo $this->render ( '../default/_formRateMedal', [ 
				'model' => $model,
				'form' => $form 
		] );
		?>	

										

										

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
				<?php ActiveForm::end(); ?>
	</div>
</div>









