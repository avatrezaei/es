<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\RulesFields\models\Wrestling */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Wrestling',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wrestlings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="wrestling-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
