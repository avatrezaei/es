<?php

namespace backend\modules\RulesFields;

class RulesFields extends \yii\base\Module
{
	public $controllerNamespace = 'backend\modules\RulesFields\controllers';

	public function init()
	{
		parent::init();

		// custom initialization code goes here
	}
}
