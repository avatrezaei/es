<?php

namespace backend\modules\RulesFields\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\RulesFields\models\Tennis;

/**
 * TennisSearch represents the model behind the search form about `backend\modules\RulesFields\models\Tennis`.
 */
class TennisSearch extends Tennis
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'eventId', 'fieldsId', 'subFieldsId'], 'integer'],
			[['name', 'value'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Tennis::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'eventId' => $this->eventId,
			'fieldsId' => $this->fieldsId,
			'subFieldsId' => $this->subFieldsId,
		]);

		$query->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'value', $this->value]);

		return $dataProvider;
	}
}
