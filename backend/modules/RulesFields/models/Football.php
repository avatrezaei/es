<?php

namespace backend\modules\RulesFields\models;

use Yii;
use \backend\models\Rules;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Football extends Rules {
	
	public $Min_Num_Sportsman;
	public $Max_Num_Sportsman;
	public $Palay_Time;
	public $Break_Time;
	public $Groups;
	public $Team_Group;
	public $Num_Promoted_Team; // /تعداد تیم هایی که به مرحله بعد صعود میکنند
	public $Num_Rate_NextStage; // /
	public $Score_Winning_Team;
	public $Score_Losers_Team;
// 	public $Score_GiveUp_Team;
	public $Score_Equal_Team;
	public $BasicCourse_Equality_Classification;
	public $PlayOff_Equality_Classification;
	public $Final_Equality_Classification1;
	public $Final_Equality_Classification2;
	public $Head_Association;
	public $Technical_Supervisor;
	public $One_Committee;
	public $Two_From_Coach_Supervisor;
	
	const List_BasicCourse_Equality_Classification = [ 
			1 => "نتیجه بازی رو در رو در دور مقدماتی",
			2 => "گل آوراژ(تفاضل گل)",
			3 => "گل زده بیشتر" 
	];
	const ArrayList_BasicCourse_Equality_Classification = [ 
			1 => [ 
					'content' => Football::List_BasicCourse_Equality_Classification [1] 
			],
			2 => [ 
					'content' => Football::List_BasicCourse_Equality_Classification [2] 
			],
			3 => [ 
					'content' => Football::List_BasicCourse_Equality_Classification [3] 
			] 
	];
	const Labels = [ 
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Num_Sportsman' => [ 
					'هر تیم متشکل از حداقل',
					' تن ورزشکار و حداکثر',
					'تن ورزشکار',
					'مربی',
					'سرپرست میباشد' 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'Ways_Of_Holding' => [ 
					'شیوه برگزاری مسابقات براساس',
					'می باشد' 
			],
			'Time_Palay' => [ 
					'مدت هر بازی',
					'دقیقه',
					'و در بین دو نیمه ',
					'دقیقه استراحت داده خواهد شد' 
			],
			'Num_Group' => [ 
					'مسابقات در ',
					'گروه',
					'تیمی انجام خواهد شد.',
					'تیم های اول تا',
					'هر گروه برای تعیین رتبه های اول تا ',
					'به دور بعد صعود خواهند کرد.' 
			],
			'Scoring' => [ 
					'برای هر برد ',
					'امتیاز و مساوی ',
					'امتیاز و بازنده ',
					'امتیاز تعلق میگیرد' 
			],
			'BasicCourse_Equality' => [ 
					'در صورت مساوی بودن امتیاز تیم ها در دور مقدماتی  از دستور عمل زیر برای رتبه بندی استفاده خواهد شد' 
			],
			'PlayOff_Equality' => [ 
					'پس از مرحله مقدماتی و در مرحله حذفی در صورت تساوی پس از 90 دقیقه',
					' تیم برنده را مشخص خواهد کرد.' 
			],
			'Final_Equality' => [ 
					'در فینال مسابقات در صورت تساوی پس از پایان مسابقه از',
					'استفاده خواهد شد و',
					'در صورت تساوی',
					'تیم قهرمان را مشخص خواهد کرد.' 
			],
			'Rate' => [ 
					'در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			],
			'Technical_Committee' => [ 
					'اعضاي كميته ي فنی:' 
			] 
	];
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[ 
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6' ,
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze',
								/////////////////////////////////////////////////
								'eventId',
								'fieldsId',
								'subFieldsId',
								'Min_Num_Sportsman',
								'Max_Num_Sportsman',
								'Palay_Time',
								'Break_Time',
								'Groups',
								'Team_Group',
								'Num_Promoted_Team',
								'Num_Rate_NextStage',
								'Score_Winning_Team',
								'Score_Losers_Team',
								'Score_Equal_Team',
								'Head_Association',
								'Technical_Supervisor',
								'One_Committee',
								'Two_From_Coach_Supervisor',
								
								
						],
						'integer' 
				],
				[ 
						[ 
								'Latest_Rules',
								'Ways_Of_Holding',
								/////////////////////////
								'name',
								'value',
								'BasicCourse_Equality_Classification',
								'PlayOff_Equality_Classification',
								'Final_Equality_Classification1',
								'Final_Equality_Classification2' 
						]
						,
						'string',
						'max' => 100 
				],
				[ 
						[ ]

						,
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'DocFile_Rules' => 'فایل قوانین و مقررات نسخه (doc) ',
				'PdfFile_Rules' => 'فایل قوانین و مقررات نسخه (pdf) ',
				
				'Num_Coach' => 'تعداد مربیان',
				'Num_CoachAssistant'=>'تعداد کمک مربی',
				'Num_HeadCoach'=>'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست',
				
				'Latest_Rules' => 'مسابقات طبق آخرین قوانین و مقرارت',
				'Ways_Of_Holding' => 'شیوه برگزاری مسابقات براساس',
				
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند',
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود',
				
				'rate_1' => 'مقام اول',
				'rate_2' => 'مقام دوم',
				'rate_3' => 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز',
				
				///////////////////////////////////
				
				'id' => Yii::t ( 'app', 'ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => Yii::t ( 'app', 'Name' ),
				'value' => Yii::t ( 'app', 'Value' ),
				 ///////////////////////////////////////////////////////
				
				'Min_Num_Sportsman' => 'حداقل تعداد بازیکنان',
				'Max_Num_Sportsman' => 'حداکثر تعداد بازیکنان',
				'Palay_Time' => '',
				'Break_Time' => '',
				'Groups' => '',
				'Team_Group' => '',
				'Num_Promoted_Team' => '',
				'Num_Rate_NextStage' => '',
				'Score_Winning_Team' => 'تیم برنده', 
				'Score_Losers_Team' => 'تیم بازنده', 
// 				'Score_GiveUp_Team' => ' تیمی که از ادامه بازی منصرف گردد ', 
				'Score_Equal_Team' => 'تیم هایی که تساوی کرده اند',
				'BasicCourse_Equality_Classification' => '',
				'PlayOff_Equality_Classification' => '',
				'Final_Equality_Classification1' => '',
				'Final_Equality_Classification2' => '',
				'Head_Association' => 'رییس انجمن',
				'Technical_Supervisor' => 'سرپرست فنی',
				'One_Committee' => 'یک نفر از کمیته داوران',
				'Two_From_Coach_Supervisor' => 'دو نفر از مربیان و سرپرستان',
				
		];
	}
	/**
	 * *
	 */
	/**
	 * *
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Latest_Rules',
				'Ways_Of_Holding',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				///////////////////////////////////////////////
				'Palay_Time',
				'Break_Time',
				'Groups',
				'Team_Group',
				'Num_Promoted_Team',
				'Num_Rate_NextStage',
				'Score_Winning_Team',
				'Score_Losers_Team',
				'Score_Equal_Team',
				'BasicCourse_Equality_Classification',
				'PlayOff_Equality_Classification',
				'Final_Equality_Classification1',
				'Final_Equality_Classification2',
				'Head_Association',
				'Technical_Supervisor',
				'One_Committee',
				'Two_From_Coach_Supervisor',
		];
	}

}
