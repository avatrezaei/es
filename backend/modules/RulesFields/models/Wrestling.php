<?php

namespace backend\modules\RulesFields\models;

use Yii;
use \backend\models\Rules;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Wrestling extends Rules {
	
	public $Check_Team_Member;
	public $Num_Weight_Freestyle;
	public $Num_Weight_GRECO;
	public $Head_Association;
	public $Technical_Supervisor;
	public $One_Committee;
	public $Two_From_Coach_Supervisor;
	const Labels = [ 
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Check_Team_Member' => [ 
					'تعداد نفرات شرکت کننده هر دانشگاه ، طبق سهمیه های مندرج در جدول ارسالی تعیین گردیده است' 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'Weight' => [ 
					'مسابقات در',
					' وزن قانونی کشتی آزاد ، در',
					' وزن قانونی کشتی فرنگی برگزار میشود.' 
			],
			'Rate' => [ 
					' در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			],
			'Technical_Committee' => [ 
					'اعضاي كميته ي فنی:' 
			] 
	];
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6' ,
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze',
								/////////////////////////////////////////////////
								'eventId',
								'fieldsId',
								'subFieldsId',
								'Check_Team_Member',
								'Num_Weight_Freestyle',
								'Num_Weight_GRECO',
								'Head_Association',
								'Technical_Supervisor',
								'One_Committee',
								'Two_From_Coach_Supervisor',
								
						],
						'integer' 
				],
				[ 
						[ 
								'Latest_Rules',
								'Ways_Of_Holding',
								/////////////////////////
								'name',
								'value',
						],
						'string',
						'max' => 100 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'DocFile_Rules' => 'فایل قوانین و مقررات نسخه (doc) ',
				'PdfFile_Rules' => 'فایل قوانین و مقررات نسخه (pdf) ',
				
				'Num_Coach' => 'تعداد مربیان',
				'Num_CoachAssistant'=>'تعداد کمک مربی',
				'Num_HeadCoach'=>'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست',
				
				'Latest_Rules' => 'مسابقات طبق آخرین قوانین و مقرارت',
				'Ways_Of_Holding' => 'شیوه برگزاری مسابقات براساس',
				
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند',
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود',
				
				'rate_1' => 'مقام اول',
				'rate_2' => 'مقام دوم',
				'rate_3' => 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز',
				
				///////////////////////////////////
				'id' => Yii::t ( 'app', 'ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => Yii::t ( 'app', 'Name' ),
				'value' => Yii::t ( 'app', 'Value' ),
				'Check_Team_Member' => '',
				'Num_Weight_Freestyle' => '',
				'Num_Weight_GRECO' => '',
				'Head_Association' => 'رییس انجمن',
				'Technical_Supervisor' => 'سرپرست فنی',
				'One_Committee' => 'یک نفر از کمیته داوران',
				'Two_From_Coach_Supervisor' => 'دو نفر از مربیان و سرپرستان',
		];
	}
	
	/*
	 */
	/**
	 * *
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Latest_Rules',
				'Ways_Of_Holding',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				///////////////////////////////////////////////
				'Check_Team_Member',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Num_Weight_Freestyle',
				'Num_Weight_GRECO',
				'Head_Association',
				'Technical_Supervisor',
				'One_Committee',
				'Two_From_Coach_Supervisor',
		];
	}
}
