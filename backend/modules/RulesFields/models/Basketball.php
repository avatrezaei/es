<?php

namespace backend\modules\RulesFields\models;

use Yii;
use \backend\models\Rules;
use function Faker\boolean;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Basketball extends Rules{
	
	public $Min_Num_Sportsman;
	public $Max_Num_Sportsman;
	public $Score_Winning_Team;
	public $Score_Losers_Team;
	public $Score_GiveUp_Team;
	/**
	 * *
	 */
	const Labels = [ 
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Num_Sportsman' => [ 
					'هر تیم متشکل از حداقل',
					' تن ورزشکار و حداکثر',
					'تن ورزشکار',
					'مربی',
					'سرپرست میباشد' 
			],
			'NumberOfTeams_Coach' => [ 
					'هر مربی میتواند فقط هدایت',
					'تیم را بر عهده داشته باشد' 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'Ways_Of_Holding' => [ 
					'شیوه برگزاری مسابقات براساس',
					'می باشد' 
			],
			'Scoring' => [ 
					'در طول برگزاری مسابقه امتیاز تیم ها به شرح زیر است',
					'تیم برنده',
					'تیم بازنده',
					' تیمی که از ادامه بازی منصرف گردد ' 
			],
			'Rate' => [ 
					'در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			] 
	];
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[ 
								'id',
								'eventId',
								'fieldsId',
								'subFieldsId' ,
						],
						'integer' 
				],
				[ 
						[ 
								'Latest_Rules',
								'Ways_Of_Holding',
								/////////////////////////
								'name',
								'value',
						],
						'string',
						'max' => 100 
				],
				[ 
						[ 
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6' ,
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze',
								/////////////////////////////////////////////////
								
								'Min_Num_Sportsman',
								'Max_Num_Sportsman',
								'Score_Winning_Team',
								'Score_Losers_Team',
								'Score_GiveUp_Team',
						],
						'integer' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'DocFile_Rules' => 'فایل قوانین و مقررات نسخه (doc) ',
				'PdfFile_Rules' => 'فایل قوانین و مقررات نسخه (pdf) ',
				
				'Num_Coach' => 'تعداد مربیان',
				'Num_CoachAssistant'=>'تعداد کمک مربی',
				'Num_HeadCoach'=>'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست',
				
				'Latest_Rules' => 'مسابقات طبق آخرین قوانین و مقرارت',
				'Ways_Of_Holding' => 'شیوه برگزاری مسابقات براساس',
				
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند',
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود',
				
				'rate_1' => 'مقام اول',
				'rate_2' => 'مقام دوم',
				'rate_3' => 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز',
				
				///////////////////////////////////
				'id' => '', // Yii::t ( 'app', 'ID' ),
				'eventId' => 'رویداد', // Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => 'رشته', // Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => 'زیر رشته', // Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => 'نام', // Yii::t ( 'app', 'Name' ),
				'value' => 'مقدار', // Yii::t ( 'app', 'Value' ),
				'Min_Num_Sportsman' => 'حداقل تعداد بازیکنان',
				'Max_Num_Sportsman' => 'حداکثر تعداد بازیکنان',
				'Score_Winning_Team' => 'تیم برنده', // Yii::t ( 'app', 'Score_Winning_Team' ),
				'Score_Losers_Team' => 'تیم بازنده', // Yii::t ( 'app', 'Score_Losers_Team' ),
				'Score_GiveUp_Team' => ' تیمی که از ادامه بازی منصرف گردد ', // Yii::t ( 'app', 'Score_GiveUp_Team' ),

		];
	}
	/**
	 * *
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Latest_Rules',
				'Ways_Of_Holding',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				///////////////////////////////////////////////
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				"Score_Winning_Team",
				"Score_Losers_Team",
				"Score_GiveUp_Team",
		];
	}
}
