<?php

namespace backend\modules\RulesFields\models;

use Yii;
use backend\models\Rules;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Swimming extends Rules {
	public $Accept_People_According_Quotas;
	public $Sportsman_Single;
	public $Sportsman_Team;
	public $University_Single;
	public $University_Team;
	public $game_Single;
	public $game_Team;
	/**
	 * *
	 */
	const Labels = [ 
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'NumberOfTeams_Coach' => [ 
					'هر مربی میتواند فقط هدایت',
					'تیم را بر عهده داشته باشد' 
			],
			'Sportsman' => [ 
					'هر ورزشکار مجاز است در',
					'ماده انفرادی',
					'ماده تیمی شرکت نماید.' 
			],
			'University' => [ 
					'هر دانشگاه در هر رشته انفرادی میتواند',
					'شرکت کننده و در تیمی',
					'تیم داشته باشد' 
			],
			'Game' => [ 
					'مسابقات در',
					'ماده انفرادی و',
					'ماده تیمی برگزار میشود' 
			],
			'Rate' => [ 
					' در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			] 
	];
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[ 
								'name',
								'value' 
						],
						'string',
						'max' => 100 
				],
				[ 
						[ 
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6',
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze',
								// ///////////////////////////////////////////////
								'Accept_People_According_Quotas',
								'Sportsman_Single',
								'Sportsman_Team',
								'University_Single',
								'University_Team',
								'game_Single',
								'game_Team',
								'NumberOfTeams_Coach' 
						],
						'integer' 
				],
				[ 
						[ 
								
								'Latest_Rules',
								'Ways_Of_Holding' 
						]
						// ///////////////////////
						,
						'string' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'DocFile_Rules' => 'فایل قوانین و مقررات نسخه (doc) ',
				'PdfFile_Rules' => 'فایل قوانین و مقررات نسخه (pdf) ',
				
				'Num_Coach' => 'تعداد مربیان',
				'Num_CoachAssistant' => 'تعداد کمک مربی',
				'Num_HeadCoach' => 'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست',
				
				'Latest_Rules' => 'مسابقات طبق آخرین قوانین و مقرارت',
				'Ways_Of_Holding' => 'شیوه برگزاری مسابقات براساس',
				
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند',
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود',
				
				'rate_1' => 'مقام اول',
				'rate_2' => 'مقام دوم',
				'rate_3' => 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز',
				
				// /////////////////////////////////
				'id' => Yii::t ( 'app', 'ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => Yii::t ( 'app', 'Name' ),
				'value' => Yii::t ( 'app', 'Value' ),
				'Accept_People_According_Quotas' => 'تعداد نفرات شرکت کندده در هر دانشگاه طبق سهمیه های مندرج در جداول ارسال تعیین گردیده است',
				// ///// 'تعداد نفرات شرکت کندده در هر دانشگاه طبق سهمیه های مندرج در جداول ارسال تعیین گردیده است',
				'Sportsman_Single' => 'ماده انفرادی مجاز ورزشکار',
				// ///// 'ماده انفرادی مجاز ورزشکار',
				'Sportsman_Team' => 'ماده تیمی مجاز ورزشکار',
				// ///// 'ماده تیمی مجاز ورزشکار',
				'University_Single' => 'رشته های انفرادی دانشگاه',
				// ///// 'رشته های انفرادی دانشگاه',
				'University_Team' => 'رشته های تیمی دانشگاه',
				// //// 'رشته های تیمی دانشگاه',
				'game_Single' => 'ماده انفرادی دانشگاه',
				// ///// 'ماده انفرادی دانشگاه',
				'game_Team' => 'ماده تیمی دانشگاه' 
		]
		// //// 'ماده تیمی دانشگاه',
		;
		// ///// 'مقام ششم'
	}
	/**
	 *
	 * @return array list of attribiut name
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Latest_Rules',
				'Ways_Of_Holding',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				// /////////////////////////////////////////////
				"Accept_People_According_Quotas",
				"Sportsman_Single",
				"Sportsman_Team",
				"University_Single",
				"University_Team",
				"game_Single",
				"game_Team" 
		];
	}
}
