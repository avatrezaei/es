<?php

namespace backend\modules\RulesFields\models;

use Yii;
use \backend\models\Rules;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Badminton extends Rules {
	public $Num_Team_Member;
	public $Min_Num_Sportsman;
	public $Max_Num_Sportsman;
	public $Is_Team;
	public $Is_Single;
	public $Is_Double;
	public $Num_TypeOfMatch;
	public $FirstPeriod_TypeOfMatch;
	public $NextPeriod_TypeOfMatch;
	public $Periodic_Classification;
	public $Equality_Classification;
	public $Allow_As_Single_Double;
	public $Allow_Sportsman_SingleOrDouble;
	public $Num_Sportsman_Single_University;
	public $Num_Team_Double_University;
	public $TypeOfMatch_Single_Double;
	public $Score_Winning_Team;
	public $Score_Losers_Team;
	public $Score_GiveUp_Team;
	public $Head_Association;
	public $Technical_Supervisor;
	public $One_Committee;
	public $Two_From_Coach_Supervisor;
	const Periodic_Classification_list = [ 
			1 => "تیم دیگر را برده باشد",
			2 => "تعداد بازی های برده بیشتری را دارد",
			3 => "تفاوت بین مجموع گیم بازی های برده و باخته بیشتری را دارد",
			4 => "تفاوت بین جمع امتیازات بازی های برده و باخته بیشتری را دارد" 
	];
	const Equality_Classification_list = [  // تقسیم بندی
			1 => "تعداد بازی های برده بیشتری دارد",
			2 => "تفاوت بین مجموع گیم بازی های برده و باخته انجام میگیرد",
			3 => "تفاوت بین جمع امتیازات بازی های برده و باخته انجام میگیرد" 
	];
	const Periodic_Classification_array = [ 
			1 => [ 
					'content' => "تیم دیگر را برده باشد" 
			],
			2 => [ 
					'content' => "تعداد بازی های برده بیشتری را دارد" 
			],
			3 => [ 
					'content' => "تفاوت بین مجموع گیم بازی های برده و باخته بیشتری را دارد" 
			],
			4 => [ 
					'content' => "تفاوت بین جمع امتیازات بازی های برده و باخته بیشتری را دارد" 
			] 
	];
	const Equality_Classification_array = [  // تقسیم بندی
			1 => [ 
					'content' => "تعداد بازی های برده بیشتری دارد" 
			],
			2 => [ 
					'content' => "تفاوت بین مجموع گیم بازی های برده و باخته انجام میگیرد" 
			],
			3 => [ 
					'content' => "تفاوت بین جمع امتیازات بازی های برده و باخته انجام میگیرد" 
			] 
	];
	const Labels = [ 
			'Periodic_Classification' => 'در مسابقات تیمی در مرحله دوره ای در صورت تساوی دو تیم از نظر برد یا باخت تیمی در رده بالاتر قرار میگیرد که به ترتیب :',
			'Equality_Classification' => 'در صورت تساوی بیش از دو تیم (سه تیم) در یک گروه تیمی در رده بالاتر قرار میگیرد که به ترتیب:',
			'Allow_Sportsman_SingleOrDouble' => 'در مسابقات یک نفره و دو نفره دانشگاه هر بازیکن میتواند در یکی از جداول یک نفره یا دو نفره حضور داشته باشد',
			'Allow_As_Single_Double' => 'در صورتیکه دانشگاهی نتواند در مسابقات تیمی شرکت نماید میتواند با حد اقل نفرات در جدول یک نفره و دو نفره دانشگاهی حضور داشته باشد',
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Num_Sportsman' => [ 
					'هر تیم متشکل از حداقل',
					' تن ورزشکار و حداکثر',
					'تن ورزشکار',
					'مربی',
					'سرپرست میباشد' 
			],
			'Num_University' => [ 
					"هر دانشگاه میتواند حداکثر ",
					"بازیکن برای جدول یک نفره و",
					"تیم برای جدول دو نفره معرفی نماید" 
			],
			'TypeOfMatch_Single_Double' => [ 
					"مسابقات یک نفره و دو نفره به صورت",
					"خواهد بود" 
			],
			'NumberOfTeams_Coach' => [ 
					'هر مربی میتواند فقط هدایت',
					'تیم را بر عهده داشته باشد' 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'Ways_Of_Holding' => [ 
					'شیوه برگزاری مسابقات براساس',
					'می باشد' 
			],
			'Num_TypeOfMatch' => [ 
					"مسابقات در",
					"قسمت ",
					"انفرادی",
					"دوبل",
					"دانشگاهي برگزار مي شود" 
			],
			'Period_TypeOfMatch' => [ 
					'مسابقات تیمی در دوره اول به صورت',
					'و در دوره های بعدی به صورت',
					'برگزار خواهد شد' 
			],
			'Scoring' => [ 
					'در طول برگزاری مسابقه امتیاز تیم ها به شرح زیر است',
					'تیم برنده',
					'تیم بازنده',
					' تیمی که از ادامه بازی منصرف گردد ' 
			],
			'Rate' => [ 
					' در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			],
			'Rate_Medal' => [ 
					'طلا',
					'نقره',
					'برنز' 
			],
			'Technical_Committee' => [ 
					'اعضاي كميته ي فنی:' 
			] 
	];
	
	// Score Medal
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[ 
								'Latest_Rules',
								'Ways_Of_Holding',
								// ///////////////////////
								'name',
								'value',
								'FirstPeriod_TypeOfMatch',
								'NextPeriod_TypeOfMatch',
								'TypeOfMatch_Single_Double',
								'Periodic_Classification',
								'Equality_Classification' 
						],
						'string' 
				],
				[ 
						[ 
								'Is_Team',
								'Is_Single',
								'Is_Double',
								'Allow_As_Single_Double',
								'Allow_Sportsman_SingleOrDouble',
								
								'Head_Association',
								'Technical_Supervisor',
								'One_Committee',
								'Two_From_Coach_Supervisor' 
						],
						'integer' 
				],
				[ 
						[ 
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6',
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze',
								// ///////////////////////////////////////////////
								'Num_Team_Member',
								'Min_Num_Sportsman',
								'Max_Num_Sportsman',
								'Num_TypeOfMatch',
								'Score_Winning_Team',
								'Score_Losers_Team',
								'Score_GiveUp_Team',
								'Num_Sportsman_Single_University',
								'Num_Team_Double_University' 
						],
						'integer' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'DocFile_Rules' => 'فایل قوانین و مقررات نسخه (doc) ',
				'PdfFile_Rules' => 'فایل قوانین و مقررات نسخه (pdf) ',
				
				'Num_Coach' => 'تعداد مربیان',
				'Num_CoachAssistant' => 'تعداد کمک مربی',
				'Num_HeadCoach' => 'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست',
				
				'Latest_Rules' => 'مسابقات طبق آخرین قوانین و مقرارت',
				'Ways_Of_Holding' => 'شیوه برگزاری مسابقات براساس',
				
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند',
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود',
				
				'rate_1' => 'مقام اول',
				'rate_2' => 'مقام دوم',
				'rate_3' => 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز',
				
				// /////////////////////////////////
				'id' => Yii::t ( 'app', 'ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => Yii::t ( 'app', 'Name' ),
				'value' => Yii::t ( 'app', 'Value' ),
				'Min_Num_Sportsman' => 'حداقل تعداد بازیکنان',
				'Max_Num_Sportsman' => 'حداکثر تعداد بازیکنان',
				'Is_Team' => 'تیمی',
				'Is_Single' => 'انفرادی',
				'Is_Double' => 'دوبل',
				'Num_TypeOfMatch' => 'تعداد انواع مسابقه',
				'FirstPeriod_TypeOfMatch' => 'نوع مسابقه در دوره اول',
				'NextPeriod_TypeOfMatch' => 'نوع مسابقه در دوره های بعد',
				'Periodic_Classification' => '',
				'Equality_Classification' => '',
				'Allow_As_Single_Double' => 'در صورتیکه دانشگاهی نتواند در مسابقات تیمی شرکت نماید میتواند با حد اقل نفرات در جدول یک نفره و دو نفره دانشگاهی حضور داشته باشد',
				'Allow_Sportsman_SingleOrDouble' => 'در مسابقات یک نفره و دو نفره دانشگاه هر بازیکن میتواند در یکی از جداول یک نفره یا دو نفره حضور داشته باشد',
				'Num_Sportsman_Single_University' => 'حداکثر بازیکن یک نفره برای هر دانشگاه',
				'Num_Team_Double_University' => 'حداکثر بازیکن دو نفره برای هر دانشگاه',
				'TypeOfMatch_Single_Double' => 'نوع مسابقات یک نفره و دونفره',
				'Score_Winning_Team' => 'تیم برنده', // Yii::t ( 'app', 'Score_Winning_Team' ),
				'Score_Losers_Team' => 'تیم بازنده', // Yii::t ( 'app', 'Score_Losers_Team' ),
				'Score_GiveUp_Team' => ' تیمی که از ادامه بازی منصرف گردد ', // Yii::t ( 'app', 'Score_GiveUp_Team' ),
				'Head_Association' => 'رییس انجمن',
				'Technical_Supervisor' => 'سرپرست فنی',
				'One_Committee' => 'یک نفر از کمیته داوران',
				'Two_From_Coach_Supervisor' => 'دو نفر از مربیان و سرپرستان' 
		];
	}
	/**
	 * *
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Latest_Rules',
				'Ways_Of_Holding',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				// /////////////////////////////////////////////
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Is_Team',
				'Is_Single',
				'Is_Double',
				'Num_TypeOfMatch',
				'FirstPeriod_TypeOfMatch',
				'NextPeriod_TypeOfMatch',
				'Periodic_Classification',
				'Equality_Classification',
				'Allow_As_Single_Double',
				'Allow_Sportsman_SingleOrDouble',
				'Num_Sportsman_Single_University',
				'Num_Team_Double_University',
				'TypeOfMatch_Single_Double',
				'Score_Winning_Team',
				'Score_Losers_Team',
				'Score_GiveUp_Team',
				'Head_Association',
				'Technical_Supervisor',
				'One_Committee',
				'Two_From_Coach_Supervisor' 
		];
	}
}
