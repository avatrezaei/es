<?php

namespace backend\modules\RulesFields\models;

use Yii;
use \backend\models\Rules;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Athletics extends Rules {
	// public $DocFile_Rules;
	// public $PdfFile_Rules;
	public $Allowed_University_Fields;
	public $Allowed_Sportman_Fields;
	public $Scoring_1;
	public $Scoring_2;
	public $Scoring_3;
	public $Scoring_4;
	public $Scoring_5;
	public $Scoring_6;
	public $Scoring_7;
	public $Scoring_8;
	public $Equality_TypeRate;
	const Labels = [ 
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Allowed_University_Fields' => [ 
					"هر دانشگاه میتواند در هر ماده",
					"شرکت کننده داشته باشد" 
			],
			'Allowed_Sportman_Fields' => [ 
					"هر ورزشکار میتواند در ",
					"ماده اعم از انفرادی و امدادی شرکت نماید " 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'Ways_Of_Holding' => [ 
					'شیوه برگزاری مسابقات براساس',
					'می باشد' 
			],
			'Scoring' => [ 
					'به نفرات اول تا هشتم هر ماده به ترتیب ',
					'امتیاز تعلق خواهد گرفت' 
			],
			'Equality_TypeRate' => [ 
					'در صورت تساوی امتیازات تیم بر اساس ',
					'در نظر گرفته خواهد شد' 
			],
			'NumberOfTeams_Coach' => [ 
					'هر مربی میتواند فقط هدایت',
					'تیم را بر عهده داشته باشد' 
			],
			'Rate' => [ 
					'در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			],
			'Rate_Medal' => [ 
					'طلا',
					'نقره',
					'برنز' 
			] 
	];
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId',
								'Scoring_1',
								'Scoring_2',
								'Scoring_3',
								'Scoring_4',
								'Scoring_5',
								'Scoring_6',
								'Scoring_7',
								'Scoring_8',
								'Allowed_University_Fields',
								'Allowed_Sportman_Fields',
								// //////
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6',
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze' 
						],
						'integer' 
				],
				[ 
						[ 
								'name',
								'value',
								'Equality_TypeRate' ,
								 ////////
								'Latest_Rules',
								'Ways_Of_Holding' 
						],
						'string',
						'max' => 100 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'id' => Yii::t ( 'app', 'ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => Yii::t ( 'app', 'Name' ),
				'value' => Yii::t ( 'app', 'Value' ),
				'DocFile_Rules' => '',
				'PdfFile_Rules' => '',
				'Allowed_University_Fields' => '',
				'Allowed_Sportman_Fields' => '',
				'Latest_Rules' => '',
				'Ways_Of_Holding' => '',
				'Num_Coach' => 'تعداد مربیان', // Yii::t ( 'app', 'Num_Coach' ),
				'Num_CoachAssistant' => 'تعداد کمک مربی',
				'Num_HeadCoach' => 'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست', // Yii::t ( 'app', 'Num_Supervisor' ),
				'Scoring_1' => '',
				'Scoring_2' => '',
				'Scoring_3' => '',
				'Scoring_4' => '',
				'Scoring_5' => '',
				'Scoring_6' => '',
				'Scoring_7' => '',
				'Scoring_8' => '',
				'Equality_TypeRate' => '',
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند', // Yii::t ( 'app', 'Coach_Is_Player' ),
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود', // Yii::t ( 'app', 'NumberOfTeams_Coach' ),
				'rate_1' => 'مقام اول',
				// ///// 'مقام اول',
				'rate_2' => 'مقام دوم',
				// ///// 'مقام دوم',
				'rate_3' => 'مقام سوم',
				// ///// 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				// ///// 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				// ///// 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز' 
		];
	}
	/**
	 * *
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Latest_Rules',
				'Ways_Of_Holding',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Coach_Is_Player',
				'NumberOfTeams_Coach',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				// ///////////////////////////////////////////
				'Allowed_University_Fields',
				'Allowed_Sportman_Fields',
				'Scoring_1',
				'Scoring_2',
				'Scoring_3',
				'Scoring_4',
				'Scoring_5',
				'Scoring_6',
				'Scoring_7',
				'Scoring_8',
				'Equality_TypeRate',
		];
	}
}
