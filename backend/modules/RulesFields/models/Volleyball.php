<?php

namespace backend\modules\RulesFields\models;

use Yii;
use \backend\models\Rules;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Volleyball extends Rules {
	
	public $Min_Num_Sportsman;
	public $Max_Num_Sportsman;
	public $FirstPeriod_Set;
	// public $FirstPeriod_Set2;
	public $PlayOff_Set;
	// public $PlayOff_Set2;
	public $Period_ResultsSet;
	public $Score_3Set_20;
	public $Score_3Set_21;
	public $Score_3Set_GiveUp;
	public $Score_5Set_30;
	public $Score_5Set_31;
	public $Score_5Set_32;
	public $Score_5Set_GiveUp;
	public $Equality;
	public $Head_Association;
	public $Technical_Supervisor;
	public $One_Committee;
	public $Two_From_Coach_Supervisor;
	const ArrayList_Equality = [ 
			1 => [ 
					'content' => Volleyball::List_Equality [1] 
			],
			2 => [ 
					'content' => Volleyball::List_Equality [2] 
			],
			3 => [ 
					'content' => Volleyball::List_Equality [3] 
			],
			4 => [ 
					'content' => Volleyball::List_Equality [4] 
			] 
	];
	const List_Equality = [ 
			1 => 'تعداد ست های برنده بر تعداد ست های بازنده تقسیم خواهد شد',
			2 => 'تقسیم امتیازات برد و باخت محاسبه خواهد شد',
			3 => 'در حالت تساوی امتیازها در مرحله قبل، برنده بازی دو تیم به مرحله بعد صعود مینماید',
			4 => 'در حالت تساوی روال مسابقات والیبال بازیهای المپیک ۱۹۸۰ ملاک عمل خواهد بود' 
	];
	const Labels = [ 
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Num_Sportsman' => [ 
					'هر تیم متشکل از حداقل',
					' تن ورزشکار و حداکثر',
					'تن ورزشکار',
					'مربی',
					'سرپرست میباشد' 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'Ways_Of_Holding' => [ 
					'شیوه برگزاری مسابقات براساس',
					'می باشد' 
			],
			'Period' => [ 
					'در دور اول مسابقات به صورت ',
					'ست برگزار میشود . ',
					' در دور دوم حذفی مسابقات به صورت ',
					'ست برگزار میشود.',
					'ست نتیجه',
					'امتیاز است .' 
			],
			'Score_3Set' => [ 
					'شیوه امتیاز دهی ۳ ست:',
					'20' => 'دو بر صفر',
					'21' => 'دو بر یک',
					'GiveUp' => 'در مسابقه حاضر نشود' 
			],
			'Score_5Set' => [ 
					'شیوه امتیاز دهی ۵ ست:',
					'30' => 'سه بر صفر',
					'31' => 'سه بر یک',
					'32' => 'سه بر دو',
					'GiveUp' => 'در مسابقه حاضر نشود' 
			],
			'Equality' => [ 
					'در حالت تساوی تیم ها از نظر امتیاز در پایان مسابقات دوره ای و تعیین مقام آنهاا ، به ترتیب مراحل زیر انجام میشود' 
			],
			'Rate' => [ 
					' در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			],
			'Technical_Committee' => [ 
					'اعضاي كميته ي فنی:' 
			] 
	];
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6' ,
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze',
								/////////////////////////////////////////////////
								'eventId',
								'fieldsId',
								'subFieldsId',
								'Min_Num_Sportsman',
								'Max_Num_Sportsman',
								'FirstPeriod_Set',
								// 'FirstPeriod_Set2',
								'PlayOff_Set',
								// 'PlayOff_Set2',
								'Period_ResultsSet',
								'Score_3Set_20',
								'Score_3Set_21',
								'Score_3Set_GiveUp',
								'Score_5Set_30',
								'Score_5Set_31',
								'Score_5Set_32',
								'Score_5Set_GiveUp',
								'Head_Association',
								'Technical_Supervisor',
								'One_Committee',
								'Two_From_Coach_Supervisor',
						],
						'integer' 
				],
				[ 
						[ 
								'Latest_Rules',
								'Ways_Of_Holding',
								/////////////////////////
								'name',
								'value',
								'Equality' 
						],
						'string',
						'max' => 100 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'DocFile_Rules' => 'فایل قوانین و مقررات نسخه (doc) ',
				'PdfFile_Rules' => 'فایل قوانین و مقررات نسخه (pdf) ',
				
				'Num_Coach' => 'تعداد مربیان',
				'Num_CoachAssistant'=>'تعداد کمک مربی',
				'Num_HeadCoach'=>'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست',
				
				'Latest_Rules' => 'مسابقات طبق آخرین قوانین و مقرارت',
				'Ways_Of_Holding' => 'شیوه برگزاری مسابقات براساس',
				
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند',
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود',
				
				'rate_1' => 'مقام اول',
				'rate_2' => 'مقام دوم',
				'rate_3' => 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز',
				
				///////////////////////////////////
				'id' => Yii::t ( 'app', 'ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => Yii::t ( 'app', 'Name' ),
				'value' => Yii::t ( 'app', 'Value' ),
				'Min_Num_Sportsman' => 'حداقل تعداد بازیکنان',
				'Max_Num_Sportsman' => 'حداکثر تعداد بازیکنان',
				'FirstPeriod_Set' => '',
				// 'FirstPeriod_Set2'=>'',
				'PlayOff_Set' => '',
				// 'PlayOff_Set2'=>'',
				'Period_ResultsSet' => '',
				'Score_3Set_20' => '',
				'Score_3Set_21' => '',
				'Score_3Set_GiveUp' => '',
				'Score_5Set_30' => '',
				'Score_5Set_31' => '',
				'Score_5Set_32' => '',
				'Score_5Set_GiveUp' => '',
				'Equality' => '',
				'Head_Association' => 'رییس انجمن',
				'Technical_Supervisor' => 'سرپرست فنی',
				'One_Committee' => 'یک نفر از کمیته داوران',
				'Two_From_Coach_Supervisor' => 'دو نفر از مربیان و سرپرستان',
		];
	}
	
	/**
	 * *
	 */
	/**
	 * *
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Latest_Rules',
				'Ways_Of_Holding',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				///////////////////////////////////////////////
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'FirstPeriod_Set',
				// 'FirstPeriod_Set2',
				'PlayOff_Set',
				// 'PlayOff_Set2',
				'Period_ResultsSet',
				'Score_3Set_20',
				'Score_3Set_21',
				'Score_3Set_GiveUp',
				'Score_5Set_30',
				'Score_5Set_31',
				'Score_5Set_32',
				'Score_5Set_GiveUp',
				'Equality',
				'Head_Association',
				'Technical_Supervisor',
				'One_Committee',
				'Two_From_Coach_Supervisor',
		];
	}
	
}
