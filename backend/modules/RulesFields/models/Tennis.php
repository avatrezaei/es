<?php

namespace backend\modules\RulesFields\models;

use Yii;
use \backend\models\Rules;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $fieldsId
 * @property integer $subFieldsId
 * @property string $name
 * @property string $value
 */
class Tennis extends Rules {
	public $Min_Num_Sportsman;
	public $Max_Num_Sportsman;
	public $Is_Team;
	public $Is_Single;
	public $Is_Double;
	public $Num_TypeOfMatch;
	public $FirstPeriod_TypeOfMatch;
	public $NextPeriod_TypeOfMatch;
	public $Num_Promoted_Team; // /تعداد تیم هایی که به مرحله بعد صعود میکنند
	public $Num_Rate_NextStage; // /
	public $TypeOfMatch_Single;
	public $Single_Set1;
	public $Single_Set2;
	public $TypeOfMatch_Double;
	public $Double_Set1;
	public $Double_Set2;
	public $Head_Association;
	public $Technical_Supervisor;
	public $One_Committee;
	public $Two_From_Coach_Supervisor;
	const Labels = [ 
			'file' => [ 
					'فایل قوانین و مقررات نسخه (doc) ',
					'فایل قوانین و مقررات نسخه (pdf) ' 
			],
			'Num_Sportsman' => [ 
					'هر تیم متشکل از ',
					'حداقل',
					' تن ورزشکار',
					' و حداکثر',
					'تن ورزشکار',
					'مربی',
					'سرپرست میباشد' 
			],
			'Latest_Rules' => [ 
					'مسابقات طبیق آخرین قوانین و مقررات',
					'برگزار خواهد شد' 
			],
			'Ways_Of_Holding' => [ 
					'شیوه برگزاری مسابقات براساس',
					'می باشد' 
			],
			'Num_TypeOfMatch' => [ 
					"مسابقات در",
					"قسمت ",
					"انفرادی",
					"دوبل",
					" دانشگاهی  برگزار مي شود" 
			],
			'Period' => [ 
					'مسابقات تیمی  در دوره مقدماتی به صورت ',
					' انجام میشود و در مرحله نهایی به صورت',
					'ادامه خواهد یافت.' 
			],
			'Num_Group' => [ 
					'مقام  های اول تا',
					'هر گروه برای تعیین مقام های اول تا',
					'بازی خواهند کرد.' 
			],
			'TypeOfMatch_Single' => [ 
					'مسابقات انفرادی به صورت',
					'و',
					'گیم از',
					'گیم  میباشد.' 
			],
			'TypeOfMatch_Double' => [ 
					'مسابقات دوبل دانشگاهی به صورت',
					'و',
					'گیم از',
					'گیم  میباشد.' 
			],
			'Rate' => [ 
					'در رده بندی نهایی دانشگاه ها به ترتیب به ازای مقام:',
					' اول',
					' دوم',
					' سوم',
					' چهارم',
					' پنجم',
					' ششم' 
			],
			'Technical_Committee' => [ 
					'اعضاي كميته ي فنی:' 
			] 
	];
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'rules';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'eventId',
								'fieldsId',
								'subFieldsId' 
						],
						'required' 
				],
				[ 
						[ 
								'Coach_Is_Player',
								'NumberOfTeams_Coach',
								'Num_Coach',
								'Num_CoachAssistant',
								'Num_HeadCoach',
								'Num_Supervisor',
								'rate_1',
								'rate_2',
								'rate_3',
								'rate_4',
								'rate_5',
								'rate_6' ,
								'RateIsMedal',
								'rate_gold',
								'rate_silver',
								'rate_bronze',
								/////////////////////////////////////////////////
								'eventId',
								'fieldsId',
								'subFieldsId',
								'Min_Num_Sportsman',
								'Max_Num_Sportsman',
								'Num_TypeOfMatch',
								'Is_Team',
								'Is_Single',
								'Is_Double',
								'Num_Promoted_Team',
								'Num_Rate_NextStage',
								'Single_Set1',
								'Single_Set2',
								'Double_Set1',
								'Double_Set2',
								'Head_Association',
								'Technical_Supervisor',
								'One_Committee',
								'Two_From_Coach_Supervisor',
						],
						'integer' 
				],
				[ 
						[ 
								'Latest_Rules',
								'Ways_Of_Holding',
								/////////////////////////
								'name',
								'value',
								'FirstPeriod_TypeOfMatch',
								'NextPeriod_TypeOfMatch',
								'TypeOfMatch_Single',
								'TypeOfMatch_Double',
						]
						,
						'string',
						'max' => 100 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'DocFile_Rules' => 'فایل قوانین و مقررات نسخه (doc) ',
				'PdfFile_Rules' => 'فایل قوانین و مقررات نسخه (pdf) ',
				
				'Num_Coach' => 'تعداد مربیان',
				'Num_CoachAssistant'=>'تعداد کمک مربی',
				'Num_HeadCoach'=>'تعداد کمک مربی',
				'Num_Supervisor' => ' تعداد سرپرست',
				
				'Latest_Rules' => 'مسابقات طبق آخرین قوانین و مقرارت',
				'Ways_Of_Holding' => 'شیوه برگزاری مسابقات براساس',
				
				'Coach_Is_Player' => 'مربی میتواند به عنوان بازیکین تیم را همراهی کند',
				'NumberOfTeams_Coach' => 'تعتداد تیم هایی که توسط هر مربی هدایت میشود',
				
				'rate_1' => 'مقام اول',
				'rate_2' => 'مقام دوم',
				'rate_3' => 'مقام سوم',
				'rate_4' => 'مقام چهارم',
				'rate_5' => 'مقام ‍پنجم',
				'rate_6' => 'مقام ششم',
				
				'RateIsMedal' => 'امتیاز دهی براساس رنگ مدال است',
				'rate_gold' => 'طلا',
				'rate_silver' => 'نقره',
				'rate_bronze' => 'برنز',
				
				///////////////////////////////////
				'id' => Yii::t ( 'app', 'ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'fieldsId' => Yii::t ( 'app', 'Fields ID' ),
				'subFieldsId' => Yii::t ( 'app', 'Sub Fields ID' ),
				'name' => Yii::t ( 'app', 'Name' ),
				'value' => Yii::t ( 'app', 'Value' ),
				'Min_Num_Sportsman' => 'حداقل تعداد بازیکنان',
				'Is_Team' => 'تیمی',
				'Is_Single' => 'انفرادی',
				'Is_Double' => 'دوبل',
				'Num_TypeOfMatch' => 'تعداد انواع مسابقه',
				'FirstPeriod_TypeOfMatch' => 'نوع مسابقه در دوره اول',
				'NextPeriod_TypeOfMatch' => 'نوع مسابقه در دوره های بعد',
				'Num_Promoted_Team' => '', // /تعداد تیم هایی که به مرحله بعد صعود میکنند
				'Num_Rate_NextStage' => '', // /
				'TypeOfMatch_Single' => '',
				'Single_Set1' => '',
				'Single_Set2' => '',
				'TypeOfMatch_Double' => '',
				'Double_Set1' => '',
				'Double_Set2' => '',
				'Head_Association' => 'رییس انجمن',
				'Technical_Supervisor' => 'سرپرست فنی',
				'One_Committee' => 'یک نفر از کمیته داوران',
				'Two_From_Coach_Supervisor' => 'دو نفر از مربیان و سرپرستان',
		];
	}
	/**
	 * *
	 */
	/**
	 * *
	 */
	public static function getAttributeName() {
		return [ 
				'DocFile_Rules',
				'PdfFile_Rules',
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Num_Coach',
				'Num_CoachAssistant',
				'Num_HeadCoach',
				'Num_Supervisor',
				'Latest_Rules',
				'Ways_Of_Holding',
				'rate_1',
				'rate_2',
				'rate_3',
				'rate_4',
				'rate_5',
				'rate_6',
				'RateIsMedal',
				'rate_gold',
				'rate_silver',
				'rate_bronze',
				///////////////////////////////////////////////
				'Min_Num_Sportsman',
				'Max_Num_Sportsman',
				'Is_Team',
				'Is_Single',
				'Is_Double',
				'Num_TypeOfMatch',
				'FirstPeriod_TypeOfMatch',
				'NextPeriod_TypeOfMatch',
				'Num_Promoted_Team',
				'Num_Rate_NextStage',
				'TypeOfMatch_Single',
				'Single_Set1',
				'Single_Set2',
				'TypeOfMatch_Double',
				'Double_Set1',
				'Double_Set2',
				'Head_Association',
				'Technical_Supervisor',
				'One_Committee',
				'Two_From_Coach_Supervisor',
		];
	}
}
	