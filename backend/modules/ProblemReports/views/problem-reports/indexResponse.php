<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\helpers\Url;

use backend\modules\ProblemReports\models\ProblemReports;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ProblemReports\models\ProblemReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Problem Reports');
$this->params['breadcrumbs'][] = $this->title;

$this->params['actions'] = [
		[
				'label' => '<i class="ace-icon fa fa-plus bigger-120 orange"></i> ' . Yii::t('app', 'Create Problem Reports'),
				'url' => ['create']
		]
];
?>
<div class="col-md-12 col-sm-12">
	<div class="portlet light ">
		<div class="portlet-title">
			<div class="caption caption-md">
				<i class="icon-bar-chart font-green"></i> <span
					class="caption-subject font-green bold uppercase"><?= $this->title; ?></span>
				
			</div>
			<div class="actions">
						 <div class="btn-group">
                                            <a class="btn green btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                                            	<?php echo Yii::t('app','More ProblemReports');?>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                <?php echo Html::a(Yii::t('app','Admin ProblemReports'),Url::to('admin'));?>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                	<?php echo Html::a(Yii::t('app','All ProblemReports'),Url::to('index'));?>
                                                </li>
                                                <li>
                                                <?php
	                                                $str=ProblemReports::StatusFa(ProblemReports::Status_Unread);
	                                                $str.='<span class="badge badge-danger">'. ProblemReports::CountStatus(ProblemReports::Status_Unread).'</span>';
	                                                echo Html::a($str,Url::to(['index','status'=>ProblemReports::Status_Unread]));
                                                ?>
                                                </li>
                                                
                                                 <li>
                                                <?php
	                                                $str=ProblemReports::StatusFa(ProblemReports::Status_Read);
	                                                $str.='<span class="badge badge-warning">'. ProblemReports::CountStatus(ProblemReports::Status_Read).'</span>';
	                                               echo Html::a($str,Url::to(['index','status'=>ProblemReports::Status_Read]));
                                                ?>
                                                </li>
                                                
                                                
                                                <li>
                                                 <?php
	                                                $str=ProblemReports::StatusFa(ProblemReports::Status_Pending);
	                                                $str.='<span class="badge badge-info">'. ProblemReports::CountStatus(ProblemReports::Status_Pending).'</span>';
	                                                echo Html::a($str,Url::to(['index','status'=>ProblemReports::Status_Pending]));
                                                ?>
                                                </li>
                                            </ul>
                                        </div>
			</div>
		</div>
		<div class="portlet-body">
			<div class="scroller" style="height: 100%;" data-always-visible="1"
				data-rail-visible1="0" data-handle-color="#D7DCE2">
				<div class="general-item-list">
                                        <?php
                                        foreach ( $models as $model ) {
                                        ?>
                                        	 <div class="item">
												<div class="item-head">
													<div class="item-details">
														<img class="item-pic rounded"
															src="../assets/pages/media/users/avatar4.jpg"> <a href=""
															class="item-name primary-link"><?= $model->userId?></a> <span
															class="item-label"><?php echo  $model->requestDateFa;?></span>
													</div>
													<span class="item-status">
                                                      <?php echo  $model->statusUI;?>
                                                       <?php echo ProblemReports::StatusFa ( $model->status );?>
						                            </span>
												</div>
												<div class="item-body"> <?php echo  $model->problemTitle;?></div>
												<div class='row'>
												 	<div class="form-group">
													 	<div class="col-md-10"></div>
													 	<div class="col-md-2">
													 	<?php 
				                                           echo  Html::a('<i class="icon-pencil"></i>', Url::to(['update','id' => $model->id]), [
				                                            		'title' => Yii::t ( 'app', 'Update' ),
				                                            		'data-hint' =>Yii::t ( 'app', 'Update' ),
				                                            		'class' => 'btn btn-icon-only btn-default hint--top hint--rounded hint--info'
				                                            ])
				                                        ?>
				                                        	<?php 
				                                           echo  Html::a('<i class="glyphicon glyphicon-eye-open"></i>', Url::to(['view','id' => $model->id]), [
				                                            		'title' => Yii::t ( 'app', 'View' ),
				                                            		'data-hint' =>Yii::t ( 'app', 'View' ),
				                                            		'class' => 'btn btn-icon-only btn-default hint--top hint--rounded hint--info'
				                                            ])
				                                        ?>
				                                         </div>
                                        			</div>
                                    			</div>
												</div>
                                        <?php } ?>
              </div>
			</div>
			
			
		</div>
	</div>
</div>