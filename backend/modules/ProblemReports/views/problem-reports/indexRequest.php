<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\helpers\Url;

use backend\modules\ProblemReports\models\ProblemReports;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ProblemReports\models\ProblemReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( 'app', 'Problem Reports' );
$this->params ['breadcrumbs'] [] = $this->title;

$this->params ['actions'] = [ 
		[ 
				'label' => '<i class="ace-icon fa fa-plus bigger-120 orange"></i> ' . Yii::t ( 'app', 'Create Problem Reports' ),
				'url' => [ 
						'create' 
				] 
		] 
];
?>
<div class="col-md-12 col-sm-12">
	<div class="portlet light ">
		<div class="portlet-title">
			<div class="caption caption-md">
				<i class="icon-bar-chart font-green"></i> <span
					class="caption-subject font-green bold uppercase"><?= $this->title; ?></span>
			</div>
			<div class="actions">
				<div class="btn-group">
					<a class="btn green btn-sm" href="javascript:;"
						data-toggle="dropdown" data-hover="dropdown"
						data-close-others="true"> 
                                            	<?php echo Yii::t('app','More ProblemReports');?>
                                                <i
						class="fa fa-angle-down"> </i>
					</a>
					<ul class="dropdown-menu pull-right">
						<li>
                                                <?php echo Html::a(Yii::t('app','Admin ProblemReports'),Url::to('admin'));?>
                         </li>
						<li class="divider"></li>
						<li>
                                                	<?php echo Html::a(Yii::t('app','All ProblemReports'),Url::to('index'));?>
                                                </li>
						<li>
                                                <?php
																																																$str = ProblemReports::StatusFa ( ProblemReports::Status_Unread );
																																																$str .= '<span class="badge badge-danger">' . ProblemReports::CountStatus ( ProblemReports::Status_Unread ) . '</span>';
																																																echo Html::a ( $str, Url::to ( [ 
																																																		'index',
																																																		'status' => ProblemReports::Status_Unread 
																																																] ) );
																																																?>
                                                </li>

						<li>
                                                <?php
																																																$str = ProblemReports::StatusFa ( ProblemReports::Status_Read );
																																																$str .= '<span class="badge badge-warning">' . ProblemReports::CountStatus ( ProblemReports::Status_Read ) . '</span>';
																																																echo Html::a ( $str, Url::to ( [ 
																																																		'index',
																																																		'status' => ProblemReports::Status_Read 
																																																] ) );
																																																?>
                                                </li>


						<li>
                                                 <?php
																																																	$str = ProblemReports::StatusFa ( ProblemReports::Status_Pending );
																																																	$str .= '<span class="badge badge-info">' . ProblemReports::CountStatus ( ProblemReports::Status_Pending ) . '</span>';
																																																	echo Html::a ( $str, Url::to ( [ 
																																																			'index',
																																																			'status' => ProblemReports::Status_Pending 
																																																	] ) );
																																																	?>
                                                </li>
					</ul>
				</div>
			</div>
		</div>
		<div class="portlet-body">

			<div class="row">

				<div class="mt-element-list">

					<div class="mt-list-container list-todo opt-2">
						<div class="list-todo-line bg-red"></div>
						<ul>
						<?php
						$i = 0;
						foreach ( $models as $model ) {
							$i ++;
							$itemName = 'item-' . $i;
							$id = "task-3-" . $i;
							?>
							
								<li class="mt-list-item">
								<div class="list-todo-icon bg-white font-yellow-crusta">
									<i class="fa fa-sticky-note-o"></i>
								</div>
								<div class="list-todo-item <?= $itemName ?>">
									<a class="list-toggle-container font-white"
										data-toggle="collapse" href="#<?=$id?>" aria-expanded="false">
										<div class="list-toggle done uppercase bg-yellow-crusta">
											<div class="list-toggle-title bold">Content Development</div>
											<div
												class="badge badge-default pull-right bg-white font-dark bold">2</div>
										</div>
									</a>
									<div class="task-list panel-collapse collapse" id="<?=$id;?>">
										<ul>
											<li class="task-list-item done">
												<div class="task-icon">
													<a href="javascript:;"> <i class="fa fa-navicon"></i>
													</a>
												</div>
												<div class="task-status">
													<a class="done" href="javascript:;"> <i class="fa fa-check"></i>
													</a> <a class="pending" href="javascript:;"> <i
														class="fa fa-close"></i>
													</a>
												</div>
												<div class="task-content">
													<h4 class="uppercase bold">
														<a href="javascript:;"><?php echo  $model->requestDateFa;?></a>
													</h4>
													<p><?php echo  $model->problemTitle;?></p>
												</div>
											</li>
											<li class="task-list-item">
												<div class="task-icon">
													<a href="javascript:;"> <i class="fa fa-cube"></i>
													</a>
												</div>
												<div class="task-status">
													<a class="done" href="javascript:;"> <i class="fa fa-check"></i>
													</a> <a class="pending" href="javascript:;"> <i
														class="fa fa-close"></i>
													</a>
												</div>
												<div class="task-content">
													<h4 class="uppercase bold">
														<a href="javascript:;"><?php echo  $model->statusChangeDateFa;?></a>
													</h4>
													<p><?php echo  $model->response;?></p>
												</div>
											</li>
										</ul>
										<div class="task-footer bg-grey">
											<div class="row">
												<div class="col-xs-6">
													<a class="task-trash" href="javascript:;"> <i
														class="fa fa-trash"></i>
													</a>
												</div>
												<div class="col-xs-6">
													<a class="task-add" href="javascript:;"> <i
														class="fa fa-plus"></i>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
							
							
							<?php
						}
						
						?>
						
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

