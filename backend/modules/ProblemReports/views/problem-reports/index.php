<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ProblemReports\models\ProblemReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( 'app', 'Problem Reports' );
$this->params ['breadcrumbs'] [] = $this->title;

$this->params ['actions'] = [ 
		[ 
				'label' => '<i class="ace-icon fa fa-plus bigger-120 orange"></i> ' . Yii::t ( 'app', 'Create Problem Reports' ),
				'url' => [ 
						'create' 
				] 
		] 
];
$this->registerJs ( <<<SCRIPT
		function moghdamUpdateFN(ev){
			window.location = window.location;
		}
		$('body').on('xhr.dt',function(e, settings, json, xhr){
			if(e.target.id == 'w0')
				$("#w0 a[data-name=status]").on('save',function(ev){moghdamUpdateFN(ev);});
		});
SCRIPT
, View::POS_READY );
?>
<div class="problem-reports-index">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark hidden-xs">
				<i class="fa fa-commenting-o font-dark"></i> <span
					class="caption-subject bold uppercase"><?= Yii::t('app','Problem Reports'); ?></span>
			</div>
			<div class="tools"></div>
		</div>
		<div class="portlet-body">
			<div class='row'>
				<div class='col-md-12'>
					<?= $widget->run()?>
				</div>
			</div>

		</div>
	</div>
</div>