<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\UserProfile;
use backend\assets\Portfolio;
use yii\helpers\Url;

UserProfile::register ( $this );
Portfolio::register ( $this );

use backend\modules\YumUsers\models\User;
use backend\modules\ProblemReports\models\ProblemReports;
use backend\models\Event;
?>

<!-- TIMELINE ITEM -->
<div class="timeline-item">
	<div class="timeline-body">
		<div class="timeline-body-arrow"></div>
		<div class="timeline-body-head">
			<div class="timeline-body-head-caption">
				<span class="timeline-body-time font-grey-cascade">
					<?php
					echo $model->getAttributeLabel ( 'requestDate' ) . ': ' . $model->requestDateFa;
					?>
				</span>
			</div>
		</div>
		<div class="timeline-body-content">
			<span class="font-green-haze">
				<?php
				echo $model->getAttributeLabel ( 'problemTitle' ) . ' : ';
				echo $model->problemTitle;
				?>  
			</span>
		</div>

		<div class="timeline-body-content">
			<span class="font-grey-cascade">
				<?php
				echo $model->getAttributeLabel ( 'problemDescription' ) . ' :<br /> ';
				echo $model->problemDescription;
				?> 
			</span>
		</div>

		<div class="timeline-body-content">
			<span class="timeline-body-time font-grey-cascade">
				<?php
				echo $model->getAttributeLabel ( 'problemPage' ) . ': ' . html::encode ( $model->problemPage, true );
				?>
			</span>
		</div>

	</div>
</div>
<!-- END TIMELINE ITEM -->

<!-- TIMELINE ITEM Image -->
<?php
if (! (is_null ( $model->imgId1 ) || empty ( $model->imgId1 )) && ! (is_null ( $model->imgId2 ) || empty ( $model->imgId2 )) && ! (is_null ( $model->imgId3 ) || empty ( $model->imgId3 ))) {
	?>
<div class="timeline-item">
	<div class="timeline-body">
		<div class="timeline-body-arrow"></div>
		<div class="timeline-body-head">
			<div class="timeline-body-head-caption">
				<span class="timeline-body-time font-grey-cascade">
				<?php echo Yii::t ( 'app', 'Image Problem Reports' );	?>
				</span>
			</div>
		</div>

		<div class="timeline-body-content">
			<div id="js-grid-juicy-projects" class="cbp">
					<?php
						for($i = 1; $i < 4; $i ++) {
							$name = 'imgId' . $i;
							if (is_null ( $model->$name ) || empty ( $model->$name ))
								continue;
							?>
					<div class="cbp-item graphic">
						<div class="cbp-caption">
							<div class="cbp-caption-defaultWrap">
								<?php echo $model->getIconThumbnail($i);?>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignCenter">
									<div class="cbp-l-caption-body">
										<a href="<?php echo $model->getImageUrl($i); ?>"
											class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase"
											data-title="Dashboard<br>byPaulFlaviusNechita">
											<?php
											echo Yii::t ( 'app', 'View' );
											?>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php }	?>
			</div>
		</div>
	</div>
</div>
<?php
}

?>
<!-- END TIMELINE ITEM Image-->
