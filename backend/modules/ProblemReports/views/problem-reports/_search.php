<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ProblemReports\models\ProblemReportsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="problem-reports-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'userId') ?>

	<?= $form->field($model, 'eventId') ?>

	<?= $form->field($model, 'problemTitle') ?>

	<?= $form->field($model, 'problemDescription') ?>

	<?php // echo $form->field($model, 'imgId1') ?>

	<?php // echo $form->field($model, 'imgId2') ?>

	<?php // echo $form->field($model, 'imgId3') ?>

	<?php // echo $form->field($model, 'problemPage') ?>

	<?php // echo $form->field($model, 'status') ?>

	<?php // echo $form->field($model, 'important') ?>

	<?php // echo $form->field($model, 'response') ?>

	<?php // echo $form->field($model, 'requestDate') ?>

	<?php // echo $form->field($model, 'statusChangeDate') ?>

	<?php // echo $form->field($model, 'valid') ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
