<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ProblemReports\models\ProblemReports */

$this->title = Yii::t ( 'app', 'Update {modelClass}: ', [ 
		'modelClass' => 'Problem Reports' 
] ) . ' ' . $model->id;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Problem Reports' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->id,
		'url' => [ 
				'view',
				'id' => $model->id 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Update' );
?>


<?php
if (Yii::$app->user->can ( 'SeniorUserEvent' )) {
	echo $this->render ( '_formResponse', [ 
			'model' => $model 
	] );
} else {
	?>
<div class="problem-reports-update">

	<div
		class="col-lg-10 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light bordered">

			<div class="portlet-body form">

				<?php echo $this->render ( '_form', [ 'model' => $model ] )?>

			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>

<?php }?>