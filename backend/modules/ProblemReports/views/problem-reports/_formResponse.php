<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\UserProfile;
use backend\assets\Portfolio;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\base\Widget;
use kartik\file\FileInput;

UserProfile::register ( $this );
Portfolio::register ( $this );

use backend\modules\YumUsers\models\User;
use backend\modules\ProblemReports\models\ProblemReports;
use backend\models\Event;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-9 col-sm-9">{input} {hint} {error}</div>';
$template_4 = Yii::$app->params ['template_4'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3 col-sm-3 control-label';
$labelClass_min = 'col-md-2 col-sm-2 control-label';
$errorClass = Yii::$app->params ['errorClass'];

$user = User::findOne ( $model->userId );
$event = Event::findOne ( $model->eventId );
?>

								
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true, 'class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

<div class="problem-reports-view">
	<div class='row'>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="portlet ">
				<div class="row">
					<div class='col-md-12'>
						<div class='profile-sidebar'>
							<div class="portlet light profile-sidebar-portlet ">
								<div class="profile-userpic">
									<?php
									if (is_object ( $user )) {
										echo $user->getIconThumbnail ( null, "img-responsive" );
									}
									?>
								</div>

								<div class="profile-usertitle" id="user_quick_info">
									<div class="profile-usertitle-name"> <?php echo Html::encode($user->FullName)?></div>
								</div>

								<div class="profile-usertitle" id="user_quick_info"></div>

								<div class="">
									<ul class="nav">
										<li>
											<div class='col-md-12'>
												<div class='form-group'>
												<?php
												$str = '<div class="col-md-2">' . $model->getAttributeLabel ( 'status' ) . ':</div>';
												$str .= '<div class="col-md-8">' . ProblemReports::StatusFa ( $model->status ) . '</div>';
												$str .= '<div class="col-md-2">' . $model->statusUI . '</div>';
												echo $str;
												?>
												</div>
											</div>
										</li>
										<li>
											<div class='col-md-12'>
												<div class='form-group'>
												<?php
												$str = '<div class="col-md-2">' . $model->getAttributeLabel ( 'important' ) . ':</div>';
												$str .= '<div class="col-md-10">' . ProblemReports::ImportantFa ( $model->important ) . '</div>';
												echo $str;
												?>
												</div>
											</div>
										</li>
									</ul>
								</div>

								<div class="profile-usertitle" id="user_quick_info"></div>
								<br>
							</div>

						</div>
						<div class='profile-content'>
							<div class="row">
								<div class="col-md-12">
									<div class="portlet light portlet-fit bordered">
										<div class="portlet-title">
											<div class="caption">
												<span class="caption-helper">
												<?php
												if (is_object ( $event ))
													echo $event->name;
												?>
												</span>
											</div>

										</div>
										<div class="portlet-body">
											<div class="timeline">
												<?php
												
												echo $this->render ( 'viewRequest', [ 
														'model' => $model 
												] );
												
												?>
												<!-- TIMELINE ITEM RESPONSE -->
												<div class="timeline-item">
													<div class="timeline-body">
														<div class="timeline-body-arrow"></div>
														<div class="timeline-body-head">
															<div class="timeline-body-head-caption">
																<span class="timeline-body-time font-grey-cascade">
																	<?php echo Yii::t('app','Response');?>
																	</span>
															</div>
															<div class="timeline-body-head-actions"></div>
														</div>
														<div class="timeline-body-content">
															<div class='row'>
																<div class='form-group'>
			<?php
			echo $form->field ( $model, 'status', [ 
					'options' => [ 
							'class' => $formGroupClass 
					],
					'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="fields-gametype">{input} </div> {hint} {error}</div>' 
			] )->radioList ( ProblemReports::StatusFa ( 0, true ), [ 
					'tag' => false,
					'unselect' => NULL,
					'item' => function ($index, $label, $name, $checked, $value) {
						return '<label class="btn btn-info ' . ($checked ? 'active' : '') . '">' . Html::radio ( $name, $checked, [ 
								'value' => $value,
								'autocomplete' => 'off' 
						] ) . $label . '</label>';
					} 
			] )->label ( NULL, [ 
					'class' => $labelClass_min 
			] );
			?>
	</div>
															</div>

															<div class='row'>
																<div class='form-group'>
	<?php
	echo $form->field ( $model, 'response', [ 
			'options' => [ 
					'class' => $formGroupClass 
			],
			'template' => $template 
	] )->textarea ( [ 
			'class' => $inputClass,
			'rows' => 10 
	] )->label ( NULL, [ 
			'class' => $labelClass_min 
	] );
	?>
	</div>
															</div>
															<div class="form-actions">
																<div class="row">
																	<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn green'])?>
			</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- END TIMELINE ITEM -->
											</div>
										</div>
									</div>
								</div>



							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
</div>

<?php ActiveForm::end(); ?>
