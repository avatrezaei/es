<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ProblemReports\models\ProblemReports */
/* @var $form yii\widgets\ActiveForm */

use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\base\Widget;
use kartik\file\FileInput;
use backend\modules\ProblemReports\models\ProblemReports;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-9 col-sm-9">{input} {hint} {error}</div>';
$template_4 = Yii::$app->params ['template_4'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3 col-sm-3 control-label';
$labelClass_min = 'col-md-2 col-sm-2 control-label';
$errorClass = Yii::$app->params ['errorClass'];
?>

								
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true, 'class' => 'form-horizontal']]); ?>
	<?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>
<div class="form-body">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class='form-group'>
			<div class="caption">
				<i class="fa fa-commenting-o"></i> <span
					class="caption-subject  bold uppercase"><?php echo Yii::t('app', 'Create Problem Reports');?></span>
				<hr>
			</div>
		</div>
	</div>
	<div class='row'>
		<div class='form-group'>
	<?php
	echo $form->field ( $model, 'problemTitle', [ 
			'options' => [ 
					'class' => $formGroupClass 
			],
			'template' => $template 
	] )->textInput ( [ 
			'maxlength' => true,
			'class' => $inputClass 
	] )->label ( NULL, [ 
			'class' => $labelClass_min 
	] );
	?>
				</div>
	</div>

	<div class='row'>
		<div class='form-group'>
			<?php
			echo $form->field ( $model, 'important', [ 
					'options' => [ 
							'class' => $formGroupClass 
					],
					'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="fields-gametype">{input} </div> {hint} {error}</div>' 
			] )->radioList ( ProblemReports::ImportantFa ( 0, true ), [ 
					'tag' => false,
					'unselect' => NULL,
					'item' => function ($index, $label, $name, $checked, $value) {
						return '<label class="btn btn-info ' . ($checked ? 'active' : '') . '">' . Html::radio ( $name, $checked, [ 
								'value' => $value,
								'autocomplete' => 'off' 
						] ) . $label . '</label>';
					} 
			] )->label ( NULL, [ 
					'class' => $labelClass_min 
			] );
			?>
				</div>
	</div>

	<div class='row'>
		<div class='form-group'>
	<?php
	echo $form->field ( $model, 'problemDescription', [ 
			'options' => [ 
					'class' => $formGroupClass 
			],
			'template' => $template 
	] )->textarea ( [ 
			'class' => $inputClass,
			'rows' => 10 
	] )->label ( NULL, [ 
			'class' => $labelClass_min 
	] );
	?>
				</div>
	</div>

	<div class='row'>
		<div class='form-group'>
	<?php
	echo $form->field ( $model, 'problemPage', [ 
			'options' => [ 
					'class' => $formGroupClass 
			],
			'template' => $template 
	] )->textInput ( [ 
			'maxlength' => true,
			'class' => $inputClass 
	] )->label ( NULL, [ 
			'class' => $labelClass_min 
	] );
	?>
				</div>
	</div>

	<!-- 		START CAPTION Upload	 -->

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class='form-group'>
			<div class="caption">
				<i class="fa fa-upload "></i> <span
					class="caption-subject  bold uppercase"><?php echo Yii::t('app','File Upload');?></span>
				<hr>
			</div>
		</div>
	</div>

	<div class='row'>
		<div class='form-group'>
			<div class='col-md-5'>
			<?php
			echo html::label ( Yii::t ( 'app', 'fileImg1' ), 'fileImg1', [ 
					'class' => 'col-md-4 col-sm-4 control-label' 
			] );
			?>
			<div class='col-md-8 col-sm-8'>
				<?php
				echo $form->field ( $model, 'fileImg1', [ 
						'options' => [ 
								'class' => $formGroupClass 
						] 
				] )->widget ( FileInput::classname (), [ 
						'id' => 'file_input_sportlocation_img1',
						'language' => 'fa',
						'options' => [ 
								'multiple' => true,
								'layoutTemplates' => 'modal' 
						],
						
						'pluginOptions' => [ 
								'showUpload' => false,
								'maxFileCount' => 1,
								'initialPreview' => (is_null ( $model->imgId1 ) || empty ( $model->imgId1 ) || $model->imgId1 == 0) ? [ ] : [ 
										$model->getIconThumbnail ( 1, $model->imgId1, 'profile-userpic' ) 
								] 
						] 
				] )->label ( false );
				
				?>
			</div>
			</div>
		</div>
	</div>
	<div class='row'>
		<div class='form-group'>
			<div class='col-md-5'>
			<?php
			echo html::label ( Yii::t ( 'app', 'fileImg2' ), 'fileImg2', [ 
					'class' => 'col-md-4 col-sm-4 control-label' 
			] );
			?>
			<div class='col-md-8 col-sm-8'>
				<?php
				echo $form->field ( $model, 'fileImg2', [ 
						'options' => [ 
								'class' => $formGroupClass 
						] 
				] )->widget ( FileInput::classname (), [ 
						'id' => 'file_input_sportlocation_img2',
						'language' => 'fa',
						'options' => [ 
								'multiple' => true,
								'layoutTemplates' => 'modal' 
						],
						
						'pluginOptions' => [ 
								'showUpload' => false,
								'maxFileCount' => 1,
								'initialPreview' => (is_null ( $model->imgId2 ) || empty ( $model->imgId2 ) || $model->imgId2 == 0) ? [ ] : [ 
										$model->getIconThumbnail ( 2, $model->imgId2, 'profile-userpic' ) 
								] 
						] 
				] )->label ( false );
				
				?>
			</div>
			</div>
		</div>
	</div>
	<div class='row'>
		<div class='form-group'>
			<div class='col-md-5'>
			<?php
			echo html::label ( Yii::t ( 'app', 'fileImg3' ), 'fileImg3', [ 
					'class' => 'col-md-4 col-sm-4 control-label' 
			] );
			?>
			<div class='col-md-8 col-sm-8'>
				<?php
				echo $form->field ( $model, 'fileImg3', [ 
						'options' => [ 
								'class' => $formGroupClass 
						] 
				] )->widget ( FileInput::classname (), [ 
						'id' => 'file_input_sportlocation_img3',
						'language' => 'fa',
						'options' => [ 
								'multiple' => true,
								'layoutTemplates' => 'modal' 
						],
						
						'pluginOptions' => [ 
								'showUpload' => false,
								'maxFileCount' => 1,
								'initialPreview' => (is_null ( $model->imgId3 ) || empty ( $model->imgId3 ) || $model->imgId3 == 0) ? [ ] : [ 
										$model->getIconThumbnail ( 3, $model->imgId3, 'profile-userpic' ) 
								] 
						] 
				] )->label ( false );
				
				?>
			</div>
			</div>
		</div>
	</div>
<?php
if (! $model->isNewRecord && Yii::$app->user->can ( 'SeniorUserEvent' )) {
	echo $this->render ( '_formResponse', [ 
			'form' => $form,
			'model' => $model 
	] );
}
?>
	</div>
<div class="form-actions">
	<div class="row">
		<div class="col-md-offset-3 col-md-9">
				<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn green'])?>
			</div>
	</div>
</div>

<?php ActiveForm::end(); ?>
