<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ProblemReports\models\ProblemReports */
/* @var $form yii\widgets\ActiveForm */

use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\base\Widget;
use kartik\file\FileInput;
use backend\modules\ProblemReports\models\ProblemReports;

$formGroupClass = Yii::$app->params ['formGroupClass'];
$template = '{label}<div class="col-md-9 col-sm-9">{input} {hint} {error}</div>';
$template_4 = Yii::$app->params ['template_4'];
$inputClass = Yii::$app->params ['inputClass'];
$labelClass = 'col-md-3 col-sm-3 control-label';
$labelClass_min = 'col-md-2 col-sm-2 control-label';
$errorClass = Yii::$app->params ['errorClass'];
?>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class='form-group'>
		<div class="caption">
			<i class="fa fa-reply "></i> <span
				class="caption-subject  bold uppercase"><?php echo Yii::t('app','Response');?></span>
			<hr>
		</div>
	</div>
</div>

<div class='row'>
	<div class='form-group'>
			<?php
			echo $form->field ( $model, 'status', [ 
					'options' => [ 
							'class' => $formGroupClass 
					],
					'template' => '{label}<div class="col-md-9 col-sm-9"><div class="btn-group" data-toggle="buttons" id="fields-gametype">{input} </div> {hint} {error}</div>' 
			] )->radioList ( ProblemReports::StatusFa ( 0, true ), [ 
					'tag' => false,
					'unselect' => NULL,
					'item' => function ($index, $label, $name, $checked, $value) {
						return '<label class="btn btn-info ' . ($checked ? 'active' : '') . '">' . Html::radio ( $name, $checked, [ 
								'value' => $value,
								'autocomplete' => 'off' 
						] ) . $label . '</label>';
					} 
			] )->label ( NULL, [ 
					'class' => $labelClass_min 
			] );
			?>
	</div>
</div>

<div class='row'>
	<div class='form-group'>
	<?php
	echo $form->field ( $model, 'response', [ 
			'options' => [ 
					'class' => $formGroupClass 
			],
			'template' => $template 
	] )->textarea ( [ 
			'class' => $inputClass,
			'rows' => 10 
	] )->label ( NULL, [ 
			'class' => $labelClass_min 
	] );
	?>
	</div>
</div>
