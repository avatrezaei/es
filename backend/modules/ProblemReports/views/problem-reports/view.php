<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\UserProfile;
use backend\assets\Portfolio;
use yii\helpers\Url;

UserProfile::register ( $this );
Portfolio::register ( $this );

use backend\modules\YumUsers\models\User;
use backend\modules\ProblemReports\models\ProblemReports;
use backend\models\Event;

/* @var $this yii\web\View */
/* @var $model backend\modules\ProblemReports\models\ProblemReports */

$this->title = $model->id;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Problem Reports' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
$this->params ['actions'] = [ 
		[ 
				'label' => Yii::t ( 'app', 'Update' ),
				'url' => [ 
						'update',
						'id' => $model->id 
				] 
		],
		[ 
				'label' => Yii::t ( 'app', 'Delete' ),
				'url' => [ 
						'delete',
						'id' => $model->id 
				],
				'options' => [ 
						'data' => [ 
								'confirm' => Yii::t ( 'app', 'Are you sure you want to delete this item?' ),
								'method' => 'post' 
						],
						'class' => 'btn btn-danger' 
				] 
		] 
];
$detalView = '<div class="row static-info"><div class="form-group"><div class="col-md-3 name"> {label}: </div><div class="col-md-5 value"> {value} </div></div></div>';
$user = User::findOne ( $model->userId );
$event = Event::findOne ( $model->eventId );
?>
<div class="problem-reports-view">
	<div class='row'>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="portlet ">
				<div class="row">
					<div class='col-md-12'>
						<div class='profile-sidebar'>
							<div class="portlet light profile-sidebar-portlet ">
								<div class="profile-userpic">
									<?php
									if (is_object ( $user )) {
										echo $user->getIconThumbnail ( null, "img-responsive" );
									}
									?>
								</div>

								<div class="profile-usertitle" id="user_quick_info">
									<div class="profile-usertitle-name"> <?php echo Html::encode($user->FullName)?></div>
								</div>

								<div class="profile-usertitle" id="user_quick_info"></div>

								<div>
									<ul class="nav">
										<li>
											<div class='col-md-12'>
												<div class='form-group'>
												<?php
												$str = '<div class="col-md-2">' . $model->getAttributeLabel ( 'status' ) . ':</div>';
												$str .= '<div class="col-md-8">' . ProblemReports::StatusFa ( $model->status ) . '</div>';
												$str .= '<div class="col-md-2">' . $model->statusUI . '</div>';
												echo $str;
												?>
												</div>
											</div>
										</li>
										<li>
											<div class='col-md-12'>
												<div class='form-group'>
												<?php
												$str = '<div class="col-md-2">' . $model->getAttributeLabel ( 'important' ) . ':</div>';
												$str .= '<div class="col-md-10">' . ProblemReports::ImportantFa ( $model->important ) . '</div>';
												echo $str;
												?>
												</div>
											</div>
										</li>
									</ul>
								</div>

								<div class="profile-usertitle" id="user_quick_info"></div>

								<div class="profile-userbuttons">
									<?=Html::a ( Yii::t ( 'app', 'Update' ), Url::to ([ 'update','id' => $model->id ] ), [ 'class' => 'btn  green btn-sm' ] )?>
									<?=Html::a ( Yii::t ( 'app', 'Delete' ), Url::to ( [ 'delete','id' => $model->id ] ), [ 'class' => 'btn  red btn-sm','data' => [ 'confirm' => Yii::t ( 'app', 'Are you sure you want to delete this item?' ),'method' => 'post' ] ] )?>
									
								</div>
								<br>
							</div>

						</div>
						<div class='profile-content'>
							<div class="row">
								<div class="col-md-12">
									<div class="portlet light portlet-fit bordered">
										<div class="portlet-title">
											<div class="caption">
												<span class="caption-helper">
												<?php
												if (is_object ( $event ))
													echo $event->name;
												?>
												</span>
											</div>

										</div>
										<div class="portlet-body">
											<div class="timeline">
												<?php
												
												echo $this->render ( 'viewRequest', [ 
														'model' => $model 
												] );
												
												?>
												<!-- TIMELINE ITEM RESPONSE -->
												<?php
												if (! is_null ( $model->response ) && ! empty ( $model->response )) {
													?>
												<div class="timeline-item">
													<div class="timeline-body">
														<div class="timeline-body-arrow"></div>
														<div class="timeline-body-head">
															<div class="timeline-body-head-caption">
																<span class="timeline-body-time font-grey-cascade">
																	<?php
													echo $model->getAttributeLabel ( 'statusChangeDate' ) . ': ' . $model->statusChangeDateFa;
													?>
																	</span>
															</div>
															<div class="timeline-body-head-actions"></div>
														</div>
														<div class="timeline-body-content">
															<span class="font-grey-cascade">
															<div class='row'>
															<?php
																echo $model->getAttributeLabel ( 'response' ) . ' :<br /> ';
																echo $model->response;
															?>
															</div>
															</span>
														</div>
													</div>
												</div>
													<?php
												}
												?>
												<!-- END TIMELINE ITEM -->
											</div>
										</div>
									</div>
								</div>



							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
</div>
