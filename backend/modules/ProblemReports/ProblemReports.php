<?php

namespace backend\modules\ProblemReports;

class ProblemReports extends \yii\base\Module
{
	public $controllerNamespace = 'backend\modules\ProblemReports\controllers';

	public function init()
	{
		parent::init();

		// custom initialization code goes here
	}
}
