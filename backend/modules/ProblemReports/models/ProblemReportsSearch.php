<?php

namespace backend\modules\ProblemReports\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ProblemReports\models\ProblemReports;

/**
 * ProblemReportsSearch represents the model behind the search form about `backend\modules\ProblemReports\models\ProblemReports`.
 */
class ProblemReportsSearch extends ProblemReports
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'userId', 'eventId', 'imgId1', 'imgId2', 'imgId3', 'status', 'important', 'valid'], 'integer'],
			[['problemTitle', 'problemDescription', 'problemPage', 'response', 'requestDate', 'statusChangeDate'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params,$status=NULL)
	{
		$query = ProblemReports::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		if(!is_null($status))
		{
			if (array_key_exists ( $status, self::Status_list ))
				$query->andWhere(['status'=>$status]);
		}
		if (!Yii::$app->user->can ( 'SeniorUserEvent' ))
		{
			$query->andWhere(['userId'=>$status]);
		}
// 		$this->load($params);

// 		if (!$this->validate()) {
// 			// uncomment the following line if you do not want to return any records when validation fails
// 			// $query->where('0=1');
// 			return $dataProvider;
// 		}

// 		$query->andFilterWhere([
// 			'id' => $this->id,
// 			'userId' => $this->userId,
// 			'eventId' => $this->eventId,
// 			'imgId1' => $this->imgId1,
// 			'imgId2' => $this->imgId2,
// 			'imgId3' => $this->imgId3,
// 			'status' => $this->status,
// 			'important' => $this->important,
// 			'requestDate' => $this->requestDate,
// 			'statusChangeDate' => $this->statusChangeDate,
// 			'valid' => $this->valid,
// 		]);

// 		$query->andFilterWhere(['like', 'problemTitle', $this->problemTitle])
// 			->andFilterWhere(['like', 'problemDescription', $this->problemDescription])
// 			->andFilterWhere(['like', 'problemPage', $this->problemPage])
// 			->andFilterWhere(['like', 'response', $this->response]);

		return $dataProvider;
	}
}
