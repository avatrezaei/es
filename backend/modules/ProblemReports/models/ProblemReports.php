<?php

namespace backend\modules\ProblemReports\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
/* need for file upload */
use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;
use backend\modules\YiiFileManager\models\FileAction;

/**
 * This is the model class for table "problem_reports".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $eventId
 * @property string $problemTitle
 * @property string $problemDescription
 * @property integer $imgId1
 * @property integer $imgId2
 * @property integer $imgId3
 * @property string $problemPage
 * @property integer $status
 * @property integer $important
 * @property string $response
 * @property string $requestDate
 * @property string $statusChangeDate
 * @property integer $valid
 */
class ProblemReports extends \yii\db\ActiveRecord {
	public $fileImg1;
	public $fileImg2;
	public $fileImg3;
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%problem_reports}}';
	}
	/**
	 * @inheritdoc
	 *
	 * @return ProvinceQuery the active query used by this AR class.
	 */
	public static function find() {
		return new ProblemReportsQuery( get_called_class () );
	}
	/**
	 *
	 * @var Status
	 */
	const Status_Unread = 1;
	const Status_Read = 2;
	const Status_Pending = 3;
	const Status_Done = 4;
	const Status_list = [ 
			self::Status_Unread => 'Unread',
			self::Status_Read => 'Read',
			self::Status_Pending => 'Pending',
			self::Status_Done => 'Done' 
	];
	/**
	 *
	 * @var Important
	 */
	const Important_Necessary = 1;
	const Important_Average = 2;
	const Important_UnNecessary = 3;
	const Important_list = [ 
			self::Important_Necessary => 'Necessary',
			self::Important_Average => 'Average',
			self::Important_UnNecessary => 'UnNecessary' 
	];
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		$fileModel = new File ();
		return [ 
				[ 
						[ 
								'userId',
								'eventId',
								'problemTitle',
								'problemDescription',
								'status' 
						],
						'required' 
				],
				[ 
						[ 
								'userId',
								'eventId',
								'imgId1',
								'imgId2',
								'imgId3',
								'status',
								'important',
								'valid' 
						],
						'integer' 
				],
				[ 
						[ 
								'problemDescription',
								'response' 
						],
						'string' 
				],
				[ 
						[ 
								'requestDate',
								'statusChangeDate' 
						],
						'safe' 
				],
				[ 
						[ 
								'problemTitle',
								'problemPage' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'fileImg1',
								'fileImg2',
								'fileImg3' 
						],
						'file',
						'skipOnEmpty' => true,
						'extensions' => 'jpg,jpeg,png',
						'checkExtensionByMimeType' => true,
						'maxSize' => File::MAX_FILE_SIZE,
						'tooBig' => " حداکثر اندازه فایل باید " . $fileModel->humanReadableFileSize ( File::MAX_FILE_SIZE ) . '  باشد',
						'tooSmall' => "حداقل اندازه فایل باید  " . $fileModel->humanReadableFileSize ( File::MIN_FILE_SIZE ) . '  باشد',
						'minSize' => File::MIN_FILE_SIZE,
						'mimeTypes' => [ 
								'image/jpeg',
								'image/jpg',
								'image/png' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'id' => Yii::t ( 'app', 'ID' ),
				'userId' => Yii::t ( 'app', 'User ID' ),
				'eventId' => Yii::t ( 'app', 'Event ID' ),
				'problemTitle' => Yii::t ( 'app', 'Problem Title' ),
				'problemDescription' => Yii::t ( 'app', 'Problem Description' ),
				'imgId1' => Yii::t ( 'app', 'Img Id1' ),
				'imgId2' => Yii::t ( 'app', 'Img Id2' ),
				'imgId3' => Yii::t ( 'app', 'Img Id3' ),
				'problemPage' => Yii::t ( 'app', 'Problem Page' ),
				'status' => Yii::t ( 'app', 'Status' ),
				'important' => Yii::t ( 'app', 'Important' ),
				'response' => Yii::t ( 'app', 'Response' ),
				'requestDate' => Yii::t ( 'app', 'Request Date' ),
				'statusChangeDate' => Yii::t ( 'app', 'Status Change Date' ),
				'valid' => Yii::t ( 'app', 'Valid' ),
				'fileImg1' => Yii::t ( 'app', 'fileImg1' ),
				'fileImg2' => Yii::t ( 'app', 'fileImg2' ),
				'fileImg3' => Yii::t ( 'app', 'fileImg3' ) 
		];
	}
	/**
	 * *
	 */
	public function beforeValidate() {
		/**
		 * eventId
		 */
		if (isset ( Yii::$app->user->identity->eventId ))
			$this->eventId = Yii::$app->user->identity->eventId;
		else {
			throw new \yii\web\HttpException ( 400, Yii::t ( 'app', 'The Security error' ) );
		}
		/**
		 * *
		 */
		if (is_null ( $this->important ))
			$this->important = self::Important_Average;
		/**
		 */
		if($this->isNewRecord)
			$this->userId = Yii::$app->user->identity->id;
		else if (Yii::$app->user->can ( 'SeniorUserEvent' ))
			$this->responseUserId= Yii::$app->user->identity->id;
		/**
		 */
		if ($this->isNewRecord)
			$this->status = self::Status_Unread;
		return parent::beforeValidate ();
	}
	/**
	 * *
	 */
	public static function StatusFa($status = 0, $all = false) {
		if ($all == true) {
			$array = [ ];
			foreach ( self::Status_list as $key => $value )
				$array [$key] = Yii::t ( 'app', $value );
			return $array;
		} else {
			if (array_key_exists ( $status, self::Status_list ))
				return \Yii::t ( 'app', self::Status_list [$status] );
			else
				return Yii::t ( 'zii', 'Not set' );
		}
	}
	/**
	 * *
	 */
	public static function ImportantFa($status = 0, $all = false) {
		if ($all == true) {
			$array = [ ];
			foreach ( self::Important_list as $key => $value )
				$array [$key] = Yii::t ( 'app', $value );
			return $array;
		} else {
			if (array_key_exists ( $status, self::Important_list ))
				return \Yii::t ( 'app', self::Important_list [$status] );
			else
				return Yii::t ( 'zii', 'Not set' );
		}
	}
	/**
	 * *
	 */
	public function getRequestDateFa() {
		return ($this->requestDate != '0000-00-00 00:00:00') ? Yii::$app->jdate->date ( 'Y/m/d', strtotime ( $this->requestDate ) ) : Yii::t ( 'zii', 'Not set' );
	}
	/**
	 * *
	 */
	public function getStatusChangeDateFa() {
		return ($this->statusChangeDate != '0000-00-00 00:00:00') ? Yii::$app->jdate->date ( 'Y/m/d', strtotime ( $this->statusChangeDate ) ) : Yii::t ( 'zii', 'Not set' );
	}
	/**
	 * *
	 */
	public function getStatusUI() {
		$color = '';
		$icon = '';
		switch ($this->status) {
			case self::Status_Unread :
				$color = 'danger';
				$icon = 'icon-badge';
				break;
			case self::Status_Read :
				$color = 'warning';
				$icon = 'icon-badge';
				break;
			case self::Status_Pending :
				$color = 'info';
				$icon = 'icon-badge';
				break;
			case self::Status_Done :
				$icon = 'icon-badge';
				$color = 'success';
				break;
			default :
				$color = 'default';
				$color = 'blue-steel';
				break;
		}
		$index = $this->status;
		return '<span class="badge badge-' . $color . '"><i class="' . $icon . '"></i></span>';
	}
	/**
	 */
	public static function CountStatus($status = NULL) {
		if ($status == null)
			return 0;
		if (array_key_exists ( $status, self::Status_list ))
			return self::find ()->where ( [ 
					'status' => $status 
			] )->count ();
		else
			return 0;
	}
	/**
	 */
	public function uploadPicFile($fileImg = 0, $attributeImg = 0) {
		// if (is_null ( $fileImg ) || is_null ( $attributeImg ) || empty ( trim($fileImg) ) || empty ( trim($attributeImg) ) || $fileImg == 0 || $attributeImg == 0)
		// return true;
		$fileModel = new File ();
		$fileModel->scenario = File::MODEL_FILE_UPLOAD_SCENARIO;
		$fileModel->modelFileTyps = 'jpg,jpeg,png';
		$fileModel->modelMimeFileTyps = [ 
				'image/jpeg',
				'image/jpg',
				'image/png' 
		];
		$fileModel->modelFileskipOnEmpty = true;
		$fileModel->modelFileSize = File::MAX_FILE_SIZE;
		/**
		 * Add icon file*
		 */
		
		$fileModel->folder_id = Folders::SPORT_LOCATION_FOLDER_ID;
		$fileModel->model_id = $this->id;
		$fileModel->model_type = 'ProblemReports';
		$fileModel->temporaryFileAttr = \yii\web\UploadedFile::getInstance ( $this, $fileImg );
		
		if ($fileModel->temporaryFileAttr != null) {
			
			if ($fileModel->validate () && $fileModel->uploadModelFile ( 'ProblemReports' . $fileImg . '_' . $this->id )) {
				
				/* remove old file in edit action call */
				$Select_Img = '';
				switch ($attributeImg) {
					case 'imgId1' :
						$Select_Img = $this->imgId1;
						break;
					case 'imgId2' :
						$Select_Img = $this->imgId2;
						break;
					case 'imgId3' :
						$Select_Img = $this->imgId3;
						break;
				}
				if (isset ( $Select_Img )) {
					$oldFileId = $Select_Img;
					if (($oldFileModel = FileAction::findOne ( $oldFileId )) !== null) {
						$oldFileModel->removeFile ( $oldFileId );
					}
				}
				
				/* save file id in categorie table */
				switch ($attributeImg) {
					case 'imgId1' :
						$this->imgId1 = ( int ) $fileModel->id;
						break;
					case 'imgId2' :
						$this->imgId2 = ( int ) $fileModel->id;
						break;
					case 'imgId3' :
						$this->imgId3 = ( int ) $fileModel->id;
						break;
				}
				$this->update ( 0 );
				return true;
			} else {
				if (count ( $fileModel->errors ) > 0) {
					$msg = '';
					foreach ( $fileModel->errors as $attribute => $errors ) {
						foreach ( $errors as $error ) {
							$msg .= $error;
						}
					}
					$this->addError ( $fileImg, $msg );
					return false;
				}
			}
		}
		return true;
	}
	
	public function getIconThumbnail($numberImg, $fileId = null, $class = '', $width = '45px', $height = '45px') {
		if (isset ( $fileId )) {
			switch (( int ) $numberImg) {
				case 1 :
					$this->imgId1 = $fileId;
					break;
				case 2 :
					$this->imgId2 = $fileId;
					break;
				case 3 :
					$this->imgId3 = $fileId;
					break;
			}
		}
		
		/* find file of categories in db */
		$filemodel = null;
		switch (( int ) $numberImg) {
			case 1 :
				$filemodel = File::findOne ( $this->imgId1 );
				break;
			case 2 :
				$filemodel = File::findOne ( $this->imgId2 );
				break;
			case 3 :
				$filemodel = File::findOne ( $this->imgId3 );
				break;
		}
		if (! is_null ( $filemodel )) {
			$folderModel = new Folders ();
			$destination = $folderModel->ProvideRealFolderPath ( Folders::SPORT_LOCATION_FOLDER_ID ) . '/' . $filemodel->name . '.' . $filemodel->ext;
			$url = Url::to ( [ 
					'/YiiFileManager/file/view-file',
					'name' => $filemodel->hashed_name 
			] );
			if ($class == '')
				return Html::img ( $url, [ 
						'alt' => Html::encode ( $this->problemTitle ),
						'width' => $width,
						'height' => $height 
				] );
			else
				return Html::img ( $url, [ 
						'alt' => Html::encode ( $this->problemTitle ),
						'class' => $class,
						'width' => $width,
						'height' => $height 
				] );
		} else {
			if ($class == '')
				return '<span class="label label-info">' . Yii::t ( 'app', 'No Picture' ) . '</span>';
			else
				return '<span class="label label-info">' . Yii::t ( 'app', 'No Picture' ) . '</span>';
		}
	}
	
	public function getImageUrl($numberImg, $fileId = null)
	{
		if (isset ( $fileId )) {
			switch (( int ) $numberImg) {
				case 1 :
					$this->imgId1 = $fileId;
					break;
				case 2 :
					$this->imgId2 = $fileId;
					break;
				case 3 :
					$this->imgId3 = $fileId;
					break;
			}
		}
		
		$filemodel = null;
		switch (( int ) $numberImg) {
			case 1 :
				$filemodel = File::findOne ( $this->imgId1 );
				break;
			case 2 :
				$filemodel = File::findOne ( $this->imgId2 );
				break;
			case 3 :
				$filemodel = File::findOne ( $this->imgId3 );
				break;
		}
		if (! is_null ( $filemodel )) {
			$folderModel = new Folders ();
			$destination = $folderModel->ProvideRealFolderPath ( Folders::SPORT_LOCATION_FOLDER_ID ) . '/' . $filemodel->name . '.' . $filemodel->ext;
			$url = Url::to ( [
					'/YiiFileManager/file/view-file',
					'name' => $filemodel->hashed_name
			] );
			return $url;
		} else {
			return '#';
		}
	}
	
	public static  function allowChangeSatus()
	{
		if (Yii::$app->user->can ( 'SeniorUserEvent' ))
			return true;
			else
				return false;
	}
	
	
	
}
/**
 * This is the ActiveQuery class for [[]].
 *
 * @see
 */
class ProblemReportsQuery extends \yii\db\ActiveQuery
{
	public function init()
	{
		parent::init();
		return $this->andWhere([ProblemReports::tableName () . '.valid' => 1]);
	}
	
	public function User()
	{
	
	}

	public function all($db = null) {
		return parent::all ( $db );
	}
}
