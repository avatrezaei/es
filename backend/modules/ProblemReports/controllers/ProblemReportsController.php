<?php

namespace backend\modules\ProblemReports\controllers;

use Yii;
use yii\helpers\Html;
use backend\modules\ProblemReports\models\ProblemReports;
use backend\modules\ProblemReports\models\ProblemReportsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\data\Sort;
use backend\modules\YumUsers\models\User;

/**
 * ProblemReportsController implements the CRUD actions for ProblemReports model.
 */
class ProblemReportsController extends Controller {
	public $layout = '@app/views/layouts/column2';
	public function behaviors() {
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'post' 
								] 
						] 
				] 
		];
	}
	public function actions() {
		return [ 
				'editable' => [ 
						'class' => 'faravaghi\xeditable\XEditableAction',
						// 'scenario'=>'editable', //optional
						'modelclass' => ProblemReports::className () 
				] 
		];
	}
	/**
	 * Lists all ProblemReports models.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		$request = Yii::$app->request;
		
		$searchModel = new ProblemReportsSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		$columns = [ 
				[ 
						'class' => 'yii\grid\SerialColumn',
						'headerOptions' => [ 
								'class' => 'col-md-1' 
						] 
				],
				[ 
						'attribute' => 'userId',
						'enableSorting' => false,
						'value' => function ($model) {
							$user=User::findOne($model->userId);
							return $user->fullName;
						},
						'headerOptions' => [ 
								'class' => 'col-md-2' 
						] 
				],
				[ 
						'attribute' => 'problemTitle',
						'enableSorting' => false,
						'format' => 'raw',
						'value' => function ($model) {
							$str = '<div class="col-md-11">' . $model->problemTitle . '</div>';
							$str .= $model->statusUI;
							return $str;
						} 
				],
				[ 
						'attribute' => 'requestDate',
						'enableSorting' => false,
						'value' => function ($model) {
							return $model->requestDateFa;
						},
						'headerOptions' => [ 
								'class' => 'col-md-1' 
						] 
				],
				[ 
						'class' => \faravaghi\xeditable\XEditableColumn::className (),
						'attribute' => 'status',
						'enableSorting' => false,
						'format' => 'raw',
						'headerOptions' => [ 
								'class' => 'col-md-2' 
						],
						'value' => function ($model) {
							return ProblemReports::StatusFa ( $model->status );
						},
						'url' => Url::to ( [ 
								'editable' 
						] ),
						'dataType' => 'select',
						'editable' => [ 
								'source' => ProblemReports::StatusFa ( 0, true ),
								'placement' => 'right' 
						],
						'visible' => ProblemReports::allowChangeSatus () 
				],
				[ 
						'class' => 'fedemotta\datatables\EActionColumn',
						'header' => Yii::t ( 'app', 'Actions' ),
						'headerOptions' => [ 
								'width' => '90px' 
						],
						'template' => '{view} {update} {delete}',
						'visibleButtons' => [ 
								'update' => function ($model) {
									if (Yii::$app->user->can ( 'SeniorUserEvent' ))
										return true;
									if ($model->status != ProblemReports::Status_Unread)
										return false;
									else
										return true;
								},
								'delete' => function ($model) {
									if (Yii::$app->user->can ( 'SeniorUserEvent' ))
										return true;
									if ($model->status != ProblemReports::Status_Unread)
										return false;
									else
										return true;
								} 
						] 
				] 
		];
		
		$clientOptions = [ 
				'ajax' => [ 
						'url' => $request->url 
				],
				'order' => [ 
						[ 
								1,
								'asc' 
						] 
				] 
		];
		
		$start = $request->get ( 'start', 0 );
		$length = $request->get ( 'length', 10 );
		
		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination ();
		$_pagination->pageSize = $length;
		$_pagination->page = floor ( $start / $length );
		
		$dataProvider->pagination = $_pagination;
		
		$sortableColumn = array (
				NULL,
				NULL,
				'status',
				'requestDate',
				'important',
				'status' 
		);
		$searchableColumn = array (
				'problemTitle' 
		);
		
		$widget = Yii::createObject ( [ 
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn 
		] );
		
		if ($request->isAjax) {
			$result = $widget->getFormattedData ( $request->post ( 'draw' ) );
			
			echo json_encode ( $result );
			Yii::$app->end ();
		} else {
			return $this->render ( 'index', [ 
					'widget' => $widget 
			] );
		}
	}
	
	/**
	 * Displays a single ProblemReports model.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		$model = $this->findModel ( $id );
		
		if (isset ( $_REQUEST ["status"] ) && array_key_exists ( ( int ) $_REQUEST ["status"], ProblemReports::Status_list )) {
			$status = ( int ) $_REQUEST ["status"];
		} else if ($model->status == ProblemReports::Status_Unread)
			$status = ProblemReports::Status_Read;
		else
			$status = $model->status;
		$model->status = $status;
		$model->save ();
		
		if (Yii::$app->request->isAjax) {
			return $this->renderAjax ( 'view', [ 
					'model' => $model 
			] );
		} else {
			return $this->render ( 'view', [ 
					'model' => $model 
			] );
		}
	}
	/**
	 * Creates a new ProblemReports model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new ProblemReports ();
		$model->status = ProblemReports::Status_Unread;
		
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			
			$flag_saveFile = true;
			$flag_saveFile = $model->uploadPicFile ( 'fileImg1', 'imgId1' );
			
			$flag_saveFile = $model->uploadPicFile ( 'fileImg2', 'imgId2' );
			
			$flag_saveFile = $model->uploadPicFile ( 'fileImg3', 'imgId3' );
			
			if ($flag_saveFile == true) {
				return $this->redirect ( [ 
						'view',
						'id' => $model->id,
						'status' => ProblemReports::Status_Unread 
				] );
			}
		} else {
			return $this->render ( 'create', [ 
					'model' => $model 
			] );
		}
	}
	
	/**
	 * Updates an existing ProblemReports model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		if ($model->load ( Yii::$app->request->post () ) && $model->save ()) {
			
			$flag_saveFile = true;
			
			$flag_saveFile = $model->uploadPicFile ( 'fileImg1', 'imgId1' );
			$flag_saveFile = $model->uploadPicFile ( 'fileImg2', 'imgId2' );
			$flag_saveFile = $model->uploadPicFile ( 'fileImg3', 'imgId3' );
			
			if ($flag_saveFile == true) {
				
				return $this->redirect ( [ 
						'view',
						'id' => $model->id,
						'status' => $model->status 
				] );
			}
		} else {
			return $this->render ( 'update', [ 
					'model' => $model 
			] );
		}
	}
	
	/**
	 * Deletes an existing ProblemReports model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the ProblemReports model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id        	
	 * @return ProblemReports the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = ProblemReports::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}
