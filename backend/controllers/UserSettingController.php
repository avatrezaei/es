<?php

namespace backend\controllers;

use Yii;
use backend\models\UserSetting;
use app\models\Language;
use backend\models\UserSettingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\helpers\Json;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * UserSettingController implements the CRUD actions for UserSetting model.
 */
class UserSettingController extends Controller {
	public $layout = '@app/views/layouts/column2';

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'Detail', 'delete', 'Edit2'],
				'rules' => [

					[
						'actions' => ['index', 'view', 'delete', 'Detail'],
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
//			'verbs' => [
//				'class' => VerbFilter::className(),
//				'actions' => [
//					'delete' => ['post'],
//				],
//			],
		];
	}

   /**
	 * Creates a new UserSetting model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionSetting() {
		
		
		
		$model = new UserSetting();
		$model->setDefualtSettingForUser(89);		
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$model->user_id = $userId;


		if (isset($_POST['hasEditable'])) {
			// use Yii's response format to encode output as JSON
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

			// read your posted model attributes
			//$index = $_POST['lang'];
			if (isset($_POST['lang'])) {
				// read or convert your posted information
				$model->char_value = $_POST['lang'];
				if ($model->validate('char_value') && $model->save(0)) {
					$value = $model->char_value;
				} else {
					if (count($model->errors) > 0) {
						$message = 'error';

						foreach ($model->errors as $errorItem) {
							foreach ($errorItem as $error)
								$message .=$error;
						}
					}
				}

				// return JSON encoded output in the below format
				return ['output' => (Html::encode($value)), 'message' => isset($message) ? $message : ''];
			}
			// else if nothing to do always return an empty JSON encoded output
			else {
				return ['output' => '', 'message' => 'error'];
			}
		}

		return $this->render('create', [
					'model' => $model,
		]);
	}

	public function actionUsersetLanguage() {
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$model = UserSetting::findOne(['user_id' => $userId, 'key' => UserSetting::KEY_LANGUAGE]);

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		if ($model == NULL) {
			return ['output' => '', 'message' =>Yii::t('app', 'The requested Setting does not exist.')];
		}
		$model->scenario = 'language_setting';


		if (isset($_POST['hasEditable'])) {


			if (isset($_POST['lang'])) {
				// read or convert your posted information
				$model->char_value = $_POST['lang'];
				if ($model->validate('char_value') && $model->save(0)) {
					$value = Html::encode(Language::getLanguageText($model->char_value)); //$model->char_value;
				} else {
					if (count($model->errors) > 0) {
						$message = 'error';

						foreach ($model->errors as $errorItem) {
							foreach ($errorItem as $error)
								$message .=$error;
						}
					}
				}

				// return JSON encoded output in the below format
				return ['output' => (Html::encode($value)), 'message' => isset($message) ? $message : ''];
			}
			// else if nothing to do always return an empty JSON encoded output
			else {
				return ['output' => '', 'message' => 'error'];
			}
		}
	}

	public function actionUsersetPagination() {
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$model = UserSetting::findOne(['user_id' => $userId, 'key' => UserSetting::KEY_PAGINATION]);

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		if ($model == NULL) {
			return ['output' => '', 'message' =>Yii::t('app', 'The requested Setting does not exist.')];
		}
		$model->scenario = 'pagination_setting';

		if (isset($_POST['hasEditable'])) {

			if (isset($_POST['pagination'])) {
				// read or convert your posted information
				$model->int_value = $_POST['pagination'];
				if ($model->validate('int_value') && $model->save(0)) {
					$value = $model->int_value;
				} else {
					if (count($model->errors) > 0) {
						$message = 'error';

						foreach ($model->errors as $errorItem) {
							foreach ($errorItem as $error)
								$message .=$error;
						}
					}
				}

				// return JSON encoded output in the below format
				return ['output' => (Html::encode($value)), 'message' => isset($message) ? $message : ''];
			}
			// else if nothing to do always return an empty JSON encoded output
			else {
				return ['output' => '', 'message' => Yii::t('app', 'Value Not Set')];
			}
		}
	}

	public function actionUsersetTheme() {
		$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$model = UserSetting::findOne(['user_id' => $userId, 'key' => UserSetting::KEY_PAGINATION]);

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		if ($model == NULL) {
			return ['output' => '', 'message' =>Yii::t('app', 'The requested Setting does not exist.')];
		} 
		$model->scenario = 'theme_setting';

		if (isset($_POST['hasEditable'])) {

			if (isset($_POST['theme'])) {
				// read or convert your posted information
				$model->char_value = $_POST['theme'];
				if ($model->validate('char_value') && $model->save(0)) {
					$value = $model->char_value;
				} else {
					if (count($model->errors) > 0) {
						$message = '';

						foreach ($model->errors as $errorItem) {
							foreach ($errorItem as $error)
								$message .=$error;
						}
					}
				}

				// return JSON encoded output in the below format
				return ['output' => (Html::encode($value)), 'message' => isset($message) ? $message : ''];
			}
			// else if nothing to do always return an empty JSON encoded output
			else {
				return ['output' => '', 'message' => Yii::t('app', 'Value Not Set')];
			}
		}
	}




	/**
	 * Finds the UserSetting model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $user_id
	 * @param string $key
	 * @return UserSetting the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($user_id, $key) {
		if (($model = UserSetting::findOne(['user_id' => $user_id, 'key' => $key])) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
