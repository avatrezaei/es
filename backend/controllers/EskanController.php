<?php

namespace backend\controllers;

use Yii;
use app\models\Eskan;
use app\models\EskanSearch;
use app\models\Trree;
use app\models\TrreeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;


/**
 * EskanController implements the CRUD actions for Eskan model.
 */
class EskanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Eskan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new EskanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columns = [
            [
                'class' => 'yii\grid\SerialColumn' ,
                'headerOptions'=>['width'=>'10px',],
            ],
            [
                'attribute' => 'user',
                'header'=>'نام',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'70px',],
                'value'=>'user.name'
            ],
            [
                'attribute' => 'user',
                'header'=>'نام خانوادگی',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'100px',],
                'headerOptions'=>['width'=>'1px',],

                'value'=>'user.last_name'
            ],
            [
                'attribute' => 'user',
                'header'=>'شماره دانشجویی',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'50px',],
                'headerOptions'=>['width'=>'10px',],

                'value'=>'user.student_number'
//function($model){
//                  //  return  $model->user->name.' '.$model->user->last_name;
//                },

            ],

//            [
//                'label'=>'شماره دانشجویی',
//                'enableSorting' => false,
//                'format' => 'raw',
//                'filter' => false,
//                'headerOptions'=>['width'=>'100px',],
//                'value'=>function($model){
//                    return  $model->user->student_number;
//                },
//            ],
            [
                'attribute' => 'room_id',
                'label'=>'آدرس اتاق',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'200px',],
                'value'=>function($model){
                    return  $model->getAddress($model->room_id);
                },

            ],
            [
                'label'=>'وضعیت تحویل',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'100px',],
                'value'=>function($model){
                    return  $model->getstate();
                },
            ],
            [
                'attribute' => 'year',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'100px',],
            ],
            [
                'attribute' => 'term',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'100px',],
            ],
            [
                'attribute' => 'entry_date',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'150px',],
            ],
            [
                'attribute' => 'exit_date',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'150px',],
            ],
            [
                'class' => 'fedemotta\datatables\EActionColumn',
                'header' => Yii::t ( 'app', 'Actions' ),
                'headerOptions' => [ ],
                'template' => '{view}',
//                'visibleButtons' => [
//                    'view' => \Yii::$app->user->can('viewEskan'),
//                   //'delete' => \Yii::$app->user->can('deleteEskan'),
//                ]
            ],
            ];
        $clientOptions = [
            'ajax' => [
                'url' => $request->url
            ],
            'order' => [
                [
                    1,
                    'asc'
                ]
            ]
        ];
        $start = $request->get ( 'start', 0 );
        $length = $request->get ( 'length', 10 );
        $_pagination = new Pagination ();
        $_pagination->pageSize = $length;
        $_pagination->page = floor ( $start / $length );

        $dataProvider->pagination = $_pagination;
        $sortableColumn = array (

            NULL,
            'room_id',
            'PersonID',
            'term',
            'year',
            'entry_date',
            'exit_date',
//            'room_id',
//            'caravan_id',
//            'mem_num',
//            'role',
        );
        $searchableColumn = array ('student_number','name','last_name','entry_date','exit_date','term','year',

        );

        $widget = Yii::createObject ( [
            'class' => 'fedemotta\datatables\DataTables',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
            'columns' => $columns,
            'clientOptions' => $clientOptions,
            'sortableColumn' => $sortableColumn,
            'searchableColumn' => $searchableColumn
        ] );

        if ($request->isAjax) {
            $result = $widget->getFormattedData ( $request->post ( 'draw' ) );

            echo json_encode ( $result );
            Yii::$app->end ();
        } else {
            return $this->render ( 'index2', [
                'widget' => $widget
            ] );
        }

    }

    /**
     * Displays a single Eskan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model1=new Trree();
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
                'model1'=>$model1,
            ]);
        }
        else{
            return $this->render('view', [
                'model' =>  $this->findModel($id),
                'model1'=>$model1,
            ]);
        }
    }

    /**
     * Creates a new Eskan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Eskan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Eskan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->delivery=1;
            $model->save();
           // die(';llmj');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Eskan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $roomid=$model->room_id;
        $model->delete();

        return $this->redirect(['trree/build','id'=>$roomid]);
    }

    /**
     * Finds the Eskan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Eskan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Eskan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
