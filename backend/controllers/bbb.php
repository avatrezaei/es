<?php

namespace backend\controllers;

use Yii;
use app\models\Trree;
use app\models\TrreeSearch;
use app\models\Eskan;
use yii\base\ExitException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrreeController implements the CRUD actions for Trree model.
 */
class TrreeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trree models.
     * @return mixed
     */
    public function actionBuild()
    {
        // echo 'Hello';
        $model = new Eskan();

        if ($model->load(Yii::$app->request->post()) )
        {
            // die;
            $_eskan = Yii::$app->request->post('Eskan');
            //die(var_dump($_eskan));
            foreach ($_eskan['PersonID'] as $person)
            {
                $haseskan = Eskan::find()->andWhere(['PersonID' => $person])->one();
                if (!$haseskan) {
                    //die(var_dump(Yii::$app->request->post()));
                    $tempMpdel = new Eskan();
                    $tempMpdel->room_id = $_eskan['room_id'];
                    $tempMpdel->PersonID = $person;
                    $tempMpdel->save();
                    //return $this->redirect(['view']);
                }
            }
            return $this->redirect(['view']);
        }
        else{

            return $this->render('create1', [
                'model' => $model,
                //'placeList'=>ArrayHelper::map(Place::find()->all(),'id','name','free'),
            ]);
        }

    }
    public function actionState()
    {
        $model = new Trree();
        $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $eskan=Eskan::find()->andWhere(['PersonId'=>$curentUserId])->one();
        if($eskan)
        {
            return $this->render('state', [
                'model' => $model,
                'room'=>$eskan->room_id,
            ]);
        }
        else
        {
            return $this->render('state', [
                'model' => $model,
                'room'=>'',
            ]);
        }
    }
    public function actionIndex()
    {
        $searchModel = new TrreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trree model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
        $model = new Trree();
        return $this->render('_treeview', [
            'model' => $model,
        ]);
    }

    public function actionGetchild($parent){
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        if($parent == '#') {
            $result = Trree::find()->andWhere(['parent_id' => 0])->all();
        }
        else {
            $result = Trree::find()->andWhere(['parent_id' => $parent])->all();
        }

        $out = [];
        foreach ($result as $data){
            $url=Url::to(['trree/create', 'parent_id' => $data->id]);
            $url1=Url::to(['trree/build', 'id' => $data->id]);
            // echo Url::to([\'post/view\', \'id\' => 100]);
            $hasChild = Trree::find()->andWhere(['parent_id' => $data->id])->count();
            if($data->type!=1)
                $txt=$data->name.'      '.'<a href="update/'.$data->id.'">'.'ویرایش'.'</a>'.'       '.'<a href="'.$url1.'">تخصیص اتاق به دانشجو</a>';
            else
                $txt=$data->name.'      '.'<a href="update/'.$data->id.'">'.'ویرایش'.
                    '</a>'.'      '.'<a href="'.$url.'">'.'افزودن گره جدید'.'</a>';
            if($data->enable==1)
                $enab=false;
            else
                $enab=true;
            $out[] = [
                'id' => $data->id,
                'text' => $txt,
                'children' => $hasChild> 0 ? true : false,
                //'icon' => "string",
                'state'=>['disabled'=>$enab],
            ];
        }
        return $out;
    }

    /**
     * Creates a new Trree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //die;

        $model = new Trree();
        $flag = true;
        //$model->parent_id = $_GE
        //die(var_dump(Yii::$app->request->post()));
        if (Yii::$app->request->post()) {
            // die;
            // die(var_dump(Yii::$app->request->post()));
            $FromNo = $_POST['FromNo'];
            $ToNo = $_POST['ToNo'];


            for ($i = $FromNo; $i <= $ToNo; $i++) {
                $model = new Trree();
                $model->load(Yii::$app->request->post());

                $model->name = $model->name . ' ' . $i;
                //if($model->type=='خوابگاه')

                //print_r($model);;
                //$result = false;
                $result=Trree::find()->andWhere(['parent_id'=>$model->parent_id, 'name'=>$model->name])->one();
                if(!$result){
                    if (!$model->save()) {
                        $flag = false;
                    }
                }
                else
                    Yii::warning('tekrari');
            }
            if($flag==true)
                return $this->redirect(['view']);
        }
        else {
            //die;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Trree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Trree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
