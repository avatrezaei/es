<?php

namespace backend\controllers;

use Yii;
use app\models\Setting;
use backend\models\SettingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\helpers\Json;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * SettingController implements the CRUD actions for Setting model.
 */
class SettingController extends Controller {
	public $layout = '@app/views/layouts/column2';

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'Detail', 'delete', 'Edit2'],
				'rules' => [

					[
						'actions' => ['index', 'view', 'delete', 'Detail'],
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
//			'verbs' => [
//				'class' => VerbFilter::className(),
//				'actions' => [
//					'delete' => ['post'],
//				],
//			],
		];
	}

	/**
	 * Lists all Setting models.
	 * @return mixed
	 */
	public function actionIndex() {
		$model = new Setting();
		$massage = '';
		if ($model->load(Yii::$app->request->post())) {
			if ($model->save()) {
				$massage = '<div class="alert alert-success" role="alert">
					<a href="#" class="alert-link">' . Yii::t('app', '{item} Successfully Added.', array('item' => Yii::t('app', 'Setting'))) . '</a>
					</div>';
			} else {
				$model->addError('content', Yii::t('app', 'Failed To Add Items To {item}.', ['item' => Yii::t('app', 'Setting')]));
			}

		}

		$searchModel = new SettingSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
					'model' => $model,
					'massage' => $massage,
		]);
	}

   public function actionEditevalue($id) {

		if (isset($_POST['editableKey']))
			$id = $_POST['editableKey'];


		$model = $this->findModel($id); // your model can be loaded here
		$model->scenario = 'create_setting';


		if (isset($_POST['hasEditable'])) {
			// use Yii's response format to encode output as JSON
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

			// read your posted model attributes
			$index = $_POST['editableIndex'];
			if (isset($_POST['Setting'][$index]['value'])) {
				// read or convert your posted information
				$model->value = $_POST['Setting'][$index]['value'];
				if ($model->save()) {
					$value = $model->value;
				} else {
					if (count($model->errors) > 0) {
						$message = 'error';

						foreach ($model->errors as $errorItem) {
							foreach ($errorItem as $error)
								$message .=$error;
						}
					}
				}

				// return JSON encoded output in the below format
				return ['output' => (Html::encode($value)), 'message' => isset($message) ? $message : ''];
			}
			// else if nothing to do always return an empty JSON encoded output
			else {
				return ['output' => '', 'message' => 'error'];
			}
		}
	}

   public function actionDetail() {
		if (isset($_POST['expandRowKey']))
			$id = Yii::$app->request->post('expandRowKey');
		else
			die(('Not Found'));


		$model = $this->findModel($id);

		return $this->renderAjax('expand_detail', [
					'model' => $model,
		]);
	}



//	public function actionDelete($id = NULL) {
//		$post = Yii::$app->request->post();
//
//		if (Yii::$app->request->isAjax) {
//
//			if (isset($post['id']))
//				$id = (int) $post['id'];
//			else if (isset($_GET['id']))
//				$id = (int) $_GET['id'];
//
//			$model=$this->findModel($id);
//			if ($model->delete()) {
//			   
//				echo Json::encode([
//					'success' => true,
//					'messages' => [Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Setting')])]]);
//			} else {
//				$detailView=$this->renderAjax('expand_detail', ['model' => $model]);
//				echo Json::encode([
//					'success' => false,
//					'detailviewcontentt'=>$detailView,
//					'messages' => [Yii::t('app','Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Setting')])]]);
//		  
//			}
//		  // return;
//		} 
////		else if (isset($_GET['id'])) {
////			$id = (int) $_GET['id'];
////			$this->findModel($id)->delete();
////			return;
////		}
//		//throw new InvalidCallException("You are not allowed to do this operation. Contact the administrator.");
//	}
//
 public function actionDelete($id = NULL) {
		$post = Yii::$app->request->post();

		if (Yii::$app->request->isAjax) {

			if (isset($post['id']))
				$id = (int) $post['id'];
			else if (isset($_GET['id']))
				$id = (int) $_GET['id'];

			$modelObj = $this->findModel($id);
			/* -----------defalt category cannt delete----------- */
			if ($modelObj != NULL && $modelObj->category == Setting::DEFAULT_CATEGORY) {
				$editable = false;
			} else {
				$editable = TRUE;
			}

			/* -----------defalt category cannt delete----------- */
			if ($modelObj != NULL && $editable) {
				if ($modelObj->delete()) {
  Yii::$app->response->format = Response::FORMAT_JSON;
				  return[
				   'success' => true,
				   'messages' =>Yii::t('app', '{item} Successfully Deleted.', ['item' => Yii::t('app', 'Setting')])]
				  ; 
				} else {
  Yii::$app->response->format = Response::FORMAT_JSON;
				  return[
				   'success' => false,
				   'messages' =>Yii::t('app', 'Failed To Delete Items To {item}.', ['item' => Yii::t('app', 'Setting')])]
				  ; 
				}
			} else {

  Yii::$app->response->format = Response::FORMAT_JSON;
				  return[
				   'success' => false,
				   'messages' =>Yii::t('app', 'Failed To Delete Default Category Items To {item}.this Action Is impossible', ['item' => Yii::t('app', 'Setting')])]
				  ; 
			}
			return;
		} else if (isset($_GET['id'])) {
			$id = (int) $_GET['id'];
			$modelObj = $this->findModel($id);

			if ($modelObj != NULL && $modelObj->category == Setting::DEFAULT_CATEGORY) {
				$editable = false;
			} else {
				$editable = TRUE;
			}
			if ($editable) {
				$modelObj->delete();
				echo Json::encode([
					'success' => true,
				]);
			} else {
				echo Json::encode([
					'success' => FALSE,
				]);
			}

			return;
		}
				throw new InvalidCallException("You are not allowed to do this operation. Contact the administrator.");

 }

	public function actionEdit2($id) {

		$post = Yii::$app->request->post();
		if (Yii::$app->request->isAjax) {
			$id = (int) $_GET['id'];
			if ($model = $this->findModel($id)) {
				if ($model->load(Yii::$app->request->post()) && $model->save()) {  
				  $detailView=$this->renderAjax('expand_detail', ['model' => $model]);
					 echo json_encode(
							   [
							  //	 'errormessage'=>$msg,
								   'detailviewcontent'=>$detailView
							   ]
							   );
				} else {
					if (count($model->errors) > 0) {
						$msg = '';
						foreach ($model->errors as $attribute => $errors) {
							foreach ($errors as $error) {
								$msg.= $error . '<br>';
							}
						}
					  
					   $detailView=$this->renderAjax('expand_detail', ['model' => $model]);
					   echo json_encode(
							   [
								   'errormessage'=>$msg,
								   'detailviewcontent'=>$detailView
							   ]
							   );
					}
				}
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		} else {
			throw new ForbiddenHttpException('The requested params forbidden');
		}
	}

	protected function findModel($id) {
		if (($model = Setting::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
