<?php

namespace backend\controllers;

use app\models\Familyform;
use backend\models\User;
use backend\models\UserSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\Json;
//use yii\filters\AccessControl;

use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\data\Sort;

use app\models\Exitform;
use app\models\ExitformSearch;
use faravaghi\jalaliDatePicker\jalaliDatePicker;
use faravaghi\jalaliDateRangePicker\jalaliDateRangePicker;

/**
 * ExitformController implements the CRUD actions for Exitform model.
 */
class ExitformController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [


		];
	}

	/**
	 * Lists all Exitform models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$searchModel = new ExitformSearch();

       // $searchModel = new UserSearch();
//        $searchModel->_userType = User::USER_TYPE_STUDENT;
 $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$columns = [
			['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'users',
                'header'=>'نام',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'70px',],
                'value'=>'users.name'

            ],
            [

                'attribute' => 'users',
                'header'=>'نام خانوادگی',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'70px',],
                'value'=>'users.last_name'

            ],
            [
                'attribute' => 'users',
                'header'=>'شماره دانشجویی',
                'enableSorting' => false,
                'format' => 'raw',
                'filter' => false,
                'headerOptions'=>['width'=>'70px',],
                'value'=>'users.student_number'

            ],
            [

                'header' => Yii::t('app', 'Userrel ID'),
                'enableSorting' => false,
                'headerOptions'=>['width'=>'200px',],
                'value'=> function ($model) {
                    return $model->user->FName.' '.$model->user->LName.'-'.$model->user->address;
                },
            ],
            [
                'attribute' => 'relation',
                'header'=>'نسبت',
                'enableSorting'=>false,
                'value'=> function ($model) {
                    return $model->user->relation;
                },
            ],
			[
				'attribute' => 'from_date',
                'filter' => false,
                'enableSorting'=>false
			],
			[
				'attribute' => 'to_date',
                'filter' => false,
                'enableSorting'=>false
			],
            [
                'attribute' => 'description',
                'enableSorting'=>false,
                'filter' => false,

            ],
        ];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = ['id','user_id','userrel_id','from_date','to_date','description'];
		$searchableColumn = ['student_number','name','last_name','user_id','userrel_id','from_date','to_date','description'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
                'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,

                'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}

	/**
	 * Displays a single Exitform model.
	 * @param integer $id
	 * @return mixed
	 */
    public function actionUser() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $users_id = $parents[0];
                $out = Familyform::find()->andWhere(['user_id'=>$users_id])->all();
                //$out1=$out['name'].$out['lname'];
                //die(var_dump($out));
                $out1 = ArrayHelper::toArray($out, [
                    'app\models\Familyform' => [
                        'id',
                        'name' => function ($model) {
                            return $model->users;
                        },
                    ],
                ]);
                //die(var_dump($out));

               // $out1=ArrayHelper::toArray($out1);
                echo Json::encode(['output'=>$out1, 'selected'=>'']);
                return;

            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
	public function actionView($id)
	{
		$model = $this->findModel($id);

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Creates a new Exitform model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Exitform();

		if ($model->load(Yii::$app->request->post())) {
            $_exit = Yii::$app->request->post('Exitform');
            $model->from_date= $_exit['from_date'];
            $model->to_date= $_exit['to_date'];
           // die(var_dump($_exit));
            $model->save();
			return $this->redirect(['create']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}
    public function actionAdmincreate()
    {
        $model = new Exitform();
        // $get = $_GET(['request']);

//die(var_dump($get));
        $user_id = '';
        if(isset($_GET['user_id']))
            $user_id = $_GET['user_id'];
        if ($model->load(Yii::$app->request->post())) {
//                $_MODEL=Yii::$app->request->post('Familyform');
//                die(var_dump($_MODEL));
            $model->user_id=$user_id;
            $model->save();
            //die(var_dump($model));
            return $this->redirect(['admincreate','user_id'=>$user_id]);
        }

        return $this->render('admincreate', [
            'model' => $model,
            'user_id' => $user_id

        ]);
    }
    public function actionTime()
    {
        $model = new Exitform();
        $model->from_date =$model->dateDisplayFormat();
        $pizza=$model->from_date;
        $pieces = explode("-", $pizza);
//echo $pieces[0];
        $year=(int)$pieces[0];
        $month=(int)$pieces[1];
        $day=(int)$pieces[2];
        echo $year;
        echo '<br>';
        echo $month;
        echo '<br>';
        echo $day;

//        return $this->render('time', [
//            'model' => $model
//           // 'user_id' => $user_id
//        ]);
    }

	/**
	 * Updates an existing Exitform model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {

            $_exit = Yii::$app->request->post('Exitform');
            //die(var_dump($_exit));
            $model->from_date= $_exit['from_date'];
            $model->to_date= $_exit['to_date'];
            $model->save();
            return $this->redirect(['create']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}
    public function actionAdminupdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $_exit = Yii::$app->request->post('Exitform');
            //die(var_dump($_exit));
            // die;
            $model->save(false);
            return $this->redirect(['admincreate']);
        } else {
            return $this->render('admincreate', [
                'model' => $model,
            ]);
        }
    }

	/**
	 * Deletes an existing Exitform model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$post = Yii::$app->request->post();
		
		$model = $this->findModel($id);
		$model->valid = 0;
		$_item = $model->name;

		if($model->save(false))
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => true,
					'messages' =>[
						Yii::t('app', '{item} Successfully Deleted.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Deleted.',[
					'item' => $_item,
				]));
				$this->redirect(['index']);
			}
		}
		else
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => false,
					'messages' =>[
						Yii::t('app', 'Failed To Delete Items To {item}.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', 'Failed To Delete Items To {item}.',[
					'item' => $_item,
				]));

				$this->redirect(['index']);
			}
		}
	}

	/**
	 * Finds the Exitform model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Exitform the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Exitform::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
