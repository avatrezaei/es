<?php

namespace backend\controllers;

use backend\models\Userd;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\Json;


//use yii\filters\AccessControl;

use yii\helpers\Url;
use mdm\admin\components\AccessControl;

use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\data\Sort;

use backend\models\User;
use backend\modules\YumUsers\models\AuthAssignment;
use backend\models\UserSearch;
use backend\models\ChangePassword;
use yii\web\UploadedFile;
use common\models\SignupForm;

/**
 * PersonController implements the CRUD actions for Person model.
 */
class PersonController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['index', 'create', 'update','delete'],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => ['index', 'create','update','delete'],
//                        'roles' => ['admin','supervisor'],
//                    ],
//
//                ],
                'only' => [ 'create','update','delete','createuser','createstudent','switchuser'],

            ],
        ];
	}
	 /**
     *
     * Block comment
     *
     */
    public function actionCreatestudent(){
         $model = new SignupForm();
         $model->scenario = 'create_student';
         $model->type     = User::USER_TYPE_STUDENT;
         
         

         $message = "";
        if ($model->load(Yii::$app->request->post())) {
           // die(var_dump($model->load(Yii::$app->request->post())));
            if ($user = $model->signup("student")) {
                $message=$this->createMessageCss('success',Yii::t('app', 'Student Successfully Created.'));
            }
            
        }
            return $this->render('createstudent', [
                'model'   => $model,
                'message' => $message,
            ]);
         

    }
    /**
     *
     * Block comment
     *
     */
    public function actionCreateuser(){
         $model = new SignupForm();
         $model->scenario = 'create_user';
         $model->type     = User::USER_TYPE_USER;
         $message = "";
        if ($model->load(Yii::$app->request->post())) {
            $_createuser = Yii::$app->request->post('SignupForm');
            $role=$_createuser['roles'];
           // $a=$model->load(Yii::$app->request->post());
            //die(var_dump($role));
            if ($user = $model->signup($role)) {
                $message=$this->createMessageCss('success',Yii::t('app', 'User Successfully Created.'));
            }
            
        }
            return $this->render('createuser', [
                'model'   => $model,
                'message' => $message,
            ]);
         

    }
    

	/**
	 * Lists all Person models.
	 * @return mixed
	 */
//	public function actionIndex()
//	{
//		$request = Yii::$app->request;
//		$searchModel = new UserSearch();
//		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//		$columns = [
//			['class' => 'yii\grid\SerialColumn'],
//
//
//			[
//				'attribute' => 'fullName',
//				'enableSorting'=>false
//			],
//
//			[
//				'attribute' => 'universityName',
//				'enableSorting'=>false
//			],
//
//
//			[
//				'attribute' => 'birthDate',
//				'enableSorting'=>false
//			],
//
//			[
//				'attribute' => 'nationalId',
//				'enableSorting'=>false
//			],
//
//			[
//				'attribute' => 'student_number',
//				'enableSorting'=>false
//			],
//
//
//
//
//
//
//			[
//				'attribute' => 'startingyear',
//				'enableSorting'=>false
//			],
//
//			[
//				'attribute' => 'startingsemester',
//				'enableSorting'=>false
//			],
//
//			[
//				'attribute' => 'username',
//				'enableSorting'=>false
//			],
//
//			[
//				'class' => 'fedemotta\datatables\EActionColumn',
//				'header' => Yii::t('app', 'Actions'),
//				'headerOptions' => ['width' => '100'],
//			],
//		];
//
//		$clientOptions = [
//			'ajax'=>[
//				'url' => $request->url,
//			],
//			'order' => [
//				[ 1, 'asc' ]
//			],
//		];
//
//		$start = $request->get('start', 0);
//		$length = $request->get('length', 10);
//
//		/**
//		 * Create Pagination Object for paging:
//		 */
//		$_pagination = new Pagination;
//		$_pagination->pageSize = $length;
//		$_pagination->page = floor($start / $length);
//
//		$dataProvider->pagination = $_pagination;
//
//		$sortableColumn = ['id','name'];
//		$searchableColumn = ['id','name'];
//
//		$widget = Yii::createObject([
//				'class' => 'fedemotta\datatables\DataTables',
//				'dataProvider' => $dataProvider,
//				'filterModel' => NULL,
//				'columns' => $columns,
//				'clientOptions' => $clientOptions,
//				'sortableColumn' => $sortableColumn,
//				'searchableColumn' => $searchableColumn,
//			]
//		);
//
//		if ($request->isAjax) {
//
//			$result = $widget->getFormattedData($request->post('draw'));
//			echo json_encode($result);
//			Yii::$app->end();
//		}
//		else{
//			return $this->render('index', [
//				'widget' => $widget,
//			]);
//		}
//	}


	/**
	 *
	 * Block comment
	 *
	 */
	
	public function actionUsers(){
		$request = Yii::$app->request;
		$searchModel = new UserSearch();
		$searchModel->_userType = User::USER_TYPE_USER;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$columns = [
			['class' => 'yii\grid\SerialColumn'],


			[
				'attribute' => 'fullName',
				'enableSorting'=>false
			],		 
 

			 
			 
			[
				'attribute' => 'nationalId',
				'enableSorting'=>false
			],

			[
				'attribute' => 'email',
				'enableSorting'=>false
			],
			
			 
 
			
		 

			[
				'attribute' => 'username',
				'enableSorting'=>false
			],

				[
						'class' => 'fedemotta\datatables\EActionColumn',
						'header' => Yii::t('app', 'Actions'),
						'template' => '{view} {delete} {profile}',
						'headerOptions' => ['width' => '150'],
						'buttons' => [


								'profile' => function ($url, $model,$data){

									$_userId = $model->id;
									$_login = Html::a(
											'<i class="icon-user " ></i>',
											Url::to(['/members/profileview', 'id' => $_userId], true),
											[
													'class' => 'hint--top hint--rounded hint--info',
													'data-hint' => Yii::t('app', 'View profile info'),
											]
									);
									return   $_login;
								},



						],
				],

		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = ['id','name'];
		$searchableColumn = ['id','name','last_name','student_number'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			
			$result = $widget->getFormattedData($request->post('draw'));
			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('users', [
				'widget' => $widget,
			]);
		}
	}
	/**
	 *
	 * Block comment
	 *
	 */
	public function actionStudents(){
		$request = Yii::$app->request;
		$searchModel = new UserSearch();
		$searchModel->_userType = User::USER_TYPE_STUDENT;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$columns = [
			['class' => 'yii\grid\SerialColumn'],


			[
				'attribute' => 'fullName',
				'enableSorting'=>false
			],

			[
				'attribute' => 'universityName',
				'enableSorting'=>false
			],
 

			// [
			// 	'attribute' => 'birthDate',
			// 	'enableSorting'=>false
			// ],
			 
			[
				'attribute' => 'nationalId',
				'enableSorting'=>false
			],
			
			[
				'attribute' => 'student_number',
				'enableSorting'=>false
			],



			
			 
			 
			
			
			// [
			// 	'attribute' => 'startingyear',
			// 	'enableSorting'=>false
			// ],

			// [
			// 	'attribute' => 'startingsemester',
			// 	'enableSorting'=>false
			// ],

			[
				'attribute' => 'username',
				'enableSorting'=>false
			],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'template' => '{view} {delete} {profile}',
				'headerOptions' => ['width' => '150'],
				'buttons' => [
					 

					'profile' => function ($url, $model,$data){
						 
						$_userId = $model->id;
						$_login = Html::a(
							'<i class="icon-user " ></i>',
							Url::to(['/members/profileview', 'id' => $_userId], true),
							[
								'class' => 'hint--top hint--rounded hint--info',
								'data-hint' => Yii::t('app', 'View profile info'),
							]
						);
						return   $_login; 
					},
					 
					 
					 
				],
			],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = ['id','name'];
		$searchableColumn = ['id','name'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			
			$result = $widget->getFormattedData($request->post('draw'));
			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('student_users', [
				'widget' => $widget,
			]);
		}
	}
	

	

	/**
	 * Displays a single Person model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Creates a new Person model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$message='';
		$model = new User();
		$model->created_at = Yii::$app->jdate->date('Y/m/d');
		$model->updated_at = Yii::$app->jdate->date('Y/m/d');
		if ($model->load( Yii::$app->request->post() ) )
		{

			$user_picture 					= UploadedFile::getInstances($model, 'user_picture')[0]; 
			$birth_certificate_picture_face = UploadedFile::getInstances($model, 'birth_certificate_picture_face')[0]; 
			$birth_certificate_picture_back = UploadedFile::getInstances($model, 'birth_certificate_picture_back')[0]; 
			$national_card_pciture 			= UploadedFile::getInstances($model, 'national_card_pciture')[0];

			//echo "<pre>";print_r($user_picture);echo "</pre>";die();
			if($model->save()){
				$model->upload($user_picture);
				$model->upload($birth_certificate_picture_face);
				$model->upload($birth_certificate_picture_back);
				$model->upload($national_card_pciture);

				$message=$this->createMessageCss('success',Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'File')]));

			} 

		}
		
		 
 		return $this->render('create', [
				'model'   => $model,
				'message' => $message,
			]);
	}

	/**
	 * Updates an existing Person model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
//	public function actionUpdate($id)
//	{
//
//		$model = $this->findModel($id);
//        //$model = $this->findModel($id);
//$modeld=Userd::find()->andWhere(['user_id'=>$id])->one();
//		if ($model->load(Yii::$app->request->post()) && $model->save()) {
//			return $this->redirect(['view', 'id' => $model->id]);
//		} else {
//			return $this->render('update', [
//				'model' => $model,
//                'modeld' => $modeld,
//			]);
//		}
//	}

	/**
	 * Deletes an existing Person model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$post = Yii::$app->request->post();
		
		$model = $this->findModel($id);
		$model->valid = 0;
		$_item = $model->name;

		if($model->save(false))
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => true,
					'messages' =>[
						Yii::t('app', '{item} Successfully Deleted.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Deleted.',[
					'item' => $_item,
				]));
				$this->redirect(['index']);
			}
		}
		else
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => false,
					'messages' =>[
						Yii::t('app', 'Failed To Delete Items To {item}.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', 'Failed To Delete Items To {item}.',[
					'item' => $_item,
				]));

				$this->redirect(['index']);
			}
		}
	}

			public function createMessageCss($msgType,$string)
   {

	   switch ($msgType) {
		   case 'success':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
					break;

		   case 'danger':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
					break;
		   default:
				 $message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
			   break;
	   }

	   return $message;

   }



	/**
	 * Finds the Person model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Person the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
