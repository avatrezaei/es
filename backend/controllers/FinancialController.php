<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\data\Pagination;
use yii\data\Sort;
use yii\data\ActiveDataProvider;

use common\components\Rezvan;

use backend\models\Draft;
use backend\models\User;
use backend\models\Messages;
use backend\models\CaravanLog;
use backend\models\EventMembers;
use backend\models\UserSearch;
use backend\models\MemberPayment;
use backend\models\MemberPaymentSearch;
use backend\models\AuthorizedUniversity;

/**
 * FinancialController implements the CaravanPayment model.
 */
class FinancialController extends \yii\web\Controller
{
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create','update','delete','register-fish'],
                        'roles' => ['admin','supervisor'],
                    ],
                     
                ],
            ],
        ];
	}

	public function actions()
	{
		return [
			'editable' => [
				'class' => 'faravaghi\xeditable\XEditableAction',
				'modelclass' => MemberPayment::className(),
			],
		];
	}

	/**
	 * Lists all Caravan models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$searchModel = new UserSearch();
		$dataProvider = $searchModel->searchPayment(Yii::$app->request->queryParams);

		$columns = [
			['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'fullname',
				'enableSorting' => false,
				'filter' => false,
				'format' => 'html',
				'value' => function($model) {
					return $model->Fullname;
				}
			],
			[
				'attribute' => 'university',
				'enableSorting' => false,
				'filter' => ArrayHelper::map(AuthorizedUniversity::find()->all(), 'universityId', 'UniversityName'),
				'filterOptions' => ['id' => 'member-university'],
				'value' => function ($model) {
					return $model->university->name;
				}
			],
			 
			[
				'header' => Yii::t('app', 'Total bill approved'),
				'filter' => false,
				'enableSorting' => false,
				'value' => function ($model) {
					return $model->TotalBillApproved;
				}
			],
			

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '60px'],
				'template' => '{showInfo}',
				'buttons' => [
					'showInfo' => function($url, $model) {
						if(count($model->memberPayment) > 0)
						{
							$_change = Html::a(
								'<i class="icon-wallet font-blue"></i>',
								Url::to(['payments', 'id' => $model->id]),
								[
									'title' => Yii::t('app', 'Payments'),
									'data-hint' => Yii::t('app', 'Payments'),
									'class' => 'hint--top hint--rounded hint--info'
								]
							);
						}
						else
						{
							$_change = '<i class="icon-wallet font-grey"></i>';
						}

						return $_change;
					}
				],
			]
		];

		$clientOptions = [
			'ajax' => [
				'url' => $request->url
			],
			 
			 
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination();
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);
		
		$dataProvider->pagination = $_pagination;

		$sortableColumn = [NULL,'person.firstname','university.name'];
		$searchableColumn = ['university.name'];

		$widget = Yii::createObject([
			'class' => 'fedemotta\datatables\DataTables',
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
			'columns' => $columns,
			'clientOptions' => $clientOptions,
			'sortableColumn' => $sortableColumn,
			'searchableColumn' => $searchableColumn
		]);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));
			
			echo json_encode($result);
			Yii::$app->end();
		} else {
			return $this->render('index', [
				'searchModel' => $searchModel,
				'widget' => $widget,
			]);
		}
	}

	/**
	 * Lists of All Payment Caravan.
	 *
	 * @return mixed
	 */
	public function actionPayments($id=NULL)
	{
		$_caravan = NULL;

		if($id != NULL)
		{
			/**
			 * Get Caravan Information
			 */
			$_member = User::find()->andWhere(['id' => (int)$id])->one();
			
			/**
			 * Check if Caravan not exist
			 */
			if($_member == NULL){
				Yii::$app->session->setFlash('error', Yii::t('app', 'Member not found!'));
				return $this->redirect(['index']);
			}
		}

		$request = Yii::$app->request;
		$searchModel = new MemberPaymentSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

		$columns = [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'username',
				'filter' => false,
				'enableSorting'=>false,
				'value' => function ($model) 
                {
                    return  $model->userName;                    
                }
			],

			[
				'attribute' => 'universityName',
				'filter' => false,
				'enableSorting'=>false,
				'format' => 'html',
				'value' => function($model){
					return  $model->universityName;
				},
				'visible' => ($id == NULL)
			],





			[
				'attribute' => 'paymentNo',
				'filter' => false,
				'enableSorting'=>false
			],

			[
				 
				'attribute' => 'amount',
				'filter' => false,
				'format' => 'raw',
				'enableSorting'=>false,
				'value' => function($model){
					return $model->amount;
				},
				 
				 
			],


			[
				 
				'attribute' => 'paymentTime',
				'filter' => false,
				'format' => 'raw',
				'enableSorting'=>false,
				'value' => function($model){
					return $model->paymentTime;
				},
				 
				 
			],

			 

 

			 

			[
				'attribute' => 'paymentDate',
				'filter' => false,
				'enableSorting'=>false
			],

			[
				'attribute' => 'payFori',
				'format' => 'raw',
				'filter' => false,
				'enableSorting'=>false
			],


			
			
			 
			 
			 

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '100'],
				'template' => '{delete} {view}',
				 
			],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => ($id == NULL) ? false : [
				[ 5, 'asc' ],
				[ 4, 'asc' ],
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = [NULL,'paymentNo','personId','amount','paymentDate','paymentTime'];
		$searchableColumn = ['member.name','paymentNo','paymentDate','amount'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'filterPosition' => \yii\grid\GridView::FILTER_POS_HEADER,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('_payments', [
				'widget' => $widget,
				'caravan' => $_caravan,
			]);
		}
	}

	/**
	 * Deletes an existing Person model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$post = Yii::$app->request->post();
		
		$model = $this->findModel($id);
		$model->valid = 0;
		$_item = $model->id;

		if($model->save(false))
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => true,
					'messages' =>[
						Yii::t('app', '{item} Successfully Deleted.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Deleted.',[
					'item' => $_item,
				]));
				$this->redirect(['index']);
			}
		}
		else
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => false,
					'messages' =>[
						Yii::t('app', 'Failed To Delete Items To {item}.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', 'Failed To Delete Items To {item}.',[
					'item' => $_item,
				]));

				$this->redirect(['index']);
			}
		}
	}

 

 
	/**
	 * Finds the CaravanPayment model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return CaravanPayment the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = MemberPayment::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Finds the Caravan model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id		   
	 * @return Caravan the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}



	public function actionRegisterFish()
	{
		 

		$model = new MemberPayment();

		 
		if ($model->load(Yii::$app->request->post()))
		{

			$model->valid = 1;			 

			if ($model->validate() && $model->save())
			{
				 
				$msg = Yii::t('app', 'information Successfully Stored');
				Yii::$app->session->setFlash('success', $msg);
				 
			}
		}

		return $this->render('_form_upload_fish', [
			'model' => $model,
		]);
	}

	/**
	 *
	 * Block comment
	 *
	 */

	public function actionRegisterFishUser($id)
	{
		 

		$model = new MemberPayment();
		$user = User::find()->where(['id'=>$id])->one();
		 
		if ($model->load(Yii::$app->request->post()))
		{

			$model->valid = 1;			 
			$model->personId = $id;
			if ($model->validate() && $model->save())
			{
				 
				$msg = Yii::t('app', 'information Successfully Stored');
				Yii::$app->session->setFlash('success', $msg);
				 
			}
		}

		return $this->render('_form_upload_fish_2', [
			'model' => $model,
			'user'  => $user,
		]);
	}
	
}