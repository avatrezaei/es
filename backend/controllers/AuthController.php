<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * AuthController implements
 */
class AuthController extends Controller
{
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [
			'access' => [ 
				'class' => AccessControl::className (),
				'rules' => [ 
					[ 
						'actions' => ['index'],
						'allow' => true,
						'roles' => [ 
							'admin'
						] 
					] 
				] 
			]
		];
	}
	
	/**
	 * Lists all University models.
	 * @return mixed
	 */
	public function actionIndex($q)
	{
		$auth = Yii::$app->authManager;
		$suffix = $q;

		// add "create" permission
		$create = $auth->createPermission('create' . $suffix);
		$create->description = 'Create a ' . $suffix;
		$auth->add($create);

		// add "update" permission
		$update = $auth->createPermission('update' . $suffix);
		$update->description = 'Update ' . $suffix;
		$auth->add($update);

		// add "view" permission
		$view = $auth->createPermission('view' . $suffix);
		$view->description = 'View ' . $suffix;
		$auth->add($view);

		// add "delete" permission
		$delete = $auth->createPermission('delete' . $suffix);
		$delete->description = 'Delete ' . $suffix;
		$auth->add($delete);

		// add "admin" permission
		$admin = $auth->createPermission('admin' . $suffix);
		$admin->description = 'Admin ' . $suffix;
		$auth->add($admin);

		// add a role and give this role the above permission
		$newRole = $auth->createRole($suffix);
		$auth->add($newRole);
		$auth->addChild($newRole, $view);
		$auth->addChild($newRole, $create);
		$auth->addChild($newRole, $update);
		$auth->addChild($newRole, $delete);
		$auth->addChild($newRole, $admin);

		$admin = $auth->getRole('SeniorUserEvent');
		$auth->addChild($admin, $newRole);

		echo "<pre>";print_r($auth->getChildren($suffix));echo "</pre>";die();
	}
}
