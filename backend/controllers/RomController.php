<?php

namespace backend\controllers;

use Yii;
use backend\models\Rom;
use backend\models\RomSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * RomController implements the CRUD actions for Rom model.
 */
class RomController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rom models.
     * @return mixed
     */
	  public function actionUpload()
    {
		die;
        $model = new Rom;
    if (!empty($_POST)) {
        $model->image = $_POST['Rom']['image'];
        $file = UploadedFile::getInstance($model, 'image');
        var_dump($file);

        // You can then do the following
        if ($model->save()) {
            $file->saveAs('uploads/' . $this->image->baseName . '.' . $this->image->extensione);
        }
        // its better if you relegate such a code to your model class
    }
    return $this->render('_form', ['model'=>$model]);
    }
    public function actionIndex()
    {
        $searchModel = new RomSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rom model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		//die;
        $model = new Rom();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				//$model->image = $_POST['Rom']['image'];
				$file = UploadedFile::getInstance($model, 'image');
				//var_dump($file);
				// You can then do the following
				$file->saveas('uploads/' . $model->image->baseName . '.' . $model->image->extensione);
				//return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Rom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Rom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['create']);
    }

    /**
     * Finds the Rom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
