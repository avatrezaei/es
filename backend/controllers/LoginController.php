<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\captcha\CaptchaAction;

use common\models\User;
use common\models\Invitees;
use common\models\LoginForm;
use common\models\SignupForm;
use common\models\CreditList;
use common\models\Consultant;

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'transparent'=>true,
				'testLimit'=> 0,
			],
		];
	}

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='column3';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		$session = Yii::$app->session;
		// $session->remove('attempts-login');

		if ($session->has('attempts-login') && $session['attempts-login']  > 2){
			$model->scenario = 'withCaptcha';
		}
		else{
			$model->scenario = 'login';
		}

		if($model->load(Yii::$app->request->post())){
			if ($model->login()) {
				/**
				 * After Login Remove Session:
				 */
				$session->remove('attempts-login');

				/**
				 * Get user model
				 */
				$_user = $model->User;
				//die(var_dump($_user));

				/**
				 * in first login redirect to ...
				 */
				if ($_user->last_login_time == '0000-00-00 00:00:00') {
					// return $this->redirect(['force-change-pass']);
				}
				else {
					Yii::$app->session->set('last_login_time', $_user->last_login_time);
				}

				/**
				 * Set last login time
				 */
				$_user->LastLoginTime = time();

				return $this->goBack();
			}
			else{
				# If login is not successful, increase the attemps 
				$session['attempts-login'] = $session->has('attempts-login') ? $session['attempts-login'] + 1 : 1;

				if ($session['attempts-login']  > 2) { 
					# Useful only for view
					$model->scenario = 'withCaptcha';
				}
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::$app->user->id);
		$lastVisit->Lastvisit = time();
		$lastVisit->save(false);
	}

}