<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use mdm\admin\components\AccessControl;
use common\models\LoginForm;
use backend\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login','logout','return','switchuser'],
                 
            ],
        ];
    }

	public function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			if ($action->id=='error' && Yii::$app->user->isGuest){
				$this->layout ='@app/views/layouts/error';
			}
			else{
				$this->layout ='@app/views/layouts/column2';
			}

			return true;
		}
		else
		{
			return false;
		}
	}
    public function actionSwitchuser($id, $save=NULL)
    {
        /**
         * Get user identity
         */
        $user = User::findOne($id);

        /**
         * Get Last UserId
         */
        $lastUserId = Yii::$app->user->identity->id;

        /**
         * Set Duration
         */
        $duration = 0;

        /**
         * Switch user
         */
        Yii::$app->user->switchIdentity($user, $duration);

        /**
         * Save Last UserId for return this
         */
        if(isset($save) && $save == 'yes'){
            Yii::$app->session->set('lastUser', $lastUserId);
            Yii::$app->session->set('referrer', Yii::$app->request->referrer);
        }

        /**
         * redirect to index
         */
        return $this->redirect(['/members/profileinfo']);
    }

    public function actionReturn()
    {
        /**
         * Get user identity
         */
        $user = User::findOne(Yii::$app->session->get('lastUser'));

        /**
         * Remove Session
         */
        Yii::$app->session->remove('lastUser');

        /**
         * Set Duration
         */
        $duration = 0;

        /**
         * Switch user
         */
        Yii::$app->user->switchIdentity($user, $duration);

        /**
         * redirect to index
         */
        $referrer = (Yii::$app->session->has('referrer') ? Yii::$app->session->get('referrer') : Url::to(['/site/index'], true));

        return $this->redirect($referrer);
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

	public function actionError()
	{
		$exception = Yii::$app->errorHandler->exception;
		if ($exception !== null) {
			return $this->render('error', ['exception' => $exception]);
		}
	}
}
