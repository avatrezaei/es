<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\SignupForm;

class SignupController extends Controller
{
	public $defaultAction = 'signup';
	//public $returnLogoutUrl = ['/login'];

	/**
	 * @inheritdoc
	 */
	 	 	public $layout='column3';

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}

	/**
	 * Logout the current user and redirect to returnLogoutUrl.
	 */
	public function actionSignup()
	{
		$model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->origsignup("student")) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
	}

}