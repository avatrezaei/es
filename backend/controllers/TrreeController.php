<?php

namespace backend\controllers;

use backend\models\User;
use Yii;
use app\models\Trree;
use app\models\TrreeSearch;
use app\models\Eskan;
use yii\base\ExitException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrreeController implements the CRUD actions for Trree model.
 */
class TrreeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trree models.
     * @return mixed
     */
    public function actionBuild()
    {
        $a=$_GET['id'];
        $room=Trree::find()->andWhere(['id'=>$a])->one();

        // echo 'Hello';
        $model = new Eskan();
        if($room->enable==1){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

            if ($model->load(Yii::$app->request->post()) )
        {
           // die;
            $_eskan = Yii::$app->request->post('Eskan');

            //die(var_dump($_eskan));
            $flag=[];
            $i=1;
            try {

                foreach ($_eskan['PersonID'] as $person) {

                    $haseskan = Eskan::find()->andWhere(['PersonID' => $person, 'year' => $_eskan['year'], 'term' => $_eskan['term']])->one();
                    $roomId=$_GET['id'];
                    if (!$haseskan) {
                        //die(var_dump(Yii::$app->request->post()));
                        $tempMpdel = new Eskan();
                        $tempMpdel->room_id = $_GET['id'];;
                        $tempMpdel->year = $_eskan['year'];
                        $tempMpdel->term = $_eskan['term'];
                        $tempMpdel->delivery = 0;
                       // $tempMpdel->entry_date = $_eskan['entry_date'];
                        $tempMpdel->exit_date = '0000-00-00';
                        $tempMpdel->PersonID = $person;
                        $tempMpdel->save();
                    } else {
                        $flag[$i] =User::getFullStudentName($person);

                    }
                    $i++;
                }
                $transaction->commit();

            } catch (Exception $e) {
                $transaction->rollback();
            }


            if(!empty($flag))
            {
                $transaction->rollback();
                $nameStr=implode($flag,',');
                $msg = 'افراد زیر با این مشخصات خوابگاه دریافت کرده اند! لطفا در انتخابتان آن ها را در نظر نگیرید:'.$nameStr;
                $model->adderror('PersonID',$msg);
            }else
            {
                return $this->redirect(['trree/build','id'=>$roomId]);

            }

        }
        return $this->render('create1', [
            'model' => $model,
        ]);
        }
        else{
           // $msg = 'افراد زیر با این مشخصات خوابگاه دریافت کرده اند! لطفا در انتخابتان آن ها را در نظر نگیرید:';
//die($msg);
          return $this->redirect(['view']);
        }
    }
    public function actionState()
    {
        $model = new Trree();
        $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $eskan=Eskan::find()->andWhere(['PersonId'=>$curentUserId])->one();
        if($eskan)
        {
            if($eskan->delivery==1)
            {
        return $this->render('state', [
            'model' => $model,
            'room'=>$eskan->room_id,
        ]);
            }
            else
            {
                return $this->render('state', [
                    'model' => $model,
                    'room'=>'x',
                ]);
            }
        }
        else
        {
            return $this->render('state', [
                'model' => $model,
                'room'=>'',
            ]);
        }
    }
    public function actionIndex()
    {
        $searchModel = new TrreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trree model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
        $model = new Trree();
        return $this->render('_treeview', [
            'model' => $model,
        ]);
    }

    public function actionGetchild($parent){
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        if($parent == '#') {
            $result = Trree::find()->andWhere(['parent_id' => 0])->all();
        }
        else {
            $result = Trree::find()->andWhere(['parent_id' => $parent])->all();
        }

        $out = [];
        foreach ($result as $data) {
            $url = Url::to(['trree/create', 'parent_id' => $data->id]);
            $url1 = Url::to(['trree/build', 'id' => $data->id]);
            $url2 = Url::to(['trree/enable', 'id' => $data->id]);
            // echo Url::to([\'post/view\', \'id\' => 100]);
            $hasChild = Trree::find()->andWhere(['parent_id' => $data->id])->count();
            if ($data->type != 1)
            {
                if($data->enable==1)
                    $txt=$data->name.'      '.'<a href="update/'.$data->id.'">'.'ویرایش'.'</a>'.'       '.'<a href="'.$url1.'">تخصیص اتاق به دانشجو</a>'.
                        ' '.'<a href="'.$url2.'" >غیرفعال</a>';
                else
                    $txt=$data->name.'      '.'<a href="update/'.$data->id.'">'.'ویرایش'.'</a>'.'   '.'<a href="'.$url2.' '.'" >فعال</a>';

            }

            else
                if($data->enable==1)
                $txt=$data->name.'      '.'<a href="update/'.$data->id.'">'.'ویرایش'.
                    '</a>'.'      '.'<a href="'.$url.'">'.'افزودن مکان جدید'.'</a>'.'   '.'<a href="'.$url2.' '.'" >غیرفعال</a>';
                else
                    $txt=$data->name.'      '.'<a href="update/'.$data->id.'">'.'ویرایش'.
                        '</a>'.'      '.'<a href="'.$url.'">'.'افزودن مکان جدید'.'</a>'.'   '.'<a href="'.$url2.' '.'" >فعال</a>';

            if($data->enable==1)
                $enab=false;
            else
                $enab=true;
            $out[] = [
                'id' => $data->id,
                'text' => $txt,
                'children' => $hasChild> 0 ? true : false,
                //'icon' => "string",
                'state'=>['disabled'=>$enab],
            ];
        }
        return $out;
    }

    /**
     * Creates a new Trree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionEnable()
    {
        $_tree =$_GET['id'];
        $model = $this->findModel($_tree);
        //$model->enab
        if($model->enable==1)
        {
            $model->disable($_tree);
//            $model->save(false);
            return $this->redirect(['view']);
        }
        else
        {
            $model->enable=1;
            $model->save(false);
            return $this->redirect(['view']);
        }
    }
    public function actionCreate()
    {

        $model = new Trree();
        $flag = true;
        //die(var_dump(Yii::$app->request->post()));
        if (Yii::$app->request->post()) {

            $_tree = Yii::$app->request->post('Trree');
//            $_tree->parent_id =
            //die(var_dump($_tree));
//            die(var_dump($_tree));
            // die;
            // die(var_dump(Yii::$app->request->post()));
            //$FromNo = $_POST['FromNo'];
            //$ToNo = $_POST['ToNo'];


            for ($i = $_tree['FromNo']; $i <= $_tree['ToNo']; $i++) {
                $model = new Trree();
                $model->load(Yii::$app->request->post());
                if(!$_tree['enable'])
                    $model->enable=1;
                $model->parent_id=$_GET['parent_id'];
                $model->name = $model->name . ' ' . $i;
                //if($model->type=='خوابگاه')

                //print_r($model);;
                //$result = false;
                $result=Trree::find()->andWhere(['parent_id'=>$model->parent_id, 'name'=>$model->name])->one();
                if(!$result){
                    if (!$model->save()) {
                        $flag = false;
                    }
                }
                else
                    Yii::warning('tekrari');
            }
            if($flag==true)
                return $this->redirect(['view']);
        }
        else {
            //die;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Trree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->enable==0){
            $childs=Trree::find()->andWhere(['parent_id'=>$model->id])->all();
                foreach ($childs as $id)
                {
                    $model->disable($id['id']);
                }
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Trree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
