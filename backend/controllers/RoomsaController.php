<?php

namespace backend\controllers;

use backend\models\Caravan;
use backend\models\EventMembers;
use backend\modules\YumUsers\models\UsersType;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use backend\models\Roomsa;
use backend\models\Subset;
use backend\models\Floor;
use backend\models\Rom;
use backend\models\Suite;
use yii\helpers\ArrayHelper;
use backend\models\Eskan;
use backend\models\Place;
use app\models\RoomsaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RoomsaController implements the CRUD actions for Roomsa model.
 */
class RoomsaController extends Controller
{
    public $layout = '@app/views/layouts/column2';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
//                    [
//                        'actions' => ['index','create','update','editable','delete','view'],
//                        'allow' => true,
//                        'matchCallback' => function($rule, $action) {
//                            return !Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin === true;
//                        }
//                    ],
                    [
                        'actions' => ['index'],
                        'roles' => ['adminRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['view'],
                        'roles' => ['viewRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['create'],
                        'roles' => ['createRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['update', 'editable'],
                        'roles' => ['updateRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['delete'],
                        'roles' => ['deleteRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['place'],
                        'roles' => ['placeRoomsa'],
                        'allow' => true,
                    ],

                    [
                        'actions' => ['subset'],
                        'roles' => ['subsetRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['suite'],
                        'roles' => ['suiteRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['floor'],
                        'roles' => ['floorRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['delete'],
                        'roles' => ['deleteRoomsa'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['room'],
                        'roles' => ['roomRoomsa'],
                        'allow' => true,
                    ],
                ]
            ]
        ];
    }


    /**
     * Lists all Roomsa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoomsaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Roomsa model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Roomsa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Eskan();
        $model->setScenario('register');

        if ($model->load(Yii::$app->request->post()) ) 
		{
            $_eskan = Yii::$app->request->post('Eskan');
             //die(var_dump($_eskan));   
            if($_eskan['room_id'])
            {
                foreach ($_eskan['room_id'] as $roomid)
            {
                if(!is_numeric($roomid))
                    throw new NotFoundHttpException('Invalid Attribute.');
            }
            foreach ($_eskan['room_id'] as $roomid)
            {
                $tempMpdel=new Eskan();
                $tempMpdel->caravan_id=$_eskan['caravan_id'];
                $tempMpdel->place_id=$_eskan['place_id'];
                $tempMpdel->subset_id=$_eskan['subset_id'];
                $tempMpdel->floor_id=$_eskan['floor_id'];
                $tempMpdel->suite_id=$_eskan['suite_id'];
                $tempMpdel->role=$_eskan['role'];
				$tempMpdel->room_id=$roomid;             
               // die(var_dump($tempMpdel->subset_id));
                $tempMpdel->save();
           }}
           else
           {
            foreach ($_eskan['suite_id'] as $suiteid)
            {
                if(!is_numeric($suiteid))
                    throw new NotFoundHttpException('Invalid Attribute.');
            }
            foreach ($_eskan['suite_id'] as $suiteid)
            {
            $tempMpdel=new Eskan();
                $tempMpdel->caravan_id=$_eskan['caravan_id'];
                $tempMpdel->place_id=$_eskan['place_id'];
                $tempMpdel->subset_id=$_eskan['subset_id'];
                $tempMpdel->floor_id=$_eskan['floor_id'];
                $tempMpdel->suite_id=$suiteid;
                $tempMpdel->role=$_eskan['role'];
                $tempMpdel->room_id=$_eskan['room_id'];             
               // die(var_dump($tempMpdel->subset_id));
                $tempMpdel->save();
           }
            }

			Yii::$app->session->setFlash ( 'success', Yii::t ( 'app', 'rooms inserted for caravan {item}', [
						'item' => Yii::t ( 'app', $model->caravanName )
				] ) );

								
 return $this->redirect(Yii::$app->request->referrer);
        } else {
			 
            return $this->render('create', [
                'model' => $model,
				'placeList'=>ArrayHelper::map(Place::find()->all(),'id','name','free'),
            ]);
        }
    }

    public function actionPlace() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $caravan_id = $parents[0];
                $caravan=Caravan::find()->andWhere(['id'=>$caravan_id])->one();

                $place = Place::find()->andWhere(['>=', 'free',$caravan->calculateCaravanUsersCount('all')])->all();
                // $place = Place::find()->all();
//                    echo "<pre>"; print_r($out);echo "</pre>";die();
                    $out = ArrayHelper::toArray($place, [
                        'backend\models\Place' => [
                            'id',
                            'name' => function ($model) {
                                return $model->NameFree;
                            },
                        ],
                    ]);
                    echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
              
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

	
	public function actionSubset() {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$place_id = $parents[0];
				$out = Subset::find()->addSelect(['id','name'])->andWhere(['parent_id'=>$place_id])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;

			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	public function actionFloor() {
	 
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
		$ids = $_POST['depdrop_parents'];
        $place_id = empty($ids[0]) ? null : $ids[0];
        $subset_id = empty($ids[1]) ? null : $ids[1];
			if ($subset_id != null) {
			
				$out = Floor::find()->addSelect(['id','name'])->andWhere(['parent_id'=>$subset_id])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;
			}
			else if ($place_id != null) {
				
				$out = Floor::find()->addSelect(['id','name'])->andWhere(['parent_id'=>$place_id])->asArray()->all(); 
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	public function actionSuite() {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$suite_id = $parents[0];
				$out = Suite::find()->addSelect(['id','name'])->andWhere(['parent_id'=>$suite_id, 'selected'=> 0] )->asArray()->all();
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	public function actionRoom() {
			 $out = [];
    if (isset($_POST['depdrop_parents'])) {
$ids = $_POST['depdrop_parents'];
        $floor_id = empty($ids[0]) ? null : $ids[0];
        $suite_id = empty($ids[1]) ? null : $ids[1];	
		if ($suite_id != null) {
				$out = Rom::find()->addSelect(['id','name'])->andWhere(['parent_id'=>$suite_id,'enable'=> 1,'selected'=> 0])->asArray()->all();
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;
			}
			else{
			$out = Rom::find()->addSelect(['id','name'])->andWhere(['parent_id'=>$floor_id,'enable'=> 1,'selected'=> 0])->asArray()->all();
				echo Json::encode(['output'=>$out, 'selected'=>'']);
				return;	
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
    /**
     * Updates an existing Roomsa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->RoomID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Roomsa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	
	
	
	

    /**
     * Finds the Roomsa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Roomsa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Roomsa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
