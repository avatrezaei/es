<?php

namespace backend\controllers;

use backend\models\Mdate;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\Json;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\data\Sort;
use yii\helpers\Url;
use backend\models\Request;
use backend\models\User;
use backend\models\RequestSearch;
use backend\models\RequestForm;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [
			 'access' => [
			 	'class' => AccessControl::className(),
			 	'rules' => [
			 		[
			 			'actions' => ['index','view','update','create','delete','changestatus','userrequests'],
			 			'roles' => ['admin'],
			 			'allow' => true,
			 		],
			 		[
			 			'actions' => ['viewby-user','updateby-user','userrequests','make'],
			 			'roles' => ['student'],
			 			'allow' => true,
			 		],


			 	]
			 ],
			 'verbs' => [
			 	'class' => VerbFilter::className(),
			 	'actions' => [
			 		'delete' => ['POST'],
			 	],
			 ],
		];
	}

	public function actions()
	{
		return [
			'editable' => [
				'class' => 'faravaghi\xeditable\XEditableAction',
				'scenario'=>'editable',  //optional
				'modelclass' => Request::className(),
			],
		];
	}

	/**
	 * Lists all Request models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$searchModel = new RequestSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$array = [
						 ['id' => Request::REQUEST_STATUS_REQUESTED , 'name' => Yii::t('app', 'Not Checked')],
	  					 ['id'=> Request::REQUEST_STATUS_REJECTED	, 'name' => Yii::t('app', 'Accepted')],
	  					 ['id' => Request::REQUEST_STATUS_ACCEPTED 	, 'name' => Yii::t('app', 'Not Accepted')]
					];
		$columns = [
			['class' => 'yii\grid\SerialColumn'],


			[
				'attribute' => 'personname',
				'filter' => false,
				'enableSorting'=>false,				
				'value' => function($model){
					 return $model->personname;
				}
			],

			[
				'attribute' => 'personcode',
				'value' => function($model){
					 return $model->personcode;
				}
			],

			[
				'attribute' => 'personuniversity',
				'value' => function($model){
					 return $model->personuniversity;
				}
			],

			[
				'attribute' => 'eduYear',
				'enableSorting'=>false,
                'value' => function($model){
                    return $model->getYear($model->eduYear);
                }
			],

			[
				'attribute' => 'semester',
				'enableSorting'=>false,
                'value' => function($model){
                    return $model->getSemester($model->semester);
                }
			],
			[
				'attribute' => 'date',
				'enableSorting'=>false,
			],

			 
			
			[ 
				'class' => \faravaghi\xeditable\XEditableColumn::className(),
				'attribute' => 'status',
				'enableSorting'=>false,
				'format' => 'raw',
				'value' => function($model) {
					return $model->statusi;
				},
				'url' => Url::to(['editable']),
				'dataType'=>'select',
				'editable'=>[
					'source'=>  Request::itemAlias('Status'),
					'placement' => 'right',
				],
			],

			 
			 


			
			

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '100'],
			],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = [NULL,'semester','date','dateIn','dateOut','comment'];
		$searchableColumn = ['user.name','user.student_number'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}
	/**
	 *
	 * Block comment
	 *
	 */

	/**
	 * Change Self Service Status
	 */
	public function actionChangestatus($id)
	{
		$model = $this->findModel($id);
		$model->status =($model->status == Request::REQUEST_STATUS_ACCEPTED ? Request::REQUEST_STATUS_REJECTED : Request::REQUEST_STATUS_ACCEPTED);

		if($model->save(false))
			$Message = Yii::t('app', '{item} Successfully {status}.');
		else
			$Message = Yii::t('app', 'Error status update {item}!');
		
		$msg = strtr($Message, array(
			'{item}' => Yii::t('app','Request').' '.$model->personName,
			'{status}' => Yii::t('zii',($model->status == Request::REQUEST_STATUS_ACCEPTED ? 'Active' : 'Deactive')),
		));

		echo Json::encode(array(
			'enable' =>(bool) $model->status,
			'message' => $msg,
			'content' => '<span class="' .($model->status == Request::REQUEST_STATUS_ACCEPTED ? 'enable' : 'disable') . '"></span>',
			'hint' => Yii::t('zii',($model->status == Request::REQUEST_STATUS_ACCEPTED ? 'Active' : 'Deactive')) 
		));

		Yii::$app->end();
	}

	

	/**
	 * Lists all Request models.
	 * @return mixed
	 */
	public function actionUserrequests()
	{
		$userId      = Yii::$app->user->identity->id;
		$request = Yii::$app->request;
		$searchModel = new RequestSearch(['personId'=>$userId  ]);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$columns = [
			['class' => 'yii\grid\SerialColumn'],


			[
				'attribute' => 'personname',
				'filter' => false,
				'enableSorting'=>false,				
				'value' => function($model){
					 return $model->personname;
				}
			],

			[
				'attribute' => 'personcode',
				'value' => function($model){
					 return $model->personcode;
				}
			],

			[
				'attribute' => 'personuniversity',
				'value' => function($model){
					 return $model->personuniversity;
				}
			],

			[
				'attribute' => 'eduYear',
				'enableSorting'=>false,
			],

			[
				'attribute' => 'semester',
				'enableSorting'=>false,
			],
			[
				'attribute' => 'date',
				'enableSorting'=>false,
			],
			[
				'attribute' => 'status',
				'enableSorting'=>false,
				'format' => 'raw',
				'value' => function($model){
					return $model->statusi;
				}
			],




				[
						'class' => 'fedemotta\datatables\EActionColumn',
						'header' => Yii::t('app', 'Actions'),
						'headerOptions' => ['width' => '140'],
						'template' => '{viewByuser} {updateByUser} {deleteByUser}',
						'buttons' => [

								'viewByuser' => function ($url, $model) {
									return Html::a(
											'<span class="glyphicon glyphicon-eye-open font-purple-plum"></span>',
											Url::to(['/request/viewby-user', 'id' => $model->id]),
											[
													'data-hint' => Yii::t('app', 'View'),
													'class' => 'hint--top hint--rounded hint--info'
												// 'style'=>'padding:1px;'
											]
									);
								},

								'updateByUser' => function ($url, $model) {
									return Html::a(
											'<span class="glyphicon glyphicon-pencil font-yellow"></span>',
											Url::to(['/request/updateby-user', 'id' => $model->id]),
											[
													'data-hint' => Yii::t('app', 'Update'),
													'class' => 'hint--top hint--rounded hint--info'
												// 'class' => 'showModalButton btn btn-success',
												// 'style'=>'padding:1px;'
											]
									);
								},
								'sendMessage' => function ($url, $model){
									return Html::a(
											'<i class="fa fa-envelope font-yellow"></i>',
											Url::to(['/messages/send', 'id' => $model->id]),
											[
													'class' => 'hint--top hint--rounded hint--info',
													'data-hint' => Yii::t('app', 'Send Message'),
											]
									);
								},
						],
				],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = [NULL,'semester','date','dateIn','dateOut','comment'];
		$searchableColumn = ['user.name','user.student_number'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}
	/**
	 *
	 * Block comment
	 *
	 */
	public function actionFile($id) {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('file', [
                        'model' => $model
            ]);
        }
    }
	

	/**
	 * Displays a single Request model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}
	public function actionViewbyUser($id)
	{
		$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		$model1=User::find()->andWhere(['id'=>$curentUserId])->one();
		if (is_null($model1)||!$curentUserId) {

			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));
		}
		$model = $this->findModel($id);

		if($model->personId!=$curentUserId)
		{
			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));

		}

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
					'model' => $model,
			]);
		}
		else{
			return $this->render('view_byuser', [
					'model' => $model,
			]);
		}
	}

	/**
	 * Creates a new Request model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		 
		$model = new Request();
		$model->scenario='create_mode';

		$transaction = false;
		
		if ($model->load ( Yii::$app->request->post () )) {
			$model->date = Yii::$app->jdate->date('Y/m/d');
			$image = UploadedFile::getInstances($model, 'image')[0];

			if($model->validate())
			{
				$name1=$image->name;
				//$ext = end((explode(".", $name1)));
				$ext='jpg';
				//die(var_dump($image));

				// generate a unique file name
				$avatar = Yii::$app->security->generateRandomString().".{$ext}";
				$model->file = $avatar;
				// the path to save file, you can set an uploadPath
				// in Yii::$app->params (as used in example below)
				$path = Yii::$app->basePath . '/web/uploads/' . $avatar;
				if(is_object($image))
				{

					if($model->save())
					{
						$image->saveAs($path);
						return $this->redirect(['view', 'id'=>$model->id]);
					}
				}else
				{
					$model->addError('image','فرم موافقت نامه نمی تواند خالی باشد');
				}

			}

			 
		}
		
		return $this->render ( 'create', [ 
				'model' => $model 
		] );
	}

	/**
	 * Creates a new Request model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionMake($id)
	{
		$model = new Request();
        $model->date =$model->dateDisplayFormat();
        //die(var_dump($model->date));
        $date=Mdate::find()->andWhere(['DateType'=>'ESKAN'])->all();
        $flag=0;
        foreach($date as $times)
        {
            if($model->date >= $times['FromDate'] && $model->date <= $times['ToDate'])
            {
                $flag+=1;
                $model->eduYear=$times['EduYear'];
                $model->semester=$times['semester'];
            }
        }
        if($flag>=1) {
            $model->scenario = 'create_mode';

            $user = User::find()->where(['id' => $id])->one();
            $transaction = false;

            if ($model->load(Yii::$app->request->post())) {
                //die(var_dump($model->eduYear));
                $model->personId = $id;
                $model->date = Yii::$app->jdate->date('Y/m/d');
                $image = UploadedFile::getInstances($model, 'image')[0];
                $res=Request::find()->andWhere(['personId'=>$id,'eduYear'=>$model->eduYear,'semester'=>$model->semester])->one();
                if(!$res){
                if ($model->validate()) {

                    // store the source file name

                    $name1 = $image->name;
                    //$ext = end((explode(".", $name1)));
                    $ext = 'jpg';
                    //die(var_dump($image));

                    // generate a unique file name
                    $avatar = Yii::$app->security->generateRandomString() . ".{$ext}";
                    $model->file = $avatar;
                    // the path to save file, you can set an uploadPath
                    // in Yii::$app->params (as used in example below)
                    $path = Yii::$app->basePath . '/web/uploads/' . $avatar;


                    if (is_object($image)) {

                        if ($model->save()) {
                            $image->saveAs($path);
                            return $this->redirect(['viewby-user', 'id' => $model->id]);
                        }
                    } else {
                        $model->addError('image', 'فرم موافقت نامه نمی تواند خالی باشد');
                    }


                }
                }
                else
                    Yii::$app->session->setFlash('error', "درخواست مورد نظر برای سال و ترم تحصیلی قبلا ثبت شده است!");


            }

            return $this->render('make', [
                'model' => $model,
                'user' => $user,
            ]);
        }
        else
            {
                Yii::$app->session->setFlash('error', "در بازه مجاز برای درخواست اسکان قرار نداریم!");
                return $this->redirect(['site/index']);
            }
	}

	/**
	 * Updates an existing Request model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->scenario='update_mode';

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdatebyUser($id)
	{
		$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
		if (!isset($curentUserId)) {
			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));
		}

		$model = $this->findModel($id);
		$model->scenario='update_mode';

		if($model->personId!=$curentUserId)
		{
			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));

		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['viewby-user', 'id' => $model->id]);
		} else {
			return $this->render('update_byuser', [
					'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Request model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$post = Yii::$app->request->post();
		
		$model = $this->findModel($id);
		$model->valid = 0;
		$_item =Yii::t('app','Request').' '.$model->personName;

		if($model->save(false))
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => true,
					'messages' =>[
						Yii::t('app', '{item} Successfully Deleted.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Deleted.',[
					'item' => $_item,
				]));
				$this->redirect(['index']);
			}
		}
		else
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => false,
					'messages' =>[
						Yii::t('app', 'Failed To Delete Items To {item}.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', 'Failed To Delete Items To {item}.',[
					'item' => $_item,
				]));

				$this->redirect(['index']);
			}
		}
	}

	/**
	 * Finds the Request model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Request the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Request::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
