<?php

namespace backend\controllers;


use Yii;
use backend\models\User;
use backend\models\UserSearch;
use backend\models\Userd;
use backend\models\UserdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \yii\helpers\Json;
use yii\helpers\Url;
use backend\models\ChangePassword;
use yii\web\UploadedFile;


/**
 * UserController implements the CRUD actions for User model.
 */
class MembersController extends Controller
{

	/* ID for Access control */
	public $currentEventId;
	public $currentUserDetailId;
	public $currentEventName;
	public $userId;

	public $_eventObj;
	public $_carvanObj;
	public $_teamObj;
	public $_universityObj;
	public $_authorizedFieldObj;
	public $_universityRespUserObj;
	public $_universityId;

   public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				//'only' => ['login', 'logout', 'signup','index'],
				'rules' => [
						[
								'allow' => true,
								'actions' => ['profileinfo','update-user','updateinfo','profileview','remove'],
								'roles' => ['admin','supervisor','student'],
						],
						[
								'allow' => true,
								'actions' => ['update-user','updateinfo','profileview'],
								'roles' => ['student'],
						],
				],
			],
		];
	}


	public function actionProfileinfo()
    {
        $message = '';

        /*-----View Info Code------*/
        $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $curentUserdModel = Userd::find()->andWhere(['user_id' => $curentUserId])->one();
        //die(var_dump($curentUserId1->id));
        if (!$curentUserId) {
            throw new \yii\web\HttpException(400, Yii::t('app', 'You are not authorized to per-form this action.you just can view your Information'));
        }

		 

		$userModel = $this->findModel($curentUserId);
        //$userModel1 = $this->findModel($curentUserId1);
        //die(var_dump($curentUserId1));


        return $this->render('manage_profile_info', array(
					'model' => $userModel ,
                    'modeld' => $curentUserdModel ,
				   // 'loginModel' => $loginModel,
						'message' => $message,
		));

	}



	public function actionProfileview($id) {
		$message='';
	    $model=$this->findModel($id);
		$model->scenario='UpdateProfileInfo';  
		
		if ($model->load(Yii::$app->request->post())&&(isset($_POST['update_prsonal_info_btn'])||isset($_POST['update_prsonal_info_btn_byadmin']))) 
		{ 

			if(isset($_POST['update_prsonal_info_btn_byadmin'])){
				$model->readonly = 1;
			}
			if($model->save(false)){
				 
				$message=$this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]) );
			}
			 
		}
		/**----------------------------------**/
		
		
	/**--------Update Picture -----------**/
	if ($model->load(Yii::$app->request->post())&&isset($_POST['update_pic_btn'])) 
	{
			$image = UploadedFile::getInstances($model, 'fileAttr')[0];
			$image1 = UploadedFile::getInstances($model, 'fileAttr1')[0];
			$image2 = UploadedFile::getInstances($model, 'fileAttr2')[0];
			$image3 = UploadedFile::getInstances($model, 'fileAttr3')[0];
			// store the source file name
            //$model->fileId = $image->name."pic".$model->id;
			$name=$image->name;
			$pieces = explode(".", $name);
			$name=$pieces[1];	
			$user=$model->username;
			
			$name1=$image1->name;
			$pieces = explode(".", $name1);
			$name1=$pieces[1];	
			
			$name2=$image2->name;
			$pieces = explode(".", $name2);
			$name2=$pieces[1];	
			
			$name3=$image3->name;
			$pieces = explode(".", $name3);
			$name3=$pieces[1];	
			
			
            //$ext = end((explode(".", $name1)));
			$ext='jpg';
			//die(var_dump($image));

            // generate a unique file name
            //$avatar = Yii::$app->security->generateRandomString().".{$ext}";

            // the path to save file, you can set an uploadPath
            // in Yii::$app->params (as used in example below)
            $path = Yii::$app->basePath . '/uploads/' . $user.'_image.'.'jpg';
			$path1 = Yii::$app->basePath . '/uploads/' . $user.'_id1.'.$name1;
			$path2 = Yii::$app->basePath . '/uploads/' . $user.'_id2.'.$name2;
			$path3 = Yii::$app->basePath . '/uploads/' . $user.'_naca.'.$name3;

            if($model->save(false))
            {
				if($model->validate(['fileAttr','fileAttr1','fileAttr2','fileAttr3'])&&$model->save(false))
				{
					if($image)$image->saveAs($path);
					if($image1)$image1->saveAs($path1);
					if($image2) $image2->saveAs($path2);
					if($image3)$image3->saveAs($path3);

					$message=$this->createMessageCss('success',Yii::t('app', 'تصویر با موفقیت بارگذاری شد.'));
					//$this->setFlashMsg('success',$message);
					//return $this->redirect(['members/profileview', 'id'=>$model->id,'#'=>'tab_1_2']);

				}

            } 
            else 
            {
				$message=$this->createMessageCss('success',Yii::t('app', 'اشکالی در بارگذاری فایل بوجود آمده است.'));
            }
	}
	/**-----------------------------------**/	   
	  
	/**-----------------------------changePassword ------------------------------------------------**/
	$changePasswordModel = new ChangePassword(['scenario'=>'change_password_byadmin']);
	$changePasswordModel->username = Yii::$app->user->identity->username;
	$changePasswordModel->passwordInput=  isset($_POST['ChangePassword']['passwordInput'])?$_POST['ChangePassword']['passwordInput']:'';
	$changePasswordModel->targetUser = (int) $id;

	if ($changePasswordModel->load(Yii::$app->request->post())&&isset($_POST['change_pass_btn'])) { 


		if ($changePasswordModel->load(Yii::$app->request->post()) ) {
			//die(var_dump($changePasswordModel->validate()));
			/*---check Password----*/
			if ($resultValidate=$changePasswordModel->validate() && $resultVerifyPass=$changePasswordModel->ValidateCurrentPassword())
			{
				if ($changePasswordModel->changePassword())
				{
					$message=$this->createMessageCss('success',Yii::t('app', 'کلمه عبور با موفقیت تغییر کرد.'));
				}
				else{
					$message=$this->createMessageCss('error',Yii::t('app', 'اشکالی در فرآیند تغییر کلمه عبور بوجود آمده است.'));
				}
				//$this->setFlashMsg('success',$message);
				//return $this->redirect(['members/profileview', 'id'=>$model->id,'#'=>'tab_1_2']);

			}
			elseif (!$resultValidate || !$resultVerifyPass)
			{
					$message=$this->createMessageCss('error',Yii::t('app', 'Error in changed password!'));
			}
		}

	}
	/**-----------------------------------------------------------------------------------------------**/  


        $curentUserdModel = Userd::find()->andWhere(['user_id' => $id])->one();

		return $this->render('update_user', [
					'model' =>$model,
					'massage' => $message,
                    'model1' => $curentUserdModel ,
					//'userDetails'=>$userDetails,
					'changePasswordModel'=>$changePasswordModel

		]);
	}



	public function actionRemove()
    {
		$admin 				= Yii::$app->user->can('admin');
		$supervisor			= Yii::$app->user->can('supervisor');
		$student 			= Yii::$app->user->can('student');

		$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

		$userId=(int)$_GET['user_id'];
		$model = User::find()->andWhere(['id' => $userId])->one();

		if($student)
		{
			if($userId!=$curentUserId)
			{
				throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));

			}

		}

		$type=$_GET['data'];

		if(!in_array($type,['_image','_id1','_id2','_naca']))
		{
			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));

		}

		$filename = '../uploads/'. $model->username.$type.'.jpg';

        unlink($filename);

		return $this->redirect(['members/profileview', 'id'=>$userId]);

	}
	public function actionUpdateUser($userId) 
	{
		$message='';
		
		$curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $model1=Userd::find()->andWhere(['user_id'=>$curentUserId])->one();
        if (!$curentUserId) {
			throw new \yii\web\HttpException(400, Yii::t('You are not authorized to per-form this action.you just can view your Information'));
		}
		$userId= $curentUserId;

 
		$model=$this->findModel($userId);
		$model->scenario='UpdateProfileInfo';  
		$model1->scenario='UpdateProfileInfo1';

        //die(var_dump($model));
		
		/**-------Update Personal info-------**/
		if ($model->load(Yii::$app->request->post())&& $model1->load(Yii::$app->request->post())&& isset($_POST['update_prsonal_info_btn']) ) {

			if(!$model->readOnly)
			{
				if( $model->save())	{
					$model1->save();

				}

			}else
			{
				$model->addError('name','You cant edit information after admin confirmation');
			}

				$message=$this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]) );
		}
		/**----------------------------------**/


	/**--------Update Picture -----------**/
	if ($model->load(Yii::$app->request->post())&&isset($_POST['update_pic_btn']))
	{
            $image= UploadedFile::getInstance($model, 'fileAttr');
            $image1= UploadedFile::getInstance($model, 'fileAttr1');
            $image2 = UploadedFile::getInstance($model, 'fileAttr2');
            $image3 = UploadedFile::getInstance($model, 'fileAttr3');

			$user=$model->username;
            $path = Yii::$app->basePath . '/uploads/' . $user.'_image.'.'jpg';
			$path1 = Yii::$app->basePath . '/uploads/' . $user.'_id1.'.'jpg';
			$path2 = Yii::$app->basePath . '/uploads/' . $user.'_id2.'.'jpg';
			$path3 = Yii::$app->basePath . '/uploads/' . $user.'_naca.'.'jpg';

            if($model->validate(['fileAttr','fileAttr1','fileAttr2','fileAttr3'])&&$model->save(false))
            {
                if($image)$image->saveAs($path);
                if($image1)$image1->saveAs($path1);
                if($image2) $image2->saveAs($path2);
                if($image3)$image3->saveAs($path3);
            }
	}
	/**-----------------------------------**/	   
	  
	/**-----------------------------changePassword ------------------------------------------------**/
	$changePasswordModel = new ChangePassword(['scenario'=>'change_password_byadmin']);
	$changePasswordModel->username = Yii::$app->user->identity->username;
	$changePasswordModel->passwordInput=  isset($_POST['ChangePassword']['passwordInput'])?$_POST['ChangePassword']['passwordInput']:'';
	$changePasswordModel->targetUser = (int) $userId;

	if ($changePasswordModel->load(Yii::$app->request->post())&&isset($_POST['change_pass_btn'])) { 


		if ($changePasswordModel->load(Yii::$app->request->post()) ) {
			//die(var_dump($changePasswordModel->validate()));
			/*---check Password----*/
			if ($resultValidate=$changePasswordModel->validate() && $resultVerifyPass=$changePasswordModel->ValidateCurrentPassword())
			{
				if ($changePasswordModel->changePassword())
				{
					$message=$this->createMessageCss('success',Yii::t('app', 'Changed Password Successfully .'));

				}
				else{
					$message=$this->createMessageCss('error',Yii::t('app', 'Error in changed password!'));
				}
			}
			elseif (!$resultValidate || !$resultVerifyPass)
			{
					$message=$this->createMessageCss('error',Yii::t('app', 'Error in changed password!'));
			}
		}

	}
	/**-----------------------------------------------------------------------------------------------**/  




/*
 * if user didn't select event userdetail can not be update
 */
	/**-------Update User Detail Personal info-------**/
		if(!empty($userDetails)) {
			$userDetails->scenario = 'UpdateUserDetailse';
			if ($userDetails->load(Yii::$app->request->post()) && isset($_POST['update_user_detail_info_btn']) && $userDetails->save()) {

				try {
					$uploadStuCartResult = $userDetails->uploadFileStuCart();
				} catch (Exception $e) {
					throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن کارت دانشجویی پیش آمده است.');
				}

				try {
					$uploadStuAssuranceCartResult = $userDetails->uploadFileAssuranceCart();
				} catch (Exception $e) {
					throw new \yii\web\HttpException(500, 'مشکل در افزودن فایل اسکن  کارت یمه پیش آمده است.');
				}

				$message = $this->createMessageCss('success', Yii::t('app', '{item} Successfully Edited.', ['item' => Yii::t('app', 'User')]));


			}
		}
	/**--------------------------------------------**/

		

		return $this->render('update_user', [
					'model' =>$model,
					'model1' =>$model1,
           // 'model1'
					'massage' => $message,
					//'userDetails'=>$userDetails,
					'changePasswordModel'=>$changePasswordModel

		]);
	}
   









	

			public function createMessageCss($msgType,$string)
   {
 
	   switch ($msgType) {
		   case 'success':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
					break;

		   case 'danger':
				 $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
					break;		 
		   default:
				 $message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';  
			   break;
	   }

	   return $message;
	   
   }


   public  function setFlashMsg($name,$message)
   {
				$session = Yii::$app->session;
				$session->setFlash($name, $message);
   }
 

 

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		 
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
