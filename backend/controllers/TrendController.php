<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\Json;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\data\Sort;

use backend\models\Trend;
use backend\models\TrendSearch;

/**
 * TrendController implements the CRUD actions for Trend model.
 */
class TrendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create','update','delete'],
                        'roles' => ['admin','supervisor'],
                    ],
                     
                ],
            ],
        ];
	}

	/**
	 * Lists all Trend models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$searchModel = new TrendSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$columns = [
			['class' => 'yii\grid\SerialColumn'],

			 
			[
				'attribute' => 'fieldId',
				'enableSorting'=>false,
				'value'=>function($model){
					return $model->fieldName;

				}
			],
			[
				'attribute' => 'name',
				'enableSorting'=>false
			],
			[
				'attribute' => 'ename',
				'enableSorting'=>false
			],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '100'],
			],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = ['id','fieldId','name','ename'];
		$searchableColumn = ['id','fieldId','name','ename'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}

	/**
	 * Displays a single Trend model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Creates a new Trend model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Trend();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Trend model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Trend model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$post = Yii::$app->request->post();
		
		$model = $this->findModel($id);
		$model->valid = 0;
		$_item = $model->name;

		if($model->save(false))
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => true,
					'messages' =>[
						Yii::t('app', '{item} Successfully Deleted.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Deleted.',[
					'item' => $_item,
				]));
				$this->redirect(['index']);
			}
		}
		else
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => false,
					'messages' =>[
						Yii::t('app', 'Failed To Delete Items To {item}.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', 'Failed To Delete Items To {item}.',[
					'item' => $_item,
				]));

				$this->redirect(['index']);
			}
		}
	}

	/**
	 * Finds the Trend model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Trend the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Trend::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
