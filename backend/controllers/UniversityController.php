<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\data\Pagination;
use yii\data\Sort;

use backend\models\University;
use backend\models\UniversitySearch;
use backend\models\Province;

/**
 * UniversityController implements the CRUD actions for University model.
 */
class UniversityController extends Controller
{
	public $layout = '@app/views/layouts/column2';
   
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index','create','update','editable','delete','view'],
						'allow' => true,
						// 'matchCallback' => function($rule, $action) {
						// 	return !Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin === true;
						// }
					],
					[
						'actions' => ['index'],
						'roles' => ['adminUniversity'],
						'allow' => true,
					],
					[
						'actions' => ['view'],
						'roles' => ['viewUniversity'],
						'allow' => true,
					],
					[
						'actions' => ['create'],
						'roles' => ['createUniversity'],
						'allow' => true,
					],
					[
						'actions' => ['update', 'editable'],
						'roles' => ['updateUniversity'],
						'allow' => true,
					],
					[
						'actions' => ['delete'],
						'roles' => ['deleteUniversity'],
						'allow' => true,
					]
				]
			]
		];
	}

	public function actions()
	{
		return [
			'editable' => [
				'class' => 'faravaghi\xeditable\XEditableAction',
				'scenario'=>'editable',  //optional
				'modelclass' => University::className(),
			],
		];
	}

	/**
	 * Lists all University models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		 

		$request = Yii::$app->request;
		$searchModel = new UniversitySearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		 
		$columns = [
			[ 
				'class' => 'yii\grid\SerialColumn' 
			],
			[
				'attribute' => 'name',
				'enableSorting'=>false,
				'format'=>'html',
				'value' => function($model) {
					return Html::a(Html::encode($model->name), Url::to(Html::encode($model->url)));;
				},
			],
			[ 
				 
				'attribute' => 'cityId',
				'enableSorting'=>false,
				'format' => 'raw',
				'value' => function($model) {
					return(is_object($model->city)) ? Html::encode($model->city->name) : '-----';
				},
				 
				 
				 
			],
			[ 
				'attribute' => 'address',
				'enableSorting'=>false,
				'format' => 'raw',
				 
				 
			],
			[
				 
				'attribute' => 'phone',
				'enableSorting'=>false,
				'format' => 'raw',
				 
				 
			],
			[ 
				'attribute' => 'fax',
				'enableSorting'=>false,
			],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '80'],
				'template' => '{view} {update} {delete}',
			],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			// 'serverSide'=> true,
			// 'fnDrawCallback' => new \yii\web\JsExpression('function() {$(".editable").editable()}'),
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = array(NULL, 'name','cityId','address','phone', 'fax');
		$searchableColumn = array('name','address','phone', 'fax');

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}
	
	public function actionView($id)
	{
		$model = $this->findModel($id);

		if(Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}

	public function actionCreate()
	{
		$model = new University();
		
		if($model->load(Yii::$app->request->post()))
		{
			if($model->save())
			{
				foreach($model->attributes as $key => $value){
					unset($model->$key);
				}
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Added.', ['item'=>'University']));

				return $this->redirect(['index']);
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		$post = Yii::$app->request->post();
		
		$model = $this->findModel($id);
		$model->valid = 0;
		if($model->save(false))
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => true,
					'messages' => [
						Yii::t('app', '{item} Successfully Deleted.', [
							'item' => $model->name
						])
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Deleted.', [
					'item' => Yii::t('app', $model->name)
				]));
				$this->redirect(['index']);
			}
		}
		else
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => false,
					'messages' => [
						Yii::t('app', 'Failed To Delete Items To {item}.', [
							'item' => $model->name
					])]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', 'Failed To Delete Items To {item}.', [
					'item' => Yii::t('app', $model->name)
				]));

				$this->redirect(['index']);
			}
		}
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if($model->load(Yii::$app->request->post())) {
			//$model->Alias = $model->catAlias;

			if($model->save()){
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Edited.', ['item'=>Yii::t('app','University')]));
				return $this->redirect(['index']);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}


	/**
	 * Finds the University model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return University the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if(($model = University::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
