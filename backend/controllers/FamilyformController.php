<?php

namespace backend\controllers;

use backend\models\Mdate;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\Json;

use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\data\Sort;

use app\models\Familyform;
use app\models\FamilyformSearch;

/**
 * FamilyformController implements the CRUD actions for Familyform model.
 */
class FamilyformController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [

		];
	}

	/**
	 * Lists all Familyform models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$request = Yii::$app->request;
//		$searchModel = new FamilyformSearch();
//		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $query = Familyform::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andFilterWhere([
            'AttachStatus'=>'FINAL',
            //'exit_date'=>'0000-00-00',
        ]);

        $searchModel = new \app\models\EskanSearch();

		$columns = [
			['class' => 'yii\grid\SerialColumn'],

//			[
//				'attribute' => 'id',
//				'enableSorting'=>false
//			],
			[
				'header' => Yii::t('app','دانشجو'),
				'enableSorting'=>false,
                'value' =>  function($model){
		    return $model->user->name.' '.$model->user->last_name;
},
    		],
			[
				'attribute' => 'FName',
				'enableSorting'=>false
			],
			[
				'attribute' => 'LName',
				'enableSorting'=>false
			],
			[
				'attribute' => 'relation',
				'enableSorting'=>false
			],
			 [
				'attribute' => 'RelationType',
                 'value' =>  function($model) {
                    if($model->RelationType==1)
                        return 'بستگانی که دانشجو میتواند به خانه آنها برود';
                    else
                        return 'بستگانی که میتوانند به ملاقات دانشجو بیایند';
                 }			 ],
			[
				'attribute' => 'AttachStatus',
                'value' =>  function($model) {
                    return $model->getstate($model->AttachStatus);
                }
			 ],

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '100'],
                'template' => '{updateeskan}{view}',
                'buttons' => [

                    'updateeskan' => function ($url, $model){
                        $_send = Html::a(
                            '<i class="fa fa-pencil font-blue"></i>',
                            \yii\helpers\Url::to(['/familyform/adminupdate', 'id' => $model->id]),
                            [
                                'class' => 'hint--top hint--rounded hint--info',
                                'data-hint' => Yii::t('app', 'بروزرسانی رکورد'),
                            ]
                        );

                        return $_send;
                    },
                    ]

            ],
		];

		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

		$sortableColumn = ['id','user_id','FName','LName','relation','job','phone','address','RelationType','mobile','AttachStatus'];
		$searchableColumn = ['id','user_id','FName','LName','relation','job','phone','address','RelationType','mobile','AttachStatus'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
	}

	/**
	 * Displays a single Familyform model.
	 * @param string $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Creates a new Familyform model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
        $model = new Familyform();
        $model->date =$model->dateDisplayFormat();
//        echo $model->date;
//        echo '<br>';
        //die;
	    $date=Mdate::find()->andWhere(['DateType'=>'FAMILY_FORM'])->all();
	    $flag=0;
	    foreach($date as $times)
        {
            if($model->date >= $times['FromDate'] && $model->date <= $times['ToDate'])
            $flag+=1;
        }
        //die;
        //die(var_dump($date));
       // $get = $_GET(['request']);

//die(var_dump($get) );
        if($flag>=1)
        {
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //die(var_dump($model));
			return $this->redirect(['create']);
		} else {
			return $this->render('create', [
				'model' => $model,

			]);
		}
        }
        else
        {
            Yii::$app->session->setFlash('error', "در بازه مجاز برای ثبت فرم خانواده قرار نداریم!");
            return $this->redirect(['site/index']);
        }
	}

    public function actionAdmincreate()
    {
        $model = new Familyform();
        // $get = $_GET(['request']);

//die(var_dump($get));
        $user_id = '';
        if(isset($_GET['user_id']))
          $user_id = $_GET['user_id'];



            if ($model->load(Yii::$app->request->post())) {
//                $_MODEL=Yii::$app->request->post('Familyform');
//                die(var_dump($_MODEL));
                $model->user_id=$user_id;
                $model->save();
                //die(var_dump($model));
                return $this->redirect(['admincreate','user_id'=>$user_id]);
            }

        return $this->render('admincreate', [
               'model' => $model,
               'user_id' => $user_id

        ]);
    }

	/**
	 * Updates an existing Familyform model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param string $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
		    if($model->AttachStatus==NULL)
            {
            $model->save();
			return $this->redirect(['create']);
            }
            else{
		        Yii::$app->session->setFlash('error', "امکان بروزرسانی رکورد وجود ندارد، چون از حالت ثبت اولیه خارج شده است!");
                return $this->redirect(['familyform/create']);
            }
        } else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}
    public function actionAdminupdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->save(false);
                return $this->redirect(['admincreate']);
            }
         else {
            return $this->render('admincreate', [
                'model' => $model,
            ]);
        }
    }
	public function actionAccept()
    {
        $curentUserId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $models=Familyform::find()->andWhere(['user_id'=>$curentUserId])->all();
        //die(var_dump($models));
        foreach ($models as $model) {
            $model->AttachStatus='FINAL';
            $model->save();
        }
        return $this->redirect(['create']);

    }
    public function actionConfirm($id)
    {


        $model = $this->findModel($id);


        if ($model==!null) {
           // die(var_dump($model));

            $model->AttachStatus='VALID';
            $model->save(false);
            return $this->redirect(['admincreate']);
        }
        else {
            die ('11');
        }

    }
    public function actionReject($id)
    {


        $model = $this->findModel($id);


        if ($model==!null) {
            // die(var_dump($model));

            $model->AttachStatus='INVALID';
            $model->save(false);
            return $this->redirect(['admincreate']);
        }
        else {
            die ('11');
        }

    }


    /**
	 * Deletes an existing Familyform model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param string $id
	 * @return mixed
	 */

        public function actionDelete($id)
    {
        $model=$this->findModel($id);
        //die(var_dump($model));
        //$roomid=$model->room_id;
        if($model->AttachStatus==NULL) {
            $model->delete();
            return $this->redirect(['familyform/create','data'=>'OK']);
        }
        else
        {
            Yii::$app->session->setFlash('error', "امکان حذف رکورد وجود ندارد، چون از حالت ثبت اولیه خارج شده است!");
            return $this->redirect(['familyform/create']);
        }


    }


	/**
	 * Finds the Familyform model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return Familyform the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Familyform::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
    public function createMessageCss($msgType,$string)
    {

        switch ($msgType) {
            case 'success':
                $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
                break;

            case 'danger':
                $message = '<div class="alert alert-'.$msgType.'" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
                break;
            default:
                $message = '<div class="alert alert-danger" role="alert" ><a href="#" class="alert-link">' . $string . '</a></div>';
                break;
        }

        return $message;

    }


    public  function setFlashMsg($name,$message)
    {
        $session = Yii::$app->session;
        $session->setFlash($name, $message);
    }
}
