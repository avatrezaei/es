<?php

namespace app\models;

use backend\models\User;
use app\models\Trree;
use app\models\TrreeSearch;
use Yii;
use backend\models\jdf;

/**
 * This is the model class for table "eskan".
 *
 * @property integer $id
 * @property integer $room_id
 * @property integer $PersonID
 * @property integer $year
 * @property integer $term
 * @property integer $delivery
 * @property string $entry_date
 * @property string $exit_date
 */
class Eskan extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eskan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'PersonID','year','term','delivery'], 'integer'],
            [['entry_date', 'exit_date'], 'string', 'max'=>52],
            [['PersonID','room_id','year','term','delivery'], 'required'],

        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'PersonID']);
    }
    public function getState()
    {
        if($this->delivery==1)
            return 'تحویل داده شده';
        else
            return 'تحویل نشده';
    }
    public function getAddress($date)
    {

        $mm=Trree::find()->andWhere(['id'=>$date])->one();
        $i=1;
        while ($mm->parent_id!=0)
        {
            $result[$i]= $mm->name;
            //echo $result[$i];
            $mm=Trree::find()->andWhere(['id'=>$mm->parent_id])->one();
            $i=$i+1;
        }
        //die(var_dump($i));
        $a='ریشه-';
        $f=0;
        for($j=$i-1;$j>=1;$j--)
        {
            $f=1;
            $a.=$result[$j];
            if($j!=1)
                $a.= '-';
        }
        if ($f==0)
            $a='ریشه';
        return $a;
    }

    /**
     * @inheritdoc
     */
    public function dateDisplayFormat($date=null)
    {
       $date=$this->entry_date;

        if ( preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date )) {

            return $date ;
        }
        else if(preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',$date))
        {

            return $date;
        }
        else if($date=='0000-00-00 00:00:00'|| $date=='0000-00-00') {
            return '0000-00-00';
        }


        return $date=$this->getJalalyDate('entry_date');
    }
    public function dateDisplayFormat1($date=null)
    {
        $date=$this->exit_date;

        if ( preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date )) {

            return $date ;
        }
        else if(preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',$date))
        {

            return $date;
        }
        else if($date=='0000-00-00 00:00:00'|| $date=='0000-00-00') {
            return '0000-00-00';
        }


        return $date=$this->getJalalyDate('exit_date');
    }
    public function getJalalyDate($date) {
        $jdf = new jdf();
        $jDateString = $jdf->jdate('Y-m-d', strtotime($this->$date));
        return $jDateString;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'شناسه اتاق',
            'PersonID' => 'دانشجو',
            'year'=>'سال',
            'term'=>'ترم',
            'entry_date'=>'تاریخ ورود',
            'exit_date'=>'تاریخ خروج',
            'delivery'=>'وضعیت تحویل اتاق',
        ];
    }
}
