<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\University;

/**
 * UniversitySearch represents the model behind the search form about `app\models\University`.
 */
class UniversitySearch extends University
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'universityType', 'cityId', 'provinceId'], 'integer'],
            [['name', 'eName', 'preffixName', 'address', 'phone', 'fax', 'email', 'url'], 'safe'],
            [                ['phone'],
                                                 'match',
                                                 'pattern' =>'/^([0-9]|[-])*$/', 
                                                 'message' => 'تنها از کاراکترهای عددی و -استفاده شود. '
                                ],
                    		[ 
						['name' ],
						'string',
						'max' => 50,
				], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = University::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
        'pageSize' =>  2,
    ],        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'universityType' => $this->universityType,
            'cityId' => $this->cityId,
            'provinceId' => $this->provinceId,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'eName', $this->eName])
            ->andFilterWhere(['like', 'preffixName', $this->preffixName])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
