<?php

namespace backend\models;

use Yii;
use backend\models\Person;
use backend\models\User;


/**
 * This is the model class for table "memberpayment".
 *
 * @property integer $id
 * @property integer $personId
 * @property string  $paymentNo
 * @property string  $paymentType
 * @property integer $paymentTime
 * @property string  $paymentDate
 * @property string  $amount
 * @property integer $payFor
 * @property string  $AccountNo
 * @property integer $comment
 * @property integer $eduYear
 * @property integer $semester
 * @property integer $valid
 */
class Memberpayment extends \yii\db\ActiveRecord
{
	/**
	 * @return Sum all bill payment
	 */
	public $sumPayment = 0;

	public $fishFileAttr;
	public $fileModel;


	/*
	* all payment type 
	*/
	const PAYMENT_TYPE_FISH   	= 0;
	const PAYMENT_TYPE_CASH		= 1;
	const PAYMENT_TYPE_EPAY	 	= 2;

	/*
	* all payment pay for 
	*/
	const PAYMENT_PAYFOR_PRE   	= 0;
	const PAYMENT_PAYFOR_RENT	= 1;


	/**
	* itemAlias
	* @param integer
	*/   
	public static function itemAlias($type, $code = NULL)
	{
		$_items = [
			'For' => [
				self::PAYMENT_PAYFOR_RENT   => Yii::t('app', 'Payment for rent'),
				self::PAYMENT_PAYFOR_PRE	=> Yii::t('app', 'Payment for pre'),		   
			],

			'Type' => [
				self::PAYMENT_TYPE_FISH 	=> Yii::t('app', 'payment type fish'),
				self::PAYMENT_TYPE_CASH		=> Yii::t('app', 'payment type cash'),
				self::PAYMENT_TYPE_EPAY  	=> Yii::t('app', 'payment type epay'),
			],
			'PaymentCode' => [
				'FISH' => Yii::t('app', 'payment type fish'),
				'CASH' => Yii::t('app', 'payment type cash'),
				'EPAY' => Yii::t('app', 'payment type epay'),
			],
			'PaymentNumber' => [
				'FISH' => self::PAYMENT_TYPE_FISH,
				'CASH' => self::PAYMENT_TYPE_CASH,
				'EPAY' => self::PAYMENT_TYPE_EPAY,
			]	 
			 
		];
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	} 

	public function getPaymentTypeText()
	{
		return self::itemAlias('type', $this->paymentType);
	}

	public function getPaymentForText()
	{
		return self::itemAlias('for', $this->paymentFor);
	}


	public function getType()
	{
		$class = '';

		switch($this->paymentType) {
			case self::PAYMENT_TYPE_FISH :
				$class = 'success';
				break;

			case self::PAYMENT_TYPE_CASH :
				$class = 'info';
				break;

			case self::PAYMENT_TYPE_EPAY :
				$class = 'warning';
				break;
		}
		
		return '<span class="label label-' . $class . '">' . self::itemAlias('Type', $this->paymentType) . '</span>';
	}


	public function getTypeCode()
	{
		$_type = '';

		switch($this->paymentType) {
			case self::PAYMENT_TYPE_FISH :
				$_type = 'FISH';
				break;

			case self::PAYMENT_TYPE_CASH :
				$_type = 'CASH';
				break;

			case self::PAYMENT_TYPE_EPAY :
				$_type = 'EPAY';
				break;
		}
		
		return $_type;
	}

	public function getPayfori()
	{
		$class = '';

		switch($this->payFor) {
			case self::PAYMENT_PAYFOR_PRE :
				$class = 'success';
				break;

			case self::PAYMENT_PAYFOR_RENT :
				$class = 'info';
				break;

			 
		}
		
		return '<span class="label label-' . $class . '">' . self::itemAlias('For', $this->payFor) . '</span>';
	}


	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%memberpayment}}';
	}

	/**
	 * @inheritdoc
	 *
	 * @return ProvinceQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new MemberPaymentQuery(get_called_class());
	}

	/*
	 * @inheritdoc
	 */
	public function rules()
	{
		//$fileModel=new File;

		return [
			[['personId','paymentNo','paymentType','paymentTime','paymentDate','amount'], 'required'],
				[['personId', 'paymentNo', 'amount','accountNo'], 'integer'],
				[['personId'], 'checkReapet'],
			//[['paidDate'], 'checkDateFormat'],
			//[['paidDate', 'insertDate', 'lastUpdateDate','statusChangeDate'], 'safe'],
			 
			 
		];
	}


	public function checkReapet($attribute, $params) {

		$personId=(int)$this->personId;
		$payFor=(int)$this->payFor;
		$eduYear=(int)$this->eduYear;
		$semester=(int)$this->semester;
		$memObj=new Memberpayment();
		$memres=$memObj->find()->where(['personId'=>$personId,'payFor'=>$payFor,'eduYear'=>$eduYear,'semester'=>$semester])->one();
		if($memres!=null)
		{
			$this->addError('personId', Yii::t('app', 'Reapeted data for Fish Payment'));
			return false;
		}

		return true;
	}


	public function getAccountNo()
	{
		return $this->AccountNo;
	}

	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'personId']);
	}


 

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'personId' => Yii::t('app', 'Person ID'),
			'paymentNo' => Yii::t('app', 'Fish No'),
			'PayerName' => Yii::t('app', 'Payer Name'),
			'paymentDate' => Yii::t('app', 'Paid Date'),
			'paymentTime' => Yii::t('app', 'Paid Time'),
			'paymentType' => Yii::t('app', 'Paid Type'),
			'payFori'	  => Yii::t('app','Payment For'),
			'amount' => Yii::t('app', 'Paid Money'),
			'valid' => Yii::t('app', 'Valid'),
			'eduYear' => Yii::t('app', 'eduction year'),
			'semester' => Yii::t('app', 'semster'),
			'personId'=>Yii::t('app', 'Person Name'),
			'accountNo'=>Yii::t('app', 'Account No'),
			'username'=>Yii::t('app','Username'),

			//
			'memberName'		=>Yii::t('app', 'Person Name'),
			'universityName'	=>Yii::t('app', 'University Name'),
		];
	}
	 


	public function getUserName()
	{
		if($this->user)
			return $this->user->Fullname;
		return "as";
	}

	public function getUniversityName()
	{
		if($this->user)
			return $this->user->Universityname;
		return "as";
	}

	 
	public function getAmount()
	{
		return Rezvan::toCurrency($this->amount);
	} 

}



/**
 * This is the ActiveQuery class for [[CaravanPayment]].
 *
 * @see CaravanPayment
 */
class MemberPaymentQuery extends \yii\db\ActiveQuery
{
	public function init()
	{
		parent::init();
		return $this->andWhere([MemberPayment::tableName() . '.valid' => 1]);
	}

	 

	public function sumAmount()
	{
		return $this->addSelect(['SUM([[amount]]) AS sumPayment']);
	}

	public function user($id)
	{
		return $this->andWhere(['[[personId]]' => (int)$id]);
	}

	/**
	 * @inheritdoc
	 *
	 * @return CaravanPayment[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}
}
