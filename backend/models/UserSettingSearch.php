<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\UserSetting;

/**
 * UserSettingSearch represents the model behind the search form about `backend\models\UserSetting`.
 */
class UserSettingSearch extends UserSetting
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'format_type', 'access_type', 'int_value'], 'integer'],
			[['key', 'char_value', 'json_value', 'create_date_time', 'modified_date_time'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = UserSetting::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'user_id' => $this->user_id,
			'format_type' => $this->format_type,
			'access_type' => $this->access_type,
			'int_value' => $this->int_value,
			'create_date_time' => $this->create_date_time,
			'modified_date_time' => $this->modified_date_time,
		]);

		$query->andFilterWhere(['like', 'key', $this->key])
			->andFilterWhere(['like', 'char_value', $this->char_value])
			->andFilterWhere(['like', 'json_value', $this->json_value]);

		return $dataProvider;
	}
}
