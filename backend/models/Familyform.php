<?php

namespace app\models;

use backend\models\User;
use Yii;
use backend\models\jdf;

/**
 * This is the model class for table "familyform".
 *
 * @property string $id
 * @property string $user_id
 * @property string $FName
 * @property string $LName
 * @property string $relation
 * @property string $job
 * @property string $phone
 * @property string $address
 * @property integer $RelationType
 * @property string $mobile
 * @property string $AttachStatus
 */
class Familyform extends \yii\db\ActiveRecord
{

    public $date;
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'familyform';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'RelationType'], 'integer'],
			[['mobile','RelationType','FName','LName','relation', 'job','phone','address' ], 'required'],
			[['AttachStatus'], 'string'],
			[['FName', 'phone'], 'string', 'max' => 20],
			[['LName', 'relation', 'job'], 'string', 'max' => 30],
			[['address'], 'string', 'max' => 255],
			['mobile', 'mobilenumber'],
            ['phone', 'phonenumber'],
		];
	}
    public function mobilenumber($attribute)
    {
        if (!preg_match('/^[0][0-9]{10}$/', $this->$attribute)) {
            $this->addError($attribute, 'شماره موبایل را با فرمت صحیح وارد نموده و رقم صفر اول را لحاظ نمائید.');
        }
    }
    public function phonenumber($attribute)
    {
        if (!preg_match('/^[0][0-9]{10}$/', $this->$attribute)) {
            $this->addError($attribute, 'شماره تلفن را با فرمت صحیح وارد نموده و رقم صفر اول را لحاظ نمائید.');
        }
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getUsers()
    {
         return $this->FName . ' ' . $this->LName . '-'.$this->address;
    }
    public function getState($data)
    {
        if($data==NULL)
            return '-';
        else if($data=='FINAL')
            return 'نهایی شده';
        else if($data=='VALID')
            return 'تایید';
        else
            return 'رد';
    }
    public function dateDisplayFormat($date=null)
    {
        //$date=$this->from_date;

        if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if ($date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '0000-00-00';
        }
        return $date = $this->getJalalyDate($date);
        //return $date;
    }
    public function getJalalyDate($date) {
        $jdf = new jdf();
        $jDateString = $jdf->jdate('Y-m-d', strtotime($date));
        return $jDateString;
    }
    public function getRel($data)
    {
        if($data==1)
            return 'بستگانی که دانشجو میتواند به خانه آنها برود';
        else if($data==2)
            return 'بستگانی که میتوانند به ملاقات دانشجو بیایند';

    }


        /**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Students'),
			'FName' => Yii::t('app', 'Name'),
			'LName' => Yii::t('app', 'Last Name'),
			'relation' => Yii::t('app', 'Relation'),
			'job' => Yii::t('app', 'Job'),
			'phone' => Yii::t('app', 'Phone'),
			'address' => Yii::t('app', 'Address'),
			'RelationType' => Yii::t('app', 'Relation Type'),
			'mobile' => Yii::t('app', 'Mobile'),
			'AttachStatus' => Yii::t('app', 'Attach Status'),
		];
	}
}
