<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "userd".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $startingyear
 * @property integer $startingsemester
 * @property string $major
 * @property string $fatherName
 * @property integer $units
 * @property integer $city_location_id
 * @property integer $birthCityid
 * @property integer $shift
 * @property string $jobincome
 */
class Userd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userd';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();



        //Scenario Values Only Accepted
        $scenarios['UpdateUser'] = ['fatherName','startingyear', 'startingsemester', 'units', 'city_location_id', 'birthCityid', 'shift', 'major','jobincome'];//Scenario Values Only Accepted

        $scenarios['UpdateProfileInfo1'] = ['fatherName','startingyear', 'startingsemester', 'units', 'city_location_id', 'birthCityid', 'shift', 'major','jobincome'];

        return $scenarios;
    }
    public function rules()
    {
        return [
            [['user_id', 'startingyear', 'startingsemester', 'units', 'city_location_id', 'birthCityid', 'shift'], 'integer'],
            [['major', 'fatherName'], 'string', 'max' => 50],
            [['jobincome'], 'string', 'max' => 52],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'startingyear'=>Yii::t('app', 'سال ورود'),
            'startingsemester'=>Yii::t('app', 'نیمسال ورود'),
            'units'=>Yii::t('app', 'تعداد واحد گذرانده شده'),
            'major'=>Yii::t('app', 'گرایش'),
            'fatherName' => Yii::t('app','Father Name'),
            'city_location_id'=>Yii::t('app', 'شهر محل سکونت'),
            'birthCityid' => Yii::t('app','BirthCity'),
            'shift'=>Yii::t('app', 'دوره'),
            'jobincome'=>Yii::t('app', 'Job in Come'),
        ];
    }
}
