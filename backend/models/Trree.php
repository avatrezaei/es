<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trree".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $type
 * @property integer $section
 * @property string $gender
 * @property integer $enable
 * @property integer $FromNo
   * @property integer $ToNo
   * @property integer $capacity
 */
class Trree extends \yii\db\ActiveRecord
{
    public $FromNo;
    public $ToNo;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'parent_id', 'type','capacity','section','gender'], 'required'],
            [['parent_id', 'type','enable','section','FromNo','ToNo','capacity'], 'integer'],
            [['name'], 'string', 'max' => 52],
            [['gender'], 'string', 'max' => 52],
        ];
    }

    /**
     * @inheritdoc
     */
    public function disable($date)
    {
       // $model=new T
        $changemodel=Trree::find()->andWhere(['id'=>$date])->one();
        $changemodel->enable=0;
        //die(var_dump( $changemodel));
        $changemodel->save(false);
        $childs=Trree::find()->andWhere(['parent_id'=>$changemodel->id])->all();
        foreach ($childs as $id)
        {
            $this->disable($id['id']);
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'شناسه',
            'name' => 'نام',
            'parent_id' => 'شناسه والد',
            'type' => 'نوع',
            'section'=>'مقطع تحصیلی',
            'gender'=>'جنسیت',
            'status'=>'وضعیت',
            'icon'=>'آیکون',
            'enable'=>'فعال',
            'FromNo'=>'از شماره',
            'ToNo'=>'تا شماره',
            'capacity'=>'ظرفیت',
        ];
    }
}
