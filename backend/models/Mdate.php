<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "managedates".
 *
 * @property string $RecID
 * @property string $DateType
 * @property string $FromDate
 * @property string $ToDate
 * @property integer $EduYear
 * @property integer $semester
 */
class Mdate extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'managedates';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['DateType', 'FromDate', 'ToDate', 'EduYear', 'semester'], 'required'],
			[['RecID', 'EduYear', 'semester'], 'integer'],
			[['DateType'], 'string'],
			[['FromDate', 'ToDate'], 'safe'],
            ['FromDate', 'compare', 'compareAttribute' => 'ToDate','operator' => '<=','message'=>Yii::t('app','date error')],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getType($date)
    {
        if($date=='REQUEST')
            return 'درخواست عضویت';
        else if ($date=='FAMILY_FORM')
            return 'ثبت اعضای خانواده';
        else
            return 'درخواست خوابگاه';
    }
    public function getYear($date)
    {
        if($date==1)
            return '1395-1396';
        else if ($date==2)
            return '1396-1397';
        else if ($date==3)
            return '1397-1398';
        else if ($date==4)
            return '1398-1399';
        else if ($date==5)
            return '1399-1400';
    }
    public function getSemester($date)
    {
        if($date==1)
            return 'نیمسال اول';
        else if ($date==2)
            return 'نیمسال دوم';
        else if ($date==3)
            return 'ترم تابستان';
    }
	public function attributeLabels()
	{
		return [
			'RecID' => Yii::t('app', 'Rec ID'),
			'DateType' => Yii::t('app', 'Date Type'),
			'FromDate' => Yii::t('app', 'From Date'),
			'ToDate' => Yii::t('app', 'To Date'),
			'EduYear' => Yii::t('app', 'Edu Year'),
			'semester' => Yii::t('app', 'Semester'),
		];
	}
}
