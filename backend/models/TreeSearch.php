<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tree;

/**
 * TreeSearch represents the model behind the search form about `app\models\Tree`.
 */
class TreeSearch extends Tree
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RecID', 'ParentID', 'enable', 'sex', 'type', 'EduSecCode'], 'integer'],
            [['PlaceName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tree::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'RecID' => $this->RecID,
            'ParentID' => $this->ParentID,
            'enable' => $this->enable,
            'sex' => $this->sex,
            'type' => $this->type,
            'EduSecCode' => $this->EduSecCode,
        ]);

        $query->andFilterWhere(['like', 'PlaceName', $this->PlaceName]);

        return $dataProvider;
    }
}
