<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\User;

/**
 * PersonSearch represents the model behind the search form about `app\models\Person`.
 */
class UserSearch extends User
{
	public $_userType;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			 
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = User::find();
		//$query->joinWith(['university']);
		// add conditions that should always apply here

		$query->andFilterWhere(['type' => $this->_userType]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

// echo "<pre>";print_r($query);echo "</pre>";die();
		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'student_number' => $this->student_number,
            'universityId' => $this->universityId,
            'nationalId' => $this->nationalId,
            'idNo' => $this->idNo,
        ]);

//		$query->andFilterWhere(['like', 'startingyear', $this->startingyear])
//        ->andFilterWhere(['like', 'startingsemester', $this->startingsemester])
//
//        ->andFilterWhere(['like', 'last_name', $this->last_name]);
        $query->andFilterWhere(['like', 'last_name', $this->last_name]);

		return $dataProvider;
	}


	public function searchPayment($params)
	{
		$query = User::find();

		// if(isset($params['columns'])){
		// 	if($params['columns'][1]['search']['value'] != ''){
		// 		$query->andFilterWhere(['=', 'university_code', (int)$params['columns'][1]['search']['value']]);
		// 	}

			 

		// 	if($params['columns'][2]['search']['value'] != ''){
		// 		$query->andFilterWhere(['=', 'status', (int)$params['columns'][2]['search']['value']]);
		// 	}
		// }

		$query->joinWith([
			'university',
			'memberPayment' ,
		]);

		

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		 

		return $dataProvider;
	}
}
