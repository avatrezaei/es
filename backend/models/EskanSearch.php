<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Eskan;

/**
 * EskanSearch represents the model behind the search form about `app\models\Eskan`.
 */
class EskanSearch extends Eskan
{
    public $user;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'room_id', 'PersonID'], 'integer'],
            [['user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Eskan::find();

        // add conditions that should always apply here
        $query->joinWith(['user']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'room_id' => $this->room_id,
            'PersonID' => $this->PersonID,
            'term' => $this->term,
        ])
       ->andFilterWhere(['like', 'user.student_number', $this->user])
            ->andFilterWhere(['like', 'user.name', $this->user])
        ->andFilterWhere(['like', 'user.last_name', $this->user]);

        return $dataProvider;
    }
}
