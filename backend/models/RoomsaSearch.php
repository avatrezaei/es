<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Roomsa;

/**
 * RoomsaSearch represents the model behind the search form about `app\models\Roomsa`.
 */
class RoomsaSearch extends Roomsa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RoomID', 'capacity', 'area'], 'integer'],
            [['address', 'sex', 'place', 'subset', 'floor', 'suite', 'room'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Roomsa::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'RoomID' => $this->RoomID,
            'capacity' => $this->capacity,
            'area' => $this->area,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'subset', $this->subset])
            ->andFilterWhere(['like', 'floor', $this->floor])
            ->andFilterWhere(['like', 'suite', $this->suite])
            ->andFilterWhere(['like', 'room', $this->room]);

        return $dataProvider;
    }
}
