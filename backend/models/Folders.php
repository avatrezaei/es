<?php

namespace backend\models;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "folders".
 *
 * @property string $id
 * @property integer $parent_id
 * @property string $name
 * @property string $hashed_name
 * @property string $path
 * @property integer $max_quota_volume
 * @property integer $max_depth
 * @property integer $total_size
 * @property string $permision
 * @property integer $user_id
 * @property integer $folder_type
 * @property string $folder_desc
 * @property string $created_date_time
 * @property string $modified_date_time
 * @property integer $is_user_root_dir
 * @property integer $active
 * @property integer $valid
 */
class Folders extends \yii\db\ActiveRecord {

	const SCENARIO_MKDIR = 'mkDir';
	const ADMINSTRATE_FOLDER_NAME = 'adminestrator';
	const USERS_FOLDER_NAME = 'users';
	const CATEGORIES_FOLDER_NAME='categories';
	const THUMBNAILS_CATEGORIES_FOLDER_NAME='thumbnails_categories';
	const USERS_PIC_FOLDER_NAME='users_pic';
	const FIELD_ICON_FOLDER_NAME='icon_field';
	const SPORT_LOCATION_FOLDER_NAME='sport_location';
	const RULES_DOCUMENT_FOLDER_NAME='rules_document';
	const RULES_PDF_FOLDER_NAME='rules_pdf';
	const FORM_3SIGNETURE_FOLDER_NAME='form_3signeture';
	const PROBLEM_REPORT_FOLDER_NAME='problem_report';

	
	
	const ADMINSTRATE_FOLDER_ID = 2;
	const USERS_FOLDER_ID = 3;
	const CATEGORIES_FOLDER_ID=4;
	const THUMBNAILS_CATEGORIES_FOLDER_ID=5;
	const USERS_PIC_FOLDER_ID=6;
	const FIELD_ICON_FOLDER_ID=7;
	const SPORT_LOCATION_FOLDER_ID=8;
	const RULES_DOCUMENT_FOLDER_ID=9;
	const RULES_PDF_FOLDER_ID=10;
	const FORM_3SIGNETURE_FOLDER_ID=11;
	const PROBLEM_REPORT_FOLDER_ID=12;

	const DIR_CHMODE = 0700;
	const FOLDER_MAX_QUOTA_VOLUME = 2147483648; //2147483648   byte =2 gig
	//const FOLDER_MAX_QUOTA_VOLUME = 200; //2048000 kb =2 gig
	const FOLDER_MAX_DEPTH = 6;
	/*
	 * (private & public radio box for folder_type)
	 * to prevent access apachee directly to this folders file
	 * publiac or private folder
	 */
	const PUBLIC_FOLDER=1;
	const PRIVATE_FOLDER=2;
	/*-----------------------------------------------*/
	public $fullFolderPath;
	public $rootDir;
	public $currentFolderId;
	public $parentPath;
	public $dirSeparator;
	public $realPath;
	public $error;
	public $fileModel;
	public $_parentFolderObj;
	public $trulyDelete = 0;

	public $image;
	
	
	public $finalBreadCrumsText;






	/* ------Added Attribute-------- */
	public $is_user_root_dir;

	/*	 * *********** ***Add new attribute */

	public function attributes() {
		$attributes = parent::attributes();
		$attributes = array_merge($attributes, [
			'is_user_root_dir'
		]);
		return $attributes;
	}

	/**
	 * @inheritdoc
	 */
	public function __construct($config = []) {
		$path = $_SERVER['DOCUMENT_ROOT'] . $this->getDirSeparator() . 'uploads';
		if (!is_dir($path)&&is_null($this->find()->where(['id' => 1])->one())) {

			$this->initialBaseDir();
			
		}
//		else if(!is_dir($path))
//		{
//		   $this->mkBaseDir($path . $this->getDirSeparator()); 
//		}
//		else if(is_null($this->find()->where(['id' => 1])->one()))
//		{
//		   $this->mkBaseDirInDB(); 
//		}

		parent::__construct($config);
	}

	public static function tableName() {
		return 'folders';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['name', 'folder_type','folder_desc'], 'required'],
			[['name'], 'dirNameValidation', 'on' => Folders::SCENARIO_MKDIR],
			[['folder_desc'], 'dirNameValidation', 'on' => Folders::SCENARIO_MKDIR],
			['folder_type', 'in', 'range' => [1,2]],		
			[['parent_id', 'is_user_root_dir', 'max_quota_volume', 'max_depth', 'total_size', 'user_id', 'folder_type', 'active', 'valid'], 'integer'],
			[['name'], 'string', 'max' => 150],
			[['path', 'folder_desc'], 'string', 'max' => 1000],
			[['permision'], 'string', 'max' => 5],
			[['currentFolderId', 'is_user_root_dir'], 'safe'],
//			[['image'], 'safe'],
//			[['image'], 'file', 'extensions'=>'jpg, gif, png,docx'],
		];
	}

	/* -------------------validation-------------------- */

	public function dirNameValidation($attribute, $params) {

		if (!empty($this->$attribute) || $this->$attribute !== '') {

			$pattern = '/^([a-zA-Z0-9]|[\p{Arabic}]|[_])*$/u';

			if (!preg_match($pattern, $this->$attribute)) {
				$this->addError($attribute, 'فقط از کاراکترهای حرفی وعددی استفاده شود.');
			}
		}
		return TRUE;
	}

	/* -------------------------------------------------- */

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'parent_id' => Yii::t('app', 'Parent ID'),
			'name' => Yii::t('app', 'Name'),
			'hashed_name' => Yii::t('app', 'Hashed Name'),
			'path' => Yii::t('app', 'Path'),
			'max_quota_volume' => Yii::t('app', 'Max Quota Volume'),
			'max_depth' => Yii::t('app', 'Max Depth'),
			'total_size' => Yii::t('app', 'Total Size'),
			'permision' => Yii::t('app', 'Permision'),
			'user_id' => Yii::t('app', 'User ID'),
			'folder_type' => Yii::t('app', 'Folder Type'),
			'folder_desc' => Yii::t('app', 'Description'),
			'active' => Yii::t('app', 'Active'),
			'valid' => Yii::t('app', 'Valid'),
		];
	}

	/* ---------relation with File-------------- */

	public function getFiles() {
		return $this->hasMany(File::className(), ['folder_id' => 'id']);
	}

	/*	 * *******************initial------------------------------- */

	public function getDirSeparator() {

		if (!empty($this->dirSeparator)) {
			return $this->dirSeparator;
		} else {
			if (strncasecmp(PHP_OS, 'win', 3))
				return ($this->dirSeparator = "/");
			else
				return ($this->dirSeparator = "/");
		}
	}

	public function getRootDir() {
	   // $path = $_SERVER['DOCUMENT_ROOT'] . $this->getDirSeparator() . 'uploads';
		$path = $_SERVER['DOCUMENT_ROOT'] . '/uploads';
		if (!is_dir($path) || is_null($this->find()->where(['id' => 1])->one())) {
			$this->initialBaseDir();
		}
		return $path;
	}

	public function initialBaseDir() {
		$path = $_SERVER['DOCUMENT_ROOT'] . $this->getDirSeparator() . 'uploads';
		if (!is_dir($path)) {
			if (!@mkdir($path, self::DIR_CHMODE, true))
				throw new \yii\web\HttpException(403, 'اشکال در ایجاد مدیریت فایل.لطفا دوباره سعی کنید');
		}

		if (is_null($this->find()->where(['id' => 1])->one())) {

			$query = "INSERT INTO `folders` (`id`, `parent_id`, `name`, `path`, `max_quota_volume`, `max_depth`, `valid`) VALUES 
			   ('1', '0', 'root', '', '', '" . self::FOLDER_MAX_DEPTH . "', '1'),
			   ('".self::ADMINSTRATE_FOLDER_ID."', '1', '" . self::ADMINSTRATE_FOLDER_NAME . "', '/" . self::ADMINSTRATE_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::USERS_FOLDER_ID."', '1', '" . self::USERS_FOLDER_NAME . "', '/" . self::USERS_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::USERS_PIC_FOLDER_ID."', '1', '" . self::USERS_PIC_FOLDER_NAME . "', '/" . self::USERS_PIC_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::FIELD_ICON_FOLDER_ID."', '1', '" . self::FIELD_ICON_FOLDER_NAME . "', '/" . self::FIELD_ICON_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::CATEGORIES_FOLDER_ID."', '1', '" . self::CATEGORIES_FOLDER_NAME . "', '/" . self::CATEGORIES_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::SPORT_LOCATION_FOLDER_ID."', '1', '" . self::SPORT_LOCATION_FOLDER_NAME . "', '/" . self::SPORT_LOCATION_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),

			   ('".self::RULES_DOCUMENT_FOLDER_ID."', '1', '" . self::RULES_DOCUMENT_FOLDER_NAME . "', '/" . self::RULES_DOCUMENT_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),

			   ('".self::FORM_3SIGNETURE_FOLDER_ID."', '1', '" . self::FORM_3SIGNETURE_FOLDER_NAME . "', '/" . self::FORM_3SIGNETURE_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),  

			   ('".self::RULES_PDF_FOLDER_ID."', '1', '" . self::RULES_PDF_FOLDER_NAME . "', '/" . self::RULES_PDF_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   		
			   ('".self::PROBLEM_REPORT_FOLDER_ID."', '1', '" . self::PROBLEM_REPORT_FOLDER_NAME . "', '/" . self::PROBLEM_REPORT_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   
			   ('".self::THUMBNAILS_CATEGORIES_FOLDER_ID."', '1', '" . self::THUMBNAILS_CATEGORIES_FOLDER_NAME . "', '/" . self::THUMBNAILS_CATEGORIES_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1')
			   ;";
			$command = Yii::$app->db->createCommand($query);

			try {
				$result = $command->execute();
				$this->mkBaseDir($path . $this->getDirSeparator());
			} catch (Exception $result) {
				//die(var_dump($result));
				throw new \yii\web\HttpException(403, 'مشکل در ایجاد شاخه های اولیه بوجود آمده است.');
			}
		}
	}


	public function mkBaseDirInDB() {
		 if (is_null($this->find()->where(['id' => 1])->one())) {

			$query = "INSERT INTO `folders` (`id`, `parent_id`, `name`, `path`, `max_quota_volume`, `max_depth`, `valid`) VALUES 
			   ('1', '0', 'root', '', '', '" . self::FOLDER_MAX_DEPTH . "', '1'),
			   ('".self::ADMINSTRATE_FOLDER_ID."', '1', '" . self::ADMINSTRATE_FOLDER_NAME . "', '/" . self::ADMINSTRATE_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::USERS_FOLDER_ID."', '1', '" . self::USERS_FOLDER_NAME . "', '/" . self::USERS_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::USERS_PIC_FOLDER_ID."', '1', '" . self::USERS_PIC_FOLDER_NAME . "', '/" . self::USERS_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::FIELD_ICON_FOLDER_ID."', '1', '" . self::FIELD_ICON_FOLDER_NAME . "', '/" . self::USERS_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::CATEGORIES_FOLDER_ID."', '1', '" . self::CATEGORIES_FOLDER_NAME . "', '/" . self::CATEGORIES_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::SPORT_LOCATION_FOLDER_ID."', '1', '" . self::SPORT_LOCATION_FOLDER_NAME . "', '/" . self::SPORT_LOCATION_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::RULES_DOCUMENT_FOLDER_ID."', '1', '" . self::RULES_DOCUMENT_FOLDER_NAME . "', '/" . self::RULES_DOCUMENT_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::RULES_PDF_FOLDER_ID."', '1', '" . self::RULES_PDF_FOLDER_NAME . "', '/" . self::RULES_PDF_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::THUMBNAILS_CATEGORIES_FOLDER_ID."', '1', '" . self::THUMBNAILS_CATEGORIES_FOLDER_NAME . "', '/" . self::THUMBNAILS_CATEGORIES_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::FORM_3SIGNETURE_FOLDER_ID."', '1', '" . self::FORM_3SIGNETURE_FOLDER_NAME . "', '/" . self::FORM_3SIGNETURE_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1'),
			   ('".self::PROBLEM_REPORT_FOLDER_ID."', '1', '" . self::PROBLEM_REPORT_FOLDER_NAME . "', '/" . self::PROBLEM_REPORT_FOLDER_NAME . "', '', '" . self::FOLDER_MAX_DEPTH . "','1')
				;";
			$command = Yii::$app->db->createCommand($query);

			try {
				$result = $command->execute();
			} catch (Exception $result) {
				//die(var_dump($result));
				throw new \yii\web\HttpException(403, 'مشکل در ایجاد شاخه های اولیه بوجود آمده است.');
			}
		}
	}
	public function mkBaseDir($rootPath) {
		$dirSeprator = $this->getDirSeparator();
		if (!is_dir($rootPath . self::ADMINSTRATE_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::ADMINSTRATE_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه کاربران ثبت نامی  لطفا دوباره سعی کنید');
		}

		if (!is_dir($rootPath . self::USERS_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::USERS_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه کاربران ثبت نامی  لطفا دوباره سعی کنید');
		}
		
		if (!is_dir($rootPath . self::FIELD_ICON_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::FIELD_ICON_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه   عکس های  فیلدهای ورزشی  لطفا دوباره سعی کنید');
		}
		 
		if (!is_dir($rootPath . self::USERS_PIC_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::USERS_PIC_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه   عکس های کاربران  لطفا دوباره سعی کنید');
		}
		
		if (!is_dir($rootPath . self::CATEGORIES_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::CATEGORIES_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه   عکس های مجموعه ها  لطفا دوباره سعی کنید');
		}		
		
		if (!is_dir($rootPath . self::THUMBNAILS_CATEGORIES_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::THUMBNAILS_CATEGORIES_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه عکس های مینیاتوری مجموعه ها لطفا دوباره سعی کنید');
		}
		
		if (!is_dir($rootPath . self::SPORT_LOCATION_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::SPORT_LOCATION_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه محل ورزشگاه  مجموعه ها لطفا دوباره سعی کنید');
		}  
		if (!is_dir($rootPath . self::RULES_DOCUMENT_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::RULES_DOCUMENT_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه فایل داکیومنت قوانین  لطفا دوباره سعی کنید');
		}
		if (!is_dir($rootPath . self::RULES_PDF_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::RULES_PDF_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه فایل پی دی اف قوانین  لطفا دوباره سعی کنید');
		}
		if (!is_dir($rootPath . self::FORM_3SIGNETURE_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::FORM_3SIGNETURE_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه فایل پی دی اف قوانین  لطفا دوباره سعی کنید');
		}

		if (!is_dir($rootPath . self::PROBLEM_REPORT_FOLDER_NAME . $dirSeprator)) {
			if (!@mkdir($rootPath . self::PROBLEM_REPORT_FOLDER_NAME . $dirSeprator, self::DIR_CHMODE, true))
				throw new CHttpException(403, 'اشکال در ایجادپوشه گزارشات خطا  لطفا دوباره سعی کنید');
		}
	}

	public function setParentObj($id = NULL) {
		if ($id == NULL)
			$id = $this->parent_id;

		if ($this->exsistsFolder($id)) {
			$this->_parentFolderObj = $this->find()->where(['id' => (int) $id])->one();
			return $this->_parentFolderObj;
		}
		$this->_parentFolderObj = NULL;
		return $this->_parentFolderObj;
	}

	public function getParentObj($id = NULL) {
		if ($this->_parentFolderObj == NULL)
			$this->setParentObj($id);

		return $this->_parentFolderObj;
	}

	/* -----------------Create Directory------------------------ */

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			
			if ($this->exsistsFolder($this->currentFolderId)) {
				$this->parent_id = $this->currentFolderId;
				$this->setParentObj();
			} else {
				die(var_dump($this->currentFolderId));
				$this->addError('name', Yii::t('app', 'The Folder Parent does not exist.'));
				return FALSE;
			}

			if ($this->getMaxDepth($this->parent_id) < 1) {
				$this->addError('name', Yii::t('app', 'Nested folders is limited and there is no possibility of creating a new branch in the depth.'));
				return FALSE;
			}

			/* ----if is user folder root must new folder by hashed name in file system-------- */
			if (isset($this->is_user_root_dir) && $this->is_user_root_dir) {
				$folderName = $this->hashed_name;
			} else {
				$folderName = $this->name;
			}

			if (!$this->makeDir($folderName)) {
				$this->addError('name', Yii::t('app', 'The problem has occurred in the creation of Folder'));
				return false;
			}


			$this->path = $this->getPathSequence($this->parent_id)
					. $this->getDirSeparator()
					. $folderName;
			
			$userId = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
			//die(var_dump( $this->is_user_root_dir));
			if ($this->isNewRecord) {
				$this->created_date_time = date('Y-m-d H:i:s');
				if (isset($this->is_user_root_dir) && !$this->is_user_root_dir)/* ----if user floder root setted before  */ {
					$this->active = 1;
					$this->is_user_root_dir = 0;
					$this->user_id = $userId;
					$this->max_depth = $this->getMaxDepth($this->parent_id) - 1;
				}
			} else {
				$this->modified_date_time = date('Y-m-d H:i:s');
			}
			return true;
		} else {
			$this->addError('name', Yii::t('app', 'The problem has occurred in the storage Folder'));
			return false;
		}
	}

	/*
	 * check exsist folder or not 
	 */

	public function exsistsFolder($id = NULL) {
		if (is_null($id)) {
			$id = $this->id;
		}

		// the following will retrieve the user 'CeBe' from the database
		$folder = Folders::find()->where(['id' => (int) $id])->one();

		if ($folder == NULL) {
			return false;
			// throw new \yii\web\HttpException(404, 'مسیر درخواست شده موجود نمی باشد.');
		}
		return true;
	}

	/* ------------------------------------------------------- */

	public function ProvideRealFolderPath($id = NULL) {

		$id = (is_null($id)) ? $this->id : $id;

		$dirSeprator = $this->getDirSeparator();
		$folderPathSeq = $this->getPathSequence($id);

		/* ---just root folder with id=1 has empty path */
//		if (empty($folderPathSeq) && $id != 1) {
//			return false;
//		}
		//if directory seprator stored sequence is not compatible by server
		if (($dirSeprator) !== "/") {
			$folderPathSeq = str_replace("/", $dirSeprator, $folderPathSeq);
		}

		$fullFolderPath = $this->getRootDir() . $folderPathSeq;
			   // echo $fullFolderPath;die;

		if (!is_dir($fullFolderPath)) {
			//if (!@mkdir($fullFolderPath, self::DIR_CHMODE, true)) {
				return false;
				//throw new \yii\web\HttpException(404, 'مسیر داده شده موجود نمی باشد.');
		   // }
		}
		return ($this->fullFolderPath = $fullFolderPath);
	}

	/*
	 * create new directory
	 * return true if created new dir
	 */

	public function makeDir($folderName = null) {

		if ($folderName == NULL) {
			$folderName = $this->name;
		}
		//provideNewFolderPath  
//echo $this->ProvideRealFolderPath($this->parent_id);die;
		if (!( $realfolderPath = $this->ProvideRealFolderPath($this->parent_id))) {
			 
			$this->addError('name', Yii::t('app', 'path is not correct'));
			return FALSE;
		}
		
					

		$newFolderPath = $realfolderPath . $this->getDirSeparator() . $folderName;
	   
		if (is_dir($newFolderPath)) {

			$this->addError('name', Yii::t('app', 'The path to the directory with this name already exists.'));
			return FALSE;
		}

		if (!mkdir($newFolderPath, self::DIR_CHMODE, true)) {
			$this->addError('name', Yii::t('app', 'The problem has occurred in the creation of new folder'));
			return FALSE;
		}

		return TRUE;
	}

	/*
	 * get sequence path stored in db 
	 * if folder dosent exist return null
	 */

	public function getParentPathSequence($id=NIL)
	{

		if ($this->_parentFolderObj == NULL)
			$this->getParentObj($id);

		if ($this->_parentFolderObj != NULL && $this->_parentFolderObj->id == $id)
			return $this->_parentFolderObj->path;


		return false; 
	}
		public function getParentMaxDepth($id=NIL)
	{
	  if ($this->_parentFolderObj == NULL)
			$this->getParentObj($id);

		if ($this->_parentFolderObj != NULL && $this->_parentFolderObj->id == $id)
			return $this->_parentFolderObj->max_depth;

		return false;
	}

		public function getPathSequence($id = null) {

		$id = (is_null($id)) ? $this->id : $id;
		if ($this->exsistsFolder($id)) {
			$pathSeq = $this->find()->where(['id' => (int) $id])->one()->path;
			return $pathSeq;
		}
		return false;
	}

	public function getMaxDepth($id = null) {

  $id = (is_null($id)) ? $this->id : $id;
		if ($this->exsistsFolder($id)) {
			$pathSeq = $this->find()->where(['id' => (int) $id])->one()->max_depth;
			return $pathSeq;
		}
		return false;
	}

	/* -----------------------Display Folder-------------------------------- */

	public function getChildFolders($folderId = NULL) {
		if ($folderId == NULL) {
			$folderId = $this->currentFolderId;
		}
		if ($this->exsistsFolder($folderId)) {
			$folderSql = "select id as folder_id,name as folder_name from  "
					. $this->tableName() .
					" where parent_id=" . (int) $folderId . " And 
						  valid=1 ";
			$command = Yii::$app->db->createCommand($folderSql);
			return $folderRows = $command->queryAll();	  // query and return all rows of result

			try {
				return $folderRows = $command->queryAll();	  // query and return all rows of result
			} catch (Exception $exc) {
				return array();
				// throw new CHttpException(400, 'Problem in request.Try again.');
			}
		} else {
			return array();
			// throw new CHttpException(400, 'شا');
		}
	}

	public function getChildFiles($folderId = NULL) {
		if ($folderId == NULL) {
			$folderId = $this->currentFolderId;
		}
		$fileObj = new File();
		if ($this->exsistsFolder($folderId)) {
			$fileSql = "select id as file_id,name as file_name,ext as file_ext,size"
					. " as file_size,file_desc as file_desc from  " . $fileObj->tableName() . " 
						where folder_id=" . (int) $folderId . "  ORDER BY id ASC";
			$command = Yii::$app->db->createCommand($fileSql);

			try {
				return $fileRows = $command->queryAll();	  // query and return all rows of result
			} catch (Exception $exc) {
				return array();
				//throw new CHttpException(400, 'Problem in request.Try again.');
			}
		} else {
			return array();
		}
	}

	public function provideContentToDisplay($folderId = NULL) {
		if ($folderId == NULL) {
			$folderId = $this->currentFolderId;
		}

		$folders = $this->getChildFolders($folderId);
		$files = $this->getChildFiles($folderId);
		//die(var_dump( $folders));

		$i = 1;			//baraye namayesh dar grid view bayad index id dar array ersaly bashad
		$arrayData = array();
		foreach ($folders as $key => $value) {
			$arrayData[$i] = array(
				'id' => $i,
				'item_id' => $value['folder_id'],
				'name' => $value['folder_name'],
				'size' => '-',
				'is_dir' => true
			);
			$i++;
		}

		foreach ($files as $key => $value) {
			$arrayData[$i] = array(
				'id' => $i,
				'item_id' => $value['file_id'],
				'name' => $value['file_name'] . '.' . $value['file_ext'],
				'size' => $value['file_size'],
				'is_dir' => FALSE
			);
			$i++;
		}
		$dataprovider = new ArrayDataProvider([
			'key' => 'id', //or whatever you id actually is of these models.
			'allModels' => array_values($arrayData),
			'pagination' => array('pageSize' => 20),
		]);

		return $dataprovider;
	}

	/* -------Remove Folder---------------------- */

	public function removeDir($id = null) {
		if ($this->exsistsFolder($this->id)) {
			$this->setParentObj();
			$this->ProvideRealFolderPath($this->id);
			//die(var_dump(rmdir($this->fullFolderPath)));
			if (!@rmdir($this->fullFolderPath)) {
				$this->addError('error', Yii::t('app','Directory should be empty.'));
				return FALSE;
			}
			return TRUE;
		}else
		{
			$this->addError('error', Yii::t('app', 'The Folder does not exist.'));
			return false;
		}
		return true;
	}

	/* ------------------------------------------ */

	public function delete($id = NULL) {

		if ($id != NULL)
			$this->id = $id;

		if ($this->trulyDelete) {
			return parent::delete();
		} else {
			$this->valid = 0;
			parent::beforeDelete();

			$result = Yii::$app->db->createCommand()->update($this->tableName(), ['valid' =>0], 'id='.(int)$this->id)->execute();
			if ($result)
				parent::afterDelete();
			return $result;
		}
	}
	/* ------- END Remove Folder---------------------- */
	
	/*----------folder type Function-----------------*/
	public function getTypeptions() {
		return array(
			self::PUBLIC_FOLDER => Yii::t('app','Public'),
			self::PRIVATE_FOLDER =>Yii::t('app','Private'),
		);
	}

	public function getTypeText() {
		$typeOption = $this->getGenderOptions();
		return ((!empty($typeOption[$this->folder_type])) ? $typeOption[$this->folder_type] : '-----');
	}	 
	/*------------------------------------------------*/
	
	public function getFolderBreadcrumbs($id=NULL) {
	
	if($id==NULL)
		$id=  $this->currentFolderId;
	
	$homePage=  \yii\helpers\Html::a(Yii::t('app','Home'),['/YiiFileManager/folders']).'>>';
	$fullBreadcrumbs=$homePage;
	
	$currentFolderModel = $this->find()->where(['id' => (int) $id])->one();
	$pathSeq=$currentFolderModel->path;
	
	if(!empty($pathSeq))
	{
		$arraySequence=explode('/', $pathSeq);
		$count=  count($arraySequence);
		$parentModels=array();
		for ($i=0;$i<$count;$i++) 
		 {
		   //die(var_dump($arraySequence));

			if(!empty($arraySequence[$i]))
			{
				/*
				 * hirearchical is top-down  => root->folder1->folder2.....
				 * so folderi has root parent by id=1
				 */
				if($i==1)
					$parentId=1;//parent of first folder is root
				else 
					$parentId=(is_object ($parentModels[($i-1)]))?$parentModels[($i-1)]->id:1;//use previos folder model that is parent id

				$parentModels[$i] = $this->findOne(['user_id'=>$currentFolderModel->user_id,'name'=>$arraySequence[$i],'parent_id'=>$parentId]);
				if($parentModels[$i]==NULL)
				{
				  return $homePage;
				}  else {
				 $fullBreadcrumbs .=  \yii\helpers\Html::a($arraySequence[$i],['/YiiFileManager/folders','id'=> $parentModels[$i]->id]).'>>';
				}
			}
		}

		return $fullBreadcrumbs;

	}else
	{
		return $homePage;
	}
	}
	
	
	
	
	/**
 * Get the directory size
 * @param directory $directory
 * @return integer
 */
function dirSize($folderId) {
			   $directory=$this->ProvideRealFolderPath($folderId);

	$size = 0;
	foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file){
		$size+=$file->getSize();
	}
	return $size;
} 


	public function humanReadableFileSize($size_in_bytes) { // Function 2
		$value = 0;
		$round = 1;
		if ($size_in_bytes >= 1073741824) {
			$value = round($size_in_bytes / 1073741824 * 10) / 10;
			return ($round) ? round($value) . 'Gb' : "{$value} Gb";
		} else if ($size_in_bytes >= 1048576) {
			$value = round($size_in_bytes / 1048576 * 10) / 10;
			return ($round) ? round($value) . 'Mb' : "{$value} Mb";
		} else if ($size_in_bytes >= 1024) {
			$value = round($size_in_bytes / 1024 * 10) / 10;
			return ($round) ? round($value) . 'Kb' : "{$value} Kb";
		} else {
			return "{$size_in_bytes} Bytes";
		}
	}
	
}
