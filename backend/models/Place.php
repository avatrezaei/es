<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "place".
 *
 * @property integer $id
 * @property string $name
 * @property integer $capacity
 * @property integer $occupy
 * @property integer $free
 * @property integer $num_of_floor
 * @property integer $num_of_suite
 * @property integer $num_of_room
 * @property string $gio_lenght
 * @property string $gio_width
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'place';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id', 'capacity', 'occupy', 'free', 'num_of_suite', 'num_of_room', 'num_of_floor'], 'integer'],
            [['name', 'gio_lenght', 'gio_width'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','name'),
            'capacity' => Yii::t('app','capacity'),
            'occupy' => Yii::t('app','occupy'),
            'free' => Yii::t('app','free'),
			'num_of_floor' => Yii::t('app','num_of_floor'),
            'num_of_suite' => Yii::t('app','num_of_suite'),
            'num_of_room' => Yii::t('app','num_of_room'),
            'gio_lenght' => Yii::t('app','gio_lenght'),
            'gio_width' => Yii::t('app','gio_width'),
        ];
    }
}
