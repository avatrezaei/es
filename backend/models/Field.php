<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "field".
 *
 * @property integer $id
 * @property string $name
 * @property string $ename
 */
class Field extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'field';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'ename'], 'required'],
			[['name', 'ename'], 'string', 'max' => 52],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'ename' => Yii::t('app', 'Ename'),
		];
	}
}
