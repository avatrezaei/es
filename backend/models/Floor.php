<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "floor".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 */
class Floor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'floor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'name'], 'required'],
            [['id', 'parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'شناسه'),
			'parent_id' => Yii::t('app', 'نام محل اسکان'),
			'name' => Yii::t('app', 'نام'),
        ];
    }
}
