<?php

namespace app\models;

use backend\models\User;
use backend\models\Userd;
use Yii;
use backend\models\jdf;

/**
 * This is the model class for table "delay".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property string $description
 * @property string $time
 */
class Delay extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'delay';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'date', 'time'], 'required'],
			[['user_id'], 'integer'],
			[['date'], 'safe'],
            [['description','time'], 'string', 'max' => 255],
		];
	}
    public function dateDisplayFormat($date=null)
    {
        //$date=$this->from_date;

        if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if ($date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '0000-00-00';
        }
        return $date = $this->getJalalyDate($date);
        //return $date;
    }
    public function getJalalyDate($date) {
        $jdf = new jdf();
        $jDateString = $jdf->jdate('Y-m-d', strtotime($date));
        return $jDateString;
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Students'),
			'date' => Yii::t('app', 'Date'),
			'time' => Yii::t('app', 'Times'),
            'description' => Yii::t('app', 'Comment'),
        ];
	}
}
