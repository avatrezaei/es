<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use backend\models\jdf;
use backend\models\University;
use yii\helpers\Html;
use yii\helpers\Url;

use yii\web\UploadedFile;;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $last_name
 * @property string $office
 * @property integer $gender
 * @property integer $age
 * @property integer $grade
 * @property string $edu
 * @property string $univ
 * @property string $homepage
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $tel
 * @property string $mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $login_attemp_count
 * @property string $last_login_time
 * @property string $last_login_attemp_time
 * @property integer $lock
 * @property integer $valid
  * @property integer $student_number
  * @property integer $university_name
  * @property integer $type
 * @property Project[] $projects
 */
class User extends \common\models\User
{


	const USER_TYPE_STUDENT = 0;
	const USER_TYPE_USER	= 1;


	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;
	const Active = 1;
	const InActive = 0;
	const MALE = 'man';
	const FEMALE = 'woman';
	const SINGLE = 'single';
	const MARRIED = 'married';
	const GRADE_PHD = 1;
	const GRADE_PHD_STUDENT = 2;
	const GRADE_MASTER = 3;
	const GRADE_MASTER_STUDENT = 4;
	const GRADE_BACHELOR = 5;
	const GRADE_BACHELOR_STUDENT = 6;
	const GRADE_KARDANI = 7;
	const GRADE_KARDANI_STUDENT = 8;
	const GRADE_DIPLOMA = 9;

	/* user pic files */
	public $fileTyps='png,jpeg,jpg';
	public $fileExtentionsArr=['png','jpg','jpeg'];
	public $mimeFileTyps=['image/jpeg','image/jpg','image/png'];
	public $fileSize=500;//500 KB
	public $fileSizeKB=500;//500 KB
    public $fileimg;
    public $fileimg1;
    public $fileimg2;
    public $fileimg3;
    public $fileAttr;
	public $fileAttr1;
	public $fileAttr2;
	public $fileAttr3;
	public $user_picture;
	public $birth_certificate_picture_face;
	public $birth_certificate_picture_back;
	public $national_card_pciture;
	public $uniname;
	public $password;
	/*
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'user';
	}

	//

	public function scenarios()
	{
		$scenarios = parent::scenarios();

		 

		//Scenario Values Only Accepted
		$scenarios['UpdateUser'] = ['userPosition','name','last_name','tel','mobile','birthDate','gender','marital','email',  'fileId','passwordInput','passwordInput_repeat','nationalId','idNo','birthCityId'];//Scenario Values Only Accepted

		$scenarios['UpdateProfileInfo'] = ['userPosition','name','last_name','tel','mobile','birthDate','gender','marital','email',  'fileId','passwordInput','passwordInput_repeat','sportNo', 'idNo','nationalId','birthCityId','student_number','city_location_id','address','jobincome','startingsemester','units','major','shift'];

		return $scenarios;
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'username',
					'name', 
					'last_name', 
					'email', 
					'tel', 
					'mobile', 
					'created_at', 
					'updated_at', 
					'student_number',
					'nationalId',
					'universityId',
					'gender',

					'idNo',




					'address',

					// 'name', 
					// 'last_name', 
					// 'email', 
					// 'tel', 
					// 'mobile', 
					// 'created_at', 
					// 'updated_at', 
					// 'student_number',
					// 'nationalId',
					// 'universityId',
					// 'gender',
					// 'fatherName',
					// 'idNo',
					// 'jobincome',
					// 'startingyear',
					// 'startingsemester',
					// 'units',
					// 'major',
					// 'birthCityId',
					// 'city_location_id',
					// 'shift',
					// 'address',
					// 'type'
					 

				], 'required',],
			//[['gender', 'age', 'grade', 'status', 'created_at', 'updated_at', 'login_attemp_count', 'lock', 'valid'], 'integer'],
			//[['last_login_time', 'last_login_attemp_time','student_number', 'name', 'last_name','university_name'], 'safe'],
			[['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],

				[['fileAttr','fileAttr1','fileAttr2','fileAttr3'], 'file',
						'extensions' =>$this->fileTyps,
						'checkExtensionByMimeType' => true,
						'tooBig' => " حداکثر اندازه فایل باید 1 مگابایت باشد.",
						'minSize' => File::MIN_FILE_SIZE,
						'mimeTypes' =>$this->mimeFileTyps,
					     'maxSize' => $this->fileSize,


				],

			[['user_picture','birth_certificate_picture_face','birth_certificate_picture_back','national_card_pciture'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],

		];
	}
 

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'					=> Yii::t('app','ID'),
			'username' 				=> Yii::t('app','Username'),
			'password' 				=> Yii::t('app','Password'),
			'salt' 					=> Yii::t('app','Salt'),
			'name' 					=> Yii::t('app','Name'),
			'last_name' 			=> Yii::t('app','Last Name'),
			'organization_id' 		=> Yii::t('app','Organization'),
			'gender' 				=> Yii::t('app','Gender'),
			'marital' 				=> Yii::t('app','Marital'),
			'homepage' 				=> Yii::t('app','Homepage'),
			'tel' 					=> Yii::t('app','Tel'),
			'mobile' 				=> Yii::t('app','Mobile'),
			'email' 				=> Yii::t('app','Email'),
			'type_assist' 			=> Yii::t('app','Type Assist'),
			'login_attemp_count' 	=> Yii::t('app','Login Attemp Count'),
			'updated_at' 			=> Yii::t('app','Update At'),
			'update_date' 			=> Yii::t('app','Update Date'),
			'created_at' 			=> Yii::t('app','Create At'),
			'create_date' 			=> Yii::t('app','Create Date'),
			'last_login_time' 		=> Yii::t('app','Last Login Time'),
			'last_login_attemp_time' => Yii::t('app','Last Login Attemp Time'),
			'lastaction' 			=> Yii::t('app','Last Action'),
			'lock' 					=> Yii::t('app','Lock'),
			'valid' 				=> Yii::t('app','Valid'),
			'active' 				=> Yii::t('app','Active'),
			'role' 					=> Yii::t('app','Role'),
			'lang' 					=> Yii::t('app','Language'),
			'passwordInput' 		=> Yii::t('app','passwordInput'),
			'passwordInput_repeat' 	=> Yii::t('app','passwordInput Repeat'),
			'sportNo' 				=> Yii::t('app','Sport Number'),
			'eName' 				=> Yii::t('app','English Name'),
			'eFamily' 				=> Yii::t('app','English Family'),
			'fatherName' 			=> Yii::t('app','Father Name'),
			'idNo' 					=> Yii::t('app','Id Number'),
			'birthDate' 			=> Yii::t('app','BirthDate'),
			'birthCityId' 			=> Yii::t('app','BirthCity'),
			'user_picture'			=>Yii::t('app', 'Avatar Pic'),
			'uniname'		=>Yii::t('app','University Name'),
			'fileAttr'		=>Yii::t('app','Please choose your image'),
			'fileAttr1'		=>Yii::t('app','Please choose your image1'),
			'fileAttr2'		=>Yii::t('app','Please choose your image2'),
			'fileAttr3'		=>Yii::t('app','Please choose your image3'),
			'field'					=>Yii::t('app','Field'),
			'birth_certificate_picture_face'=>Yii::t('app', 'صفحه اول شناسنامه'),
			'birth_certificate_picture_back'=>Yii::t('app', 'صفحه دوم شناسنامه'),
			'national_card_pciture'=>Yii::t('app', 'تصویر کارت ملی'),
			'nationalId'=>Yii::t('app', 'National Id'),
			'creatorUserId'=>Yii::t('app', 'Creator User Id'),
			'teamId'=>Yii::t('app', 'Team'),
			'caravanId'=>Yii::t('app', 'Caravan'),
			'userType'=>Yii::t('app', 'User Type'),
			'userPosition'=>Yii::t('app', 'User Position'),
			'athleteSubFieldId'=>Yii::t('app', 'Athlete Sub Field Id'),
			//'university_name'=>Yii::t('app', 'University Name'),
			'student_number'=>Yii::t('app', 'Student Number'),
			'city_location_id'=>Yii::t('app', 'شهر محل سکونت'),
			'address'=>Yii::t('app', 'Address'),
			'jobincome'=>Yii::t('app', 'Job in Come'),
			'marital'=>Yii::t('app', 'وضعیت تاهل'),
			'shift'=>Yii::t('app', 'دوره'),
			//'startingyear'=>Yii::t('app', 'سال ورود'),
			//'startingsemester'=>Yii::t('app', 'نیمسال ورود'),
			'units'=>Yii::t('app', 'تعداد واحد گذرانده شده'),
			'major'=>Yii::t('app', 'گرایش'),
			'universityName'=>Yii::t('app','University Name'),
			'universityId'=>Yii::t('app','University Name'),
			'fullName' => Yii::t('app','Full Name'),
		];
	}


	/**
	 * @inheritdoc
	 * @return MessagesQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new UserQuery(get_called_class());
	}


public function getIconThumbnail($fileId=null,$class='')
	{

		if(isset($fileId))
			$this->fileId=$fileId;

		$avatarPath = Yii::$app->request->BaseUrl . '/images/avatar-default.jpeg';

		/* find file of categories in db */
		if (($filemodel = File::findOne($this->fileId)) !== null)
		{
			$folderModel = new Folders;
			$destination = $folderModel->ProvideRealFolderPath(Folders::USERS_PIC_FOLDER_ID) . '/' . $filemodel->name . '.' . $filemodel->ext;
			$url = Url::to([ '/YiiFileManager/file/view-file', 'name' => $filemodel->hashed_name]);

			if($class=='')
				return Html::img($url, ['alt' => $this->FullName, 'class' => 'user-pic rounded', 'width' => '45px', 'height' => '45px']);
			elseif($class=='update')
			   return Html::img($url, ['alt' => $this->FullName,  'width' => '150px', 'height' => '130px']);
			elseif($class=='UpdatePreview')
				return Html::img($url, ['alt' => $this->FullName, 'class' => 'user-pic rounded pull-right bg-font-grey-silver', 'width' => '100px', 'height' => '100px']);
			else
				return Html::img($url, ['alt' => $this->FullName, 'class' => 'user-pic rounded ' . $class]);
		}
		else
		{
			if($class=='')
				return Html::img($avatarPath, ['alt' =>  $this->FullName, 'class' => 'user-pic rounded','width' => '45px', 'height' => '45px']);
			else
				return Html::img($avatarPath, ['alt' =>  $this->FullName, 'class' => 'user-pic rounded ' . $class,'width' => '45px', 'height' => '45px']);
		}
	}
	public function upload($file)
	{
		//echo "<pre>";print_r($file);echo "</pre>";die();
		$ext = end((explode(".", $file->name)));
		// generate a unique file name
        $avatar = Yii::$app->security->generateRandomString().".{$ext}";
        // the path to save file, you can set an uploadPath
        // in Yii::$app->params (as used in example below)
        $path = Yii::$app->basePath . '/uploads/' . $avatar;

        return $file->saveAs($path);
	}
	

	public function dateDisplayFormat($date=null)
	{
		$date=$this->birthDate;

		if ( preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date )) {

			return $date ;
		}
		else if(preg_match ('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',$date))
		{

			return $date;
		}
		else if($date=='0000-00-00 00:00:00'|| $date=='0000-00-00') {
			return '0000-00-00';
		}


		return $date=$this->getJalalyDate( 'birthDate');
	}



	/**
	*/
	public function getJalalyDate($date) {
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d', strtotime($this->$date));
		return $jDateString;
	}


	//
	public static function itemAlias($type,$code=NULL)
	{
		$_items = [
			'Gender' => [
				self::FEMALE => Yii::t('app', 'Female'),
				self::MALE => Yii::t('app', 'Male'),
			],
			'Marital' => [
				self::SINGLE => Yii::t('app', 'Single'),
				self::MARRIED => Yii::t('app', 'Married'),
			],
			'GenderText' => [
				self::FEMALE => Yii::t('app', 'Mrs.'),
				self::MALE => Yii::t('app', 'Mr.'),
			],
			'GenderText' => [
				self::USER_TYPE_STUDENT => Yii::t('app', 'Student'),
				self::USER_TYPE_USER 	=> Yii::t('app', 'User'),
			],
			'Grade' => [
				self::GRADE_PHD => Yii::t('app', 'PHD'),
				self::GRADE_PHD_STUDENT => Yii::t('app', 'PHD_STUDENT'),
				self::GRADE_MASTER => Yii::t('app', 'MASTER'),
				self::GRADE_MASTER_STUDENT => Yii::t('app', 'MASTER_STUDENT'),
				self::GRADE_BACHELOR => Yii::t('app', 'BACHELOR'),
				self::GRADE_BACHELOR_STUDENT => Yii::t('app', 'BACHELOR_STUDENT'),
				self::GRADE_KARDANI => Yii::t('app', 'kARDANI'),
				self::GRADE_KARDANI_STUDENT => Yii::t('app', 'kARDANI_STUDENT'),
				self::GRADE_DIPLOMA => Yii::t('app', 'DIPLOMA'),
			],
		];
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}


	//
	public function getFullName(){
		return $this->name.' '.$this->last_name;
	}

	public function getInfo(){
		return $this->fullname." (".$this->nationalId.")";
	}

	/**
	 *
	 * Get University Name
	 *
	 */
	public function getUniversityName()
	{
		if($this->university)
			return $this->university->name;
		return Yii::t('app','Unknow');
	}
	/**
	 *
	 * Get University
	 *
	 */
	public function getUniversity()
	{
		return $this->hasOne(University::className(), ['id' => 'universityId']);
	}
	
	public static function getFullStudentName($id)
    {
        $model=new User();
       $user= User::findOne($id);
        return $user->name.' '.$user->last_name.' ';

    }



	public static  function getRoleNames($id='')
	{
		if(trim($id)=='')
		$id=\Yii::$app->user->identity->id;
		$roles= Yii::$app->authManager->getRolesByUser($id);
		$names=[];
		foreach ($roles as $role)
			$names[]=$role->name;
		return $names;
		
			
	}
	/****
	 * 
	 */
	public static function  checkedRoles($roleName,$id='')
	{
		if(trim($id)=='')
		$id=\Yii::$app->user->identity->id;
		$roles= Yii::$app->authManager->getRolesByUser($id);
		foreach ($roles as $role)
		{
			if($roleName==$role->name)
				return  true;
		}
		return false;
		
	}
	 public function getFaDateFromTimeStamp($dateField) {
		$rows = (new \yii\db\Query())
				->select(['DATE_FORMAT( `' . $dateField . '` , "%Y-%m-%d" ) AS date'])
				->from($this->tableName())
				->where('id=:id', array(':id' => $this->id))
				->all();
		$gDateString = isset($rows[0]['date']) ? $rows[0]['date'] : '';
		if($gDateString=='0000-00-00')
			return '0000-00-00';
		$this->$dateField = $gDateString;
		$jdf = new jdf();
		$jDateString = $jdf->jdate('Y-m-d', strtotime($gDateString));
		$jDateString=str_replace("-","/",$jDateString);
		return $jDateString;
	}
	public function getSex()
	{
		$class = '';

		switch ($this->gender) {
			case self::MALE:
				$class = 'success';
				break;

			case self::FEMALE:
				$class = 'info';
				break;

			default:
				$class = 'danger';
				break;
		}
		return '<span class="label label-' . $class . '">' . ($this->gender != NULL ? self::itemAlias('Gender', $this->gender) : Yii::t('app', 'not_find')) . '</span>';
	}
	public function getMarital()
	{
		$class = '';

		switch ($this->marital) {
			case self::SINGLE:
				$class = 'success';
				break;

			case self::MARRIED:
				$class = 'info';
				break;

			default:
				$class = 'danger';
				break;
		}
		return '<span class="label label-' . $class . '">' . ($this->gender != NULL ? self::itemAlias('Marital', $this->marital) : Yii::t('app', 'not_find')) . '</span>';
	}
}


/**
 * This is the ActiveQuery class for [[Messages]].
 *
 * @see Messages
 */

class UserQuery extends \yii\db\ActiveQuery
{
	public function init()
	{
		parent::init();
		return $this->andWhere([User::tableName(). '.valid' => 1]);
	}


	public function students()
	{
		return $this->andWhere([User::tableName(). '.type' => User::USER_TYPE_STUDENT]);
	}

	public function users()
	{
		return $this->andWhere([User::tableName(). '.type' => User::USER_TYPE_USER]);
	}

	public function user($id)
	{
		$this->andWhere(['id'=> $id]);
		return $this;
	}

	 
	/**
	 * @inheritdoc
	 * @return Messages[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * @inheritdoc
	 * @return Messages|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
