<?php

namespace backend\models;

use Yii;
use \app\models\Setting;
use backend\modules\YiiFileManager\models\File;

/**
 * This is the model class for table "user_setting".
 *
 * @property integer $user_id
 * @property string $key
 * @property integer $format_type
 * @property integer $access_type
 * @property integer $int_value
 * @property string $char_value
 * @property string $json_value
 * @property string $create_date_time
 * @property string $modified_date_time
 */
class UserSetting extends \yii\db\ActiveRecord {

	const USER_ACCESS_TYPE = 1;
	const ADMIN_ACCESS_TYPE = 2;
	const INT_FORMAT_TYPE = 1;
	const CHAR_FORMAT_TYPE = 2;
	const JSON_FORMAT_TYPE = 3;
	const KEY_LANGUAGE = 'lang';
	const KEY_PAGINATION = 'pagination';
	const KEY_THEME = 'theme';
	const KEY_FILETYPE = 'file_type';

	/* --------for admin setting--------- */

	public $extInputArray;
	public $targetUser;
	/* ---------------------------------- */
	public $allowedMimeTypeArray = array(
		"mimeTypes" => array(
			//pdf file
			'application/pdf', 'application/pdf',
			//pic file
			'image/gif', 'image/jpeg', 'image/png', //pdf file
			//movie mp3 file
			'application/powerpoint',
			'application/vnd.ms-powerpoint',
			'application/x-mspowerpoint',
			'application/msword',
			'application/msword',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
		),
		"ext" => array('jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx'),
	);

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user_setting';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		$fileObj = new File;

		return [
			[['char_value'], 'required', 'on' => ['language_setting', 'theme_setting']],
			[['int_value'], 'required', 'on' => ['pagination_setting']],
			[['extInputArray'], 'required', 'on' => ['update_filetype_setting']],
			//[['extInputArray'],  'in', 'range' => ($fileObj->AllFileExtentionArray)],
			[['int_value', 'targetUser'], 'integer'],
			//[['user_id', 'key', 'format_type', 'access_type', 'int_value', 'char_value', 'json_value'], 'required'],
			// [['user_id', 'key', 'format_type', 'access_type', 'int_value', 'char_value', 'json_value'], 'required'],
			//[['user_id', 'key', 'format_type', 'access_type', 'int_value', 'char_value', 'json_value'], 'required'],
			// [['user_id', 'format_type', 'access_type', 'int_value'], 'integer'],
			//[['json_value'], 'string'],
			[['create_date_time', 'modified_date_time'], 'safe'],
			[['key', 'char_value'], 'string', 'max' => 100]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'user_id' => Yii::t('app', 'User ID'),
			'key' => Yii::t('app', 'Key'),
			'format_type' => Yii::t('app', 'Format Type'),
			'access_type' => Yii::t('app', 'Access Type'),
			'int_value' => Yii::t('app', 'Value'),
			'char_value' => Yii::t('app', 'Value'),
			'json_value' => Yii::t('app', 'Value'),
			'create_date_time' => Yii::t('app', 'Create Date Time'),
			'modified_date_time' => Yii::t('app', 'Modified Date Time'),
			'extInputArray' => Yii::t('app', 'Allowed Extention For user'),
		];
	}

	public function getLanguageValueByUserID($userId = NULL) {
		if ($userId == NULL) {
			$userId = $this->user_id;
		}

		$languageModel = UserSetting::findOne(['user_id' => $userId, 'key' => self::KEY_LANGUAGE]);
		if ($languageModel != NULL) {
			return $languageModel->char_value;
		} else {
			return NULL;
		}
	}

	public function getPaginationValueByUserID($userId = NULL) {
		if ($userId == NULL) {
			$userId = $this->user_id;
		}

		$paginationModel = UserSetting::findOne(['user_id' => $userId, 'key' => self::KEY_PAGINATION]);
		if ($paginationModel != NULL) {
			return $paginationModel->int_value;
		} else {
			return NULL;
		}
	}

	public function getThemeValueByUserID($userId = NULL) {
		if ($userId == NULL) {
			$userId = $this->user_id;
		}

		$themeModel = UserSetting::findOne(['user_id' => $userId, 'key' => self::KEY_THEME]);
		if ($themeModel != NULL) {
			return $themeModel->char_value;
		} else {
			return NULL;
		}
	}

	public function getFileTypeByUserID($userId = NULL) {
		if ($userId == NULL) {
			$userId = $this->user_id;
		}

		$fileTypeModel = UserSetting::findOne(['user_id' => $userId, 'key' => self::KEY_FILETYPE]);
		if ($fileTypeModel != NULL) {
			return unserialize($fileTypeModel->json_value);
		} else {
			return NULL;
		}
	}

	/*
	 * this function get Defult setting from ASdmin Setting(setting Model)
	 * and set user settting theme-pagination-lang as user create
	 */

	public function setDefualtSettingForUser($userId) {
		// die('iiiiiii'.$userId);
		if ($this->setDefualtLanguageForUser($userId) &&
				$this->setDefualtPaginationForUser($userId) &&
				$this->setDefualtThemeForUser($userId) &&
				$this->setDefualtFileTypeForUser($userId)) {
			return TRUE;
		}
		return FALSE;
	}

	public function setDefualtLanguageForUser($userId) {

		$LangModel = new UserSetting;
		$langStr = Setting::getLanguageSetting();
		/* ---userfolder obj attribute--- */
		$LangModel->key = self::KEY_LANGUAGE;
		$LangModel->user_id = $userId;
		$LangModel->format_type = self::CHAR_FORMAT_TYPE;
		$LangModel->access_type = self::USER_ACCESS_TYPE;
		//must save language id in usersetting
		$LangModel->char_value = \app\models\Language::find()->where(['content' => $langStr])->one()->id;
		$LangModel->create_date_time = date('Y-m-d H:i:s');


		if ($LangModel->save(0)) {
			//log check exceptions????
			return TRUE;
		} else {
			//log check exceptions????
			return FALSE;
		}
	}

	public function setDefualtThemeForUser($userId) {

		$LangModel = new UserSetting;

		/* ---userfolder obj attribute--- */
		$LangModel->key = self::KEY_THEME;
		$LangModel->user_id = $userId;
		$LangModel->format_type = self::CHAR_FORMAT_TYPE;
		$LangModel->access_type = self::USER_ACCESS_TYPE;
		$LangModel->char_value = Setting::getThemeSetting();
		$LangModel->create_date_time = date('Y-m-d H:i:s');


		if ($LangModel->save(0)) {
			//log check exceptions????
			return TRUE;
		} else {
			//log check exceptions????
			return FALSE;
		}
	}

	public function setDefualtPaginationForUser($userId) {

		$LangModel = new UserSetting;

		/* ---userfolder obj attribute--- */
		$LangModel->key = self::KEY_PAGINATION;
		$LangModel->user_id = $userId;
		$LangModel->format_type = self::CHAR_FORMAT_TYPE;
		$LangModel->access_type = self::USER_ACCESS_TYPE;
		$LangModel->int_value = Setting::getPaginationSetting();
		$LangModel->create_date_time = date('Y-m-d H:i:s');


		if ($LangModel->save(0)) {
			//log check exceptions????
			return TRUE;
		} else {
			//log check exceptions????
			return FALSE;
		}
	}

	public function setDefualtFileTypeForUser($userId) {

		$LangModel = new UserSetting;
		$fileObj = new File;


		/* ---userfolder obj attribute--- */
		$LangModel->key = self::KEY_FILETYPE;
		$LangModel->user_id = $userId;
		$LangModel->format_type = self::JSON_FORMAT_TYPE;
		$LangModel->access_type = self::ADMIN_ACCESS_TYPE;
		$LangModel->json_value = serialize($fileObj->allowedMimeTypeArray);
		$LangModel->create_date_time = date('Y-m-d H:i:s');


		if ($LangModel->save(0)) {
			//log check exceptions????
			return TRUE;
		} else {
			//log check exceptions????
			return FALSE;
		}
	}

	public function getAllFileTypeAsListData() {

		$fileObj = new File;
		$dataArray = array();
		foreach ($fileObj->AllFileExtentionArray as $ext) {
			$dataArray[] = ['ext' => $ext, 'name' => Yii::t('app', $ext)];
		}

		$dataArray = \yii\helpers\ArrayHelper::map($dataArray, 'ext', 'name');
		return $dataArray;
	}

	public function UpdateFileTypeUserSetting($userId = NULL) {
		
		
		if ($userId == NULL)
			$userId = $this->targetUser;

	   // die(var_dump($userId));
		$fileObj = new File;
		foreach ($this->extInputArray as $ext) {
			foreach ($fileObj->AllFileExtMimeTypeArray[$ext]['mimeTypes'] as $index => $value) {
				$newFileMimeType[] = $value;
			}
			foreach ($fileObj->AllFileExtMimeTypeArray[$ext]['ext'] as $index => $value) {
				$newFileExt[] = $value;
			}
		}

		$newFileExtMimeTypeArray = ['mimeTypes' => $newFileMimeType, 'ext' => $newFileExt];
		$userSettingModel = UserSetting::findOne(['user_id' => $userId, 'key' => self::KEY_FILETYPE]);

		if ($userSettingModel != NULL) {

			/* ---userfolder obj attribute--- */
			$userSettingModel->json_value = serialize($newFileExtMimeTypeArray);

			if ($userSettingModel->save(0)) {
				return TRUE;
			} else {
				$this->addError('extInputArray', Yii::t('app', 'Fail to store new data.'));
				return FALSE;
			}
		} else {
			$this->addError('extInputArray', Yii::t('app', 'Fail to store new data.'));
			return false;
		}
	}

	public static function getAllowedFileExtUser($userId = NULL) {
		if ($userId == NULL)
			$userId = $this->targetUser;

		$fileTypeModel = UserSetting::findOne(['user_id' => $userId, 'key' => self::KEY_FILETYPE]);
		if ($fileTypeModel != NULL) {

			$filetypeArray = unserialize($fileTypeModel->json_value);
			/* file type includes ext & mime type, we need just xt */
			$allowedExt = $filetypeArray['ext'];
			return $allowedExt;
		} else {
			return array();
		}
	}

}
