<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "managedates".
 *
 * @property string $RecID
 * @property string $DateType
 * @property string $FromDate
 * @property string $ToDate
 * @property integer $EduYear
 * @property integer $semester
 */
class Managedates extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'managedates';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['RecID', 'DateType', 'FromDate', 'ToDate', 'EduYear', 'semester'], 'required'],
			[['RecID', 'EduYear', 'semester'], 'integer'],
			[['DateType'], 'string'],
			[['FromDate', 'ToDate'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'RecID' => Yii::t('app', 'Rec ID'),
			'DateType' => Yii::t('app', 'Date Type'),
			'FromDate' => Yii::t('app', 'From Date'),
			'ToDate' => Yii::t('app', 'To Date'),
			'EduYear' => Yii::t('app', 'Edu Year'),
			'semester' => Yii::t('app', 'Semester'),
		];
	}
}
