<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $content
 *
 * @property AcademicBackground[] $academicBackgrounds
 * @property Experience[] $experiences
 * @property Membership[] $memberships
 */
class Language extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'language';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['content'], 'required'],
			[['content'], 'string', 'max' => 2]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'content' => Yii::t('app', 'Content'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAcademicBackgrounds()
	{
		return $this->hasMany(AcademicBackground::className(), ['lang' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getExperiences()
	{
		return $this->hasMany(Experience::className(), ['lang' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMemberships()
	{
		return $this->hasMany(Membership::className(), ['lang' => 'id']);
	}
	
   public static function getLanguageText($langID)
   {
	   $langModel=Language::findOne((int)$langID);
	  $categorieName=($langModel)?($langModel->content) : ('-----'); 
	   return $categorieName;
   }
   
   }
