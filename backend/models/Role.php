<?php

 

namespace backend\models;
use Yii;
use yii\rbac\Item;

/**
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Role extends AuthItem
{
    /**
     * @inheritdoc 
     */
    public function getUnassignedItems()
    {
        $data  = [];
        $items = $this->manager->getItems(null, $this->item !== null ? [$this->item->name] : []);

        if ($this->item === null) {
            foreach ($items as $item) {
                $data[$item->name] = $this->formatName($item);
            }
        } else {
            foreach ($items as $item) {
                if ($this->manager->canAddChild($this->item, $item)) {
                    $data[$item->name] = $this->formatName($item);
                }
            }
        }

        return $data;
    }

    /**
     * Formats name.
     *
     * @param  Item $item
     * @return string
     */
    protected function formatName(Item $item)
    {
        return empty($item->description) ? $item->name : $item->name . ' (' . $item->description . ')';
    }

    /**
     * @inheritdoc 
     */
    protected function createItem($name)
    {
        return $this->manager->createRole($name);
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'name' => Yii::t('app', 'Name'),
            'rule' => Yii::t('app', 'Rule'),
        ];
    }
}