<?php

namespace app\models;

use backend\models\User;
use Yii;
use backend\models\jdf;

/**
 * This is the model class for table "exitform".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $userrel_id
 * @property integer $status
 * @property string $from_date
 * @property string $to_date
 * @property string $description
 */
class Exitform extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'exitform';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'userrel_id', 'from_date', 'to_date', 'description'], 'required'],
			[['user_id', 'userrel_id','status'], 'integer'],
			[['from_date', 'to_date','user_id'], 'safe'],
			[['description'], 'string', 'max' => 255],
		];
	}
    public function dateDisplayFormat($date=null)
    {
        //$date=$this->from_date;

        if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if ($date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '0000-00-00';
        }
        return $date = $this->getJalalyDate($date);
        //return $date;
    }
    public function getJalalyDate($date) {
        $jdf = new jdf();
        $jDateString = $jdf->jdate('Y-m-d', strtotime($date));
        return $jDateString;
    }


    public function getUser()
    {
        return $this->hasOne(Familyform::className(), ['id' => 'userrel_id']);
    }


    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getState($data)
    {
        if($data==0)
            return 'تائیدنشده';
        else if($data==1)
            return 'تائیددرخواست';
        else
            return 'رد درخواست';

    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Students'),
			'userrel_id' => Yii::t('app', 'Userrel ID'),
			'from_date' => Yii::t('app', 'From Date'),
			'to_date' => Yii::t('app', 'To Date'),
			'description' => Yii::t('app', 'Description'),
			'status' => Yii::t('app', 'Status'),

		];
	}
}
