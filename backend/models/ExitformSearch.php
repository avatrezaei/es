<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Exitform;

/**
 * ExitformSearch represents the model behind the search form about `app\models\Exitform`.
 */
class ExitformSearch extends Exitform
{
    public $users;
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'user_id', 'userrel_id'], 'integer'],
			[['from_date', 'to_date', 'description','users'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Exitform::find();

		// add conditions that should always apply here
        $query->joinWith(['users']);
        $dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
            'exit_form.id' => $this->id,
            'user_id' => $this->user_id,
            'userrel_id' => $this->userrel_id,
            'from_date' => $this->from_date,
            'to_date' => $this->to_date,
        ])
            ->andFilterWhere(['like', 'users.student_number', $this->users])
            ->andFilterWhere(['like', 'users.name', $this->users])
            ->andFilterWhere(['like', 'users.last_name', $this->users]);
		$query->andFilterWhere(['like', 'description', $this->description]);

		return $dataProvider;
	}
}
