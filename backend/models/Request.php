<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;
use backend\models\jdf;


/**
 * This is the model class for table "request".
 *
 * @property integer $id
 * @property integer $personId
 * @property integer $semester
 * @property string $date
 * @property string $dateIn
 * @property string $dateOut
 * @property string $comment
 * @property integer $status
 */
class Request extends \yii\db\ActiveRecord
{
	const REQUEST_STATUS_REQUESTED = 0;
	const REQUEST_STATUS_REJECTED = 1;
	const REQUEST_STATUS_ACCEPTED  = 2;

	public $image;
	public $date;
	/**
	 *
	 * Request semesters
	 *
	 */
	const REQUEST_SEMESTER_ONE   = 0;
	const REQUEST_SEMESTER_TWO   = 1;
	const REQUEST_SEMESTER_THREE = 2;

	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios ['editable'] = [];
		$scenarios ['create_mode'] = ['comment', 'semester', 'eduYear', 'personId','file','date', 'dateIn', 'dateOut', 'status'];
		$scenarios ['update_mode'] =  [ 'comment','semester', 'eduYear', 'personId','file','date', 'dateIn', 'dateOut', 'status'];
		return $scenarios;
	}



	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%request}}';
	}

	/**
	 * @inheritdoc
	 * @return MessagesQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new RequestQuery(get_called_class());
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		 
		return [
			[[ 'semester', 'eduYear', 'personId',  ], 'required'],
			[['personId', 'semester', 'status'], 'integer'],
			[['personId'], 'checkReapetedRequest','on'=>'create_mode'],
			[['file','image','comment',],'safe'],
			[['date', 'dateIn', 'dateOut'], 'string', 'max' => 52],
			/* ---------file validation ---------- */
			[['image'],
					'file',
					//'skipOnEmpty' => false,
					'extensions' => 'png, jpg',
					'checkExtensionByMimeType' => true,
					'mimeTypes' =>['image/jpeg','image/png','image/jpg'],


			],

		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
		 
			'semester' => Yii::t('app', 'Semester'),
			//'semesteri' => Yii::t('app', 'Semester'),
			'date' => Yii::t('app', 'Date'),
			'dateIn' => Yii::t('app', 'Date In'),
			'dateOut' => Yii::t('app', 'Date Out'),
			'comment' => Yii::t('app', 'Comment'),
			'status' => Yii::t('app', 'Status'),
			'statusi' => Yii::t('app', 'Status'),
			'eduYear' => Yii::t('app', 'Eduction Year'),
			'fileAttr'=> Yii::t('app', 'Agreement Form'),
			'personId' => Yii::t('app', 'Person ID'),
			// other relationla field
			'personname'=> Yii::t('app', 'requster name'),
			'personcode'=> Yii::t('app', 'requster code'),
			'personuniversity'=> Yii::t('app', 'requster university'),
			'image' => Yii::t('app','Accept form'),
		];
	}



	public function checkReapetedRequest($attribute, $params) {

		$personId=(int)$this->personId;
		$eduYear=(int)$this->eduYear;
		$semester=(int)$this->semester;
		$reqObj=new Request();
		$reqresult=$reqObj->find()->where(['personId'=>$personId,'eduYear'=>$eduYear,'semester'=>$semester])->one();
		if($reqresult!=null)
		{
			$this->addError('personId', Yii::t('app', 'Reapeted data for Request'));
			return false;
		}

		return true;
	}

	//
	/**
	 *
	 * Block comment
	 *
	 */
	public function getStat()
	{
		//if (Yii::$app->user->can('updateMeal')){
			return Html::a(
				'<span class="' . ($this->status == self::REQUEST_STATUS_ACCEPTED ? 'enable' : 'disable') . '"></span>',
				[
					'changestatus',
					'id' => $this->id 
				],
				[ 
					'class' => 'status hint--top hint--rounded hint--' . ($this->status == self::REQUEST_STATUS_ACCEPTED ? 'success' : 'error'),
					'data-hint' => Yii::t('zii', ($this->status == self::REQUEST_STATUS_ACCEPTED ? 'Active' : 'Deactive')),
				]
			);
		//}
		// else{
		// 	return '<span class="label label-' . ($this->status == self::REQUEST_STATUS_ACCEPTED ? 'success' : 'important') . '">' . Html::encode(self::itemAlias('Status', $this->status)) . '</span>';
		// }
	}



	 
	





	//
    public function dateDisplayFormat($date=null)
    {
        //$date=$this->from_date;

        if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if ($date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '0000-00-00';
        }
        return $date = $this->getJalalyDate($date);
        //return $date;
    }
    public function getJalalyDate($date) {
        $jdf = new jdf();
        $jDateString = $jdf->jdate('Y-m-d', strtotime($date));
        return $jDateString;
    }
    public function getYear($date)
    {
        if($date==1)
            return '1395-1396';
        else if ($date==2)
            return '1396-1397';
        else if ($date==3)
            return '1397-1398';
        else if ($date==4)
            return '1398-1399';
        else if ($date==5)
            return '1399-1400';
    }
    public function getSemester($date)
    {
        if($date==1)
            return 'نیمسال اول';
        else if ($date==2)
            return 'نیمسال دوم';
        else if ($date==3)
            return 'ترم تابستان';
    }
	public function upload()
    {
        if ($this->validate()) 
        {
            $this->file->saveAs('uploads\\' . $this->file->baseName . '.' . $this->file->extension);
            return true;
        } 
        else 
        {
            return false;
        }
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'personId']);
	}

	public function getPersonname()
	{
		if($this->user) return $this->user->name." ".$this->user->last_name;
		return Yii::t('app','Unkonwn');
	}

	public function getPersoncode()
	{
		if($this->user) return $this->user->student_number;
		return Yii::t('app','Unkonwn');
	}

	public function getPersonuniversity()
	{
		if($this->user)
			return $this->user->universityname;
		return Yii::t('app','Unkonwn');
	}
	/**
 *
 * Block comment
 *
 */
 public function getStatusi()
    {
        $class = '';

        switch($this->status) {

        	case self::REQUEST_STATUS_REQUESTED :
                $class = 'default';
                break;

            case self::REQUEST_STATUS_REJECTED :
                $class = 'warning';
                break;            

            case self::REQUEST_STATUS_ACCEPTED :
                $class = 'success';
                break;  
                        
            default:
                $class = 'default';                
                break;        
        }
        return $this->StatusText;
       // return '<span class="status-text'.$this->id.' label label-' . $class . '">' . $this->StatusText . '</span>';
    }
    /**
     *
     * Block comment
     *
     */
    public function getStatusText()
    {
        return self::itemAlias('Status', $this->status);
    }
    /**
     *
     * Block comment
     *
     */
     public static function itemAlias($type, $code = NULL)
    {
        $_items = [
            'Status' => [
                self::REQUEST_STATUS_REQUESTED 	=> Yii::t('app', 'Not Checked'),                
                self::REQUEST_STATUS_ACCEPTED	=>Yii::t('app', 'Accepted'),
                self::REQUEST_STATUS_REJECTED   => Yii::t('app', 'Not Accepted'),
            ],
                         
        ];
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}


    
    
    
    


/**
 * This is the ActiveQuery class for [[Messages]].
 *
 * @see Messages
 */
class RequestQuery extends \yii\db\ActiveQuery
{
	public function init()
	{
		parent::init();
		return $this->andWhere([Request::tableName(). '.valid' => 1]);
	}

	public function person($id)
	{
		$this->andWhere(['personId'=> $id]);
		return $this;
	}

	 
	/**
	 * @inheritdoc
	 * @return Messages[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * @inheritdoc
	 * @return Messages|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
