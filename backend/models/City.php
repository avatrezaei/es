<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property integer $province_id
 * @property string $name
 *
 * @property Province $province
 * @property University1[] $university1s
 */
class City extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'city';
	}

	/**
	 * @inheritdoc
	 * @return PlacesQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new CityQuery(get_called_class());
	}

   /**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['province_id', 'name'], 'required'],
			[['province_id'], 'integer'],
			[['name'], 'string', 'max' => 200]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'province_id' => Yii::t('app', 'Province ID'),
			'name' => Yii::t('app', 'Name'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProvince()
	{
		return $this->hasOne(Province::className(), ['id' => 'province_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUniversity1s()
	{
		return $this->hasMany(University1::className(), ['cityId' => 'id']);
	}

	public function getProvinceName()
	{
		return $this->province->name;
	}
}

/**
 * This is the ActiveQuery class for [[Places]].
 *
 * @see Places
 */
class CityQuery extends \yii\db\ActiveQuery
{
	public function state($parent)
	{
		return $this->andWhere(['province_id' => $parent]);
	}

	/**
	 * @inheritdoc
	 * @return Places[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}
}