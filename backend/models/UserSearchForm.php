<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;

/**
 * Send Message Form
 */
class UserSearchForm extends Model
{
	const MALE 		= 'man';
	const FEMALE 	= 'woman';
	const BOTH 		= 'both';

	/**
	 * @var Name
	 * @return string
	 */
	public $name;

	/**
	 * @var Family
	 * @return string
	 */
	public $family;

	/**
	 * @var National Code
	 * @return string
	 */
	public $nationalId;

	/**
	 * @var Student Number
	 * @return string
	 */
	public $studentNumber;

	/**
	 * @var Username
	 * @return string
	 */
	public $username;

	/**
	 * @var E-mail
	 * @return string
	 */
	public $email;

	/**
	 * @var Gender User
	 * @return string
	 */
	public $sex = self::BOTH;

	/**
	 * @var Mobile
	 * @return string
	 */
	public $mobile;

	/**
	 * @var Job Title
	 * @return integer
	 */
	public $title;

	/**
	 * @var University
	 * @return integer
	 */
	public $universityId;

	/**
	 * @var Team
	 * @return integer
	 */
	public $teamId;

	/**
	 * @var Field
	 * @return integer
	 */
	public $fieldId;

	/**
	 * @var unique Code: userId + 1000
	 * @return integer
	 */
	public $uniqueCode;

	/**
	 * @var Array User
	 * @return integer array
	 */
	public $users = [];

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title','universityId','teamId','fieldId'], 'integer'],
			[['name','family','username','email','nationalId','mobile','sex','uniqueCode'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'name' => Yii::t('app', 'Name'),
			'family' => Yii::t('app', 'Family'),
			'username' => Yii::t('app', 'Username'),
			'email' => Yii::t('app', 'Email'),
			'studentNumber' => Yii::t('app', 'Student No'),
			'sex' => Yii::t('app', 'Gender'),
			'mobile' => Yii::t('app', 'Mobile'),
			'nationalId' => Yii::t('app', 'National Id'),
			'title' => Yii::t('app', 'User Title'),
			'universityId' => Yii::t('app', 'Univercity'),
			'teamId' => Yii::t('app', 'Team'),
			'fieldId' => Yii::t('app', 'Field'),
			'uniqueCode' => Yii::t('app', 'Personal Code'),
		];
	}

	public static function itemAlias($type,$code=NULL)
	{
		$_items = [
			'Gender' => [
				self::BOTH => Yii::t('app', 'Not Important'),
				self::MALE => Yii::t('app', 'Male'),
				self::FEMALE => Yii::t('app', 'Female'),
			],
		];
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
}