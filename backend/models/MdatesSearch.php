<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Mdate;

/**
 * MdatesSearch represents the model behind the search form about `app\models\Mdate`.
 */
class MdatesSearch extends Mdate
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['RecID', 'EduYear', 'semester'], 'integer'],
			[['DateType', 'FromDate', 'ToDate'], 'safe'],
            ['FromDate', 'compare', 'compareAttribute' => 'ToDate','message'=>'ee'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Mdate::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
            'RecID' => $this->RecID,
            'FromDate' => $this->FromDate,
            'ToDate' => $this->ToDate,
            'EduYear' => $this->EduYear,
            'semester' => $this->semester,
        ]);

		$query->andFilterWhere(['like', 'DateType', $this->DateType]);

		return $dataProvider;
	}
}
