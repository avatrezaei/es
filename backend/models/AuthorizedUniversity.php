<?php

namespace backend\models;

use Yii;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use backend\modules\YumUsers\models\User;
use backend\modules\YumUsers\models\UsersDetails;

use backend\models\CompetitionParticipant;

/**
 * This is the model class for table "authorized_university".
 *
 * @property integer $id
 * @property integer $eventId
 * @property integer $universityId
 * @property string $quotas
 * @property integer $valid
 */
class AuthorizedUniversity extends \yii\db\ActiveRecord
{
	const ALL_UNIVERSITY = 0;
	
	/* id from user table */
	public $respUserId;
	public $respUserObj;
	
	/**
	 * @return integer Maximum of Medical Staff
	 */
	public $medicalStaff = 0;

	/**
	 * @return integer Maximum of driver
	 */
	public $driver = 0;

	/**
	 * @return integer Maximum of support
	 */
	public $support = 0;

	/**
	 * @return integer Maximum of Vip Staff
	 */
	public $vipStaff = 0;

	/**
	 * @return integer Maximum of Other Staff
	 */
	public $otherStaff = 0;

	/**
	 * @return integer Total fellows
	 */
	public $totalFellows = 0;

	/**
	 * @return bolean has Caravan Supervisor
	 */
	public $hasSupervisor = true;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%authorized_university}}';
	}

	/**
	 * @inheritdoc
	 *
	 * @return ProvinceQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new AuthorizedUniversityQuery(get_called_class());
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['eventId','universityId'], 'required'],
			[['id','eventId','universityId'], 'integer'],
			['quotas', 'string'],
			/*University relation atribute*/
			[['university','userDetailId','quotas'], 'safe']
		];
	}

	public function getUniversity()
	{
		return $this->hasOne(University::className(), ['id' => 'universityId']);
	}

	public function getEvent()
	{
		return $this->hasOne(Event::className(), ['id' => 'eventId']);
	}

	public function getCompetitionParticipants()
	{
		return $this->hasMany(CompetitionParticipant::className(), ['university_id' => 'universityId']);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
				'id' => Yii::t('app', 'ID'),
				'eventId' => Yii::t('app', 'Event'),
				'universityId' => Yii::t('app', 'University'),
				'userDetailId' => Yii::t('app', 'University User'),
				'quotas' => Yii::t('app', 'Medical Staff'),
				'medicalStaff' => Yii::t('app', 'Medical Staff'),
				'driver' => Yii::t('app', 'Driver'),
				'support' => Yii::t('app', 'Support'),
				'vipStaff' => Yii::t('app', 'Vip Staff'),
				'otherStaff' => Yii::t('app', 'Other Staff'),
				'totalFellows' => Yii::t('app', 'Total Fellows'),
				'hasSupervisor' => Yii::t('app', 'Has Caravan Supervisor'),
		];
	}

	public function getUniversityName()
	{
	
		return $this->university->name;
	}

	public function getEventName()
	{
		return $this->event->name;
	}

	public function getQuota()
	{
		$out = NULL;

		/**
		 * Decode Quota field
		 */
		$_extra = [
			'totalFellows' => 0,
			'medicalStaff' => 0,
			'driver' => 0,
			'support' => 0,
			'vipStaff' => 0,
			'otherStaff' => 0,
			'hasSupervisor' => true
		];
		$_temp = Json::decode($this->quotas);

		if(count($_temp) > 0)
			foreach ($_temp as $key => $value) {
				$_extra[$key] = $value;
			}

		if(!is_array($_extra))
			return NULL;

		return $_extra;
	}

	public function setQuota($data)
	{
		/**
		 * Decode Quota field
		 */
		$_extra = [
			'totalFellows' => 0,
			'medicalStaff' => 0,
			'driver' => 0,
			'support' => 0,
			'vipStaff' => 0,
			'otherStaff' => 0,
			'hasSupervisor' => true
		];

		foreach ($_extra as $key => $value) {
			switch ($key) {
				case 'totalFellows':
				case 'medicalStaff':
				case 'driver':
				case 'support':
				case 'vipStaff':
					$_extra[$key] = $data['totalFellows'];
					break;
				
				case 'otherStaff':
					$_extra[$key] =  $data['otherStaff'];
					break;

				case 'hasSupervisor':
					$_extra[$key] =  $data['hasSupervisor'];
					break;
			}
		}

		$this->quotas = Json::encode($_extra);
	}

	public function hasUniversityRespUser($universityId = null, $eventId = null)
	{
		return $this->getUniversityRespUserDetailId();
	}

	public function getUniversityRespUserDetailId($universityId = null, $eventId = null)
	{
		if(isset($this->userDetailId) &&($this->userDetailId)&&$this->valid==1) {
			return $this->userDetailId;
		}
		
		return false;
	}

	/**
	 * @return array object
	 */
	public static function findRemainingUniversity()
	{
		//die(var_dump(University::find()->andWhere(['NOT IN', 'id', AuthorizedUniversity::find()->select(['universityId'])])->all()));
		//die(var_dump(AuthorizedUniversity::find()->select(['universityId'])->all()));
		return University::find()->andWhere(['NOT IN', 'id', AuthorizedUniversity::find()->select(['universityId'])])->all();
	}
	
	/**
	 */
	public function getUniversityRespUserObj()
	{
		if(isset($this->userDetailId))
		{
			$Obj = new UsersDetails();
			$userDetailObj = $Obj->findOne(['id' =>(int) $this->userDetailId, 'valid' => 1]);
			
			if(is_object($userDetailObj)) {
				return($this->respUserObj = $userDetailObj->user);
			}
		}
		
		return null;
	}

	public function getUniversityRespUserInfo()
	{
		$this->getUniversityRespUserObj();
		
		if(is_object($this->respUserObj))
		{
			$userInfo = Html::encode($this->respUserObj->FullName);
		}
		else
		{
			$userInfo = '<i class="fa fa-exclamation-triangle font-yellow"></i>';
		}

		return $userInfo;
	}

	public function getUserId()
	{
		$this->getUniversityRespUserObj();		
		return is_object($this->respUserObj) ? $this->respUserObj->id : 0 ;
	}

	public function getUserRespLastLogintime()
	{
		if(is_object($this->respUserObj))
		{
			if($this->respUserObj->isUserLoginEver()) {
				$userLastLogin = Html::encode($this->respUserObj->getFullJalalyDate('last_login_time'));
			} else {
				return '<i class="fa fa-low-vision font-blue-hoki"></i>';//Yii::t('app', 'Not Login');
			}
		}
		else
		{
			$userLastLogin = '<i class="fa fa-dot-circle-o font-red"></i>';
		}

		return $userLastLogin;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see \yii\db\BaseActiveRecord::save()
	 */
	public function save($runValidation = true, $attributeNames = NULL)
	{
		if($this->isNewRecord)
		{
			$model = $this->findOne(['eventId' => $this->eventId, 'universityId' => $this->universityId]);
			if(is_null($model) || empty($model))
			{
				if(parent::save($runValidation, $attributeNames))
				{
					Yii::$app->session->setFlash('success', Yii::t('app', 'UniversityIsRegistered'));
					return true;
				}
				else
				{
					Yii::$app->session->setFlash('error', Yii::t('app', 'UniversityNotIsRegistered'));
					return false;
				}
			}
			else
			{
				Yii::$app->session->setFlash('warning', Yii::t('app', 'UniversityIsAlreadyRegistered'));
				return true;
			}
		}
		else 
			return parent::save($runValidation,$attributeNames);
	}
}

/**
 * This is the ActiveQuery class for [[Caravan]].
 *
 * @see Event
 */
class AuthorizedUniversityQuery extends \yii\db\ActiveQuery
{
	public function init()
	{
		parent::init();
		return $this->andWhere([AuthorizedUniversity::tableName() . '.valid' => 1]);
	}

	/**
	 *
	 * @param unknown $id			
	 */
	public function university($id)
	{
		return $this->andWhere('universityId', $id);
	}
}