<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_detail".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $shift
 * @property integer $startingyear
 * @property integer $startingsemester
 * @property integer $units
 * @property string $field
 * @property string $major
 */
class UserDetail extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'user_detail';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'shift', 'startingyear', 'startingsemester', 'units', 'field', 'major'], 'required'],
			[['user_id', 'shift', 'startingyear', 'startingsemester', 'units'], 'integer'],
			[['field', 'major'], 'string', 'max' => 52],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'shift' => Yii::t('app', 'Shift'),
			'startingyear' => Yii::t('app', 'Startingyear'),
			'startingsemester' => Yii::t('app', 'Startingsemester'),
			'units' => Yii::t('app', 'Units'),
			'field' => Yii::t('app', 'Field'),
			'major' => Yii::t('app', 'Major'),
		];
	}
}
