<?php

namespace backend\models;

use Yii;


/**
 * This is the model class for table "person".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $student_number
 * @property integer $university_code
 * @property string $starting_year
 * @property string $starting_semester
 * @property string $firstname
 * @property string $lastname
 * @property integer $national_code
 * @property integer $password
 * @property integer $birth_certificate_number
 */
class Person extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'person';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[[ 'student_number', 'university_code', 'starting_year', 'starting_semester', 'firstname', 'lastname', 'national_code', 'password', 'birth_certificate_number'], 'required'],
			[['type', 'student_number', 'university_code', 'national_code', 'password', 'birth_certificate_number'], 'integer'],
			[['starting_year', 'starting_semester'], 'string', 'max' => 32],
			[['firstname', 'lastname'], 'string', 'max' => 52],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'type' => Yii::t('app', 'Type'),
			'student_number' => Yii::t('app', 'Student Number'),
			'university_code' => Yii::t('app', 'University Code'),
			'starting_year' => Yii::t('app', 'Starting Year'),
			'starting_semester' => Yii::t('app', 'Starting Semester'),
			'firstname' => Yii::t('app', 'Firstname'),
			'lastname' => Yii::t('app', 'Lastname'),
			'national_code' => Yii::t('app', 'National Code'),
			'password' => Yii::t('app', 'Password'),
			'birth_certificate_number' => Yii::t('app', 'Birth Certificate Number'),
			'fullname'=>Yii::t('app', 'Payer Name'),
			'university'=>Yii::t('app', 'University'),
		];
	}



	public function getUniversity()
	{
		return $this->hasOne(University::className(), ['id' => 'university_code']);
	}

	public function getMemberPayment()
	{
		return $this->hasMany(MemberPayment::className(), ['personId' => 'id']);
	}


	public function getUniversityname()
	{
		if($this->university) return $this->university->name;
		return Yii::t('app','Unkonwn');
	}


	public function getFullName()
	{
		return $this->firstname." ".$this->lastname;

	}

 


	public function getWitingCount()
	{
		$_sum = 0;
		$_payments = $this->waitingPayment;

		if($_payments !== NULL) {
			$_sum = $_payments->sumPayment > 0 ? \common\components\Rezvan::toCurrency($_payments->sumPayment) : 0;
		}

		return $_sum;
		// return count($this->waitingPayment);
	}


	public function getTotalBillApproved()
	{
		$_sumPayment = 0;
		$_allPayment = $this->approvedPayment;

		if($_allPayment !== NULL) {
			$_sumPayment = $_allPayment->sumPayment > 0 ? \common\components\Rezvan::toCurrency($_allPayment->sumPayment) : 0;
		}

		return $_sumPayment;
	}

	


	public function getWaitingPayment()
	{
		return $this->hasOne(MemberPayment::className(), ['personId' => 'id'])->sumAmount()->waiting();
	}

	public function getApprovedPayment()
	{
		return $this->hasOne(MemberPayment::className(), ['personId' => 'id'])->sumAmount();
	}
}
