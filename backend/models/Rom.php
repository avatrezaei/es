<?php

namespace backend\models;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "rom".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property integer $capacity
 * @property integer $enable
 * @property integer $selected
 */
class Rom extends \yii\db\ActiveRecord
{
	
	 public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'name', 'capacity'], 'required'],
            [['id', 'parent_id', 'capacity', 'enable', 'selected'], 'integer'],
            [['name'], 'string', 'max' => 20],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'شناسه'),
		'name' => Yii::t('app', 'نام'),
		'parent_id' => Yii::t('app', 'سوئیت'),
		'capacity' => Yii::t('app', 'ظرفیت'),
		'enable' => Yii::t('app', 'فعال'),
		'selected' => Yii::t('app', 'انتخاب شده'),
		'image' => Yii::t('app', 'انتخاب تصویر'),
        ];
    }
}
