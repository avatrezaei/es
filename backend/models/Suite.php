<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "suite".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $capacity
 * @property integer $enable
 * @property integer $selected
 */
class Suite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'parent_id', 'capacity'], 'required'],
            [['id', 'parent_id', 'capacity', 'enable', 'selected'], 'integer'],
            [['name'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
		'id' => Yii::t('app', 'شناسه'),
		'name' => Yii::t('app', 'نام'),
		'parent_id' => Yii::t('app', 'طبقه'),
		'capacity' => Yii::t('app', 'ظرفیت'),
		'enable' => Yii::t('app', 'فعال'),
		'selected' => Yii::t('app', 'انتخاب شده'),

        ];
    }
}
