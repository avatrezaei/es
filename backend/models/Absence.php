<?php

namespace app\models;

use backend\models\User;
use Yii;
use backend\models\jdf;
/**
 * This is the model class for table "absence".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property string $description
 * @property string $reason_of_absence
 */
class Absence extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'absence';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'date'], 'required'],
			[['user_id'], 'integer'],
			[['date'], 'safe'],
			[['description','reason_of_absence'], 'string', 'max' => 255],
		];
	}
    public function dateDisplayFormat($date=null)
    {
        //$date=$this->from_date;

        if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if (preg_match('/^(12[0-9][0-9]|13[0-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {

            return $date;
        } else if ($date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '0000-00-00';
        }
        return $date = $this->getJalalyDate($date);
        //return $date;
    }
    public function getJalalyDate($date) {
        $jdf = new jdf();
        $jDateString = $jdf->jdate('Y-m-d', strtotime($date));
        return $jDateString;
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getReason($date)
    {
        if($date=='1')
            return 'علت اول';
        else if($date=='2')
            return 'علت دوم';
        else if($date=='3')
            return 'علت سوم';
        else if($date=='4')
            return 'علت چهارم';
        else if($date=='5')
            return 'علت پنجم';
    }
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'Students'),
			'date' => Yii::t('app', 'Date'),
			'description' => Yii::t('app', 'Comment'),
			'reason_of_absence' => Yii::t('app', 'Absence Reason'),
		];
	}
}
