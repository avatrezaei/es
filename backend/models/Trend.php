<?php

namespace backend\models;

use Yii;
use backend\models\Field;
/**
 * This is the model class for table "trend".
 *
 * @property integer $id
 * @property integer $fieldId
 * @property string $name
 * @property string $ename
 */
class Trend extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'trend';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['fieldId', 'name', 'ename'], 'required'],
			[['fieldId'], 'integer'],
			[['name', 'ename'], 'string', 'max' => 52],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'fieldId' => Yii::t('app', 'Field Name'),
			'name' => Yii::t('app', 'Name'),
			'ename' => Yii::t('app', 'Ename'),
		];
	}


	/**
	 *
	 * get field
	 *
	 */
	public function getField(){

		return $this->hasOne(Field::className(), ['id' => 'fieldId']);
	}
	/**
	 *
	 * get field name
	 *
	 */
	public function getFieldName(){
		if($this->field)
			return $this->field->name;
		return Yii::app('app','Unknow');
	}
	
	
}
