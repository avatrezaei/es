<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MemberPayment;

/**
 * MemberPaymentSearch represents the model behind the search form about `app\models\MemberPayment`.
 */
class MemberPaymentSearch extends MemberPayment
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'personId', 'paymentNo', 'amount', 'valid'], 'integer'],
			//[['PayerName', 'paidDate', 'insertDate', 'lastUpdateDate', 'statusChangeDate'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = MemberPayment::find();

		// add conditions that should always apply here
		$query->joinWith(['user']);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
            'id' => $this->id,
            'personId' => $this->personId,
            'paymentNo' => $this->paymentNo,
            'amount' => $this->amount,
            'valid' => $this->valid,
        ]);

		$query->andFilterWhere(['like', 'paymentType', $this->paymentType])
            ->andFilterWhere(['like', 'paymentTime', $this->paymentTime]);

		return $dataProvider;
	}
}
