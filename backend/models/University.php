<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "university".
 *
 * @property integer $id
 * @property string $name
 * @property string $eName
 * @property integer $universityType
 * @property integer $cityId
 * @property integer $provinceId
 * @property string $preffixName
 * @property string $address
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $url
 * @property string $level
 *
 * @property City $city
 * @property Province $province
 */
class University extends \yii\db\ActiveRecord
{
	const GOVERNMENT_UNIVERSITYTYPE = 'government';
	const AZAD_UNIVERSITYTYPE = 'azad';
	const PROFIT_UNIVERSITYTYPE = 'profit';
	const APPLIED_UNIVERSITYTYPE = 'applied';
	const OTHER_UNIVERSITYTYPE = 'other';

	const LevelOne   = 1;
	const LevelTwo   = 2;
	const LevelThree = 3;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%university}}';
	}

	/**
	 * @inheritdoc
	 * 
	 * @return PlacesQuery the active query used by this AR class.
	 */
	public static function find(){
		return new UniversityQuery(get_called_class());
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name','universityType','preffixName'],'required'],
			[['cityId','provinceId'],'integer'],
			['universityType','in','range' => ['government','azad','profit','applied','other']],
		 	['level','in','range' => [1,2,3,4]],
		 	[['name','eName','email'],'string','max' => 70],
			['email','email'],
			['preffixName','unique'],
			[['preffixName' ],'string','max' => 10],
			[['address' ],'string','max' => 400],
			[['phone' ],'string','max' => 50],
			[['fax' ],'string','max' => 20],
			[['url' ],'string','max' => 100],
			[['preffixName','eName'],'match','pattern' => '/^[a-z A-Z]+$/','message' => 'تنها از کاراکترهای انگلیسی استفاده شود. '],
			[['preffixName'],'string','max' => 15,'min' => 2],
			[['phone','fax'],'match','pattern' => '/^[0-9-_]+$/','message' => 'تنها از کاراکترهای عددی و -استفاده شود. '] 
		];
	}



	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios ['editable'] = [];
		return $scenarios;
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels(){
		return [
			'id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'eName' => Yii::t('app', 'English Name'),
			'universityType' => Yii::t('app', 'University Type'),
			'cityId' => Yii::t('app', 'City'),
			'provinceId' => Yii::t('app', 'Province'),
			'preffixName' => Yii::t('app', 'Preffix Name'),
			'address' => Yii::t('app', 'Address'),
			'phone' => Yii::t('app', 'Phone'),
			'fax' => Yii::t('app', 'Fax'),
			'email' => Yii::t('app', 'Email'),
			'url' => Yii::t('app', 'Url'),
			'level' => Yii::t('app', 'Level'),
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCity()
	{
		return $this->hasOne(City::className(), ['id' => 'cityId']);
	}
	
	 

	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getAuthorizedUniversity()
	{
		return $this->hasMany(AuthorizedUniversity::className(), ['universityId' => 'id']);
	}

	public function getAuthUniversity()
	{
		return $this->hasOne(AuthorizedUniversity::className(), ['universityId' => 'id'])->andWhere(['eventId' => Yii::$app->user->identity->eventId]);
	}

	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCaravan()
	{
		return $this->hasMany(Caravan::className(), ['universityId' => 'id']);
	}

	public static function Name($id = NULL)
	{
		if(is_null($id))
			return Yii::t('zii', 'Not set');
		
		$university = self::findOne($id);
		return(is_null($university))? Yii::t('zii', 'Not set'): $university->name;
	}

	/**
	 *
	 * @param unknown $type			
	 * @param unknown $code			
	 */
	public static function itemAlias($type, $code = NULL)
	{
		$_items = [
			'Type' => [
				self::GOVERNMENT_UNIVERSITYTYPE => Yii::t('app', 'Government'),
				self::AZAD_UNIVERSITYTYPE => Yii::t('app', 'Azad'),
				self::PROFIT_UNIVERSITYTYPE => Yii::t('app', 'Profit'),
				self::APPLIED_UNIVERSITYTYPE => Yii::t('app', 'Applied'),
				self::OTHER_UNIVERSITYTYPE => Yii::t('app', 'Other')
			],
			'Level' => [
				self::LevelOne => Yii::t('app', 'Level One'),
				self::LevelTwo => Yii::t('app', 'Level Two'),
				self::LevelThree => Yii::t('app', 'Level Three'),
			],
		];
		if(isset($code))
			return isset($_items [$type] [$code])? $_items [$type] [$code] : false;
		else
			return isset($_items [$type])? $_items [$type] : false;
	}

	public function getType()
	{
		$class = '';

		switch($this->universityType)
		{
			case self::GOVERNMENT_UNIVERSITYTYPE :
				$class = 'success';
				break;

			case self::AZAD_UNIVERSITYTYPE :
				$class = 'warning';
				break;

			case self::PROFIT_UNIVERSITYTYPE :
				$class = 'danger';
				break;

			case self::APPLIED_UNIVERSITYTYPE :
				$class = 'info';
				break;

			case self::OTHER_UNIVERSITYTYPE :
				$class = 'default';
				break;
		}

		return '<span class="label label-' . $class . '">' . self::itemAlias('Type', $this->universityType). '</span>';
	}

	public function getLevel()
	{
		$class = '';

		switch($this->level)
		{
			case self::LevelOne :
				$class = 'success';
				break;

			case self::LevelTwo :
				$class = 'warning';
				break;

			case self::LevelThree :
				$class = 'danger';
				break;

			default:
				$class = 'default';
		}

		return '<span class="label label-' . $class . '">' . self::itemAlias('Level', $this->level). '</span>';
	}

	public function getCityName()
	{
		return isset($this->city)? $this->city->name : Yii::t('zii', 'Not set');
	}

	public function getStateName()
	{
		return isset($this->province)? $this->province->name : Yii::t('zii', 'Not set');
	}

	public function getAttributeText($attribute)
	{
		$Text =(strlen($this->$attribute)> 0)?($this->$attribute):('-----');
		return $Text;
	}

	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert)){
			if(! $insert){
				if($this->scenario == 'editable'){
					/**
					 * Get the attribute values that have been modified
					 */
					$dirty = $this->getDirtyAttributes();
					
					/**
					 * Check the attribute values that have been modified is cityId
					 */
					if(array_key_exists('cityId', $dirty)){
						/**
						 * Get Selected City Information
						 */
						$city = City::find()->where([
								'id' => $this->cityId 
						])->one();
						if($city !== NULL){
							/**
							 * Set State Id in university detail
							 */
							$this->provinceId = $city->province_id;
						} else {
							return false;
						}
					}
				}
			}

			/**replace arabic character by persian**/
			$arabicChar = array('ي','ك');
			$persianChar = array('ی','ک');
			$this->name = str_replace($arabicChar, $persianChar, $this->name);
			$this->address = str_replace($arabicChar, $persianChar, $this->address);
			return true;
		} else {
			return false;
		}
	}
}

/**
 * This is the ActiveQuery class for [[Places]].
 *
 * @see Places
 */
class UniversityQuery extends \yii\db\ActiveQuery
{
	public function init()
	{
		parent::init();
		return $this->andWhere([University::tableName(). '.valid' => 1]);
	}
	
	public function city($id)
	{
		return $this->andWhere(['cityId' => $id]);
	}
	
	/**
	 * @inheritdoc
	 * 
	 * @return Places[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}
}
