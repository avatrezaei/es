<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tree".
 *
 * @property string $RecID
 * @property string $ParentID
 * @property string $PlaceName
 * @property integer $enable
 * @property integer $sex
 * @property integer $type
 * @property integer $EduSecCode
 */
class Tree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ParentID', 'enable', 'sex', 'type', 'EduSecCode'], 'integer'],
            [['PlaceName'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'RecID' => Yii::t('app', 'Rec ID'),
            'ParentID' => Yii::t('app', 'Parent ID'),
            'PlaceName' => Yii::t('app', 'Place Name'),
            'enable' => Yii::t('app', 'Enable'),
            'sex' => Yii::t('app', 'Sex'),
            'type' => Yii::t('app', 'Type'),
            'EduSecCode' => Yii::t('app', 'Edu Sec Code'),
        ];
    }
}
