<?php

$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/../../common/config/params-local.php'),
	require(__DIR__ . '/params.php')
);

return [
	'id' => 'fum-api',
	'basePath' => dirname(__DIR__),	
	'bootstrap' => ['log'],
	'modules' => [
		'v1' => [
			'basePath' => '@app/modules/v1',
			'class' => 'api\modules\v1\Module',
		]
	],
	'components' => [		
		'i18n' => [
			'translations' => [
				'app*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages',
					'sourceLanguage' => 'fa',
					'fileMap' => [
						'app' => 'app.php',
					],
				],
			],
		],

		'user' => [
			'identityClass' => 'api\modules\v1\models\Agents',
			'enableAutoLogin' => false,
			'loginUrl' => NULL,
		],

		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],

		'sms' =>[
			'class' => 'faravaghi\rezvansms\Rezvansms',
			'login' => 'msport', // Username
			'password' => '35373537', // Password
			'url' => 'http://sms.3300.ir/almassms.asmx?wsdl',
		],

		'urlManager' => [
			'enablePrettyUrl' => true,
			'enableStrictParsing' => true,
			'showScriptName' => false,
			'rules' => [
				[
					'class' => 'yii\rest\UrlRule',
					'controller' => 'v1/agent',
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'POST access' => 'access',
						'POST validate' => 'validate',
					]
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'v1/profile',
					'tokens' => [
						'{id}' => '<id:\\w+>'
					],
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'POST me' => 'me',
						'POST events' => 'events',
						'POST team' => 'team',
						'POST rival' => 'rival',
						'POST set-token' => 'set-token',
					]
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'v1/transport',
					'tokens' => [
						'{id}' => '<id:\\w+>'
					],
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'POST dailyservice' => 'dailyservice',
						'POST freetime' => 'freetime',
						'POST stations' => 'stations',
					]
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'v1/nutrition',
					'tokens' => [
						'{id}' => '<id:\\w+>'
					],
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'POST mymeal' => 'mymeal',
						'POST meal' => 'meal',
						'POST selves' => 'selves',
					]
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'v1/seat',
					'tokens' => [
						'{id}' => '<id:\\w+>'
					],
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'POST places' => 'places',
						'POST place' => 'place',
					]
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'v1/fields',
					'tokens' => [
						'{id}' => '<id:\\w+>'
					],
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'POST all' => 'all',
						'POST subfields' => 'subfields',
					]
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'v1/competitions',
					'tokens' => [
						'{id}' => '<id:\\w+>'
					],
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'POST rankingpoints' => 'rankingpoints',
						'POST rankingmedals' => 'rankingmedals',
						'POST games' => 'games',
					]
				],
				[
					'class' => 'yii\rest\UrlRule',
					'pluralize' => false,
					'controller' => 'v1/match',
					'except' => ['index', 'view', 'create', 'update', 'options', 'delete'],
					'extraPatterns' => [
						'GET rankingpoints' => 'rankingpoints',
						'GET rankingmedals' => 'rankingmedals',
						'GET games' => 'games',
					]
				],
			],
		]
	],
	'params' => $params,
];



