<?php
namespace api\modules\v1\models;

use yii\helpers\Url;
use yii\db\ActiveRecord;

class Subfields extends \backend\models\Subfields
{
	public function fields()
	{
		return [
			'id',
			'name',
		];
	}
}