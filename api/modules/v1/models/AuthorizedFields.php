<?php
namespace api\modules\v1\models;

use yii\helpers\Url;
use yii\db\ActiveRecord;

class AuthorizedFields extends \backend\models\AuthorizedFields
{
	public function fields()
	{
		return [
			'id' => 'fieldsId',
			'name' => function(){
				return $this->FieldName;
			},
		];
	}
}