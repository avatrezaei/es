<?php
namespace api\modules\v1\models;

use yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use common\components\Rezvan;
use backend\modules\YumUsers\models\User;

class Agents extends ActiveRecord implements IdentityInterface
{
	const ACCESS_CODE_LENGHT = 5;

	const STATUS_BANNED		= -1;
	const STATUS_REGISTER	= 1;
	const STATUS_ACTIVE		= 10;

	public static function tableName()
	{
		return '{{%user}}';
	}

	public static function find()
	{
		return new AgentsQuery(get_called_class());
	}

	/**
	 * Finds an identity by the given ID.
	 *
	 * @param string|integer $id the ID to be looked for
	 * @return IdentityInterface|null the identity object that matches the given ID.
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Finds an identity by the given token.
	 *
	 * @param string $token the token to be looked for
	 * @return IdentityInterface|null the identity object that matches the given token.
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne(['agentUID' => $token, 'agentStatus' => self::STATUS_ACTIVE]);
	}

	/**
	 * @return int|string current user ID
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string current user auth key
	 */
	public function getAuthKey()
	{
		return $this->agentUID;
	}

	/**
	 * @param string $authKey
	 * @return boolean if auth key is valid for current user
	 */
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Get event id from user session
	 */
	public function getEventId()
	{
		return Yii::$app->session->get('eventId');
	}

	/**
	 * Set event id to user session
	 */
	public function setEventId($eventId)
	{
		return Yii::$app->session->set('eventId', $eventId);
	}

	/**
	 * Generates new Id for edit object
	 */
	public function generateAccessCode()
	{
		do {
			/**
			 * Generate new token
			 */
			$_id = Rezvan::randCode(self::ACCESS_CODE_LENGHT, false);
		}
		while ((int)$_id[0] == 0);

		/**
		 * Check if id not exist
		 */

		$this->agentActiveCode = $_id;
	}
}

class AgentsQuery extends \yii\db\ActiveQuery
{
	public function phone($phone)
	{
		return $this->andWhere(['mobile' => $phone]);
	}

	public function identify($id)
	{
		return $this->andWhere(['agentUID' => $id]);
	}

	public function token($googleCode)
	{
		return $this->andWhere(['googleToken' => $googleCode]);
	}

	public function code($accessCode)
	{
		return $this->andWhere(['agentActiveCode' => $accessCode]);
	}

	public function active()
	{
		return $this->andWhere(['agentStatus' => Agents::STATUS_ACTIVE]);
	}
}