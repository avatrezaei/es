<?php
namespace api\modules\v1\models;

use yii\helpers\Url;
use yii\db\ActiveRecord;

class Games extends \backend\models\Game
{
	public function fields()
	{
		return [
			'fieldId' => function(){
				return $this->competition->field_id;
			},
			'fieldname' => function(){
				return $this->competition->FieldName;
			},
			'subfieldname' => function(){
				return $this->competition->SubfieldName;
			},
			'date',
			'round' => 'round_id',
			'statusCode' => 'status',
			'group' => function(){
				return $this->group;
			},
			'statusText' => function(){
				return $this->StatusText;
			},
			'date',
			'starttime' => function(){
				return $this->begin_time != NULL ? \Yii::$app->formatter->asTime($this->begin_time) : NULL;
			},
			'endtime' => function(){
				return $this->end_time != NULL ? \Yii::$app->formatter->asTime($this->end_time) : NULL;
			},
			'participants' => function(){
				return $this->participants;
			},
			'location' => function(){
				return $this->location;
			},
		];
	}
}