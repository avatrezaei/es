<?php
namespace api\modules\v1\models;

use yii\helpers\Url;
use yii\db\ActiveRecord;

class RankingPoints extends \backend\models\UniversityRanking
{
	public function fields()
	{
		return [
			'universityId' => 'university_id',
			'name' => function(){
				return $this->university->name;
			},
			'rank1',
			'rank2',
			'rank3',
			'rank4',
			'rank5',
			'rank6',
			'point',
			'rank',
		];
	}
}