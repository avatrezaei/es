<?php
namespace api\modules\v1\models;

use yii\helpers\Url;
use yii\db\ActiveRecord;

class RankingMedals extends \backend\models\UniversityRanking
{
	public function fields()
	{
		return [
			'universityId' => 'university_id',
			'name' => function(){
				return $this->university->name;
			},
			'gold' => 'rank1',
			'silver' => 'rank2',
			'bronze' => 'rank3',
		];
	}
}