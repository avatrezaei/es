<?php
namespace api\modules\v1\models;

use yii\helpers\Url;

use yii\db\ActiveRecord;
use backend\models\Eskan;
use backend\models\EventMembers;

use backend\modules\YiiFileManager\models\File;
use backend\modules\YiiFileManager\models\Folders;

class User extends \backend\modules\YumUsers\models\User
{
	public function getAvatar()
	{
		$filemodel = (new \yii\db\Query())
				->from('file')
				->where(['id' => $this->fileId])
				->one();

		if ($filemodel !== null) {
			return 'https://sems.um.ac.ir' . \Yii::$app->urlManager->createUrl([ '/YiiFileManager/file/view-file', 'name' => $filemodel['hashed_name']], true);
		}

		return false;
	}

	public function getMyPlace()
	{
		/**
		 * Find Caravan ID
		 */
		$_event = EventMembers::find()->user($this->id)->events(\Yii::$app->user->identity->eventId)->one();

		/**
		 * Find Eskan
		 */
		$model = Eskan::find()->andWhere(['caravan_id' => $_event->caravanId])->groupBy(['place_id', 'subset_id'])->all();

		return $model;
	}

	public function fields()
	{
		return [
			'name' => 'name',
			'family' => 'last_name',
			'fatherName',
			'idNo',
			'nationalId',
			'birthDate',
			'email',
			'section' => function () {
				return $this->UserDetailInfo->EducationalSectionNameText;
			},
			'group' => function () {
				return $this->UserDetailInfo->EducationalGroupsNameText;
			},
			'university' => function () {
				return $this->UserDetailInfo->UniversityName;
			},
			'sportfield' => function () {
				$_fieldName = $this->eventMembersByEvent->FieldName;
				if($_fieldName == '--')
					return '';
				else
					return $_fieldName;
			},
			'avatar' => function () {
				return $this->getAvatar();
			},
			'place' => function () {
				return $this->MyPlace;
			},
		];
	}
}