<?php
namespace api\modules\v1;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'api\modules\v1\controllers';

	public function init()
	{
		parent::init();
		\Yii::$app->user->enableSession = false;


		\Yii::$app->i18n->translations['app'] = [
			'sourceLanguage' => 'fa',
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@backend/messages',
		];
	}
}
