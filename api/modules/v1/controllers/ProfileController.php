<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use yii\db\Query;
use yii\data\Pagination;

use api\modules\v1\models\User;
use backend\models\Teams;
use backend\models\Event;
use backend\modules\YumUsers\models\UsersDetails;

class ProfileController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\User';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}

	/*public function actions()
	{
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}*/

	public function actionSetToken()
	{
		$request = Yii::$app->request;
		$uid = $request->getAuthUser();
		$params = $request->post();
		$result = [];

		if(!isset($uid) || !isset($params['token'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('api', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NULL;

		/**
		 * Get User model
		 */
		$user = User::find()->where(['agentUID' => $uid])->one();

		if($user === NULL){
			$result['message'] = Yii::t('api', 'User not found!');
		}
		else{
			$user->googleToken = $params['token'];
			if(!$user->update(false)){
				$result['message'] = Yii::t('api', 'Error in save google token');
			}
		}

		return $result;
	}

	public function actionMe()
	{
		$request = Yii::$app->request;
		$uid = $request->getAuthUser();
		$params = $request->post();
		$result = [];

		if(!isset($uid) || !isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('api', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = User::find()->where(['agentUID' => $uid])->one();

		return $result;
	}

	public function actionEvents()
	{
		$request = Yii::$app->request;
		$uid = $request->getAuthUser();
		$result = [];

		if(!isset($uid)){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('api', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}
		
		$user = User::find()->where(['agentUID' => $uid])->one();
		$userId = $user->id;

		$userDetail = UsersDetails::find()->select('id')->user($userId)->all();

		$_userDetailIds = NULL;
		foreach ($userDetail as $data) {
			$_userDetailIds[] = $data->id;
		}

		$eventsInfo = Event::find()->joinWith(['eventmember'])->where(['usersDetailsId' => $_userDetailIds])->sortByDate()->all();

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = $eventsInfo;

		return $result;
	}

	public function actionTeam()
	{
		$request = Yii::$app->request;
		$uid = $request->getAuthUser();
		$params = $request->post();
		$result = [];

		if(!isset($uid) || !isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$me = User::find()->where(['agentUID' => $uid])->one();

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = Teams::find()->joinWith('teamMembers')->andWhere(['usersDetailsId' => $me->UserDetailInfo->id])->one();

		return $result;
	}

	public function actionRival()
	{
		$request = Yii::$app->request;
		$uid = $request->getAuthUser();
		$params = $request->post();
		$result = [];

		if(!isset($uid) || !isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$me = User::find()->where(['agentUID' => $uid])->one();
		$_team = Teams::find()->joinWith('teamMembers')->andWhere(['usersDetailsId' => $me->UserDetailInfo->id])->one();
		$_rivals = Teams::find()->joinWith('teamMembers')->rival($_team->id, $_team->fieldId)->all();

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = $_rivals;

		return $result;
	}
}