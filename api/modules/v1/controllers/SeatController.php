<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use yii\db\Query;
use yii\data\Pagination;

use backend\models\Eskan;
use backend\models\Place;
use api\modules\v1\models\User;
use backend\models\EventMembers;

class SeatController extends ActiveController
{
	public $modelClass = 'backend\models\Eskan';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}

	/*public function actions()
	{
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}*/

	public function actionPlace()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$uid = $request->getAuthUser();
		$result = [];

		if(!isset($uid) || !isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('api', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		$eventId = $params['eventId'];
		Yii::$app->user->identity->eventId = $eventId;

		/**
		 * Get User Detail
		 */
		$_user = User::find()->where(['agentUID' => $uid])->one();

		/**
		 * Find Caravan ID
		 */
		$_event = EventMembers::find()->user($_user->id)->events($eventId)->one();

		/**
		 * Find Eskan
		 */
		$model = Eskan::find()->andWhere(['caravan_id' => $_event->caravanId])->groupBy(['place_id', 'subset_id'])->all();

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = $model;

		return $result;
	}

	public function actionPlaces()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		/**
		 * Find Places
		 */
		$model = Place::find()->all();

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = $model;

		return $result;
	}
}