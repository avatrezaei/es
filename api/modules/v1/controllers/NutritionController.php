<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use yii\db\Query;
use yii\data\Pagination;

use api\modules\v1\models\User;
use backend\models\NutritionUsers;
use backend\models\NutritionMeal;
use backend\models\NutritionSelf;
use backend\models\NutritionAllotment;

class NutritionController extends ActiveController
{
	public $modelClass = 'backend\models\NutritionUsers';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['selves'],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}

	public function actionMeal()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NutritionMeal::find()->active()->all();

		return $result;
	}

	public function actionMymeal()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$uid = $request->getAuthUser();
		$result = [];

		if(!isset($uid) || !isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('api', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		$eventId = $params['eventId'];
		Yii::$app->user->identity->eventId = $eventId;

		/**
		 * Get User Detail
		 */
		$_user = User::find()->where(['agentUID' => $uid])->one();

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NutritionAllotment::find()->user($_user->id)->active()->sortDate()->all();

		return $result;
	}

	public function actionSelves()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NutritionSelf::find()->active()->all();

		return $result;
	}
}