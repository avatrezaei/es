<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use yii\db\Query;
use yii\data\Pagination;

use backend\models\Station;
use backend\models\Transport;
use api\modules\v1\models\User;

class TransportController extends ActiveController
{
	public $modelClass = 'backend\models\Transport';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}

	/*public function actions()
	{
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}*/

	public function actionDailyservice()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = Transport::find()->dailyService()->active()->all();

		return $result;
	}

	public function actionFreetime()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = Transport::find()->freeTime()->active()->all();

		return $result;
	}

	public function actionStations()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = Station::find()->active()->all();

		return $result;
	}
}