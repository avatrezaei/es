<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use yii\db\Query;
use yii\data\Pagination;

use api\modules\v1\models\Games;
use api\modules\v1\models\RankingPoints;
use api\modules\v1\models\RankingMedals;
use backend\models\Competition;

class CompetitionsController extends ActiveController
{
	public $modelClass = 'backend\models\Competition';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			// 'except' => ['view'],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}

	/*public function actions()
	{
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}*/

	public function actionRankingpoints()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = RankingPoints::find()->joinWith(['university'])->orderPoint()->all();

		return $result;
	}

	public function actionRankingmedals()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NULL; // RankingMedals::find()->joinWith(['university'])->orderRank()->all();

		return $result;
	}

	public function actionGames()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['fieldId']) || !isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		Yii::$app->user->identity->eventId = $params['eventId'];

		$result['success'] = true;
		$result['message'] = NULL; 

		$_competition = Competition::find()->select(['id'])->field($params['fieldId']);
		if(isset($params['subfieldId']))
			$_competition->subfield($params['subfieldId']);

		if($_competition === NULL){
			$result['data'] = NULL;
		}
		else{
			$query = Games::find()
					->joinWith(['competition'])
					->andWhere(['competition_id' => $_competition])
					->andWhere(['<>', 'bye', 1])
					->orderBy(['date' => SORT_ASC, 'begin_time' => SORT_ASC]);

			$result['data'] = $query->all();
		}

		return $result;
	}
}