<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use yii\db\Query;
use yii\data\Pagination;

use api\modules\v1\models\Games;
use api\modules\v1\models\RankingPoints;
use api\modules\v1\models\RankingMedals;
use backend\models\Competition;
use backend\models\Event;

class MatchController extends ActiveController
{
	public $modelClass = 'backend\models\Competition';

	public function actionRankingpoints()
	{
		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NULL;

		$request = Yii::$app->request;
		$params = $request->get();

		if(!isset($params['eventId'])){
			$eventId = 5;
		}
		else{
			$eventId = $params['eventId'];
		}

		/**
		 * set eventId to session
		 */
		Yii::$app->session->set('eventId', $eventId);

		$result['data'] = RankingPoints::find()->joinWith(['university'])->orderPoint()->all();
		return $result;
	}

	public function actionRankingmedals()
	{
		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NULL;

		return $result;
		$request = Yii::$app->request;
		$params = $request->get();

		if(!isset($params['eventId'])){
			$eventId = 5;
		}
		else{
			$eventId = $params['eventId'];
		}

		/**
		 * set eventId to session
		 */
		Yii::$app->session->set('eventId', $eventId);

		$result['data'] = RankingMedals::find()->joinWith(['university'])->orderRank()->all();

		return $result;
	}

	public function actionGames()
	{
		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = NULL;

		$request = Yii::$app->request;
		$params = $request->get();

		if(!isset($params['fieldId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		if(!isset($params['eventId'])){
			$eventId = 5;
		}
		else{
			$eventId = $params['eventId'];
		}

		/**
		 * set eventId to session
		 */
		Yii::$app->session->set('eventId', $eventId);

		$_competition = Competition::find()->select(['id'])->field($params['fieldId']);
		if(isset($params['subfieldId']))
			$_competition->subfield($params['subfieldId']);

		if($_competition === NULL){
			$result['data'] = NULL;
		}
		else{
			$query = Games::find()
					->joinWith(['competition'])
					->andWhere(['competition_id' => $_competition])
					->andWhere(['<>', 'bye', 1])
					->orderBy([Games::tableName().'.status' => SORT_DESC,'date' => SORT_DESC, 'begin_time' => SORT_ASC]);

			$result['data'] = $query->all();
		}

		return $result;
	}

	protected function thisEvent()
	{
		$event = Event::find()->currentstart()->one();
		if($event != NULL)
			return $event->id;

		return 0;
	}
}