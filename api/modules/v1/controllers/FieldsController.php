<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

use yii\db\Query;
use yii\data\Pagination;

use api\modules\v1\models\AuthorizedFields;
use api\modules\v1\models\Subfields;

class FieldsController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\AuthorizedFields';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'except' => ['all','subfields'],
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
				QueryParamAuth::className(),
			],
		];
		return $behaviors;
	}

	public function actions()
	{
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}

	public function actionAll()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		/**
		 * set eventId to session
		 */
		Yii::$app->session->set('eventId', $params['eventId']);

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = AuthorizedFields::find()->thisEvent()->AllUniversity()->all();

		return $result;
	}

	public function actionSubfields()
	{
		$request = Yii::$app->request;
		$params = $request->post();
		$result = [];

		if(!isset($params['eventId']) || !isset($params['fieldId'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('app', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		/**
		 * set eventId to session
		 */
		Yii::$app->session->set('eventId', $params['eventId']);

		$query = Subfields::find()->field($params['fieldId']);

		/**
		 * 
		 */
		if($params['eventId'] == 1){
			$query->gender(Subfields::GENDER_WOMAN);
		}
		else if($params['eventId'] == 5){
			$query->gender(Subfields::GENDER_MAN);
		}

		$result['success'] = true;
		$result['message'] = NULL;
		$result['data'] = $query->all();

		return $result;
	}
}