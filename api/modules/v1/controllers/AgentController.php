<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;

use yii\rest\ActiveController;

use faravaghi\rezvansms\Rezvansms;

use backend\models\Messages;
use backend\models\Event;
use backend\models\EventMembers;
use api\modules\v1\models\Agents;

class AgentController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\Agents';

	public function behaviors()
	{
		return ArrayHelper::merge([
			[
				'class' => Cors::className(),
				'cors' => [
					'Origin' => ['*'],
					'Access-Control-Request-Method' => ['POST'],
				],
			],
		], parent::behaviors());
	}

	public function actionValidate()
	{
		$request = Yii::$app->request;
		$params = $request->post();

		$result = ['success' => false, 'message' => Yii::t('app', 'System error occurred, please try again.')];

		if(!isset($params['code']) || !isset($params['uid'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('api', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		/**
		 * Check Mobile validat
		 */
		$model = Agents::find()->code($params['code'])->identify($params['uid'])->one();
		if($model !== NULL){
			if($model->agentStatus == Agents::STATUS_BANNED){
				return [
					'success' => false,
					'name' => 'User Banned',
					'message' => Yii::t('app', 'The user is banned.'),
					'code' => 0,
					'status' => 401,
				];
			}

			$model->agentStatus = Agents::STATUS_ACTIVE;
			$model->agentActiveCode = NULL;

			if($model->save()){
				$result['success'] = true;
				$result['message'] = NULL;

				/**
				 * Get curent Event for this user
				 */
				$currentEventId = Event::find()->select('id')->current();

				/**
				 * Get UserDetail Id on event
				 */
				$_event = EventMembers::find()->user($model->id)->events($currentEventId)->one();

				/**
				 * set eventId to session
				 */
				Yii::$app->session->set('eventId', $_event->eventId);

				$result['data'] = $_event;

				return $result;
			}
		}
		else{
			$result['success'] = false;
			$result['message'] = Yii::t('app', 'Invalid parameters');
		}

		return $result;
	}

	public function actionAccess()
	{
		$request = Yii::$app->request;
		$params = $request->post();

		$result = ['success' => false, 'message' => Yii::t('app', 'System error occurred, please try again.')];

		if(!isset($params['mobile']) || !isset($params['uid'])){
			return [
				'success' => false,
				'name' => 'Bad Request',
				'message' => Yii::t('api', 'Lacks the required parameters'),
				'code' => 0,
				'status' => 400,
			];
		}

		/**
		 * Check Mobile validat
		 */

		$model = Agents::find()->phone($params['mobile'])->one();

		if($model !== NULL)
		{
			if($model->agentStatus == Agents::STATUS_BANNED){
				return [
					'success' => false,
					'name' => 'User Banned',
					'message' => Yii::t('app', 'The user is banned.'),
					'code' => 0,
					'status' => 401,
				];
			}

			$model->agentUID = $params['uid'];
			$model->generateAccessCode();
			
			$model->agentStatus = Agents::STATUS_REGISTER;
		}
		else{
			return [
				'success' => false,
				'name' => 'User Not Found',
				'message' => Yii::t('app', 'Your mobile phone is not registered.'),
				'code' => 0,
				'status' => 401,
			];
		}

		if($model->save()){
			/**
			 * Create Message
			 */
			$_message = Yii::t('app', 'your access cdoe is: {code}', ['code' => $model->agentActiveCode]);
			/**
			 * Send Active Code via SMS
			 */
			$response = Yii::$app->sms->SendSms2($model->mobile, $_message);

			if($response['status']){
				/**
				 * Archive Message
				 */
				$sms = new Messages;
				$sms->smsMobile = $model->mobile;
				$sms->smsMessage = $_message;
				$sms->smsIdentifier = $response['data']['long'];
				$sms->Date = time();
				$sms->smsUserId = $model->id;
				$sms->smsStatus = 0;
				$sms->smsType = Messages::TYPE_SYSTEM_SEND;
				$sms->smsSendUserId = 0;
				$sms->save();

				$result['success'] = true;
				$result['message'] = NULL;
			}
		}

		return $result;
	}
}