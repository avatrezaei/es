<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['actions'] = [
	[
		'label' => <?= $generator->generateString('Update', [], 'app') ?>,
		'url' => ['update', <?= $urlParams ?>]
	],
	[
		'label' => <?= $generator->generateString('Delete', [], 'app') ?>,
		'url' => ['delete', <?= $urlParams ?>],
		'options' => [
			'data' => [
				'confirm' => <?= $generator->generateString('Are you sure you want to delete this item?') ?>,
				'method' => 'post',
			],
			'class' => 'btn btn-danger'
		]
	],
];

?>

<?php echo "<?php"?> if (Yii::$app->request->isAjax): ?>
<div class="modal-header" id="modalHeader">
	<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
</div>
<?php echo "<?php"?> endif; ?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">
	<div class="profile-content">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div>
						<h4 class="profile-desc-title"> <?= "<?= " ?>Html::encode($this->title) ?> </h4>

						<?= "<?= " ?>DetailView::widget([
							'model' => $model,
							'template' => Yii::$app->params['detalView'],
							'attributes' => [
					<?php
					if (($tableSchema = $generator->getTableSchema()) === false) {
						foreach ($generator->getColumnNames() as $name) {
							echo "\t\t\t\t\t\t'" . $name . "',\r\n";
						}
					} else {
						$i = 0;
						foreach ($generator->getTableSchema()->columns as $column) {
							if($column->isPrimaryKey != 1){
								$format = $generator->generateColumnFormat($column);
								echo ($i == 0 ? "\t\t\t'" : "\t\t\t\t\t\t\t\t'") . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\r\n";
							$i++;
							}
						}
					}
					?>
							],
						]) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>