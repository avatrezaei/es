<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
	$searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use Yii;
use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\Json;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\data\Sort;

use <?= ltrim($generator->modelClass, '\\') ?>;
<?php if (!empty($generator->searchModelClass)): ?>
use <?= ltrim($generator->searchModelClass, '\\') . (isset($searchModelAlias) ? " as $searchModelAlias" : "") ?>;
<?php else: ?>
use yii\data\ActiveDataProvider;
<?php endif; ?>

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '@app/views/layouts/column2';

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'roles' => ['admin<?= $modelClass?>'],
						'allow' => true,
					],
					[
						'actions' => ['view'],
						'roles' => ['view<?= $modelClass?>'],
						'allow' => true,
					],
					[
						'actions' => ['create'],
						'roles' => ['create<?= $modelClass?>'],
						'allow' => true,
					],
					[
						'actions' => ['update'],
						'roles' => ['update<?= $modelClass?>'],
						'allow' => true,
					],
					[
						'actions' => ['delete'],
						'roles' => ['delete<?= $modelClass?>'],
						'allow' => true,
					]
				]
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all <?= $modelClass ?> models.
	 * @return mixed
	 */
	public function actionIndex()
	{
<?php if (!empty($generator->searchModelClass)): ?>
		$request = Yii::$app->request;
		$searchModel = new <?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

<?php if ($generator->indexWidgetType === 'grid' || $generator->indexWidgetType === 'datatable'): ?>
		$columns = [
			['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
	foreach ($generator->getColumnNames() as $name) {
		if (++$count < 6) {
			echo "			[
				'attribute' => '" . $name . "',
				'enableSorting'=>false
			],\n";
		} else {
			echo "			// [
			//	'attribute' => '" . $name . "',
			//	'enableSorting'=>false
			// ],\n";
		}
	}
} else {
	foreach ($tableSchema->columns as $column) {
		$format = $generator->generateColumnFormat($column);
		if (++$count < 6) {
			echo "			[
				'attribute' => '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',
				'enableSorting'=>false
			],\n";
		} else {
			echo "			// [
			//	'attribute' => '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',
			//	'enableSorting'=>false
			// ],\n";
		}
	}
}
?>

			[
				'class' => 'fedemotta\datatables\EActionColumn',
				'header' => Yii::t('app', 'Actions'),
				'headerOptions' => ['width' => '100'],
			],
		];

<?php endif; ?>
		$clientOptions = [
			'ajax'=>[
				'url' => $request->url,
			],
			'order' => [
				[ 1, 'asc' ]
			],
		];

		$start = $request->get('start', 0);
		$length = $request->get('length', 10);

		/**
		 * Create Pagination Object for paging:
		 */
		$_pagination = new Pagination;
		$_pagination->pageSize = $length;
		$_pagination->page = floor($start / $length);

		$dataProvider->pagination = $_pagination;

<?php
$columns = array();

if (($tableSchema = $generator->getTableSchema()) === false) {
	foreach ($generator->getColumnNames() as $name) {
		$columns[] = $name;
	}
} else {
	foreach ($tableSchema->columns as $column) {
		$columns[] = $column->name;
	}
}
?>
		$sortableColumn = ['<?php echo implode("','", $columns); ?>'];
		$searchableColumn = ['<?php echo implode("','", $columns); ?>'];

		$widget = Yii::createObject([
				'class' => 'fedemotta\datatables\DataTables',
				'dataProvider' => $dataProvider,
				'filterModel' => NULL,
				'columns' => $columns,
				'clientOptions' => $clientOptions,
				'sortableColumn' => $sortableColumn,
				'searchableColumn' => $searchableColumn,
			]
		);

		if ($request->isAjax) {
			$result = $widget->getFormattedData($request->post('draw'));

			echo json_encode($result);
			Yii::$app->end();
		}
		else{
			return $this->render('index', [
				'widget' => $widget,
			]);
		}
<?php else: ?>
		$dataProvider = new ActiveDataProvider([
			'query' => <?= $modelClass ?>::find(),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
<?php endif; ?>
	}

	/**
	 * Displays a single <?= $modelClass ?> model.
	 * <?= implode("\n	 * ", $actionParamComments) . "\n" ?>
	 * @return mixed
	 */
	public function actionView(<?= $actionParams ?>)
	{
		$model = $this->findModel(<?= $actionParams ?>);

		if (Yii::$app->request->isAjax) {
			return $this->renderAjax('view', [
				'model' => $model,
			]);
		}
		else{
			return $this->render('view', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Creates a new <?= $modelClass ?> model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new <?= $modelClass ?>();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', <?= $urlParams ?>]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing <?= $modelClass ?> model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * <?= implode("\n	 * ", $actionParamComments) . "\n" ?>
	 * @return mixed
	 */
	public function actionUpdate(<?= $actionParams ?>)
	{
		$model = $this->findModel(<?= $actionParams ?>);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', <?= $urlParams ?>]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing <?= $modelClass ?> model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * <?= implode("\n	 * ", $actionParamComments) . "\n" ?>
	 * @return mixed
	 */
	public function actionDelete(<?= $actionParams ?>)
	{
		$post = Yii::$app->request->post();
		
		$model = $this->findModel(<?= $actionParams ?>);
		$model->valid = 0;
		$_item = $model->name;

		if($model->save(false))
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => true,
					'messages' =>[
						Yii::t('app', '{item} Successfully Deleted.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', '{item} Successfully Deleted.',[
					'item' => $_item,
				]));
				$this->redirect(['index']);
			}
		}
		else
		{
			if(Yii::$app->request->isAjax)
			{
				echo Json::encode([
					'success' => false,
					'messages' =>[
						Yii::t('app', 'Failed To Delete Items To {item}.',[
							'item' => $_item,
						]) 
					]
				]);
			}
			else
			{
				Yii::$app->session->setFlash('success', Yii::t('app', 'Failed To Delete Items To {item}.',[
					'item' => $_item,
				]));

				$this->redirect(['index']);
			}
		}
	}

	/**
	 * Finds the <?= $modelClass ?> model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * <?= implode("\n	 * ", $actionParamComments) . "\n" ?>
	 * @return <?=				   $modelClass ?> the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel(<?= $actionParams ?>)
	{
<?php
if (count($pks) === 1) {
	$condition = '$id';
} else {
	$condition = [];
	foreach ($pks as $pk) {
		$condition[] = "'$pk' => \$$pk";
	}
	$condition = '[' . implode(', ', $condition) . ']';
}
?>
		if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
