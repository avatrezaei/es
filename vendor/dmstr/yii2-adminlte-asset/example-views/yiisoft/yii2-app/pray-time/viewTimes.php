<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PrayTimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pray Times ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pray-time-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?//= Html::a('Create Pray Time', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    
    <?php
 
$this->registerJs(
   '$("document").ready(function(){ 
        $("#city_name").on("pjax:end", function() { 
            $.pjax.reload({container:"#pray_times"});  //Reload GridView
        });
    });'
);
?>
    
    <div class="pray-time-search">
    <?php Pjax::begin(['id' => 'city_name']) ?>
    <?php
    
      $cities = app\models\PrayCity::find()->orderBy('name')->all();
    
      $listData = \yii\helpers\ArrayHelper::map($cities,'id','name');
      
      echo $form = \yii\helpers\BaseHtml::beginForm(Yii::$app->getUrlManager()->createUrl('pray-time/view-times'),'get',['data-pjax' => true ]); 
      echo \yii\helpers\BaseHtml::dropDownList('city_id',$searchModel->city_id,$listData,array('prompt'=>'--- انتخاب شهر ---'));
    
    
    ?>
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?php echo \yii\helpers\BaseHtml::endForm(); ?>
    <?php Pjax::end() ?>
    </div>
    
    <?php Pjax::begin(['id' => 'pray_times']) ?>
    
    <div>
      <?php
      //echo $searchModel->getMonthName();
        if($searchModel->city) {
          echo '<h2>اوقات شرعی شهر'.$searchModel->city->name.'</h2>';
          echo '<div style="text-align: right;">';
          
          if(($searchModel->month >= 1) && ($searchModel->month < 12)) 
            echo Html::a('Next month', ['view-times','city_id' => $city_id, 'mid' => $month + 1]) .' <=== ';
          
          echo ' ( '.$monthName.' )';
             
          if(($searchModel->month > 1) && ($searchModel->month <= 12))     
            echo ' ===> '.Html::a('Prev month', ['view-times','city_id' => $searchModel->city_id, 'mid' => $searchModel->month - 1]);
          
          
          echo '</div>';
        }
     ?>
    </div>
       
    
    
    <?php //Pjax::begin(['id' => 'pray_times']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function($searchModel){
            $day = (int)Yii::$app->jdate->date('d');
            if($searchModel->day == $day){
                return ['class' => 'danger'];
            }
    },
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],
            'day',

           // 'id',
            'morning',
            'sunrise',
            'noon',
            'sundown',
            // 'night',
            // 'midnight',
            // 'day',
            // 'month',
            // 'city_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end() ?>
    
    
</div>
