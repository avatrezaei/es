<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PrayTime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pray-time-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'morning')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'sunrise')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'noon')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'sundown')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'night')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'midnight')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'day')->textInput() ?>

    <?= $form->field($model, 'month')->textInput() ?>

    <?= $form->field($model, 'city_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
