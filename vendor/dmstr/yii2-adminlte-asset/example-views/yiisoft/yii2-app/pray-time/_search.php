<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PrayTimeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pray-time-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'morning') ?>

    <?= $form->field($model, 'sunrise') ?>

    <?= $form->field($model, 'noon') ?>

    <?= $form->field($model, 'sundown') ?>

    <?php // echo $form->field($model, 'night') ?>

    <?php // echo $form->field($model, 'midnight') ?>

    <?php // echo $form->field($model, 'day') ?>

    <?php // echo $form->field($model, 'month') ?>

    <?php // echo $form->field($model, 'city_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
