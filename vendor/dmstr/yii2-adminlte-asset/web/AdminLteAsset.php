<?php
namespace dmstr\web;

use yii\base\Exception;
use yii\web\AssetBundle;

/**
 * AdminLte AssetBundle
 * @since 0.1
 */
class AdminLteAsset extends AssetBundle
{
    public $sourcePath = '@vendor/themes/fumsems/assets';
    public $css = [
        'global/plugins/font-awesome/css/font-awesome.min.css',
        'global/plugins/simple-line-icons/simple-line-icons.min.css',
        'global/plugins/bootstrap/css/bootstrap-rtl.min.css',
        'global/plugins/uniform/css/uniform.default.css',
        'global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css',
        'global/css/components-rtl.min.css',
        'global/css/plugins-rtl.min.css',
        'layouts/layout/css/layout-rtl.min.css',
        'layouts/layout/css/themes/darkblue-rtl.min.css',
        'layouts/layout/css/custom-rtl.min.css',
        'pages/css/login-rtl.min.css',
        'global/plugins/icheck/skins/all.css',
        'global/css/hd.css',
    ];
    public $js = [
        'global/scripts/app.min.js',
        'global/scripts/hd.js',
        'global/plugins/icheck/icheck.min.js'
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    /**
     * @var string|bool Choose skin color, eg. `'skin-blue'` or set `false` to disable skin loading
     * @see https://almsaeedstudio.com/themes/AdminLTE/documentation/index.html#layout
     */
    public $skin = '_all-skins';

    /**
     * @inheritdoc
     */
    public function init()
    {
        // Append skin color file if specified
//        if ($this->skin) {
//            if (('_all-skins' !== $this->skin) && (strpos($this->skin, 'skin-') !== 0)) {
//                throw new Exception('Invalid skin specified');
//            }
//
//            $this->css[] = sprintf('css/skins/%s.min.css', $this->skin);
//        }

        parent::init();
    }
}
