<?php
$name='YekanWeb-Regular';
$type='TTF';
$desc=array (
  'CapHeight' => 700,
  'XHeight' => 500,
  'FontBBox' => '[-170 -459 1208 1094]',
  'Flags' => 4,
  'Ascent' => 1094,
  'Descent' => -459,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 500,
);
$unitsPerEm=2048;
$up=-73;
$ut=49;
$strp=259;
$strs=50;
$ttffile='D:/PHP/xampp/htdocs/sportevent/vendor/kartik-v/mpdf/ttfonts/Yekan.ttf';
$TTCfontID='0';
$originalsize=45380;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='yekan';
$panose=' 8 0 2 0 5 3 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 1277, -651, 0
// usWinAscent/usWinDescent = 1330, -673
// hhea Ascent/Descent/LineGap = 1330, -673, 0
$useOTL=255;
$rtlPUAstr='\x{0E000}\x{0E009}\x{0E00B}\x{0E014}-\x{0E016}\x{0E818}';
$GSUBScriptLang=array (
  'arab' => 'DFLT ',
);
$GSUBFeatures=array (
  'arab' => 
  array (
    'DFLT' => 
    array (
      'ccmp' => 
      array (
        0 => 0,
      ),
      'fina' => 
      array (
        0 => 1,
      ),
      'init' => 
      array (
        0 => 2,
      ),
      'isol' => 
      array (
        0 => 3,
      ),
      'medi' => 
      array (
        0 => 4,
      ),
      'rlig' => 
      array (
        0 => 5,
        1 => 6,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 44676,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 44826,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 44988,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 45112,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 45126,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 4,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 45252,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 4,
    'Flag' => 9,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 45294,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'arab' => 'DFLT ',
);
$GPOSFeatures=array (
  'arab' => 
  array (
    'DFLT' => 
    array (
      'mark' => 
      array (
        0 => 0,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 42348,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>