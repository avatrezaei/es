/*!
 * @package   yii2-detail-view
 * @author    Kartik Visweswaran <kartikv2@gmail.com>
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2015
 * @version   1.7.1
 *
 * Client extension for the yii2-detail-view extension 
 * 
 * Author: Kartik Visweswaran
 * Copyright: 2014 - 2015, Kartik Visweswaran, Krajee.com
 * For more JQuery plugins visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */

(function ($) {
    "use strict";

    var KvDetailView_tiny = function (element, options) {
        var self = this;
        self.$element = $(element);
        $.each(options, function (key, value) {
            self[key] = value;
        });
        self.init();
    };

    KvDetailView_tiny.prototype = {
        constructor: KvDetailView_tiny,
        init: function () {
            var self = this;
            self.initElements();
            self.listen();
        },
        alert: function(type, msg) {
            var self = this, css;
            css = self.alertMessageSettings[type];
            if (msg) {
                css = css || 'alert alert-' + type;
                return self.alertTemplate.replace('{content}', msg).replace('{class}', css);
            }
            return '';
        },
        initAlert: function () {
            var self = this, $alert = self.$element.find('.kv-alert-container');
            $alert.find('.alert .close').each(function() {
                var $el = $(this);
                $el.off('click').on('click', function() {
                    setTimeout(function() {
                        if (!$alert.find('.alert').length)  {
                            $alert.hide();
                        }
                    }, 300);
                });
            });            
        },
        listen: function () {
            var self = this, $alert = self.$element.find('.kv-alert-container'), 
                $detail = self.$element.find('.kv-detail-view');
            $detail.closest('form').on('afterValidate', function (event, messages) {
                if (messages !== undefined) {
                    $detail.removeClass('kv-detail-loading');
                }
            });
            self.$btnSaveTiny.on('click', function () {
                $alert.hide();
                $detail.removeClass('kv-detail-loading').addClass('kv-detail-loading');
            });
            self.$btnUpdateTiny.on('click', function () {
                self.setMode('edit');
            });
            self.$btnDeleteTiny.on('click', function () {
                self.setMode('view');
            });
            self.$btnDeleteTiny.on('click', function (ev) {
            var $el = $(this), params = self.deleteParams, confirmMsg = self.deleteConfirm,
                settings = self.deleteAjaxSettings || {};
                ev.preventDefault();
                if  (confirmMsg && !confirm(confirmMsg)) {
                    return;
                }
                settings = $.extend({
                    type: 'post',
                    dataType: 'json',
                    data: params,
                    url: $el.attr('href'),
                    beforeSend: function() {
                        $alert.html('').hide();
                        $detail.removeClass('kv-detail-loading').addClass('kv-detail-loading');
                    },
                    success: function (data) {
                        if (data.success) {
                            $detail.hide();
                            self.$btnDeleteTiny.attr('disabled', 'disabled');
                            self.$btnUpdateTiny.attr('disabled', 'disabled');
                            self.$btnDeleteTiny.attr('disabled', 'disabled');
                            self.$btnSaveTiny.attr('disabled', 'disabled');
                        };
                        $.each(data.messages, function(key, msg) {
                            $alert.append(self.alert(key, msg));
                        });
                        $alert.hide().fadeIn('slow', function() {
                            $detail.removeClass('kv-detail-loading'); 
                            self.initAlert();                           
                        });
                    },
                    error: function(xhr, txt, err) {
                        var msg = '';
                        if (self.showErrorStack) {
                            msg = $.trim($(xhr.responseText).text()).replace(/\n\s*\n/g, '\n').replace(/\</g, '&lt;');
                            msg = msg.length ? '<pre>' +  msg + '</pre>' : '';
                        }
                        msg = self.alert('kv-detail-error', err + msg);
                        $detail.removeClass('kv-detail-loading');
                        $alert.html(msg).hide().fadeIn('slow');
                        self.initAlert();
                    }
                }, settings);
                $.ajax(settings);
            });
            self.initAlert();
        },
        setMode: function (mode) {
           // alert('&&&&');
            var self = this, t = self.fadeDelay;
            if (mode === 'edit') {
                self.$attribsTiny.fadeOut(t, function () {
                    self.$formAttribsTiny.fadeIn(t);
                    self.$element.removeClass('kv-view-mode kv-edit-mode').addClass('kv-edit-mode');
                });
                self.$buttons1Tiny.fadeOut(t, function () {
                    self.$buttons2Tiny.fadeIn(t);
                });
            }
            else {
                self.$formAttribsTiny.fadeOut(t, function () {
                    self.$attribsTiny.fadeIn(t);
                    self.$element.removeClass('kv-view-mode kv-edit-mode').addClass('kv-view-mode');
                });
                self.$buttons2Tiny.fadeOut(t, function () {
                    self.$buttons1Tiny.fadeIn(t);
                });
            }
        },
        initElements: function () {
            var self = this, $el = self.$element;
            self.$btnUpdateTiny = $el.find('.tiny-kv-btn-update');
            self.$btnDeleteTiny = $el.find('.tiny-kv-btn-delete');
            self.$btnDeleteTiny = $el.find('.tiny-kv-btn-view');
            self.$btnSaveTiny = $el.find('.tiny-kv-btn-save');
            self.$attribsTiny = $el.find('.kv-attribute');
            self.$formAttribsTiny = $el.find('.kv-form-attribute');
            self.$buttons1Tiny = $el.find('.kv-buttons-1');
            self.$buttons2Tiny = $el.find('.kv-buttons-2');
        }
    };

    //KvDetailView_tiny plugin definition
    $.fn.kvDetailView_tiny = function (option) {
        var args = Array.apply(null, arguments);
        args.shift();
        return this.each(function () {
            var $this = $(this),
                data = $this.data('kvDetailView'),
                options = typeof option === 'object' && option;

            if (!data) {
                data = new KvDetailView_tiny(this, $.extend({}, $.fn.kvDetailView.defaults, options, $(this).data()));
                $this.data('kvDetailView', data);
            }

            if (typeof option === 'string') {
                data[option].apply(data, args);
            }
        });
    };

    $.fn.kvDetailView_tiny.defaults = {
        mode: 'view',
        fadeDelay: 800,
        alertTemplate: '',
        alertMessageSettings: {},
        deleteParams: {},
        deleteAjaxSettings: {},
        deleteConfirm: '',
        showErrorStack: false
    };

    $.fn.kvDetailView_tiny.Constructor = KvDetailView_tiny;
}(window.jQuery));