<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */

$this->title = Yii::t('rbac-admin', 'Assignments');
$this->params['breadcrumbs'][] = $this->title;

$columns = array_merge(
    [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'class' => 'yii\grid\DataColumn',
        'attribute' => $usernameField,
    ],
    ], $extraColumns, [
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}'
    ],
    ]
);
?>


<div class="send-messsage-index">
    <div class="col-lg-12 col-md-12  ">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green">
                    <span class="caption-subject bold">
                        <i class="icon-envelope-open"></i>
                        <?= Html::encode($this->title) ?>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                 

     

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]);
    ?>
    <?php Pjax::end(); ?>

 
            </div>
        </div>
    </div>
</div>
 
