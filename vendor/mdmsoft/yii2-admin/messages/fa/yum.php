<?php

return array(
'Remember me next time' => 'مرا بخاطر بسپار',
 '{attribute} is invalid.' => '{attribute} دارای مقدار معتبری نیست.',
 /* password validator message */
'{attribute} is too short (min. {num} characters).' => '{attribute} کوتاه است. (حداقل کاراکتر {num})',
 '{attribute} is too long (max. {num} characters).' => '{attribute} طولانی است. (خداکثر کاراکتر {num})',
 '{attribute} must include at least {num} upper case letters.' => '{attribute} (حداقل باید دارای  {num}حرف بزرگ باشد.)',
 '{attribute} must include at least {num} lower case letters.' => '{attribute} (حداقل باید دارای  {num}حرف کوچک باشد.)',
 '{attribute} must include at least {num} digits.' => '{attribute} (حداقل باید دارای  {num}کاراکتر عددی باشد.)',
 '{attribute} must include at least {num} symbols.' => '{attribute} (حداقل باید دارای  {num}کاراکتر سمبولیک باشد.)',
 '{attribute} must not contain whitespace.' => '{attribute} (حداقل باید دارای کاراکتر فاصله باشد.)',
 '{attribute} must not contain more than {num} sequentially repeated characters.' => '{attribute} (خداکثر باید دارای  {num}کاراکتر تکراری  باشد.)',
 'Password Reset Successfully' => 'پسورد با موفقیت بازنشانی شد',
 'Your current password is not correct' => 'کلمه عبور فعلی وارد شده صحیح نمی باشد',
 'The new password has been saved' => 'کلمه عبور با موفقیت تغییر کرد',
 'There was an error saving the password' => 'در روند تغییر کلمه عبور اشکال ایجاد شده است،لطفا دوباره سعی کنید',
 'Your password has expired. Please enter your new Password below:' => 'تاریخ اعتبار کلمه عیور شما منقضی شده است، لطفا کلمه عیور جدید وارد کنید',
 'Fields with <span class="required">*</span> are required.' => ' پر کردن فیلد های دارای <span class="required">*</span>الزامی است',
 'Username or Password is incorrect' => 'نام کاربری/کلمه عبور صحیح را وارد کنید.',
 'This account was blocked For 15 Min' => 'حساب کاربری شما مسدوداست. شما برای 15 دقیقا امکان ورود نخواهید داشت !  ',
 'This account  blocked For 15 Min' => 'حساب کاربری شما غیرفعال شد. شما برای 15 دقیقا امکان ورود نخواهید داشت.',
'You are not authorized to per-form this action.you just can view your Information'=>'شما  مجاز به انجام این عملیات نیستید ',
	'You may optionally enter a comparison operator (<, <=, >, >=, <> or =) at the beginning of each of your search values to specify how the comparison should be done. '=>
	'به منظور جستجوی مقادیر عددی می توان از یک عملگر مقایسه (<، <=،>،> =، <> و یا =)  استفاده کرد.	
',
	'Advanced Search'=>'جست و جوی پیشرفته',
	'No results found'=>' هيچ موضوعی در مورد جستجوي شما پيدا نشد',
   'Displaying {start}-{end}of {count} results.'=>'نمایش{start}-{end}از{count}نتیجه',
	
	'Please fill out the following form with your login credentials:'=>'',
	'User is inactive and wating admin confirm'=>'حساب کاریری تا زمان تایید مدیر سیستم غیر فعال است. ',
	'Register Done Successfully'=>'ثبت نام با موفقیت انجام شد.لطفا منتظر ایمیل تایید مدیر سیستم باشید',
   'There was an error in sending email.confirm is done'=>'تایید صورت گرفت. مشکل در ارسال ایمیل به کاربر پیدا شده است',
	'Invalid request. Please do not repeat this request again.'=>'درخواست نامعتبر است.',
  
	'if you forget your password  click {link} '=>'	  در صورتی که پسورد خود را فراموش کرده اید	link  کلیک کنید !',
	'this'=>'اینجا',
   'Pleaze {link} to use more facilities. '=>'به منظور استفاده از امکانات سیستم  link   کنید ',
	
	);
