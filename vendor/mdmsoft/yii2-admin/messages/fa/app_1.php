<?php
return array(

	/*********************Models************ */
	'Project' => 'پروژه ',
	'Country' => 'کشور',
	'Article' => 'مقاله',
 
	/************************************** */
	/*****************Buttoms************** */
	'Create' => 'ایجاد',
	'Delete' => 'حذف',
	'View' => 'مشاهده',
	'Reset Grid' => 'به روز رسانی ',
	'Select a Language' => 'انتخاب زبان...',
	'Select a Sicence Level' => 'انتخاب سطح علمی...',
	'Select a Country' => 'انتخاب کشور ...',
	'Extera Info' => 'اطلاعات بیشتر',
	'Add Child' => 'افزودن مورد',
	'Delete' => 'حذف',
	'New'=>'جدید',
	'Preview'=>'پیش نمایش',
	/************************************** */
	/********Country attibutes************** */
	'English Title' => 'عنوان انگلیسی',
	'Persian  Title' => 'عنوان فارسی',

	/********Experience attibutes************** */
	'Subject' => 'موضوع',
	'Position' => 'وضعیت',
	'Item' => 'موارد',	
		'Experience Items' => ' آیتم تجربیات',

	/*******Academic Background Attribute********** */
	'Supervisor' => 'ناظر',
	'From Date' => 'از تاریخ',
	'To Date' => 'تا تاریخ',
	'Theses Title' => 'عنوان پایان نامه ',
	'Science Level' => 'سطح علمی',
	'City' => 'شهر',
	/*********Setting Attributes**************** */
	'Category' => 'دسته',
	'Key' => 'عنوان تنظیم',
	'Value' => 'مقدار',
	'Description' => 'توضیحات',
	/*********Common Attributes**************** */
	 'ID' => 'شناسه',
	'Title' => 'عنوان',
	'Date' => 'تاریخ',
	'Description' => 'توضیحات',
	'Lang' => 'زبان',
	'Created Date Time' => 'تاریخ ایجاد',
	'Modified Date Time' => 'تاریخ ویرایش',
	'User ID' => ' کاربر',
	'Username' => 'نام کاربری',
	'Password' => 'کلمه عبور',
	'Salt' => 'Salt',
	'Name' => 'نام',
	'Last Name' => 'نام خانوادگی',
	'Office' => 'سمت',
	'Gender' => 'جنسیت',
	'Age' => 'سن',
	'Grade' => 'مدرک تحصیلی',
	'Edu' => 'رشته تحصیلی',
	'Univercity' => 'دانشگاه',
	'Homepage' => 'صفحه خانگی',
	'Tel' => 'تلفن',
	'Mobile' => 'موبایل',
	'Email' => 'پست الکترونیکی',
	'Type Assist' => 'نوع همکاری',
	'Organization' => 'سازمان',
	'Login Attemp Count' => '',
	'Update Date' => 'تاریخ ویرایش',
	'Create Date' => 'تاریخ ایجاد ',
	'Last Login Time' => 'زمان آخرین ورود',
	'Last Login Attemp Time' => 'زمان آخرین تلاش ورود',
	'Last Action' => 'آخرین فعالیت',
	'Lock' => 'مسدود',
	'Active' => 'فعال',
	'Valid' => 'معتبر',
	'Role' => 'سطح دسترسی',
	'Retype your new password' => 'تکرار کلمه عبور ',
	'Your current password' => '  کلمه عبور فعلی',
	/*****************************************/
	/****************** messages*************/
	'{item} Successfully Added.' => '{item} با موفقیت اضافه شد',
	'{item} Successfully Deleted.' => '{item} با موفقیت حذف شد',
	'{item} Successfully Edited.' => '{item} با موفقیت ویرایش شد',
	'Failed To Add Items To {item}.' => 'عملیات اضافه کردن {item} با مشکل مواجه شده است.',
	'Failed To Edit Items To {item}.' => 'عملیات ویرایش کردن {item} با مشکل مواجه شده است.',
	'Failed To Delete Items To {item}.' => 'عملیات حذف کردن {item} با مشکل مواجه شده است.',
	
	'Are you sure you want to delete this item?' => 'آیا از حذف این آیتم اطمینان دارید؟',
	'The {item} # {id} was successfully deleted.{url} to proceed.' => '{item} شماره با موفقیت حذف شد',
	'Error occured while saving children.'=>'در هنگام افزودن آیتم ها خطا رخ داده است.',
	'You are only allowed to define {num} {item}'=>'شما تنها مجاز به تعریف  {num}  {item} می باشید'
);
