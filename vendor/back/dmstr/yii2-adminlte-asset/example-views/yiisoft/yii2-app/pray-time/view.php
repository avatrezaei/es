<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PrayTime */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pray Times', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pray-time-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'city_id' => $model->city_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'city_id' => $model->city_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'morning',
            'sunrise',
            'noon',
            'sundown',
            'night',
            'midnight',
            'day',
            'month',
            'city_id',
        ],
    ]) ?>

</div>
