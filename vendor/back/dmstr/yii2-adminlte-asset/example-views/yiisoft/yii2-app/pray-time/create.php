<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PrayTime */

$this->title = 'Create Pray Time';
$this->params['breadcrumbs'][] = ['label' => 'Pray Times', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pray-time-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
