<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model johnitvn\rbacplus\models\AuthItem */

?>
<?php //die(var_dump($controllerArray)) ?>
<div class="auth-item-create">
    <?= $this->render('_form', [
        'model' => $model,
        'controllerArray'=>$controllerArray

    ]) ?>
</div>
