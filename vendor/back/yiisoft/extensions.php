<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'boundstate/yii2-htmlconverter' => 
  array (
    'name' => 'boundstate/yii2-htmlconverter',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@boundstate/htmlconverter' => $vendorDir . '/boundstate/yii2-htmlconverter',
    ),
  ),
  'cakebake/yii2-bootstrap-select' => 
  array (
    'name' => 'cakebake/yii2-bootstrap-select',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@cakebake/bootstrap/select' => $vendorDir . '/cakebake/yii2-bootstrap-select',
    ),
  ),
  'kartik-v/yii2-widgets' => 
  array (
    'name' => 'kartik-v/yii2-widgets',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/widgets' => $vendorDir . '/kartik-v/yii2-widgets',
    ),
  ),
  'moonlandsoft/yii2-phpexcel' => 
  array (
    'name' => 'moonlandsoft/yii2-phpexcel',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@moonland/phpexcel' => $vendorDir . '/moonlandsoft/yii2-phpexcel',
    ),
  ),
  'drsdre/yii2-wizardwidget' => 
  array (
    'name' => 'drsdre/yii2-wizardwidget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@drsdre/wizardwidget' => $vendorDir . '/drsdre/yii2-wizardwidget',
    ),
  ),
  'faravaghi/yii2-jalali-datepicker' => 
  array (
    'name' => 'faravaghi/yii2-jalali-datepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@faravaghi/jalaliDatePicker' => $vendorDir . '/faravaghi/yii2-jalali-datepicker',
    ),
  ),
  'fedemotta/yii2-widget-datatables' => 
  array (
    'name' => 'fedemotta/yii2-widget-datatables',
    'version' => '1.3.0.0',
    'alias' => 
    array (
      '@fedemotta/datatables' => $vendorDir . '/fedemotta/yii2-widget-datatables',
    ),
  ),
);
