<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
	$safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */

$formGroupClass = Yii::$app->params['formGroupClass'];
$template = Yii::$app->params['template'];
$inputClass = Yii::$app->params['inputClass'];
$labelClass = Yii::$app->params['labelClass'];
$errorClass = Yii::$app->params['errorClass'];

?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

	<?= "<?php " ?>$form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>
	<?= "<?php " ?>echo $form->errorSummary($model, ['class' => 'alert alert-danger']);  ?>

	<div class="form-body">

<?php foreach ($generator->getColumnNames() as $attribute) {
	if (in_array($attribute, $safeAttributes)) {
		echo "		<?php\r\n\t\t\techo " . $generator->generateActiveField($attribute) . ";\r\n\t\t?>\n\n";
	}
} ?>
	</div>

	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				<?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Create', [], 'app') ?> : <?= $generator->generateString('Update', [], 'app') ?>, ['class' => $model->isNewRecord ? 'btn green' : 'btn blue']) ?>
			</div>
		</div>
	</div>

	<?= "<?php " ?>ActiveForm::end(); ?>

</div>
