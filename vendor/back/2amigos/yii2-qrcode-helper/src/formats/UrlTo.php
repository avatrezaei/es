<?php
/**
 * @copyright Copyright (c) 2013-15 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
namespace dosamigos\qrcode\formats;

use yii\base\InvalidConfigException;

/**
 * Class UrlTo formats a string to properly create a Url QrCode
 *
 * @author Mohammad Ebrahim Amini <info@dkr.co.ir>
 * @link http://www.dkr.co.ir/
 * @link http://www.faravaghi.ir/
 * @package dosamigos\qrcode\formats
 */
class UrlTo extends FormatAbstract
{
    public $url;

    /**
     * @inheritdoc
     */
    public function getText()
    {
        return "{$this->url}";
    }
}