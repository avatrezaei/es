/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'fa';
         // Toolbar configuration generated automatically by the editor based on config.toolbarGroups.
config.toolbar = [
    { name: 'document',    items : [ 'Source','-','NewPage','DocProps','Preview','Print','-','Templates' ] },

	//{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Scayt' ] },
//	'/',
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
	{ name: 'tools', items: [ 'Maximize' ] },
        //'/',
	{ name: 'styles', items: [ 'Styles', 'Format' ] },
        { name: 'colors',      items : [ 'TextColor','BGColor' ] },

	
	//{ name: 'others', items: [ '-' ] },
	//{ name: 'about', items: [ 'About' ] }
];


//config.toolbar_Full =
//[
//    { name: 'document',    items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
//    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ,'-','RemoveFormat'] },
//    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
//    { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
//    '/',
//    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
//    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
//    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
//    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
//    '/',
//    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
//    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
//    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] }
//];



//http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-fileTools_defaultFileName

//http://ckeditor.com/forums/Support/How-enable-CKFinder-my-CKEditor
config.filebrowserImageBrowseUrl = '/../browse.php';
//config.filebrowserImageUploadUrl = 'http://localhost/homepageparent/yii2_homepage/backend/web/index.php?r=project/upload';
config.filebrowserImageUploadUrl = 'http://localhost/homepageparent/yii2_homepage/backend/web/index.php?r=YiiFileManager/ck-editor-file-upload/upload';

//config.filebrowserImageUploadUrl =  'http://localhost/homepageparent/yii2_homepage/backend//ckupload.php';

// Toolbar groups configuration.
//config.toolbarGroups = [
//	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
//	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
//	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
//	{ name: 'forms' },
//	'/',
//	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
//	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
//	{ name: 'links' },
//	{ name: 'insert' },
//	'/',
//	{ name: 'styles' },
//	{ name: 'colors' },
//	{ name: 'tools' },
//	{ name: 'others' },
//	{ name: 'about' }
//];
	// config.uiColor = '#AADC6E';
};
