Rezvan SMS
==========
Extension to send SMS using web service

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist faravaghi/yii2-rezvan-sms "*"
```

or add

```
"faravaghi/yii2-rezvan-sms": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, add it as a component  :

```php
'sms' =>[
	'class' => 'faravaghi\rezvansms\Rezvansms',
	'login' => 'USERNAME', // User to web login 
	'password' => 'PASSWORD', // password to web login
	'url' => 'http://sms.3300.ir/almassms.asmx?wsdl',
],
```

Now you can use in your project:

```php
Yii::$app->sms->SendSms2(['09123456789'], 'Hello! this is sample sms.');
```