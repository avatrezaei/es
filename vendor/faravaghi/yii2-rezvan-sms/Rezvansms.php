<?php
/**
 * Extension to send SMS
 * Based on Mohammad Amini code <http://www.yiiframework.com/user/99958> for yii-RezvanSMS
 *
 * @author faravaghi <faravaghi@gmail.com>
 * @license MIT
 * @version 1.0.1
 */

namespace faravaghi\rezvansms;

use yii\base\Component;
use Exception;

/**
 * How to use
 *
 * You at least need PHP 5.3+ adn cURL.
 *
 * Change your config/main.php to add a new component:
 *
 * 'components' => [
 *	 'sms' => [
 *		 'class'	=> 'yii\faravaghi\RezvanSMS',
 *		 'userid'	 => 'user to userid', // User to web userid 
 *		 'password'   => 'password to userid',	 // password to web userid
 *		 'testMode' => true			// If you gonna only test
 *	]
 *]
 *
 * Now you can use the extension like this:
 *
 * Yii::app()->sms->SendSms(['+34600000000', '+01555000000'], 'Hello!');
 *
 */
class Rezvansms extends Component
{
	const REQUEST_SUCCESS   = true;
	const REQUEST_UNSUCCESS = false;

	const MODE_DISPLAY_ONTO_MOBILE = 0;
	const MODE_STORED_IN_PHONE_MEMORY = 1;
	const MODE_STORED_IN_SIM_MEMORY = 3;
	const MODE_STORED_IN_APPLICATION = 4;

	/**
	 * User to login
	 * @var string
	 */
	public $login;

	/**
	 * User Password
	 * @var string
	 */
	public $password;

	/**
	 * To Encodings.
	 * @var boolean
	 */
	public $sms_encodings = 2;

	/**
	 * To Encodings.
	 * @var boolean
	 */
	public $sms_mclass = self::MODE_STORED_IN_PHONE_MEMORY;

	/**
	 * If you want to use SSL(HTTPS)
	 * @var boolean
	 */
	public $useSSL = false; //not working by now

	/**
	 * Api URL
	 * Api Url to call without 'http'
	 * @var string
	 */
	public $url = '';

	/**
	 * Server Response
	 * @var array
	 */
	protected $response;

	/**
	 * Server Response CODE
	 * @var array
	 */
	protected $responseCode;

	/**
	 * Server Response CODE
	 * @var array
	 */
	protected $client;

	/**
	 * Initialization
	 *
	 */
	public function init()
	{
		require(dirname(__FILE__) . "/lib/nusoap.php");

		$this->client = new \nusoap_client($this->url, 'wsdl', '', '', '', '');
		$this->client->soap_defencoding = 'UTF-8';
		$this->client->decode_utf8 = false;

		if ($this->client->fault) {
			trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faulstring})", E_ERROR);
		} else {
			$error = $this->client->getError();
			if ($error) {
				throw new \yii\web\HttpException(400, $error);
			}
		}

		parent::init();

		\Yii::trace('Extension initializating', 'RezvanSMS');
	}

	/**
	 * List of API calls
	 * @return array
	 */
	protected function calls()
	{
		return [
			'SendSms'  => [
				'returnKey'=> 'pMessageIds',
				'responseCode' => 'SendSmsResult',
				'required' => ['mobiles', 'messages'],
				'type' => ['string','string']
			],

			'Send1Sms' => [
				'returnKey'=> 'pMessageIds',
				'responseCode' => 'Send1SmsResult',
				'required' => ['mobiles', 'messages'],
				'type' => ['string','string']
			],

			'SendSms2' => [
				'returnKey'=> 'pMessageIds',
				'responseCode' => 'SendSms2Result',
				'required' => ['mobiles', 'messages'],
				'optional' => ['mclass','Encodings'],
				'type' => ['string','string','int','int']
			],

			'SendSms3' => [
				'returnKey'=> 'result',
				'responseCode' => 'SendSms3Result',
				'required' => ['mobiles', 'messages'],
				'optional' => ['UDH'],
				'fixed' => [
					'mclass'=>['int' => 1],
					'Encodings' => ['int' => $this->sms_encodings]
				],
				'type' => ['string','string']
			],

			'GetCredit' => [
				'returnKey'=> 'pCredit',
				'responseCode' => 'GetCreditResult'
			],

			'GetInfo2' => [
				'returnKey'=> ['pSmsNumber','pCredit','pName','pRole','pUserId','pEndDate'],
				'responseCode' => 'GetInfo2Result'
			],

			'GetMessageStatus' => [
				'returnKey'=> 'pStatuses',
				'responseCode' => 'GetMessageStatusResult',
				'required' => ['pMessageIds'],
				'type' => ['long']
			],

			'GetRealMessageStatuses' => [
				'returnKey'=> 'pStatuses',
				'responseCode' => 'GetRealMessageStatusesResult',
				'required' => ['pMessageIds'],
				'type' => ['long']
			],
		];
	}

	/**
	 * Process API calls
	 *
	 * Basic usage component
	 *
	 * You can use optional keys like this
	 *
	 * Yii::app()->sms->SendSms(['+34600000000', '+01555000000'], 'Hello!');
	 *
	 *
	 * @param string $call Call name in camelCase
	 * @param array $args Arguments to convert the API call array
	 * @return mixed Response to API call in accordance with responseCode and type RezvanSMS.call()
	 */
	public function __call($call, $args)
	{
		$result = ['status' => self::REQUEST_UNSUCCESS];
		$calls = $this->calls();

		$callName = $call;

		if(!isset($calls[$callName]))
			throw new Exception('Unknown call');

		$params = $calls[$callName];

		$params['required'] = isset($params['required']) ?(array) $params['required'] : [];
		$params['optional'] = isset($params['optional']) ?(array) $params['optional'] : [];
		$params['fixed'] = isset($params['fixed']) ? $params['fixed'] : [];
		$params['type'] = isset($params['type']) ? $params['type'] : [];

		$args = $this->populate($args, $params['required'], $params['optional'], $params['type']);
		$args = array_merge($args, $params['fixed']);

		$response = $this->request($callName, $args);

		if(isset($params['responseCode']))
		{
			if(isset($response[$params['responseCode']]))
			{
				if($response[$params['responseCode']] < 0)
				{
					if(!is_array($params['returnKey'])){
						$result['status'] = self::REQUEST_SUCCESS;
						$result['data'] = $response[$params['returnKey']];
						$result['message'] = '';

						return $result;
					}
					else{
						unset($response[$params['responseCode']]);
						$result['status'] = self::REQUEST_SUCCESS;
						$result['data'] = $response[$params['returnKey']];
						$result['message'] = '';

						return $result;
						return $response;
					}
				}
				else{
					// throw new CException('Method execution failed');
					$result['message'] = \Yii::t('rezvan', $response[$params['responseCode']]);
					$result['status'] = self::REQUEST_UNSUCCESS;

					return $result;
				}

				if(isset($params['returnType']))
				{
					if(! settype($response[$params['responseCode']], $params['returnType']))
					{
						$result['message'] = \Yii::t('rezvan', 'Type is not supported');
						$result['status'] = self::REQUEST_UNSUCCESS;

						return $result;
					}
				}

				return $response[$params['responseCode']];
			}
			else{
				// throw new CException('Key not found');
				return $result['status'] == self::REQUEST_UNSUCCESS;
			}
		}

		return $result;
	}

	/*
	* Formation of an array of call parameters of the arguments
	* @ Param mixed $ args The arguments passed to the method call
	* @ Param array $ required array parameters required
	* @ Param array $ optional array of additional parameters
	* @ Return array An array of call parameters
	**/
	protected function populate($args, $required, $optional, $type)
	{
		$allParams = array_merge($required, $optional);

		// Special case
		if(count($args) === 1)
			// Check if the first argument is an array
			if(is_array($args[0]))
				// ...And if all keys - line
				if(array_reduce(array_keys($args[0]), function($prev, $curr) { return $prev && is_string($curr); }, true))
				{
					foreach($required as $requiredParam)
						if(! isset($args[0][$requiredParam]))
							throw new Exception('It lacks some parameters');

					return $args[0];
				}

		if(count($args) < count($required))
			throw new Exception('It lacks some required parameteres');

		if(count($args) > count($allParams))
			throw new Exception('Too much parameteres');

		$params = [];

		foreach($args as $id => $arg){
			if(!empty($type))
				$params[$allParams[$id]] = [$type[$id] => $arg];
			else
				$params[$allParams[$id]] = $arg;
		}

		return $params;
	}

	/**
	 * API Call
	 * @param string $call name to call
	 * @param array $params call parameteres
	 * @return array server response
	 */
	protected function request($call, array $params = [])
	{
		//$params = array_map(function($val) { return join(',',(array) $val); }, $params);
		$params = array_merge($params,['pUsername' => $this->login, 'pPassword' => $this->password]);

		$response = $this->client->call($call, $params);

		return $response;
	}

	/**
	 * Return the server response to the last request
	 * @return array
	 */
	public function getResponse()
	{
		return $this->response;
	}

	/**
	 * Return the server response to the last request
	 * @return array
	 */
	public function getResponseCode()
	{
		return $this->responseCode;
	}
}
