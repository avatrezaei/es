<?php
/**
 * Extension to Food Reserve
 * Based on Mohammad Amini code <http://www.yiiframework.com/user/99958> for yii-Rezvan-Food-Reserve
 *
 * @author faravaghi <faravaghi@gmail.com>
 * @license MIT
 * @version 1.0.1
 */

namespace faravaghi\foodreserve;

use yii\base\Component;
use Exception;

/**
 * How to use
 *
 * You at least need PHP 5.3+ adn cURL.
 *
 * Change your config/main.php to add a new component:
 *
 * 'components' => [
 *	 'food' => [
 *		 'class'	=> 'yii\faravaghi\FoodReserve',
 *		 'username' => 'user to userid', // User to web userid 
 *		 'password' => 'password to userid',	 // password to web userid
 *	]
 *]
 */
class FoodReserve extends Component
{
	const REQUEST_SUCCESS 	= true;
	const REQUEST_UNSUCCESS = false;

	const RESPONSE_SUCCESS 	= 'SUCCESS';
	const RESPONSE_UNSUCCESS = 'FAIL';

	/**
	 * User to login
	 * @var string
	 */
	public $username;

	/**
	 * User Password
	 * @var string
	 */
	public $password;

	/**
	 * Api URL
	 * Api Url to call without 'http'
	 * @var string
	 */
	public $url = '';

	/**
	 * Server Response
	 * @var array
	 */
	protected $response;

	/**
	 * Server Response CODE
	 * @var array
	 */
	protected $client;

	/**
	 * Initialization
	 *
	 */
	public function init()
	{
		require(dirname(__FILE__) . "/lib/nusoap.php");

		$this->client = new \nusoap_client($this->url, false);
		$this->client->soap_defencoding = 'UTF-8';
		$this->client->decode_utf8 = false;

		if ($this->client->fault) {
			trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faulstring})", E_ERROR);
		} else {
			$error = $this->client->getError();
			if ($error) {
				throw new \yii\web\HttpException(400, $error);
			}
		}

		parent::init();

		\Yii::trace('Extension initializating', 'FoodReserve');
	}

	/**
	 * List of API calls
	 * @return array
	 */
	protected function calls()
	{
		return [
			'DefinePersons' => [
				'required' => ['persons'],
			],

			'Reserve' => [
				'required' => ['persons','from','to','meal','selfservice'],
				'fixed' => [
					'mode' => 'reserve',
				],
			],

			'Cancel' => [
				'required' => ['persons','from','to','meal'],
				'optional' => ['selfservice'],
				'fixed' => [
					'mode' => 'cancel',
				],
			],

			'Report' => [
				'required' => ['persons','from','to','meal'],
				'fixed' => [
					'mode' => '',
				],
			],
		];
	}

	/**
	 * Process API calls
	 *
	 * Basic usage component
	 *
	 * You can use optional keys like this
	 *
	 * Yii::app()->sms->SendSms(['+34600000000', '+01555000000'], 'Hello!');
	 *
	 *
	 * @param string $call Call name in camelCase
	 * @param array $args Arguments to convert the API call array
	 * @return mixed Response to API call in accordance with responseCode and type RezvanSMS.call()
	 */
	public function __call($call, $args)
	{
		$result = ['status' => self::REQUEST_UNSUCCESS];
		$calls = $this->calls();

		$callName = $call;

		if(!isset($calls[$callName]))
			throw new Exception('Unknown call');

		$params = $calls[$callName];

		$params['required'] = isset($params['required']) ?(array) $params['required'] : [];
		$params['optional'] = isset($params['optional']) ?(array) $params['optional'] : [];
		$params['fixed'] = isset($params['fixed']) ? $params['fixed'] : [];

		$args = $this->populate($args, $params['required'], $params['optional']);
		$args = array_merge($params['fixed'], $args);

		$callName = ($call == 'Cancel') ? 'Reserve' : $call;

		$response = $this->request($callName, $args);

		if(isset($response['status']))
		{
			if($response['status'] == self::RESPONSE_SUCCESS)
			{
				if(isset($response['list'])){
					$result['status'] = self::REQUEST_SUCCESS;
					$result['message'] = $response['message'];
					$result['data'] = $response['list'];

					if($call == 'Report')
						$result['report'] = $response['report'];

					return $result;
				}
				else{
					$result['status'] = self::REQUEST_SUCCESS;
					$result['message'] = $response['message'];
					$result['data'] = NULL;

					return $result;
				}
			}
			else
			{
				$result['status'] = self::REQUEST_UNSUCCESS;
				$result['message'] = $response['message'];
				$result['data'] = isset($response['list']) ? $response['list'] : NULL;

				return $result;
			}
		}
		else
		{
			return $result['status'] == self::REQUEST_UNSUCCESS;
		}

		return $result;
	}

	/*
	* Formation of an array of call parameters of the arguments
	* @ Param mixed $ args The arguments passed to the method call
	* @ Param array $ required array parameters required
	* @ Param array $ optional array of additional parameters
	* @ Return array An array of call parameters
	**/
	protected function populate($args, $required, $optional)
	{
		$allParams = array_merge($required, $optional);

		// Special case
		if(count($args) === 1)
			// Check if the first argument is an array
			if(is_array($args[0]))
				// ...And if all keys - line
				if(array_reduce(array_keys($args[0]), function($prev, $curr) { return $prev && is_string($curr); }, true))
				{
					foreach($required as $requiredParam)
						if(! isset($args[0][$requiredParam]))
							throw new Exception('It lacks some parameters');

					return $args[0];
				}

		if(count($args) < count($required))
			throw new Exception('It lacks some required parameteres');

		if(count($args) > count($allParams))
			throw new Exception('Too much parameteres');

		$params = [];

		foreach($args as $id => $arg){
			$params[$allParams[$id]] = $arg;
		}

		return $params;
	}

	/**
	 * API Call
	 * @param string $call name to call
	 * @param array $params call parameteres
	 * @return array server response
	 */
	protected function request($call, array $params = [])
	{
		$params = array_merge($params,['user' => $this->username, 'pass' => md5($this->password)]);
		$response = $this->client->call($call, $params);

		return $response;
	}
}