<?php

/**
 * Curl wrapper PHP v2
 * @author hackerone
 */
class Notification
{
	/**
	 * @var array
	 * Default options from config.php
	 */
	public $options = [];

	/**
	 * @var array
	 * request specific options - valid only for single request
	 */
	public $request_options = [];

	/**
	 * @var string
	 * Holds response data right after sending a request.
	 */
	public $response = null;

	/**
	 * @var integer HTTP-Status Code
	 * This value will hold HTTP-Status Code. False if request was not successful.
	 */
	public $responseCode = null;

	/**
	 * @var string
	 * RestFul Url
	 */
	public $url = 'https://fcm.googleapis.com/fcm';

	/**
	 * @var string
	 * Access Key for Auth with google api
	 */
	public $serverKey;

	/**
	 * @var string
	 * params for rest field
	 */
	public $params;

	/**
	 * @var object
	 * Holds cURL-Handler
	 */
	private $_curl = null;

	/**
	 * @var raw
	 * 
	 */
	private $_header, $_headerMap, $_error, $_status, $_info;

	/**
	 * @var array
	 * default config
	 */
	private $_config = array(
		CURLOPT_RETURNTRANSFER => true,
		// CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HEADER		 => false,
		CURLOPT_VERBOSE		=> true,
		CURLOPT_AUTOREFERER	=> true,		 
		CURLOPT_CONNECTTIMEOUT => 30,
		CURLOPT_TIMEOUT		=> 30,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_HTTPHEADER => [
			"cache-control: no-cache",
			"content-type: application/json"
		],
	);

	/**
	 * Initialization
	 *
	 */
	public function init( )
	{
		$this->params['authorization'] = $this->accessKey;

		// Yii::trace( 'Extension initializating', 'RezvanPayment' );
	}

	public static function mergeArray()
	{
		$args=func_get_args();
		$res=array_shift($args);
		while(!empty($args))
		{
			$next=array_shift($args);
			foreach($next as $k => $v)
			{
				if(is_array($v) && isset($res[$k]) && is_array($res[$k]))
					$res[$k]=self::mergeArray($res[$k],$v);
				elseif(is_numeric($k))
					isset($res[$k]) ? $res[]=$v : $res[$k]=$v;
				else
					$res[$k]=$v;
			}
		}
		return $res;
	}

	public function getOptions()
	{
		$options = self::mergeArray($this->request_options, $this->options, $this->_config);
		return $options;
	}

	public function setOption($key, $value, $default = false)
	{
		if($default)
			$this->options[$key] = $value;
		else
			$this->request_options[$key] = $value;

		return $this;
	}

	/**
	 * Clears Options
	 * This will clear only the request specific options. Default options cannot be cleared.
	 */
	public function resetOptions()
	{
		$this->request_options = [];
		return $this;
	}

	/**
	 * Resets the Option to Default option
	 */
	public function resetOption($key)
	{
		if(isset($this->request_options[$key]))
			unset($this->request_options[$key]);
		return $this;
	}

	public function setOptions($options, $default = false)
	{
		if($default)
			$this->options = $options + $this->request_options;
		else
			$this->request_options = $options + $this->request_options;

		return $this;
	}

	public function buildUrl($call, $data=[])
	{
		$parsed = parse_url($this->url . $call);
		
		isset($parsed['query']) ? parse_str($parsed['query'], $parsed['query']) : $parsed['query'] = [];

		$params = isset($parsed['query']) ? $data + $parsed['query'] : $data;
		$parsed['query'] = ($params) ? '?' . http_build_query($params) : '';
		if (!isset($parsed['path'])) {
			$parsed['path']='/';
		}

		$parsed['port'] = isset($parsed['port'])?':'.$parsed['port']:'';

		return $parsed['scheme'].'://'.$parsed['host'].$parsed['port'].$parsed['path'].$parsed['query'];
	}

	public function exec($url, $options)
	{
		$this->_error = null;
		$this->_header = null;
		$this->_headerMap = null;
		$this->_info = null;
		$this->_status = null;

		$this->_curl = curl_init($url);
		curl_setopt_array($this->_curl, $options);

		$output = curl_exec($this->_curl);
		$_response = CJSON::decode($output);

		$this->_status = curl_getinfo($this->_curl, CURLINFO_HTTP_CODE);

		if(!$_response){
			$this->_error = curl_error($this->_curl);
			// $this->_info = curl_getinfo($this->_curl);

			if($this->_error == 7)
				$this->responseCode = 'timeout';
		}
		else if(isset($_response['status'])){
			$this->_error = true;
			$this->_info = $_response['status'];
		}

		curl_close($this->_curl);
// header("Content-type: text/json");
// echo $output;die();

		$this->response = array(
			'error' => $this->_error,
			'errorMessage' => $this->_info,
			'response' => $output
		);

		return CJSON::encode($this->response);
	}

	public function _processHeader($response, $header_size)
	{
		return array(substr($response, 0, $header_size), substr($response, $header_size));
	}

	public function get($call, $params = [])
	{
		$exec_url = $this->buildUrl($call, $params);
		$options = $this->getOptions();
		return $this->exec($exec_url,  $options);
	}

	public function post($call, $data, $params = [])
	{
		$url = $this->buildUrl($call, $params);
		$this->params = array_merge( $this->params, $data );

		$options = $this->getOptions();
		$options[CURLOPT_POST] = true;
		$options[CURLOPT_POSTFIELDS] = $this->params;

		return $this->exec($url, $options);
	}

	/**
	 * Gets header of the last curl call if header was enabled
	 */
	public function getHeaders()
	{
		if(!$this->_header)
			return [];

		if(!$this->_headerMap){

			$headers = explode("\r\n", trim($this->_header));
			$output = [];
			$output['http_status'] = array_shift($headers);

			foreach($headers as $line){
				$params = explode(':', $line, 2);

				if(!isset($params[1]))
					$output['http_status'] = $params[0];
				else
					$output[trim($params[0])] = trim($params[1]);
			}

			$this->_headerMap = $output;
		}
		return $this->_headerMap;
	}

	public function addHeader($header = [])
	{
		$h = isset($this->request_options[CURLOPT_HTTPHEADER]) ? $this->request_options[CURLOPT_HTTPHEADER] : [];
		foreach($header as $k => $v){
			$h[] = $k.': '.$v;
		}

		$this->setHeaders($h);
		return $this;
	}

	public function getHeader($key)
	{
		$headers = array_change_key_case($this->getHeaders(), CASE_LOWER);
		$key = strtolower($key);

		return @$headers[$key];
	}

	public function setHeaders($header = [], $default = false)
	{
		if ($this->_isAssoc($header)) {
			$out = [];
			foreach ($header as $k => $v) {
				$out[] = $k .': '.$v;
			}
			$header = $out;
		}

		$this->setOption(CURLOPT_HTTPHEADER, $header, $default);
		return $this;
	}

	private function _isAssoc($arr)
	{
		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	public function getError()
	{
		return $this->_error;
	}

	public function getInfo()
	{
		return $this->_info;
	}

	public function getStatus()
	{
		return $this->_status;
	}
}