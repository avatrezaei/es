<?php

namespace johnitvn\rbacplus\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use johnitvn\rbacplus\models\Permission;
use johnitvn\rbacplus\models\PermissionSearch;
use yii\caching\TagDependency;
use johnitvn\rbacplus\components\RouteRule;
use johnitvn\rbacplus\components\Configs;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use Exception;

/**
 * PermissionController is controller for manager permissions
 * @author John Martin <john.itvn@gmail.com>
 * @since 1.0.0
 */
class PermissionController extends Controller {

    const CACHE_TAG = 'mdm.admin.route';

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Permission models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PermissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

   
     public function getActionsFromNameSpace($path = NULL,$modulesID=Null) {

        if ($path == NULL)
            $path = Yii::$app->controllerPath;

        //$path = __DIR__;


        $fqcns = array();

        $allFiles = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        $phpFiles = new \RegexIterator($allFiles, '/\.php$/');
        foreach ($phpFiles as $phpFile) {
            $content = file_get_contents($phpFile->getRealPath());
            $tokens = token_get_all($content);
            $namespace = '';
            for ($index = 0; isset($tokens[$index]); $index++) {
                if (!isset($tokens[$index][0])) {
                    continue;
                }
                if (T_NAMESPACE === $tokens[$index][0]) {
                    $index += 2; // Skip namespace keyword and whitespace
                    while (isset($tokens[$index]) && is_array($tokens[$index])) {
                        $namespace .= $tokens[$index++][1];
                    }
                }
                if (T_CLASS === $tokens[$index][0]) {
                    $index += 2; // Skip class keyword and whitespace
                    $fqcns[] = $namespace . '\\' . $tokens[$index][1];
                }
            }
        }
        /*
         * get actions from controller class
         */
        foreach (($fqcns) as $controllerClass) {

            foreach (get_class_methods($controllerClass) as $method) {

                if (!preg_match('/^action\w+$/', $method))
                    continue;


                $action_id = preg_replace('/action/', '', strtolower($method), 1);

                if ($action_id !== 's') {
                    $route = $controllerClass . '_' . $action_id;
                    $controllerclass = substr($controllerClass, strrpos($controllerClass, '\\') + 1);
                    $controller_id = preg_replace('/controller/', '', strtolower($controllerclass), 1);
                    
                    if($modulesID!=null)
                    $route =$modulesID.'_'. $controller_id . '_' . $action_id;
                    else
                    $route = $controller_id . '_' . $action_id;


                    //$item['actions'][$route] = $route;
                    
                    
                    $item[$route] = $route;
                }
            }
        }

        return $item;
    }    
  
     public function getAppRoutesNew() {

        $key = __METHOD__;
        $cache = Configs::instance()->cache;
        // if ($cache === null || ($result = $cache->get($key)) === false) {
        $result = [];
        $moduleName=[];
        $this->getRouteRecrusiveNew(Yii::$app, $result,$moduleName);
        // if ($cache !== null) {
        $cache->set($key, $result, Configs::instance()->cacheDuration, new TagDependency([
            'tags' => self::CACHE_TAG
        ]));
        //     }
        //  }
        return $result;
    }
    
    public function getModulesID( &$result,&$moduleName) {
        
        
            $key = __METHOD__;
        $cache = Configs::instance()->cache;
        // if ($cache === null || ($result = $cache->get($key)) === false) {
 
        $this->getRouteRecrusiveNew(Yii::$app, $result,$moduleName);
                 // die(var_dump($moduleName));

        
   
    }

    /**
     * Get route(s) recrusive
     * @param \yii\base\Module $module
     * @param array $result
     */
    private function getRouteRecrusiveNew($module, &$result,&$moduleName) {

        $token = "Get Route of '" . get_class($module) . "' with id '" . $module->uniqueId . "'";
        Yii::beginProfile($token, __METHOD__);
        try {
            foreach ($module->getModules() as $id => $child) {
                if (($child = $module->getModule($id)) !== null) {
                    $this->getRouteRecrusiveNew($child, $result,$moduleName);
                }
            }
//            
//            foreach ($module->controllerMap as $id => $type) {
//                $this->getControllerActions($type, $id, $module, $result);
//            }
            
           $result[] = ($module->controllerPath) ;
           $moduleName[] = ($module->id) ;
//           
//           
//            $namespace = trim($module->controllerNamespace, '\\') . '\\';
//            //$result[] = '-------------';
//           $result[] = ($namespace) ;

           // die(var_dump($namespace));
           // $this->getControllerFiles($module, $namespace, '', $result);
           // $result[] = ($module->uniqueId === '' ? '' : '/' . $module->uniqueId) . '/*';
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }
    
    /*
     * =========================================================================
     */
    /**
     * Displays a single Permission model.
     * @param string $name
     * @return mixed
     */
    public function actionView($name) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => $name,
                'content' => $this->renderPartial('view', [
                    'model' => $this->findModel($name),
                ]),
                'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('rbac', 'Edit'), ['update', 'name' => $name], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($name),
            ]);
        }
    }

    /**
     * Creates a new Permission model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate_bynamespace() {
        $request = Yii::$app->request;
        $model = new Permission(null);

        if ($request->isAjax) {
            
        /*provide all actions or permision create*/
        $backendControllerArray = $this->getActionsFromNameSpace(Yii::$app->controllerPath);
        $frontendControllerArray = $this->getActionsFromNameSpace('/var/www/html/test/advanced-rbac-plus/frontend/controllers');
        
        $controllerArray=array();
        $routes = $this->getAppRoutesNew();
        foreach ($routes as $key => $value) {
            $controllerArray[]=$this->getActionsFromNameSpace($value);
        }
      
        $controllerArray= array_merge($controllerArray, $backendControllerArray);
        $controllerArray= array_merge($controllerArray, $frontendControllerArray);
//        echo '<pre>';
//        die(var_dump(($controllerArray)));
        /*---------------------------------------*/
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('rbac', "Create new {0}", ["Permission"]),
                    'content' => $this->renderPartial('create', [
                        'model' => $model,'controllerArray'=>$controllerArray
                    ]),
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('rbac', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => 'true',
                    'title' => Yii::t('rbac', "Create new {0}", ["Permission"]),
                    'content' => '<span class="text-success">' . Yii::t('rbac', "Have been create new {0} success", ["Permission"]) . '</span>',
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('rbac', 'Create More'), ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('rbac', "Create new {0}", ["Permission"]),
                    'content' => $this->renderPartial('create', [
                        'model' => $model,'controllerArray'=>$controllerArray
                    ]),
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('rbac', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'name' => $model->name]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                             'controllerArray'=>$controllerArray
                ]);
            }
        }
    }
    
    
    public function actionInitial() {

   
        
        if( \Yii::$app->getModule('rbac')->initialPermisionIntoDB)
        {
            /*------------------------ first clear table -------------------*/
            $authDBManager=Yii::$app->authManager;
            
            //$authDBManager->removeAll();
            //$authDBManager->removeAllAssignments();
            //$authDBManager->removeAllRoles();
            //$authDBManager->removeAllRules();
            //$authDBManager->removeAllRules();
            $authDBManager->removeAllPermissions();//permition authitem by type2
         //   \Yii::$app->db->createCommand("Delete FROM " . $authDBManager->itemTable . "  WHERE 1 ")->execute();
            /******************************************************************************** */
            
            
            
        /*provide all actions or permision create*/
        $backendControllerArray = $this->getActionsFromNameSpace(Yii::$app->controllerPath);
        $frontendControllerArray = $this->getActionsFromNameSpace('/var/www/html/test/advanced-rbac-plus/frontend/controllers');
        
        $controllerArray=array();
        $result = [];
        $moduleName=[];
         $this->getModulesID($result,$moduleName);
        
        foreach ($moduleName as $key => $modulesID) {
             if (($module =  Yii::$app->getModule($modulesID)) !== null) {
                   $controllerArray[]=$this->getActionsFromNameSpace($module->controllerPath,$modulesID);
                }
        }
      
        $controllerArray= array_merge($controllerArray, $backendControllerArray);
        $controllerArray= array_merge($controllerArray, $frontendControllerArray);
            foreach($controllerArray as $key=>$actions)
            {
                if(is_array($actions))
                {
   foreach($actions as $action)
                {
                    
                    $permisionObj=new  Permission(null);
            $permisionObj->name = $action;
            $permisionObj->description = $action;
            $permisionObj->save();
                    
                }
                }else
                {
            $permisionObj=new  Permission(null);
            $permisionObj->name = $actions;
            $permisionObj->description = $actions;    
                        $permisionObj->save();
}
             
            }
        }
                        return $this->redirect(['index']);


    }

 public function actionCreate() {
        $request = Yii::$app->request;
        $model = new Permission(null);

        if ($request->isAjax) {
            
        /*provide all actions or permision create*/
        $backendControllerArray = $this->getActionsFromNameSpace(Yii::$app->controllerPath);
        $frontendControllerArray = $this->getActionsFromNameSpace('/var/www/html/test/advanced-rbac-plus/frontend/controllers');
        
        $controllerArray=array();
        $result = [];
        $moduleName=[];
         $this->getModulesID($result,$moduleName);
        
        foreach ($moduleName as $key => $modulesID) {
             if (($module =  Yii::$app->getModule($modulesID)) !== null) {
                   $controllerArray[]=$this->getActionsFromNameSpace($module->controllerPath,$modulesID);
                }
        }
      
        $controllerArray= array_merge($controllerArray, $backendControllerArray);
        $controllerArray= array_merge($controllerArray, $frontendControllerArray);
        
        if( \Yii::$app->getModule('rbac')->initialPermisionIntoDB)
        {
            /*------------------------ first clear table -------------------*/
            $authDBManager=Yii::$app->authManager;
            
            //$authDBManager->removeAll();
            //$authDBManager->removeAllAssignments();
            //$authDBManager->removeAllRoles();
            //$authDBManager->removeAllRules();
            //$authDBManager->removeAllRules();
            $authDBManager->removeAllPermissions();
         //   \Yii::$app->db->createCommand("Delete FROM " . $authDBManager->itemTable . "  WHERE 1 ")->execute();
            /******************************************************************************** */
            
            
            foreach($controllerArray as $key=>$actions)
            {
                if(is_array($actions))
                {
   foreach($actions as $action)
                {
                    
                    $permisionObj=new  Permission(null);
            $permisionObj->name = $action;
            $permisionObj->description = $action;
            $permisionObj->save();
                    
                }
                }else
                {
            $permisionObj=new  Permission(null);
            $permisionObj->name = $actions;
            $permisionObj->description = $actions;    
                        $permisionObj->save();
}
             
            }
        }
//        echo '<pre>';
        /*---------------------------------------*/
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('rbac', "Create new {0}", ["Permission"]),
                    'content' => $this->renderPartial('create', [
                        'model' => $model,'controllerArray'=>$controllerArray
                    ]),
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('rbac', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => 'true',
                    'title' => Yii::t('rbac', "Create new {0}", ["Permission"]),
                    'content' => '<span class="text-success">' . Yii::t('rbac', "Have been create new {0} success", ["Permission"]) . '</span>',
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('rbac', 'Create More'), ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('rbac', "Create new {0}", ["Permission"]),
                    'content' => $this->renderPartial('create', [
                        'model' => $model,'controllerArray'=>$controllerArray
                    ]),
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('rbac', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'name' => $model->name]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                             'controllerArray'=>$controllerArray
                ]);
            }
        }
    }

   
    /**
     * Updates an existing Permission model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param string $name
     * @return mixed
     */
    public function actionUpdate($name) {
        $request = Yii::$app->request;
        $model = $this->findModel($name);

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('rbac', "Update {0}", ['"' . $name . '" Permission']),
                    'content' => $this->renderPartial('update', [
                        'model' => $this->findModel($name),
                    ]),
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('rbac', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => 'true',
                    'title' => $name,
                    'content' => $this->renderPartial('view', [
                        'model' => $this->findModel($name),
                    ]),
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('rbac', 'Edit'), ['update', 'name' => $name], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('rbac', "Update {0}", ['"' . $name . '" Permission']),
                    'content' => $this->renderPartial('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('rbac', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('rbac', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'name' => $model->name]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Permission model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $name
     * @return mixed
     */
    public function actionDelete($name) {
        $request = Yii::$app->request;
        $this->findModel($name)->delete();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => true];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Permission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $name
     * @return Permission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($name) {
        if (($model = Permission::find($name)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('rbac', 'The requested page does not exist.'));
        }
    }

    /*     * ******************* Functions for get actions **************************** */

    /**
     * Get list of application routes
     * @return array
     */
    public function getAppRoutes() {

        $key = __METHOD__;
        $cache = Configs::instance()->cache;
        // if ($cache === null || ($result = $cache->get($key)) === false) {
        $result = [];

        $this->getRouteRecrusive(Yii::$app, $result);
        // if ($cache !== null) {
        $cache->set($key, $result, Configs::instance()->cacheDuration, new TagDependency([
            'tags' => self::CACHE_TAG
        ]));
        //     }
        //  }
        return $result;
    }

    /**
     * Get route(s) recrusive
     * @param \yii\base\Module $module
     * @param array $result
     */
    private function getRouteRecrusive($module, &$result) {

        $token = "Get Route of '" . get_class($module) . "' with id '" . $module->uniqueId . "'";
        Yii::beginProfile($token, __METHOD__);
        try {
            foreach ($module->getModules() as $id => $child) {
                if (($child = $module->getModule($id)) !== null) {
                    $this->getRouteRecrusive($child, $result);
                }
            }

            foreach ($module->controllerMap as $id => $type) {

                $this->getControllerActions($type, $id, $module, $result);
            }
            $namespace = trim($module->controllerNamespace, '\\') . '\\';
            //die(var_dump($namespace));
            $this->getControllerFiles($module, $namespace, '', $result);
            $result[] = ($module->uniqueId === '' ? '' : '/' . $module->uniqueId) . '/*';
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

/**
     * Get list controller under module
     * @param \yii\base\Module $module
     * @param string $namespace
     * @param string $prefix
     * @param mixed $result
     * @return mixed
     */

    private function getControllerFiles2($module, $namespace, $prefix, &$result) {
        $path = @Yii::getAlias('@' . str_replace('\\', '/', $namespace));
        $token = "Get controllers from '$path'";
        Yii::beginProfile($token, __METHOD__);
        try {
            if (!is_dir($path)) {
                return;
            }
            foreach (scandir($path) as $file) {
                if ($file == '.' || $file == '..') {
                    continue;
                }
                if (is_dir($path . '/' . $file)) {
                    $this->getControllerFiles($module, $namespace . $file . '\\', $prefix . $file . '/', $result);
                } elseif (strcmp(substr($file, -14), 'Controller.php') === 0) {
                    $id = Inflector::camel2id(substr(basename($file), 0, -14));
                    $className = $namespace . Inflector::id2camel($id) . 'Controller';
                    if (strpos($className, '-') === false && class_exists($className) && is_subclass_of($className, 'yii\base\Controller')) {
                        $this->getControllerActions($className, $prefix . $id, $module, $result);
                    }
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    /**
     * Get list controller under module
     * @param \yii\base\Module $module
     * @param string $namespace
     * @param string $prefix
     * @param mixed $result
     * @return mixed
     */
    private function getControllerFiles($module, $namespace, $prefix, &$result) {
        $path = @Yii::getAlias('@' . str_replace('\\', '/', $namespace));
        $token = "Get controllers from '$path'";
        Yii::beginProfile($token, __METHOD__);
        try {
            if (!is_dir($path)) {
                return;
            }
            foreach (scandir($path) as $file) {
                if ($file == '.' || $file == '..') {
                    continue;
                }
                if (is_dir($path . '/' . $file)) {
                    $this->getControllerFiles($module, $namespace . $file . '\\', $prefix . $file . '/', $result);
                } elseif (strcmp(substr($file, -14), 'Controller.php') === 0) {
                    $id = Inflector::camel2id(substr(basename($file), 0, -14));
                    $className = $namespace . Inflector::id2camel($id) . 'Controller';
                    if (strpos($className, '-') === false && class_exists($className) && is_subclass_of($className, 'yii\base\Controller')) {
                        $this->getControllerActions($className, $prefix . $id, $module, $result);
                    }
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    /**
     * Get list action of controller
     * @param mixed $type
     * @param string $id
     * @param \yii\base\Module $module
     * @param string $result
     */
    private function getControllerActions($type, $id, $module, &$result) {
        $token = "Create controller with cofig=" . VarDumper::dumpAsString($type) . " and id='$id'";
        Yii::beginProfile($token, __METHOD__);
        try {
            /* @var $controller \yii\base\Controller */
            $controller = Yii::createObject($type, [$id, $module]);
            //    $controller133 = Yii::createObject('backend\controllers\SiteController');
            //   echo '<pre>';
            //   die(var_dump(''));
            $this->getActionRoutes($controller, $result);
            $result[] = '/' . $controller->uniqueId . '/*';
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    /**
     * Get route of action
     * @param \yii\base\Controller $controller
     * @param array $result all controller action.
     */
    private function getActionRoutes($controller, &$result) {
        $token = "Get actions of controller '" . $controller->uniqueId . "'";
        Yii::beginProfile($token, __METHOD__);
        try {
            $prefix = '/' . $controller->uniqueId . '/';
            foreach ($controller->actions() as $id => $value) {
                $result[] = $prefix . $id;
            }
            $class = new \ReflectionClass($controller);
            foreach ($class->getMethods() as $method) {
                $name = $method->getName();
                if ($method->isPublic() && !$method->isStatic() && strpos($name, 'action') === 0 && $name !== 'actions') {
                    $result[] = $prefix . Inflector::camel2id(substr($name, 6));
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    /* ================================================= */

    public function actionDynamicAc() {

        if (isset($_POST['controller_id']) && is_string($_POST['controller_id'])) {
            $controller_id = $_POST['controller_id'];
            $controllers = $this->getAppControllers();
            foreach ($controllers as $id => $item) {
                if ($id == $controller_id) {
                    $actions = $item['actions'];
                    if (is_array($actions)) {
                        foreach ($actions as $key => $value) {
                            echo CHtml::tag('option', array('value' => $key), CHtml::encode($value), true);
                        }
                    } else {
                        echo CHtml::tag('option', array('value' => 0), 'موردی پیدا نشد', true);
                    }
                }
            }
        }
    }

    /**
     * Return an array of controllers and their actions.
     */
    public function getAppControllers() {
        $controllers = array();
        $directories = array(
            Yii::$app->controllerPath,
        );
        //$directories = $this->scanModules(Yii::$app->getModules());
        //array_unshift($directories, Yii::$app->controllerPath);

        foreach ($directories as $module => $dir) {

            foreach (glob($dir . DIRECTORY_SEPARATOR . '*Controller.php') as $controller) {
                $class = pathinfo($controller, PATHINFO_FILENAME);
               // die(var_dump(('backend\controllers\\' . $class)));

                if (!class_exists($class, FALSE))
                    require($controller);
               // die(var_dump(class_exists('backend\controllers' . $class, FALSE)));
                if (!class_exists($class, FALSE) || !is_subclass_of($class, 'CController'))
                    continue;

                $controller_id = preg_replace('/controller/', '', strtolower($class), 1);

                $item = array(
                    'module' => is_string($module) ? $module : NULL,
                    'actions' => array(),
                );

                foreach (get_class_methods($class) as $method) {
                    if (!preg_match('/^action\w+$/', $method))
                        continue;

                    $action_id = preg_replace('/action/', '', strtolower($method), 1);

                    if ($action_id !== 's') {
                        $route = $controller_id . '_' . $action_id;

                        if (is_string($module))
                            $route = $module . '_' . $route;

                        //$item['actions'][$action_id] = $route;
                        $item['actions'][$route] = $route;
                    }
                }

                $controllers[$controller_id] = $item;
            }
        }

        return $controllers;
    }

    /**
     * Return an array of controller directories
     */
    function scanModules($modules = array(), $parent = NULL) {
        $directories = array();

        foreach ($modules as $id => $mod) {
            $module = $parent ? $parent->getModule($id) : Yii::$app->getModule($id);

            $directories[$id] = $module->controllerPath;

            $subModules = $module->getModules();
            if (!empty($subModules)) {
                foreach ($subModules as $sId => $sMod) {
                    $directories = array_merge($directories, $this->scanModules($subModules, $module));
                }
            }
        }
        return $directories;
    }

}
