<?php
namespace common\components;

use Yii;
use yii\helpers\Html;

use yii\base\InvalidConfigException;
use yii\base\Widget;

use yii\data\Pagination;

class RezvanPagination  extends \yii\widgets\LinkPager
{
	public function init()
	{
		parent::init();
	}

	public function run()
	{
		parent::run();
	}

	protected function renderPageButton($label, $page, $class, $disabled, $active)
	{
		$options = ['class' => $class === '' ? null : $class];
		if ($active) {
			Html::addCssClass($options, $this->activePageCssClass);
		}
		if ($disabled) {
			Html::addCssClass($options, $this->disabledPageCssClass);

			return Html::tag('li', Html::tag('span', $label), $options);
		}
		$linkOptions = $this->linkOptions;
		$linkOptions['data-page'] = $page + 1;
		$linkOptions['class'] = 'rezvan-link';

		return Html::tag('li', Html::a($label, '#'.$page, $linkOptions), $options);
	}
}
