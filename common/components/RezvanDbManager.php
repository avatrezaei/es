<?php
 
namespace common\components;
 
use Yii;
use yii\db\Connection;
use yii\db\Query;
use yii\db\Expression;
use yii\base\InvalidCallException;
use yii\base\InvalidParamException;
use yii\di\Instance;
use yii\rbac\DbManager;
use yii\rbac\Item;
use common\components\RezvanAssignment;
 
 
 
/**
 * Description of MyDbManager
 *
 * @author ctala
 */
class RezvanDbManager extends DbManager {

	/**
	 * Get event id
	 */
	public function getEventId()
	{
		return Yii::$app->user->identity->eventId;
	}

	/**
	 * @inheritdoc
	 */
	public function getRolesByUser($userId)
	{
		if (empty($userId)) {
			return [];
		}

		$eventId = $this->getEventId();

		$query = (new Query)->select('b.*')
			->from(['a' => $this->assignmentTable, 'b' => $this->itemTable])
			->where('{{a}}.[[item_name]]={{b}}.[[name]]')
			->andWhere(['a.user_id' => (string) $userId])
			->andWhere(['a.event_id' => (string) $eventId])
			->andWhere(['b.type' => Item::TYPE_ROLE]);

		$roles = [];
		foreach ($query->all($this->db) as $row) {
			$roles[$row['name']] = $this->populateItem($row);
		}
		return $roles;
	}

	/**
	 * @inheritdoc
	 */
	public function AdminAccess($userId, $adminRoleName)
	{
		if (empty($userId)) {
			return null;
		}

		$row = (new Query)->from($this->assignmentTable)
			->where(['user_id' => (string) $userId, 'item_name' => $adminRoleName])
			->one($this->db);

		if ($row === false) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function getAssignment($roleName, $userId)
	{
		if (empty($userId)) {
			return null;
		}

		$eventId = $this->getEventId();

		$row = (new Query)->from($this->assignmentTable)
			->where(['user_id' => (string) $userId, 'event_id' => (string) $eventId, 'item_name' => $roleName])
			->one($this->db);

		if ($row === false) {
			return null;
		}

		return new RezvanAssignment([
			'userId' => $row['user_id'],
			'eventId' => $row['event_id'],
			'roleName' => $row['item_name'],
			'createdAt' => $row['created_at'],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function getAssignments($userId)
	{
		if (empty($userId)) {
			return [];
		}

		$eventId = $this->getEventId();

		$query = (new Query)
			->from($this->assignmentTable)
			->where(['user_id' => (string) $userId, 'event_id' => (string) $eventId]);

		$assignments = [];
		foreach ($query->all($this->db) as $row) {
			$assignments[$row['item_name']] = new RezvanAssignment([
				'userId' => $row['user_id'],
				'eventId' => $row['event_id'],
				'roleName' => $row['item_name'],
				'createdAt' => $row['created_at'],
			]);
		}

		return $assignments;
	}

	/**
	 * Returns all role assignment information for the specified user and event.
	 * @param string|integer $userId the user ID (see [[\yii\web\User::id]])
	 * @param string|integer $eventId the event ID (see [[\yii\web\Event::id]])
	 * @return Assignment[] the assignments indexed by role names. An empty array will be
	 * returned if there is no role assigned to the user in event.
	 */
	public function getAssignmentsWithEvent($userId, $eventId)
	{
		if (empty($userId)) {
			return [];
		}

		$query = (new Query)
			->from($this->assignmentTable)
			->where(['user_id' => (string) $userId, 'event_id' => (string) $eventId]);

		$assignments = [];
		foreach ($query->all($this->db) as $row) {
			$assignments[$row['item_name']] = new RezvanAssignment([
				'userId' => $row['user_id'],
				'eventId' => $row['event_id'],
				'roleName' => $row['item_name'],
				'createdAt' => $row['created_at'],
			]);
		}

		return $assignments;
	}

	/**
	 * @inheritdoc
	 */
	public function assign($role, $userId)
	{
		$eventId = $this->getEventId();

		$assignment = new RezvanAssignment([
			'userId' => $userId,
			'eventId' => $eventId,
			'roleName' => $role->name,
			'createdAt' => time(),
		]);


		$this->db->createCommand()
			->insert($this->assignmentTable, [
				'user_id' => $assignment->userId,
				'event_id' => $assignment->eventId,
				'item_name' => $assignment->roleName,
				'created_at' => $assignment->createdAt,
			])->execute();

		return $assignment;
	}

	/**
	 * @inheritdoc
	 */
	public function assignByEventId($role, $userId, $eventId)
	{
		$assignment = new RezvanAssignment([
			'userId' => $userId,
			'eventId' => $eventId,
			'roleName' => $role->name,
			'createdAt' => time(),
		]);


		$this->db->createCommand()
			->insert($this->assignmentTable, [
				'user_id' => $assignment->userId,
				'event_id' => $assignment->eventId,
				'item_name' => $assignment->roleName,
				'created_at' => $assignment->createdAt,
			])->execute();

		return $assignment;
	}

	/**
	 * @inheritdoc
	 */
	public function revoke($role, $userId)
	{
		if (empty($userId)) {
			return false;
		}

		$eventId = $this->getEventId();

		return $this->db->createCommand()
			->delete($this->assignmentTable, ['user_id' => (string) $userId, 'event_id' => (string) $eventId, 'item_name' => $role->name])
			->execute() > 0;
	}

	/**
	 * Returns all role assignment information for the specified role.
	 * @param string $roleName
	 * @return Assignment[] the assignments. An empty array will be
	 * returned if role is not assigned to any user.
	 * @since 2.0.7
	 */
	public function getUserIdsByRole($roleName)
	{
		if (empty($roleName)) {
			return [];
		}

		$eventId = $this->getEventId();

		return (new Query)->select('[[user_id]]')
			->from($this->assignmentTable)
			->where(['item_name' => $roleName, 'event_id' => $eventId])->column($this->db);
	}
}