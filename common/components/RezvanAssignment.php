<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components;

use Yii;
use yii\base\Object;
use yii\rbac\Assignment;

/**
 * Assignment represents an assignment of a role to a user.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class RezvanAssignment extends Assignment
{
    /**
     * @var string|integer user ID (see [[\yii\web\User::id]])
     */
    public $userId;
    /**
     * @var string the role name
     */
    public $roleName;
	/**
	 * @var string|integer event ID (see [[\yii\web\Event::id]])
	 */
	public $eventId;
	/**
	 * @var integer UNIX timestamp representing the assignment creation time
	 */
	public $createdAt;
}
