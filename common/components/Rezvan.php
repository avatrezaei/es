<?php
namespace common\components;

use yii\helpers\Html;
use jDate;
/**
 * Rezvan represents the data needed to String Functions
 * 
 */
class Rezvan extends Html
{
	
	/**
	 *  Returns the first $wordsreturned out of $string.  If string
	 *	contains more words than $wordsreturned, the entire string is returned
	 *  @return the first n words from a string
	 */	
	static function get_snippet( $str, $wordCount = 10, $more = false) 
	{
		if($str == '')
			return false;

		$str_len = self::mb_str_word_count($str);

		$word = implode( 
			'', 
			array_slice( 
				preg_split(
					'/([\s,\.;\?\!]+)/', 
					$str, 
					$wordCount*2+1, 
					PREG_SPLIT_DELIM_CAPTURE
				),
				0,
				$wordCount*2-1
			)
		);

		return $word . ($more && ($str_len > $wordCount) ? '...' : '');
	}

	static function mb_str_word_count($string, $format = 0, $charlist = '[]') {
		mb_internal_encoding( 'UTF-8');
		mb_regex_encoding( 'UTF-8');

		$words = mb_split('[^\x{0600}-\x{06FF}]', $string);
		switch ($format) {
			case 0:
				return count($words);
				break;
			case 1:
			case 2:
				return $words;
				break;
			default:
				return $words;
				break;
		}
	}

	/**
	 * Convert Georgian Date to Persian Date with Special Format.
	 * @param string
	 */
	static function PersianDate($value, $format=null, $convert=true) {
		$out = null;

		if($value == null || $value == '0000-00-00 00:00:00'){
			$out = '---';
		}

		if(!is_numeric($value) && is_string($value)){
			$value = strtotime($value);
		}

		if ($format != null) {
			$out = \Yii::$app->jdate->date($format, $value);
		}
		else{
			$out = \Yii::$app->jdate->date("H:i:s - Y/m/d", $value);
		}

		if($convert)
			$out = self::convertNumbers($out);

		return $out;
	}

	/**
	 * Convert Georgian Date to Persian Date with Special Format.
	 * @param string
	 */
	static function PersianTime($value, $format=null) {
		$out = null;
		$len = strlen($value);

		if($value == null || $value == '00:00:00'){
			$out = '---';
		}

		if($len == 8)
			$value = substr($value, 0, -3);

		$out = self::convertNumbers($value);

		return $out;
	}

	public static function cleanText($text=""){
		$text = preg_replace('/[^A-Z0-9]+/i', '-', $text);
		$text = strtolower(trim($text, '-'));

		return $text;
	}

	/**
	 * Convert string to integer number.
	 * @param string currency number
	 * @return integer number
	 */
	public static function toInt($str)
	{
		return (int)preg_replace("/\..+$/i", "", preg_replace("/[^0-9\.]/i", "", $str));
	}

	public static function convert($number)
	{
		return self::convertNumbers($number);
	}

	private static function convertNumbers($matches)
	{
		$farsi_array = array("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹");
		$english_array = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
		return str_replace($english_array, $farsi_array, $matches);
	}

	/**
	 * Convert string to integer number.
	 * @param integer amount
	 * @param boolean for show currency text
	 * @param string format currency
	 * @return string amount
	 */
	public static function toCurrency($amount, $format=false, $showCurrency=true, $convert=false)
	{
		$out = '';

		if(!$format)
			$out = \Yii::$app->formatter->asDecimal($amount) . ($showCurrency ? ' ' . \Yii::t('zii', 'Rials') : '');
		else
			$out = \Yii::$app->formatter->asDecimal($amount);

		if($convert)
			$out = self::convertNumbers($out);
		return $out;
	}

	/**
	* Is RTL
	* Check if there RTL characters (Arabic, Persian, Hebrew)
	*
	* @author	Khaled Attia <sourcecode@khal3d.com>
	* @param	String $string
	* @return	bool
	*/
	public static function is_rtl( $string ) {
		$rtl_chars_pattern = '/[\x{0590}-\x{05ff}\x{0600}-\x{06ff}]/u';
		return preg_match($rtl_chars_pattern, $string);
	}

	/**
	* Is Validate Iranian National Code
	* Check the National Code
	*
	* @author	Mohammad Ebrahim Amini (www.dkr.co.ir)
	* @param	String $string
	* @return	bool
	*/
	public static function isValidNationalCode($input) {
		# check if input has 10 digits that all of them are not equal
		if (strlen($input) == 10)
		{
			if ($input == '1111111111' || $input == '0000000000' || $input == '2222222222' || $input == '3333333333' || $input == '4444444444' || $input == '5555555555' || $input == '6666666666' || $input == '7777777777' || $input == '8888888888' || $input == '9999999999' || $input == '0123456789'){
				return false;
			}

			$c = substr($input,9,1);

			$n = substr($input,0,1) * 10 +
				substr($input,1,1) * 9 +
				substr($input,2,1) * 8 +
				substr($input,3,1) * 7 +
				substr($input,4,1) * 6 +
				substr($input,5,1) * 5 +
				substr($input,6,1) * 4 +
				substr($input,7,1) * 3 +
				substr($input,8,1) * 2;
			$r = $n - (int)($n / 11) * 11;

			if (($r == 0 && $r == $c) || ($r == 1 && $c == 1) || ($r > 1 && $c == 11 - $r)){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	public static function randCode($count=8, $char=true) {
		$chars = !$char ? '0123456789'
			: 'abcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$data = '';
		$m = strlen($chars) - 1;
		for ($i=0; $i < $count; $i++)
			$data .= $chars[mt_rand(0,$m)];
		return $data;
	}

	public static function __rand_seed($value=0) {
		// Form a 32-bit figure for the random seed with the lower 16-bits
		// the microseconds of the current time, and the upper 16-bits from
		// received value
		$seed = ((int) $value % 65535) << 16;
		$seed += (int) ((double) microtime() * 1000000) % 65535;
		mt_srand($seed);
	}
}