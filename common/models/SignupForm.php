<?php
namespace common\models;

use backend\models\Userd;
use common\models\User;
use common\models\UserSearch;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $repeatpassword;
    public $username;
    public $email;
    public $password;
	public $last_name;
	public $name;
	public $student_number;
	public $universityId;
	public $nationalId;
    public $type;
    public $roles;
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => Yii::t('app','ID'),
            'username'              => Yii::t('app','Username'),
            'password'              => Yii::t('app','Password'),
            'salt'                  => Yii::t('app','Salt'),
            'name'                  => Yii::t('app','Name'),
            'last_name'             => Yii::t('app','Last Name'),
            'organization_id'       => Yii::t('app','Organization'),
            'gender'                => Yii::t('app','Gender'),
            'marital'               => Yii::t('app','Marital'),
            'homepage'              => Yii::t('app','Homepage'),
            'tel'                   => Yii::t('app','Tel'),
            'mobile'                => Yii::t('app','Mobile'),
            'email'                 => Yii::t('app','Email'),
            'type_assist'           => Yii::t('app','Type Assist'),
            'login_attemp_count'    => Yii::t('app','Login Attemp Count'),
            'updated_at'            => Yii::t('app','Update At'),
            'update_date'           => Yii::t('app','Update Date'),
            'created_at'            => Yii::t('app','Create At'),
            'create_date'           => Yii::t('app','Create Date'),
            'last_login_time'       => Yii::t('app','Last Login Time'),
            'last_login_attemp_time' => Yii::t('app','Last Login Attemp Time'),
            'lastaction'            => Yii::t('app','Last Action'),
            'lock'                  => Yii::t('app','Lock'),
            'valid'                 => Yii::t('app','Valid'),
            'active'                => Yii::t('app','Active'),
            'role'                  => Yii::t('app','Role'),
            'lang'                  => Yii::t('app','Language'),
            'passwordInput'         => Yii::t('app','passwordInput'),
            'passwordInput_repeat'  => Yii::t('app','passwordInput Repeat'),
            'sportNo'               => Yii::t('app','Sport Number'),
            'eName'                 => Yii::t('app','English Name'),
            'eFamily'               => Yii::t('app','English Family'),
            'fatherName'            => Yii::t('app','Father Name'),
            'idNo'                  => Yii::t('app','Id Number'),
            'birthDate'             => Yii::t('app','BirthDate'),
            'birthCityId'           => Yii::t('app','BirthCity'),
            'user_picture'          =>Yii::t('app', 'Avatar Pic'),
            'uniname'       =>Yii::t('app','University Name'),
            'fileAttr'      =>Yii::t('app','Please choose your image'),
            'fileAttr1'     =>Yii::t('app','Please choose your image1'),
            'fileAttr2'     =>Yii::t('app','Please choose your image2'),
            'fileAttr3'     =>Yii::t('app','Please choose your image3'),
            'field'                 =>Yii::t('app','Field'),
            'birth_certificate_picture_face'=>Yii::t('app', 'صفحه اول شناسنامه'),
            'birth_certificate_picture_back'=>Yii::t('app', 'صفحه دوم شناسنامه'),
            'national_card_pciture'=>Yii::t('app', 'تصویر کارت ملی'),
            'nationalId'=>Yii::t('app', 'National Id'),
            'creatorUserId'=>Yii::t('app', 'Creator User Id'),
            'teamId'=>Yii::t('app', 'Team'),
            'caravanId'=>Yii::t('app', 'Caravan'),
            'userType'=>Yii::t('app', 'User Type'),
            'userPosition'=>Yii::t('app', 'User Position'),
            'athleteSubFieldId'=>Yii::t('app', 'Athlete Sub Field Id'),
            //'university_name'=>Yii::t('app', 'University Name'),
            'student_number'=>Yii::t('app', 'Student Number'),
            'city_location_id'=>Yii::t('app', 'شهر محل سکونت'),
            'address'=>Yii::t('app', 'Address'),
            'jobincome'=>Yii::t('app', 'Job in Come'),
            'marital'=>Yii::t('app', 'وضعیت تاهل'),
            'shift'=>Yii::t('app', 'دوره'),
            'startingyear'=>Yii::t('app', 'سال ورود'),
            'startingsemester'=>Yii::t('app', 'نیمسال ورود'),
            'units'=>Yii::t('app', 'تعداد واحد گذرانده شده'),
            'major'=>Yii::t('app', 'گرایش'),
            'universityId'=>Yii::t('app','University Name'),
            'fullName' => Yii::t('app','Full Name'),
            'roles' => Yii::t('app','Roles'),
            'repeatpassword' => Yii::t('app','Repeat Password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
             
//            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
//            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This National ID has already been taken.'],
         ['nationalId', 'integer'],

            ['email', 'filter', 'filter' => 'trim'],
             
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
			
			['name', 'filter', 'filter' => 'trim'],
           
			
			['last_name', 'filter', 'filter' => 'trim'],
             
			
			['universityId', 'filter', 'filter' => 'trim'],
            
			
			
			['student_number', 'filter', 'filter' => 'trim'],


            [['username', 'password','repeatpassword','name','last_name'], 'required'],
            ['password', 'string', 'min' => 8],
            ['repeatpassword', 'compare', 'compareAttribute'=>'password', 'message'=>Yii::t('app',"Passwords don't match"),]
        ];
    }
    /**
     *
     * Block comment
     *
     */
     public function scenarios()
 
        {
 
            $scenarios = parent::scenarios();
 
            $scenarios['create_student'] = ['name','last_name','email','universityId','username','student_number','password','repeatpassword'];
            $scenarios['create_user']    = ['name','last_name','email','nationalId','username','password','repeatpassword'];
 
            return $scenarios;
 
        }
    

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($rolename)
    {
        if ($this->validate()) {
            $user = new User();
            if($rolename=='student')
            {
                $user->nationalId = $this->username;
                $user->universityId = $this->universityId;
                $user->student_number = $this->student_number;
            }

            else
                $user->nationalId = $this->nationalId;
            $user->email = $this->email;
			$user->name = $this->name;
			$user->last_name = $this->last_name;
            $user->username = $this->username;
            //$user->type     = 0;
            $user->type = $this->type;

            




            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                $userdetail=new Userd();
                $userdetail->user_id=$user->id;
                $userdetail->save(false);
                $manager = Yii::$app->getAuthManager();
                $name = $rolename;
                $manager = Yii::$app->authManager;
                $item = $manager->getRole($name);
            
            $manager->assign($item, $user->id);
                return $user;
            }
        }


        return null;
    }
    public function origsignup($rolename)
    {
        if ($this->validate()) {
            $user = new User();

            $user->username = $this->username;
            $user->email = $this->email;
            $user->name = $this->name;
            $user->last_name = $this->last_name;
            $user->universityId = $this->universityId;
            $user->student_number = $this->student_number;
            $user->nationalId = $this->username;
            $user->type     = 0;
            //$user->type = $this->type;






            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                $userdetail=new Userd();
                $userdetail->user_id=$user->id;
                $userdetail->save(false);
                $manager = Yii::$app->getAuthManager();
                $name = $rolename;
                $manager = Yii::$app->authManager;
                $item = $manager->getRole($name);

                $manager->assign($item, $user->id);
                return $user;
            }
        }


        return null;
    }
}
