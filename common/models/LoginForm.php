<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Login form
 */
class LoginForm extends Model
{
	const SCENARIO_LOGIN	= 'login';
	const SCENARIO_CAPTCHA  = 'withCaptcha';

	public $username;
	public $password;
	public $rememberMe = true;
	public $verifyCode;
    public $nationalId;
	private $_user;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			// username and password are both required
			[['username', 'password'], 'required', 'on' => self::SCENARIO_LOGIN],
			['username', 'match','pattern' =>'/^[A-Za-z0-9_-]+$/u'],
			// rememberMe must be a boolean value
			['rememberMe', 'boolean'],
			// password is validated by validatePassword()
			['password', 'validatePassword'],
			['verifyCode', 'captcha', 'captchaAction'=> Url::to('/login/captcha'), 'message'=> Yii::t('yii', 'The verification code is incorrect.')],
			[['username', 'password', 'verifyCode'], 'required', 'on' => self::SCENARIO_CAPTCHA],
		];
	}

	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_LOGIN] = ['username', 'password'];
		$scenarios[self::SCENARIO_CAPTCHA] = ['username', 'password', 'verifyCode'];
		return $scenarios;
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$user = $this->getUser();

			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, Yii::t('app','Incorrect username or password.'));
			}

			if($user){
				switch ($user->status) {
					case User::STATUS_BANNED:
						$this->addError('errors',Yii::t('user','You account is blocked.'));
						break;

					case User::STATUS_INACTIVE:
						$this->addError('errors',Yii::t('user','You account is not activated.'));
						break;
				}
			}
		}
	}

	/**
	 * Logs in a user using the provided username and password.
	 *
	 * @return boolean whether the user is logged in successfully
	 */
	public function login()
	{
		if ($this->validate()) {
			return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
		} else {
			return false;
		}
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	protected function getUser()
	{
		if ($this->_user === null) {
			$this->_user = User::findByUsername($this->username);
		}

		return $this->_user;
	}
}
