<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'concessionaire' => 'دانشگاه فردوسی مشهد',
    'systemname' => 'سامانه ی مدیریت رویدادهای ورزشی /سیزدهین المپیاد دانشجویی کشور'
];
