<?php
return [
	'language'=>'fa-IR',
	  'sourceLanguage'=>'fa',

	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	
	'components' => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		 'authManager' => [
			'class' => 'yii\rbac\DbManager',
		],

		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					// 'sourceLanguage' => 'fa-IR',
					'fileMap' => [
						'zii' => 'zii.php',
					],
				],
			],
		],

		'formatter' => [
			'locale' => 'fa_IR@calendar=persian',
			'calendar' => \IntlDateFormatter::TRADITIONAL,
			'timeZone' => 'UTC',
		],

 
	],
];
